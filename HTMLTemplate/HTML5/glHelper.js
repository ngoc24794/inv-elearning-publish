var GlobalHelper = (function() {
    var btn = document.getElementById("submitControlBtn");
    var isCheckSubmit = true,
        isCaption = false,
        indexVolume = 0.5,
        dataLocal = {},
        isCheckMenu = true;
    return {
        generateBranchId: function(id, key) {
            return id + "_" + key;
        },
        getOriginId: function(branchId) {
            if (branchId != null) {
                var arr = branchId.split('_');
                if (arr.length > 0) {
                    return arr[0];
                }
            }
            return "";
        },
        createTags: function(type, id, className) {
            var xmlNs = "http://www.w3.org/2000/svg";
            switch (type) {
                case "div":
                    {
                        var div = document.createElement('div');
                        if (id) {
                            div.id = id;
                        }
                        if (className) {
                            div.className = className;
                        }
                        return div;
                        break;
                    }
                case "a":
                    {
                        var a = document.createElement('a');
                        if (id) {
                            a.id = id;
                        }
                        if (className) {
                            a.className = className;
                        }
                        return a;
                        break;
                    }
                case "span":
                    {
                        var span = document.createElement('span');
                        if (id) {
                            span.id = id;
                        }
                        if (className) {
                            span.className = className;
                        }
                        return span;
                        break;
                    }
                case "li":
                    {
                        var li = document.createElement('li');
                        if (id) {
                            li.id = id;
                        }
                        if (className) {
                            li.className = className;
                        }
                        return li;
                        break;
                    }
                case "button":
                    {
                        var button = document.createElement('button');
                        if (id) {
                            button.id = id;
                        }
                        if (className) {
                            button.className = className;
                        }
                        return button;
                        break;
                    }
                case "svg":
                    {
                        var svg = document.createElementNS(xmlNs, 'svg');
                        if (id) {
                            svg.setAttribute('id', id);
                        }
                        if (className) {
                            svg.setAttribute('class', className);
                        }
                        return svg;
                        break;
                    }
                case "g":
                    {
                        var g = document.createElementNS(xmlNs, 'g');
                        if (id) {
                            g.setAttribute('id', id);
                        }
                        if (className) {
                            g.setAttribute('class', className);
                        }
                        return g;
                        break;
                    }
                case "path":
                    {
                        var path = document.createElementNS(xmlNs, 'path');
                        if (id) {
                            path.setAttribute('id', id);
                        }
                        if (className) {
                            path.setAttribute('class', className);
                        }
                        return path;
                        break;
                    }
                case "stop":
                    {
                        var stop_1 = document.createElementNS(xmlNs, 'stop');
                        if (id) {
                            stop_1.setAttribute('id', id);
                        }
                        if (className) {
                            stop_1.setAttribute('class', className);
                        }
                        return stop_1;
                        break;
                    }
                case "input":
                    {
                        var input = document.createElement('input');
                        if (id) {
                            input.setAttribute('id', id);
                        }
                        if (className) {
                            input.setAttribute('class', className);
                        }
                        return input;
                        break;
                    }
                case "linearGradient":
                    {
                        var linearGradient = document.createElementNS(xmlNs, 'linearGradient');
                        if (id) {
                            linearGradient.setAttribute('id', id);
                        }
                        if (className) {
                            linearGradient.setAttribute('class', className);
                        }
                        return linearGradient;
                    }
                case "radialGradient":
                    {
                        var radialGradient = document.createElementNS(xmlNs, 'radialGradient');
                        if (id) {
                            radialGradient.setAttribute('id', id);
                        }
                        if (className) {
                            radialGradient.setAttribute('class', className);
                        }
                        return radialGradient;
                    }
                case "pattern":
                    {
                        var pattern = document.createElementNS(xmlNs, 'pattern');
                        if (id) {
                            pattern.setAttribute('id', id);
                        }
                        if (className) {
                            pattern.setAttribute('class', className);
                        }
                        return pattern;
                    }
                case "image":
                    {
                        var image = document.createElementNS(xmlNs, 'image');
                        if (id) {
                            image.setAttribute('id', id);
                        }
                        if (className) {
                            image.setAttribute('class', className);
                        }
                        return image;
                    }
                case "defs":
                    {
                        var defs = document.createElementNS(xmlNs, 'defs');
                        return defs;
                    }
                case "tspan":
                    {
                        var tspan = document.createElement('tspan');
                        if (id) {
                            tspan.id = id;
                        }
                        if (className) {
                            tspan.className = className;
                        }
                        return tspan;
                        break;
                    }
                case 'text':
                    {
                        var text = document.createElementNS(xmlNs, "text");
                        if (id) {
                            text.setAttribute('id', id);
                        }
                        if (className) {
                            text.setAttribute('class', className);
                        }
                        return text;
                    }
                case 'rect':
                    {
                        var rect = document.createElementNS(xmlNs, "rect");
                        if (id) {
                            rect.setAttribute('id', id);
                        }
                        if (className) {
                            rect.setAttribute('class', className);
                        }
                        return rect;
                    }
                case 'textarea':
                    {
                        var textarea = document.createElement('textarea');
                        return textarea;
                    }
                case 'circle':
                    {
                        var circle = document.createElementNS(xmlNs, 'circle');
                        return circle;
                    }
            }
        },
        appendIdContent: function(elementParent, elementChild) {
            elementParent.appendChild(elementChild);
        },
        nextSlide: function() {
            var currentSlideIndex = DOC.slds.indexOf(GlobalResources.CurrentSlide.Data);
            if (currentSlideIndex < DOC.slds.length - 1) {
                this.jumToSlideIndex(currentSlideIndex + 1);
                $("*[data-index = " + (currentSlideIndex) + "]").removeClass("menuSelected");
                $("*[data-index = " + (currentSlideIndex + 1) + "]").addClass("menuSelected");
            }
        },
        previousSlide: function() {
            var currentSlideIndex = DOC.slds.indexOf(GlobalResources.CurrentSlide.Data);
            if (currentSlideIndex > 0) {
                this.jumToSlideIndex(currentSlideIndex - 1);
                $("*[data-index = " + (currentSlideIndex) + "]").removeClass("menuSelected");
                $("*[data-index = " + (currentSlideIndex - 1) + "]").addClass("menuSelected");
            }
        },
        jumToSlideID: function(slideID) {
            for (var i = 0; i < DOC.slds.length; i++) {
                var slideData = DOC.slds[i];
                if (slideData.id == slideID) {
                    this.jumToSlideIndex(i);
                    this.menuSelected(slideID + "_li");
                    return;
                }
            }
        },
        loadSlideCurrent: function() {
            this.jumToSlideID($('#menuContent > .menuSelected').attr('id').split('_')[0]);
        },
        jumToSlideIndex: function(slideIndex) {
            this.clearNotifi();
            var slideData = DOC.slds[slideIndex];
            var slide = new Slide(slideData, slideIndex);
            GlobalResources.CurrentSlide = slide;
            var currentSlideIndex = DOC.slds.indexOf(GlobalResources.CurrentSlide.Data);
            this.appendIdContent($("#content")[0], slide.generateHTML());
            setNavigationControl(slideData.slC, DOC.plr);
            setSidebarControl(slideData.slC, DOC.plr);
            helperQues();
            slide.setTriggers();
            genNote(slideData);
            slide.load();
            this.menuSelected(slideData.id + "_li");
        },
        menuSelected: function(selectedId) {
            $('#menuContent li').removeClass("menuSelected");
            $("#" + selectedId).addClass("menuSelected");
        },
        play: function() {
            var timeLine = null;
            if (GlobalResources.CurrentSlide != null && GlobalResources.CurrentSlide.MainTimeline != null) {
                timeLine = GlobalResources.CurrentSlide.MainTimeline;
            }
            if (timeLine != null) {
                timeLine.play();
            }
        },
        pause: function() {
            var timeLine = null;
            if (GlobalResources.CurrentSlide != null && GlobalResources.CurrentSlide.MainTimeline != null) {
                timeLine = GlobalResources.CurrentSlide.MainTimeline;
            }
            if (timeLine != null) {
                timeLine.pause();
            }
        },
        reset: function() {
            if (GlobalResources.CurrentSlide != null) {
                GlobalResources.CurrentSlide.load();
            }
        },
        hiddenSubmit: function() {
            btn.style.display = "none";
        },
        showSubmit: function() {
            btn.style.display = "flex";
        },
        setIsCaption: function(isCap) {
            isCaption = isCap;
        },
        getIsCaption: function() {
            return isCaption;
        },
        showSubmitBtn: function(slideData) {
            if (slideData.slC.sB) {
                this.showSubmit();
            } else {
                this.hiddenSubmit();
            }
        },
        onloaded: function() {
            this.jumToSlideIndex(0);
        },
        clearNotifi: function() {
            var eleMessage = document.getElementById('contain-message');
            if (eleMessage != null) {
                eleMessage.parentElement.removeChild(eleMessage);
            }
        },
        isNumberKey: function(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46)
                return false;
            return true;
        },
        hiddenVolumeSetting: function() {
            if (wrapperVol) {
                wrapperVol.style.display = '';
                if (volumeControl.select) {
                    volumeControl.classList.remove("volumeBtnSelected");
                    volumeControl.select = false;
                }
            }
            var eleSetting = document.getElementById('settingContentWrapper');
            var eleBtnSetting = document.getElementById("settingControlBtn");
            if (eleSetting && eleBtnSetting) {
                eleSetting.setAttribute('class', 'hidden');
                eleBtnSetting.classList.remove("settingBtnSelected");
            }
        },
        disbleSlideControl: function() {
            var slidePanel = document.getElementById('sidePanel');
            var controlPane = document.getElementById('controlPanel');
            var eleSlide = document.getElementById(GlobalResources.CurrentSlide.Data.id);
            if (slidePanel) {
                slidePanel.style.pointerEvents = 'none';
            }
            if (controlPane) {
                controlPane.style.pointerEvents = 'none';
            }
            if (eleSlide) {
                eleSlide.style.pointerEvents = 'none';
            }
        },
        enableSlideControl: function() {
            var slidePanel = document.getElementById('sidePanel');
            var controlPane = document.getElementById('controlPanel');
            if (slidePanel) {
                slidePanel.style.pointerEvents = '';
            }
            if (controlPane) {
                controlPane.style.pointerEvents = '';
            }
        },
        set DataLocal(data) {
            dataLocal = data;
        },
        get DataLocal() {
            return dataLocal;
        },
        set Volume(volume) {
            indexVolume = volume;
        },
        get Volume() {
            return indexVolume;
        },
        set IsSubmit(isSubmit) {
            isCheckSubmit = isSubmit;
        },
        get IsSubmit() {
            return isCheckSubmit;
        },
        set IsMenu(isMenu) {
            isCheckMenu = isMenu;
        },
        get IsMenu() {
            return isCheckMenu;
        }
    };

    function clearContainer() {
        $("#content")[0].innerHTML = "";
    }

    function helperQues() {
        var check = false;
        $("#caption").html("");
        if (GlobalResources.CurrentSlide.Data.slt == 'Result') {
            check = true;
            QuizzModule.ProcessResult(GlobalResources.CurrentSlide.Data, 1);
        }
        var i = DOC.slds.indexOf(GlobalResources.CurrentSlide.Data);
        if (i != 0) {
            if (GlobalResources.CurrentSlide.Data.slt == "QuestionSlide") {
                if (!check) {
                    if (GlobalHelper.DataLocal.Slide['slide' + i].IsSubmit == true) {
                        document.getElementById("submitControlBtn").style.display = "none";
                        showNextPrev();
                        isCheckSubmit = false;
                    } else {
                        isCheckSubmit = true;
                    }
                }
                var ele = document.getElementsByClassName('answerContainer')[0];
                if (ele != null) {
                    ele.style.pointerEvents = '';
                }
            } else {
                GlobalHelper.IsSubmit = false;
            }
            GlobalHelper.DataLocal.CurrentSlide = i;
            try {
                localStorage.setItem(KEY, Ma.en(JSON.stringify(GlobalHelper.DataLocal)));
            } catch (error) {
                console.log("Not support localStorage");
            }

        }
    }

    function showNextPrev() {
        var elePrev = document.getElementById('prevControlBtn');
        var eleNext = document.getElementById('nextControlBtn');
        if (elePrev || eleNext) {
            elePrev.style.display = 'flex';
            eleNext.style.display = 'flex';
        }
    }
})();
String.prototype.toHHMMSS = function() {
    var sec_num = parseInt(this, 10);
    var hours = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);
    if (hours < 10) {
        hours = "0" + hours;
    }
    if (minutes < 10) {
        minutes = "0" + minutes;
    }
    if (seconds < 10) {
        seconds = "0" + seconds;
    }
    var result = minutes + ':' + seconds;
    if (sec_num > 3600)
        result = hours + ':' + result;
    return result;
};
var btn = document.getElementById("submitControlBtn");
var btnResume = document.getElementById('resumeBtn');
var btnNotResume = document.getElementById('notResumeBtn');
var elemResume = document.getElementById('resumeContentWrapper');
if (btnResume) {
    btnResume.onclick = function() {
        GlobalHelper.jumToSlideIndex(GlobalHelper.DataLocal.CurrentSlide);
        elemResume.style.display = 'none';
         QuizzModule.SetTimeSlide();
    }
}
if (btnNotResume) {
    btnNotResume.onclick = function() {
        QuizzModule.InitLocalStorage();
        GlobalHelper.onloaded();
        elemResume.style.display = 'none';
         QuizzModule.SetTimeSlide();
    }
}
if (btn) {
    btn.addEventListener('click', function() {
        QuizzModule.SubmitInteraction();
    });
}
//xử lí ẩn những volume và menu khi click vào ở ngoài
if (content) {
    content.addEventListener('click', function() {
        GlobalHelper.hiddenVolumeSetting();
    }, false);
}