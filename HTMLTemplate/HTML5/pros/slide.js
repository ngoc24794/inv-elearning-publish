function Slide(slideData, slideIndex) {
    var slideData = slideData;
    var slideIndex = slideIndex || null;
    var divRoot;
    var mySelf = null;
    return {
        generateHTML: function () {
            mySelf = this;
            if (slideData != null) {
                divRoot = GlobalHelper.createTags("div", slideData.id, '');
                $(divRoot).addClass("stretch-fill");
                var divContainer = GlobalHelper.createTags("div", '', '');
                $(divContainer).addClass("stretch-fill");
                divRoot.style.position = "absolute";
                divRoot.style.background = "#fff";
                divRoot.style.top = "0px";
                GlobalHelper.appendIdContent(divRoot, divContainer);
                GlobalHelper.showSubmitBtn(slideData);
                var feedBack;
                if(slideIndex){
                    feedBack = this.getFeedBackQuestion();
                }  
                for (var i = 0; i < slideData.lts.length; i++) {
                    var layoutData = slideData.lts[i];
                    if (layoutData != null) {
                        var layoutModule = GlobalResources.getLayoutModule(layoutData.lT);
                        if (layoutModule != null) {
                            var layout = layoutModule.generateLayout(layoutData, slideData);
                            if (layout && (typeof layout.generateHTML !== 'underfinded')) {
                                GlobalHelper.appendIdContent(divContainer, layout.generateHTML());
                                if (feedBack) {
                                    if (layoutData.id === feedBack.IdFeedback) {
                                        layout.show();
                                        layout.setStyleForFeedback();
                                    }
                                }
                                this.Layouts.push(layout);
                            }
                        }
                    }
                }
                return divRoot;
            }
        },
        getFeedBackQuestion: function () {
            var feedBack;
            if (GlobalResources.CurrentSlide.Data.slt == "QuestionSlide") {
                feedBack = GlobalHelper.DataLocal.Slide['slide' + slideIndex];
            }
            return feedBack;
        },
        getLayoutById: function (id) {
            for (var i = 0; i < this.Layouts.length; i++) {
                var child = this.Layouts[i];
                if (child.Data.id == id) {
                    return child;
                }
            }
        },
        getElementById: function (id) {
            for (var i = 0; i < this.Layouts.length; i++) {
                var layout = this.Layouts[i];
                for (var j = 0; j < layout.Elements.length; j++) {
                    var element = layout.Elements[j];
                    if (element.Data.id == id) {
                        return element;
                    }
                }
            }
        },
        setTriggers: function () {
            for (var i = 0; i < this.Layouts.length; i++) {
                this.Layouts[i].setTriggers();
            }
        },
        get Data() {
            return slideData;
        },
        load: function () {
            if (mySelf.Layouts.length > 0) {
                mySelf.Layouts[0].show();
                mySelf.Layouts[0].Timeline.pause();
            }
            TransitionModule.setTransition(slideData, slideData.trans, startAnimation);
        },
        unload: function () { },
        Layouts: [],
        get MainTimeline() {
            for (var i = 0; i < this.Layouts.length; i++) {
                var layout = this.Layouts[i];
                if (layout.Data.iM == true) {
                    return layout.Timeline;
                }
            }
            return null;
        }
    };
    function startAnimation() {
        if (mySelf.Layouts.length > 0)
            mySelf.Layouts[0].show(true);
        clearContainer();
    }
    function clearContainer() {
        for (var i = 0; i < $("#content")[0].children.length; i++) {
            var child = $("#content")[0].children[i];
            if (child.id != GlobalResources.CurrentSlide.Data.id && child.id != "caption") {
                $(child).remove();
            }
        }
    }
}
