function Layout(layoutData, slideData) {
    var layoutData = layoutData;
    var slideData = slideData;
    var divRoot;
    var isVisible = false;
    return {
        generateHTML: function () {
            if (layoutData != null) {
                if (layout != slideData.the && this.Timeline == null) {
                    this.Timeline = new TimelineMax();
                    this.Timeline.eventCallback("onUpdate", SliderModule.updateSlider);
                    if (layoutData.tim) {
                        this.Timeline.set({}, {}, layoutData.tim.dT);
                    }
                    this.Timeline.set({}, {}, layoutData.tim.dT);
                    this.Timeline.progress(0);
                    this.Timeline.pause();
                }
                divRoot = GlobalHelper.createTags("div", layoutData.id + '_w', '');
                divRoot.style.display = "none";
                //divRoot.style.position = 'absolute';
                $(divRoot).addClass("stretch-fill");
                var svgBackground = generateLayoutBackground(layoutData, slideData);
                GlobalHelper.appendIdContent(divRoot, svgBackground);
                if (layoutData != slideData.the && layoutData.iM) {
                    if (slideData.the.e) {
                        var themeBackground = (new Layout(slideData.the, slideData)).generateHTML();
                        GlobalHelper.appendIdContent(divRoot, themeBackground);
                        themeBackground.style.display = "block";
                        //themeBackground.style.zIndex = "0";
                        themeBackground.style.position = "absolute";
                        themeBackground.style.top = 0;
                    }
                }
                var divContainer = GlobalHelper.createTags("div", '', '');
                $(divContainer).addClass("stretch-fill");
                // divContainer.style.top = 0;
                // divContainer.style.position = 'absolute';
                GlobalHelper.appendIdContent(divRoot, divContainer);
                if (layoutData.e != null) {
                    for (var i = 0; i < layoutData.e.length; i++) {
                        var elementData = layoutData.e[i];
                        var element = new Element(elementData, slideData, layoutData.iM);
                        if (element && (typeof element.generateHTML !== 'underfined')) {
                            var elementTag = element.generateHTML();
                            if (elementTag) {
                                GlobalHelper.appendIdContent(divContainer, elementTag);
                                this.Elements.push(element);
                            }
                            element.onEnter = function (s) {
                            };
                        }
                    }
                }
                return divRoot;
            }
        },
        show: function (withAnimation) {
            if (withAnimation === void 0) { withAnimation = false; }
            divRoot.style.display = "block";
            GlobalResources.CurrentLayer = divRoot;
            this.setAnimations();
            if (this.Timeline != null) {
                this.Timeline.progress(0.001);
                if (withAnimation)
                    this.Timeline.play();
            }
            this.triggerExcute("TimelineStarts");
        },
        hide: function () {
            divRoot.style.display = "none";
            if (this.Timeline != null) {
                this.Timeline.stop();
            }
        },
        setStyleForFeedback: function () {
            divRoot.style.top = '0px';
            divRoot.style.left = '0px';
            divRoot.style.position = 'absolute';
        },
        setTriggers: function () {
            for (var i = 0; i < this.Elements.length; i++) {
                this.Elements[i].setTriggers();
            }
            TriggerModule.setTriggers(layoutData.id, layoutData.tri, { tl: this.Timeline, data: this.Data });
        },
        reLoad: function () {
        },
        getChildById: function (id) {
            for (var i = 0; i < this.Elements.length; i++) {
                var child = this.Elements[i];
                if (child.Data.Id == id) {
                    return child;
                }
            }
        },
        setAnimations: function () {
            for (var i = 0; i < this.Elements.length; i++) {
                var element = this.Elements[i];
                element.setAnimations(this.Timeline);
                element.setTimeline(this.Timeline);
            }
        },
        get Data() {
            return layoutData;
        },
        get IsVisible() {
            return isVisible;
        },
        set IsVisible(value) {
            isVisible = value;
            if (isVisible) {
                this.show();
            }
            else {
                this.hide();
            }
        },
        Elements: [],
        Timeline: null,
        triggerExcute: function (event) {
            var layers = GlobalResources.CurrentSlide.Data.lts;
            var currentLayer = null;
            if (GlobalResources.CurrentLayer != null) {
                for (var i = 0; i < layers.length; i++) {
                    if (layers[i].id + "_w" == GlobalResources.CurrentLayer.id) {
                        currentLayer = layers[i];
                        break;
                    }
                }
                if (currentLayer != null) {
                    if (currentLayer.tri != null) {
                        currentLayer.tri.forEach(function (trigger) {
                            if (trigger.E == event) {
                                TriggerActionModule.getTriggerAction(trigger);
                            }
                        });
                    }
                }
            }
        }
    };
    function generateLayoutBackground(layoutData, slideData) {
        var svgLayout = GlobalHelper.createTags('svg', '', '');
        svgLayout.id = GlobalHelper.generateBranchId(layoutData.id, "bgr");
        svgLayout.setAttribute('width', slideData.w);
        svgLayout.setAttribute('height', slideData.h);
        svgLayout.setAttribute('position', 'absolute');
        var rectSvgLayout = GlobalHelper.createTags('rect', '', '');
        rectSvgLayout.setAttribute('x', 0);
        rectSvgLayout.setAttribute('y', 0);
        rectSvgLayout.setAttribute('width', '100%');
        rectSvgLayout.setAttribute('height', '100%');
        if (layoutData.bg) {
            var fillColor = FillColorHelper.genGTag(layoutData.bg, { id: layoutData.id, width: svgLayout.attributes["width"].value, height: svgLayout.attributes["height"].value });
            if (fillColor) {
                rectSvgLayout.setAttribute("fill", fillColor.ID);
                if (fillColor.Tag)
                    GlobalHelper.appendIdContent(svgLayout, fillColor.Tag);
            }
        }
        GlobalHelper.appendIdContent(svgLayout, rectSvgLayout);
        return svgLayout;
    }
}
var NormalLayoutModule = (function () {
    return {
        Level: GlobalEnum.ModuleLevel.Layout,
        ContentType: "Normal",
        generateLayout: function (layoutData, slideDataOwner) {
            return new Layout(layoutData, slideDataOwner);
        }
    };
})();
GlobalResources.addModule(NormalLayoutModule);