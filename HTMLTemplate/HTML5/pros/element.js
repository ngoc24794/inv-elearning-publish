"use strict";
function Element(elementData, slideData,isMainLayout) {
    var elementData = elementData;
    var slideData = slideData;
    var divContainer;
    var isVisible = false;
    return {
        generateHTML: function () {
            if (elementData != null) {
                var curModule = GlobalResources.getElementModule(elementData.c);
                var content;
                divContainer = GlobalHelper.createTags("div", elementData.id, '');
                var gShape = ShapeHelper.generateShapes(elementData.shs, { "id": elementData.id, "width": elementData.w, "height": elementData.h });
                divContainer.style.position = "absolute";
                divContainer.style.width = elementData.w + "px";
                divContainer.style.height = elementData.h + "px";
                divContainer.style.top = elementData.T + "px";
                divContainer.style.left = elementData.l + "px";
                divContainer.style.zIndex = elementData.z;
                divContainer.style.transform = "rotate(" + elementData.agl + "deg)";
                if (elementData.opc) {
                    divContainer.style.opacity = elementData.opc;
                }
                if (elementData.c=="Answers") {
                    var svgShape = GlobalHelper.createTags("svg", GlobalHelper.generateBranchId(elementData.id, 's'), '');
                    svgShape.setAttribute('width', elementData.w + "px");
                    svgShape.setAttribute('height', elementData.h + "px");
                    svgShape.style.overflow = "unset";
                    var useHTML = null;
                    var g = null;
                    var check = false;
                    if (elementData.efs) {
                        if (elementData.efs[0].typ == "SoftEdge") {
                            check = true;
                        }
                        for (var i = 0; i < elementData.efs.length; i++) {
                            var effEle = EffectModule.genGEffect(elementData.efs[i], { Id: elementData.id, width: elementData.w, height: elementData.h }, elementData.agl);
                            if (effEle.use !== undefined) {
                                useHTML = effEle.use;
                            }
                            if (svgShape.firstChild == undefined) {
                                svgShape.appendChild(effEle.g);
                                effEle.g.outerHTML = effEle.g.outerHTML;
                            }
                            else {
                                var defs = effEle.g.querySelector('defs');
                                if (defs != null) {
                                    svgShape.firstChild.appendChild(defs);
                                }
                                var use = effEle.g.querySelector('use');
                                if (use != undefined) {
                                    svgShape.firstChild.appendChild(use);
                                }
                            }
                        }
                    }
                    if (!check) {
                        svgShape.appendChild(gShape);
                    }
                    divContainer.appendChild(svgShape);
                }
                if (curModule != null) {
                    content = curModule.generateElement(elementData, slideData,isMainLayout);
                    if (content.tagName.toLowerCase() == "div") {
                        GlobalHelper.appendIdContent(divContainer, content);
                        return divContainer;
                    }
                }
                if (content == null || content == undefined || (content.tagName.toLowerCase() == "g")) {
                    var svgContainer = GlobalHelper.createTags("svg", GlobalHelper.generateBranchId(elementData.id, 's'), '');
                    svgContainer.setAttribute('xmlns', "http://www.w3.org/2000/svg");
                    svgContainer.style.position = "absolute";
                    svgContainer.setAttribute("width", +elementData.w);
                    svgContainer.setAttribute("height", +elementData.h);
                    // svgContainer.setAttribute('viewBox', "-2 -2 147 123");
                    svgContainer.style.overflow = "unset";
                    var shape = ShapeHelper.generateShapes(elementData.shs, { id: elementData.id, width: elementData.w, height: elementData.h });
                    var useHTML = null;
                    var g = null;
                    if (elementData.efs) {
                        for (var i = 0; i < elementData.efs.length; i++) {
                            var effEle = EffectModule.genGEffect(elementData.efs[i], { Id: elementData.id, width: elementData.w, height: elementData.h }, elementData.agl);
                            if (effEle.use !== undefined) {
                                useHTML = effEle.use;
                            }
                            if (svgContainer.firstChild == undefined) {
                                svgContainer.appendChild(effEle.g);
                                effEle.g.outerHTML = effEle.g.outerHTML;
                            }
                            else {
                                var defs = effEle.g.querySelector('defs');
                                if (defs != null) {
                                    svgContainer.firstChild.appendChild(defs);
                                }
                                var use = effEle.g.querySelector('use');
                                if (use != undefined) {
                                    svgContainer.firstChild.appendChild(use);
                                }
                            }
                        }
                    }
                    if (elementData.shs) {
                        var image = ImageModule.generateElement(elementData, slideData);
                        if (elementData.efs == null || elementData.efs[0].typ !== "SoftEdge") {
                            if (elementData.c === "Image") {
                                shape = null;
                                if (useHTML != null) {
                                    GlobalHelper.appendIdContent(image, useHTML);
                                    g = image;
                                    useHTML.outerHTML = useHTML.outerHTML;
                                }
                            }
                            else {
                                if (useHTML != null) {
                                    GlobalHelper.appendIdContent(shape, useHTML);
                                    useHTML.outerHTML = useHTML.outerHTML;
                                }
                                GlobalHelper.appendIdContent(svgContainer, shape);
                            }
                        }
                    }
                    if (content)
                        GlobalHelper.appendIdContent(svgContainer, content);
                    GlobalHelper.appendIdContent(divContainer, svgContainer);
                    return divContainer;
                }
            }
        },
        load: function () {
            divContainer.style.display = "block";
            var evt = new CustomEvent('onLoad', null);
            divContainer.dispatchEvent(evt);
        },
        unload: function () {
            divContainer.style.display = "none";
            var evt = new CustomEvent('onUnload', null);
            divContainer.dispatchEvent(evt);
        },
        changeState: function () {
        },
        setAnimations: function (timline) {
            AnimationModule.setAnimations(elementData, timline);
        },
        setTriggers: function () {
            TriggerModule.setTriggers(elementData.id, elementData.tri, this);
        },
        setTimeline: function (timeline) {
            if (timeline != null) {
                if (elementData.tim.sT > 0) {
                    timeline.set(this, { IsVisible: false }, 0);
                    timeline.set(this, { IsVisible: true }, elementData.tim.sT);
                }
                if (elementData.tim.sT + elementData.tim.dT < Math.round(slideData.tim.dT)) {
                    timeline.set(this, { IsVisible: false }, elementData.tim.sT + elementData.tim.dT);
                }
            }
        },
        get Data() {
            return elementData;
        },
        get IsVisible() {
            return isVisible;
        },
        set IsVisible(value) {
            if (value == true) {
                this.load();
            }
            else {
                value = false;
                this.unload();
            }
            isVisible = value;
        },
        set onLoad(value) {
            if (divContainer != null) {
                divContainer.addEventListener('onLoad', value);
            }
        },
        set onUnload(value) {
            if (divContainer != null) {
                divContainer.addEventListener('onUnload', value);
            }
        }
    };
}
