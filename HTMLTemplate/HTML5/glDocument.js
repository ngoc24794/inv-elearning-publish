var GlobalDocument = (function () {
    return {
        generateElement: function (elementData, slideData) {
            if (elementData != null) {
                var curModule = GlobalResources.getElementModule(elementData.c);
                if (curModule != null) {
                    var content = curModule.generateElement(elementData, slideData);
                    if (content.tagName == "div") {
                        return content;
                    }
                    else if (content.tagName == "g") {
                        var divContainer = GlobalHelper.createTags("div", elementData.Id, '');
                        var svgContainer = GlobalHelper.createTags("svg", '', '');
                        GlobalHelper.appendIdContent(svgContainer, content);
                        var shape = ShapeHelper.generateShapes(elementData.shs, { ID: elementData.Id, Width: elementData.w, Height: elementData.h });
                        GlobalHelper.appendIdContent(svgContainer, shape);
                        GlobalHelper.appendIdContent(divContainer, svgContainer);
                        return divContainer;
                    }
                }
            }
        },
        generateLayout: function (layoutData, slideData) {
            var divRoot = GlobalHelper.createTags("div", layoutData.Id, '');
            var svgBackground = generateLayoutBackground(layoutData);
            GlobalHelper.appendIdContent(divRoot, svgBackground);
            var divContainer = GlobalHelper.createTags("div", '', '');
            GlobalHelper.appendIdContent(divRoot, divContainer);
            for (var i = 0; i < layoutData.e.length; i++) {
                var elementData = layoutData.e[i];
                var element = this.generateElement(elementData, slideData);
                if (element) {
                }
            }
        }
    };
})();
