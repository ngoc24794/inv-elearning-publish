var progress = document.getElementById("progressVolume");
var wrapperVol = document.getElementById('wrapperControlVolumeSound');
var volumeControl = document.getElementById('volumeControlBtn');
var volume = document.getElementById('volume');
var mute = document.getElementById('mute');
var volumeBtn = document.getElementById('volumeBtn');
var muteBtn = document.getElementById('muteBtn');
$('.tabs li').click(function (e) {
    $('.tabs li').removeClass("tabSelected");
    $(this).addClass("tabSelected");
    $('#contentSidebar section').removeClass("contentSidebarSelected");
    $("#" + $(this).data('id')).addClass("contentSidebarSelected");
});
$('#settingControlBtn').click(function (e) {
    if ($(this).hasClass('settingBtnSelected')) {
        $(this).removeClass('settingBtnSelected');
        $('#settingContentWrapper').addClass('hidden');
    }
    else {
        $(this).addClass('settingBtnSelected');
        $('#settingContentWrapper').removeClass('hidden');
    }
});
$("#uncheckedMenu").change(function () {
    if (!this.checked) {
        $('#sidePanel').addClass('hidden');
    }
    else {
        $('#sidePanel').removeClass('hidden');
    }
    calculateSize();
});
$("#uncheckedMenu").prop('checked', true);
volumeControl.select = false;
volumeControl.addEventListener('click', function () {
    if (this.select) {
        wrapperVol.style.display = 'none';
        $(this).removeClass('volumeBtnSelected');
    }
    else {
        wrapperVol.style.display = 'flex';
        $(this).addClass('volumeBtnSelected');
    }
    this.select = !this.select;
});
$("#controlVolume").draggable({
    axis: 'y',
    containment: "#controlVolumeSound",
    drag: function (event, ui) {
        this.style.background = '#fff';
        var a = ui.helper.context.offsetTop;
        if (a == 37) {
            ChangeVolume(false);
        }
        else {
            ChangeVolume(true);
        }
        progress.style.height = (a / 37) * 100 + "%";
        wrapperVol.style.display = 'flex';
        GlobalHelper.Volume = ((37 - a) / 37);
        volumeVideo(GlobalHelper.Volume);
    },
    stop: function () {
        this.style.background = "";
    }
});
document.getElementById("fullScreenControlBtn").select = false;
document.getElementById("fullScreenControlBtn").addEventListener('click', function () {
    var eleBody = document.getElementsByTagName("BODY")[0];
    if (!this.select) {
        launchFullScreen(eleBody);
        changeEleScreen(false);
    }
    else {
        cancelFullscreen(eleBody);
        changeEleScreen(true);
    }
    this.select = !this.select;
}, false);
volume.addEventListener('click', function () {
    this.style.display = 'none';
    mute.style.display = 'block';
}, false);
mute.addEventListener('click', function () {
    this.style.display = 'none';
    ;
    volume.style.display = 'block';
}, false);
document.getElementById('captionControlBtn').querySelector('label').addEventListener('click', function () {
    GlobalResources.ShowCaption = !GlobalResources.ShowCaption;
}, false);
function volumeVideo(percent) {
    var videos = document.getElementById('content').querySelectorAll('video');
    if (videos.length > 0) {
        videos.forEach(function print(ele) {
            ele.volume = percent;
        });
    }
}
function ChangeVolume(state) {
    if (typeof volume !== 'undefined' && typeof (mute) !== 'undefined') {
        if (state === false) {
            volume.style.display = 'none';
            mute.style.display = 'block';
            volumeBtn.style.display = 'none';
            muteBtn.style.display = 'block';
        }
        else {
            volume.style.display = 'block';
            mute.style.display = 'none';
            volumeBtn.style.display = 'block';
            muteBtn.style.display = 'none';
        }
    }
}
function changeEleScreen(state) {
    if (!state) {
        document.getElementById('fullscreen').style.display = 'none';
        document.getElementById('miniScreen').style.display = 'block';
        $("#uncheckedMenu").prop('checked', false);
        $('#sidePanel').addClass('hidden');
        $('#wrapper').addClass('hiddenSideBar');
    }
    else {
        document.getElementById('fullscreen').style.display = 'block';
        document.getElementById('miniScreen').style.display = 'none';
        $("#uncheckedMenu").prop('checked', true);
        $('#sidePanel').removeClass('hidden');
        $('#wrapper').removeClass('hiddenSideBar');
    }
}
function launchFullScreen(element) {
    if (element.requestFullScreen) {
        element.requestFullScreen();
    }
    else if (element.mozRequestFullScreen) {
        element.mozRequestFullScreen();
    }
    else if (element.webkitRequestFullScreen) {
        element.webkitRequestFullScreen();
    }
    else if (element.msRequestFullScreen) {
        element.msRequestFullScreen();
    }
}
function cancelFullscreen() {
    if (document.cancelFullScreen) {
        document.cancelFullScreen();
    }
    else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
    }
    else if (document.webkitCancelFullScreen) {
        document.webkitCancelFullScreen();
    }
}
function heightThumbScroll() {
    var menuContentHeight = document.getElementById('menuContentWrapper').offsetHeight;
    var menuListHeight = document.getElementById('menuListContent').offsetHeight;
    var scrollBar = document.getElementById('menuScrollBar');
    scrollBar.style.height = Math.min(1, menuListHeight / menuContentHeight) * menuListHeight + "px";
    scrollBar.style.display = scrollBar.style.offsetHeight == menuListHeight ? "none" : "block";
}
heightThumbScroll();
function checkScroll() {
    var mListContentHeight = document.getElementById('menuListContent').offsetHeight;
    var mContentHeight = document.getElementById('menuContent').offsetHeight;
    var mScrollWrapper = document.getElementById('menuScrollWrapper');
    if (mContentHeight <= mListContentHeight) {
        mScrollWrapper.style.display = "none";
    }
    else {
        mScrollWrapper.style.display = "block";
    }
}
menuContentWrapper.addEventListener("mouseover", heightThumbScroll);
menuContentWrapper.addEventListener("mouseover", checkScroll);
function mouseMenuWheel(e) {
    var menuListHeight = document.getElementById('menuListContent').offsetHeight;
    var menuContentWrapper = document.getElementById('menuContentWrapper');
    var menuContentHeight = menuContentWrapper.offsetHeight;
    var scrollBar = document.getElementById('menuScrollBar');
    var scrollBarHeight = scrollBar.offsetHeight;
    var deltaChange = menuListHeight / 5;
    var deltaHeightContent = Math.max(0, menuContentHeight - menuListHeight);
    var deltaHeightScroll = Math.max(0, menuListHeight - scrollBarHeight);
    if (e.deltaY < 0) {
        menuContentWrapper.style.top = Math.min(0, menuContentWrapper.offsetTop + deltaChange) + "px";
    }
    else if (e.deltaY > 0) {
        menuContentWrapper.style.top = Math.max(-deltaHeightContent, menuContentWrapper.offsetTop - deltaChange) + "px";
    }
    var scrollTop = -menuContentWrapper.offsetTop / deltaHeightContent * deltaHeightScroll;
    scrollBar.style.top = Math.min(deltaHeightScroll, scrollTop) + "px";
}
menuContentWrapper.addEventListener("wheel", mouseMenuWheel);
var isClickScrollBar = 0;
var mouseThumbFlag = 0, preY = 0, startPostion = 0, startScrollOffset = 0;
function thumbMouseEvent() {
    isClickScrollBar = 1;
    var scrollThumb = document.getElementById('menuScrollBar');
    scrollThumb.addEventListener('mousedown', function (e) {
        document.addEventListener("mousemove", mouseThumbMove, false);
        mouseThumbFlag = 1;
        startPostion = e.pageY;
        startScrollOffset = scrollThumb.offsetTop;
        this.style.opacity = 0.7;
    }, false);
    document.addEventListener("mouseup", function () {
        mouseThumbFlag = 0;
        document.removeEventListener("mousemove", mouseThumbMove);
        scrollThumb.style.opacity = 0.5;
    }, false);
    menuContentWrapper.addEventListener('touchstart', function (e) {
        menuContentWrapper.addEventListener("touchmove", mouseThumbMove, false);
        mouseThumbFlag = 1;
        startPostion = e.touches[0].pageY;
        startScrollOffset = scrollThumb.offsetTop;
        this.style.opacity = 0.7;
    }, false);
    menuContentWrapper.addEventListener("touchend", function () {
        mouseThumbFlag = 0;
        menuContentWrapper.removeEventListener("touchmove", mouseThumbMove);
        scrollThumb.style.opacity = 0.5;
    }, false);
}
function mouseThumbMove(e) {
    if (mouseThumbFlag === 1) {
        preY = e.pageY != undefined ? e.pageY : e.touches[0].pageY;
        var deltaChange = preY - startPostion;
        var menuListHeight = document.getElementById('menuListContent').offsetHeight;
        var menuContentWrapper = document.getElementById('menuContentWrapper');
        var menuContentHeight = menuContentWrapper.offsetHeight;
        var scrollBar = document.getElementById('menuScrollBar');
        var scrollBarHeight = scrollBar.offsetHeight;
        var deltaHeightContent = Math.max(0, menuContentHeight - menuListHeight);
        var deltaHeightScroll = Math.max(0, menuListHeight - scrollBarHeight);
        scrollBar.style.top = Math.min(deltaHeightScroll, Math.max(0, startScrollOffset + deltaChange)) + "px";
        var contentTop = -scrollBar.offsetTop / deltaHeightScroll * deltaHeightContent;
        menuContentWrapper.style.top = Math.max(-deltaHeightContent, contentTop) + "px";
    }
}
thumbMouseEvent();
var topL = 0;
document.getElementById('menuScrollWrapper').addEventListener("mousedown", function (e) {
    if (e.srcElement != this)
        return;
    var menuListHeight = document.getElementById('menuListContent').offsetHeight;
    var menuContentWrapper = document.getElementById('menuContentWrapper');
    var menuContentHeight = menuContentWrapper.offsetHeight;
    var scrollBar = document.getElementById('menuScrollBar');
    var scrollBarHeight = scrollBar.offsetHeight;
    var deltaHeightContent = Math.max(0, menuContentHeight - menuListHeight);
    var deltaHeightScroll = Math.max(0, menuListHeight - scrollBarHeight);
    var scrollTop = 0;
    if (e.layerY > scrollBar.offsetTop + scrollBarHeight) {
        scrollTop = e.layerY - scrollBarHeight;
    }
    else if (e.layerY < scrollBar.offsetTop) {
        scrollTop = e.layerY;
    }
    scrollBar.style.top = Math.min(deltaHeightScroll, Math.max(0, scrollTop)) + "px";
    var contentTop = -scrollBar.offsetTop / deltaHeightScroll * deltaHeightContent;
    menuContentWrapper.style.top = Math.max(-deltaHeightContent, contentTop) + "px";
});
