var xmlns = "http://www.w3.org/2000/svg";
var KEY = DOC.slds[0].id;

var click = {
    x: 0,
    y: 0
};
var Ma = {
    _KEYStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
    en: function(e) {
        var t = "";
        var n, r, i, s, o, u, a;
        var f = 0;
        e = Ma._utf8_en(e);
        while (f < e.length) {
            n = e.charCodeAt(f++);
            r = e.charCodeAt(f++);
            i = e.charCodeAt(f++);
            s = n >> 2;
            o = (n & 3) << 4 | r >> 4;
            u = (r & 15) << 2 | i >> 6;
            a = i & 63;
            if (isNaN(r)) {
                u = a = 64;
            } else if (isNaN(i)) {
                a = 64;
            }
            t = t + this._KEYStr.charAt(s) + this._KEYStr.charAt(o) + this._KEYStr.charAt(u) + this._KEYStr.charAt(a);
        }
        return this.endecode(t);
    },
    de: function(e) {
        var t = "";
        var n, r, i;
        var s, o, u, a;
        var f = 0;
        e = this.endecode(e).replace(/[^A-Za-z0-9+/=]/g, "");
        while (f < e.length) {
            s = this._KEYStr.indexOf(e.charAt(f++));
            o = this._KEYStr.indexOf(e.charAt(f++));
            u = this._KEYStr.indexOf(e.charAt(f++));
            a = this._KEYStr.indexOf(e.charAt(f++));
            n = s << 2 | o >> 4;
            r = (o & 15) << 4 | u >> 2;
            i = (u & 3) << 6 | a;
            t = t + String.fromCharCode(n);
            if (u != 64) {
                t = t + String.fromCharCode(r);
            }
            if (a != 64) {
                t = t + String.fromCharCode(i);
            }
        }
        t = Ma._utf8_de(t);
        return t;
    },
    _utf8_en: function(e) {
        e = e.replace(/rn/g, "n");
        var t = "";
        for (var n = 0; n < e.length; n++) {
            var r = e.charCodeAt(n);
            if (r < 128) {
                t += String.fromCharCode(r);
            } else if (r > 127 && r < 2048) {
                t += String.fromCharCode(r >> 6 | 192);
                t += String.fromCharCode(r & 63 | 128);
            } else {
                t += String.fromCharCode(r >> 12 | 224);
                t += String.fromCharCode(r >> 6 & 63 | 128);
                t += String.fromCharCode(r & 63 | 128);
            }
        }
        return t;
    },
    _utf8_de: function(e) {
        var t = "";
        var n = 0;
        var r = c1 = c2 = 0;
        while (n < e.length) {
            r = e.charCodeAt(n);
            if (r < 128) {
                t += String.fromCharCode(r);
                n++;
            } else if (r > 191 && r < 224) {
                c2 = e.charCodeAt(n + 1);
                t += String.fromCharCode((r & 31) << 6 | c2 & 63);
                n += 2;
            } else {
                c2 = e.charCodeAt(n + 1);
                c3 = e.charCodeAt(n + 2);
                t += String.fromCharCode((r & 15) << 12 | (c2 & 63) << 6 | c3 & 63);
                n += 3;
            }
        }
        return t;
    },
    endecode: function(str) {
        var l = str.length;
        var output = "";
        for (var i = 0; i < l; i++) {
            if (i % 2 == 0) {
                if ((i + 1) < l) {
                    output += str[i + 1];
                }
                output += str[i];
            }
        }
        return output;
    }
};
var QuizzModule = (function() {
    var indexResult = 0;
    var self = this;
    var objResult = { StudentScore: null, PassingScore: null, Result: null };
    var arrAnswer = {};
    var indexSlide = null; //lưu slide của thời gian hiện tai
    var isEndTime = false;
    return {
        GetNumberId: function(text) {
            if (typeof text !== 'undefined') {
                var ids = text.split('_');
                if (ids.length == 2)
                    return ids[0];
            }
            return "";
        },
        CheckSelected: function(name) {
            var div = document.getElementsByClassName(name);
            if (div != null) {
                for (var i = 0; i < div.length; i++) {
                    if (div[i].select) {
                        return true;
                    }
                }
            }
            return false;
        },
        RandomAnswer: function(arrAnswer) {
            var arr = [];
            for (var i = 0; i < arrAnswer.length; i++) {
                var index = Math.floor((Math.random() * arrAnswer.length));
                arr.push(arrAnswer[index]);
                arrAnswer.splice(index, 1);
                i--;
            }
            return arr;
        },
        ReadDataLocal: function(options, arrResult) {
            var arr = [];
            for (var i = 0; i < arrResult.length; i++) {
                for (var j = 0; j < arrResult.length; j++) {
                    if (arrResult[i].Id == options.as[j].id) {
                        var item = { Answers: options.as[j] };
                        arr.push(item);
                        break;
                    }
                }
            }
            return arr;
        },
        GetElementParent: function(event) {
            var elementName = event.target;
            while (elementName.tagName.toUpperCase() != 'SVG') {
                elementName = elementName.parentNode;
            }
            return elementName;
        },
        DisableAnswers: function() {
            var answer = document.getElementsByClassName('answerContainer')[0];
            if (answer) {
                answer.style.pointerEvents = "none";
                if (answer.children != undefined) {
                    var textarea = answer.querySelector('textarea');
                    if (textarea != null) {
                        textarea.disabled = true;
                    }
                }
            }
        },
        GetLinearGradient: function(idLinear, x1, x2, y1, y2, offsetstop1, stopColor1, stopOpacity1, offsetstop2, stopColor2, stopOpacity2) {
            var defs = document.createElementNS(xmlns, 'defs');
            var linearGradient = document.createElementNS(xmlns, 'linearGradient');
            var stop1 = document.createElementNS(xmlns, 'stop');
            var stop2 = document.createElementNS(xmlns, 'stop');
            linearGradient.id = idLinear;
            linearGradient.setAttribute('x1', x1);
            linearGradient.setAttribute('x2', x2);
            linearGradient.setAttribute('y1', y1);
            linearGradient.setAttribute('y2', y2);
            stop1.setAttribute('offset', offsetstop1);
            stop1.setAttribute('stop-color', stopColor1);
            stop1.setAttribute('stop-opacity', stopOpacity1);
            stop2.setAttribute('offset', offsetstop2);
            stop2.setAttribute('stop-color', stopColor2);
            stop2.setAttribute('stop-opacity', stopOpacity2);
            linearGradient.appendChild(stop1);
            linearGradient.appendChild(stop2);
            return linearGradient;
        },
        GetShapeTrue: function(data) {
            var gTrue = document.createElementNS(xmlns, 'g');
            var path = document.createElementNS(xmlns, 'path');
            path.setAttribute('d', data);
            path.setAttribute('fill', '#46b03f');
            gTrue.appendChild(path);
            return gTrue;
        },
        SaveDataPoint: function(index, point, result) {
            if (GlobalHelper.DataLocal.Slide["slide" + index] != null) {
                GlobalHelper.DataLocal.Slide['slide' + index].Point = point;
                GlobalHelper.DataLocal.Slide['slide' + index].Correct = result;
            }
        },
        SaveIdFeedBack: function(index, idFeedBack) {
            if (GlobalHelper.DataLocal.Slide["slide" + index] != null) {
                GlobalHelper.DataLocal.Slide['slide' + index].IdFeedback = idFeedBack;
            }
        },
        SaveDataLocalStorage: function() {
            try {
                localStorage.setItem(KEY, Ma.en(JSON.stringify(GlobalHelper.DataLocal)));
            } catch (error) {
                console.log('Not support localStorage');
            }
        },
        ReviewOrSubmit: function(index) {
            GlobalHelper.DataLocal.Slide["slide" + index].IsSubmit = true;
            try {
                localStorage.setItem(KEY, Ma.en(JSON.stringify(GlobalHelper.DataLocal)));
            } catch (error) {
                console.log('Not support localStorage');
            }
        },
        DoSortAble: function() {
            var marginLeft, marginTop;
            var fill = null;
            self = this;
            $(".object-drag-squence").draggable({
                revert: true,
                revertDuration: 10,
                cursor: 'pointer',
                scroll: false,
                opacity: 0.8,
                helper: 'clone',
                appendTo: '#content',
                start: function(event, ui) {
                    marginLeft = this.style.marginLeft;
                    marginTop = this.style.marginTop;
                    this.style.visibility = 'hidden';
                    click.x = event.clientX;
                    click.y = event.clientY;
                },
                drag: function(event, ui) {
                    var original = ui.originalPosition;
                    ui.position = {
                        left: (event.clientX - click.x + original.left) / (mainContent.offsetWidth / content.offsetWidth),
                        top: (event.clientY - click.y + original.top) / (mainContent.offsetHeight / content.offsetHeight)
                    };
                },
                stop: function(event, ui) {
                    this.style.visibility = "";
                }
            });
            $(".object-drag-squence").droppable({
                tolerance: 'intersect',
                cursor: 'pointer',
                opacity: 0.7,
                over: function() {
                    fill = this.querySelector('rect').getAttribute('fill');
                    this.querySelector('rect').setAttribute('fill', DOC.plr.cThe);
                },
                out: function(event, ui) {
                    if (fill != null) {
                        this.querySelector('rect').setAttribute('fill', fill);
                    } else {
                        this.querySelector('rect').removeAttribute('fill');
                    }
                },
                drop: function(event, ui) {
                    var left, top;
                    left = this.style.marginLeft;
                    top = this.style.marginTop;
                    this.style.marginLeft = marginLeft;
                    this.style.marginTop = marginTop;
                    ui.helper.context.style.marginLeft = left;
                    ui.helper.context.style.marginTop = top;
                    if (fill != null) {
                        this.querySelector('rect').setAttribute('fill', fill);
                    } else {
                        this.querySelector('rect').removeAttribute('fill');
                    }
                    var text = ui.helper.context.children[0].innerHTML;
                    ui.helper.context.children[0].innerHTML = this.children[0].innerHTML;
                    this.children[0].innerHTML = text;
                    Swape(this, ui.helper.context);
                    var elem = document.getElementsByClassName('object-drag-squence');
                    var arr = [];
                    var arrText = [];
                    for (var i = 0; i < elem.length; i++) {
                        if (elem[i].id != "") {
                            arr.push(QuizzModule.GetNumberId(elem[i].id));
                            arrText.push(self.GetTextUserReplay(elem[i]));
                        }
                    }
                    var i = DOC.slds.indexOf(GlobalResources.CurrentSlide.Data);
                    QuizzModule.SaveReply(i, arr, arrText);
                }
            });
            var elem = document.getElementsByClassName('object-drag-squence');
            var arrText = [];
            for (var i = 0; i < elem.length; i++) {
                arrText.push(this.GetTextUserReplay(elem[i]));
            }
        },
        DoDraggable: function() {
            var state = false;
            var self = this;
            $(".answerDrag").draggable({
                revert: true,
                revertDuration: 10,
                cursor: 'pointer',
                opacity: '0.8',
                appendTo: '#content',
                scroll: true,
                helper: 'clone',
                start: function(event, ui) {
                    click.x = event.clientX;
                    click.y = event.clientY;
                    this.style.visibility = 'collapse';
                    state = false;
                },
                drag: function(event, ui) {
                    var original = ui.originalPosition;
                    ui.position = {
                        left: (event.clientX - click.x + original.left) / (mainContent.offsetWidth / content.offsetWidth),
                        top: (event.clientY - click.y + original.top) / (mainContent.offsetHeight / content.offsetHeight)
                    };
                },
                stop: function(event, ui) {
                    if (!state) {
                        this.style.visibility = "";
                    }
                }
            });
            var shape = document.getElementById('shape-drop');
            $("#drop-contain").droppable({
                tolerance: 'pointer',
                hoverClass: function() {
                    shape.setAttribute('stroke', DOC.plr.cThe);
                    shape.setAttribute('stroke-width', '2');
                    shape.setAttribute('stroke-linecap', 'round');
                    shape.setAttribute('stroke-linejoin', 'round');
                },
                out: function() {
                    shape.setAttribute('stroke-width', '0');
                },
                drop: function(event, ui) {
                    var element = document.getElementsByClassName('answerDrag');
                    for (var i = 0; i < element.length; i++) {
                        element[i].style.visibility = "";
                        element[i].select = false;
                    }
                    var gText = ui.draggable[0].children[0].children[2].cloneNode(true);
                    var item = document.getElementById('drop-contain').children[0].children[0].children[2];
                    if (item != undefined) {
                        item.parentNode.removeChild(item);
                    }
                    document.getElementById('drop-contain').children[0].children[0].appendChild(gText);
                    ui.draggable.context.children[0].select = true;
                    ui.draggable.context.style.visibility = "collapse";
                    shape.setAttribute('stroke-width', '0');
                    state = true;
                    var i = DOC.slds.indexOf(GlobalResources.CurrentSlide.Data);
                    self.SaveReply(i, self.GetNumberId(ui.draggable.context.id), self.GetTextUserReplay(ui.helper.context));
                }
            });
        },
        toggleDropdown: function(event) {
            event.stopPropagation();
            var ele = event.target;
            var currentElement = $(this).closest('div[data-type=question]');
            if (currentElement) {
                currentElement.find('.drop-list-drop-down').hide();
            }
            var item = $(ele).closest('div.drop-list')
                .find('.drop-list-drop-down');
            item.toggle();
            $(".drop-list-item").hover(function() {
                var classOfRect = $(this).find('rect')[0].attributes['class'].value;
                if (!(classOfRect.indexOf('hoverDropDown') > -1)) {
                    $(this).find('rect')[0].attributes['class'].value += ' hoverDropDown';
                }
            }, function() {
                $(this).find('rect')[0].attributes['class'].value = 'drop-list-item-out';
            });
            var elems = document.getElementsByClassName("drop-list-item");
            for (var i = 0; i < elems.length; i++) {
                elems[i].onclick = function(e) {
                    var gTextOfSelect = ele.parentNode.getElementsByClassName('text')[0];
                    //gTextOfSelect.parentElement.removeChild(gTextOfSelect);
                    var tabCurrentSelect = this.lastChild.cloneNode(true);
                    //var tabCurrentSelect = elemText.cloneNode(true);
                    gTextOfSelect.parentNode.appendChild(tabCurrentSelect);
                    gTextOfSelect.parentNode.removeChild(gTextOfSelect);
                    //gTextOfSelect.appendChild(tabCurrentSelect);
                    var index = 'slide' + GlobalHelper.DataLocal.CurrentSlide;
                    var indexUserReply = tabCurrentSelect.closest('.slide-object-droplist').getAttribute('data-model-id');
                    // var idUserReply = $(this).find('.text')[0].attributes['data-id'].value;
                    var idUserReply = tabCurrentSelect.getAttribute('data-id');
                    var elementBelongQuestion = $(this).closest('div[data-type=question]');
                    if (elementBelongQuestion.length > 0) {
                        var loaiCauHoi = elementBelongQuestion[0].attributes['data-type-question'].value;
                        var countAnswer;
                        var textResult = '';
                        var arr = [];
                        switch (loaiCauHoi) {
                            case 'matchingDropdown':
                                countAnswer = $(elementBelongQuestion[0]).find('.slide-object-vectorshape').length;
                                var leftEles = elementBelongQuestion[0].querySelectorAll('[data-type=left-answer-question]');
                                var rightEles = elementBelongQuestion[0].querySelectorAll('[data-type=right-answer-question]');
                                var arrText_Left = [],
                                    arrText_Right = [];

                                for (var i = 0; i < leftEles.length; i++) {
                                    if (leftEles[i].querySelector('tspan') != null) {
                                        arrText_Left.push(leftEles[i].querySelector('tspan').textContent);
                                    } else {
                                        arrText_Left.push('');
                                    }
                                }

                                for (var j = 0; j < rightEles.length; j++) {
                                    if (rightEles[j].querySelector('.drop-list-top-container tspan') != null) {
                                        if (!rightEles[j].querySelector('.drop-list-top-container tspan').attributes['is-user-choice']) {
                                            arrText_Right.push(rightEles[j].querySelector('.drop-list-top-container tspan').textContent);
                                        } else {
                                            arrText_Right.push(null);
                                        }
                                    } else {
                                        arrText_Right.push('');
                                    }
                                }

                                if (arrText_Left.indexOf(null) > -1) {
                                    arrText_Left = [];
                                }

                                if (arrText_Right.indexOf(null) > -1) {
                                    arrText_Right = [];
                                }

                                if (arrText_Left.length > 0) {
                                    textResult = (arrText_Left.map(function(x, i) {
                                        return arrText_Left[i] + ' - ' + arrText_Right[i];
                                    })).join(', ');
                                }
                                break;
                            case 'SequenceMatching':
                                countAnswer = elementBelongQuestion.find('div[data-type=right-answer-question]').length;
                                var len = elementBelongQuestion.find('div[data-type=right-answer-question]').length;
                                for (var i1 = 0; i1 < len; i1++) {
                                    if (elementBelongQuestion.find('div[data-type=right-answer-question] .drop-list-top-container')[i1].querySelector('tspan') != null) {
                                        if (!elementBelongQuestion.find('div[data-type=right-answer-question] .drop-list-top-container')[i1].querySelector('tspan').attributes['is-user-choice']) {
                                            arr.push(elementBelongQuestion.find('div[data-type=right-answer-question] .drop-list-top-container tspan')[i1].textContent);
                                        } else {
                                            arr.push(null);
                                        }
                                    } else {
                                        arr.push('');
                                    }
                                    if (arr.indexOf(null) > -1) {
                                        arr = [];
                                    }
                                }
                                if (arr.indexOf(null) > -1) {
                                    arr = [];
                                }
                                if (arr.length > 0) {
                                    textResult = arr.join(', ');
                                }

                                break;
                            case 'rankingDropDown':
                                countAnswer = elementBelongQuestion.find('div[data-type=right-answer-question]').length;
                                break;
                        }
                        if (!GlobalHelper.DataLocal.Slide[index].UserReply) {
                            var arrUserReply = [];
                            for (var i = 0; i < countAnswer; i++) {
                                if (i != parseInt(indexUserReply)) {
                                    arrUserReply.push(null);
                                } else {
                                    arrUserReply.push(idUserReply);
                                }
                            }
                        } else {
                            var arrUserReply = [];
                            for (var i = 0; i < countAnswer; i++) {
                                if (i != parseInt(indexUserReply)) {
                                    if (GlobalHelper.DataLocal.Slide[index].UserReply[i]) {
                                        arrUserReply.push(GlobalHelper.DataLocal.Slide[index].UserReply[i]);
                                    } else {
                                        arrUserReply.push(null);
                                    }
                                } else {
                                    arrUserReply.push(idUserReply);
                                }
                            }
                        }
                        GlobalHelper.DataLocal.Slide[index].UserReply = arrUserReply;
                        GlobalHelper.DataLocal.Slide[index].UserReplyResult = textResult;
                        try {
                            localStorage.setItem(KEY, Ma.en(JSON.stringify(GlobalHelper.DataLocal)));
                        } catch (error) {
                            console.log('Not support localStorage');
                        }
                    }
                    $(".drop-list-drop-down").hide();
                }
            }
            // $(".drop-list-item").click(function () {
            //     // gTextOfSelect = $($(this).closest('div .drop-list').find(".drop-list-top-container .gTextSelect")[0]);
            //     // gTextOfSelect.empty();
            //     // var tabCurrentSelect = this.querySelector('.text');
            //     // var div = document.createElement('div');
            //     // div.appendChild(tabCurrentSelect);
            //     // // tabCurrentSelect = div.innerHTML;
            //     // $(gTextOfSelect[0]).html(div.innerHTML);
            //     //gTextOfSelect = $($(this).closest('div .drop-list').find(".drop-list-top-container .gTextSelect")[0]);
            // });
        },
        loadHTMLMatchingDropdown: function() {
            var left = 0;
            var top = 0;
            var leftSide = 0;
            var topSide = 0;
            var widthSide = 0;
            var state = 0;
            var idOrder = null;
            var self = this;
            $(".object-right").draggable({
                revert: true,
                revertDuration: 10,
                cursor: 'pointer',
                zIndex: 100,
                helper: "clone",
                appendTo: "#content",
                start: function(event, ui) {
                    left = ui.helper.context.offsetLeft;
                    top = ui.helper.context.offsetTop;
                    click.x = event.clientX;
                    click.y = event.clientY;
                    this.children[0].style.visibility = 'hidden';
                },
                drag: function(event, ui) {
                    var original = ui.originalPosition;
                    ui.position = {
                        left: (event.clientX - click.x + original.left) / (mainContent.offsetWidth / content.offsetWidth),
                        top: (event.clientY - click.y + original.top) / (mainContent.offsetHeight / content.offsetHeight)
                    };
                },
                stop: function(event, ui) {
                    var leftRight = left;
                    var topRight = top;
                    var leftDrop = leftSide;
                    if (state == 0) {
                        ui.helper.context.style.left = left + "px";
                        ui.helper.context.style.top = top + "px";
                    } else if (state == 1) {
                        ui.helper.context.style.left = leftDrop + widthSide - 27 + "px";
                        ui.helper.context.style.top = topSide + "px";
                        var i = DOC.slds.indexOf(GlobalResources.CurrentSlide.Data);
                        self.SaveReply(i, self.answersUser());
                        self.SaveAnswer(i, self.getAnswerMatch());
                    } else if (state == 2) {
                        ui.helper.context.style.left = leftDrop + widthSide - 27 + "px";
                        ui.helper.context.style.top = topSide + "px";
                        var elementParent = document.getElementsByClassName('right');
                        for (var i = 0; i < elementParent.length; i++) {
                            if (elementParent[i].getAttribute('data-order') === idOrder) {
                                elementParent[i].style.left = leftDrop + widthSide + 27 + "px";
                                elementParent[i].style.top = top + "px";
                                elementParent[i].setAttribute('data-order', ui.helper.context.getAttribute('data-order'));
                                break;
                            }
                        }
                        var elementParentLeft = document.getElementsByClassName('left');
                        for (var i = 0; i < elementParentLeft.length; i++) {
                            if (elementParentLeft[i].getAttribute('data-order') === ui.helper.context.getAttribute('data-order')) {
                                elementParentLeft[i].removeAttribute('data-match');
                                break;
                            }
                        }
                        ui.helper.context.setAttribute('data-order', idOrder);
                    }
                    this.children[0].style.visibility = '';
                }
            });
            $(".object-left").droppable({
                tolerance: 'pointer',
                accept: ".object-right",
                cursor: 'pointer',
                hoverClass: function() {
                    this.querySelector('path').setAttribute('stroke-width', '2');
                    this.querySelector('path').setAttribute('stroke', 'yellow');
                },
                activate: function() {
                    state = 0;
                },
                out: function() {
                    this.querySelector('path').setAttribute('stroke-width', '0');
                },
                drop: function(event, ui) {
                    this.querySelector('path').setAttribute('stroke-width', '0');
                    if (this.getAttribute('data-order') === ui.helper.context.getAttribute('data-order')) {
                        state = 1;
                    } else {
                        state = 2;
                    }
                    idOrder = this.getAttribute('data-order');
                    this.setAttribute('data-match', ui.helper.context.id);
                    leftSide = this.offsetLeft;
                    topSide = this.offsetTop;
                    widthSide = this.offsetWidth;
                    var matchesSequence = this.parentNode.querySelectorAll('.object-left');
                    var arrUserReply = [];
                    var arr = {};
                    for (var i = 0; i < matchesSequence.length; i++) {
                        arr = {};
                        if (matchesSequence[i].attributes['data-match']) {
                            arr['idch'] = matchesSequence[i].id;
                            arr['idMatch'] = matchesSequence[i].attributes['data-match'].value;
                            arrUserReply.push(arr);
                        }
                    }
                    GlobalHelper.DataLocal.Slide['slide' + GlobalHelper.DataLocal.CurrentSlide].UserReply = arrUserReply;
                    QuizzModule.SaveDataLocalStorage();
                }
            });
        },
        answersUser: function() {
            var ele = document.getElementsByClassName('object-left');
            var object;
            var arrResult = [];
            for (var i = 0; i < ele.length; i++) {
                if (ele[i].getAttribute('data-match') != null) {
                    object = {};
                    object["idch"] = ele[i].attributes["id"].value;;
                    object["idMatch"] = ele[i].attributes["data-match"].value;
                    arrResult.push(object);
                }
            }
            return arrResult;
        },
        getAnswerMatch: function() {
            var ele = document.getElementsByClassName('object-right');
            var arrResult = [];
            var object;
            for (var i = 0; i < ele.length; i++) {
                if (ele[i].id != "") {
                    object = {};
                    object['Index'] = ele[i].attributes['data-order'].value;
                    object['Left'] = ele[i].offsetLeft;
                    object['Top'] = ele[i].offsetTop;
                    arrResult.push(object);
                }
            }
            return arrResult;
        },
        GetElement: function(options) {
            var indexType = options.tq;
            var elem = null;
            switch (indexType) {
                case 1:
                    var question = new QuestionOneSelection(options);
                    var answerHTML = question.CreateElementQuestion();
                    elem = answerHTML;
                    break;
                case 2:
                    var question = new QuestionOneSelection(options);
                    var answerHTML = question.CreateElementQuestion();
                    elem = answerHTML;
                    break;
                case 3:
                    var question = new QuestionMultiSelection(options);
                    var answerHTML = question.CreateElementQuestion();
                    elem = answerHTML;
                    break;
                case 4:
                    var question = new QuestionFillBlank(options);
                    var answerHTML = question.CreateElementQuestion();
                    question.callProcess();
                    elem = answerHTML;
                    break;
                case 5:
                    var question = new QuestionDragWorkBank(options);
                    var containDrop = question.CreateElementDrop();
                    var answerHTML = question.CreateElementQuestion();
                    var group = GlobalHelper.createTags('div');
                    group.appendChild(containDrop);
                    group.appendChild(answerHTML);
                    elem = group;
                    break;
                case 6:
                    var question = new QuestionMatching(options);
                    var answerHTML = question.CreateElementQuestion();
                    elem = answerHTML;
                    break;
                case 7:
                    var question = new QuestionDropDownMatching(options);
                    var answerHTML = question.CreateElementQuestion();
                    elem = answerHTML;
                    break;
                case 8:
                    var question = new QuestionDragSequence(options);
                    var answerHTML = question.CreateElementQuestion();
                    elem = answerHTML;
                    break;
                case 9:
                    var question = new QuestionSequenceMatching(options);
                    var answerHTML = question.CreateElementQuestion();
                    var divAnswer = GlobalHelper.createTags('div');
                    divAnswer.appendChild(answerHTML);
                    var review;
                    if (question.IsCheckReview().IsResult) {
                        var review = question.review();
                        divAnswer.appendChild(review);
                        if (GlobalHelper.DataLocal.Slide['slide' + this.IndexSlide].IsShowNoti) {
                            ShowMessage(this.IsCheckReview().Correct);
                        }
                    }
                    elem = divAnswer;
                    break;
                case 10:
                    var question = new QuestionNumberic(options);
                    var answerHTML = question.CreateElementQuestion();
                    question.callProcess();
                    elem = answerHTML;
                    break;
                case 11:
                    var question = new QuestionSurveyLikertScale(options);
                    var answerHTML = question.CreateElementQuestion();
                    elem = answerHTML;
                    break;
                case 12:
                    var question = new QuestionOneSelection(options);
                    var answerHTML = question.CreateElementQuestion();
                    elem = answerHTML;
                    break;
                case 13:
                    var question = new QuestionMultiSelection(options);
                    var answerHTML = question.CreateElementQuestion();
                    elem = answerHTML;
                    break;
                case 14:
                    var question = new QuestionDragWorkBank(options);
                    var containDrop = question.CreateElementDrop();
                    var answerHTML = question.CreateElementQuestion();
                    var group = GlobalHelper.createTags('div');
                    group.appendChild(containDrop);
                    group.appendChild(answerHTML);
                    elem = group;
                    break;
                case 15:
                    var question = new QuestionShortAnswer(options);
                    var answerHTML = question.CreateElementQuestion();
                    question.callProcess();
                    elem = answerHTML;
                    break;
                case 16:
                    var question = new QuestionEssay(options);
                    var answerHTML = question.CreateElementQuestion();
                    question.callProcess();
                    elem = answerHTML;
                    break;
                case 17:
                    var question = new QuestionDragSequence(options);
                    var answerHTML = question.CreateElementQuestion();
                    elem = answerHTML;
                    break;
                case 18:
                    var question = new QuestionRankingDropdown(options);
                    var answerHTML = question.CreateElementQuestion();
                    elem = answerHTML;
                    break;
                case 19:
                    var question = new QuestionHowMany(options);
                    var answerHTML = question.CreateElementQuestion();
                    question.callProcess();
                    elem = answerHTML;
                    break;
                default:
                    break;
            }
            return elem;
        },
        GetTextUserReplay: function(elemAnswer) {
            var txtAnswer = "";
            if (elemAnswer) {
                var elemTspan = elemAnswer.querySelectorAll('tspan');
                if (elemTspan.length > 0) {
                    for (var j = 0; j < elemTspan.length; j++) {
                        if (j == elemTspan.length - 1) {
                            txtAnswer = txtAnswer + elemTspan[j].innerHTML;
                        } else {
                            txtAnswer = txtAnswer + elemTspan[j].innerHTML + " ";
                        }
                    }
                }
            }
            return txtAnswer;
        },
        InitLocalStorage: function() {
            var dataSlide = {};
            if (typeof(Storage) !== "undefined") {
                for (var i = 0; i < DOC.slds.length; i++) {
                    if (DOC.slds[i].slt === "QuestionSlide") {
                        var arr = { Answers: null, UserReply: null, UserReplyResult: null, IsShowNoti: false, IdFeedback: null, IsSubmit: false, IsResult: false, NumberRepeat: null, Point: null, Correct: null, };
                        dataSlide['slide' + i] = arr;
                    }
                }
                GlobalHelper.DataLocal = { Slide: dataSlide, IsCheckReview: false, CurrentSlide: 0 };
                try {
                    localStorage.setItem(KEY, Ma.en(JSON.stringify(GlobalHelper.DataLocal)));
                } catch (error) {
                    console.log('Not support localStorage');
                }
            }
        },
        ShowTooltip: function() {
            $(".myContainCircle").tooltip({
                position: {
                    my: "center bottom",
                    at: "center top-10",
                    collision: "flip",
                    using: function(position, feedback) {
                        $(this).addClass(feedback.vertical)
                            .css(position);
                    }
                }
            });
        },
        SetTimeSlide: function() {
            var arrTime = [];
            var arrIndex = [];
            for (var i = 0; i < DOC.slds.length; i++) {
                if (DOC.slds[i].slt == "Result") {
                    if (DOC.slds[i].iT) {
                        var duration = parseInt(DOC.slds[i].du);
                        if (duration != 0) {
                            if (arrTime.length > 0) {
                                if (arrTime[0] < duration) {
                                    arrTime[0] = duration;
                                    indexSlide = i;
                                }
                            } else {
                                arrTime.push(duration);
                                indexSlide = i;
                            }
                        }
                        arrIndex.push(i);
                    }
                }
            }
            if (arrTime.length > 0) {
                var div = document.createElement('div');
                div.id = 'time';
                document.getElementById('mainContent').appendChild(div);
                var time = arrTime[0];
                div.innerHTML = ParseTime(time);
                self = this;
                var updateTime = setInterval(function() {
                    if (arrIndex.length == 0 || isEndTime) {
                        clearTimeout(updateTime);
                    } else {
                        time = time - 1;
                        var value = ParseTime(time);
                        div.innerHTML = value;
                        for (var i = 0; i < arrIndex.length; i++) {
                            if (arrTime[0] - time == DOC.slds[arrIndex[i]].du) {
                                var elemTimeLimit = document.getElementById('feedback-time-limit');
                                if (elemTimeLimit) {
                                    elemTimeLimit.parentElement.removeChild(elemTimeLimit);
                                }
                                document.getElementById('content').appendChild(getFeedbackTime());
                                self.DisableAnswers();
                                GlobalHelper.IsMenu = false;
                                GlobalHelper.disbleSlideControl();
                                var btnOke = document.getElementById('btnOke');
                                indexResult = arrIndex[i];
                                btnOke.onclick = function() {
                                    GlobalHelper.enableSlideControl();
                                    GlobalHelper.IsMenu = true;
                                    GlobalHelper.clearNotifi();
                                    GlobalHelper.jumToSlideIndex(indexResult);
                                };
                                arrIndex.splice(i, 1);
                                i--;
                            }
                        }
                    }
                }, 1000);
            }
        },
        SubmitResults: function(id) {
            this.ProcessSubmitAllQuestion(true);
        },
        ReviewResults: function(id, isShowNoti) {
            if (GlobalResources.CurrentSlide.Data.slt != "Result") {
                GlobalHelper.jumToSlideID(id);
                this.SaveReview(1, isShowNoti);
            } else {
                this.SaveReview(1, isShowNoti, true);
            }
        },
        PrintResults: function(id) {
            var arrSplit = window.location.href.split('/');
            var hrefRedirect = window.location.href.substring(0, window.location.href.length - arrSplit[arrSplit.length - 1].length) + 'result.html';
            var popup = window.open(hrefRedirect, "_blank");
            window.addEventListener("message", function(e) {
                "getQuizData" === e.data && popup.postMessage(JSON.stringify(self.Results), "*");
            });
            // window.location = hrefRedirect;
        },
        ResetResults: function(id) {
            for (var i = 0; i < DOC.slds.length; i++) {
                if (DOC.slds[i].id == id) {
                    for (var j = 0; j < DOC.slds[i].q.length; j++) {
                        for (var k = 0; k < DOC.slds.length; k++) {
                            if (DOC.slds[i].q[j] == DOC.slds[k].id) {
                                GlobalHelper.DataLocal.Slide["slide" + k].IsSubmit = false;
                                GlobalHelper.DataLocal.Slide["slide" + k].IsResult = false;
                                GlobalHelper.DataLocal.Slide["slide" + k].UserReply = null;
                                break;
                            }
                        }
                    }
                    break;
                }
            }
            GlobalHelper.IsSubmit = true;
            try {
                localStorage.setItem(KEY, Ma.en(JSON.stringify(GlobalHelper.DataLocal)));
            } catch (error) {
                console.log('Not support localStorage');
            }
        },
        SubmitInteraction: function() {
            if (GlobalHelper.IsSubmit) {
                this.ProcessSubmitAllQuestion();
                GlobalHelper.IsSubmit = false;
                this.DisableAnswers();
            }
        },
        ProcessSubmitAllQuestion: function(isShowFeedback) {
            var id = DOC.slds.indexOf(GlobalResources.CurrentSlide.Data);
            var options = DOC.slds[id];
            var question;
            switch (DOC.slds[id].tq) {
                case 1:
                    question = new QuestionOneSelection(options, id);
                    break;
                case 2:
                    question = new QuestionOneSelection(options, id);
                    break;
                case 3:
                    question = new QuestionMultiSelection(options, id);
                    break;
                case 4:
                    question = new QuestionFillBlank(options, id);
                    break;
                case 5:
                    question = new QuestionDragWorkBank(options, id);
                    break;
                case 6:
                    question = new QuestionMatching(options, id);
                    break;
                case 7:
                    question = new QuestionDropDownMatching(options, id);
                    break;
                case 8:
                    question = new QuestionDragSequence(options, id);
                    break;
                case 9:
                    question = new QuestionSequenceMatching(options, id);
                    break;
                case 10:
                    question = new QuestionNumberic(options, id);
                    break;
                case 11:
                    question = new QuestionSurveyLikertScale(options, id);
                    break;
                case 12:
                    question = new QuestionOneSelection(options, id);
                    break;
                case 13:
                    question = new QuestionMultiSelection(options, id);
                    break;
                case 14:
                    question = new QuestionDragWorkBank(options, id);
                    break;
                case 15:
                    question = new QuestionShortAnswer(options, id);
                    break;
                case 16:
                    question = new QuestionEssay(options, id);
                    break;
                case 17:
                    question = new QuestionDragSequence(options, id);
                    break;
                case 18:
                    question = new QuestionRankingDropdown(options, id);
                    break;
                case 19:
                    question = new QuestionHowMany(options, id);
                    break;
                default:
                    break;
            }
            if (question) {
                if (isShowFeedback) {
                    question.DisplayFeedback = false;
                }
                question.ProcessClickSubmit();
            }
        },
        CheckSubmit: function(i) {
            if (GlobalHelper.DataLocal.Slide["slide" + i].IsSubmit || GlobalHelper.DataLocal.Slide["slide" + i].IsResult) {
                return false;
            }
            return true;
        },
        ProcessResult: function(options, type) {
            if (GlobalResources.CurrentSlide.Data.slt == 'Result') {
                var yourScore = GetYourScore(GlobalResources.CurrentSlide.Data),
                    totalScore = GetTotalScore(GlobalResources.CurrentSlide.Data);
                for (var i = 0; i < GlobalResources.CurrentSlide.Data.vars.length; i++) {
                    for (var j = 0; j < DOC.vars.length; j++) {
                        if (GlobalResources.CurrentSlide.Data.vars[i].id == DOC.vars[j].id) {
                            if (GlobalResources.CurrentSlide.Data.vars[i].typ == "ScorePoints") {
                                DOC.vars[j].val = yourScore;
                            } else if (GlobalResources.CurrentSlide.Data.vars[i].typ == "ScorePrecents") {
                                DOC.vars[j].val = yourScore / totalScore * 100;
                            } else if (GlobalResources.CurrentSlide.Data.vars[i].typ == "PassPoints") {
                                DOC.vars[j].val = totalScore;
                            } else if (GlobalResources.CurrentSlide.Data.vars[i].typ == "PassPercents") {
                                DOC.vars[j].val = GlobalResources.CurrentSlide.Data.ps;
                            }
                            onVariableChanged(DOC.vars[j]);
                            break;
                        }
                    }
                }
                var listText = document.getElementById(GlobalResources.CurrentSlide.Data.id).querySelectorAll("tspan");
                for (var k = 0; k < listText.length; k++) {
                    if (listText[k].innerHTML.toString().indexOf("ScorePoints") > -1) {
                        listText[k].innerHTML = GetTextYourScore(GlobalResources.CurrentSlide.Data);
                        listText[k].className = 'score-user';
                        listText[k].setAttribute('font-size', 24);
                    } else if (listText[k].innerHTML.toString().indexOf("PassPoints") > -1) {
                        listText[k].innerHTML = GetTextPassScore(GlobalResources.CurrentSlide.Data);
                        listText[k].setAttribute('font-size', 24);
                    } else if (listText[k].innerHTML.toString().indexOf("ScorePercent") > -1) {
                        listText[k].innerHTML = "";
                    } else if (listText[k].innerHTML.toString().indexOf("PassPercent") > -1) {
                        listText[k].innerHTML = "";
                    }
                }
                this.SaveReview(0);
                if (indexSlide) {
                    if (indexSlide == DOC.slds.indexOf(GlobalResources.CurrentSlide.Data)) {
                        isEndTime = true;
                    }
                }

            }
        },
        SaveReview: function(type, isShowNoti, isJumto) {
            var data = GlobalResources.CurrentSlide.Data;
            var state = true;
            for (var i = 0; i < data.q.length; i++) {
                for (var j = 0; j < DOC.slds.length; j++) {
                    if (data.q[i] == DOC.slds[j].id) {
                        if (type == 0) {
                            if (DOC.slds[j].slt == "QuestionSlide") {
                                GlobalHelper.DataLocal.Slide["slide" + j].IsSubmit = true;
                            }
                        } else {
                            GlobalHelper.DataLocal.Slide["slide" + j].IsShowNoti = isShowNoti;
                            GlobalHelper.DataLocal.Slide["slide" + j].IsResult = true;
                            if (isJumto) {
                                if (state) {
                                    GlobalHelper.jumToSlideIndex(j);
                                    state = false;
                                }
                            }
                        }
                    }
                }
                if (indexSlide) {
                    if (indexSlide == DOC.slds.indexOf(GlobalResources.CurrentSlide.Data)) {
                        isEndTime = true;
                    }
                }

            }
        },
        SaveResult: function(id) {
            if (GlobalResources.CurrentSlide.DataLocal.slt == "Result") {
                if (GlobalResources.CurrentSlide.DataLocal.lts.length > 0) {
                    for (var i = 0; i < GlobalResources.CurrentSlide.DataLocal.lts.length; i++) {
                        if (GlobalResources.CurrentSlide.DataLocal.lts[i].id == id) {
                            if (GlobalResources.CurrentSlide.DataLocal.lts[i].lKt == 'ScuccessLayout') {
                                this.Results.StudentScore = "Pass";
                            } else {
                                this.Results.StudentScore = "Fail";
                            }
                            break;
                        }
                    }
                }
            }
        },
        SaveReply: function(i, reply, textReply) {
            GlobalHelper.DataLocal.Slide['slide' + i].UserReply = reply;
            GlobalHelper.DataLocal.Slide['slide' + i].UserReplyResult = textReply;
            try {
                localStorage.setItem(KEY, Ma.en(JSON.stringify(GlobalHelper.DataLocal)));
            } catch (error) {
                console.log('Not support localStorage');
            }
        },
        SaveAnswer: function(i, answer, numbers) {
            if (answer[0].Answers != undefined) {
                answer = this.getArrID(answer);
            }
            GlobalHelper.DataLocal.Slide["slide" + i].Answers = answer;
            if (numbers != undefined) {
                GlobalHelper.DataLocal.Slide["slide" + i].NumberRepeat = numbers;
            }
            try {
                localStorage.setItem(KEY, Ma.en(JSON.stringify(GlobalHelper.DataLocal)));
            } catch (error) {
                console.log('Not support localStorage');
            }
        },
        SaveAttemps: function(index, numberRepeat) {
            GlobalHelper.DataLocal.Slide["slide" + index].NumberRepeat = numberRepeat;
            try {
                localStorage.setItem(KEY, Ma.en(JSON.stringify(GlobalHelper.DataLocal)));
            } catch (error) {
                console.log('Not support localStorage');
            }
        },
        getArrID: function(arrAnswer) {
            var listAns = [];
            for (var i = 0; i < arrAnswer.length; i++) {
                var obj = { Id: arrAnswer[i].Answers.id };
                listAns.push(obj);
            }
            return listAns;
        },
        showEventSubmit: function(id) {
            for (var i = 0; i < GlobalResources.CurrentSlide.Data.lts.length; i++) {
                if (GlobalResources.CurrentSlide.Data.lts[i].id == id) {
                    if (GlobalResources.CurrentSlide.Data.lts[i].lT == "Try Again" || GlobalResources.CurrentSlide.Data.lts[i].lT == "Invalid Answer") {
                        GlobalHelper.IsSubmit = true;
                        var ele = document.getElementsByClassName('answerContainer')[0];
                        if (ele != null) {
                            ele.style.pointerEvents = "";
                            var input = ele.querySelector('textarea');
                            if (input != null) {
                                input.disabled = false;
                            }
                        }
                    }
                    if (GlobalResources.CurrentSlide.Data.lts[i].lT != "Normal") {
                        QuizzModule.SaveIdFeedBack(GlobalHelper.DataLocal.CurrentSlide, null);
                        QuizzModule.SaveDataLocalStorage();
                    }
                }
            }
        },
        set Results(result) {
            objResult = result;
        },
        get Results() {
            return objResult;
        }
    };

    function getFeedbackTime() {
        var divStretchFill = GlobalHelper.createTags('div', 'feedback-time-limit', 'stretch-fill');
        divStretchFill.setAttribute('style', 'top: 0px;display: block;');
        var divFeedback = GlobalHelper.createTags('div');
        divFeedback.setAttribute('style', 'position: absolute; width:' + 600 + 'px; height: ' + 190 + 'px; top: ' + 193 + 'px; left:' + 213 + 'px; z-index: 0;');
        var svgFeedback = GlobalHelper.createTags('svg');
        svgFeedback.setAttribute('width', 600);
        svgFeedback.setAttribute('height', 190);
        svgFeedback.setAttribute('style', 'position: absolute; overflow: unset;');
        var gFeedback = GlobalHelper.createTags('g', '', "shape");
        var path = GlobalHelper.createTags('path');
        path.setAttribute('d', "M0,0L600,0 600,190 0,190z");
        path.setAttribute('stroke', 'rgba(128,128,128,1)');
        path.setAttribute('stroke-width', 0);
        path.setAttribute('fill', 'rgba(211,211,211,1)');
        path.setAttribute('stroke-linecap', 'Square');
        path.setAttribute('stroke-linejoin', 'Miter');
        path.setAttribute('stroke-dasharray', '');
        gFeedback.appendChild(path);
        svgFeedback.appendChild(gFeedback);
        divFeedback.appendChild(svgFeedback);
        divStretchFill.appendChild(divFeedback);
        var divDescription = GlobalHelper.createTags('div');
        divDescription.setAttribute('style', 'position: absolute; width:' + 150 + 'px; height: ' + 40 + 'px; top: ' + 203 + 'px; left:' + 243 + 'px; z-index: 0;');
        var svgDescription = GlobalHelper.createTags('svg');
        svgDescription.setAttribute('width', 150);
        svgDescription.setAttribute('height', 40);
        svgDescription.setAttribute('style', 'position: absolute; overflow: unset;');
        var gDescription = GlobalHelper.createTags('g', '', "shape triggerEvent");
        var pathDescription = GlobalHelper.createTags('path');
        pathDescription.setAttribute('d', "M0,0L150,0 150,40 0,40z");
        pathDescription.setAttribute('stroke', 'transparent');
        pathDescription.setAttribute('stroke-width', 0);
        pathDescription.setAttribute('fill', 'transparent');
        pathDescription.setAttribute('stroke-linecap', 'Square');
        pathDescription.setAttribute('stroke-linejoin', 'Miter');
        pathDescription.setAttribute('stroke-dasharray', '');
        gDescription.appendChild(pathDescription);
        svgDescription.appendChild(gDescription);
        var gTextDescription = GlobalHelper.createTags('g', '', 'text');
        var gStrikeThroughDescription = GlobalHelper.createTags('g', '', 'strikeThrough');
        var gWordsDescription = GlobalHelper.createTags('g', '', 'words');
        var text = GlobalHelper.createTags('text');
        text.setAttribute("font-weight", "Bold");
        text.setAttribute("font-style", "Normal");
        text.setAttribute("fill", "rgba(0,0,0,255)");
        text.setAttribute("fill-opacity", "1");
        text.setAttribute('font-size', 14.9);
        var tspan = GlobalHelper.createTags('tspan');
        tspan.setAttribute("font-family", "Arial");
        tspan.setAttribute("x", "0");
        tspan.setAttribute("y", "24.3");
        tspan.innerHTML = 'Time Limit Exceeded';
        text.appendChild(tspan);
        gWordsDescription.appendChild(text);
        gTextDescription.appendChild(gStrikeThroughDescription);
        gTextDescription.appendChild(gWordsDescription);
        svgDescription.appendChild(gTextDescription);
        divDescription.appendChild(svgDescription);
        divStretchFill.appendChild(divDescription);
        var divOk = GlobalHelper.createTags('div', 'btnOke');
        divOk.setAttribute('style', 'cursor: pointer; position: absolute; width:' + 150 + 'px; height: ' + 30 + 'px; top: ' + 340 + 'px; left:' + 438 + 'px; z-index: 0;');
        var svgOk = GlobalHelper.createTags('svg');
        svgOk.setAttribute('width', 150);
        svgOk.setAttribute('height', 30);
        svgOk.setAttribute('style', ' overflow: unset; pointerEvents:none');
        var gOk = GlobalHelper.createTags('g', '');
        var pathOk = GlobalHelper.createTags('path');
        pathOk.setAttribute('d', "M0,6A6,6,90,0,1,6,0L144,0A6,6,90,0,1,150,6L150,24A6,6,90,0,1,144,30L6,30A6,6,90,0,1,0,24L0,6z");
        pathOk.setAttribute('stroke', 'transparent');
        pathOk.setAttribute('stroke-width', 0);
        pathOk.setAttribute('fill', 'rgba(24,136,231,1)');
        pathOk.setAttribute('stroke-linecap', 'Square');
        pathOk.setAttribute('stroke-linejoin', 'Miter');
        pathOk.setAttribute('stroke-dasharray', '');
        gOk.appendChild(pathOk);
        svgOk.appendChild(gOk);
        var gTextOk = GlobalHelper.createTags('g', '', 'text');
        var gStrikeThroughOk = GlobalHelper.createTags('g', '', 'strikeThrough');
        var gWordsOk = GlobalHelper.createTags('g', '', 'words');
        var textOk = GlobalHelper.createTags('text');
        textOk.setAttribute("font-weight", "Normal");
        textOk.setAttribute("font-style", "Normal");
        textOk.setAttribute("fill", "rgba(0,0,0,255)");
        textOk.setAttribute("fill-opacity", "1");
        textOk.setAttribute('font-size', 13.1);
        var tspanOk = GlobalHelper.createTags('tspan');
        tspanOk.setAttribute("font-family", "Arial");
        tspanOk.setAttribute("x", "65.6");
        tspanOk.setAttribute("y", "18.6");
        tspanOk.innerHTML = 'OK';
        textOk.appendChild(tspanOk);
        gWordsOk.appendChild(textOk);
        gTextOk.appendChild(gStrikeThroughOk);
        gTextOk.appendChild(gWordsOk);
        svgOk.appendChild(gTextOk);
        divOk.appendChild(svgOk);
        divStretchFill.appendChild(divOk);
        var divContent = GlobalHelper.createTags('div');
        divContent.setAttribute('style', 'position: absolute; width:' + 540 + 'px; height: ' + 50 + 'px; top: ' + 270 + 'px; left:' + 243 + 'px; z-index: 0;');
        var svgContent = GlobalHelper.createTags('svg');
        svgContent.setAttribute('width', 540);
        svgContent.setAttribute('height', 50);
        svgContent.setAttribute('style', 'position: absolute; overflow: unset;');
        var gContent = GlobalHelper.createTags('g', '', "shape triggerEvent");
        var pathContent = GlobalHelper.createTags('path');
        pathContent.setAttribute('d', "M0,0L540,0 540,50 0,50z");
        pathContent.setAttribute('stroke', 'transparent');
        pathContent.setAttribute('stroke-width', 0);
        pathContent.setAttribute('fill', 'transparent');
        pathContent.setAttribute('stroke-linecap', 'Square');
        pathContent.setAttribute('stroke-linejoin', 'Miter');
        pathContent.setAttribute('stroke-dasharray', '');
        gContent.appendChild(pathContent);
        svgContent.appendChild(gContent);
        var gTextContent = GlobalHelper.createTags('g', '', 'text');
        var gStrikeThroughContent = GlobalHelper.createTags('g', '', 'strikeThrough');
        var gWordsContent = GlobalHelper.createTags('g', '', 'words');
        var textContent = GlobalHelper.createTags('text');
        textContent.setAttribute("font-weight", "Normal");
        textContent.setAttribute("font-style", "Normal");
        textContent.setAttribute("fill", "rgba(0,0,0,255)");
        textContent.setAttribute("fill-opacity", "1");
        textContent.setAttribute('font-size', 16.8);
        var tspanContent = GlobalHelper.createTags('tspan');
        tspanContent.setAttribute("font-family", "Arial");
        tspanContent.setAttribute("x", "0");
        tspanContent.setAttribute("y", "30");
        tspanContent.innerHTML = 'You have riched the time limit set for the quiz. Press \'OK\' to continue!';
        textContent.appendChild(tspanContent);
        gWordsContent.appendChild(textContent);
        gTextContent.appendChild(gStrikeThroughContent);
        gTextContent.appendChild(gWordsContent);
        svgContent.appendChild(gTextContent);
        divContent.appendChild(svgContent);
        divStretchFill.appendChild(divContent);
        var divShape = GlobalHelper.createTags('div');
        divShape.setAttribute('style', 'position: absolute; width:' + 540 + 'px; height: ' + 3 + 'px; top: ' + 243 + 'px; left:' + 243 + 'px; z-index: 0;');
        var svgShape = GlobalHelper.createTags('svg');
        svgShape.setAttribute('width', 540);
        svgShape.setAttribute('height', 3);
        svgShape.setAttribute('style', 'position: absolute; overflow: unset;');
        var gShape = GlobalHelper.createTags('g', '', "shape triggerEvent");
        var pathShape = GlobalHelper.createTags('path');
        pathShape.setAttribute('d', "M0,0L540,0 540,3 0,3z");
        pathShape.setAttribute('stroke', 'transparent');
        pathShape.setAttribute('stroke-width', 0);
        pathShape.setAttribute('fill', 'rgba(24,136,231,1)');
        pathShape.setAttribute('stroke-linecap', 'Square');
        pathShape.setAttribute('stroke-linejoin', 'Miter');
        pathShape.setAttribute('stroke-dasharray', '');
        gShape.appendChild(pathShape);
        svgShape.appendChild(gShape);
        divShape.appendChild(svgShape);
        divStretchFill.appendChild(divShape);
        divStretchFill.innerHTML = divStretchFill.innerHTML;
        return divStretchFill;
    }

    function ParseTime(t) {
        var second, minute, hour, mili;
        hour = Math.floor(t / 3600);
        minute = Math.floor(t % 3600);
        minute = Math.floor(minute / 60);
        second = Math.floor(t % 60);
        return ParseString(hour) + ":" + ParseString(minute) + ":" + ParseString(second);
    }

    function ParseString(i) {
        if (i < 10) {
            return "0" + i;
        } else {
            return i;
        }
    }
})();
$('#content').on('mouseleave', '.drop-list', function() {
    $(this).find('.drop-list-drop-down').css('display', 'none');
});

function Swape(obj1, obj2) {
    var temp = document.createElement("div");
    obj1.parentNode.insertBefore(temp, obj1);
    obj2.parentNode.insertBefore(obj1, obj2);
    temp.parentNode.insertBefore(obj2, temp);
    temp.parentNode.removeChild(temp);
}

function GetYourScore(options) {
    var totalScore = 0;
    for (var i = 0; i < DOC.slds.length; i++) {
        if (DOC.slds[i].slt === "QuestionSlide") {
            for (var j = 0; j < options.q.length; j++) {
                if (DOC.slds[i].id == options.q[j]) {
                    var score = GlobalHelper.DataLocal.Slide["slide" + i].Point;
                    if (score != null) {
                        totalScore += parseFloat(score);
                    }
                }
            }
        }
    }
    return parseInt(totalScore);
}

function GetTextYourScore(options) {
    var str;
    if (GetTotalScore(options) != 0) {
        QuizzModule.Results.StudentScore = Math.round(GetYourScore(options) / GetTotalScore(options) * 10000) / 100;
        str = QuizzModule.Results.StudentScore + "% (" + GetYourScore(options) + " Point)";
    } else {
        str = "0% (" + GetYourScore(options) + " Point)";
    }
    return str;
}

function GetTotalScore(options) {
    var total = 0;
    for (var i = 0; i < DOC.slds.length; i++) {
        for (var j = 0; j < options.q.length; j++) {
            if (DOC.slds[i].id == options.q[j]) {
                for (var k = 0; k < DOC.slds[i].lts.length; k++) {
                    if (!DOC.slds[i].lts[k].iM) {
                        if (DOC.slds[i].lts[k].lT == "Correct") {
                            total += parseInt(DOC.slds[i].lts[k].sC);
                            break;
                        }
                    }
                }
            }
        }
    }
    if (total == 0) {
        total = 10;
    }
    return parseFloat(total);
}

function GetTextPassScore(options) {
    QuizzModule.Results.PassingScore = parseInt(options.ps);
    return parseInt(options.ps) + "% (" + GetTotalScore(options) * GlobalResources.CurrentSlide.Data.ps / 100 + " Point)";
}

function GetScoreFeedBack(name, options) {
    for (var i = 0; i < options.lts.length; i++) {
        if (!options.lts[i].iM) {
            if (options.lts[i].lT == name) {
                return options.lts[i].sC;
            }
        }
    }
    return null;
}

function ShowFeedback(name, options) {
    for (var i = 0; i < options.lts.length; i++) {
        if (!options.lts[i].iM) {
            if (options.lts[i].lT === name) {
                var layout = GlobalResources.CurrentSlide.getLayoutById(options.lts[i].id);
                var elemLayout = document.getElementById(options.lts[i].id + "_w");
                var videos = elemLayout.querySelectorAll('video');
                var audios = elemLayout.querySelectorAll('audio');
                if (videos.length > 0) {
                    for (var i = 0; i < videos.length; i++) {
                        if (videos[i].getAttribute('data-play')) {
                            videos[i].play();
                        }
                    }
                }
                if (audios.length > 0) {
                    for (var i = 0; i < audios.length; i++) {
                        audios[i].play();
                    }
                }
                if (layout != null) {
                    layout.show();
                    break;
                }
            }
        }
    }
}

function ShowMessage(state) {
    var el = document.getElementById('contain-message');
    if (el != null) {
        el.parentElement.removeChild(el);
    }
    var noti = null;
    if (state) {
        noti = new Notification("Correct");
    } else {
        noti = new Notification("InCorrect");
    }
    if (noti != null) {
        document.getElementById('container').appendChild(noti.ShowMessage());
    }
}

function PrintResult(id) {
    for (var i = 0; i < DOC.slds.length; i++) {
        if (DOC.slds[i].id == id) {
            break;
        }
    }
}

function GetObjectDataLocal() {
    if (typeof(Storage) !== "undefined") {
        var object = null;
        try {
            var data = localStorage.getItem(KEY);
            if (data) {
                object = JSON.parse(Ma.de(data));
            }
        } catch (error) {
            console.log('Not support localStorage');
        }
        return object;
    }
    return null;
}
window.onload = function() {
    var obj = GetObjectDataLocal();
    if (obj) {
        elemResume.style.display = "flex";
        GlobalHelper.DataLocal = obj;
    } else {
        QuizzModule.InitLocalStorage();
        GlobalHelper.onloaded();
    }
}
var Result = (function() {
    function Result(stt, question, correctAnswer, studentAnswer, result, pointAward) {
        this.stt = stt;
        this.question = question;
        this.correctAnswer = correctAnswer;
        this.studentAnswer = studentAnswer;
        this.result = result;
        this.pointAward = pointAward;
    }
    Object.defineProperty(Result.prototype, "Stt", {
        get: function() {
            return this.stt;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Result.prototype, "Question", {
        get: function() {
            return this.question;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Result.prototype, "CorrectAnswer", {
        get: function() {
            return this.correctAnswer;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Result.prototype, "StudentAnswer", {
        get: function() {
            return this.studentAnswer;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Result.prototype, "Result", {
        get: function() {
            return this.result;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Result.prototype, "PointAward", {
        get: function() {
            return this.pointAward;
        },
        enumerable: true,
        configurable: true
    });
    return Result;
}());
var Notification = (function() {
    function Notification(name) {
        this.Name = name;
    }
    Notification.prototype.CreateEleQuestion = function(name, color) {
        var self = this;
        var divContainer = document.createElement('div');
        var tspanContent = document.createElement('tspan');
        var tspan = document.createElement('tspan');
        divContainer.id = "contain-message";
        tspan.className = "closeMessage";
        tspan.innerHTML = "&times;";
        tspan.addEventListener('click', function(e) {
            self.CloseMessage(e.target.parentElement);
        }, false);
        tspanContent.innerHTML = name;
        divContainer.className = "message";
        divContainer.style.background = color;
        divContainer.appendChild(tspanContent);
        divContainer.appendChild(tspan);
        return divContainer;
    };
    Notification.prototype.CreateEleVideo = function() {
        var self = this;
        var l = new Lauguage('EN', 'video');
        var div1 = document.createElement('div');
        var a = document.createElement('a');
        var divContainer = document.createElement('div');
        var div2 = document.createElement('div');
        var div3 = document.createElement('div');
        var tspan = document.createElement('tspan');
        divContainer.id = "contain-message";
        divContainer.className = "notifiVideo";
        div1.innerHTML = l.Process().End;
        div2.style.marginLeft = "8%";
        a.href = "chrome://flags/#autoplay-policy";
        a.setAttribute('TARGET', '_blank');
        a.innerHTML = 'chrome://flags/#autoplay-policy';
        div3.innerHTML = l.Process().TurnOn;
        div3.className = "turnOnVideo";
        div2.innerHTML = l.Process().Begin;
        tspan.className = "closeMessage";
        tspan.innerHTML = "&times;";
        div3.addEventListener('click', function() {
            var listEle = document.querySelectorAll('video');
            for (var i = 0; i < listEle.length; i++) {
                listEle[i].muted = false;
            }
            ChangeVolume(true);
        }, false);
        tspan.addEventListener('click', function(e) {
            self.CloseMessage(e.target.parentElement);
        }, false);
        div1.appendChild(a);
        divContainer.appendChild(div2);
        divContainer.appendChild(div3);
        divContainer.appendChild(div1);
        divContainer.appendChild(tspan);
        return divContainer;
    };
    Notification.prototype.CloseMessage = function(elem) {
        elem.parentElement.removeChild(elem);
    };
    Notification.prototype.ShowMessage = function() {
        var d = null;
        switch (this.Name) {
            case "InCorrect":
                d = this.CreateEleQuestion("Incorrect", "#be3a3a");
                break;
            case "Correct":
                d = this.CreateEleQuestion("Correct", "rgb(41, 132, 67)");
                break;
            case "Video":
                d = this.CreateEleVideo();
                break;
            default:
                break;
        }
        return d;
    };
    return Notification;
}());
var Lauguage = (function() {
    function Lauguage(country, type) {
        this.Country = country;
        this.Type = type;
    }
    Lauguage.prototype.Process = function() {
        switch (this.Type) {
            case 'video':
                switch (this.Country) {
                    case 'VN':
                        var obj = { Begin: "Video hoặc audio này đang tắt tiếng ", TurnOn: "BẬT", End: " hoặc truy cập vào đường links này " };
                        return obj;
                        break;
                    case 'EN':
                        var obj = {
                            Begin: "This video or audio is muted",
                            TurnOn: "Turn On",
                            End: " or visit this link "
                        };
                        return obj;
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }
    };
    return Lauguage;
}());
var Question = (function() {
    function Question(options) {
        this.Options = options;
        this.IndexSlide = DOC.slds.indexOf(options);
        this.CurrentOptions = this.GetOptionAnswer(options);
        this.UserReply = null;
        this.NumberRepeat = null;
        this.ArrAnswer = [];
        this.Type = 0;
        this.DisplayFeedback = (options.ft != "None") ? true : false;
    }
    Question.prototype.GetIdFeedBack = function(nameFeedback) {
        var feedBack = this.Options.lts.filter(function(x) {
            return x.lT == nameFeedback;
        })[0];
        var idFeedBack = '';
        if (feedBack) {
            idFeedBack = feedBack.id;
        }
        return idFeedBack;
    };
    Question.prototype.GetData = function() {
        switch (this.Options.tq) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 8:
            case 11:
            case 12:
            case 13:
            case 14:
            case 17:
                this.GetData_HaveasLocalStorage();
                break;
            default:
                break;
        }
        this.Type = this.Options.gt;
    };
    Question.prototype.GetData_HaveasLocalStorage = function() {
        var slide = 'slide' + this.IndexSlide;
        if (GlobalHelper.DataLocal.Slide[slide].Answers != null) {
            this.ArrAnswer = QuizzModule.ReadDataLocal(this.CurrentOptions, GlobalHelper.DataLocal.Slide[slide].Answers);
            this.NumberRepeat = GlobalHelper.DataLocal.Slide[slide].NumberRepeat;
            this.UserReply = GlobalHelper.DataLocal.Slide[slide].UserReply;
        } else {
            for (var j = 0; j < this.CurrentOptions.as.length; j++) {
                var currentAnswer = this.CurrentOptions.as[j];
                var item = { Answers: currentAnswer };
                this.ArrAnswer.push(item);
            }
            this.NumberRepeat = this.Options.atm;
            if (this.Options.sff) {
                this.ArrAnswer = QuizzModule.RandomAnswer(this.ArrAnswer);
            }
            QuizzModule.SaveAnswer(this.IndexSlide, this.ArrAnswer, this.NumberRepeat);
        }
    };
    Question.prototype.GetOptionAnswer = function(options) {
        for (var i = 0; i < options.lts.length; i++) {
            if (options.lts[i].iM) {
                for (var j = 0; j < options.lts[i].e.length; j++) {
                    if (options.lts[i].e[j].c == "Answers") {
                        return options.lts[i].e[j];
                    }
                }
            }
        }
        return null;
    };
    Question.prototype.IsCheckReview = function() {
        if (GlobalHelper.DataLocal.Slide['slide' + this.IndexSlide]) {
            return GlobalHelper.DataLocal.Slide['slide' + this.IndexSlide];
        }
        return null;
    };
    return Question;
}());
var QuestionOneSelection = (function(_super) {
    __extends(QuestionOneSelection, _super);

    function QuestionOneSelection(options) {
        var _this = _super.call(this, options) || this;
        _this.GetData();
        return _this;
    }
    QuestionOneSelection.prototype.CreateElementQuestion = function() {
        var containAnswer = document.createElement('div');
        var self = this;
        containAnswer.id = this.CurrentOptions.id;
        containAnswer.setAttribute('class', 'answerContainer');
        containAnswer.style.width = this.CurrentOptions.w + "px";
        containAnswer.style.height = this.CurrentOptions.h + "px";
        var answers = this.CurrentOptions.as;
        var top = 0;
        for (var i = 0; i < answers.length; i++) {
            var contentdiv = document.createElement('div');
            var svg = document.createElementNS(xmlns, 'svg');
            var gText1 = document.createElementNS(xmlns, 'g');
            var check = false;
            var gShape = ShapeHelper.generateShapes(this.ArrAnswer[i].Answers.shs, { "id": this.ArrAnswer[i].Answers.id, "width": this.ArrAnswer[i].Answers.w, "height": this.ArrAnswer[i].Answers.h });
            var g = document.createElementNS(xmlns, 'g');
            if (this.ArrAnswer[i].Answers.efs != null && this.ArrAnswer[i].Answers.efs[0] != null) {
                if (this.ArrAnswer[i].Answers.efs[0].typ == "SoftEdge") {
                    check = true;
                }
                var effEle = EffectModule.genGEffect(this.ArrAnswer[i].Answers.efs[0], { Id: this.ArrAnswer[i].Answers.id, width: this.ArrAnswer[i].Answers.w, height: this.ArrAnswer[i].Answers.h }, this.ArrAnswer[i].Answers.agl);
                if (effEle.g != undefined) {
                    g.appendChild(effEle.g);
                }
            }
            if (!check) {
                g.appendChild(gShape);
            }
            var g1 = document.createElementNS(xmlns, 'g');
            var g2 = document.createElementNS(xmlns, 'g');
            var g3 = document.createElementNS(xmlns, 'g');
            var rect = document.createElementNS(xmlns, 'rect');
            var defs = document.createElementNS(xmlns, 'defs');
            var linearGradient = document.createElementNS(xmlns, 'linearGradient');
            var linearGradient1 = document.createElementNS(xmlns, 'linearGradient');
            var circleInside = document.createElementNS(xmlns, 'circle');
            var circleOut = document.createElementNS(xmlns, 'circle');
            var circleCheck = document.createElementNS(xmlns, 'circle');
            contentdiv.id = this.ArrAnswer[i].Answers.id + "_a";
            if (i == 0) {
                contentdiv.style.marginTop = this.CurrentOptions.as[0].T + "px";
            } else {
                contentdiv.style.marginTop = this.CurrentOptions.as[1].T - this.CurrentOptions.as[0].T - this.CurrentOptions.as[0].h + "px";
            }
            contentdiv.style.marginLeft = this.ArrAnswer[i].Answers.l + "px";
            contentdiv.setAttribute('class', 'myContentSVG');
            contentdiv.style.width = this.ArrAnswer[i].Answers.w + "px";
            contentdiv.style.height = this.ArrAnswer[i].Answers.h + "px";
            svg.id = this.ArrAnswer[i].Answers.id + "_s";
            svg.setAttribute('class', 'Answer-OneSelect');
            svg.setAttribute('height', this.ArrAnswer[i].Answers.h);
            svg.setAttribute('width', this.ArrAnswer[i].Answers.w);
            svg.select = false;
            rect.setAttribute('width', this.ArrAnswer[i].Answers.w + "px");
            rect.setAttribute('height', this.ArrAnswer[i].Answers.h + "px");
            rect.setAttribute('opacity', '0');
            rect.setAttribute('rx', '10px');
            rect.setAttribute('ry', '10px');
            g.setAttribute('class', 'eventable');
            g.id = "object-g_" + this.ArrAnswer[i].Answers.id;
            g1.id = "uniqueDom-a_" + this.ArrAnswer[i].Answers.id;
            g2.id = "uniqueDom-b_" + this.ArrAnswer[i].Answers.id;
            g3.id = "uniqueDom-c_" + this.ArrAnswer[i].Answers.id;
            var radio = this.ArrAnswer[i].Answers.rB;
            circleInside.id = "object-circle-inside_" + this.ArrAnswer[i].Answers.id;
            circleInside.setAttribute('cx', (radio.lR) - this.ArrAnswer[i].Answers.l);
            circleInside.setAttribute('cy', (radio.tR) - this.ArrAnswer[i].Answers.T);
            circleInside.setAttribute('r', radio.rCi);
            circleInside.setAttribute('fill', radio.C.col);
            circleOut.id = "object-circle-out_" + this.CurrentOptions.as[i].id;
            circleOut.setAttribute('cx', radio.lR - this.ArrAnswer[i].Answers.l);
            circleOut.setAttribute('cy', radio.tR - this.ArrAnswer[i].Answers.T);
            circleOut.setAttribute('r', radio.r);
            circleOut.setAttribute('fill', radio.C.col);
            circleOut.setAttribute('stroke-width', radio.wB);
            circleOut.setAttribute('stroke', radio.cB.col);
            circleCheck.id = this.ArrAnswer[i].Answers.id + "_cc";
            circleCheck.setAttribute('class', "Circle-Selected");
            circleCheck.setAttribute('cx', radio.lR - this.ArrAnswer[i].Answers.l);
            circleCheck.setAttribute('cy', radio.tR - this.ArrAnswer[i].Answers.T);
            circleCheck.setAttribute('r', radio.rC);
            circleCheck.setAttribute('fill', radio.cC.col);
            circleCheck.style.visibility = "hidden";
            if (this.UserReply != null) {
                if (this.UserReply == this.ArrAnswer[i].Answers.id) {
                    circleCheck.style.visibility = "visible";
                    svg.select = true;
                }
            }
            gText1.setAttribute('class', 'myCircle');
            gText1.setAttribute('transform', "translate(" + (this.ArrAnswer[i].Answers.ch.l - this.ArrAnswer[i].Answers.l) + "," + (this.ArrAnswer[i].Answers.ch.T - this.ArrAnswer[i].Answers.T) + ")");
            var g6 = TextModule.generateElementByParas(this.ArrAnswer[i].Answers.ct.pr);
            g6.setAttribute('class', 'mySpanText');
            if (this.Type == 0) {
                if (this.IsCheckReview().IsResult) {
                    var gCheck = document.createElementNS(xmlns, 'g');
                    gCheck.setAttribute('transform', "translate(0," + (this.ArrAnswer[i].Answers.h - 18) / 2 + ")");
                    if (this.ArrAnswer[i].Answers.cr == true) {
                        gCheck.appendChild(QuizzModule.GetShapeTrue('m-8,10.56875l0,-1.1125l2.875,-2.78125l1.15,0l4.025,3.89375l10.925,-10.56875l1.15,0l2.875,2.78125l0,1.1125l-14.375,13.90625l-1.15,0l-7.475,-7.23125z'));
                    }
                    contentdiv.style.left = this.ArrAnswer[i].Answers.l - this.CurrentOptions.l - 12 + "px";
                    svg.setAttribute('viewBox', "-15 0 " + (this.ArrAnswer[i].Answers.w + 18) + " " + (this.ArrAnswer[i].Answers.h));
                    svg.setAttribute('width', this.ArrAnswer[i].Answers.w + 18);
                    if (GlobalHelper.DataLocal.Slide['slide' + this.IndexSlide].IsShowNoti) {
                        ShowMessage(this.IsCheckReview().Correct);
                    }
                }
            }
            if (QuizzModule.CheckSubmit(this.IndexSlide)) {
                svg.addEventListener('click', function(event) {
                    self.ProcessOneSelection(event);
                }, true);
                svg.setAttribute('class', 'mySvg Answer-OneSelect');
                circleOut.setAttribute('class', "myCircle");
                circleInside.setAttribute('class', "myCircle");
                rect.setAttribute('class', "myRect");
                circleCheck.setAttribute('class', "myCircle Circle-Selected");
            }
            gText1.appendChild(g6);
            g1.appendChild(rect);
            g2.appendChild(circleOut);
            g2.appendChild(circleInside);
            g2.appendChild(circleCheck);
            g.appendChild(g1);
            g.appendChild(g2);
            g.appendChild(gText1);
            if (gCheck != null) {
                g.appendChild(gCheck);
            }
            svg.appendChild(g);
            contentdiv.appendChild(svg);
            containAnswer.appendChild(contentdiv);
        }
        return containAnswer;
    };
    QuestionOneSelection.prototype.ProcessOneSelection = function(event) {
        var element = QuizzModule.GetElementParent(event);
        var numberId = QuizzModule.GetNumberId(element.id);
        var circle = document.getElementById(numberId + "_cc");
        var divCheck = document.getElementById(numberId + "_s");
        var children = document.getElementsByClassName('Answer-OneSelect');
        var circleSelected = document.getElementsByClassName('Circle-Selected');
        if (children != null) {
            for (var i = 0; i < children.length; i++) {
                children[i].select = false;
            }
            for (var i = 0; i < circleSelected.length; i++) {
                circleSelected[i].style.visibility = "hidden";
            }
            circle.style.visibility = 'visible';
            divCheck.select = true;
        }
        QuizzModule.SaveReply(this.IndexSlide, numberId, QuizzModule.GetTextUserReplay(element));
    };
    QuestionOneSelection.prototype.ProcessClickSubmit = function() {
        if (QuizzModule.CheckSelected('Answer-OneSelect')) {
            if (this.Type == 0) {
                if (this.CheckAnswerOneSelection(this.GetUserReply())) {
                    if (this.DisplayFeedback) {
                        ShowFeedback("Correct", this.Options);
                        QuizzModule.SaveIdFeedBack(this.IndexSlide, this.GetIdFeedBack("Correct"));
                    }
                    QuizzModule.SaveDataPoint(this.IndexSlide, GetScoreFeedBack("Correct", this.Options), true);
                    QuizzModule.ReviewOrSubmit(this.IndexSlide);
                    return;
                } else {
                    if (this.NumberRepeat == 0) {
                        if (this.DisplayFeedback) {
                            ShowFeedback("Try Again", this.Options);
                            QuizzModule.SaveIdFeedBack(this.IndexSlide, this.GetIdFeedBack("Try Again"));
                        }
                        QuizzModule.SaveAttemps(this.IndexSlide, 0);
                    } else {
                        if (this.NumberRepeat == 1) {
                            if (this.DisplayFeedback) {
                                ShowFeedback("Incorrect", this.Options);
                                QuizzModule.SaveIdFeedBack(this.IndexSlide, this.GetIdFeedBack("Incorrect"));
                            }
                            QuizzModule.SaveDataPoint(this.IndexSlide, GetScoreFeedBack("Incorrect", this.Options), false);
                            QuizzModule.ReviewOrSubmit(this.IndexSlide);
                        } else {
                            this.NumberRepeat--;
                            if (this.DisplayFeedback) {
                                ShowFeedback("Try Again", this.Options);
                                QuizzModule.SaveIdFeedBack(this.IndexSlide, this.GetIdFeedBack("Try Again"));
                            }
                            QuizzModule.SaveAttemps(this.IndexSlide, this.NumberRepeat);
                        }
                    }
                }
            } else {
                if (this.DisplayFeedback) {
                    ShowFeedback("Thank you", this.Options);
                    QuizzModule.SaveIdFeedBack(this.IndexSlide, this.GetIdFeedBack("Thank you"));
                }
                QuizzModule.ReviewOrSubmit(this.IndexSlide);
            }
        } else {
            if (this.DisplayFeedback) {
                QuizzModule.SaveIdFeedBack(this.IndexSlide, this.GetIdFeedBack("Invalid Answer"));
                ShowFeedback("Invalid Answer", this.Options);
            }
        }
        QuizzModule.SaveDataLocalStorage();
    };
    QuestionOneSelection.prototype.GetUserReply = function() {
        var child = document.getElementsByClassName('Answer-OneSelect');
        var result = null;
        for (var i = 0; i < child.length; i++) {
            if (child[i].select) {
                result = child[i].id;
                break;
            }
        }
        return result;
    };
    QuestionOneSelection.prototype.CheckAnswerOneSelection = function(result) {
        var index = QuizzModule.GetNumberId(result);
        for (var i = 0; i < this.CurrentOptions.as.length; i++) {
            if (this.CurrentOptions.as[i].id == index) {
                if (this.CurrentOptions.as[i].cr == true) {
                    return true;
                }
            }
        }
        return false;
    };
    return QuestionOneSelection;
}(Question));
var QuestionMultiSelection = (function(_super) {
    __extends(QuestionMultiSelection, _super);

    function QuestionMultiSelection(options) {
        var _this = _super.call(this, options) || this;
        _this.GetData();
        return _this;
    }
    QuestionMultiSelection.prototype.GetArrUserSelected = function() {
        if (this.UserReply != null) {
            var lsRessult = this.UserReply.split(',');
            for (var i = 0; i < lsRessult.length; i++) {
                this.ArrUserSelected.push(lsRessult[i]);
            }
        }
    };
    QuestionMultiSelection.prototype.CreateElementQuestion = function() {
        var containAnswer = document.createElement('div');
        containAnswer.id = this.CurrentOptions.id;
        containAnswer.className = 'answerContainer';
        containAnswer.style.width = this.CurrentOptions.w + "px";
        containAnswer.style.height = this.CurrentOptions.h + "px";
        var self = this;
        for (var i = 0; i < this.ArrAnswer.length; i++) {
            var contentdiv = document.createElement('div');
            var svg = document.createElementNS(xmlns, 'svg');
            var g = document.createElementNS(xmlns, 'g');
            var g1 = document.createElementNS(xmlns, 'g');
            var g2 = document.createElementNS(xmlns, 'g');
            var g3 = document.createElementNS(xmlns, 'g');
            var rect = document.createElementNS(xmlns, 'rect');
            var defs = document.createElementNS(xmlns, 'defs');
            var linearGradient = document.createElementNS(xmlns, 'linearGradient');
            var linearGradient1 = document.createElementNS(xmlns, 'linearGradient');
            var path = document.createElementNS(xmlns, 'path');
            var text = document.createElementNS(xmlns, 'tspan');
            var rectInside = document.createElementNS(xmlns, 'rect');
            var rectOut = document.createElementNS(xmlns, 'rect');
            var gText1 = document.createElementNS(xmlns, 'g');
            var gPathTrue = document.createElementNS(xmlns, 'g');
            var check = false;
            var gShape = ShapeHelper.generateShapes(this.ArrAnswer[i].Answers.shs, { "id": this.ArrAnswer[i].Answers.id, "width": this.ArrAnswer[i].Answers.w, "height": this.ArrAnswer[i].Answers.h }, this.ArrAnswer[i].Answers.agl);
            gShape.setAttribute("pointerEvents", "none");
            if (this.ArrAnswer[i].Answers.efs != null && this.ArrAnswer[i].Answers.efs[0] != null) {
                if (this.ArrAnswer[i].Answers.efs[0].typ == "SoftEdge") {
                    check = true;
                }
                var effEle = EffectModule.genGEffect(this.ArrAnswer[i].Answers.efs[0], { Id: this.ArrAnswer[i].Answers.id, width: this.ArrAnswer[i].Answers.w, height: this.ArrAnswer[i].Answers.h }, this.ArrAnswer[i].Answers.agl);
                if (effEle.g != undefined) {
                    g.appendChild(effEle.g);
                }
            }
            if (!check) {
                g.appendChild(gShape);
            }
            contentdiv.id = this.ArrAnswer[i].Answers.id + "_a";
            if (i == 0) {
                contentdiv.style.marginTop = this.CurrentOptions.as[0].T + "px";
            } else {
                contentdiv.style.marginTop = this.CurrentOptions.as[1].T - this.CurrentOptions.as[0].T - this.CurrentOptions.as[0].h + "px";
            }
            contentdiv.style.marginLeft = this.ArrAnswer[i].Answers.l + "px";
            contentdiv.setAttribute('class', 'myContentSVG');
            contentdiv.style.width = this.ArrAnswer[i].Answers.w + "px";
            contentdiv.style.height = this.ArrAnswer[i].Answers.h + "px";
            svg.id = this.ArrAnswer[i].Answers.id + "_s";
            svg.setAttribute('class', "myAnswer-MultiSelect");
            svg.setAttribute('height', this.ArrAnswer[i].Answers.h);
            svg.setAttribute('width', this.ArrAnswer[i].Answers.w);
            svg.select = false;
            rect.setAttribute('width', this.ArrAnswer[i].Answers.w + "px");
            rect.setAttribute('height', this.ArrAnswer[i].Answers.h + "px");
            rect.setAttribute('opacity', '0');
            rect.setAttribute('rx', '15');
            rect.setAttribute('ry', '15');
            var radio = this.ArrAnswer[i].Answers.chb;
            rectInside.setAttribute('x', radio.lC - this.ArrAnswer[i].Answers.l + (radio.wCo - radio.wC) / 2);
            rectInside.setAttribute('y', radio.tC - this.ArrAnswer[i].Answers.T + (radio.wCo - radio.wC) / 2);
            rectInside.setAttribute('width', radio.wC);
            rectInside.setAttribute('height', radio.wC);
            rectInside.setAttribute('fill', radio.cC.col);
            rectInside.setAttribute('rx', 5);
            rectInside.setAttribute('ry', 5);
            rectOut.setAttribute('x', radio.lC - this.ArrAnswer[i].Answers.l);
            rectOut.setAttribute('y', radio.tC - this.ArrAnswer[i].Answers.T);
            rectOut.setAttribute('width', radio.wCo);
            rectOut.setAttribute('height', radio.wCo);
            rectOut.setAttribute('fill', radio.cC.col);
            rectOut.setAttribute('rx', 5);
            rectOut.setAttribute('ry', 5);
            rectOut.setAttribute("stroke", radio.cB.col);
            rectOut.setAttribute("stroke-width", radio.wB);
            g.setAttribute('class', 'eventable');
            g.id = "object-g_" + this.ArrAnswer[i].Answers.id;
            g1.id = "uniqueDom-a_" + this.ArrAnswer[i].Answers.id;
            g2.id = "uniqueDom-b_" + this.ArrAnswer[i].Answers.id;
            g3.id = "uniqueDom-c_" + this.ArrAnswer[i].Answers.id;
            gPathTrue.setAttribute('transform', 'translate(' + (radio.lC - this.ArrAnswer[i].Answers.l + radio.wB / 2) + "," + (radio.tC - this.ArrAnswer[i].Answers.T + radio.wB / 2) + ")");
            path.id = "object-path-d_" + this.ArrAnswer[i].Answers.id;
            path.setAttribute('d', radio.pC);
            path.setAttribute('fill', radio.cP.col);
            path.setAttribute('stroke-linejoin', "round");
            path.setAttribute('stroke-linecap', "round");
            path.style.visibility = 'hidden';
            path.select = false;
            gText1.setAttribute('class', 'myCircle');
            gText1.setAttribute('transform', "translate(" + (this.ArrAnswer[i].Answers.ch.l - this.ArrAnswer[i].Answers.l) + "," + (this.ArrAnswer[i].Answers.ch.T - this.ArrAnswer[i].Answers.T) + ")");
            var text = new Text(this.ArrAnswer[i].Answers.ct.pr);
            var gTextAnswer = text.GenTexts();
            if (this.UserReply != null) {
                for (var k = 0; k < this.UserReply.length; k++) {
                    if (this.ArrAnswer[i].Answers.id == this.UserReply[k]) {
                        path.style.visibility = "visible";
                        svg.select = true;
                    }
                }
            }
            var gCheck = document.createElementNS(xmlns, 'g');
            if (this.Type == 0) {
                if (this.IsCheckReview().IsResult) {
                    gCheck.setAttribute('transform', "translate(0," + (this.ArrAnswer[i].Answers.h - 18) / 2 + ")");
                    if (this.ArrAnswer[i].Answers.cr == true) {
                        gCheck.appendChild(QuizzModule.GetShapeTrue("m-14,10.56875l0,-1.1125l2.875,-2.78125l1.15,0l4.025,3.89375l10.925,-10.56875l1.15,0l2.875,2.78125l0,1.1125l-14.375,13.90625l-1.15,0l-7.475,-7.23125z"));
                    }
                    contentdiv.style.left = this.ArrAnswer[i].l - this.CurrentOptions.l - 12 + "px";
                    svg.setAttribute('viewBox', "-22 0 " + (this.CurrentOptions.as[i].w + 8) + " " + (this.ArrAnswer[i].Answers.h));
                    svg.setAttribute('width', this.CurrentOptions.as[i].w + 8);
                    if (GlobalHelper.DataLocal.Slide['slide' + this.IndexSlide].IsShowNoti) {
                        ShowMessage(this.IsCheckReview().Correct);
                    }
                }
            }
            gTextAnswer.setAttribute('class', 'mySpanText');
            if (QuizzModule.CheckSubmit(this.IndexSlide)) {
                svg.addEventListener('click', function(event) {
                    self.ProcessMultiSelection(event);
                }, true);
                path.setAttribute('class', 'myPath');
                rect.setAttribute('class', "myRect");
                svg.setAttribute('class', 'mySvg myAnswer-MultiSelect');
            }
            g1.appendChild(rect);
            g2.appendChild(rectOut);
            g2.appendChild(rectInside);
            gPathTrue.appendChild(path);
            gText1.appendChild(gTextAnswer);
            g.appendChild(g1);
            g.appendChild(g2);
            g.appendChild(gText1);
            g.appendChild(gPathTrue);
            if (gCheck != null) {
                g.appendChild(gCheck);
            }
            svg.appendChild(g);
            contentdiv.appendChild(svg);
            containAnswer.appendChild(contentdiv);
        }
        return containAnswer;
    };
    QuestionMultiSelection.prototype.ProcessMultiSelection = function(event) {
        var element = QuizzModule.GetElementParent(event);
        var numberId = QuizzModule.GetNumberId(element.id);
        var path = document.getElementById('object-path-d_' + numberId);
        element.select = !element.select;
        if (element.select) {
            path.style.visibility = 'visible';
        } else {
            path.style.visibility = 'hidden';
        }
        var elemAnswer = document.getElementsByClassName('answerContainer')[0].children;
        var arrTextUser = [];
        if (elemAnswer) {
            for (var i = 0; i < elemAnswer.length; i++) {
                if (elemAnswer[i].children[0].select) {
                    var txtAnswer = "";
                    var elemTspan = document.getElementsByClassName('answerContainer')[0].children[i];
                    arrTextUser.push(QuizzModule.GetTextUserReplay(elemTspan));
                }
            }
        }
        QuizzModule.SaveReply(this.IndexSlide, this.GetArrUserReply(), arrTextUser);
    };
    QuestionMultiSelection.prototype.ProcessClickSubmit = function() {
        if (QuizzModule.CheckSelected('myAnswer-MultiSelect')) {
            if (this.Type == 0) {
                if (this.CheckAnswerMultiSelection()) {
                    if (this.DisplayFeedback) {
                        ShowFeedback("Correct", this.Options);
                        QuizzModule.SaveIdFeedBack(this.IndexSlide, this.GetIdFeedBack("Correct"));
                    }
                    QuizzModule.SaveDataPoint(this.IndexSlide, GetScoreFeedBack("Correct", this.Options), true);
                    QuizzModule.ReviewOrSubmit(this.IndexSlide);
                    return;
                } else {
                    if (this.NumberRepeat == 0) {
                        if (this.DisplayFeedback) {
                            ShowFeedback("Try Again", this.Options);
                            QuizzModule.SaveIdFeedBack(this.IndexSlide, this.GetIdFeedBack("Try Again"));
                        }
                        QuizzModule.SaveAttemps(this.IndexSlide, 0);
                    } else {
                        if (this.NumberRepeat == 1) {
                            if (this.DisplayFeedback) {
                                ShowFeedback("Incorrect", this.Options);
                                QuizzModule.SaveIdFeedBack(this.IndexSlide, this.GetIdFeedBack("Incorrect"));
                            }
                            QuizzModule.SaveDataPoint(this.IndexSlide, GetScoreFeedBack("Incorrect", this.Options), false);
                            QuizzModule.ReviewOrSubmit(this.IndexSlide);
                        } else {
                            if (this.DisplayFeedback) {
                                ShowFeedback("Try Again", this.Options);
                                QuizzModule.SaveIdFeedBack(this.IndexSlide, this.GetIdFeedBack("Try Again"));
                            }
                            this.NumberRepeat--;
                            QuizzModule.SaveAttemps(this.IndexSlide, this.NumberRepeat);
                        }
                    }
                }
            } else {
                if (this.DisplayFeedback) {
                    ShowFeedback("Thank you", this.Options);
                    QuizzModule.SaveIdFeedBack(this.IndexSlide, this.GetIdFeedBack("Thank you"));
                }
                QuizzModule.ReviewOrSubmit(this.IndexSlide);
            }
        } else {
            if (this.DisplayFeedback) {
                ShowFeedback("Invalid Answer", this.Options);
                QuizzModule.SaveIdFeedBack(this.IndexSlide, this.GetIdFeedBack("Invalid Answer"));
            }
        }
        QuizzModule.SaveDataLocalStorage();
    };
    QuestionMultiSelection.prototype.GetArrUserReply = function() {
        var children = document.getElementsByClassName('myAnswer-MultiSelect');
        var arr = [];
        for (var i = 0; i < children.length; i++) {
            if (children[i].select) {
                arr.push(QuizzModule.GetNumberId(children[i].id));
            }
        }
        if (arr.length == 0) {
            return null;
        }
        return arr;
    };
    QuestionMultiSelection.prototype.CheckAnswerMultiSelection = function() {
        var index = 0;
        var indexTrue = 0;
        var str = [];
        var arrUserSelected = this.GetArrUserReply();
        for (var k = 0; k < this.CurrentOptions.as.length; k++) {
            if (this.CurrentOptions.as[k].cr) {
                str.push(this.CurrentOptions.as[k].id);
            }
        }
        if (arrUserSelected.length == str.length) {
            for (var i = 0; i < arrUserSelected.length; i++) {
                for (var j = 0; j < arrUserSelected.length; j++) {
                    if (str[i] == arrUserSelected[j]) {
                        indexTrue++;
                        break;
                    }
                }
            }
            if (indexTrue == arrUserSelected.length) {
                return true;
            }
            return false;
        } else
            return false;
    };
    return QuestionMultiSelection;
}(Question));
var QuestionDragWorkBank = (function(_super) {
    __extends(QuestionDragWorkBank, _super);

    function QuestionDragWorkBank(options) {
        var _this = _super.call(this, options) || this;
        _this.GetData();
        return _this;
    }
    QuestionDragWorkBank.prototype.CreateElementQuestion = function() {
        var containAnswer = document.createElement('div');
        containAnswer.id = this.CurrentOptions.id;
        containAnswer.style.width = this.CurrentOptions.w + "px";
        containAnswer.style.height = this.CurrentOptions.h + "px";
        containAnswer.setAttribute('class', 'answerContainer');
        var self = this;
        for (var i = 0; i < this.ArrAnswer.length; i++) {
            var answer = document.createElement("div");
            var dragAnswer = document.createElementNS(xmlns, 'svg');
            var dragShape = document.createElementNS(xmlns, 'rect');
            var g = document.createElementNS(xmlns, 'g');
            var g2 = document.createElementNS(xmlns, 'g');
            var gText1 = document.createElementNS(xmlns, 'g');
            var defs = document.createElementNS(xmlns, 'defs');
            answer.id = this.ArrAnswer[i].Answers.id + '_a';
            answer.style.width = this.ArrAnswer[i].Answers.w + "px";
            answer.style.height = this.ArrAnswer[i].Answers.h + "px";
            if (i == 0) {
                answer.style.marginTop = this.CurrentOptions.as[0].T + "px";
            } else {
                answer.style.marginTop = this.CurrentOptions.as[1].T - this.CurrentOptions.as[0].T - this.CurrentOptions.as[0].h + "px";
            }
            dragAnswer.id = 'drag-answer_' + this.ArrAnswer[i].Answers.id;
            dragAnswer.setAttribute('width', this.ArrAnswer[i].Answers.w);
            dragAnswer.setAttribute('height', this.ArrAnswer[i].Answers.h);
            dragAnswer.setAttribute('class', "drag-answer");
            dragShape.id = "object-drag-shape_" + this.ArrAnswer[i].Answers.id;
            dragShape.setAttribute('rx', this.ArrAnswer[i].Answers.cRad);
            dragShape.setAttribute('ry', this.ArrAnswer[i].Answers.cRad);
            dragShape.setAttribute('fill', "url(#linearDrag)");
            dragShape.setAttribute('width', this.ArrAnswer[i].Answers.w);
            dragShape.setAttribute('height', this.ArrAnswer[i].Answers.h);
            var linear = QuizzModule.GetLinearGradient('linearDrag', "0%", "100%", "0%", "100%", "0%", "#FBFBFB", "1", "100%", "#EBEBEB", "1");
            var text = new Text(this.ArrAnswer[i].Answers.ct.pr);
            var gText = text.GenTexts();
            gText1.setAttribute('class', 'myCircle');
            gText1.setAttribute('transform', "translate(" + (this.ArrAnswer[i].Answers.ch.l - this.ArrAnswer[i].Answers.l) + "," + (this.ArrAnswer[i].Answers.ch.T - this.ArrAnswer[i].Answers.T) + ")");
            if (this.UserReply != null) {
                if (this.UserReply == this.ArrAnswer[i].Answers.id.toString()) {
                    answer.style.visibility = "hidden";
                    answer.select = true;
                }
            }
            if (QuizzModule.CheckSubmit(this.IndexSlide)) {
                dragShape.setAttribute('class', "myDropShape");
                dragAnswer.setAttribute('class', 'myContentDrop drag-answer');
                answer.setAttribute('class', 'answerDrag');
                setTimeout(function() { QuizzModule.DoSortAble(); }, 1);
                answer.style.cursor = 'pointer';
            }
            if (this.Type == 0) {
                if (this.IsCheckReview().IsResult) {
                    if (this.ArrAnswer[i].Answers.cr == true) {
                        if (this.UserReply != this.ArrAnswer[i].Answers.id.toString()) {
                            dragShape.setAttribute('stroke', 'green');
                            dragShape.setAttribute('stroke-width', "2");
                            dragShape.setAttribute('width', this.CurrentOptions.as[i].w - 4);
                            dragShape.setAttribute('height', this.CurrentOptions.as[i].h - 4);
                            dragShape.setAttribute('x', 2);
                            dragShape.setAttribute('y', 2);
                        }
                    }
                    if (GlobalHelper.DataLocal.Slide['slide' + this.IndexSlide].IsShowNoti) {
                        ShowMessage(this.IsCheckReview().Correct);
                    }
                }
            }
            gText1.appendChild(gText);
            g2.appendChild(gText1);
            g.appendChild(dragShape);
            defs.appendChild(linear);
            dragAnswer.appendChild(defs);
            dragAnswer.appendChild(g);
            dragAnswer.appendChild(g2);
            answer.appendChild(dragAnswer);
            containAnswer.appendChild(answer);
        }
        setTimeout(function() {
            QuizzModule.DoDraggable();
        }, 10);
        return containAnswer;
    };
    QuestionDragWorkBank.prototype.CreateElementDrop = function() {
        var div = document.createElement('div');
        var svgDrop = document.createElementNS(xmlns, 'svg');
        var g = document.createElementNS(xmlns, 'g');
        var g1 = document.createElementNS(xmlns, 'g');
        var g2 = document.createElementNS(xmlns, 'g');
        var rect = document.createElementNS(xmlns, 'rect');
        div.id = "drop-contain";
        div.style.left = (this.CurrentOptions.wb.l - this.CurrentOptions.l) + "px";
        div.style.top = (this.CurrentOptions.wb.T - this.CurrentOptions.T) + "px";
        div.style.width = this.CurrentOptions.wb.w + "px";
        div.style.height = this.CurrentOptions.wb.h + "px";
        div.style.position = "absolute";
        div.style.zIndex = this.CurrentOptions.z;
        svgDrop.setAttribute('width', this.CurrentOptions.wb.w);
        svgDrop.setAttribute('height', this.CurrentOptions.wb.h);
        rect.id = "shape-drop";
        rect.setAttribute('width', this.CurrentOptions.wb.w - 4);
        rect.setAttribute('height', this.CurrentOptions.wb.h - 4);
        rect.setAttribute('x', 2);
        rect.setAttribute('y', 2);
        rect.setAttribute('rx', this.CurrentOptions.wb.cRad);
        rect.setAttribute('ry', this.CurrentOptions.wb.cRad);
        rect.setAttribute('fill', 'url(#linearDrag)');
        var defs = QuizzModule.GetLinearGradient('linearDrag', "0%", "100%", "0%", "100%", "0%", "#FBFBFB", "1", "100%", "#EBEBEB", "1");
        g1.appendChild(defs);
        g2.appendChild(rect);
        g.appendChild(g1);
        g.appendChild(g2);
        for (var i = 0; i < this.CurrentOptions.as.length; i++) {
            if (this.UserReply != null) {
                if (this.CurrentOptions.as[i].id == this.UserReply) {
                    var g5 = document.createElementNS(xmlns, 'g');
                    var gText1 = document.createElementNS(xmlns, 'g');
                    gText1.setAttribute('transform', "translate(" + (this.CurrentOptions.as[i].ch.l - this.CurrentOptions.as[i].l) + "," + (this.CurrentOptions.as[i].ch.T - this.CurrentOptions.as[i].T) + ")");
                    var text = new Text(this.CurrentOptions.as[i].ct.pr);
                    var gText = text.GenTexts();
                    gText1.appendChild(gText);
                    g5.appendChild(gText1);
                    g.appendChild(g5);
                } else {
                    continue;
                }
            }
            if (this.Type == 0) {
                var data = GlobalHelper.DataLocal.Slide["slide" + this.IndexSlide];
                if (data.IsResult) {
                    if (this.UserReply == this.CurrentOptions.as[i].id.toString()) {
                        if (this.CurrentOptions.as[i].cr == true) {
                            rect.setAttribute('stroke', 'green');
                            rect.setAttribute('stroke-width', "2");
                        } else {
                            rect.setAttribute('stroke', 'red');
                            rect.setAttribute('stroke-width', "2");
                        }
                    } else {
                        rect.setAttribute('stroke', 'red');
                        rect.setAttribute('stroke-width', "2");
                    }
                    rect.setAttribute('x', 2);
                    rect.setAttribute('y', 2);
                    break;
                }
            }
        }
        svgDrop.appendChild(g);
        div.appendChild(svgDrop);
        return div;
    };
    QuestionDragWorkBank.prototype.ProcessClickSubmit = function() {
        var id = this.GetElementSelected();
        if (id != null) {
            if (this.Type == 0) {
                for (var j = 0; j < this.CurrentOptions.as.length; j++) {
                    if (this.CurrentOptions.as[j].id == id) {
                        if (this.CurrentOptions.as[j].cr) {
                            if (this.DisplayFeedback) {
                                ShowFeedback("Correct", this.Options);
                                QuizzModule.SaveIdFeedBack(this.IndexSlide, this.GetIdFeedBack("Correct"));
                            }
                            QuizzModule.SaveDataPoint(this.IndexSlide, GetScoreFeedBack("Correct", this.Options), true);
                            QuizzModule.ReviewOrSubmit(this.IndexSlide);
                            return;
                        }
                    }
                }
                if (this.NumberRepeat == 0) {
                    if (this.DisplayFeedback) {
                        ShowFeedback("Try Again", this.Options);
                        QuizzModule.SaveIdFeedBack(this.IndexSlide, this.GetIdFeedBack("Try Again"));
                    }
                    QuizzModule.SaveAttemps(this.IndexSlide, this.NumberRepeat);
                } else {
                    if (this.NumberRepeat == 1) {
                        if (this.DisplayFeedback) {
                            ShowFeedback("Incorrect", this.Options);
                            QuizzModule.SaveIdFeedBack(this.IndexSlide, this.GetIdFeedBack("Incorrect"));
                        }
                        QuizzModule.SaveDataPoint(this.IndexSlide, GetScoreFeedBack("Incorrect", this.Options), false);
                        QuizzModule.ReviewOrSubmit(this.IndexSlide);
                    } else {
                        if (this.DisplayFeedback) {
                            ShowFeedback("Try Again", this.Options);
                            QuizzModule.SaveIdFeedBack(this.IndexSlide, this.GetIdFeedBack("Try Again"));
                        }
                        this.NumberRepeat--;
                        QuizzModule.SaveAttemps(this.IndexSlide, this.NumberRepeat);
                    }
                }
            } else {
                if (this.DisplayFeedback) {
                    ShowFeedback("Thank you", this.Options);
                    QuizzModule.SaveIdFeedBack(this.IndexSlide, this.GetIdFeedBack("Thank you"));
                }
                QuizzModule.ReviewOrSubmit(this.IndexSlide);
            }
        } else {
            if (this.DisplayFeedback) {
                ShowFeedback("Invalid Answer", this.Options);
            }
        }
        QuizzModule.SaveDataLocalStorage();
    };
    QuestionDragWorkBank.prototype.GetElementSelected = function() {
        var listEle = document.getElementsByClassName('answerDrag');
        for (var i = 0; i < listEle.length; i++) {
            if (listEle[i].children[0].select) {
                var id = QuizzModule.GetNumberId(listEle[i].id);
                QuizzModule.SaveReply(this.IndexSlide, id);
                return id;
            }
        }
        return null;
    };
    return QuestionDragWorkBank;
}(Question));
var QuestionFillBlank = (function(_super) {
    __extends(QuestionFillBlank, _super);

    function QuestionFillBlank(options) {
        var _this = _super.call(this, options) || this;
        _this.GetData();
        _this.idQuestion = _this.CurrentOptions.id;
        return _this;
    }
    QuestionFillBlank.prototype.CreateElementQuestion = function() {
        var container = document.createElement('div');
        var inputResult = document.createElement('textarea');
        container.style.width = this.CurrentOptions.w + "px";
        container.style.height = this.CurrentOptions.h + "px";
        container.setAttribute('class', 'answerContainer');
        container.style.overflow = 'visible';
        inputResult.id = "a_" + this.CurrentOptions.id;
        inputResult.style.width = this.CurrentOptions.w - 10 + "px";
        inputResult.style.height = this.CurrentOptions.h - 5 + "px";
        inputResult.style.resize = 'none';
        inputResult.placeholder = this.CurrentOptions.tip.txt;
        inputResult.style.fontFamily = this.CurrentOptions.tip.ff;
        inputResult.style.fontSize = this.CurrentOptions.tip.fs + "px";
        inputResult.style.color = "black";
        inputResult.style.position = "absolute";
        inputResult.style.background = 'transparent';
        inputResult.className = "inputFill";
        if (this.UserReply != null) {
            inputResult.value = this.UserReply;
        }
        if (!QuizzModule.CheckSubmit(this.IndexSlide)) {
            inputResult.setAttribute('disabled', 'true');
        }
        container.appendChild(inputResult);
        if (this.Type == 0) {
            if (this.IsCheckReview().IsResult) {
                var div = document.createElement('div');
                var title = document.createElement('tspan');
                var textTitle = document.createElement('text');
                title.innerHTML = "Correct response";
                title.style.fontFamily = 'Arial';
                title.style.fontSize = '22px';
                div.style.display = 'grid';
                div.style.height = "auto";
                div.style.width = this.CurrentOptions.w + "px";
                div.style.top = this.CurrentOptions.h + 30 + "px";
                div.style.position = "absolute";
                textTitle.appendChild(title);
                div.appendChild(textTitle);
                for (var i = 0; i < this.CurrentOptions.as.length; i++) {
                    if (this.CurrentOptions.as[i].txt != "") {
                        var tspan = document.createElement("tspan");
                        var text = document.createElement('text');
                        tspan.innerHTML = "+ " + this.CurrentOptions.as[i].txt;
                        tspan.style.fontFamily = 'Arial';
                        tspan.style.fontSize = '20px';
                        text.style.marginLeft = "15px";
                        text.style.marginTop = '10px';
                        text.appendChild(tspan);
                        div.appendChild(text);
                    }
                }
                container.appendChild(div);
                if (GlobalHelper.DataLocal.Slide['slide' + this.IndexSlide].IsShowNoti) {
                    ShowMessage(this.IsCheckReview().Correct);
                }
            }
        }
        return container;
    };
    QuestionFillBlank.prototype.ProcessClickSubmit = function() {
        var text = document.getElementsByClassName('inputFill')[0];
        this.StringAnswer = text.value;
        if (this.StringAnswer != "") {
            if (this.Type == 0) {
                for (var i = 0; i < this.CurrentOptions.as.length; i++) {
                    if (this.StringAnswer.toUpperCase() == this.CurrentOptions.as[i].txt.toUpperCase()) {
                        if (this.DisplayFeedback) {
                            ShowFeedback("Correct", this.Options);
                            QuizzModule.SaveIdFeedBack(this.IndexSlide, this.GetIdFeedBack("Correct"));
                        }
                        QuizzModule.SaveDataPoint(this.IndexSlide, GetScoreFeedBack("Correct", this.Options), true);
                        QuizzModule.ReviewOrSubmit(this.IndexSlide);
                        return;
                    }
                }
                if (this.NumberRepeat == 0) {
                    if (this.DisplayFeedback) {
                        ShowFeedback("Try Again", this.Options);
                        QuizzModule.SaveIdFeedBack(this.IndexSlide, this.GetIdFeedBack("Try Again"));
                    }
                    QuizzModule.SaveAttemps(this.IndexSlide, this.NumberRepeat);
                } else {
                    if (this.NumberRepeat == 1) {
                        if (this.DisplayFeedback) {
                            ShowFeedback("Incorrect", this.Options);
                            QuizzModule.SaveIdFeedBack(this.IndexSlide, this.GetIdFeedBack("Incorrect"));
                        }
                        QuizzModule.SaveDataPoint(this.IndexSlide, GetScoreFeedBack("Incorrect", this.Options), false);
                        QuizzModule.ReviewOrSubmit(this.IndexSlide);
                    } else {
                        if (this.DisplayFeedback) {
                            ShowFeedback("Try Again", this.Options);
                            QuizzModule.SaveIdFeedBack(this.IndexSlide, this.GetIdFeedBack("Try Again"));
                        }
                        this.NumberRepeat--;
                        QuizzModule.SaveAttemps(this.IndexSlide, this.NumberRepeat);
                    }
                }
            } else {
                if (this.DisplayFeedback) {
                    ShowFeedback("Thank you", this.Options);
                    QuizzModule.SaveIdFeedBack(this.IndexSlide, this.GetIdFeedBack("Thank you"));
                }
                QuizzModule.ReviewOrSubmit(this.IndexSlide);
            }
        } else {
            if (this.DisplayFeedback) {
                ShowFeedback("Invalid Answer", this.Options);
                QuizzModule.SaveIdFeedBack(this.IndexSlide, this.GetIdFeedBack("Invalid Answer"));
            }
        }
        QuizzModule.SaveDataLocalStorage();
    };
    QuestionFillBlank.prototype.callProcess = function() {
        var self = this;
        var id = '#' + self.idQuestion + ' textarea';
        $('#mainContent').on('change', id, function(e) {
            if ($(this).val() != "") {
                var text = $(this).val();
                self.UserReply = text;
                QuizzModule.SaveReply(self.IndexSlide, self.UserReply);
            }
        });
    };
    return QuestionFillBlank;
}(Question));
var QuestionDragSequence = (function(_super) {
    __extends(QuestionDragSequence, _super);

    function QuestionDragSequence(options) {
        var _this = _super.call(this, options) || this;
        _this.GetData();
        return _this;
    }
    QuestionDragSequence.prototype.CreateElementQuestion = function() {
        var containAnswer = document.createElement('div');
        containAnswer.id = "contain-answers";
        containAnswer.style.width = this.CurrentOptions.w + "px";
        containAnswer.style.height = this.CurrentOptions.h + "px";
        containAnswer.setAttribute('class', 'answerContainer');
        var self = this;
        if (this.UserReply != null) {
            var arrUserReplay = this.UserReply.toString().split(',');
            var arr = [];
            for (var i = 0; i < arrUserReplay.length; i++) {
                for (var j_1 = 0; j_1 < this.CurrentOptions.as.length; j_1++) {
                    if (arrUserReplay[i] == this.CurrentOptions.as[j_1].id) {
                        var obj = { Answers: this.CurrentOptions.as[j_1] };
                        arr.push(obj);
                        break;
                    }
                }
            }
            this.ArrAnswer = arr;
        }
        for (var i = 0; i < this.ArrAnswer.length; i++) {
            var answer = document.createElement("div");
            var dragAnswer = document.createElementNS(xmlns, 'svg');
            var gText1 = document.createElementNS(xmlns, 'g');
            var dragShape = document.createElementNS(xmlns, 'rect');
            var g = document.createElementNS(xmlns, 'g');
            var g2 = document.createElementNS(xmlns, 'g');
            var defs = document.createElementNS(xmlns, 'defs');
            var divNum = document.createElement('div');
            answer.id = this.ArrAnswer[i].Answers.id + "_a";
            answer.style.width = this.ArrAnswer[i].Answers.w + 24 + "px";
            answer.style.height = this.ArrAnswer[i].Answers.h + "px";
            if (i == 0) {
                answer.style.marginTop = this.CurrentOptions.as[0].T + "px";
            } else {
                answer.style.marginTop = this.CurrentOptions.as[1].T - this.CurrentOptions.as[0].T - this.CurrentOptions.as[0].h + "px";
            }
            answer.className = "object-drag-squence";
            answer.style.display = 'flex';;
            answer.setAttribute('data-order', i);
            var currentAnswer = this.ArrAnswer[i].as;
            dragAnswer.setAttribute('class', 'myDrag');
            dragAnswer.setAttribute('width', this.ArrAnswer[i].Answers.w + "px");
            dragAnswer.setAttribute('height', this.ArrAnswer[i].Answers.h + "px");
            dragShape.id = "object-drag-shape" + i;
            dragShape.setAttribute('width', this.ArrAnswer[i].Answers.w + "px");
            dragShape.setAttribute('height', this.ArrAnswer[i].Answers.h + "px");
            dragShape.setAttribute('rx', 30);
            dragShape.setAttribute('ry', 30);
            dragShape.setAttribute('fill', 'url(#linearDrag)');
            divNum.className = "myAnswerNum";
            divNum.style.lineHeight = this.ArrAnswer[i].Answers.h + "px";
            divNum.style.width = "24px";
            divNum.style.Height = this.ArrAnswer[i].Answers.h + "px";
            divNum.innerHTML = i + 1 + ".";
            if (this.Type == 0) {
                if (this.IsCheckReview().IsResult) {
                    if (this.ArrAnswer[i].Answers.id == this.CurrentOptions.as[i].id) {
                        divNum.innerHTML = i + 1 + ".";
                        divNum.style.color = "green";
                    } else {
                        for (var j = 0; j < this.CurrentOptions.as.length; j++) {
                            if (this.ArrAnswer[i].Answers.id == this.CurrentOptions.as[j].id) {
                                divNum.innerHTML = j + 1 + ".";
                                divNum.style.color = "red";
                            }
                        }
                    }
                    if (GlobalHelper.DataLocal.Slide['slide' + this.IndexSlide].IsShowNoti) {
                        ShowMessage(this.IsCheckReview().Correct);
                    }
                }
            }
            var linear = QuizzModule.GetLinearGradient('linearDrag', "0%", "100%", "0%", "100%", "0%", "#F5F5F5", "1", "100%", "#e2e2e2", "1");
            var gText = TextModule.generateElementByParas(this.ArrAnswer[i].Answers.ct.pr);
            gText1.setAttribute('transform', "translate(" + (this.ArrAnswer[i].Answers.ch.l - this.ArrAnswer[i].Answers.l) + "," + (this.ArrAnswer[i].Answers.ch.T - this.ArrAnswer[i].Answers.T) / 2 + ")");
            if (QuizzModule.CheckSubmit(this.IndexSlide)) {
                gText.setAttribute('class', 'mySpanText');
                gText1.setAttribute('class', 'myCircle');
                dragAnswer.setAttribute('class', 'myContentDrop myDrag');
                dragShape.setAttribute('class', "myDropShape");
                setTimeout(function() { QuizzModule.DoSortAble(); }, 1);
                answer.style.cursor = "pointer";
            }
            gText1.appendChild(gText);
            defs.appendChild(linear);
            g.appendChild(dragShape);
            g2.appendChild(gText1);
            dragAnswer.appendChild(defs);
            dragAnswer.appendChild(g);
            dragAnswer.appendChild(g2);
            answer.appendChild(divNum);
            answer.appendChild(dragAnswer);
            containAnswer.appendChild(answer);
        }
        return containAnswer;
    };
    QuestionDragSequence.prototype.ProcessClickSubmit = function() {
        if (this.Type == 0) {
            var children = document.getElementsByClassName('myDrag');
            var k = 0;
            this.StringOrderSort = this.GetArrUserReply();
            for (var i = 0; i < children.length; i++) {
                if (this.CurrentOptions.as[i].id == this.StringOrderSort[i]) {
                    k++;
                } else
                    break;
            }
            if (k == children.length) {
                if (this.DisplayFeedback) {
                    ShowFeedback("Correct", this.Options);
                    QuizzModule.SaveIdFeedBack(this.IndexSlide, this.GetIdFeedBack("Correct"));
                }
                QuizzModule.SaveDataPoint(this.IndexSlide, GetScoreFeedBack("Correct", this.Options), true);
                QuizzModule.ReviewOrSubmit(this.IndexSlide);
            } else {
                if (this.NumberRepeat == 0) {
                    if (this.DisplayFeedback) {
                        ShowFeedback("Try Again", this.Options);
                        QuizzModule.SaveIdFeedBack(this.IndexSlide, this.GetIdFeedBack("Try Again"));
                    }
                    QuizzModule.SaveAttemps(this.IndexSlide, this.NumberRepeat);
                } else {
                    if (this.NumberRepeat == 1) {
                        if (this.DisplayFeedback) {
                            ShowFeedback("Incorrect", this.Options);
                            QuizzModule.SaveIdFeedBack(this.IndexSlide, this.GetIdFeedBack("Incorrect"));
                        }
                        QuizzModule.SaveDataPoint(this.IndexSlide, GetScoreFeedBack("Incorrect", this.Options), false);
                        QuizzModule.ReviewOrSubmit(this.IndexSlide);
                    } else {
                        if (this.DisplayFeedback) {
                            ShowFeedback("Try Again", this.Options);
                            QuizzModule.SaveIdFeedBack(this.IndexSlide, this.GetIdFeedBack("Try Again"));
                        }
                        this.NumberRepeat--;
                        QuizzModule.SaveAttemps(this.IndexSlide, this.NumberRepeat);
                    }
                }
            }
        } else {
            if (this.DisplayFeedback) {
                ShowFeedback("Thank you", this.Options);
                QuizzModule.SaveIdFeedBack(this.IndexSlide, this.GetIdFeedBack("Thank you"));
            }
            QuizzModule.ReviewOrSubmit(this.IndexSlide);
        }
        QuizzModule.SaveDataLocalStorage();
    };
    QuestionDragSequence.prototype.GetArrUserReply = function() {
        var children = document.getElementsByClassName('object-drag-squence');
        var arr = [];
        for (var i = 0; i < children.length; i++) {
            arr.push(QuizzModule.GetNumberId(children[i].id));
        }
        QuizzModule.SaveReply(this.IndexSlide, arr);
        return arr;
    };
    return QuestionDragSequence;
}(Question));
var QuestionSurveyLikertScale = (function(_super) {
    __extends(QuestionSurveyLikertScale, _super);

    function QuestionSurveyLikertScale(options) {
        var _this = _super.call(this, options) || this;
        _this.GetData();
        return _this;
    }
    QuestionSurveyLikertScale.prototype.CreateElementQuestion = function() {
        var containAnswerCircle = document.createElement('div');
        var self = this;
        containAnswerCircle.id = 'contain-answers';
        containAnswerCircle.style.width = this.CurrentOptions.w + "px";
        containAnswerCircle.style.height = this.CurrentOptions.h + "px";
        containAnswerCircle.setAttribute('class', 'answerContainer');
        for (var i = 0; i < this.ArrAnswer.length; i++) {
            var groupRaidoCircle = document.createElement('div');
            var answer = document.createElement("div");
            var groupAnswerCircle = document.createElement('div');
            var svg = document.createElementNS(xmlns, "svg");
            svg.setAttribute('width', this.CurrentOptions.as[i].ch.w + "px");
            svg.setAttribute('height', this.CurrentOptions.as[i].ch.h + "px");
            var text = new Text(this.ArrAnswer[i].Answers.ct.pr);
            var g = text.GenTexts();
            g.setAttribute('transform', "translate(" + (this.ArrAnswer[i].Answers.ch.l - this.ArrAnswer[i].Answers.l) + "," + (this.ArrAnswer[i].Answers.ch.T - this.ArrAnswer[i].Answers.T) + ")");
            answer.style.width = this.CurrentOptions.as[i].ch.w + "px";
            answer.style.height = this.CurrentOptions.as[i].ch.h + "px";
            answer.style.left = this.CurrentOptions.as[i].ch.l + "px";
            answer.style.top = this.CurrentOptions.as[i].ch.T - this.CurrentOptions.as[i].T + "px";
            answer.style.position = "absolute";
            groupRaidoCircle.id = 'group-answer-survey' + i;
            groupRaidoCircle.className = "myGroupRadioCircle";
            groupRaidoCircle.style.position = "absolute";
            groupRaidoCircle.style.width = this.CurrentOptions.as[i].lC.w + "px";
            groupRaidoCircle.style.height = this.CurrentOptions.as[i].lC.h + "px";
            groupRaidoCircle.style.left = this.CurrentOptions.as[i].lC.l - this.CurrentOptions.as[i].l + "px";
            groupRaidoCircle.style.top = 0 + "px";
            groupAnswerCircle.id = "group-answer-circle" + i;
            groupAnswerCircle.className = "myGroupAnswerCircle";
            groupAnswerCircle.style.width = this.CurrentOptions.as[i].w + "px";
            groupAnswerCircle.style.height = this.CurrentOptions.as[i].h + "px";
            groupAnswerCircle.style.left = this.CurrentOptions.as[i].l + "px";
            groupAnswerCircle.style.top = this.CurrentOptions.as[i].T + "px";
            for (var j = 0; j < this.CurrentOptions.as[i].S.length; j++) {
                var containSVG = document.createElement('div');
                var svg_1 = document.createElementNS(xmlns, 'svg');
                var containTooltip = document.createElement('div');
                var tooltip = document.createElement('p');
                var pathCircleOut = document.createElementNS(xmlns, 'circle');
                var pathCircleInside = document.createElementNS(xmlns, 'circle');
                var pathCircleChecked = document.createElementNS(xmlns, 'circle');
                var g1 = document.createElementNS(xmlns, 'g');
                var _id = i + "-" + j;
                var choice = this.CurrentOptions.as[i].S[j].ch;
                containSVG.className = "myContainCircle";
                containSVG.style.left = choice.lR + (32 - choice.r) + "px";
                containSVG.style.top = (this.CurrentOptions.as[i].h - this.CurrentOptions.as[i].S[0].ch.r * 2 - this.CurrentOptions.as[i].S[0].ch.wB * 2) / 2 + "px";
                pathCircleOut.id = _id + "_po";
                pathCircleOut.style.pointerEvents = 'none';
                pathCircleInside.style.pointerEvents = 'none';
                pathCircleInside.select = false;
                svg_1.id = _id + "_s";
                svg_1.setAttribute('width', (choice.r + choice.wB) * 2);
                svg_1.setAttribute('height', (choice.r + choice.wB) * 2);
                pathCircleOut.setAttribute('r', choice.r);
                pathCircleOut.setAttribute('cx', choice.r + choice.wB);
                pathCircleOut.setAttribute('cy', choice.r + choice.wB);
                pathCircleOut.setAttribute('fill', 'white');
                pathCircleOut.setAttribute('stroke', choice.cB.col);
                pathCircleOut.setAttribute('stroke-width', choice.wB);
                pathCircleInside.setAttribute('r', choice.rCi);
                pathCircleInside.setAttribute('cx', choice.r + choice.wB);
                pathCircleInside.setAttribute('cy', choice.r + choice.wB);
                pathCircleInside.setAttribute('fill', choice.C.col);
                pathCircleChecked.id = _id + "_p";
                pathCircleChecked.setAttribute('r', choice.rC);
                pathCircleChecked.setAttribute('cx', choice.r + choice.wB);
                pathCircleChecked.setAttribute('cy', choice.r + choice.wB);
                pathCircleChecked.setAttribute('fill', choice.cC.col);
                pathCircleChecked.style.visibility = 'hidden';
                containSVG.setAttribute('title', this.CurrentOptions.as[i].S[j].txt);
                if (this.UserReply != null) {
                    if (this.UserReply[i] == j) {
                        pathCircleChecked.style.visibility = 'visible';
                    }
                }
                if (QuizzModule.CheckSubmit(this.IndexSlide)) {
                    svg_1.addEventListener('click', function(event) {
                        self.ProcessClickRadio(event);
                    });
                    pathCircleChecked.setAttribute('class', 'myPathCircleInside');
                    containSVG.className = 'myContainCircle myHoverToolTip';
                    setTimeout(function() {
                        QuizzModule.ShowTooltip();
                    }, 10);
                }
                g1.appendChild(pathCircleOut);
                g1.appendChild(pathCircleInside);
                g1.appendChild(pathCircleChecked);
                svg_1.appendChild(g1);
                containSVG.appendChild(svg_1);
                groupRaidoCircle.appendChild(containSVG);
            }
            svg.appendChild(g);
            answer.appendChild(svg);
            groupAnswerCircle.appendChild(answer);
            groupAnswerCircle.appendChild(groupRaidoCircle);
            containAnswerCircle.appendChild(groupAnswerCircle);
        }
        if (this.UserReply == null) {
            var arr = [];
            for (var k = 0; k < this.CurrentOptions.as.length; k++) {
                arr.push(null);
            }
            this.UserReply = arr;
        }
        return containAnswerCircle;
    };
    QuestionSurveyLikertScale.prototype.ProcessClickRadio = function(event) {
        var numbers = QuizzModule.GetNumberId(event.target.id);
        var path = document.getElementById(numbers + "_p");
        var answerIndex = parseInt(numbers.split('-')[0]);
        var childIndex = parseInt(numbers.split('-')[1]);
        this.UserReply[answerIndex] = childIndex;
        this.changeColor(answerIndex);
        QuizzModule.SaveReply(this.IndexSlide, this.UserReply);
    };
    QuestionSurveyLikertScale.prototype.changeColor = function(i) {
        for (var j = 0; j < this.CurrentOptions.as[i].S.length; j++) {
            document.getElementById(i + "-" + j + "_p").style.visibility = "hidden";
        }
        var number = parseInt(this.UserReply[i]);
        document.getElementById(i + "-" + number + "_p").style.visibility = "visible";
    };
    QuestionSurveyLikertScale.prototype.ProcessClickSubmit = function() {
        if (this.GetArrUserReply().length == this.CurrentOptions.as.length) {
            if (this.DisplayFeedback) {
                ShowFeedback("Thank you", this.Options);
                QuizzModule.SaveIdFeedBack(this.IndexSlide, this.GetIdFeedBack("Thank you"));
            }
            QuizzModule.ReviewOrSubmit(this.IndexSlide);
        } else {
            if (this.DisplayFeedback) {
                ShowFeedback("Invalid Answer", this.Options);
                QuizzModule.SaveIdFeedBack(this.IndexSlide, this.GetIdFeedBack("Invalid Answer"));
            }
        }
        QuizzModule.SaveDataLocalStorage();
    };
    QuestionSurveyLikertScale.prototype.GetArrUserReply = function() {
        var arr = [];
        var listElement = document.getElementsByClassName('myPathCircleInside');
        for (var i = 0; i < listElement.length; i++) {
            if (listElement[i].style.visibility == "visible") {
                var numbers = QuizzModule.GetNumberId(listElement[i].id);
                var answerIndex = parseInt(numbers.split('-')[0]);
                var childIndex = parseInt(numbers.split('-')[1]);
                arr[answerIndex] = childIndex;
            }
        }
        return arr;
    };
    return QuestionSurveyLikertScale;
}(Question));
var QuestionEssay = (function(_super) {
    __extends(QuestionEssay, _super);

    function QuestionEssay(options) {
        var _this = _super.call(this, options) || this;
        _this.tip = options.lts[0].e[1].tip;
        _this.idQuestion = options.lts[0].e[1].id;
        _this.GetData();
        return _this;
    }
    QuestionEssay.prototype.GetData = function() {
        var slide = 'slide' + this.IndexSlide;
        if (GlobalHelper.DataLocal.Slide[slide].UserReply != null) {
            this.UserReply = GlobalHelper.DataLocal.Slide[slide].UserReply;
        } else {
            var text = "";
            var id = "#" + this.idQuestion;
            text = $(id).find('textarea').val();
            if (!text) {
                text = "";
            }
            var textInput = { Text: text, FontSize: this.tip.fs, FontFamily: this.tip.ff };
            return this.UserReply = textInput;
        }
    };
    QuestionEssay.prototype.CreateElementQuestion = function() {
        var widthFrame = this.CurrentOptions.w,
            heightFrame = this.CurrentOptions.h,
            topFrame = this.CurrentOptions.T,
            leftFrame = this.CurrentOptions.l;
        var divSlideLayer = GlobalHelper.createTags('div', '', 'slide-layer base-layer question shown');
        divSlideLayer.setAttribute('style', 'z-index:0; width:' + this.Options.w + 'px; height:' + this.Options.h + 'px;');
        divSlideLayer.setAttribute('type-question', 'essay');
        var divSlideObject = GlobalHelper.createTags('div', '', 'slide-object slide-object-textinput shown answerContainer');
        divSlideObject.setAttribute('style', 'z-index:1;width:' + (widthFrame + 1) + 'px;height:' + (heightFrame + 1) + 'px;opacity:1; top: 0px;');
        var div = GlobalHelper.createTags('div', this.Options.lts[0].e[1].id);
        div.setAttribute('style', 'height:' + (heightFrame + 1) + 'px; width:' + (widthFrame + 1) + 'px; transform:translate(0px, 0px);');
        var textarea = GlobalHelper.createTags('textarea');
        var fontFamily = this.CurrentOptions.tip.ff,
            fontSize = this.CurrentOptions.tip.fs;
        if (this.CurrentOptions.tip.fg) {
            var fontColor = this.CurrentOptions.tip.fg.col;
        } else {
            var fontColor = 'black';
        }
        textarea.setAttribute('style', 'font-family:' + fontFamily + '; font-size:' + fontSize +
            'px; line-height:normal;font-weight:normal; direction:ltr; text-align:left;color:' + fontColor + '; width:' + (widthFrame - 10) +
            'px; height:' + heightFrame + 'px; padding-top:8px; padding-right:10px; padding-bottom:0; padding-left:10px; resize: none;');
        if (!QuizzModule.CheckSubmit(this.IndexSlide)) {
            textarea.setAttribute('disabled', true);
        }
        textarea.innerHTML = this.UserReply.Text;
        if (this.CurrentOptions.tip.txt == "") {
            textarea.setAttribute('placeholder', 'type your text here');
        } else {
            textarea.setAttribute('placeholder', this.CurrentOptions.tip.txt);
        }
        textarea.setAttribute('maxlength', this.Options.MaxLength);
        div.appendChild(textarea);
        divSlideObject.appendChild(div);
        divSlideLayer.appendChild(divSlideObject);
        return divSlideObject;
    };
    QuestionEssay.prototype.ProcessClickSubmit = function() {
        var slide = 'slide' + this.IndexSlide;
        if (GlobalHelper.DataLocal.Slide[slide].as != null) {
            this.NumberRepeat = GlobalHelper.DataLocal.Slide[slide].NumberRepeat;
        }
        var ele = $('#' + this.idQuestion + ' textarea');
        if (ele.val() != "") {
            if (this.DisplayFeedback) {
                ShowFeedback("Thank you", this.Options);
                QuizzModule.SaveIdFeedBack(this.IndexSlide, this.GetIdFeedBack("Thank you"));
            }
            QuizzModule.ReviewOrSubmit(this.IndexSlide);
        } else {
            if (this.DisplayFeedback) {
                ShowFeedback("Invalid Answer", this.Options);
                QuizzModule.SaveIdFeedBack(this.IndexSlide, this.GetIdFeedBack("Invalid Answer"));
            }
        }
        QuizzModule.SaveDataLocalStorage();
    };
    QuestionEssay.prototype.callProcess = function() {
        var self = this;
        var id = '#' + self.idQuestion + ' textarea';
        $('#mainContent').on('change', id, function(e) {
            if ($(this).val() != "") {
                var text = $(this).val();
                self.UserReply = { Text: text };
                QuizzModule.SaveReply(self.IndexSlide, self.UserReply);
            }
        });
    };
    return QuestionEssay;
}(Question));
var QuestionRankingDropdown = (function(_super) {
    __extends(QuestionRankingDropdown, _super);

    function QuestionRankingDropdown(options) {
        var _this = _super.call(this, options) || this;
        _this.datach = null;
        _this.idQuestion = _this.Options.lts[0].e.filter(function(x) { return x.as; })[0].id;
        _this.loaiCauHoi = 'rankingDropDown';
        _this.GetData();
        return _this;
    }
    QuestionRankingDropdown.prototype.GetData = function() {
        this.CurrentOptions = this.Options.lts[0].e.filter(function(x) { return x.as; })[0];
        var slide = 'slide' + this.IndexSlide;
        if (GlobalHelper.DataLocal.Slide[slide].Answers != null) {
            this.ArrAnswer = GlobalHelper.DataLocal.Slide[slide].Answers;
            this.UserReply = GlobalHelper.DataLocal.Slide[slide].UserReply;
            this.NumberRepeat = GlobalHelper.DataLocal.Slide[slide].NumberRepeat;
        } else {
            var answers = this.CurrentOptions.as;
            var idsDropDown = [];
            for (var i2 = 0; i2 < answers.length; i2++) {
                idsDropDown.push(QuizzModule.RandomAnswer(answers.map(function(x) { return x.ch.id; })));
            }
            this.UserReply = GlobalHelper.DataLocal.Slide[slide].UserReply;
            this.ArrAnswer = idsDropDown;
            this.NumberRepeat = this.Options.atm;
        }
        QuizzModule.SaveAnswer(this.IndexSlide, this.ArrAnswer, this.NumberRepeat);
    };
    QuestionRankingDropdown.prototype.CreateElementQuestion = function() {
        var _this = this;
        var answers = this.CurrentOptions.as;
        var datach = this.CurrentOptions.as.map(function(x) { return x.ct; });
        var framech = this.CurrentOptions.as.map(function(x) { return x.ch; });
        var frameAnswer = this.CurrentOptions;
        var multi = 1;
        if (this.IsCheckReview()) {
            if (this.IsCheckReview().IsResult) {
                multi = 0.5;
            }
        }
        var divAbsolute = GlobalHelper.createTags('div', '', 'contain-answers slide-object answerContainer');
        divAbsolute.setAttribute('style', 'height: ' + frameAnswer.h + 'px; width:' +
            frameAnswer.w * multi + 'px;' + '; top:' + 0 + 'px; position: absolute; overflow-y: auto; overflow-x: hidden;');
        divAbsolute.setAttribute('id', this.idQuestion);
        divAbsolute.setAttribute('data-type', 'question');
        divAbsolute.setAttribute('data-type-question', this.loaiCauHoi);
        var noiDungDropDown = [];
        var idsDropDown = [];
        var i1 = 0;
        for (var i = 0; i < answers.length; i++) {
            noiDungDropDown = [];
            idsDropDown = [];
            var gChildSelect1;
            var tspan1 = GlobalHelper.createTags('tspan');
            tspan1.setAttribute('x', '12');
            tspan1.setAttribute('y', '30');
            tspan1.setAttribute('is-user-choice', 'no');
            var gSelect = GlobalHelper.createTags('g', '', 'select');
            var rect = GlobalHelper.createTags('rect');
            rect.setAttribute('width', frameAnswer.as[i].w * multi);
            rect.setAttribute('height', frameAnswer.as[i].h);
            rect.setAttribute('rx', 8);
            rect.setAttribute('ry', 8);
            rect.setAttribute('stroke', '#cecece');
            rect.setAttribute('fill', 'white');
            gSelect.appendChild(rect);
            var contentRight = document.createTextNode('--Select--');
            tspan1.appendChild(contentRight);
            var text1 = GlobalHelper.createTags('text', '', 'text');
            text1.setAttribute('font-family', 'Arial');
            text1.setAttribute('font-size', '16px');
            text1.setAttribute('fill', '#4B4B4B');
            text1.setAttribute('fill-opacity', '1');
            text1.appendChild(tspan1);
            if (this.UserReply && this.UserReply[i]) {
                var selectUser = answers.filter(function(x) { return x.ch.id == _this.UserReply[i]; })[0];
                gChildSelect1 = GlobalHelper.createTags('g', '', 'text');
                var gText = TextModule.generateElementByParas(selectUser.ct.pr);
                gText.setAttribute('transform', 'translate(' +
                    (selectUser.ct.l - answers.filter(function(x) { return x.ch.id === selectUser.ch.id; })[0].l) +
                    ',' + (selectUser.ct.T - selectUser.ch.T) + ')');
                gText.setAttribute('data-id', selectUser.ch.id);
                gChildSelect1.appendChild(gText);
                gSelect.appendChild(gChildSelect1);
            } else {
                gChildSelect1 = GlobalHelper.createTags('g', '', 'text');
                gChildSelect1.appendChild(text1);
                gSelect.appendChild(gChildSelect1);
            }
            var gChildSelect2 = GlobalHelper.createTags('g');
            gChildSelect2.setAttribute('transform', 'translate(' + (frameAnswer.as[i].w * multi - 20) +
                ', ' + (framech[i].h / 2) + ')');
            var path1 = GlobalHelper.createTags('path');
            path1.setAttribute('d', 'M 0 0 L 10 0 5 5 0 0');
            path1.setAttribute('fill', '#494949');
            path1.setAttribute('stroke', 'rgba(255, 255, 255, 0.8');
            gChildSelect2.appendChild(path1);
            gSelect.appendChild(gChildSelect2);
            var svg1 = GlobalHelper.createTags('svg');
            svg1.setAttribute('width', frameAnswer.as[i].w);
            svg1.setAttribute('height', frameAnswer.as[i].h);
            svg1.setAttribute('style', 'z-index:' + framech[i].z + ';');
            svg1.appendChild(gSelect);
            var divSelect = GlobalHelper.createTags('div', '', 'drop-list-top-container');
            divSelect.setAttribute('data-model-id', i);
            if (QuizzModule.CheckSubmit(this.IndexSlide)) {
                divSelect.addEventListener("click", function(e) {
                    QuizzModule.toggleDropdown(e);
                });
                rect.setAttribute('style', 'cursor: pointer;');
            }
            divSelect.appendChild(svg1);
            divSelect.innerHTML = divSelect.outerHTML;
            var gGroup = GlobalHelper.createTags('g', '', 'drop-list-cover');
            var topTransformOption = 0;
            var g4;
            idsDropDown = this.ArrAnswer[i];
            i1 = 0;
            for (i1 = 0; i1 < idsDropDown.length; i1++) {
                noiDungDropDown.push(this.CurrentOptions.as.filter(function(x) { return x.ch.id == idsDropDown[i1]; })[0].ct);
            }
            for (var j = 0; j < noiDungDropDown.length; j++) {
                var g3 = TextModule.generateElementByParas(noiDungDropDown[j].pr);
                var currentAnswer = this.CurrentOptions.as.filter(function(x) { return x.ch.id == idsDropDown[j]; })[0];
                g3.setAttribute('transform', 'translate(' + (datach[i].l - currentAnswer.l) + ',' + 0 + ')');
                g3.setAttribute('data-id', idsDropDown[j]);
                var rect3 = GlobalHelper.createTags('rect');
                rect3.setAttribute('width', frameAnswer.as[i].w * multi);
                rect3.setAttribute('height', framech[i].h);
                rect3.setAttribute('class', 'drop-list-item-out');
                rect3.setAttribute('fill', 'white');
                g4 = GlobalHelper.createTags('g', '', 'drop-list-item');
                g4.setAttribute('transform', 'translate(0,' + topTransformOption + ')');
                g4.appendChild(rect3);
                g4.appendChild(g3);
                gGroup.appendChild(g4);


                topTransformOption += framech[i].h;
            }
            var svgGroupOption = GlobalHelper.createTags('svg');
            svgGroupOption.setAttribute('width', frameAnswer.as[i].w);
            svgGroupOption.setAttribute('height', topTransformOption);
            svgGroupOption.setAttribute('z-index', noiDungDropDown.length - i);
            svgGroupOption.appendChild(gGroup);
            var divGroupOption = GlobalHelper.createTags('div', '', 'drop-list-drop-down-inner');
            divGroupOption.appendChild(svgGroupOption);
            var divScrollGroupOption = GlobalHelper.createTags('div', '', 'is-scrollable drop-list-drop-down');
            divScrollGroupOption.setAttribute('style', 'width:' + (frameAnswer.as[i].w - 2) * multi + 'px;border-color:#cecece;display:none;border-radius:16px;padding-top:' + (frameAnswer.as[i].h / 2) + 'px; top:' + (frameAnswer.as[i].h / 2) + 'px;');
            divScrollGroupOption.appendChild(divGroupOption);
            var divDropList = GlobalHelper.createTags('div', '', 'drop-list');
            divDropList.setAttribute('data-model-id', i);
            divDropList.appendChild(divScrollGroupOption);
            divDropList.appendChild(divSelect);
            var divCover = GlobalHelper.createTags('div');
            divCover.setAttribute('style', 'position: absolute; top: 0px; left: 0px;');
            divCover.appendChild(divDropList);
            var divRight = GlobalHelper.createTags('div', '', 'slide-object slide-object-droplist shown');
            divRight.setAttribute('style', 'z-index:' + (noiDungDropDown.length * 2 - i * 2) + ';width:' + frameAnswer.as[i].w + 'px;height:' +
                frameAnswer.as[i].h + 'px;opacity:' + 1 + ';transform-origin:115px 11px;transform:translate(' +
                frameAnswer.as[i].l + 'px,' + frameAnswer.as[i].T + 'px) rotate(0deg) scale(1, 1);');
            divRight.setAttribute('data-type', 'right-answer-question');
            divRight.setAttribute('data-model-id', i);
            divRight.appendChild(divCover);
            divAbsolute.appendChild(divRight);
        }
        return divAbsolute;
    };
    QuestionRankingDropdown.prototype.ProcessClickSubmit = function() {
        if (!this.answerUser()) {
            if (this.DisplayFeedback) {
                ShowFeedback("Invalid Answer", this.Options);
                QuizzModule.SaveIdFeedBack(this.IndexSlide, this.GetIdFeedBack("Invalid Answer"));
            }
        } else {
            if (this.DisplayFeedback) {
                ShowFeedback("Thank you", this.Options);
                QuizzModule.SaveIdFeedBack(this.IndexSlide, this.GetIdFeedBack("Thank you"));
            }
            QuizzModule.ReviewOrSubmit(this.IndexSlide);
        }
        QuizzModule.SaveDataLocalStorage();
    };
    QuestionRankingDropdown.prototype.answerUser = function() {
        var arrAnswerUserSelect = [];
        var idQuestion = '#' + this.idQuestion;
        var answers = $(idQuestion).find('[data-type="right-answer-question"]');
        for (var i = 0; i < answers.length; i++) {
            var currentAnswer = answers[i];
            if ($(currentAnswer).find('.drop-list-top-container').find('.text')[0].attributes['data-id']) {
                var answerUser = $(currentAnswer).find('.drop-list-top-container .text')[0].attributes['data-id'].value;
            } else {
                arrAnswerUserSelect = null;
                return arrAnswerUserSelect;
            }
            arrAnswerUserSelect.push(answerUser);
        }
        return arrAnswerUserSelect;
    };
    return QuestionRankingDropdown;
}(Question));
var QuestionShortAnswer = (function(_super) {
    __extends(QuestionShortAnswer, _super);

    function QuestionShortAnswer(options) {
        var _this = _super.call(this, options) || this;
        _this.idQuestion = _this.CurrentOptions.id;
        _this.TypeInput = 'text';
        _this.clauseReview = [];
        _this.GetData();
        return _this;
    }
    QuestionShortAnswer.prototype.GetData = function() {
        var slide = 'slide' + this.IndexSlide;
        if (GlobalHelper.DataLocal.Slide[slide].UserReply != null) {
            this.UserReply = GlobalHelper.DataLocal.Slide[slide].UserReply;
            this.NumberRepeat = GlobalHelper.DataLocal.Slide[slide].NumberRepeat;
        } else {
            this.UserReply = "";
            this.NumberRepeat = this.Options.atm;
        }
        QuizzModule.SaveReply(this.IndexSlide, this.UserReply);
    };
    QuestionShortAnswer.prototype.callProcess = function() {
        var self = this;
        var id = '#' + self.idQuestion + ' textarea';
        $('#mainContent').on('change', id, function(e) {
            if ($(this).val() != "") {
                var text = $(this).val();
                self.UserReply = text;
                QuizzModule.SaveReply(self.IndexSlide, self.UserReply);
                QuizzModule.SaveAttemps(self.IndexSlide, self.Options.atm);
            }
        });
    };
    QuestionShortAnswer.prototype.CreateElementQuestion = function() {
        var widthFrame = this.CurrentOptions.w,
            heightFrame = this.CurrentOptions.h,
            topFrame = this.CurrentOptions.T,
            leftFrame = this.CurrentOptions.l;
        var divSlideObject = GlobalHelper.createTags('div', '', 'slide-object slide-object-textinput shown answerContainer');
        divSlideObject.setAttribute('style', 'z-index:1;width:' + (widthFrame + 1) + 'px;height:' + (heightFrame + 1) + 'px;opacity:1; overflow: visible;');
        var div = GlobalHelper.createTags('div', this.Options.lts[0].e[1].id);
        div.setAttribute('style', 'height:' + (heightFrame + 1) + 'px; width:' + (widthFrame + 1) + 'px; transform:translate(0px, 0px);');
        var textarea = GlobalHelper.createTags('textarea');
        var fontFamily = this.CurrentOptions.tip.ff,
            fontSize = this.CurrentOptions.tip.fs;
        if (this.CurrentOptions.tip.fg) {
            var fontColor = this.CurrentOptions.tip.fg.col;
        } else {
            var fontColor = 'black';
        }
        textarea.setAttribute('style', 'font-family:' + fontFamily + '; font-size:' + fontSize +
            'px; line-height:normal;font-weight:normal; direction:ltr; text-align:left;color:' + fontColor + '; width:' + (widthFrame - 10) +
            'px; height:' + heightFrame + 'px; padding-top:8px; padding-right:10px; padding-bottom:0; padding-left:10px; resize: none;');
        textarea.setAttribute('type', this.TypeInput);
        if (this.TypeInput === "number") {
            function onlyInputNumber() {
                return GlobalHelper.isNumberKey(event);
            }
            // textarea.setAttribute('onkeypress', "return GlobalHelper.isNumberKey(event); ");
        }
        if (this.CurrentOptions.tip.txt == "") {
            textarea.setAttribute('placeholder', 'type your text here');
        } else {
            textarea.setAttribute('placeholder', this.CurrentOptions.tip.txt);
        }
        if (!QuizzModule.CheckSubmit(this.IndexSlide)) {
            textarea.setAttribute('disabled', "disabled");
        }
        textarea.value = this.UserReply;
        textarea.setAttribute('maxlength', 256);
        div.appendChild(textarea);
        divSlideObject.appendChild(div);
        if (this.IsCheckReview()) {
            if (this.IsCheckReview().IsResult) {
                var divReview = GlobalHelper.createTags('div', '', 'review');
                divReview.setAttribute('style', 'margin-top: 30px;');
                divSlideObject.appendChild(divReview);
                if (GlobalHelper.DataLocal.Slide['slide' + this.IndexSlide].IsShowNoti) {
                    ShowMessage(this.IsCheckReview().Correct);
                }
            }
        }
        this.genHTML = divSlideObject;
        return divSlideObject;
    };
    QuestionShortAnswer.prototype.ProcessClickSubmit = function() {
        var ele = $('#' + this.idQuestion + ' textarea');
        if (ele.val() != "") {
            var text = ele.val();
            this.UserReply = { Text: text };
            if (this.DisplayFeedback) {
                ShowFeedback("Thank you", this.Options);
                QuizzModule.SaveIdFeedBack(this.IndexSlide, this.GetIdFeedBack("Thank you"));
            }
            QuizzModule.ReviewOrSubmit(this.IndexSlide);
        } else {
            if (this.DisplayFeedback) {
                ShowFeedback("Invalid Answer", this.Options);
                QuizzModule.SaveIdFeedBack(this.IndexSlide, this.GetIdFeedBack("Invalid Answer"));
            }
        }
        QuizzModule.SaveDataLocalStorage();
    };
    return QuestionShortAnswer;
}(Question));
var QuestionHowMany = (function(_super) {
    __extends(QuestionHowMany, _super);

    function QuestionHowMany(options) {
        var _this = _super.call(this, options) || this;
        _this.TypeInput = 'number';
        return _this;
    }
    return QuestionHowMany;
}(QuestionShortAnswer));
var QuestionNumberic = (function(_super) {
    __extends(QuestionNumberic, _super);

    function QuestionNumberic(options) {
        return _super.call(this, options) || this;
    }
    QuestionNumberic.prototype.review = function() {
        this.process();
        if(this.Options.nuC.NumericMode == 0){
            textCombine = 'and';
        }else{
            textCombine = 'or';
        }
        if (this.clauseReview.length > 0) {
            var html = '<span style=\"font-family:' + this.CurrentOptions.tip.ff +
                '; font-size:' + 22 + 'px; margin-top:15px;\"' + '>' + 'Correct response' + '</span><br/>' +
                '<ul style=\"font-family:' + this.CurrentOptions.tip.ff +
                '; margin-left: 15px; margin-top: 10px; font-size:' + 20 + 'px;\"' + '><li>+ ' + this.clauseReview[0] + '</li>';
            for (var i = 1; i < this.clauseReview.length; i++) {
                html += '<li>+ '+ textCombine + ' ' + this.clauseReview[i] + '</li>';
            }
            html += '</ul>';
        }
        return html;
    };
    QuestionNumberic.prototype.CreateElementQuestion = function() {
        _super.prototype.CreateElementQuestion.call(this);
        setTimeout(function() {
            $.fn.focusToEnd = function() {
                return this.each(function() {
                    var v = $(this).val();
                    $(this).focus().val("").val(v);
                });
            };

            function char_count(str, letter) {
                var count = 0;
                for (var i = 0; i < str.length; i++) {
                    if (str.charAt(i) === letter) {
                        count += 1;
                    }
                }
                return count;
            }
            $("textarea[type=number]").bind("paste", function(e) {
                var num = e.originalEvent.clipboardData.getData('text');
                return $.isNumeric(num);
            });
            $("textarea[type=number]").keypress(function(e) {
                if (this.selectionStart === 0) {
                    if (e.key === '-') {
                        if (char_count(e.target.value, '-') === 0) {
                            return true;
                        }
                        $(this).focusToEnd();
                        return false;
                    }
                    if (e.key === '.') {
                        if (char_count(e.target.value, '.') === 0 && char_count(e.target.value, '-') === 0) {
                            return true;
                        }
                        $(this).focusToEnd();
                        return false;
                    }
                    if (e.which < 48 || e.which > 57) {
                        $(this).focusToEnd();
                        return false;
                    } else {
                        if (char_count(e.target.value, '-') !== 0 || char_count(e.target.value, '.') !== 0) {
                            $(this).focusToEnd();
                            return false;
                        }
                    }
                } else if (this.selectionEnd === e.target.value.length) {
                    if (e.which < 48 || e.which > 57) {
                        if (e.key === '.') {
                            if (char_count(e.target.value, '.') === 0) {
                                return true;
                            }
                        }
                        return false;
                    }
                } else {
                    if (e.key === '.') {
                        if (char_count(e.target.value, '.') !== 0) {
                            $(this).focusToEnd();
                            return false;
                        }
                    } else if (e.which < 48 || e.which > 57) {
                        $(this).focusToEnd();
                        return false;
                    }
                }
            }).keydown(function(e) {
                switch (event.keyCode) {
                    case 86:
                        if (event.ctrlKey) {
                            var value = $(this).val();
                            console.log(e);
                            return true;
                        }
                        break;
                    case 36:
                        if (event.shiftKey) {
                            return true;
                        }
                    case 8:
                        return true;
                }
            });
        }, 10);
        if (this.IsCheckReview()) {
            if (this.IsCheckReview().IsResult) {
                $(this.genHTML).find('.review')[0].innerHTML = this.review();
                if (GlobalHelper.DataLocal.Slide['slide' + this.IndexSlide].IsShowNoti) {
                    ShowMessage(this.IsCheckReview().Correct);
                }
            }
        }
        return this.genHTML;
    };
    QuestionNumberic.prototype.resultOfQuestion = function() {
        var isAny;
        if (this.Options.nuC.NumericMode === 0) {
            isAny = false;
        } else {
            isAny = true;
        }
        if (isAny) {
            if (this.ArrResult.indexOf(true) > -1) {
                return true;
            } else {
                return false;
            }
        } else {
            if (this.ArrResult.indexOf(false) > -1) {
                return false;
            } else {
                return true;
            }
        }
    };
    QuestionNumberic.prototype.ProcessClickSubmit = function() {
        var ele = $('#' + this.idQuestion + ' textarea');
        if (ele.val() != "") {
            this.process();
            var check = this.resultOfQuestion();
            if (check) {
                if (this.DisplayFeedback) {
                    ShowFeedback("Correct", this.Options);
                    QuizzModule.SaveIdFeedBack(this.IndexSlide, this.GetIdFeedBack("Correct"));
                }
                QuizzModule.SaveDataPoint(this.IndexSlide, GetScoreFeedBack("Correct", this.Options), true);
                QuizzModule.ReviewOrSubmit(this.IndexSlide);
            } else {
                if (this.NumberRepeat == 0) {
                    if (this.DisplayFeedback) {
                        ShowFeedback("Try Again", this.Options);
                        QuizzModule.SaveIdFeedBack(this.IndexSlide, this.GetIdFeedBack("Try Again"));
                    }
                    this.NumberRepeat--;
                } else {
                    if (this.NumberRepeat == 1) {
                        if (this.DisplayFeedback) {
                            ShowFeedback("Incorrect", this.Options);
                            QuizzModule.SaveIdFeedBack(this.IndexSlide, this.GetIdFeedBack("Incorrect"));
                        }
                        QuizzModule.SaveDataPoint(this.IndexSlide, GetScoreFeedBack("Incorrect", this.Options), false);
                        QuizzModule.ReviewOrSubmit(this.IndexSlide);
                    } else {
                        if (this.DisplayFeedback) {
                            ShowFeedback("Try Again", this.Options);
                            QuizzModule.SaveIdFeedBack(this.IndexSlide, this.GetIdFeedBack("Try Again"));
                        }
                        this.NumberRepeat--;
                    }
                }
            }
        } else {
            if (this.DisplayFeedback) {
                ShowFeedback("Invalid Answer", this.Options);
                QuizzModule.SaveIdFeedBack(this.IndexSlide, this.GetIdFeedBack("Invalid Answer"));
            }
        }
        QuizzModule.SaveDataLocalStorage();
    };
    QuestionNumberic.prototype.process = function() {
        this.clauseReview = [];
        this.ArrResult = [];
        var clauses = this.Options.nuC.val;
        for (var i = 0; i < clauses.length; i++) {
            var clause = clauses[i];
            var result = this.processAnswer(clause);
            this.ArrResult.push(result);
        }
    };
    QuestionNumberic.prototype.processAnswer = function(currentClause) {
        var compare = currentClause.Cp;
        var value1 = currentClause.v1;
        var value2 = currentClause.v2;
        var id = "#" + this.CurrentOptions.id;
        var number = this.UserReply;
        var typeCompares = ["Equal to", "Between", "Less than", "Less than or equal to", "Greater than",
            "Greater than or equal to", "Not equal to", "Not between"
        ];
        return this.checkClause(typeCompares[compare], number, value1, value2);
    };
    QuestionNumberic.prototype.checkEqual = function(number, value1) {
        return (number === value1);
    };
    QuestionNumberic.prototype.checkBetween = function(number, value1, value2) {
        return ((number >= value1) && (number <= value2));
    };
    QuestionNumberic.prototype.checkGreaterThan = function(number, value1) {
        return (number > value1);
    };
    QuestionNumberic.prototype.checkLessThan = function(number, value1) {
        return (number < value1);
    };
    QuestionNumberic.prototype.checkClause = function(type, number, value1, value2) {
        var text;
        switch (type) {
            case "Equal to":
                {
                    var check = this.checkEqual(number, value1);
                    text = "is equal to " + value1;
                    this.clauseReview.push(text);
                    break;
                }
            case "Between":
                {
                    var check = this.checkBetween(number, value1, value2);
                    text = "Between " + value1 + " and " + value2;
                    this.clauseReview.push(text);
                    break;
                }
            case "Greater than":
                {
                    var check = this.checkGreaterThan(number, value1);
                    text = "is greater than " + value1;
                    this.clauseReview.push(text);
                    break;
                }
            case "Greater than or equal to":
                {
                    var check = this.checkGreaterThan(number, value1) || this.checkEqual(number, value1);
                    text = "is greater than or equal to " + value1;
                    this.clauseReview.push(text);
                    break;
                }
            case "Less than":
                {
                    var check = this.checkLessThan(number, value1);
                    text = "is less than " + value1;
                    this.clauseReview.push(text);
                    break;
                }
            case "Less than or equal to":
                {
                    var check = this.checkLessThan(number, value1) || this.checkEqual(number, value1);
                    text = "is less than or equal to " + value1;
                    this.clauseReview.push(text);
                    break;
                }
            case "Not equal to":
                {
                    var check = !this.checkEqual(number, value1);
                    text = "is not equal to " + value1;
                    this.clauseReview.push(text);
                    break;
                }
            case "Not between":
                {
                    var check = !this.checkBetween(number, value1, value2);
                    text = "Not between " + value1 + " and " + value2;
                    this.clauseReview.push(text);
                    break;
                }
        }
        return check;
    };
    return QuestionNumberic;
}(QuestionHowMany));
var QuestionSequenceMatching = (function(_super) {
    __extends(QuestionSequenceMatching, _super);

    function QuestionSequenceMatching(options) {
        var _this = _super.call(this, options) || this;
        _this.loaiCauHoi = 'SequenceMatching';
        return _this;
    }
    QuestionSequenceMatching.prototype.review = function() {
        var framech = this.CurrentOptions.as.map(function(x) { return x.ch; });
        var frameAnswer = this.CurrentOptions;
        var multi = 1;
        if (this.IsCheckReview()) {
            if (this.IsCheckReview().IsResult) {
                multi = 0.5;
            }
        }
        var divReview = GlobalHelper.createTags('div', '', 'review');
        for (var i = 0; i < this.CurrentOptions.as.length; i++) {
            var divSlideObjectReview = GlobalHelper.createTags('div', '', 'slide-object slide-object-shufflegroup shown');
            divSlideObjectReview.setAttribute('style', 'z-index:' + framech[i].z + '; left:' + (frameAnswer.w * multi + 10) +
                'px; top: ' + frameAnswer.as[i].T + 'px;');
            var svgReview = GlobalHelper.createTags('svg');
            svgReview.setAttribute('width', framech[i].w / 2);
            svgReview.setAttribute('height', framech[i].h);
            var gReview = GlobalHelper.createTags('g');
            var idTextRight = this.answersRight()[i];
            var textAnswerRight = this.CurrentOptions.as.filter(function(x) { return x.ch.id == idTextRight; })[0];
            var gReview = GlobalHelper.createTags('g');
            var gTextReview = TextModule.generateElementByParas(textAnswerRight.ct.pr);
            gReview.appendChild(gTextReview);
            svgReview.appendChild(gReview);
            divSlideObjectReview.appendChild(svgReview);
            divReview.appendChild(divSlideObjectReview);
        }
        return divReview;
    };
    QuestionSequenceMatching.prototype.ProcessClickSubmit = function() {
        this.mark();
    };
    QuestionSequenceMatching.prototype.answersRight = function() {
        var answers = this.CurrentOptions.as;
        var answersRight = answers.map(function(x) { return x.ch.id; });
        return answersRight;
    };
    QuestionSequenceMatching.prototype.mark = function() {
        var answersUser = this.answerUser();
        var checks = [];
        if (this.answerUser()) {
            var answersRight = this.answersRight();
            for (var i3 = 0; i3 < answersRight.length; i3++) {
                if (answersUser[i3] == answersRight[i3]) {
                    checks.push(true);
                } else {
                    checks.push(false);
                }
            }
            if (checks.indexOf(false) > -1) {
                if (this.NumberRepeat == 0) {
                    if (this.DisplayFeedback) {
                        ShowFeedback("Try Again", this.Options);
                    }
                    QuizzModule.SaveIdFeedBack(this.IndexSlide, this.GetIdFeedBack("Try Again"));
                    this.NumberRepeat--;
                } else {
                    if (this.NumberRepeat == 1) {
                        if (this.DisplayFeedback) {
                            ShowFeedback("Incorrect", this.Options);
                        }
                        QuizzModule.SaveDataPoint(this.IndexSlide, GetScoreFeedBack("Incorrect", this.Options), false);
                        QuizzModule.SaveIdFeedBack(this.IndexSlide, this.GetIdFeedBack("Incorrect"));
                        QuizzModule.ReviewOrSubmit(this.IndexSlide);
                    } else {
                        if (this.DisplayFeedback) {
                            ShowFeedback("Try Again", this.Options);
                        }
                        this.NumberRepeat--;
                    }
                }
            } else {
                if (this.DisplayFeedback) {
                    ShowFeedback("Correct", this.Options);
                }
                QuizzModule.SaveIdFeedBack(this.IndexSlide, this.GetIdFeedBack("Correct"));
                QuizzModule.SaveDataPoint(this.IndexSlide, GetScoreFeedBack("Correct", this.Options), true);
                QuizzModule.ReviewOrSubmit(this.IndexSlide);
            }
        } else {
            ShowFeedback("Invalid Answer", this.Options);
            QuizzModule.SaveIdFeedBack(this.IndexSlide, this.GetIdFeedBack("Invalid Answer"));
        }
        QuizzModule.SaveDataLocalStorage();
    };
    return QuestionSequenceMatching;
}(QuestionRankingDropdown));
var QuestionMatching = (function(_super) {
    __extends(QuestionMatching, _super);

    function QuestionMatching(options) {
        var _this = _super.call(this, options) || this;
        _this.idQuestion = _this.CurrentOptions.id;
        _this.ArrResult = [];
        _this.GetData();
        return _this;
    }
    QuestionMatching.prototype.GetData = function() {
        var slide = 'slide' + this.IndexSlide;
        if (GlobalHelper.DataLocal.Slide[slide].Answers != null) {
            this.ArrAnswer = GlobalHelper.DataLocal.Slide[slide].Answers;
            this.UserReply = GlobalHelper.DataLocal.Slide[slide].UserReply;
            this.NumberRepeat = GlobalHelper.DataLocal.Slide[slide].NumberRepeat;
        } else {
            //this.ArrAnswer = QuizzModule.GetLeftTopMatch(this.CurrentOptions);
            for (var j = 0; j < this.CurrentOptions.as.length; j++) {
                //var currentAnswer = this.CurrentOptions.as[j];
                //var item = { Answers: currentAnswer };
                this.ArrAnswer.push(this.CurrentOptions.as[j]);
            }
            this.ArrAnswer = QuizzModule.RandomAnswer(this.ArrAnswer);
            this.NumberRepeat = this.Options.atm;
        }
        QuizzModule.SaveAnswer(this.IndexSlide, this.ArrAnswer, this.NumberRepeat);
    };
    QuestionMatching.prototype.leftHTML = function(currentAnswer, i) {
        var divLeft = GlobalHelper.createTags("div", currentAnswer.ch.id, 'slide-object object-left left');
        divLeft.setAttribute('style', 'left:' + currentAnswer.ch.l + 'px; top:' +
            currentAnswer.ch.T + "px;");
        divLeft.style.width = currentAnswer.ch.w + "px";
        divLeft.style.height = currentAnswer.ch.h + "px";
        divLeft.setAttribute('data-order', i);
        if (this.UserReply && this.UserReply != []) {
            if (this.UserReply.filter(function(x) { return x.idch == divLeft.attributes['id'].value; }) != []) {
                var idMatchFilter = this.UserReply.filter(function(x) { return x.idch == divLeft.attributes['id'].value; })[0];
                if (idMatchFilter) {
                    divLeft.setAttribute('data-match', idMatchFilter.idMatch);
                }
            }
        }
        var svgLeft = GlobalHelper.createTags("svg");
        svgLeft.setAttribute("width", currentAnswer.ch.w);
        svgLeft.setAttribute("height", currentAnswer.ch.h);
        var fillChoice = FillColorHelper.genGTag(currentAnswer.cC, { "id": currentAnswer.ch.id, "width": currentAnswer.ch.w, "height": currentAnswer.ch.h });
        svgLeft.appendChild(fillChoice.Tag);
        var path = GlobalHelper.createTags("path");
        path.setAttribute("d", currentAnswer.cP);
        path.setAttribute("fill", fillChoice.ID);
        var gText = TextModule.generateElementByParas(currentAnswer.ct.pr);
        gText.setAttribute('transform', 'translate(' + (currentAnswer.ct.l - currentAnswer.ch.l) + ',' + (currentAnswer.ct.T - currentAnswer.ch.T) + ')');
        svgLeft.appendChild(path);
        svgLeft.appendChild(gText);
        if (this.IsCheckReview()) {
            if (this.IsCheckReview().IsResult) {
                var gReview = GlobalHelper.createTags('g');
                gReview.setAttribute('transform', 'translate(' + 0 + ',' + 5 + ')');
                var circle1 = GlobalHelper.createTags('circle');
                circle1.setAttribute('cx', 0);
                circle1.setAttribute('cy', 0);
                circle1.setAttribute('r', 8);
                circle1.setAttribute('fill', 'black');
                circle1.setAttribute('stroke', 'none');
                var circle2 = GlobalHelper.createTags('circle');
                circle2.setAttribute('cx', 0);
                circle2.setAttribute('cy', 0);
                circle2.setAttribute('r', 8);
                circle2.setAttribute('fill', 'transparent');
                circle2.setAttribute('stroke-width', 1);
                if (i === parseInt(this.ArrAnswer[i].Index) && this.ArrAnswer[i].Left != (this.CurrentOptions.as[0].M.l - this.CurrentOptions.l)) {
                    circle2.setAttribute('stroke', '#008408');
                } else {
                    circle2.setAttribute('stroke', '#db0000');
                }
                var text = GlobalHelper.createTags('text');
                text.setAttribute('text-anchor', 'middle');
                text.setAttribute('y', 4);
                var tspan = GlobalHelper.createTags('tspan');
                tspan.setAttribute('fill', 'white');
                tspan.innerHTML = i + 1;
                text.appendChild(tspan);
                gReview.appendChild(circle1);
                gReview.appendChild(circle2);
                gReview.appendChild(text);
                var gLeft = GlobalHelper.createTags('g');
                gLeft.innerHTML = gReview.outerHTML;
                svgLeft.appendChild(gLeft);
                if (GlobalHelper.DataLocal.Slide['slide' + this.IndexSlide].IsShowNoti) {
                    ShowMessage(this.IsCheckReview().Correct);
                }
            }
        }
        divLeft.appendChild(svgLeft);
        return divLeft;
    };
    QuestionMatching.prototype.rightHTML = function(currentAnswer, i) {
        var divRight = GlobalHelper.createTags("div", currentAnswer.M.id, 'slide-object object-right right');
        if (this.UserReply) {
            var idMatches = this.UserReply.map(function(x) {
                return x.idMatch;
            });
            if (idMatches.indexOf(currentAnswer.M.id) > -1) {
                divRight.setAttribute("style", "width:" + currentAnswer.M.w + "px; height:" + currentAnswer.M.h +
                    "px;" + 'left:' + (this.CurrentOptions.as[i].M.l - 37) + 'px; top:' + this.CurrentOptions.as[i].M.T + "px; z-index: 1;");
            } else {
                divRight.setAttribute("style", "width:" + currentAnswer.M.w + "px; height:" + currentAnswer.M.h +
                    "px;" + 'left:' + (this.CurrentOptions.as[i].M.l) + 'px; top:' + this.CurrentOptions.as[i].M.T + "px; z-index: 1;");
            }
        } else {
            divRight.setAttribute("style", "width:" + currentAnswer.M.w + "px; height:" + currentAnswer.M.h +
                "px;" + 'left:' + (this.CurrentOptions.as[i].M.l) + 'px; top:' + this.CurrentOptions.as[i].M.T + "px; z-index: 1;");
        }


        divRight.setAttribute('data-order', this.ArrAnswer[i].Index);
        var svgRight = GlobalHelper.createTags("svg");
        svgRight.setAttribute("width", currentAnswer.M.w);
        svgRight.setAttribute("height", currentAnswer.M.h);
        var fillMatch = FillColorHelper.genGTag(currentAnswer.mC, { "id": currentAnswer.M.id, "width": currentAnswer.M.w, "height": currentAnswer.M.h });
        svgRight.appendChild(fillMatch.Tag);
        var path = GlobalHelper.createTags("path");
        path.setAttribute("d", currentAnswer.mP);
        path.setAttribute("fill", fillMatch.ID);
        var gText = TextModule.generateElementByParas(currentAnswer.mT.pr);
        gText.setAttribute('transform', 'translate(' + (currentAnswer.mT.l - currentAnswer.M.l) + ',' + (currentAnswer.mT.T - currentAnswer.M.T) + ')');
        svgRight.appendChild(path);
        svgRight.appendChild(gText);
        if (this.IsCheckReview()) {
            if (this.IsCheckReview().IsResult) {
                var gReview = GlobalHelper.createTags('g');
                gReview.setAttribute('transform', 'translate(' + (currentAnswer.M.w) + ',' + 5 + ')');
                var circle1 = GlobalHelper.createTags('circle');
                circle1.setAttribute('cx', 0);
                circle1.setAttribute('cy', 0);
                circle1.setAttribute('r', 8);
                circle1.setAttribute('fill', 'transparent');
                circle1.setAttribute('stroke-width', 1);
                var circle2 = GlobalHelper.createTags('circle');
                circle2.setAttribute('cx', 0);
                circle2.setAttribute('cy', 0);
                circle2.setAttribute('r', 7);
                circle2.setAttribute('stroke-width', 1.5);
                if (i === parseInt(this.ArrAnswer[i].Index) && this.ArrAnswer[i].Left != (this.CurrentOptions.as[0].M.l - this.CurrentOptions.l)) {
                    circle1.setAttribute('stroke', '#5cc959');
                    circle2.setAttribute('fill', '#33a833');
                    circle2.setAttribute('stroke', '#008408');
                } else {
                    circle1.setAttribute('stroke', '#fb3b3b');
                    circle2.setAttribute('fill', 'red');
                    circle2.setAttribute('stroke', 'black');
                }
                var text = GlobalHelper.createTags('text');
                text.setAttribute('text-anchor', 'middle');
                text.setAttribute('y', 4);
                var tspan = GlobalHelper.createTags('tspan');
                tspan.setAttribute('fill', 'white');
                tspan.innerHTML = i + 1;
                text.appendChild(tspan);
                gReview.appendChild(circle1);
                gReview.appendChild(circle2);
                gReview.appendChild(text);
                var gRight = GlobalHelper.createTags('g');
                gRight.innerHTML = gReview.outerHTML;
                svgRight.appendChild(gRight);
                if (GlobalHelper.DataLocal.Slide['slide' + this.IndexSlide].IsShowNoti) {
                    ShowMessage(this.IsCheckReview().Correct);
                }
            }
        }
        divRight.appendChild(svgRight);
        return divRight;
    };
    QuestionMatching.prototype.CreateElementQuestion = function() {
        var div = GlobalHelper.createTags('div', '', 'answer-list answerContainer');
        div.setAttribute('style', 'position: absolute; top: 0px; width:' + this.CurrentOptions.w + 'px; height:' +
            this.CurrentOptions.h + 'px; overflow-y: auto; overflow-x: hidden;');
        var currentData;
        var divHTMLNeedAppend;
        var answers = this.ArrAnswer;
        var ans = this.CurrentOptions.as;
        for (var i = 0; i < answers.length; i++) {
            var divAnswer = GlobalHelper.createTags('div');
            divAnswer.setAttribute('style', 'position: absolute; left:' + (ans[i].l) +
                'px;top:' + (ans[i].ch.T) + 'px; width:' +
                ans[i].w + 'px; height:' + ans[i].h + 'px; z-index: 0;');
            var svgAnswer = GlobalHelper.createTags('svg');
            svgAnswer.setAttribute('width', ans[i].w);
            svgAnswer.setAttribute('height', ans[i].h);
            var gShapes = ShapeHelper.generateShapes(ans[i].shs, { "ID": ans[i].id, "Width": ans[i].w, "Height": ans[i].h });
            svgAnswer.appendChild(gShapes);
            divAnswer.appendChild(svgAnswer);
            div.appendChild(divAnswer);
            currentData = answers[i];
            divHTMLNeedAppend = this.leftHTML(this.CurrentOptions.as[i], i);
            div.appendChild(divHTMLNeedAppend);
            divHTMLNeedAppend = this.rightHTML(currentData, i);
            div.appendChild(divHTMLNeedAppend);
        }
        var divTestQuestion = GlobalHelper.createTags('div', this.CurrentOptions.id);
        divTestQuestion.setAttribute('style', "width:" + this.CurrentOptions.w + "px; height:" + this.CurrentOptions.h +
            "px; position: absolute; left:" + this.CurrentOptions.l + "px; top:" + this.CurrentOptions.T + "px;");
        if (QuizzModule.CheckSubmit(this.IndexSlide)) {
            setTimeout(function() {
                QuizzModule.loadHTMLMatchingDropdown();
            }, 10);
        }
        divTestQuestion.appendChild(div);
        return div;
    };
    QuestionMatching.prototype.answersRight = function() {
        var obj = {};
        var answersRight = [];
        var answers = this.CurrentOptions.as;
        for (var i = 0; i < answers.length; i++) {
            obj = {};
            obj["idch"] = answers[i].ch.id;
            obj["idMatch"] = answers[i].M.id;
            answersRight.push(obj);
        }
        return answersRight;
    };
    QuestionMatching.prototype.answersUser = function() {
        var currentElement;
        var elements = $("#" + this.idQuestion + "> div > div[data-match]");
        var object;
        var arrResult = [];
        for (var i = 0; i < elements.length; i++) {
            object = new Object();
            currentElement = elements[i];
            var id1 = currentElement.attributes["id"].value;
            var id2 = currentElement.attributes["data-match"].value;
            object["idch"] = id1;
            object["idMatch"] = id2;
            arrResult.push(object);
        }
        return arrResult;
    };
    QuestionMatching.prototype.check = function() {
        var check = false;
        var answersRight = this.answersRight();
        var answersUser = this.answersUser();
        var idch, idMatch, idMatchRight;
        if (answersUser.length < answersRight.length) {
            ShowFeedback("Invalid Answer", this.Options);
            return null;
        } else {
            for (var i = 0; i < answersUser.length; i++) {
                idch = answersUser[i].idch;
                idMatch = answersUser[i].idMatch;
                idMatchRight = answersRight.filter(function(x) { return x.idch === idch; })[0].idMatch;
                if (idMatch === idMatchRight) {
                    this.ArrResult.push(true);
                } else {
                    this.ArrResult.push(false);
                }
            }
            if (this.ArrResult.indexOf(false) > -1) {
                check = false;
            } else {
                check = true;
            }
        }
        return check;
    };
    QuestionMatching.prototype.ProcessClickSubmit = function() {
        var check = this.check();
        if (check != null) {
            if (check) {
                if (this.DisplayFeedback) {
                    ShowFeedback("Correct", this.Options);
                    QuizzModule.SaveIdFeedBack(this.IndexSlide, this.GetIdFeedBack("Correct"));
                }
                QuizzModule.SaveDataPoint(this.IndexSlide, GetScoreFeedBack("Correct", this.Options), true);
                QuizzModule.ReviewOrSubmit(this.IndexSlide);
            } else {
                if (this.NumberRepeat == 0) {
                    if (this.DisplayFeedback) {
                        ShowFeedback("Try Again", this.Options);
                        QuizzModule.SaveIdFeedBack(this.IndexSlide, this.GetIdFeedBack("Try Again"));
                    }
                } else {
                    if (this.NumberRepeat == 1) {
                        if (this.DisplayFeedback) {
                            ShowFeedback("Incorrect", this.Options);
                            QuizzModule.SaveIdFeedBack(this.IndexSlide, this.GetIdFeedBack("Incorrect"));
                        }
                        QuizzModule.SaveDataPoint(this.IndexSlide, GetScoreFeedBack("Incorrect", this.Options), false);
                        QuizzModule.ReviewOrSubmit(this.IndexSlide);
                    } else {
                        if (this.DisplayFeedback) {
                            ShowFeedback("Try Again", this.Options);
                            QuizzModule.SaveIdFeedBack(this.IndexSlide, this.GetIdFeedBack("Try Again"));
                        }
                        this.NumberRepeat--;
                        QuizzModule.SaveAttemps(this.IndexSlide, this.NumberRepeat);
                    }
                }
            }
        }
        QuizzModule.SaveDataLocalStorage();
    };
    return QuestionMatching;
}(Question));

var QuestionDropDownMatching = (function(_super) {
    __extends(QuestionDropDownMatching, _super);

    function QuestionDropDownMatching(options) {
        var _this = _super.call(this, options) || this;
        _this.dataMatch = null;
        _this.datach = null;
        _this.asRight = _this.answersRight();
        _this.idQuestion = _this.CurrentOptions.id;
        _this.GetData();
        return _this;
    }
    QuestionDropDownMatching.prototype.GetData = function() {
        var slide = 'slide' + this.IndexSlide;
        if (GlobalHelper.DataLocal.Slide[slide].Answers != null) {
            this.ArrAnswer = GlobalHelper.DataLocal.Slide[slide].Answers;
            this.UserReply = GlobalHelper.DataLocal.Slide[slide].UserReply;
            this.NumberRepeat = GlobalHelper.DataLocal.Slide[slide].NumberRepeat;
        } else {
            var answers = this.CurrentOptions.as;
            var idsDropDown = [];
            if (this.Options.sff) {
                for (var i2 = 0; i2 < answers.length; i2++) {
                    idsDropDown.push(QuizzModule.RandomAnswer(answers.map(function(x) { return x.id; })));
                }
            } else {
                for (var i2 = 0; i2 < answers.length; i2++) {
                    idsDropDown.push(answers.map(function(x) { return x.id; }));
                }
            }
            this.UserReply = GlobalHelper.DataLocal.Slide[slide].UserReply;
            this.ArrAnswer = idsDropDown;
            this.NumberRepeat = this.Options.atm;
        }
        QuizzModule.SaveAnswer(this.IndexSlide, this.ArrAnswer, this.NumberRepeat);
    };
    QuestionDropDownMatching.prototype.CreateElementQuestion = function() {
        var _this = this;
        var multi = 1;
        if (this.IsCheckReview()) {
            if (this.IsCheckReview().IsResult) {
                multi = 0.5;
            }
        }
        var answers = this.CurrentOptions.as;
        var datach = answers.map(function(x) { return x.ct; });
        this.datach = datach;
        var dataMatch = answers.map(function(x) { return x.cbT; });
        this.dataMatch = dataMatch;
        var framech = answers.map(function(x) { return x.ch; });
        var frameMatch = answers.map(function(x) { return x.Cb; });
        var choicePaths = answers.map(function(x) { return x.chPath; });
        var frameAnswer = this.CurrentOptions;
        var numSequence = answers.length;
        var divGroup = GlobalHelper.createTags('div', '', 'group');
        var ischeck = true;
        var noiDungDropDown = [];
        var idsDropDown = [];
        var i1 = 0;
        for (var i = 0; i < numSequence; i++) {
            noiDungDropDown = [];
            idsDropDown = [];
            var svg1 = GlobalHelper.createTags('svg', '', 'slide-object slide-object-vectorshape shown');
            svg1.setAttribute('style', 'z-index:' + (2 * numSequence - i * 2 - 1) + ';width:' + answers[i].w +
                'px;height:' + answers[i].h + 'px;opacity:1;transform-origin:' +
                ((framech[i].w - 1) / 2) + 'px ' + ((framech[i].h - 1) / 2) +
                'px;transform:translate(0px, ' +
                answers[i].T + 'px) rotate(0deg) scale(1, 1);');
            svg1.setAttribute('data-type', 'left-answer-question');
            svg1.setAttribute('data-model-id', answers[i].id);
            var g = GlobalHelper.createTags('g');
            var g1 = GlobalHelper.createTags('g', '', 'khung1');
            var path = GlobalHelper.createTags('path');
            path.setAttribute('fill', '#FFFFFF');
            path.setAttribute('fill-opacity', '0');
            path.setAttribute('d', 'M0,0 L' + answers[i].w + ',0 ' + answers[i].h + ',' + answers[i].h + ' 0,' + answers[i].h + 'Z');
            g1.appendChild(path);
            var g2 = TextModule.generateElementByParas(datach[i].pr);
            g.appendChild(g1);
            g.appendChild(g2);
            g2.setAttribute('transform', 'translate(' + (datach[i].l - answers[i].l) + ',' + (datach[i].T - answers[i].T) + ')');
            svg1.appendChild(g);
            divGroup.appendChild(svg1);
            var gChildSelect1;
            var tspan1 = GlobalHelper.createTags('tspan');
            tspan1.setAttribute('x', '12');
            tspan1.setAttribute('y', '30');
            tspan1.setAttribute('is-user-choice', 'no');
            var gSelect = GlobalHelper.createTags('g', '', 'select');
            var rect = GlobalHelper.createTags('rect');
            rect.setAttribute('width', frameMatch[i].w * multi);
            rect.setAttribute('height', frameMatch[i].h);
            rect.setAttribute('rx', 8);
            rect.setAttribute('ry', 8);
            rect.setAttribute('stroke', '#cecece');
            rect.setAttribute('fill', 'white');
            gSelect.appendChild(rect);
            tspan1.textContent = "--Select--";
            var text1 = GlobalHelper.createTags('text');
            text1.setAttribute('font-family', 'Arial');
            text1.setAttribute('font-size', '16px');
            text1.setAttribute('fill', '#4B4B4B');
            text1.setAttribute('fill-opacity', '1');
            text1.appendChild(tspan1);
            if (this.UserReply && this.UserReply[i]) {
                var selectUser = answers.filter(function(x) { return x.id == _this.UserReply[i]; })[0];
                gChildSelect1 = GlobalHelper.createTags('g', '', 'text');
                var gText = TextModule.generateElementByParas(selectUser.cbT.pr);
                gText.setAttribute('transform', 'translate(' + (selectUser.cbT.l - selectUser.Cb.l) + ',' + (selectUser.cbT.T - selectUser.Cb.T) + ')');
                gText.setAttribute('data-id', selectUser.id);
                gChildSelect1.appendChild(gText);
                gSelect.appendChild(gChildSelect1);
            } else {
                gChildSelect1 = GlobalHelper.createTags('g', '', 'text');
                gChildSelect1.appendChild(text1);
                gSelect.appendChild(gChildSelect1);
            }
            var gChildSelect2 = GlobalHelper.createTags('g');
            gChildSelect2.setAttribute('transform', 'translate(' + (frameMatch[i].w * multi - 20) + ', ' + (frameMatch[i].h / 2) + ')');
            var path1 = GlobalHelper.createTags('path');
            path1.setAttribute('d', 'M 0 0 L 10 0 5 5 0 0');
            path1.setAttribute('fill', '#494949');
            path1.setAttribute('stroke', 'rgba(255, 255, 255, 0.8');
            gChildSelect2.appendChild(path1);
            gSelect.appendChild(gChildSelect2);
            var svg1 = GlobalHelper.createTags('svg');
            svg1.setAttribute('width', frameMatch[i].w * multi);
            svg1.setAttribute('height', frameMatch[i].h);
            svg1.setAttribute('style', 'z-index:' + frameMatch[i].z + ';');
            svg1.appendChild(gSelect);
            var divSelect = GlobalHelper.createTags('div', '', 'drop-list-top-container');
            divSelect.setAttribute('data-model-id', i);
            if (QuizzModule.CheckSubmit(this.IndexSlide)) {
                divSelect.addEventListener("click", function(e) {
                    QuizzModule.toggleDropdown(e);
                });
            }
            //divSelect.innerHTML = svg1.outerHTML;
            divSelect.appendChild(svg1);
            divSelect.innerHTML = divSelect.outerHTML;
            var gGroup = GlobalHelper.createTags('g', '', 'drop-list-cover');
            var topTransformOption = 0;
            var g4;
            idsDropDown = this.ArrAnswer[i];
            i1 = 0;
            for (i1 = 0; i1 < idsDropDown.length; i1++) {
                noiDungDropDown.push(this.CurrentOptions.as.filter(function(x) { return x.id == idsDropDown[i1]; })[0].cbT);
            }
            var answersRight = answers.map(function(x) { return x.id; });
            for (var j = 0; j < noiDungDropDown.length; j++) {
                var g3 = TextModule.generateElementByParas(noiDungDropDown[j].pr);
                var rect3 = GlobalHelper.createTags('rect');
                g3.setAttribute('transform', 'translate(' + (dataMatch[i].l - frameMatch[i].l) + ',' + (dataMatch[i].T - frameMatch[i].T) + ')');
                g3.setAttribute('data-id', idsDropDown[j]);
                rect3.setAttribute('width', frameMatch[i].w);
                rect3.setAttribute('height', frameMatch[i].h);
                rect3.setAttribute('class', 'drop-list-item-out');
                rect3.setAttribute('fill', 'white');
                g4 = GlobalHelper.createTags('g', '', 'drop-list-item');
                g4.setAttribute('transform', 'translate(0,' + topTransformOption + ')');
                g4.appendChild(rect3);
                g4.appendChild(g3);
                gGroup.appendChild(g4);
                topTransformOption += frameMatch[i].h;
            }
            var svgGroupOption = GlobalHelper.createTags('svg');
            svgGroupOption.setAttribute('width', frameMatch[i].w);
            svgGroupOption.setAttribute('height', topTransformOption);
            svgGroupOption.setAttribute('z-index', noiDungDropDown.length - i);
            svgGroupOption.appendChild(gGroup);
            var divGroupOption = GlobalHelper.createTags('div', '', 'drop-list-drop-down-inner');
            divGroupOption.appendChild(svgGroupOption);
            var divScrollGroupOption = GlobalHelper.createTags('div', '', 'is-scrollable drop-list-drop-down');
            divScrollGroupOption.setAttribute('style', 'width:' + (frameMatch[i].w - 2) + 'px;border-color:#cecece;display:none;border-radius:16px;padding-top:' + frameMatch[i].h / 2 + 'px; ' + 'top:' + frameMatch[i].h / 2 + 'px;');
            divScrollGroupOption.appendChild(divGroupOption);
            var divDropList = GlobalHelper.createTags('div', '', 'drop-list');
            divDropList.setAttribute('data-model-id', i);
            divDropList.appendChild(divScrollGroupOption);
            divDropList.appendChild(divSelect);
            var divCover = GlobalHelper.createTags('div');
            divCover.setAttribute('style', 'position: absolute; top: 0px; left: 0px;');
            divCover.appendChild(divDropList);
            var divRight = GlobalHelper.createTags('div', '', 'slide-object slide-object-droplist shown');
            divRight.setAttribute('style', 'z-index:' + (noiDungDropDown.length * 2 - i * 2) + ';width:' + (frameMatch[i].w * multi) + 'px;height:' +
                frameMatch[i].h + 'px;opacity:' + 1 + ';transform-origin:115px 11px;transform:translate(' +
                frameMatch[i].l + 'px,' + frameMatch[i].T + 'px) rotate(0deg) scale(1, 1);');
            divRight.setAttribute('data-type', 'right-answer-question');
            divRight.setAttribute('data-model-id', i);
            divRight.appendChild(divCover);
            divGroup.appendChild(divRight);
        }
        var divSlideObject = GlobalHelper.createTags('div', '', 'slide-object slide-object-shufflegroup shown');
        divSlideObject.appendChild(divGroup);
        var divAbsolute = GlobalHelper.createTags('div', this.idQuestion, 'scroll-answers answerContainer');
        divAbsolute.setAttribute('style', 'height: ' + frameAnswer.h + 'px; width:' + frameAnswer.w + 'px; position: absolute; overflow-y: auto; left: 0px; top: 0px; overflow-x: hidden;');
        divAbsolute.setAttribute('data-type', 'question');
        divAbsolute.setAttribute('data-type-question', 'matchingDropdown');
        divAbsolute.appendChild(divSlideObject);
        if (this.IsCheckReview(this.IndexSlide).IsResult) {
            for (i = 0; i < numSequence; i++) {
                var divSlideObjectReview = GlobalHelper.createTags('div', '', 'slide-object slide-object-shufflegroup shown');
                var svgReview = GlobalHelper.createTags('svg');
                svgReview.setAttribute('width', frameMatch[i].w);
                svgReview.setAttribute('height', frameMatch[i].h);
                svgReview.setAttribute('style', 'z-index:' + frameMatch[i].z + ';');
                svgReview.setAttribute('transform', 'translate(' + (frameMatch[i].w * multi + frameMatch[i].l + 20) + ',' + (frameMatch[i].T) + ')');
                var gReview = GlobalHelper.createTags('g');
                var idTextRight = this.asRight[i];
                var textAnswerRight = this.CurrentOptions.as.filter(function(x) { return x.id == idTextRight; })[0];
                var gReview = GlobalHelper.createTags('g');
                var gTextReview = TextModule.generateElementByParas(textAnswerRight.cbT.pr);
                gReview.appendChild(gTextReview);
                svgReview.appendChild(gReview);
                divSlideObjectReview.appendChild(svgReview);
                divAbsolute.appendChild(divSlideObjectReview);
            }
            if (GlobalHelper.DataLocal.Slide['slide' + this.IndexSlide].IsShowNoti) {
                ShowMessage(this.IsCheckReview().Correct);
            }
        }
        return divAbsolute;
    };
    // QuestionDropDownMatching.prototype.callProcess = function () {
    //     $(document).ready(function () {
    //         $(".drop-list .drop-list-top-container").click(function () {
    //             event.stopPropagation();
    //             var currentElement = $(this).parents().closest('div[data-type=question]');
    //             if (currentElement) {
    //                 currentElement.find('.drop-list-drop-down').hide();
    //             }
    //             var el = $(this).parents().closest('div.drop-list');
    //             if (el) {
    //                 $(el).find('.drop-list-drop-down').toggle();
    //             }
    //         });
    //         $(document).click(function () {
    //             $(".drop-list-drop-down").hide();
    //         });
    //         $(".drop-list-item").hover(function () {
    //             $(this).find('rect').attr("fill", "yellow");
    //         }, function () {
    //             $(this).find('rect').attr("fill", "white");
    //         });
    //         $(".drop-list-item").click(function () {
    //             var value = $(this).find('text tspan').text();
    //             var dataId = $(this).find('text tspan')[0].attributes["data-id"].value;
    //             var closestDropList = $(this).parents().closest('div .drop-list');
    //             var selectOfDropDown = closestDropList.find('.drop-list-top-container text tspan');
    //             selectOfDropDown.text(value);
    //             selectOfDropDown.attr('data-id', dataId);
    //             $(".drop-list-drop-down").hide();
    //         });
    //     });
    // };
    QuestionDropDownMatching.prototype.ProcessClickSubmit = function() {
        this.mark();
    };
    QuestionDropDownMatching.prototype.answersRight = function() {
        var answers = this.CurrentOptions.as;
        var answersRight = answers.map(function(x) { return x.id; });
        return answersRight;
    };
    QuestionDropDownMatching.prototype.answerUser = function() {
        var arrAnswerUserSelect = [];
        var idQuestion = '#' + this.idQuestion;
        var answers = $(idQuestion).find('[data-type="right-answer-question"]');
        for (var i = 0; i < answers.length; i++) {
            var currentAnswer = answers[i];
            if ($(currentAnswer).find('.drop-list-top-container .text')[0]) {
                var answerUser = $(currentAnswer).find('.drop-list-top-container .text')[0].attributes["data-id"].value;
                arrAnswerUserSelect.push(answerUser);
                if (i == answers.length - 1) {
                    return arrAnswerUserSelect;
                }
            }
        }
        return null;
    };
    QuestionDropDownMatching.prototype.mark = function() {
        var answersUser = this.answerUser();
        var checks = [];
        if (answersUser) {
            var answersRight = this.answersRight();
            for (var i3 = 0; i3 < answersRight.length; i3++) {
                if (answersUser[i3] == answersRight[i3]) {
                    checks.push(true);
                } else {
                    checks.push(false);
                }
            }
            if (checks.indexOf(false) > -1) {
                if (this.NumberRepeat == 0) {
                    if (this.DisplayFeedback) {
                        ShowFeedback("Try Again", this.Options);
                        QuizzModule.SaveIdFeedBack(this.IndexSlide, this.GetIdFeedBack("Try Again"));
                    }
                } else {
                    if (this.NumberRepeat == 1) {
                        if (this.DisplayFeedback) {
                            ShowFeedback("Incorrect", this.Options);
                            QuizzModule.SaveIdFeedBack(this.IndexSlide, this.GetIdFeedBack("Incorrect"));
                        }
                        QuizzModule.SaveDataPoint(this.IndexSlide, GetScoreFeedBack("Incorrect", this.Options), false);
                        QuizzModule.ReviewOrSubmit(this.IndexSlide);
                    } else {
                        if (this.DisplayFeedback) {
                            ShowFeedback("Try Again", this.Options);
                            QuizzModule.SaveIdFeedBack(this.IndexSlide, this.GetIdFeedBack("Try Again"));
                        }
                        this.NumberRepeat--;
                        QuizzModule.SaveAttemps(this.IndexSlide, this.NumberRepeat);
                    }
                }
            } else {
                if (this.DisplayFeedback) {
                    ShowFeedback("Correct", this.Options);
                    QuizzModule.SaveIdFeedBack(this.IndexSlide, this.GetIdFeedBack("Correct"));
                }
                QuizzModule.SaveDataPoint(this.IndexSlide, GetScoreFeedBack("Correct", this.Options), true);
                QuizzModule.ReviewOrSubmit(this.IndexSlide);
            }
        } else {
            if (this.DisplayFeedback) {
                ShowFeedback("Invalid Answer", this.Options);
                QuizzModule.SaveIdFeedBack(this.IndexSlide, this.GetIdFeedBack("Invalid Answer"));
            }
        }
        QuizzModule.SaveDataLocalStorage();
    };
    return QuestionDropDownMatching;
}(Question));
GlobalResources.addModule((function() {
    return {
        Level: GlobalEnum.ModuleLevel.Element,
        ContentType: "Answers",
        generateElement: function(elementData, slideDataOwner) {
            var elem = QuizzModule.GetElement(slideDataOwner);
            return elem;
        }
    };
})());