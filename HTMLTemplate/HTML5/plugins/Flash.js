var Flash = (function () {
    function Flash(options) {
        this.options = options;
    }
    Flash.prototype.genFlash = function () {
        var currentFlashObj = this.options;
        var divFlash = GlobalHelper.createTags('div', '', '');
        var type = 'application/x-shockwave-flash';
        var mimeTypes = navigator.mimeTypes;
        if (mimeTypes && mimeTypes[type]) {
            var objectFlash = document.createElement('object');
            objectFlash.width = currentFlashObj.w;
            objectFlash.height = currentFlashObj.h;
            objectFlash.style.position = 'absolute';
            objectFlash.style.top = '0px';
            objectFlash.style.left = '0px';
            var embedFlash = document.createElement('embed');
            embedFlash.src = currentFlashObj.fU;
            embedFlash.width = currentFlashObj.w;
            embedFlash.height = currentFlashObj.h;
            embedFlash.setAttribute('quality', 'high');
            embedFlash.setAttribute('type', 'application/x-shockwave-flash');
            embedFlash.setAttribute('wmode', 'transparent');
            objectFlash.appendChild(embedFlash);
            GlobalHelper.appendIdContent(divFlash, objectFlash);
        }
        else {
            var notSupport = document.createElement('div');
            notSupport.className = 'notSupportFlash';
            notSupport.style.background = '#ccc';
            notSupport.style.border = '1px solid #666';
            notSupport.style.padding = '10px';
            notSupport.innerHTML = 'swf';
            GlobalHelper.appendIdContent(divFlash, notSupport);
        }
        return divFlash;
    };
    return Flash;
}());
GlobalResources.addModule((function () {
    return {
        Level: GlobalEnum.ModuleLevel.Element,
        ContentType: "Flash",
        generateElement: function (elementData, slideDataOwner) {
            var divFlash = new Flash(elementData);
            return divFlash.genFlash();
        }
    };
})());
