"use strict";
var listTriggerEvent = [];
// Hàm gọi tất cả event
function CallEvent() {
    var DELAY = 300,
        clicks = 0,
        timer = null;
    $(function() {
        $("#content").on("click", ".triggerEvent", function(e) {
                let elementID = this.id;
                let elementClick = this;

                clicks++; //count clicks
                if (clicks === 1) {
                    timer = setTimeout(function() {
                        // Kiểm tra nếu như có class đang tìm 
                        // thì sẽ tìm sự kiện và đối tượng bị ảnh hưởng được luus trong danh sachs event
                        if ($(elementClick).attr("class").replace(/[\n\t]/g, " ").indexOf(" " + elementID + "_" + "UserClicks ") > -1) {
                            TriggerActionModule.setTriggerAction(elementID, "UserClicks");
                        }
                        //alert("Single Click"); //perform single-click action
                        clicks = 0; //after action performed, reset counter

                    }, DELAY);

                } else {
                    clearTimeout(timer); //prevent single-click action
                    //alert("Double Click"); //perform double-click action
                    if ($(this).attr("class").replace(/[\n\t]/g, " ").indexOf(" " + elementID + "_" + "UserDoubleClicks ") > -1) {
                        TriggerActionModule.setTriggerAction(elementID, "UserDoubleClicks");
                    }
                    clicks = 0; //after action performed, reset counter
                }

            })
            .on("dblclick", ".triggerEvent", function(e) {
                e.preventDefault(); //cancel system double-click event
            });
    });
    /*==== END Xử lý Trigger EVENT Click & DoubleClick ====*/

    /*==== BEGIN Xử lý Trigger EVENT ClickOutSide ====*/
    $("#content").on("click", function(event) {
        var element = $(".ClickOutside");
        //let eventData = listTriggerEvent.filter(x => x.EventKey === "UserClicksOutside");
        let eventData = listTriggerEvent.filter(function(x) {
            if (x.EventKey == "UserClicksOutside") {
                return x;
            }
        });
        eventData.forEach(function(item, e) {
            let objectElementID = item.Data.tD.osI;
            if (objectElementID.length > 0) {
                for (var i = 0; i < objectElementID.length; i++) {
                    var container = $("#" + objectElementID[i]);
                    //if the target of the click isn't the container nor a descendant of the container
                    if (!container.is(event.target) && container.has(event.target).length === 0) {
                        TriggerActionModule.getTriggerAction(item.Data);
                    }
                    break;
                }
            }
        });
    });
    /*==== END Xử lý Trigger EVENT ClickOutSide ====*/

    $("#content").on("click", ".closeEvent", function(e) {
        e.stopPropagation();
    });

    /*==== BEGIN Xử lý Trigger EVENT RightClick ====*/
    $("#content").on("contextmenu", ".triggerEvent", function() {
        let elementID = this.id;
        let elementRightClick = this;
        /*
        Kiểm tra nếu như có class đang tìm 
        thì sẽ tìm sự kiện và đối tượng bị ảnh hưởng được lưu trong danh sách event
        */
        //if ($(elementRightClick).hasClass(elementID + "_" + "UserRightClicks"))
        if ($(elementRightClick).attr("class").replace(/[\n\t]/g, " ").indexOf(" " + elementID + "_" + "UserRightClicks ") > -1) {
            TriggerActionModule.setTriggerAction(elementID, "UserRightClicks");
        } else {
            return;
        }
    });
    /*==== END Xử lý Trigger EVENT RightClick ====*/

    /*==== BEGIN Xử lý Event Presses a key ====*/
    $("#content").on("keydown", ".triggerEvent", function(e) {
        let elementID = this.id;
        let elementKeypress = this;
        e = e || window.event;
        /*Kiểm tra nếu như có class đang tìm 
        thì sẽ tìm sự kiện và đối tượng bị ảnh hưởng được lưu trong danh sách event
        */
        if ($(elementKeypress).hasClass(elementID + "_" + "UserPressesAKey")) {
            //let eventData = listTriggerEvent.filter(x => x.EventKey === elementID + "_UserPressesAKey");
            var eventData = listTriggerEvent.filter(function(x) {
                if (x.EventKey == elementID + "_UserPressesAKey") {
                    return x;
                }
            });
            if (eventData.length > 0) {
                for (var i = 0; i < eventData.length; i++) {
                    // var charKey = e.charCode || e.keyCode;
                    var charKey = e.which || e.keyCode,
                        stringKey = String.fromCharCode(charKey),
                        upperKey = stringKey.toUpperCase(),
                        stringKey1 = null;
                    switch (charKey) {
                        case 8:
                            stringKey1 = "BackSpace";
                            break;
                        case 9:
                            stringKey1 = "Tab";
                            break;
                        case 16:
                            stringKey1 = "!";
                            break;
                        case 19:
                            stringKey1 = "Pause";
                            break;
                        case 20:
                            stringKey1 = "Capital";
                            break;
                        case 13:
                            stringKey1 = "Enter";
                            break;
                        case 27:
                            stringKey1 = "Esc";
                            break;
                        case 32:
                            stringKey1 = "Space";
                            break;
                        case 33:
                            stringKey1 = "PageUp";
                            break;
                        case 34:
                            stringKey1 = "PageDown";
                            break;
                        case 35:
                            stringKey1 = "End";
                            break;
                        case 36:
                            stringKey1 = "Home";
                            break;
                        case 37:
                            stringKey1 = "Left";
                            break;
                        case 38:
                            stringKey1 = "Up";
                            break;
                        case 39:
                            stringKey1 = "Right";
                            break;
                        case 40:
                            stringKey1 = "Down";
                            break;
                        case 45:
                            stringKey1 = "Insert";
                            break;
                        case 46:
                            stringKey1 = "Delete";
                            break;
                            //Begin number in NumLock
                        case 96:
                            stringKey1 = "NumPad0";
                            break;
                        case 97:
                            stringKey1 = "NumPad1";
                            break;
                        case 98:
                            stringKey1 = "NumPad2";
                            break;
                        case 99:
                            stringKey1 = "NumPad3";
                            break;
                        case 100:
                            stringKey1 = "NumPad4";
                            break;
                        case 101:
                            stringKey1 = "NumPad5";
                            break;
                        case 102:
                            stringKey1 = "NumPad6";
                            break;
                        case 103:
                            stringKey1 = "NumPad7";
                            break;
                        case 104:
                            stringKey1 = "NumPad8";
                            break;
                        case 105:
                            stringKey1 = "NumPad9";
                            break;
                        case 106:
                            stringKey1 = "Multiply";
                            break;
                        case 107:
                            stringKey1 = "OemPlus";
                            break;
                        case 109:
                            stringKey1 = "Subtract";
                            break;
                        case 110:
                            stringKey1 = "Decimal";
                            break;
                        case 111:
                            stringKey1 = "Divide";
                            break;
                            //End number in NumLock
                        case 112:
                            stringKey1 = "F1";
                            break;
                        case 113:
                            stringKey1 = "F2";
                            break;
                        case 114:
                            stringKey1 = "F3";
                            break;
                        case 115:
                            stringKey1 = "F4";
                            break;
                        case 116:
                            stringKey1 = "F5";
                            break;
                        case 117:
                            stringKey1 = "F6";
                            break;
                        case 118:
                            stringKey1 = "F7";
                            break;
                        case 119:
                            stringKey1 = "F8";
                            break;
                        case 120:
                            stringKey1 = "F9";
                            break;
                        case 121:
                            stringKey1 = "F10";
                            break;
                        case 122:
                            stringKey1 = "F11";
                            break;
                        case 123:
                            stringKey1 = "F12";
                            break;
                        case 144:
                            stringKey1 = "NumLock";
                            break;
                        case 145:
                            stringKey1 = "Scroll";
                            break;
                        case 186:
                            stringKey1 = "Oem1";
                            break;
                        case 188:
                            stringKey1 = "OemComma";
                            break;
                        case 190:
                            stringKey1 = "Decimal";
                            break;
                        case 191:
                            stringKey1 = "Divide" || "OemQuestion";
                            break;
                        case 192:
                            stringKey1 = "OemTilde";
                            break;
                        case 219:
                            stringKey1 = "OemOpenBrackets";
                            break;
                        case 221:
                            stringKey1 = "Oem6"; //[
                            break;
                        case 222:
                            stringKey1 = "OemOpenBrackets";
                            break;
                    }
                    let upperKey1 = stringKey1;

                    if (upperKey == eventData[i].Data.tD.kS.K &&
                        eventData[i].Data.tD.kS.iC == e.ctrlKey &&
                        eventData[i].Data.tD.kS.iS == e.shiftKey &&
                        eventData[i].Data.tD.kS.iA == e.altKey) {
                        TriggerActionModule.getTriggerAction(eventData[i].Data);
                    } else if (stringKey == eventData[i].Data.tD.kS.K) {
                        TriggerActionModule.getTriggerAction(eventData[i].Data);
                    } else if (upperKey1 == eventData[i].Data.tD.kS.K) {
                        TriggerActionModule.getTriggerAction(eventData[i].Data);
                    }
                    e.preventDefault();
                }
            }
        } else {
            return;
        }
    });
    /*==== END Xử lý Event Presses a key ====*/

    /*==== BEGIN Xử lý Event Loses focus ====*/
    $("#content").on("focusout", ".triggerEvent", function() {
        let elementID = this.id;
        let elementFocus = this;
        /*
        Kiểm tra nếu như có class đang tìm 
        thì sẽ tìm sự kiện và đối tượng bị ảnh hưởng được lưu trong danh sách event
        */
        if ($(elementFocus).attr("class").replace(/[\n\t]/g, " ").indexOf(" " + elementID + "_" + "ControlLosesFocus ") > -1) {
            TriggerActionModule.setTriggerAction(elementID, "ControlLosesFocus");
        } else {
            return;
        }
    });
    /*==== END Xử lý Event Loses focus ====*/

    $("#mainContent").on("contextmenu", function(e) {
        e.preventDefault();
    });

    // $(document).on("keydown", function(e) {
    //     if (e.keyCode == 123) { // Prevent F12
    //         return false;
    //     } else if (e.ctrlKey && e.shiftKey && e.keyCode == 73) { // Prevent Ctrl+Shift+I        
    //         return false;
    //     }
    // });
}
CallEvent();

function customEventMouse() {
    $("#content").on("click", ".triggerEvent", function(e) {
        e.stopPropagation()
    });
}