"use strict";
var TransitionModule = (function() {
    return {
        setTransition: function(slideData, transitionData, onComplete) {
            if (slideData.trans != null) {
                this.getTransition(slideData, transitionData, onComplete);
            } else {
                onComplete();
            }
        },
        getTransition: function(slideData, transitionData, onComplete) {
            switch (transitionData.nm) {
                case TransitionName.Fade:
                    getFadeTransition(transitionData, slideData, onComplete);
                    break;
                case TransitionName.Push:
                    getPushTransition(transitionData, slideData, onComplete);
                    break;
                case TransitionName.Split:
                    getSplitTransition(transitionData, slideData, onComplete);
                    break;
                case TransitionName.RandomBars:
                    getRandomBarsTransition(transitionData, slideData, onComplete);
                    break;
                case TransitionName.Circle:
                    getCircleTransition(transitionData, slideData, onComplete);
                    break;
                case TransitionName.Diamond:
                    getDiamondTransition(transitionData, slideData, onComplete)
                    break;
                case TransitionName.Plus:
                    getPlusTransition(transitionData, slideData, onComplete);
                    break;
                case TransitionName.In:
                    getInTransition(transitionData, slideData, onComplete);
                    break;
                case TransitionName.Out:
                    getOutTransition(transitionData, slideData, onComplete);
                    break;
                case TransitionName.Uncover:
                    getUnCoverTransition(transitionData, slideData, onComplete);
                    break;
                case TransitionName.Cover:
                    getCoverTransition(transitionData, slideData, onComplete);
                    break;
                case TransitionName.Newsflash:
                    getNewsflashTransition(transitionData, slideData, onComplete);
                    break;
                case TransitionName.Dissolve:
                    getDissolveTransition(transitionData, slideData, onComplete);
                    break;
                case TransitionName.Checkerboard:
                    getCheckerboadTransition(transitionData, slideData, onComplete);
                    break;
                case TransitionName.Blinds:
                    getBlindsTransition(transitionData, slideData, onComplete);
                    break;
                case TransitionName.Clock:
                    getClockTransition(transitionData, slideData, onComplete);
                    break;
                case TransitionName.Zoom:
                    getZoomTransition(transitionData, slideData, onComplete);
                    break;
                default:
                    onComplete();
                    break;
            }
        }
    }
    //Hiệu ứng Transition: Fade
    function getFadeTransition(transitionData, slideData, onComplete) {
        let fromValue = null,
            toValue = null,
            entrance = null,
            // currentSlide = GlobalResources.CurrentSlideContainer; //<div> DOM
            // previousSlide = GlobalResources.PreviousSlide;
            group = transitionData.effOpt;
        switch (group) {
            case "Smoothly":
                fromValue = { opacity: 0 };
                toValue = { opacity: 1, onComplete: onComplete };
                entrance = TweenLite.fromTo("#" + slideData.id, transitionData.durTra, fromValue, toValue);
                break;
            case "Through Back":
                fromValue = { backgroundColor: "black", opacity: 0.5 };
                toValue = { backgroundColor: "none", opacity: 1, onComplete: onComplete };
                entrance = TweenLite.fromTo("#" + slideData.id, transitionData.durTra, fromValue, toValue);
                break;
        }
    }

    //Hiệu ứng Transition: Push
    function getPushTransition(transitionData, slideData, onComplete) {
        let fromValue = null,
            toValue = null,
            entrance = null,
            group = transitionData.effOpt;
        switch (group) {
            case "From Top":
                fromValue = { y: -slideData.h };
                toValue = { y: 0, onComplete: onComplete };
                entrance = TweenLite.fromTo("#" + slideData.id, transitionData.durTra, fromValue, toValue);
                break;
            case "From Bottom":
                fromValue = { y: slideData.h };
                toValue = { y: 0, onComplete: onComplete };
                entrance = TweenLite.fromTo("#" + slideData.id, transitionData.durTra, fromValue, toValue);
                break;
            case "From Right":
                fromValue = { x: slideData.w };
                toValue = { x: 0, onComplete: onComplete };
                entrance = TweenLite.fromTo("#" + slideData.id, transitionData.durTra, fromValue, toValue);
                break;
            case "From Left":
                fromValue = { x: -slideData.w };
                toValue = { x: 0, onComplete: onComplete };
                entrance = TweenLite.fromTo("#" + slideData.id, transitionData.durTra, fromValue, toValue);
                break;
        }
    }

    //Hiệu ứng Transition: Split
    function getSplitTransition(transitionData, slideData, onComplete) {
        let transition = SplitTransition(transitionData, slideData);
        let value = { percent: 100, onComplete: onComplete };
        let entrance = TweenLite.to(transition, transitionData.durTra, value);
    }

    //Hiệu ứng Transition: Circle
    function getCircleTransition(transitionData, slideData, onComplete) {
        let transition = CircleTransition(transitionData, slideData);
        let value = { percent: 100, onComplete: onComplete };
        let entrance = TweenLite.to(transition, transitionData.durTra, value);
    }

    //Hiệu ứng Transition: Diamond
    function getDiamondTransition(transitionData, slideData, onComplete) {
        let transition = DiamondTransition(transitionData, slideData);
        let value = { percent: 100, onComplete: onComplete };
        let entrance = TweenLite.to(transition, transitionData.durTra, value);
    }

    //Hiệu ứng Transition: Plus
    function getPlusTransition(transitionData, slideData, onComplete) {
        let transition = PlusTransition(transitionData, slideData);
        let value = { percent: 100, onComplete: onComplete };
        let entrance = TweenLite.to(transition, transitionData.durTra, value);
    }

    //Hiệu ứng Transition: In
    function getInTransition(transitionData, slideData, onComplete) {
        let transition = InTransition(transitionData, slideData);
        let value = { percent: 100, onComplete: onComplete };
        let entrance = TweenLite.to(transition, transitionData.durTra, value);
    }

    //Hiệu ứng Transition: Out
    function getOutTransition(transitionData, slideData, onComplete) {
        let fromValue = { scaleX: 0, scaleY: 0 };
        let toValue = { scaleX: 1, scaleY: 1, onComplete: onComplete };
        let entrance = TweenLite.fromTo("#" + slideData.id, transitionData.durTra, fromValue, toValue);
    }

    //Hiệu ứng Transition: Dissolve
    function getDissolveTransition(transitionData, slideData, onComplete) {
        let transition = DissolveTransition(transitionData, slideData);
        let value = { percent: 100, onComplete: onComplete };
        let entrance = TweenLite.to(transition, transitionData.durTra, value);
    }

    //Hiệu ứng Transition: Checkerboad
    function getCheckerboadTransition(transitionData, slideData, onComplete) {
        let transition = CheckerboadTransition(transitionData, slideData);
        let value = { percent: 100, onComplete: onComplete };
        let entrance = TweenLite.to(transition, transitionData.durTra, value);
    }

    //Hiệu ứng Transition: Blinds
    function getBlindsTransition(transitionData, slideData, onComplete) {
        let transition = BlindsTransition(transitionData, slideData);
        let value = { percent: 100, onComplete: onComplete };
        let entrance = TweenLite.to(transition, transitionData.durTra, value);
    }

    //Hiệu ứng Transition: Clock
    function getClockTransition(transitionData, slideData, onComplete) {
        let transition = ClockTransition(transitionData, slideData);
        let value = { angle: 360, onComplete: onComplete };
        let entrance = TweenLite.to(transition, transitionData.durTra, value);
    }

    //Hiệu ứng Transition: UnCover
    function getUnCoverTransition(transitionData, slideData, onComplete) {
        let fromValue = null,
            toValue = null,
            entrance = null,
            previousSlide = null, // transition này chỉ chạy cho previousSlide khi cấu hình cho currentSlide
            group = transitionData.effOpt;

        if (GlobalResources.PreviousSlide != undefined) {
            previousSlide = GlobalResources.PreviousSlide.id;
        } else if (GlobalResources.PreviousSlide == undefined) {
            return;
        }
        $("#content").css("position", "relative");
        $("#" + previousSlide).css("z-index", "10");
        
        switch (group) {
            case "From Top":
                fromValue = { y: 0 };
                toValue = { y: slideData.h, onComplete: onComplete };
                entrance = TweenLite.fromTo("#" + previousSlide, transitionData.durTra, fromValue, toValue);
                break;
            case "From Bottom":
                fromValue = { y: 0 };
                toValue = { y: -slideData.h, onComplete: onComplete };
                entrance = TweenLite.fromTo("#" + previousSlide, transitionData.durTra, fromValue, toValue);
                break;
            case "From Right":
                fromValue = { x: 0 };
                toValue = { x: -slideData.w, onComplete: onComplete };
                entrance = TweenLite.fromTo("#" + previousSlide, transitionData.durTra, fromValue, toValue);
                break;
            case "From Left":
                fromValue = { x: 0 };
                toValue = { x: slideData.w, onComplete: onComplete };
                entrance = TweenLite.fromTo("#" + previousSlide, transitionData.durTra, fromValue, toValue);
                break;
            case "From Top-Right":
                fromValue = { x: 0, y: 0 };
                toValue = { x: -slideData.w, y: slideData.h, onComplete: onComplete };
                entrance = TweenLite.fromTo("#" + previousSlide, transitionData.durTra, fromValue, toValue);
                break;
            case "From Top-Left":
                fromValue = { x: 0, y: 0 };
                toValue = { x: slideData.w, y: slideData.h, onComplete: onComplete };
                entrance = TweenLite.fromTo("#" + previousSlide, transitionData.durTra, fromValue, toValue);
                break;
            case "From Bottom-Right":
                fromValue = { x: 0, y: 0 };
                toValue = { x: -slideData.w, y: -slideData.h, onComplete: onComplete };
                entrance = TweenLite.fromTo("#" + previousSlide, transitionData.durTra, fromValue, toValue);
                break;
            case "From Bottom-Left":
                fromValue = { x: 0, y: 0 };
                toValue = { x: slideData.w, y: -slideData.h, onComplete: onComplete };
                entrance = TweenLite.fromTo("#" + previousSlide, transitionData.durTra, fromValue, toValue);
                break;
        }
    }

    //Hiệu ứng Transition: Cover
    function getCoverTransition(transitionData, slideData, onComplete) {
        let fromValue = null,
            toValue = null,
            entrance = null,
            // let currentSlide = GlobalResources.CurrentSlideContainer; //<div> DOM
            // let previousSlide = GlobalResources.PreviousSlide;
            group = transitionData.effOpt;
        switch (group) {
            case "From Top":
                fromValue = { y: -slideData.h };
                toValue = { y: 0, onComplete: onComplete };
                entrance = TweenLite.fromTo("#" + slideData.id, transitionData.durTra, fromValue, toValue);
                break;
            case "From Bottom":
                fromValue = { y: slideData.h };
                toValue = { y: 0, onComplete: onComplete };
                entrance = TweenLite.fromTo("#" + slideData.id, transitionData.durTra, fromValue, toValue);
                break;
            case "From Right":
                fromValue = { x: slideData.w };
                toValue = { x: 0, onComplete: onComplete };
                entrance = TweenLite.fromTo("#" + slideData.id, transitionData.durTra, fromValue, toValue);
                break;
            case "From Left":
                fromValue = { x: -slideData.w };
                toValue = { x: 0, onComplete: onComplete };
                entrance = TweenLite.fromTo("#" + slideData.id, transitionData.durTra, fromValue, toValue);
                break;
            case "From Top-Right":
                fromValue = { x: slideData.w, y: -slideData.h };
                toValue = { x: 0, y: 0, onComplete: onComplete };
                entrance = TweenLite.fromTo("#" + slideData.id, transitionData.durTra, fromValue, toValue);
                break;
            case "From Top-Left":
                fromValue = { x: -slideData.w, y: -slideData.h };
                toValue = { x: 0, y: 0, onComplete: onComplete };
                entrance = TweenLite.fromTo("#" + slideData.id, transitionData.durTra, fromValue, toValue);
                break;
            case "From Bottom-Right":
                fromValue = { x: slideData.w, y: slideData.h };
                toValue = { x: 0, y: 0, onComplete: onComplete };
                entrance = TweenLite.fromTo("#" + slideData.id, transitionData.durTra, fromValue, toValue);
                break;
            case "From Bottom-Left":
                fromValue = { x: -slideData.w, y: slideData.h };
                toValue = { x: 0, y: 0, onComplete: onComplete };
                entrance = TweenLite.fromTo("#" + slideData.id, transitionData.durTra, fromValue, toValue);
                break;
        }
    }

    //Hiệu ứng Transition: Newsflash
    function getNewsflashTransition(transitionData, slideData, onComplete) {
        let fromValue = null,
            toValue = null,
            entrance = null,
            //currentSlide = GlobalResources.CurrentSlideContainer, //<div> DOM chạy transition Clockwise
            //previousSlide = GlobalResources.PreviousSlide, //previousSlide chạy transition CounterClockwise
            group = transitionData.effOpt;
        switch (group) {
            case "Clockwise":
                fromValue = { scaleX: 0, scaleY: 0, rotation: -540 };
                toValue = { scaleX: 1, scaleY: 1, rotation: 0, onComplete: onComplete };
                entrance = TweenLite.fromTo("#" + slideData.id, transitionData.durTra, fromValue, toValue);
                break;
            case "CounterClockwise":
                fromValue = { scaleX: 0, scaleY: 0, rotation: 540 };
                toValue = { scaleX: 1, scaleY: 1, rotation: 0, onComplete: onComplete };
                entrance = TweenLite.fromTo("#" + slideData.id, transitionData.durTra, fromValue, toValue);
                break;
        }
    }

    //Hiệu ứng Transition: RandomBars
    function getRandomBarsTransition(transitionData, slideData, onComplete) {
        let transition = RandomBarsTransition(transitionData, slideData);
        let value = { percent: 100, onComplete: onComplete };
        let entrance = TweenLite.to(transition, transitionData.durTra, value);
    }

    //Hiệu ứng Transition: Zoom
    function getZoomTransition(transitionData, slideData, onComplete) {
        let fromValue = null,
            toValue = null,
            entrance = null,
            group = transitionData.effOpt;
        switch (group) {
            case "In":
                fromValue = { scaleX: 0, scaleY: 0 };
                toValue = { scaleX: 1, scaleY: 1, onComplete: onComplete };
                entrance = TweenLite.fromTo("#" + slideData.id, transitionData.durTra, fromValue, toValue);
                break;
            case "Out":
                fromValue = { scaleX: 6, scaleY: 6 };
                toValue = { scaleX: 1, scaleY: 1, onComplete: onComplete };
                entrance = TweenLite.fromTo("#" + slideData.id, transitionData.durTra, fromValue, toValue);
        }
    }

})();

//Hiệu ứng Split transition
function SplitTransition(transitionData, slideData) {
    let x1 = 0,
        x2 = 0,
        x3 = 0,
        x4 = 0,
        y1 = 0,
        y2 = 0,
        y3 = 0,
        y4 = 0;
    let width = slideData.w,
        height = slideData.h;
    let wrapper = $("#" + slideData.id);
    // let currentSlide = GlobalResources.CurrentSlideContainer; //<div> DOM
    // let previousSlide = GlobalResources.PreviousSlide;
    let path = null,
        svg = null;
    let _percent = 0;

    //Tính các điểm cố định ban đầu
    x1 = -width;
    x4 = width;
    y1 = -height;
    y4 = height;

    //Vẽ theo kiểu đi ra theo chiều ngang
    function drawShapeVOut(percent) {
        if (percent == 100) {
            $(svg).remove();
            path = null;
            $(wrapper).css("clipPath", "");
            return;
        }
        x2 = width / 2 - percent * width / 2 / 100;
        x3 = width / 2 + percent * width / 2 / 100;
        let str = "M" + x2 + " " + y1 + " " + x3 + " " + y1 + " " + x3 + " " + y4 + " " + x2 + " " + y4 + "z";

        if (path != null && str != null)
            path.setAttribute("d", str);
    }

    //Vẽ theo kiểu đi vào theo chiều ngang
    function drawShapeVIn(percent) {
        if (percent == 100) {
            $(svg).remove();
            path = null;
            $(wrapper).css("clipPath", "");
            return;
        }
        x2 = width / 2 - (100 - percent) * width / 2 / 100;
        x3 = width / 2 + (100 - percent) * width / 2 / 100;
        let str = "M" + x1 + " " + y1 + " " + x2 + " " + y1 + " " + x2 + " " + y4 + " " + x1 + " " + y4 + "z" +
            "M" + x3 + " " + y1 + " " + x4 + " " + y1 + " " + x4 + " " + y4 + " " + x3 + " " + y4 + "z";
        if (path != null && str != null)
            path.setAttribute("d", str);
    }

    //Vẽ theo kiểu đi ra theo chiều ngang
    function drawShapeHOut(percent) {
        if (percent == 100) {
            $(svg).remove();
            path = null;
            $(wrapper).css("clipPath", "");
            return;
        }
        y2 = height / 2 - percent * height / 2 / 100;
        y3 = height / 2 + percent * height / 2 / 100;
        let str = "M" + x1 + " " + y2 + " " + x4 + " " + y2 + " " + x4 + " " + y3 + " " + x1 + " " + y3 + "z";

        if (path != null && str != null)
            path.setAttribute("d", str);
    }

    //Vẽ theo kiểu đi vào theo chiều ngang
    function drawShapeHIn(percent) {
        if (percent == 100) {
            $(svg).remove();
            path = null;
            $(wrapper).css("clipPath", "");
            return;
        }
        y2 = height / 2 - (100 - percent) * height / 2 / 100;
        y3 = height / 2 + (100 - percent) * height / 2 / 100;
        let str = "M" + x1 + " " + y1 + " " + x4 + " " + y1 + " " + x4 + " " + y2 + " " + x1 + " " + y2 + "z" +
            "M" + x1 + " " + y3 + " " + x4 + " " + y3 + " " + x4 + " " + y4 + " " + x1 + " " + y4 + "z";
        if (path != null && str != null)
            path.setAttribute("d", str);
    }

    function init() {
        path = document.getElementById("awh_p1_" + slideData.id);
        if (path == null) {
            let xmlNs = "http://www.w3.org/2000/svg";
            let parent = $(wrapper).parent();
            svg = document.createElementNS(xmlNs, 'svg');
            svg.setAttributeNS(null, "class", "abc");
            svg.setAttributeNS(null, "id", "awh_svg");

            svg.setAttribute("width", "" + width);
            svg.setAttribute("height", "" + height);
            let clipPath = document.createElementNS(xmlNs, 'clipPath');
            clipPath.setAttributeNS(null, "id", "awh_clp1_" + slideData.id);
            path = document.createElementNS(xmlNs, 'path');
            path.setAttributeNS(null, "id", "awh_p1_" + slideData.id);
            clipPath.appendChild(path);
            svg.appendChild(clipPath);

            $(wrapper).css("clipPath", "url('#" + clipPath.id + "')");
            $(parent).append(svg);
            clipPath.InnerHTML = clipPath.OuterHTML;
        }
    }

    function drawShape(percent) {
        let group = transitionData.effOpt;
        switch (group) {
            case "VerticalIn":
                drawShapeVIn(percent);
                break;
            case "VerticalOut":
                drawShapeVOut(percent);
                break;
            case "HorizontalIn":
                drawShapeHIn(percent);
                break;
            case "HorizontalOut":
                drawShapeHOut(percent);
                break;
            default:
                drawShapeVIn(percent);
                break;
        }
    }

    return {
        get percent() {
            return _percent;
        },
        set percent(value) {
            _percent = value;
            init();
            drawShape(_percent);
        }
    }

}

//Hiệu ứng Circle Transition
function CircleTransition(transitionData, slideData) {
    let cx = 0,
        cy = 0,
        rx = 0,
        ry = 0,
        x1 = 0,
        x2 = 0,
        y1 = 0,
        y2 = 0;
    let width = slideData.w;
    let height = slideData.h;
    // let currentSlide = GlobalResources.CurrentSlideContainer; //<div> DOM
    // let previousSlide = GlobalResources.PreviousSlide;
    let wrapper = $("#" + slideData.id);
    let path = null,
        svg = null;
    let _percent = 0;
    let isInput = null;

    //Tính các điểm cố định ban đầu
    cx = width / 2; //Tọa độ X điểm trung tâm.
    cy = height / 2; //Tọa độ Y điểm trung tâm.

    x1 = -width / 2;
    x2 = width + width / 2;
    y1 = -height / 2;
    y2 = height + height / 2;

    //Vẽ theo kiểu đi ra
    function drawShapeOut(percent) {
        if (percent == 100) {
            $(svg).remove();
            path = null;
            $(wrapper).css("clipPath", "");
            return;
        }

        rx = width * percent / 100; //Bán kính chiều ngang.
        ry = height * percent / 100; //Bán kính chiều dọc.
        let str = calculatePathData();

        if (path != null && str != null)
            path.setAttribute("d", str);
    }

    //Tính toán data dựa vào các điểm đặt biệt
    function calculatePathData() {
        let d = [
            "M", cx, cy,
            "m", -rx, 0,
            "a", rx, ry,
            0, 1, 0, 2 * rx, 0,
            "a", rx, ry,
            0, 1, 0, -2 * rx, 0
        ];
        return d.join(" ");
    }

    //Vẽ theo kiểu đi vào
    function drawShapeIn(percent) {
        if (percent == 100) {
            $(svg).remove();
            path = null;
            $(wrapper).css("clipPath", "");
            return;
        }

        rx = width * (100 - percent) / 100; //Bán kính chiều ngang.
        ry = height * (100 - percent) / 100; //Bán kính chiều dọc.
        let str = "M" + x1 + " " + y1 + " " + x2 + " " + y1 + " " + x2 + " " + y2 + " " + x1 + " " + y2 + "z" + calculatePathData();
        if (path != null && str != null)
            path.setAttribute("d", str);
    }

    function init() {
        path = document.getElementById("awh_p1_" + slideData.id);
        if (path == null) {
            let xmlNs = "http://www.w3.org/2000/svg";
            let parent = $(wrapper).parent();
            svg = document.createElementNS(xmlNs, 'svg');
            svg.setAttributeNS(null, "class", "abc");
            svg.setAttributeNS(null, "id", "awh_svg");

            svg.setAttribute("width", "" + width);
            svg.setAttribute("height", "" + height);
            let clipPath = document.createElementNS(xmlNs, 'clipPath');
            clipPath.setAttributeNS(null, "id", "awh_clp1_" + slideData.id);
            path = document.createElementNS(xmlNs, 'path');
            path.setAttributeNS(null, "id", "awh_p1_" + slideData.id);
            clipPath.appendChild(path);
            svg.appendChild(clipPath);

            $(wrapper).css("clipPath", "url('#" + clipPath.id + "')");
            $(parent).append(svg);
            clipPath.InnerHTML = clipPath.OuterHTML;
        }
    }

    // function drawShape(percent){

    // }

    return {
        get percent() {
            return _percent;
        },
        set percent(value) {
            _percent = value;
            init();
            drawShapeOut(_percent)
        }
    }

}

//Hiệu ứng Dinamond Transition
function DiamondTransition(transitionData, slideData) {
    let x1 = 0,
        x2, x3 = 0,
        x4 = 0,
        x5 = 0,
        y1 = 0,
        y2 = 0,
        y3 = 0,
        y4 = 0,
        y5 = 0;
    let width = slideData.w;
    let height = slideData.h;
    // let currentSlide = GlobalResources.CurrentSlideContainer; //<div> DOM
    // let previousSlide = GlobalResources.PreviousSlide;
    let wrapper = $("#" + slideData.id);
    let path = null,
        svg = null;
    let _percent = 0;
    let isInput = null;

    //Tính các điểm cố định ban đầu
    x1 = -width / 2;
    x3 = slideData.w / 2;
    x5 = width + width / 2;
    y1 = -height / 2;
    y3 = slideData.h / 2;
    y5 = height + height / 2;

    //Vẽ theo kiểu đi ra
    function drawShapeOut(percent) {
        if (percent == 100) {
            $(svg).remove();
            path = null;
            $(wrapper).css("clipPath", "");
            return;
        }

        x1 = width / 2 - percent / 100 * width;
        x2 = width / 2 + percent / 100 * width;
        y1 = height / 2 - percent / 100 * height;
        y2 = height / 2 + percent / 100 * height;
        let str = "M" + x1 + " " + y3 + " " + x3 + " " + y1 + " " + x2 + " " + y3 + " " + x3 + " " + y2 + "z";

        if (path != null && str != null)
            path.setAttribute("d", str);
    }

    function init() {
        path = document.getElementById("awh_p1_" + slideData.id);
        if (path == null) {
            let xmlNs = "http://www.w3.org/2000/svg";
            let parent = $(wrapper).parent();
            svg = document.createElementNS(xmlNs, 'svg');
            svg.setAttributeNS(null, "class", "abc");
            svg.setAttributeNS(null, "id", "awh_svg");

            svg.setAttribute("width", "" + width);
            svg.setAttribute("height", "" + height);
            let clipPath = document.createElementNS(xmlNs, 'clipPath');
            clipPath.setAttributeNS(null, "id", "awh_clp1_" + slideData.id);
            path = document.createElementNS(xmlNs, 'path');
            path.setAttributeNS(null, "id", "awh_p1_" + slideData.id);
            clipPath.appendChild(path);
            svg.appendChild(clipPath);

            $(wrapper).css("clipPath", "url('#" + clipPath.id + "')");
            $(parent).append(svg);
            clipPath.InnerHTML = clipPath.OuterHTML;
        }
    }

    return {
        get percent() {
            return _percent;
        },
        set percent(value) {
            _percent = value;
            init();
            drawShapeOut(_percent);
        }
    }

}

//Hiệu ứng Plus Transition
function PlusTransition(transitionData, slideData) {
    let x1 = 0,
        x2 = 0,
        y1 = 0,
        y2 = 0; //4 điểm điều khiển
    let width = slideData.w;
    let height = slideData.h;
    // let currentSlide = GlobalResources.CurrentSlideContainer; //<div> DOM
    // let previousSlide = GlobalResources.PreviousSlide;
    let wrapper = $("#" + slideData.id);
    let path = null,
        svg = null;
    let _percent = 0;
    let isInput = null;

    //Vẽ theo kiểu đi ra
    function drawShapeOut(percent) {
        if (percent == 100) {
            $(svg).remove();
            path = null;
            $(wrapper).css("clipPath", "");
            return;
        }

        x1 = width * (100 - percent) / 200;
        x2 = width * (100 + percent) / 200;
        y1 = height * (100 - percent) / 200;
        y2 = height * (100 + percent) / 200;
        let str = "M" + x1 + " " + (-width) + " " + x2 + " " + (-width) + " " + x2 + " " + y1 + " " + width + " " + y1 + " " + width + " " + y2 + " " + x2 + " " + y2 + " " +
            x2 + " " + height + " " + x1 + " " + height + " " + x1 + " " + y2 + " " + -width + " " + y2 + " " + -width + " " + y1 + " " + x1 + " " + y1 + "z";

        if (path != null && str != null)
            path.setAttribute("d", str);
    }

    function init() {
        path = document.getElementById("awh_p1_" + slideData.id);
        if (path == null) {
            let xmlNs = "http://www.w3.org/2000/svg";
            let parent = $(wrapper).parent();
            svg = document.createElementNS(xmlNs, 'svg');
            svg.setAttributeNS(null, "class", "abc");
            svg.setAttributeNS(null, "id", "awh_svg");

            svg.setAttribute("width", "" + width);
            svg.setAttribute("height", "" + height);
            let clipPath = document.createElementNS(xmlNs, 'clipPath');
            clipPath.setAttributeNS(null, "id", "awh_clp1_" + slideData.id);
            path = document.createElementNS(xmlNs, 'path');
            path.setAttributeNS(null, "id", "awh_p1_" + slideData.id);
            clipPath.appendChild(path);
            svg.appendChild(clipPath);

            $(wrapper).css("clipPath", "url('#" + clipPath.id + "')");
            $(parent).append(svg);
            clipPath.InnerHTML = clipPath.OuterHTML;
        }
    }

    return {
        get percent() {
            return _percent;
        },
        set percent(value) {
            _percent = value;
            init();
            drawShapeOut(_percent);
        }
    }
}

//Hiệu ứng In Transition
function InTransition(transitionData, slideData) {
    let x1 = 0,
        x2 = 0,
        x3 = 0,
        x4 = 0,
        y1 = 0,
        y2 = 0,
        y3 = 0,
        y4 = 0;
    let width = slideData.w;
    let height = slideData.h;
    // let currentSlide = GlobalResources.CurrentSlideContainer; //<div> DOM
    // let previousSlide = GlobalResources.PreviousSlide;
    let wrapper = $("#" + slideData.id);
    let path = null,
        svg = null;
    let _percent = 0;

    x1 = 0;
    x4 = width;
    y1 = 0;
    y4 = height;

    function drawShapeIn(percent) {
        if (percent == 100) {
            $(svg).remove();
            path = null;
            $(wrapper).css("clipPath", "");
            return;
        }

        x2 = percent * width / 200;
        x3 = width - x2;
        y2 = percent * height / 200;
        y3 = height - y2;
        //let str = `M${x1} ${y1} h ${x4} v ${y4} h ${-x4} z M ${x2} ${y2} ${x2} ${y3} ${x3} ${y3} ${x3} ${y2} z`;
        let str = 'M' + x1 + ' ' + y1 + ' ' + x4 + ' ' + y4 + ' ' + (-x4) + ' z M ' + x2 + ' ' + y2 + ' ' + x2 + ' ' + y3 + ' ' + x3 + ' ' + y3 + ' ' + x3 + ' ' + y2 + ' z ';

        if (path != null && str != null)
            path.setAttribute("d", str);
    }

    function init() {
        path = document.getElementById("awh_p1_" + slideData.id);
        if (path == null) {
            let xmlNs = "http://www.w3.org/2000/svg";
            let parent = $(wrapper).parent();
            svg = document.createElementNS(xmlNs, 'svg');
            svg.setAttributeNS(null, "class", "abc");
            svg.setAttributeNS(null, "id", "awh_svg");

            svg.setAttribute("width", "" + width);
            svg.setAttribute("height", "" + height);
            let clipPath = document.createElementNS(xmlNs, 'clipPath');
            clipPath.setAttributeNS(null, "id", "awh_clp1_" + slideData.id);
            path = document.createElementNS(xmlNs, 'path');
            path.setAttributeNS(null, "id", "awh_p1_" + slideData.id);
            clipPath.appendChild(path);
            svg.appendChild(clipPath);

            $(wrapper).css("clipPath", "url('#" + clipPath.id + "')");
            $(parent).append(svg);
            clipPath.InnerHTML = clipPath.OuterHTML;
        }
    }

    return {
        get percent() {
            return _percent;
        },
        set percent(value) {
            _percent = value;
            init();
            drawShapeIn(_percent);
        }
    }

}

//Hiệu ứng Dissolve Transition
function DissolveTransition(transitionData, slideData) {
    let width = slideData.w;
    let height = slideData.h;
    // let currentSlide = GlobalResources.CurrentSlideContainer; //<div> DOM
    let previousSlide = null;
    var divBgTransition = document.createElement('div');
    divBgTransition.id = "bg-transition";
    document.getElementById('content').appendChild(divBgTransition);

    $("#bg-transition").css("width","100%");
    $("#bg-transition").css("height","100%");
    $("#bg-transition").css("background","#fff");

    if (GlobalResources.PreviousSlide != undefined) {
        previousSlide = GlobalResources.PreviousSlide.id;
    } else if (GlobalResources.PreviousSlide == undefined) {
        previousSlide = "bg-transition";
    }
    let wrapper = $("#" + previousSlide);
    let path = null,
        svg = null;
    let _percent = 0;
    let rectWidth = 20,
        rectHeight = 15; //Kích thước hình chữ nhật nhỏ.
    let preNum = 0;
    let points = shuffle(getPoints());

    function Point(x, y) {
        this.x = x;
        this.y = y;
    }

    //Lấy danh sách các điểm cần vẽ hình chữ nhật
    function getPoints() {
        let result = [];
        for (var i = 0; i < width; i += rectWidth) {
            for (var j = 0; j < height; j += rectHeight) {
                result.push(new Point(i, j));
            }
        }
        return result;
    }

    //Trộn mảng
    function shuffle(array) {
        var currentIndex = array.length,
            temporaryValue, randomIndex;
        while (0 !== currentIndex) {
            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;
            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }

        return array;
    }

    function drawShape(percent) {
        if (percent == 100) {
            $(svg).remove();
            path = null;
            $(wrapper).css("clipPath", "");
            return;
        }

        let numOfPoint = Math.floor(percent * (points.length) / 100);
        if (numOfPoint == preNum)
            return;
        preNum = numOfPoint;

        let str = "";
        for (var i = numOfPoint; i < points.length; i++) {
            //str += `M ${points[i].x} ${points[i].y} h ${rectWidth} v ${rectHeight} h ${-rectWidth} z `;
            str += 'M' + points[i].x + ' ' + points[i].y + ' h ' + rectWidth + ' v ' + rectHeight  + ' h ' + (-rectWidth) + ' z ';
        }

        if (path != null && str != null)
            path.setAttribute("d", str);
    }

    function init() {
        $("#" + previousSlide).css("z-index", "10");
        path = document.getElementById("awh_p1_" + previousSlide);
        if (path == null) {
            let xmlNs = "http://www.w3.org/2000/svg";
            let parent = $(wrapper).parent();
            svg = document.createElementNS(xmlNs, 'svg');
            svg.setAttributeNS(null, "class", "abc");
            svg.setAttributeNS(null, "id", "awh_svg");

            svg.setAttribute("width", "" + width);
            svg.setAttribute("height", "" + height);
            let clipPath = document.createElementNS(xmlNs, 'clipPath');
            clipPath.setAttributeNS(null, "id", "awh_clp1_" + previousSlide);
            path = document.createElementNS(xmlNs, 'path');
            path.setAttributeNS(null, "id", "awh_p1_" + previousSlide);
            clipPath.appendChild(path);
            svg.appendChild(clipPath);

            $(wrapper).css("clipPath", "url('#" + clipPath.id + "')");
            $(parent).append(svg);
            clipPath.InnerHTML = clipPath.OuterHTML;
        }
    }

    return {
        get percent() {
            return _percent;
        },
        set percent(value) {
            _percent = value;
            init();
            drawShape(_percent);
        }
    }
}

//Hiệu ứng Checkerboad Transition
function CheckerboadTransition(transitionData, slideData) {
    let width = slideData.w;
    let height = slideData.h;
    // let currentSlide = GlobalResources.CurrentSlideContainer; //<div> DOM
    // let previousSlide = GlobalResources.PreviousSlide;
    let wrapper = $("#" + slideData.id);
    let path = null,
        svg = null;
    let _percent = 0;
    let preNum = 0;
    let numberHorizontal = 0;
    let numberVertical = 0;
    let rectWidth = 0;
    let rectHeight = 0;
    let points = [];

    function init() {
        if (transitionData.effOpt === "From Top") {
            numberHorizontal = 10;
            numberVertical = 8;
            rectWidth = width / numberHorizontal;
            rectHeight = height / numberVertical;
            points = getPointsV();
        } else if (transitionData.effOpt === "From Left") {
            numberHorizontal = 6;
            numberVertical = 14;
            rectWidth = width / numberHorizontal;
            rectHeight = height / numberVertical;
            points = getPointsH();
        }

        path = document.getElementById("awh_p1_" + slideData.id);
        if (path == null) {
            let xmlNs = "http://www.w3.org/2000/svg";
            let parent = $(wrapper).parent();
            svg = document.createElementNS(xmlNs, 'svg');
            svg.setAttributeNS(null, "class", "abc");
            svg.setAttributeNS(null, "id", "awh_svg");

            svg.setAttribute("width", "" + width);
            svg.setAttribute("height", "" + height);
            let clipPath = document.createElementNS(xmlNs, 'clipPath');
            clipPath.setAttributeNS(null, "id", "awh_clp1_" + slideData.id);
            path = document.createElementNS(xmlNs, 'path');
            path.setAttributeNS(null, "id", "awh_p1_" + slideData.id);
            clipPath.appendChild(path);
            svg.appendChild(clipPath);

            $(wrapper).css("clipPath", "url('#" + clipPath.id + "')");
            $(parent).append(svg);
            clipPath.InnerHTML = clipPath.OuterHTML;
        }
    }

    function drawShapeTop(percent) {
        if (percent == 100) {
            $(svg).remove();
            path = null;
            $(wrapper).css("clipPath", "");
            return;
        }

        let str = "";

        let cRWidth = percent * rectWidth / 100;
        for (var i = 0; i < points.length; i++) {
            //str += `M ${points[i].x} ${points[i].y} h ${cRWidth} v ${rectHeight} h ${-cRWidth} z `;
            str += 'M' + points[i].x + ' ' + points[i].y + ' h ' + cRWidth + ' v ' + rectHeight + ' h ' + (-cRWidth) + ' z ';
        }

        if (path != null && str != null)
            path.setAttribute("d", str);
    }

    function drawShapeLeft(percent) {
        if (percent == 100) {
            $(svg).remove();
            path = null;
            $(wrapper).css("clipPath", "");
            return;
        }

        let str = "";

        let cRHeight = percent * rectHeight / 100;
        for (var i = 0; i < points.length; i++) {
            //str += `M ${points[i].x} ${points[i].y} h ${rectWidth} v ${cRHeight} h ${-rectWidth} z `;
            str += 'M' + points[i].x + ' ' + points[i].y + ' h ' + rectWidth + ' v ' + cRHeight + ' h ' + (-rectWidth) + ' z ';
        }

        if (path != null && str != null)
            path.setAttribute("d", str);
    }

    function getPointsH() {
        let result = [];
        let count = 0;
        let j = 0;
        for (var i = 0; i < height; i += rectHeight) {
            if (count % 2 == 0) {
                j = 0;
            } else {
                j = -rectWidth / 2;
            }

            for (; j < width; j += rectWidth) {
                result.push(new Point(j, i));
            }
            count++;
        }
        return result;
    }

    function getPointsV() {
        let result = [];
        let count = 0;
        let j = 0;
        for (var i = 0; i < width; i += rectWidth) {
            if (count % 2 == 0) {
                j = 0;
            } else {
                j = -rectHeight / 2;
            }

            for (; j < height; j += rectHeight) {
                result.push(new Point(i, j));
            }
            count++;
        }
        return result;
    }


    function Point(x, y) {
        this.x = x;
        this.y = y;
    }

    function drawShape(percent) {
        let group = transitionData.effOpt;
        switch (group) {
            case "From Left":
                drawShapeTop(percent);
                break;
            case "From Top":
                drawShapeLeft(percent);
                break;
            default:
                drawShapeTop(percent);
                break;
        }
    }

    return {
        get percent() {
            return _percent;
        },
        set percent(value) {
            _percent = value;
            init();
            drawShape(_percent);
        }
    }
}

//Hiệu ứng Blinds Transition
function BlindsTransition(transitionData, slideData) {
    let width = slideData.w;
    let height = slideData.h;
    // let currentSlide = GlobalResources.CurrentSlideContainer; //<div> DOM
    // let previousSlide = GlobalResources.PreviousSlide;
    let wrapper = $("#" + slideData.id);
    let path = null,
        svg = null;
    let _percent = 0;
    let preNum = 0;
    let numberHorizontal = 0;
    let numberVertical = 0;
    let rectWidth = 0;
    let rectHeight = 0;
    let points = [];


    function init() {
        if (transitionData.effOpt == "From Vertical") {
            numberHorizontal = 6;
            numberVertical = 6;
            rectWidth = width / numberHorizontal;
            rectHeight = height / numberVertical;
            points = getPointsV();
        } else if (transitionData.effOpt == "From Horizontal") {
            numberHorizontal = 6;
            numberVertical = 6;
            rectWidth = width / numberHorizontal;
            rectHeight = height / numberVertical;
            points = getPointsH();
        }

        path = document.getElementById("awh_p1_" + "test-dissolve");
        if (path == null) {
            let xmlNs = "http://www.w3.org/2000/svg";
            let parent = $(wrapper).parent();
            svg = document.createElementNS(xmlNs, 'svg');
            svg.setAttributeNS(null, "class", "abc");
            svg.setAttributeNS(null, "id", "awh_svg");

            svg.setAttribute("width", "" + width);
            svg.setAttribute("height", "" + height);
            let clipPath = document.createElementNS(xmlNs, 'clipPath');
            clipPath.setAttributeNS(null, "id", "awh_clp1_" + "test-dissolve");
            path = document.createElementNS(xmlNs, 'path');
            path.setAttributeNS(null, "id", "awh_p1_" + "test-dissolve");
            clipPath.appendChild(path);
            svg.appendChild(clipPath);

            $(wrapper).css("clipPath", "url('#" + clipPath.id + "')");
            $(parent).append(svg);
            clipPath.InnerHTML = clipPath.OuterHTML;
        }
    }

    function drawShapeV(percent) {
        if (percent == 100) {
            $(svg).remove();
            path = null;
            $(wrapper).css("clipPath", "");
            return;
        }

        let str = "";

        let cRWidth = percent * rectWidth / 100;
        for (var i = 0; i < points.length; i++) {
            //str += `M ${points[i].x} ${points[i].y} h ${cRWidth} v ${rectHeight} h ${-cRWidth} z `;
            str += 'M' + points[i].x + ' ' + points[i].y + ' h ' + cRWidth + ' v ' + rectHeight + ' h ' + (-cRWidth) + ' z ';
        }

        if (path != null && str != null)
            path.setAttribute("d", str);
    }

    function drawShapeH(percent) {
        if (percent == 100) {
            $(svg).remove();
            path = null;
            $(wrapper).css("clipPath", "");
            return;
        }

        let str = "";

        let cRHeight = percent * rectHeight / 100;
        for (var i = 0; i < points.length; i++) {
            //str += `M ${points[i].x} ${points[i].y} h ${rectWidth} v ${cRHeight} h ${-rectWidth} z `;
            str += 'M' + points[i].x + ' ' + points[i].y + ' h ' + rectWidth + ' v ' + cRHeight + ' h ' + (-rectWidth) + ' z ';
        }

        if (path != null && str != null)
            path.setAttribute("d", str);
    }

    function getPointsH() {
        let result = [];
        let count = 0;
        let j = 0;
        for (var i = 0; i < height; i += rectHeight) {
            if (count % 2 == 0) {
                j = 0;
            } else {
                j = -rectWidth / 2;
            }

            for (; j < width; j += rectWidth) {
                result.push(new Point(j, i));
            }
            count++;
        }
        return result;
    }

    function getPointsV() {
        let result = [];
        let count = 0;
        let j = 0;
        for (var i = 0; i < width; i += rectWidth) {
            if (count % 2 == 0) {
                j = 0;
            } else {
                j = -rectHeight / 2;
            }

            for (; j < height; j += rectHeight) {
                result.push(new Point(i, j));
            }
            count++;
        }
        return result;
    }


    function Point(x, y) {
        this.x = x;
        this.y = y;
    }

    function drawShape(percent) {
        let group = transitionData.effOpt;
        switch (group) {
            case "From Vertical":
                drawShapeV(percent);
                break;
            case "From Horizontal":
                drawShapeH(percent);
                break;
            default:
                drawShapeV(percent);
                break;
        }
    }

    return {
        get percent() {
            return _percent;
        },
        set percent(value) {
            _percent = value;
            init();
            drawShape(_percent);
        }
    }
}

//Hiệu ứng Clock Transition
function ClockTransition(transitionData, slideData) {
    let _currentAngle = 0;
    // let currentSlide = GlobalResources.CurrentSlideContainer; //<div> DOM
    // let previousSlide = GlobalResources.PreviousSlide;
    let wrapper = $("#" + slideData.id);
    let path = null,
        svg = null;
    let width = slideData.w;
    let height = slideData.h;

    let r = Math.sqrt(Math.pow(width, 2) / 4 + Math.pow(height, 2) / 4);
    let x = width / 2,
        y = 0 - r + height / 2;
    let xt = width / 2;
    let yt = height / 2;

    //Hàm vẽ đường Clip theo góc quay alpha
    function drawShape(alpha) {
        if (alpha == 360) {
            $(svg).remove();
            path = null;
            $(wrapper).css("clipPath", "");
            return;
        }

        let x1 = xt + (x - xt) * Math.cos(alpha / 180 * Math.PI) - (y - yt) * Math.sin(alpha / 180 * Math.PI);
        let y1 = yt + (x - xt) * Math.sin(alpha / 180 * Math.PI) + (y - yt) * Math.cos(alpha / 180 * Math.PI);
        let large = 0;
        if (alpha > 180)
            large = 1;
        let str = "M " + xt + " " + yt + " L " + x + " " + y + " " +
            " A " + r + " " + r + " " + (alpha / 180 * Math.PI) + " " + large + " 1 " +
            " " + x1 + " " + y1 + " Z";

        if (path != null && str != null)
            path.setAttribute("d", str);
    }

    function init() {
        path = document.getElementById("awh_p1_" + slideData.id);
        if (path == null) {
            let xmlNs = "http://www.w3.org/2000/svg";
            let parent = $(wrapper).parent();
            svg = document.createElementNS(xmlNs, 'svg');
            svg.setAttributeNS(null, "class", "abc");
            svg.setAttributeNS(null, "id", "awh_svg");

            svg.setAttribute("width", "" + width);
            svg.setAttribute("height", "" + height);
            let clipPath = document.createElementNS(xmlNs, 'clipPath');
            clipPath.setAttributeNS(null, "id", "awh_clp1_" + slideData.id);
            path = document.createElementNS(xmlNs, 'path');
            path.setAttributeNS(null, "id", "awh_p1_" + slideData.id);
            clipPath.appendChild(path);
            svg.appendChild(clipPath);

            $(wrapper).css("clipPath", "url('#" + clipPath.id + "')");
            $(parent).append(svg);
            clipPath.InnerHTML = clipPath.OuterHTML;
        }
    }

    return {
        get angle() {
            return _currentAngle;
        },
        set angle(value) {
            _currentAngle = value;
            init();
            drawShape(_currentAngle);
        }
    }
}

//Hiệu ứng RandomBars Transition
function RandomBarsTransition(transitionData, slideData) {
    let x1 = 0,
        x2 = 0,
        y1 = 0,
        y2 = 0;
    let width = slideData.w;
    let height = slideData.h;
    let wrapper = $("#" + slideData.id);
    let path = null,
        svg = null;
    let _percent = 0;
    let preNum = 0;
    let radomPointsV = getVPoints();
    let radomPointsH = getHPoints();

    //Random theo chiều ngang
    function drawShapeV(percent) {
        if (percent == 100) {
            $(svg).remove();
            path = null;
            $(wrapper).css("clipPath", "");
            return;
        }

        let numOfPoint = Math.floor(percent * (radomPointsH.length) / 100);
        if (numOfPoint == preNum)
            return;
        preNum = numOfPoint;

        let str = "";
        for (var i = 0; i < numOfPoint - 1; i++) {
            //str += (` M ${-width} ${radomPointsH[i]} h ${width + 2 * width} v 1 h ${-width - 2 * width}z`);
            str += 'M' + (-width) + ' ' + radomPointsH[i] + ' h ' + (width + 2 * width) + ' v 1 h ' + (-width -2 * width) + ' z ';
        }

        if (path != null && str != null)
            path.setAttribute("d", str);
    }

    //Random theo chiều dọc
    function drawShapeH(percent) {
        if (percent == 100) {
            $(svg).remove();
            path = null;
            $(wrapper).css("clipPath", "");
            return;
        }

        let numOfPoint = Math.floor(percent * (radomPointsV.length) / 100);
        if (numOfPoint == preNum)
            return;
        preNum = numOfPoint;

        let str = "";
        for (var i = 0; i < numOfPoint - 1; i++) {
            //str += (` M ${radomPointsV[i]} ${-height} h 1 v ${height + 2 * height} h -1 z`);
            str += 'M' + radomPointsV[i] + ' ' + -height + ' h 1 v ' + (height + 2 * height) + ' h -1 z ';
        }

        if (path != null && str != null)
            path.setAttribute("d", str);
    }

    function getHPoints() {
        let points = [],
            result = [];
        for (var i = -height; i < height + height; i++) {
            points.push(i);
        }

        for (var i = 0; i < height + height; i++) {
            let number = getRandomInt(0, points.length - 1);
            result.push(points[number]);
            points.splice(number, 1);
        }
        return result;
    }

    function getVPoints() {
        let points = [],
            result = [];
        for (var i = -width; i < width + width; i++) {
            points.push(i);
        }

        for (var i = 0; i < width + width; i++) {
            let number = getRandomInt(0, points.length - 1);
            result.push(points[number]);
            points.splice(number, 1);
        }
        return result;
    }

    //Lấy số ngẫu nhiên
    function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    function init() {
        path = document.getElementById("awh_p1_" + slideData.id);
        if (path == null) {
            let xmlNs = "http://www.w3.org/2000/svg";
            let parent = $(wrapper).parent();
            svg = document.createElementNS(xmlNs, 'svg');
            svg.setAttributeNS(null, "class", "abc");
            svg.setAttributeNS(null, "id", "awh_svg");

            svg.setAttribute("width", "" + width);
            svg.setAttribute("height", "" + height);
            let clipPath = document.createElementNS(xmlNs, 'clipPath');
            clipPath.setAttributeNS(null, "id", "awh_clp1_" + slideData.id);
            path = document.createElementNS(xmlNs, 'path');
            path.setAttributeNS(null, "id", "awh_p1_" + slideData.id);
            clipPath.appendChild(path);
            svg.appendChild(clipPath);

            $(wrapper).css("clipPath", "url('#" + clipPath.id + "')");
            $(parent).append(svg);
            clipPath.InnerHTML = clipPath.OuterHTML;
        }
    }

    function drawShape(percent) {
        let group = transitionData.effOpt;
        switch (group) {
            case "Vertical":
                drawShapeV(percent);
                break;
            case "Horizontal":
                drawShapeH(percent);
                break;
        }
    }

    return {
        get percent() {
            return _percent;
        },
        set percent(value) {
            _percent = value;
            init();
            drawShape(_percent);
        }
    }
}
