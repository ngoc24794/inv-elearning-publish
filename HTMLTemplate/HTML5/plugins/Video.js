'use strict';
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var VideoModule = (function () {
    var isCaption = false;
    var newwindows = {};
    return {
        StateVideo: function (state) {
            var v = document.getElementById('content').querySelector('video');
            if (v != null) {
                if (state) {
                    v.play();
                }
                else {
                    v.pause();
                }
            }
        },
        Popitup: function (url, nm, str) {
            if ((newwindows[nm] == null) || (newwindows[nm].closed)) {
                newwindows[nm] = window.open(url, nm, str);
            }
            newwindows[nm].focus();
        },
        setIsCaption: function (isCap) {
            isCaption = isCap;
        },
        getIsCaption: function () {
            return isCaption;
        }
    };
})();
var MainVideoAudio = (function () {
    function MainVideoAudio(options, type, isMainlayout) {
        this.Options = options;
        this.Type = type;
        this.ArrCaptions = [];
        this.isMainlayout = isMainlayout;
    }
    MainVideoAudio.prototype.CreateElementVideo = function () {
        if (this.Options.cp != null) {
            this.ArrCaptions = this.GetSubtilesArray(this.Options.cp);
        }
        var blockVideo = document.createElement("div");
        var video;
        if (this.Type == 0) {
            video = document.createElement('video');
        }
        else {
            video = document.createElement('audio');
        }
        var source = document.createElement('source');
        var widthVideo = this.Options.w;
        var heightVideo = this.Options.h;
        var zIndex = this.Options.z;
        blockVideo.id = "blockVideo_" + this.Options.id;
        blockVideo.className = "myBlockVideo";
        if (this.Type == 0) {
            blockVideo.style.width = this.Options.w + "px";
            blockVideo.style.height = this.Options.h + 39 + "px";
            blockVideo.style.left = 0 + "px";
            blockVideo.style.top = 0 + "px";
            blockVideo.style.position = "absolute";
        }
        else {
            blockVideo.style.width = "0px";
            blockVideo.style.height = "0px";
            video.style.width = "0px";
        }
        video.style.position = "absolute";
        video.id = "video_" + this.Options.id;
        video.className = "myVideo";
        video.controls = false;
        video.volume = GlobalHelper.Volume;
        var self = this;
        video.addEventListener('timeupdate', function (event) { self.UpdateTimeAuto(event); });
        video.onloadedmetadata = function (event) {
            self.LoadedMetadataVideo(event);
        };
        if (this.Type == 0) {
            if (this.Options.sV == "InNewBroswer" || this.Options.pV == "WhenClicked") {
                video.addEventListener("click", function (event) { self.ClickVideo(event); });
            }
            if (this.Options.pV == "Automatically" && this.Options.sV != "InNewBroswer") {
                video.setAttribute("data-play", 'auto');
            }
        }
        source.setAttribute('src', this.Options.mU);
        video.appendChild(source);
        blockVideo.appendChild(video);
        if (this.Type == 0) {
            if (this.Options.sC == "BelowVideo") {
                var xmlns_1 = "http://www.w3.org/2000/svg";
                var controlVideo = document.createElement('div');
                var btnPlay = document.createElement('div');
                var svgIcon = document.createElementNS(xmlns_1, 'svg');
                var listColor = document.createElementNS(xmlns_1, 'defs');
                var linearGradient = document.createElementNS(xmlns_1, 'linearGradient');
                var stop1 = document.createElementNS(xmlns_1, 'stop');
                var stop2 = document.createElementNS(xmlns_1, 'stop');
                var stop3 = document.createElementNS(xmlns_1, 'stop');
                var path = document.createElementNS(xmlns_1, 'path');
                var progressBar = document.createElement('div');
                var actionProgressBar = document.createElement('div');
                var time = document.createElement('span');
                controlVideo.id = "control_" + this.Options.id;
                controlVideo.className = "myControl";
                controlVideo.style.top = heightVideo + "px";
                btnPlay.id = "btnPlay_" + this.Options.id;
                btnPlay.className = "myPlay";
                btnPlay.title = "play/pause";
                btnPlay.addEventListener('click', function (event) {
                    self.ClickPlay(event);
                });
                btnPlay.addEventListener('mousemove', function (event) {
                    self.MouseMovePlay(event);
                });
                btnPlay.addEventListener('mouseleave', function (event) {
                    self.MouseLeavePlay(event);
                });
                svgIcon.id = "icon_" + this.Options.id;
                svgIcon.setAttribute("class", "myIcon");
                svgIcon.setAttribute("width", "20");
                svgIcon.setAttribute("height", "20");
                linearGradient.id = "fillLinear_" + this.Options.id;
                linearGradient.setAttribute("class", 'fillLinear');
                linearGradient.setAttribute('x1', "50%");
                linearGradient.setAttribute('x2', "50%");
                linearGradient.setAttribute('y1', "0%");
                linearGradient.setAttribute('y2', "100%");
                stop1.setAttribute("offset", "0%");
                stop1.setAttribute("stop-color", "#ECAD26");
                stop2.setAttribute("offset", "50%");
                stop2.setAttribute("stop-color", "#ECBB65");
                stop3.setAttribute("offset", "100%");
                stop3.setAttribute("stop-color", "#EED6AD");
                path.id = "path_" + this.Options.id;
                path.setAttribute('class', "myPath");
                path.setAttribute('fill', "#acaaaa");
                progressBar.id = "progressBar_" + this.Options.id;
                progressBar.className = "myProgressBar";
                progressBar.addEventListener('click', function (event) { self.ClickProgress(event); });
                var widthProgress = Math.abs(widthVideo - 111);
                progressBar.style.width = widthProgress + "px";
                actionProgressBar.id = "actionProgressBar_" + this.Options.id;
                actionProgressBar.className = "myActionProgressBar";
                time.id = "time_" + this.Options.id;
                time.className = "myTime";
                time.innerHTML = "00:00:00.0";
                controlVideo.appendChild(btnPlay);
                controlVideo.appendChild(progressBar);
                controlVideo.appendChild(time);
                btnPlay.appendChild(svgIcon);
                svgIcon.appendChild(listColor);
                listColor.appendChild(linearGradient);
                linearGradient.appendChild(stop1);
                linearGradient.appendChild(stop2);
                linearGradient.appendChild(stop3);
                svgIcon.appendChild(path);
                progressBar.appendChild(actionProgressBar);
                blockVideo.appendChild(controlVideo);
            }
        }
        if (this.Options.cp != null) {
            this.ProcessCaption();
        }
        return blockVideo;
    };
    MainVideoAudio.prototype.LoadedMetadataVideo = function (event) {
        var numberId = this.GetNumberId(event);
        var video = document.getElementById("video_" + numberId);
        if (this.Type == 0) {
            var path = document.getElementById("path_" + numberId);
            var widthVideo = this.Options.w;
            var heightVideo = this.Options.h;
            var widthInitial = event.target.videoWidth;
            var heightInitial = event.target.videoHeight;
            var ratioWidth = widthVideo / widthInitial;
            var ratioHeight = heightVideo / heightInitial;
            var scale = "scale(" + ratioWidth + "," + ratioHeight + ")";
            video.style.transform = scale;
            if (this.Options.sC == "BelowVideo") {
                if (this.Options.pV == "Automatically" && this.Options.sV != "InNewBroswer" && this.isMainlayout) {
                    path.setAttribute('d', "M2,2 7,2 7,18 2,18z M11,2 16,2 16,18 11,18z");
                }
                else {
                    path.setAttribute('d', "M2,2 L18,10 2,18 z");
                }
            }
        }
        if (this.isMainlayout) {
            if (this.Options.pV == "Automatically" && this.Options.sV != "InNewBroswer") {
                var v = video.play();
                video.autoplay=true;
                if (v !== undefined) {
                    v.then(function (_) {
                    }).catch(function (error) {
                        if (document.getElementById("contain-message") == null) {
                            var noti = new Notification("Video");
                            document.getElementById('container').appendChild(noti.ShowMessage());
                        }
                        video.muted = true;
                    });
                }
            }
        }
    };
    MainVideoAudio.prototype.UpdateTimeVideo = function (currentTime) {
        var elementID = this.Options.ID;
        var videoObj = document.getElementById('video_' + this.Options.ID);
        var path = document.getElementById('path_' + this.Options.ID);
        var startTime = this.Options.Timing.StartTime;
        var endTime = this.Options.Timing.EndTime;
        if (videoObj != undefined) {
            if (startTime > currentTime) {
            }
            else if (startTime <= currentTime && currentTime <= endTime) {
                videoObj.currentTime = currentTime - startTime;
                videoObj.pause();
            }
            else {
                if (endTime < videoObj.duration) {
                    videoObj.play();
                }
            }
            this.ChangePlayPause(videoObj, path);
        }
    };
    MainVideoAudio.prototype.ClickVideo = function (event) {
        var numberId = this.GetNumberId(event);
        var video = document.getElementById("video_" + numberId);
        var path = document.getElementById("path_" + numberId);
        var source = document.getElementById("source_" + numberId);
        var pathVideo = source.src;
        if (this.Options.sV == "InNewBroswer") {
            var width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
            var height = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
            video.pause();
            this.ChangePlayPause(video, path);
            var str = "height=" + width + "width=" + height;
            VideoModule.Popitup("./" + this.Options.mU, "video", str);
        }
        else if (this.Options.pV == "WhenClicked") {
            this.CheckVideoPlayPause(video, path);
        }
    };
    MainVideoAudio.prototype.ClickPlay = function (event) {
        var numberId = this.GetNumberId(event);
        var video = document.getElementById('video_' + numberId);
        var path = document.getElementById('path_' + numberId);
        this.CheckVideoPlayPause(video, path);
    };
    MainVideoAudio.prototype.ChangePlayPause = function (video, path) {
        if (path != null && video != null) {
            if (video.paused || video.ended) {
                path.setAttribute('d', "M2,2 L18,10 2,18 z");
            }
            else {
                path.setAttribute('d', "M2,2 7,2 7,18 2,18z M11,2 16,2 16,18 11,18z");
            }
        }
    };
    MainVideoAudio.prototype.CheckVideoPlayPause = function (video, path) {
        if (video.paused || video.ended) {
            if (path != null) {
                path.setAttribute('d', "M2,2 7,2 7,18 2,18z M11,2 16,2 16,18 11,18z");
            }
            video.play();
        }
        else {
            if (path != null) {
                path.setAttribute('d', "M2,2 L18,10 2,18 z");
            }
            video.pause();
        }
    };
    MainVideoAudio.prototype.MouseMovePlay = function (event) {
        var numberId = this.GetNumberId(event);
        var path = document.getElementById('path_' + numberId);
        path.setAttribute('fill', "url(#fillLinear_" + numberId + ")");
    };
    MainVideoAudio.prototype.MouseLeavePlay = function (event) {
        var numberId = this.GetNumberId(event);
        var path = document.getElementById('path_' + numberId);
        path.setAttribute('fill', "#acaaaa");
    };
    MainVideoAudio.prototype.ClickProgress = function (event) {
        var numberId = this.GetNumberId(event);
        var progressBar = document.getElementById("progressBar_" + numberId);
        var actionProgressBar = document.getElementById('actionProgressBar_' + numberId);
        var video = document.getElementById('video_' + numberId);
        var setCurrentTimeVideo = (event.offsetX * video.duration) / progressBar.offsetWidth;
        actionProgressBar.style.width = event.offsetX + "px";
        video.currentTime = setCurrentTimeVideo;
    };
    MainVideoAudio.prototype.UpdateTimeAuto = function (event) {
        var numberId = this.GetNumberId(event);
        var video = document.getElementById("video_" + numberId);
        var path = document.getElementById('path_' + numberId);
        if (video != null) {
            if (video.paused || (GlobalResources.ShowCaption) && this.ArrCaptions.length > 0) {
                var currentItem = this.ArrCaptions[0];
                var showCaption = document.getElementById('textCaption_' + numberId);
                if (showCaption == null)
                    this.ProcessCaption();
                if (showCaption != null) {
                    if (currentItem != null && video.currentTime >= currentItem.startTime && video.currentTime <= currentItem.endTime) {
                        if (currentItem.text != null) {
                            showCaption.innerHTML = currentItem.text;
                            showCaption.style.visibility = "visible";
                        }
                        else
                            showCaption.style.visibility = "hidden";
                    }
                    else {
                        currentItem = this.GetCurrentItem(this.ArrCaptions, video);
                        if (currentItem == null || currentItem.text == null) {
                            showCaption.innerHTML = "";
                            showCaption.style.visibility = "hidden";
                        }
                        else {
                            showCaption.innerHTML = currentItem.text;
                            showCaption.style.visibility = "visible";
                        }
                    }
                }
            }
        }
        if (this.Options.sC == "BelowVideo") {
            if (video != null) {
                this.ChangePlayPause(video, path);
                var actionProgressBar = document.getElementById('actionProgressBar_' + numberId);
                var time = document.getElementById('time_' + numberId);
                var setWithProgressbar = (video.currentTime * 100) / video.duration;
                actionProgressBar.style.width = setWithProgressbar + "%";
                time.innerHTML = this.ParseTime(video.currentTime);
            }
        }
    };
    MainVideoAudio.prototype.ParseTime = function (t) {
        var second, minute, hour, mili;
        hour = Math.floor(t / 3600);
        minute = Math.floor(t % 3600);
        minute = Math.floor(minute / 60);
        second = Math.floor(t % 60);
        mili = Math.floor((t - (hour * 3600 + minute * 60 + second)) * 10);
        return this.ParseString(hour) + ":" + this.ParseString(minute) + ":" + this.ParseString(second) + "." + mili;
    };
    MainVideoAudio.prototype.ParseString = function (i) {
        if (i < 10) {
            return "0" + i;
        }
        else {
            return i;
        }
    };
    MainVideoAudio.prototype.ProcessCaption = function () {
        var captionContent = document.createElement('p');
        captionContent.id = "textCaption_" + this.Options.id;
        captionContent.style.width = "auto";
        captionContent.className = "myCaption";
        captionContent.style.zIndex = 100;
        var itemCaption = document.getElementById('textCaption_' + this.Options.id);
        if (itemCaption != null) {
            document.getElementById('caption').removeChild(itemCaption);
        }
        document.getElementById('caption').appendChild(captionContent);
    };
    MainVideoAudio.prototype.GetCurrentItem = function (arrSubs, video) {
        for (var i = 0; i < arrSubs.length; ++i) {
            if (video.currentTime >= arrSubs[i].startTime && video.currentTime <= arrSubs[i].endTime) {
                return arrSubs[i];
            }
        }
        return null;
    };
    MainVideoAudio.prototype.ParseTimestamp = function (s) {
        var match = s.match(/^(?:([0-9]+):)?([0-5][0-9]):([0-5][0-9](?:[.,][0-9]{0,3})?)/);
        if (match == null) {
            throw 'Invalid timestamp format: ' + s;
        }
        var hours = parseInt(match[1] || "0", 10);
        var minutes = parseInt(match[2], 10);
        var seconds = parseFloat(match[3].replace(',', '.'));
        return seconds + 60 * minutes + 60 * 60 * hours;
    };
    MainVideoAudio.prototype.GetSubtilesArray = function (vtt) {
        var lines = vtt.trim().replace('\r\n', '\n').split(/[\r\n]/).map(function (line) {
            return line.trim();
        });
        var objects = [];
        var start = null;
        var end = null;
        var payload = null;
        for (var i = 0; i < lines.length; i++) {
            if (lines[i].indexOf('-->') >= 0) {
                var splitted = lines[i].split(/[ \t]+-->[ \t]+/);
                if (splitted.length != 2) {
                    throw 'Error when splitting "-->": ' + lines[i];
                }
                start = this.ParseTimestamp(splitted[0]);
                end = this.ParseTimestamp(splitted[1]);
            }
            else if (lines[i] == '') {
                if (start != null && end) {
                    var obj = { startTime: start, endTime: end, text: payload };
                    objects.push(obj);
                    start = null;
                    end = null;
                    payload = null;
                }
            }
            else if (start != null && end) {
                if (payload == null) {
                    payload = lines[i];
                }
                else {
                    payload += '\n' + lines[i];
                }
            }
        }
        if (start != null && end) {
            var obj = { startTime: start, endTime: end, text: payload };
            objects.push(obj);
        }
        return objects;
    };
    MainVideoAudio.prototype.GetNumberId = function (e) {
        var ids = e.target.id.split('_');
        if (ids.length == 2)
            return ids[1];
        return "";
    };
    return MainVideoAudio;
}());
var Audio = (function (_super) {
    __extends(Audio, _super);
    function Audio(options, isMainlayout) {
        return _super.call(this, options, 1, isMainlayout) || this;
    }
    Audio.prototype.genElement = function () {
        return _super.prototype.CreateElementVideo.call(this);
    };
    return Audio;
}(MainVideoAudio));
var Video = (function (_super) {
    __extends(Video, _super);
    function Video(options, isMainlayout) {
        return _super.call(this, options, 0, isMainlayout) || this;
    }
    Video.prototype.genElement = function () {
        return _super.prototype.CreateElementVideo.call(this);
    };
    return Video;
}(MainVideoAudio));
var YoutubeModule = function () {
    return {
        Level: GlobalEnum.ModuleLevel.Element,
        ContentType: "Youtube",
        generateElement: function (e) {
            var div = document.createElement('div');
            var iframe = document.createElement('iframe');
            div.style.left = "0px";
            div.style.top = "0px";
            div.style.position = 'absolute';
            iframe.id = e.id;
            iframe.style.width = e.w + "px";
            iframe.style.height = e.h + "px";
            iframe.style.left = "0px";
            iframe.style.top = "0px";
            iframe.style.zIndex = e.z;
            iframe.setAttribute('frameborder', 0);
            var str = "?rel=0&playsinline=1&enablejsapi=1";
            if (e.fullscr) {
                iframe.setAttribute("allowfullscreen", true);
            }
            if (e.hiAn) {
                str += "&showinfo=0&iv_load_policy=3";
            }
            if (e.hiCo) {
                str += "&controls=0";
            }
            if (e.st != 0) {
                iframe.duration = e.st;
            }
            if (e.aupl) {
                str += "&autoplay=1";
            }
            if (e.sppa) {
                str += "&start=" + e.st + "&end=" + e.end;
            }
            else {
                str += "&start=0";
            }
            iframe.setAttribute('src', e.urlvi + str);
            div.appendChild(iframe);
            return div;
        }
    };
}();
GlobalResources.addModule((function () {
    return {
        Level: GlobalEnum.ModuleLevel.Element,
        ContentType: "Video",
        generateElement: function (elementData, options, isMainlayout) {
            var v = new Video(elementData, isMainlayout);
            return v.genElement();
        }
    };
})());
GlobalResources.addModule((function () {
    return {
        Level: GlobalEnum.ModuleLevel.Element,
        ContentType: "Audio",
        generateElement: function (elementData, options, isMainlayout) {
            var a = new Audio(elementData, isMainlayout);
            return a.genElement();
        }
    };
})());
GlobalResources.addModule(YoutubeModule);
