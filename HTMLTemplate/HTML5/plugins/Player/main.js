"use strict";

function genMenu() {
    var menuWrapper = document.getElementById("menuWrapper");
    var menu = document.getElementById("menuContent");
    var menuSearch = document.getElementById("menuSearch");
    var menuListContent = document.getElementById("menuListContent");
    var count = 1;
    for (var i = 0; i < DOC.slds.length; i++) {
        if (DOC.slds[i].slt == "Result" || !DOC.slds[i].cs)
            continue;
        var liTag = GlobalHelper.createTags('li', GlobalHelper.generateBranchId(DOC.slds[i].id, "li"), 'menuList');
        liTag.select = false;
        var imgTag = GlobalHelper.createTags('div', '', 'imgMenuWrapper');
        var imgMenuTag = GlobalHelper.createTags('div', '', 'imgMenu');
        imgMenuTag.style.backgroundImage = "url('." + DOC.slds[i].thmU + "')";
        GlobalHelper.appendIdContent(imgTag, imgMenuTag);
        GlobalHelper.appendIdContent(liTag, imgTag);
        var infoSlide = GlobalHelper.createTags('div', '', 'infoMenuWrapper');
        var titleSlide = GlobalHelper.createTags('div', '', 'infoMenu');
        var numberTitle = GlobalHelper.createTags('span', '', 'titleMenu');
        numberTitle.innerHTML = (count++) + ". ";
        GlobalHelper.appendIdContent(titleSlide, numberTitle);
        var infoTitle = GlobalHelper.createTags('span', '', 'infoTitle');
        infoTitle.setAttribute("title", DOC.slds[i].nm);
        infoTitle.innerHTML = DOC.slds[i].nm;
        GlobalHelper.appendIdContent(titleSlide, infoTitle);
        GlobalHelper.appendIdContent(infoSlide, titleSlide);
        GlobalHelper.appendIdContent(liTag, infoSlide);
        liTag.setAttribute("data-index", i);
        liTag.onclick = function(e) {
            if (GlobalHelper.IsMenu) {
                GlobalHelper.jumToSlideIndex(e.currentTarget.getAttribute("data-index"));
            }
        };
        GlobalHelper.appendIdContent(menu, liTag);
    }
    if (!DOC.plr.iS) {
        menuSearch.style.display = 'none';
    }
}

function searchValue() {
    var input = document.getElementById("menuSearchInput");
    var filter = input.value.toUpperCase();
    var ul = document.getElementById("menuContent");
    var li = ul.getElementsByTagName("li");
    var clearSearch = document.getElementById('clearSearch');
    var dNoResult = document.createElement('div');
    var divContent = document.getElementById('noResult');
    var check = false;
    if (divContent != null) {
        divContent.parentElement.removeChild(divContent);
    }
    dNoResult.id = "noResult";
    for (var i = 0; i < li.length; i++) {
        var span0 = li[i].getElementsByTagName("span")[0];
        var span1 = li[i].getElementsByTagName("span")[1];
        if (span0.innerHTML.toUpperCase().indexOf(filter) > -1 || span1.innerHTML.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
            check = true;
        } else {
            li[i].style.display = "none";
            dNoResult.innerHTML = "No matches found";
            ul.appendChild(dNoResult);
        }
    }
    if (check == true) {
        dNoResult.innerHTML = "";
    }
    $('#clearSearch').click(function(e) {
        document.getElementById('menuSearchInput').value = "";
        clearSearch.style.display = "none";
        var divContent = document.getElementById('noResult');
        if (divContent != null) {
            divContent.parentElement.removeChild(divContent);
        }
        for (var i = 0; i < li.length; i++) {
            li[i].style.display = "";
        }
    });
    if (input.value != "") {
        clearSearch.style.display = "block";
    } else {
        clearSearch.style.display = "none";
    }
}
document.addEventListener('keyup', searchValue);

function loadInfo() {
    if (DOC.plr.iL) {
        document.getElementById('userImg').style.backgroundImage = "url('." + DOC.plr.lU + "')";
    }
    if (!DOC.plr.iL) {
        document.getElementById('userImg').style.display = "none";
    }
    if (DOC.plr.ti) {
        document.getElementById('headerTitle').innerHTML = DOC.plr.ti;
    }
    if (DOC.plr.u) {
        document.getElementById('userName').innerHTML = DOC.plr.u;
    } else if (!DOC.plr.ti && !DOC.plr.u && !DOC.plr.iL) {
        document.getElementById('mainHeader').style.display = "none";
    }
}
loadInfo();

function defineStorage() {
    if (!window.localStorage) {
        Object.defineProperty(window, "localStorage", new(function() {
            var aKeys = [],
                oStorage = {};
            Object.defineProperty(oStorage, "getItem", {
                value: function(sKey) { return sKey ? this[sKey] : null; },
                writable: false,
                configurable: false,
                enumerable: false
            });
            Object.defineProperty(oStorage, "key", {
                value: function(nKeyId) { return aKeys[nKeyId]; },
                writable: false,
                configurable: false,
                enumerable: false
            });
            Object.defineProperty(oStorage, "setItem", {
                value: function(sKey, sValue) {
                    if (!sKey) {
                        return;
                    }
                    document.cookie = escape(sKey) + "=" + escape(sValue) + "; expires=Tue, 19 Jan 2038 03:14:07 GMT; path=/";
                },
                writable: false,
                configurable: false,
                enumerable: false
            });
            Object.defineProperty(oStorage, "length", {
                get: function() { return aKeys.length; },
                configurable: false,
                enumerable: false
            });
            Object.defineProperty(oStorage, "removeItem", {
                value: function(sKey) {
                    if (!sKey) {
                        return;
                    }
                    document.cookie = escape(sKey) + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT; path=/";
                },
                writable: false,
                configurable: false,
                enumerable: false
            });
            Object.defineProperty(oStorage, "clear", {
                value: function() {
                    if (!aKeys.length) {
                        return;
                    }
                    for (var sKey in aKeys) {
                        document.cookie = escape(sKey) + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT; path=/";
                    }
                },
                writable: false,
                configurable: false,
                enumerable: false
            });
            this.get = function() {
                var iThisIndx;
                for (var sKey in oStorage) {
                    iThisIndx = aKeys.indexOf(sKey);
                    if (iThisIndx === -1) {
                        oStorage.setItem(sKey, oStorage[sKey]);
                    } else {
                        aKeys.splice(iThisIndx, 1);
                    }
                    delete oStorage[sKey];
                }
                for (aKeys; aKeys.length > 0; aKeys.splice(0, 1)) {
                    oStorage.removeItem(aKeys[0]);
                }
                for (var aCouple, iKey, nIdx = 0, aCouples = document.cookie.split(/\s*;\s*/); nIdx < aCouples.length; nIdx++) {
                    aCouple = aCouples[nIdx].split(/\s*=\s*/);
                    if (aCouple.length > 1) {
                        oStorage[iKey = unescape(aCouple[0])] = unescape(aCouple[1]);
                        aKeys.push(iKey);
                    }
                }
                return oStorage;
            };
            this.configurable = false;
            this.enumerable = true;
        })());
    }
}
try {
    defineStorage();
} catch (e) {}

function setNavigationControl(slideConfig, playerConfig) {
    var gridTemplateColumns = "";
    var prevControlBtn = document.getElementById('prevControlBtn');
    var controlSliderBtn = document.getElementById('controlSliderBtn');
    var nextControlBtn = document.getElementById('nextControlBtn');
    var volumeWrapper = document.getElementById('volumeWrapper');
    var sliderWrapper = document.getElementById('sliderWrapper');
    var timeSlider = document.getElementById('timeSlider');
    var restartControlBtn = document.getElementById('restartControlBtn');
    if (slideConfig.pfM == "Custom") {
        prevControlBtn.style.display = (slideConfig.pB ? "flex" : "none ");
        controlSliderBtn.style.display = (slideConfig.sbE ? "flex" : "none ");
        nextControlBtn.style.display = (slideConfig.nB ? "flex" : "none ");
        volumeWrapper.style.display = (playerConfig.iV ? "flex" : "none ");
        sliderWrapper.style.display = (slideConfig.sbE ? "flex" : "none ");
        timeSlider.style.display = (slideConfig.sbE ? "flex" : "none ");
        restartControlBtn.style.display = (slideConfig.sbE ? "flex" : "none ");
    } else {
        prevControlBtn.style.display = (slideConfig.pB ? "flex" : "none ");
        controlSliderBtn.style.display = (playerConfig.iSb ? "flex" : "none ");
        nextControlBtn.style.display = (slideConfig.nB ? "flex" : "none ");
        volumeWrapper.style.display = (playerConfig.iV ? "flex" : "none ");
        sliderWrapper.style.display = (playerConfig.iSb ? "flex" : "none ");
        timeSlider.style.display = (playerConfig.iSb ? "flex" : "none ");
        restartControlBtn.style.display = (playerConfig.iSb ? "flex" : "none ");
    }
}

function setSidebarControl(slideConfig, playerConfig) {
    var sideTabConfig = document.getElementById('sideTab');
    var tabMenu = document.getElementById('tabMenu');
    var tabResources = document.getElementById('tabResources');
    var tabGlossary = document.getElementById('tabGlossary');
    var tabNote = document.getElementById('tabNote');
    if (slideConfig.pfM == "Custom") {
        tabMenu.style.display = (slideConfig.mE ? "flex" : "none ");
        tabResources.style.display = (slideConfig.glE ? "flex" : "none ");
        tabGlossary.style.display = (slideConfig.reE ? "flex" : "none ");
        tabNote.style.display = (slideConfig.nE ? "flex" : "none ");
    } else {
        tabMenu.style.display = (playerConfig.iM ? "flex" : "none ");
        tabResources.style.display = (playerConfig.iG ? "flex" : "none ");
        tabGlossary.style.display = (playerConfig.iR ? "flex" : "none ");
        tabNote.style.display = (playerConfig.iN ? "flex" : "none ");
    }
    var indexArr = [playerConfig.mPo, playerConfig.rPo, playerConfig.glP, playerConfig.nPo];
    var objects = [tabMenu, tabResources, tabGlossary, tabNote];
    var count = 1;
    while (count < 5) {
        var tab = objects[indexArr.indexOf(count)];
        if (tab.style.display == "flex") {
            $('.tabs li').each(function() {
                var firstTab = 'order' + (indexArr.indexOf(count + 1));
                if ($(this).hasClass(firstTab)) {
                    $(this).addClass("tabSelected");
                    $('#contentSidebar section').removeClass("contentSidebarSelected");
                    $("#" + $(this).data('id')).addClass("contentSidebarSelected");
                }
            });
            break;
        }
        count++;
    }
}

function genResourceContent() {
    var resourceContent = document.getElementById('resourceContent');
    var resourceDescription = document.getElementById('resourceDescription');
    resourceDescription.innerHTML = DOC.plr.rDes;
    if (DOC.plr.res) {
        for (var r = 0; r < DOC.plr.res.length; r++) {
            var res = DOC.plr.res[r];
            var aResource = GlobalHelper.createTags('a', '', 'links');
            aResource.setAttribute('title', res.ti);
            aResource.setAttribute('target', '_blank');
            if (res.u) {
                aResource.setAttribute('href', res.u);
            }
            if (res.fU) {
                aResource.setAttribute('href', res.fU);
            }
            aResource.innerHTML = res.ti;
            resourceContent.appendChild(aResource);
        }
    }
}

function genGlossary() {
    var glossaryTermWrapper = document.getElementById('glossaryTermWrapper');
    var termWrapper = document.getElementById('termWrapper');
    var glossaryDefinitionWrapper = document.getElementById('glossaryDefinitionWrapper');
    var definitionContent = document.getElementById('definitionContent');
    menuContentWrapper.style.top = "0px";
    document.getElementById('menuScrollBar').style.top = "0px";

    if (DOC.plr.gl) {
        for (var g = 0; g < DOC.plr.gl.length; g++) {
            var gl = DOC.plr.gl[g];
            if (gl.tm) {
                var liTerm = GlobalHelper.createTags('li', '', 'termContent');
                liTerm.setAttribute('title', gl.tm);
                liTerm.setAttribute('data-term', 'def' + g);
                var spTerm = GlobalHelper.createTags('span', '', 'term');
                spTerm.innerHTML = gl.tm;
                liTerm.appendChild(spTerm);
                termWrapper.appendChild(liTerm);
            }
            if (gl.def) {
                var liDef = GlobalHelper.createTags('li', '', 'defContent');
                liDef.setAttribute('title', gl.def);
                var spDef = GlobalHelper.createTags('span', '', 'definition');
                liDef.id = 'def' + g;
                spDef.innerHTML = gl.def;
                liDef.appendChild(spDef);
                definitionContent.appendChild(liDef);
            }
        }
    }
}

function genNote(slideData) {
    var noteContentWrapper = document.getElementById('noteContentWrapper');
    noteContentWrapper.innerHTML = "";
    var pNote = document.createElement('p');
    pNote.innerHTML = slideData.nt;
    pNote.className = "noteContent";
    noteContentWrapper.appendChild(pNote);
}

function defaultPlayer() {
    if (DOC.plr.iM) {
        genMenu();
    }
    if (DOC.plr.iR) {
        var resourceDescription = document.getElementById('resourceDescription');
        resourceDescription.innerHTML = DOC.plr.rDes;
        genResourceContent();
    }
    if (DOC.plr.iG) {
        genGlossary();
    }
}
defaultPlayer();

function customPlayer(slideConfig) {
    if (slideConfig.mE) {
        genMenu();
    }
    if (slideConfig.reE) {
        genResourceContent();
    }
    if (slideConfig.glE) {
        genGlossary();
    }
}

function loadSubmitBtn(slideConfig) {
    if (slideConfig.sB) {
        document.getElementById('submitControlBtn').style.display = "flex";
    }
}

function tabPosition() {
    $("#tabMenu").addClass('order' + DOC.plr.mPo);
    $("#tabResources").addClass('order' + DOC.plr.rPo);
    $("#tabGlossary").addClass('order' + DOC.plr.glP);
    $("#tabNote").addClass('order' + DOC.plr.nPo);
}
tabPosition();
$('#menuContent li').click(function(e) {
    $('#menuContent li').removeClass("menuSelected");
    $(this).addClass("menuSelected");
});
if (DOC.plr.gl) {
    $('#termWrapper li').click(function (e) {
        $("#termWrapper li").removeClass("termSelected");
        $(this).addClass("termSelected");
        $("#definitionContent li").removeClass("defSelected");
        $("#" + $(this).data('term')).addClass("defSelected");
    });
};
function onBodyResize() {
    calculateSize();
    heightThumbScroll();
    checkScroll();
    menuContentWrapper.removeEventListener("wheel", mouseMenuWheel);
    menuContentWrapper.addEventListener("wheel", mouseMenuWheel);
}
onBodyResize();

function calculateSize() {
    var bodyWidth = document.body.offsetWidth;
    var bodyHeight = document.body.offsetHeight;
    var wrapper = document.getElementById('wrapper');
    var widthSidebar = document.getElementById('sidePanel').offsetWidth;
    var heightNavigation = document.getElementById('controlPanel').offsetHeight;
    var content = document.getElementById('container');
    var baseSizeContent = {
        w: content.offsetWidth,
        h: content.offsetHeight
    };
    var currentSizeContent = {
        w: bodyWidth - widthSidebar,
        h: bodyHeight - heightNavigation
    };
    var widthRatio = currentSizeContent.w / baseSizeContent.w;
    var heightRatio = currentSizeContent.h / baseSizeContent.h;
    var ratio = 1;
    if (widthRatio > heightRatio) {
        wrapper.style.height = "100%";
        wrapper.style.width = 100 * (widthSidebar + heightRatio * baseSizeContent.w) / bodyWidth + "%";
        ratio = heightRatio;
    } else {
        wrapper.style.width = "100%";
        wrapper.style.height = 100 * (heightNavigation + widthRatio * baseSizeContent.h) / bodyHeight + "%";
        ratio = widthRatio;
    }
    content.style.transform = 'matrix(' + ratio + ', 0, 0, ' + ratio + ', 0, 0)';
}
var PlayerControl = function() {
    return {
        playPauseClick: function() {
            var timeLine = null;
            if (GlobalResources.CurrentSlide != null && GlobalResources.CurrentSlide.MainTimeline != null) {
                timeLine = GlobalResources.CurrentSlide.MainTimeline;
            }
            if (timeLine != null) {
                if (timeLine.paused()) {
                    timeLine.play();
                    this.playIcon();
                    mediaControl("play");
                    if (timeLine.progress() === 1) {
                        GlobalHelper.reset();
                    }
                } else {
                    timeLine.pause();
                    this.pauseIcon();
                    mediaControl("pause");
                }
            }
        },
        playIcon: function() {
            document.getElementById('pause').style.display = "inline-block";
            document.getElementById('play').style.display = "none";
        },
        pauseIcon: function() {
            document.getElementById('play').style.display = "inline-block";
            document.getElementById('pause').style.display = "none";
        },
        resetClick: function() {
            GlobalHelper.reset();
        },
        nextClick: function() {
            triggerExcute("NextButton");
        },
        previousClick: function() {
            triggerExcute("PreviousButton");
        },
    };

    function triggerExcute(target) {
        var layers = GlobalResources.CurrentSlide.Data.lts;
        var divRoot = GlobalResources.CurrentLayer;
        var currentLayer = null;
        var checkStatus = true;
        for (var i = 0; i < layers.length; i++) {
            if (layers[i].id + "_w" == divRoot.id) {
                currentLayer = layers[i];
                break;
            }
        }
        if (currentLayer != null) {
            if (currentLayer.tri != null) {
                currentLayer.tri.forEach(function(trigger) {
                    if (trigger.Ta == target) {
                        TriggerActionModule.getTriggerAction(trigger);
                        checkStatus = false;
                    }
                });
            }
        }
        if (checkStatus) {
            if (target == "NextButton") {
                GlobalHelper.nextSlide();
            } else if (target == "PreviousButton") {
                GlobalHelper.previousSlide();
            }
        }
    }

    function mediaControl(stage) {
        var videos = $("video");
        var audios = $("audio");
        if (videos.length > 0) {
            switch (stage) {
                case "pause":
                    for (var i = 0; i < videos.length; i++) {
                        videos[i].pause();
                    }
                    if (audios.length > 0) {
                        for (var i = 0; i < audios.length; i++) {
                            audios[i].pause();
                        }
                    }
                    break;
                case "play":
                    for (var i = 0; i < videos.length; i++) {
                        videos[i].play();
                    }
                    if (audios.length > 0) {
                        for (var i = 0; i < audios.length; i++) {
                            audios[i].play();
                        }
                    }
                    break;
                case "stop":
                    for (var i = 0; i < videos.length; i++) {
                        videos[i].currentTime = 0;
                        videos[i].pause();
                    }
                    if (audios.length > 0) {
                        for (var i = 0; i < audios.length; i++) {
                            videos[i].currentTime = 0;
                            audios[i].pause();
                        }
                    }
                    break;
            }
        }
    }
}();