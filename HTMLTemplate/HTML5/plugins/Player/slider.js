var SliderModule = function () {
    return {
        initSlider: function () {
            $("#slider").slider({
                range: false,
                min: 0,
                max: 100,
                step: .1,
                slide: function (event, ui) {
                    var timeLine = null;
                    if (GlobalResources.CurrentSlide != null && GlobalResources.CurrentSlide.MainTimeline != null) {
                        timeLine = GlobalResources.CurrentSlide.MainTimeline;
                    }
                    if (timeLine != null) {
                        timeLine.pause();
                        timeLine.progress(ui.value / 100);
                    }
                }
            });
        },
        updateSlider: function () {
            var timeLine = null;
            if (GlobalResources.CurrentSlide != null && GlobalResources.CurrentSlide.MainTimeline != null) {
                timeLine = GlobalResources.CurrentSlide.MainTimeline;
            }
            if (timeLine != null) {
                $("#slider").slider("value", timeLine.progress() * 100);
                $("#progress").css("width", timeLine.progress() * 100 + "%");
                document.getElementById('currentTime').innerText = timeLine.time().toString().toHHMMSS();
                document.getElementById('totalTime').innerText = timeLine.duration().toString().toHHMMSS();
                if (timeLine.progress() === 1) {
                    timeLine.pause();
                }
                if (timeLine.paused()) {
                    document.getElementById('play').style.display = "inline-block";
                    document.getElementById('pause').style.display = "none";
                }
                else {
                    document.getElementById('pause').style.display = "inline-block";
                    document.getElementById('play').style.display = "none";
                }
            }
        }
    };
}();
SliderModule.initSlider();
