"use strict";
Array.prototype.groupBy = function (prop) {
    return this.reduce(function (groups, item) {
        var val = item[prop];
        groups[val] = groups[val] || [];
        groups[val].push(item);
        return groups;
    }, {});
};
var ComboChart = (function () {
    function ComboChart(chart) {
        this.chart = chart;
        this.data = chart.data;
        this.primaryMajorHorizontal = chart.options.DataGridline.IsPrimaryMajorHorizontal;
        this.primaryMinorHorizontal = chart.options.DataGridline.IsPrimaryMinorHorizontal;
        this.primaryMajorVertical = chart.options.DataGridline.IsPrimaryMajorVertical;
        this.primaryMinorVertical = chart.options.DataGridline.IsPrimaryMinorVertical;
        this.colorMajorHorizontal = chart.options.DataGridline.StrokeMajorHorizontal;
        this.colorMinorHorizontal = chart.options.DataGridline.StrokeMajorHorizontal;
        this.colorMajorVertical = chart.options.DataGridline.StrokeMajorVertical;
        this.colorMinorVertical = chart.options.DataGridline.StrokeMinorVertical;
        this.arrIndexSeriPrimary = chart.arrOptionAxis()[0];
        this.arrIndexSeriSecondary = chart.arrOptionAxis()[1];
        if (this.chart.options.ConfigSeriOption.PrimaryAxis) {
            this.gapWidthPrimary = chart.options.ConfigSeriOption.PrimaryAxis.GapWidth;
        }
        if (chart.options.ConfigSeriOption.SecondaryAxis) {
            this.gapWidthSecondary = chart.options.ConfigSeriOption.SecondaryAxis.GapWidth;
        }
        this.truongHopPrimary = this.caseStacked100(true);
        this.truongHopSecondary = this.caseStacked100(false);
        this.fillArea = this.fillArea();
    }
    ComboChart.prototype.fillArea = function () {
        var dataSeries = this.data.map(function (x) {
            return x.Series;
        });
        var combineData = [];
        for (var i = 0; i < dataSeries.length; i++) {
            combineData = combineData.concat(dataSeries[i]);
        }
        if (combineData.length !== 0) {
            var dataGroupByAxis = combineData.groupBy("Type");
            var keyTypeChart = Object.keys(dataGroupByAxis);
            var countType = 0;
            var arrTypeChart = keyTypeChart.map(function (x) {
                return x.split("_")[0];
            });
            if (isExistInArray(arrTypeChart, "ColumnChart")) {
                countType++;
            }
            if (isExistInArray(arrTypeChart, "LineChart")) {
                countType++;
            }
            if (isExistInArray(arrTypeChart, "AreaChart")) {
                countType++;
            }
            if (countType > 1) {
                return false;
            }
        }
        return true;
    };
    ComboChart.prototype.gChart = function () {
        var gChart = GlobalHelper.createTags('g', '', 'chart');
        this.chart.gChart = gChart;
        var rect = GlobalHelper.createTags('rect');
        rect.setAttribute('width', this.chart.wCanvas);
        rect.setAttribute('height', this.chart.hCanvas);
        var gFill = FillColorHelper.genGTag(this.chart.options.Fill, { "id": this.chart.options.id, "width": this.chart.wCanvas, "height": this.chart.hCanvas });
        if (gFill) {
            if (gFill.Tag)
                GlobalHelper.appendIdContent(gChart, gFill.Tag);
        }
        rect.setAttribute("fill", gFill.ID);
        rect.setAttribute('id', this.chart.options.ID + "_r");
        this.chart.gChart.appendChild(rect);
        var dataPrimary = [];
        var dataSecondary = [];
        var dataPrimaryGroupByAxis = [];
        var dataSecondaryGroupByAxis = [];
        var dataSeries = this.data.map(function (x) {
            return x.Series;
        });
        var combineData = [];
        for (var i = 0; i < dataSeries.length; i++) {
            combineData = combineData.concat(dataSeries[i]);
        }
        if (combineData.length !== 0) {
            var dataGroupByAxis = combineData.groupBy("Type");
            var keyTypeChart = Object.keys(dataGroupByAxis);
            var countType = 0;
            var arrTypeChart = keyTypeChart.map(function (x) {
                return x.split("_")[0];
            });
            if (isExistInArray(arrTypeChart, "ColumnChart")) {
                countType++;
            }
            if (isExistInArray(arrTypeChart, "LineChart")) {
                countType++;
            }
            if (isExistInArray(arrTypeChart, "AreaChart")) {
                countType++;
            }
            var fillArea = true;
            if (countType > 1) {
                fillArea = false;
            }
            var indexOfIndexToDraw = [];
            indexOfIndexToDraw = this.indexOfIndexToDraw(keyTypeChart);
            var dataProcess = [];
            var analyst = [];
            var maxDataPrimary = 0;
            var maxDataSecondary = 0;
            for (var stt = 0; stt < indexOfIndexToDraw.length; stt++) {
                dataProcess = dataGroupByAxis[keyTypeChart[stt]];
                analyst = new AnalystData(dataProcess, keyTypeChart[stt], this.chart);
                if (analyst.dataProcess[2].length > 0) {
                    var primary = analyst.getMaxData(analyst.dataProcess[0]);
                    if (maxDataPrimary < primary) {
                        maxDataPrimary = primary;
                    }
                }
                if (analyst.dataProcess[3].length > 0) {
                    var secondary = analyst.getMaxData(analyst.dataProcess[1]);
                    if (maxDataSecondary < secondary) {
                        maxDataSecondary = secondary;
                    }
                }
            }
            var maxValAxisPrimary = 0, maxValAxisSecondary = 0;
            var stepPrimary, stepSecondary;
            stepPrimary = CalculatorStep(maxDataPrimary);
            stepSecondary = CalculatorStep(maxDataSecondary);
            maxValAxisPrimary = this.chart.maxValAxis(maxDataPrimary, stepPrimary);
            if (maxDataSecondary > 0) {
                maxValAxisSecondary = this.chart.maxValAxis(maxDataSecondary, stepSecondary);
            }
            if (this.truongHopPrimary === 1) {
                maxValAxisPrimary = 1;
                stepPrimary = 0.1;
            }
            if (this.truongHopSecondary === 1) {
                maxValAxisSecondary = 1;
                stepSecondary = 0.1;
            }
            var gGridLine = this.drawGridLine(maxValAxisPrimary, maxValAxisSecondary, stepPrimary, stepSecondary);
            gChart.appendChild(gGridLine);
            var gDrawChart = GlobalHelper.createTags('g', '', 'drawChart');
            gDrawChart.setAttribute('opacity', this.chart.options.Data[0].Series[0].Opacity);
            this.chart.gChart.appendChild(gDrawChart);
            var getAllTypeChart = keyTypeChart.map(function (x) {
                return x.split("_")[1];
            });
            if (isExistInArray(getAllTypeChart, "Stacked100")) {
                var seriesOption = this.chart.seriesOption;
                for (var m = 0; m < 2; m++) {
                    var dataPrimaryOrSecondary = seriesOption.filter(function (x) {
                        if (x.SeriOption === m + 1)
                            return x;
                    }).groupBy("Type");
                    var typeChartsPrimaryOrSecondary = Object.keys(dataPrimaryOrSecondary);
                    var indexOfIndexToDrawSecond = this.indexOfIndexToDraw(typeChartsPrimaryOrSecondary);
                    var index;
                    var currentTypeChart;
                    for (var n = 0; n < typeChartsPrimaryOrSecondary.length; n++) {
                        var index;
                        var currentTypeChart;
                        for (stt = 0; stt < indexOfIndexToDraw.length; stt++) {
                            index = indexOfIndexToDraw[stt];
                            currentTypeChart = keyTypeChart[index];
                            dataProcess = dataGroupByAxis[currentTypeChart];
                            analyst = new AnalystData(dataProcess, currentTypeChart, this.chart);
                            if (currentTypeChart.split("_")[0] === "AreaChart") {
                                this.drawChart(analyst, keyTypeChart[index], maxValAxisPrimary, maxValAxisSecondary, stepPrimary, stepSecondary, 0, this.fillArea);
                            }
                            else {
                                this.drawChart(analyst, keyTypeChart[index], maxValAxisPrimary, maxValAxisSecondary, stepPrimary, stepSecondary, 0, this.fillArea);
                            }
                        }
                    }
                }
            }
            else {
                var index;
                var currentTypeChart;
                for (stt = 0; stt < indexOfIndexToDraw.length; stt++) {
                    index = indexOfIndexToDraw[stt];
                    currentTypeChart = keyTypeChart[index];
                    dataProcess = dataGroupByAxis[currentTypeChart];
                    analyst = new AnalystData(dataProcess, currentTypeChart, this.chart);
                    this.drawChart(analyst, keyTypeChart[index], maxValAxisPrimary, maxValAxisSecondary, stepPrimary, stepSecondary, 0, this.fillArea);
                }
            }
        }
        if (this.chart.options.DataTable.IsDataTableKey) {
            this.chart.displayDataTable();
            var indexProcess = this.indexProcess(keyTypeChart, indexOfIndexToDraw, dataGroupByAxis);
            gChart.appendChild(this.fillTextDataTable(indexProcess));
        }
        else {
            var categoryTranslate = false;
            if (isExistInArray(arrTypeChart, "AreaChart") && fillArea) {
                categoryTranslate = true;
            }
            var gCategoryName = this.chart.fillTextCategory(categoryTranslate);
            if (gCategoryName) {
                gCategoryName.setAttribute('class', 'categoryName_Chart');
                gChart.appendChild(gCategoryName);
            }
        }
        var gLegend = this.chart.drawLegendTemplate();
        gChart.appendChild(gLegend);
        this.chart.fillTextAxisTitle();
        if (this.chart.options.DataLabel.Texts.length > 0) {
            var gDataLabel = this.chart.drawDataLabel(this.chart.options.DataLabel.Texts);
            gDataLabel.setAttribute('class', 'dataLabel');
            gChart.appendChild(gDataLabel);
        }
        if (this.chart.options.DataChartTitle) {
            var chartTitle = this.chart.options.DataChartTitle;
            var point = new Point(chartTitle.Left, chartTitle.Top);
            var text = new TextChart("chartTitle" + this.chart.options.id, chartTitle.Text, point, chartTitle.FontFamily, chartTitle.FontSize, chartTitle.Stroke.Color);
            var gTextChartTitle = text.getElement();
            gTextChartTitle.setAttribute('class', 'chartTitle');
            gChart.appendChild(gTextChartTitle);
        }
        return gChart;
    };
    ComboChart.prototype.indexOfIndexToDraw = function (keyTypeChart) {
        var i;
        var indexToDraw = this.prioty(keyTypeChart);
        var pointAxis = [];
        for (var n = 0; n < keyTypeChart.length; n++) {
            pointAxis.push(this.chart.seriesOption.filter(function (x) {
                return x.Type === keyTypeChart[n];
            })[0].SeriOption);
        }
        var arrPrioty = [];
        for (n = 0; n < keyTypeChart.length; n++) {
            arrPrioty[n] = indexToDraw[n] + (pointAxis[n] - 1) / 2;
        }
        var indexSorted = [];
        for (i = 0; i < arrPrioty.length; i++) {
            indexSorted.push(arrPrioty[i]);
        }
        indexSorted.sort();
        var indexOfIndexToDraw = [];
        for (i = 0; i < indexSorted.length; i++) {
            for (var j = 0; j < arrPrioty.length; j++) {
                if (indexSorted[i] === arrPrioty[j]) {
                    indexOfIndexToDraw.push(j);
                    break;
                }
            }
        }
        return indexOfIndexToDraw;
    };
    ComboChart.prototype.caseStacked100 = function () {
        var primary = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;
        var index = 1;
        if (!primary) {
            index = 2;
        }
        var seriesOption = this.chart.seriesOption;
        var keys = Object.keys(this.chart.seriesOption.groupBy("SeriOption"));
        var isPrimaryOrSecondary = false;
        var isAllStacked100 = false;
        if (isExistInArray(keys, index + "")) {
            isPrimaryOrSecondary = true;
        }
        if (isPrimaryOrSecondary) {
            var dataPrimaryOrSecondary = seriesOption.filter(function (x) {
                return x.SeriOption === index;
            }).groupBy("Type");
            var typeChartsPrimaryOrSecondary = Object.keys(dataPrimaryOrSecondary);
            var arr = [];
            for (var i = 0; i < typeChartsPrimaryOrSecondary.length; i++) {
                arr.push(typeChartsPrimaryOrSecondary[i].split("_")[1]);
            }
            if (!isExistInArray(arr, "Stacked") && !isExistInArray(arr, "Clustered")) {
                return 1;
            }
            else {
                var indexOfIndexToDraw = this.indexOfIndexToDraw(typeChartsPrimaryOrSecondary);
                var firstTypeDraw = typeChartsPrimaryOrSecondary[indexOfIndexToDraw[0]].split("_")[1];
                if (firstTypeDraw === "Stacked100") {
                    return 2;
                }
                else {
                    return 3;
                }
            }
        }
        return 0;
    };
    ComboChart.prototype.indexProcess = function (keyTypeChart, indexOfIndexToDraw, dataGroupByAxis) {
        var currentTypeChart;
        var currentIndex, currentKey;
        var keyPrimarys = [];
        var keySecondarys = [];
        var analyst;
        var indexProcess = [];
        var dataProcess = [];
        var j;
        for (var i = 0; i < keyTypeChart.length; i++) {
            currentIndex = indexOfIndexToDraw[i];
            currentTypeChart = keyTypeChart[currentIndex];
            dataProcess = dataGroupByAxis[currentTypeChart];
            analyst = new AnalystData(dataProcess, currentTypeChart, this.chart);
            analyst = analyst.analyst();
            keyPrimarys = Object.keys(analyst[2].groupBy("ID"));
            keySecondarys = Object.keys(analyst[3].groupBy("ID"));
            if (currentTypeChart.split("_")[1] !== "Clustered") {
                keyPrimarys.reverse();
                keySecondarys.reverse();
            }
            for (j = 0; j < keyPrimarys.length; j++) {
                indexProcess.push(keyPrimarys[j]);
            }
            for (j = 0; j < keySecondarys.length; j++) {
                indexProcess.push(keySecondarys[j]);
            }
        }
        return indexProcess;
    };
    ComboChart.prototype.fillTextDataTable = function (indexProcess) {
        var dataTableOfChartElement = this.chart.options.DataTable.IsWithLegendKey;
        var heightCategory = this.chart.dataTable.HeightTable.HeightCategory;
        var numSeri = this.chart.numSeri();
        var numCategory = this.chart.numCategory();
        var avgWidth = this.chart.avgWidth();
        var fontFamily = this.chart.options.DataTable.DataTable.FontFamily;
        var fontSize = this.chart.options.DataTable.DataTable.FontSize;
        var colorText = this.chart.options.DataTable.DataTable.Stroke.Color;
        var xDataTable = this.chart.xChart - this.chart.dataTable.DataTable.WidthLeftTable;
        var yOx = this.chart.yChart + this.chart.hChart;
        var heightSum = 0;
        var gTextDataTable = GlobalHelper.createTags('g', '', 'textDataTable');
        for (var q = 0; q < numSeri; q++) {
            heightSum += this.chart.dataTable.HeightTable.Series[q].Height;
        }
        var dataTexts = [];
        var categoryName = this.chart.getCategory();
        var getArrData = this.chart.getArrData();
        var indexProcess = indexProcess;
        for (var i = 0; i < numCategory; i++) {
            var point = new Point(this.chart.xChart + avgWidth * i + avgWidth / 2, yOx + heightCategory / 2);
            GenerateText(dataTexts, categoryName[i], categoryName[i], point, fontFamily, fontSize, colorText, "middle", "middle");
        }
        var heightSumStep = 0;
        for (var _i = 0; _i < numCategory; _i++) {
            heightSumStep = 0;
            for (var j = 0; j < numSeri; j++) {
                heightSumStep += this.chart.options.DataTable.HeightTable.Series[j].Height;
                var point = new Point(this.chart.xChart + avgWidth * _i + avgWidth / 2, yOx + heightCategory + heightSumStep - this.chart.options.DataTable.HeightTable.Series[j].Height / 2);
                GenerateText(dataTexts, "value" + categoryName[_i], getArrData[_i][indexProcess[j] - 1].Value, point, fontFamily, fontSize, colorText, "middle", "middle");
            }
        }
        if (!dataTableOfChartElement) {
            heightSumStep = 0;
            for (var _j = 0; _j < numSeri; _j++) {
                heightSumStep += this.chart.options.DataTable.HeightTable.Series[_j].Height;
                var point = new Point(xDataTable + (this.chart.xChart - xDataTable) / 2, yOx + heightCategory + heightSumStep - this.chart.options.DataTable.HeightTable.Series[_j].Height / 2);
                GenerateText(dataTexts, "value", this.chart.getArrSeries()[indexProcess[_j] - 1], point, fontFamily, fontSize, colorText, "middle", "middle");
            }
        }
        else if (dataTableOfChartElement) {
            heightSumStep = 0;
            var gLegendsDataTable = GlobalHelper.createTags('g', '', 'legendsDatatable');
            for (var _j2 = 0; _j2 < numSeri; _j2++) {
                var seriTable = this.chart.dataTable.HeightTable.Series[_j2];
                heightSumStep += seriTable.Height;
                var w = seriTable.WidthLegend;
                var h = seriTable.HeightLegend;
                var xSquare = this.chart.xChart - this.chart.dataTable.DataTable.WidthTableLegend;
                var ySquare;
                var xText = this.chart.xChart - this.chart.dataTable.DataTable.WidthTableSeri;
                var yText = yOx + heightCategory + heightSumStep - seriTable.Height / 2;
                var pointText = new Point(xText, yText);
                var thickness = this.chart.options.DataTable.DataTable.Thickness;
                var text = "text";
                var font = fontFamily;
                var size = fontSize;
                var color = colorText;
                var textAlign = "start";
                var textBaseline = "middle";
                var currentSeriOption = this.chart.seriesOption[indexProcess[_j2] - 1];
                var colorSquare = currentSeriOption.Fill;
                var type = this.typeShapeLegend(currentSeriOption.Type);
                if (type === "rect") {
                    ySquare = yText - seriTable.HeightLegend / 2;
                }
                else {
                    ySquare = yText;
                }
                var point = new Point(xSquare, ySquare, colorSquare);
                gLegendsDataTable.appendChild(ChartModule.drawLegend(seriTable.id, point, w, h, pointText, thickness, currentSeriOption.Name, font, size, color, textAlign, textBaseline, type));
            }
            gTextDataTable.appendChild(gLegendsDataTable);
        }
        gTextDataTable.appendChild(drawText(dataTexts, this.ctx));
        return gTextDataTable;
    };
    ComboChart.prototype.typeShapeLegend = function (type) {
        if (type.split("_")[0] === "LineChart") {
            return "line";
        }
        else {
            return "rect";
        }
    };
    ComboChart.prototype.drawChart = function (analyst, keyTypeChart, maxValAxisPrimary, maxValAxisSecondary, stepPrimary, stepSecondary) {
        var truongHop = arguments.length > 6 && arguments[6] !== undefined ? arguments[6] : 0;
        var fillArea = arguments.length > 7 && arguments[7] !== undefined ? arguments[7] : true;
        var typeChart = typeChartAcordingKey(keyTypeChart);
        var analystChart;
        switch (typeChart) {
            case "ColumnChart":
                analystChart = new AnalystDataColumnChart(analyst, keyTypeChart, maxValAxisPrimary, maxValAxisSecondary, stepPrimary, stepSecondary, truongHop);
                analystChart.draw();
                break;
            case "LineChart":
                analystChart = new AnalystDataLineChart(analyst, keyTypeChart, maxValAxisPrimary, maxValAxisSecondary, stepPrimary, stepSecondary, truongHop);
                analystChart.draw();
                break;
            case "AreaChart":
                analystChart = new AnalystDataAreaChart(analyst, keyTypeChart, maxValAxisPrimary, maxValAxisSecondary, stepPrimary, stepSecondary, truongHop, fillArea);
                analystChart.draw();
                break;
        }
    };
    ComboChart.prototype.drawGridLine = function (maxValAxisPrimary, maxValAxisSecondary, stepPrimary, stepSecondary) {
        var gridLine = new GridLine(this.primaryMajorHorizontal, this.primaryMajorVertical, this.primaryMinorHorizontal, this.primaryMinorVertical, this.colorMajorHorizontal, this.colorMinorHorizontal, this.colorMajorVertical, this.colorMinorVertical);
        var xMin, yMin;
        var g = GlobalHelper.createTags('g', '', 'gridLine');
        if (maxValAxisPrimary > 0) {
            xMin = this.chart.xChart;
            yMin = this.chart.yChart;
            if (this.truongHopPrimary === 1) {
                maxValAxisPrimary = 1;
                stepPrimary = 0.1;
            }
            var gPrimaryGridLine = this.chart.drawGrid(gridLine, maxValAxisPrimary, stepPrimary, xMin, yMin);
            gPrimaryGridLine.setAttribute('class', 'gridLine_primary');
            g.appendChild(gPrimaryGridLine);
            var gTextPrimaryGrid = this.chart.textPrimaryGrid(maxValAxisPrimary, stepPrimary, this.truongHopPrimary);
            if (gTextPrimaryGrid) {
                gTextPrimaryGrid.setAttribute('class', 'textGridLine_primary');
                g.appendChild(gTextPrimaryGrid);
            }
        }
        if (maxValAxisSecondary > 0) {
            xMin = this.chart.xChart;
            yMin = this.chart.yChart;
            var marginSecondary = 0;
            if (this.truongHopSecondary === 1) {
                maxValAxisSecondary = 1;
                stepSecondary = 0.1;
            }
            var gSecondaryGridLine = GlobalHelper.createTags('g', '', 'gridLine_primary');
            if (maxValAxisPrimary === 0) {
                var gLineOfGridLine_Secondary = this.chart.drawGrid(gridLine, maxValAxisSecondary, stepSecondary, xMin, yMin);
                gLineOfGridLine_Secondary.setAttribute('class', 'textGridLine_secondary');
                g.appendChild(gLineOfGridLine_Secondary);
            }
            var gTextSecondaryGrid = this.chart.textSecondaryGrid(maxValAxisSecondary, stepSecondary, this.truongHopSecondary);
            gTextSecondaryGrid.setAttribute('class', 'textGridLine_secondary');
            g.appendChild(gTextSecondaryGrid);
        }
        return g;
    };
    ComboChart.prototype.prioty = function (keyTypeChart) {
        var points = [];
        var point;
        for (var m = 0; m < keyTypeChart.length; m++) {
            switch (keyTypeChart[m].split("_")[0]) {
                case "AreaChart":
                    point = 1;
                    break;
                case "ColumnChart":
                    point = 2;
                    break;
                case "LineChart":
                    point = 3;
                    break;
                case "PointChart":
                    point = 4;
                    break;
            }
            points.push(point);
        }
        return points;
    };
    return ComboChart;
}());
var AnalystData = (function () {
    function AnalystData(getData, typeChart, chart) {
        this.getData = getData;
        this.typeChart = typeChart.split("_")[0];
        this.indexTemplate = typeChart.split("_")[1];
        this.chart = chart;
        this.dataProcess = this.analyst();
        if (this.indexTemplate != "Stacked100") {
            this.maxDataPrimary = this.maxDataVal(this.dataProcess[0]);
            this.maxDataSecondary = this.maxDataVal(this.dataProcess[1]);
        }
        else if (this.indexTemplate === "Stacked100") {
            this.maxDataPrimary = 1;
            this.maxDataSecondary = 1;
        }
    }
    AnalystData.prototype.getMaxData = function (dataProcess) {
        if (this.indexTemplate != "Stacked100") {
            return this.maxDataVal(dataProcess);
        }
        else if (this.indexTemplate === "Stacked100") {
            return 0.952;
        }
    };
    AnalystData.prototype.analyst = function () {
        var analyst;
        if (this.indexTemplate === "Clustered") {
            var analystDataClustered = new AnalystDataClustered(this);
            analyst = analystDataClustered.getAllData();
        }
        else if (this.indexTemplate === "Stacked" || this.indexTemplate === "Stacked100") {
            var analystDataStacked = new AnalystDataStacked(this);
            analyst = analystDataStacked.getAllData();
        }
        return analyst;
    };
    AnalystData.prototype.maxDataVal = function (arrParent) {
        var sizeArr = arrParent.length;
        var maxData = 0;
        if (sizeArr > 0) {
            for (var i = 0; i < sizeArr; i++) {
                for (var j = 0; j < arrParent[0].length; j++) {
                    if (arrParent[i][j] > maxData) {
                        maxData = arrParent[i][j];
                    }
                }
            }
        }
        return maxData;
    };
    return AnalystData;
}());
var AnalystDataClustered = (function () {
    function AnalystDataClustered(analyst) {
        this.analyst = analyst;
        this.getData = analyst.getData;
        this.chart = analyst.chart;
    }
    AnalystDataClustered.prototype.getAllData = function () {
        if (Object.keys(this.getData).length > 0) {
            var data = this.getData;
            var dataPrimary = [];
            var dataSecondary = [];
            dataPrimary = data.groupBy("IsPrimaryAxis").true;
            dataSecondary = data.groupBy("IsPrimaryAxis").false;
            if (!dataPrimary) {
                dataPrimary = [];
            }
            if (!dataSecondary) {
                dataSecondary = [];
            }
            var dataGroupId;
            var keysChild;
            var numCategory = this.chart.numCategory();
            var analystPrimary = this.processData(dataPrimary, numCategory);
            var analystSecondary = this.processData(dataSecondary, numCategory);
            var analystData = [];
            analystData.push(analystPrimary, analystSecondary, dataPrimary, dataSecondary);
            return analystData;
        }
    };
    AnalystDataClustered.prototype.processData = function (data, numCategory) {
        if (data) {
            var dataGroupId = data.groupBy("ID");
            var keysChild = Object.keys(dataGroupId);
            var arrParent = [];
            var arrChild = [];
            if (keysChild.length > 0) {
                for (var i = 0; i < numCategory; i++) {
                    arrChild = [];
                    for (var j = 0; j < keysChild.length; j++) {
                        arrChild.push(dataGroupId[keysChild[j]][i].Value);
                    }
                    arrParent.push(arrChild);
                }
            }
            return arrParent;
        }
    };
    return AnalystDataClustered;
}());
var AnalystDataStacked = (function () {
    function AnalystDataStacked(analyst) {
        this.analyst = analyst;
        this.getData = analyst.getData;
        this.chart = analyst.chart;
    }
    AnalystDataStacked.prototype.getAllData = function () {
        if (Object.keys(this.getData).length > 0) {
            var data = this.getData;
            var dataPrimary = [];
            var dataSecondary = [];
            dataPrimary = data.groupBy("IsPrimaryAxis").true;
            dataSecondary = data.groupBy("IsPrimaryAxis").false;
            if (!dataPrimary) {
                dataPrimary = [];
            }
            if (!dataSecondary) {
                dataSecondary = [];
            }
            var dataGroupId;
            var numCategory = this.chart.numCategory();
            var analystPrimary = this.processData(dataPrimary, numCategory);
            var analystSecondary = this.processData(dataSecondary, numCategory);
            var analystData = [];
            analystData.push(analystPrimary, analystSecondary, dataPrimary, dataSecondary);
            return analystData;
        }
    };
    AnalystDataStacked.prototype.processData = function (data, numCategory) {
        if (data) {
            var dataGroupId = data.groupBy("ID");
            var keysChild = Object.keys(dataGroupId);
            var arrParent = [];
            var arrChild = [];
            var sum = 0;
            if (keysChild.length > 0) {
                for (var i = 0; i < numCategory; i++) {
                    sum = 0;
                    arrChild = [];
                    for (var j = 0; j < keysChild.length; j++) {
                        sum += dataGroupId[keysChild[j]][i].Value;
                        arrChild.push(sum);
                    }
                    arrParent.push(arrChild);
                }
            }
            return arrParent;
        }
    };
    return AnalystDataStacked;
}());
var AnalystDataColumnChart = (function () {
    function AnalystDataColumnChart(analyst, keyTypeChart, maxValAxisPrimary, maxValAxisSecondary, stepPrimary, stepSecondary, truongHop) {
        if (truongHop === void 0) { truongHop = 0; }
        this.analyst = analyst;
        this.getData = analyst.getData;
        this.chart = analyst.chart;
        this.maxValAxisPrimary = maxValAxisPrimary;
        this.maxValAxisSecondary = maxValAxisSecondary;
        this.stepPrimary = stepPrimary;
        this.stepSecondary = stepSecondary;
        if (this.chart.options.ConfigSeriOption.PrimaryAxis) {
            this.gapWidthPrimary = this.chart.options.ConfigSeriOption.PrimaryAxis.GapWidth;
            this.overlapPrimary = this.chart.options.ConfigSeriOption.PrimaryAxis.OverLap;
        }
        if (this.chart.options.ConfigSeriOption.SecondaryAxis) {
            this.gapWidthSecondary = this.chart.options.ConfigSeriOption.SecondaryAxis.GapWidth;
            this.overlapSecondary = this.chart.options.ConfigSeriOption.SecondaryAxis.OverLap;
        }
        this.truongHop = truongHop;
    }
    AnalystDataColumnChart.prototype.draw = function () {
        var dataAnalyst = this.analyst.analyst();
        var dataPrimary = dataAnalyst[2];
        var dataSecondary = dataAnalyst[3];
        var idsSeriPrimary = Object.keys(dataPrimary.groupBy("ID"));
        var idsSeriSecondary = Object.keys(dataSecondary.groupBy("ID"));
        var colorsPrimary = [];
        var colorsSecondary = [];
        var strokesPrimary = [];
        var strokesSecondary = [];
        var seriesOption = this.chart.seriesOption;
        for (var i = 0; i < dataPrimary.length; i++) {
            colorsPrimary.push(dataPrimary[i].Fill);
            strokesPrimary.push(dataPrimary[i].Stroke);
        }
        for (i = 0; i < dataSecondary.length; i++) {
            colorsSecondary.push(dataSecondary[i].Fill);
            strokesSecondary.push(dataSecondary[i].Stroke);
        }
        var gRectsColumn;
        if (dataPrimary.length > 0) {
            gRectsColumn = this.drawColAcordingAxis(idsSeriPrimary.length, this.gapWidthPrimary, this.overlapPrimary, colorsPrimary, strokesPrimary, 0, this.maxValAxisPrimary, this.stepPrimary);
            $(this.chart.gChart).find('.drawChart')[0].appendChild(gRectsColumn);
        }
        if (dataSecondary.length > 0) {
            gRectsColumn = this.drawColAcordingAxis(idsSeriSecondary.length, this.gapWidthSecondary, this.overlapSecondary, colorsSecondary, strokesSecondary, 1, this.maxValAxisSecondary, this.stepSecondary);
            $(this.chart.gChart).find('.drawChart')[0].appendChild(gRectsColumn);
        }
    };
    AnalystDataColumnChart.prototype.rectsDataClustered = function (numObj, gapWidth, overlap, colors, strokes, indexPrimaryOrSecondary, maxValAxis, step) {
        var marginGapSecondaryAxis = this.chart.marginGap(numObj, gapWidth);
        var widthGapSecondaryAxis = this.chart.widthGap(numObj, gapWidth);
        var avgWidth = this.chart.avgWidth();
        var numCategory = this.chart.numCategory();
        var getArrData = this.analyst.analyst()[indexPrimaryOrSecondary];
        var getArrIDSeri = this.analyst.analyst()[indexPrimaryOrSecondary + 2].map(function (x) { return x.IDSeri; });
        var stepHeight = this.chart.stepHeight(step, maxValAxis);
        if (overlap !== 0) {
            var changeOverlap = this.changeOverlap(numObj, overlap, gapWidth);
            var widthChangeOverlap = this.widthChangeOverlap(numObj, changeOverlap);
            widthGapSecondaryAxis = widthGapSecondaryAxis + changeOverlap;
        }
        var x = 0;
        var fill = true;
        var rects = [];
        var bd = 0;
        var idx = 0;
        for (var j = 0; j < numCategory; j++) {
            bd = avgWidth * j + this.chart.xChart + marginGapSecondaryAxis;
            for (var i = 0; i < numObj; i++) {
                var rec = [];
                if (i !== 0) {
                    bd = bd + widthGapSecondaryAxis;
                    if (overlap !== 0) {
                        bd -= widthChangeOverlap;
                    }
                }
                var val = getArrData[j][i];
                var h = this.chart.h_val(val, step, stepHeight);
                var y_val = this.chart.cacl_y(val, step, stepHeight);
                var w = widthGapSecondaryAxis;
                var color = colors[i];
                var p1 = new Point(bd, y_val);
                rec = new Rect(getArrIDSeri[j * numObj + i], p1, w, h, color, fill, strokes[i]);
                rects.push(rec);
            }
        }
        return rects;
    };
    AnalystDataColumnChart.prototype.rectsDataStacked = function (numObj, gapWidth, overlap, colors, strokes, indexPrimaryOrSecondary, maxValAxis, step) {
        var marginGapSecondaryAxis = this.chart.marginGap(numObj, gapWidth);
        var widthGapSecondaryAxis = this.chart.widthGap(numObj, gapWidth);
        var avgWidth = this.chart.avgWidth();
        var numCategory = this.chart.numCategory();
        var getArrData = this.analyst.analyst()[indexPrimaryOrSecondary];
        var getArrIDSeri = this.analyst.analyst()[indexPrimaryOrSecondary + 2].map(function (x) { return x.IDSeri; });
        var stepHeight = this.chart.stepHeight(step, maxValAxis);
        if (overlap !== 0) {
            if (numObj == 1) {
                var analyst = this.analyst.analyst();
                var numPrimary = 0;
                var numSecondary = 0;
                if (analyst[0].length > 0) {
                    numPrimary = analyst[0][0].length;
                }
                if (analyst[1].length > 0) {
                    numSecondary = analyst[1][0].length;
                }
                var numSeriLager = numPrimary > numSecondary ? numPrimary : numSecondary;
                widthGapSecondaryAxis = this.chart.widthGap(numSeriLager, gapWidth);
                marginGapSecondaryAxis = this.chart.marginGap(numSeriLager, gapWidth);
            }
            var changeOverlap = this.changeOverlap(numObj, overlap, gapWidth);
            var widthChangeOverlap = this.widthChangeOverlap(numObj, changeOverlap);
            widthGapSecondaryAxis = widthGapSecondaryAxis + changeOverlap;
        }
        var x = 0;
        var fill = true;
        var rects = [];
        var bd = 0;
        var idx = 0;
        for (var j = 0; j < numCategory; j++) {
            bd = avgWidth * j + this.chart.xChart + marginGapSecondaryAxis;
            for (var i = 0; i < numObj; i++) {
                var rec = [];
                if (i !== 0) {
                    bd = bd + widthGapSecondaryAxis;
                    if (overlap !== 0) {
                        bd -= widthChangeOverlap;
                    }
                }
                var val = getArrData[j][i];
                var valSeriBefore;
                if (getArrData[j][i - 1]) {
                    valSeriBefore = getArrData[j][i - 1];
                }
                else {
                    valSeriBefore = 0;
                }
                var h = this.chart.h_val(val - valSeriBefore, step, stepHeight);
                var y_val = this.chart.cacl_y(val, step, stepHeight);
                var w = widthGapSecondaryAxis;
                var color = colors[i];
                var p1 = new Point(bd, y_val);
                rec = new Rect(getArrIDSeri[j * numObj + i], p1, w, h, color, fill);
                rects.push(rec);
            }
        }
        return rects;
    };
    AnalystDataColumnChart.prototype.rectsDataStacked100 = function (numObj, gapWidth, overlap, colors, strokes, indexPrimaryOrSecondary, maxValAxis, step) {
        var marginGapSecondaryAxis = this.chart.marginGap(numObj, gapWidth);
        var widthGapSecondaryAxis = this.chart.widthGap(numObj, gapWidth);
        var avgWidth = this.chart.avgWidth();
        var numCategory = this.chart.numCategory();
        var getArrData = this.analyst.analyst()[indexPrimaryOrSecondary];
        var getArrIDSeri = this.analyst.analyst()[indexPrimaryOrSecondary + 2].map(function (x) { return x.IDSeri; });
        var stepHeight = this.chart.stepHeight(step, maxValAxis);
        if (overlap !== 0) {
            if (numObj == 1) {
                var analyst = this.analyst.analyst();
                var numPrimary = 0;
                var numSecondary = 0;
                if (analyst[0].length > 0) {
                    numPrimary = analyst[0][0].length;
                }
                if (analyst[1].length > 0) {
                    numSecondary = analyst[1][0].length;
                }
                var numSeriLager = numPrimary > numSecondary ? numPrimary : numSecondary;
                widthGapSecondaryAxis = this.chart.widthGap(numSeriLager, gapWidth);
                marginGapSecondaryAxis = this.chart.marginGap(numSeriLager, gapWidth);
            }
            var changeOverlap = this.changeOverlap(numObj, overlap, gapWidth);
            var widthChangeOverlap = this.widthChangeOverlap(numObj, changeOverlap);
            widthGapSecondaryAxis = widthGapSecondaryAxis + changeOverlap;
        }
        var x = 0;
        var fill = true;
        var rects = [];
        var bd = 0;
        var idx = 0;
        for (var j = 0; j < numCategory; j++) {
            bd = avgWidth * j + this.chart.xChart + marginGapSecondaryAxis;
            for (var i = 0; i < numObj; i++) {
                var rec = [];
                if (i !== 0) {
                    bd = bd + widthGapSecondaryAxis;
                    if (overlap !== 0) {
                        bd -= widthChangeOverlap;
                    }
                }
                var val = getArrData[j][i] / getArrData[j][numObj - 1];
                var valSeriBefore;
                if (getArrData[j][i - 1]) {
                    valSeriBefore = getArrData[j][i - 1] / getArrData[j][numObj - 1];
                }
                else {
                    valSeriBefore = 0;
                }
                var h = this.chart.h_val(val - valSeriBefore, step, stepHeight);
                var y_val = this.chart.cacl_y(val, step, stepHeight);
                var w = widthGapSecondaryAxis;
                var color = colors[i];
                var p1 = new Point(bd, y_val);
                rec = new Rect(getArrIDSeri[j * numObj + i], p1, w, h, color, fill, strokes[i]);
                rects.push(rec);
            }
        }
        return rects;
    };
    AnalystDataColumnChart.prototype.drawColAcordingAxis = function (numObj, gapWidthSeri, overlap, colors, strokes, indexPrimaryOrSecondary, maxVal, step) {
        var rects;
        var g = GlobalHelper.createTags('g', '', 'columns');
        if (this.analyst.indexTemplate === "Clustered") {
            rects = this.rectsDataClustered(numObj, gapWidthSeri, overlap, colors, strokes, indexPrimaryOrSecondary, maxVal, step);
        }
        else if (this.analyst.indexTemplate === "Stacked") {
            rects = this.rectsDataStacked(numObj, gapWidthSeri, overlap, colors, strokes, indexPrimaryOrSecondary, maxVal, step);
        }
        else if (this.analyst.indexTemplate === "Stacked100") {
            rects = this.rectsDataStacked100(numObj, gapWidthSeri, overlap, colors, strokes, indexPrimaryOrSecondary, maxVal, step);
        }
        for (var m = 0; m < rects.length; m++) {
            var rect = rects[m];
            var fillColor = FillColorHelper.genGTag(colors[m], ConfigFrame(rect.getElement()));
            if (colors[m].CoT !== "Solid")
                g.appendChild(fillColor.Tag);
            rect.color = { "col": fillColor.ID };
            g.appendChild(rect.getElement());
        }
        return g;
    };
    AnalystDataColumnChart.prototype.changeOverlap = function (numObj, overlap, gapWidth) {
        var widthCategory = this.chart.avgWidth();
        var widthSeri = this.calculatorWidthSeri(widthCategory, numObj, gapWidth);
        var changeOverlap = 0;
        if (this.analyst.indexTemplate === "Stacked" || this.analyst.indexTemplate === "Stacked100") {
            if (numObj == 1) {
                var analyst = this.analyst.analyst();
                var numPrimary = 0;
                var numSecondary = 0;
                if (analyst[0].length > 0) {
                    numPrimary = analyst[0][0].length;
                }
                if (analyst[1].length > 0) {
                    numSecondary = analyst[1][0].length;
                }
                var countTemp = numPrimary + numSecondary - numObj;
                if (countTemp > 0) {
                    widthSeri = this.calculatorWidthSeri(widthCategory, countTemp, gapWidth);
                    changeOverlap = widthSeri * (countTemp - 1);
                }
            }
        }
        var widthOverLapAm100 = widthSeri * numObj / (2 * numObj - 1);
        var widthOverLapChange = widthSeri - widthOverLapAm100;
        if (numObj > 1) {
            if (overlap > 0 && overlap <= 100) {
                var objCategoryWidth = widthSeri * (numObj - 1);
                changeOverlap = objCategoryWidth * overlap / 100;
            }
            else if (overlap >= -100 && overlap <= 0) {
                changeOverlap = widthOverLapChange * overlap / 100;
            }
        }
        return changeOverlap;
    };
    AnalystDataColumnChart.prototype.widthChangeOverlap = function (numObj, changeOverlap) {
        var widthChangeOverlap = (numObj > 1 ? 1 + 1.0 / (numObj - 1) : 0) * changeOverlap;
        return widthChangeOverlap;
    };
    AnalystDataColumnChart.prototype.calculatorWidthSeri = function (widthCategory, countObjSeri, gapWidth) {
        return widthCategory / (countObjSeri + gapWidth / 100);
    };
    return AnalystDataColumnChart;
}());
var AnalystDataLineChart = (function () {
    function AnalystDataLineChart(analyst, keyTypeChart, maxValAxisPrimary, maxValAxisSecondary, stepPrimary, stepSecondary, truongHop) {
        this.analyst = analyst;
        this.ctx = analyst.chart.ctx;
        this.getData = analyst.getData;
        this.chart = analyst.chart;
        this.maxValAxisPrimary = maxValAxisPrimary;
        this.maxValAxisSecondary = maxValAxisSecondary;
        this.stepPrimary = stepPrimary;
        this.stepSecondary = stepSecondary;
        this.truongHop = truongHop;
    }
    AnalystDataLineChart.prototype.draw = function () {
        var dataAnalyst = this.analyst.analyst();
        var dataPrimary = dataAnalyst[2];
        var dataSecondary = dataAnalyst[3];
        var idsSeriPrimary = Object.keys(dataPrimary.groupBy("ID"));
        var idsSeriSecondary = Object.keys(dataSecondary.groupBy("ID"));
        var colorsPrimary = [];
        var colorsSecondary = [];
        var strokesPrimary = [];
        var strokesSecondary = [];
        var seriesOption = this.chart.seriesOption;
        for (var i = 0; i < idsSeriPrimary.length; i++) {
            colorsPrimary.push(findValInArrayAttributeId(seriesOption, idsSeriPrimary[i]).Stroke.col);
        }
        for (i = 0; i < idsSeriSecondary.length; i++) {
            colorsSecondary.push(findValInArrayAttributeId(seriesOption, idsSeriSecondary[i]).Stroke.col);
        }
        var arrThicknessPrimary = dataPrimary.map(function (x) {
            return x.StrokeThickness;
        });
        var arrThicknessSecondary = dataSecondary.map(function (x) {
            return x.StrokeThickness;
        });
        var gLines;
        if (dataPrimary.length > 0) {
            gLines = this.drawLineAcordingAxis(idsSeriPrimary.length, colorsPrimary, 0, this.maxValAxisPrimary, this.stepPrimary, arrThicknessPrimary);
            $(this.chart.gChart).find('.drawChart')[0].appendChild(gLines);
        }
        if (dataSecondary.length > 0) {
            gLines = this.drawLineAcordingAxis(idsSeriSecondary.length, colorsSecondary, 1, this.maxValAxisSecondary, this.stepSecondary, arrThicknessSecondary);
            $(this.chart.gChart).find('.drawChart')[0].appendChild(gLines);
        }
    };
    AnalystDataLineChart.prototype.drawLineAcordingAxis = function (numObj, colors, indexPrimaryOrSecondary, maxValAxis, step, arrThickness) {
        if (this.analyst.indexTemplate === "Clustered") {
            var points = this.pointsLineChartClustered(numObj, colors, indexPrimaryOrSecondary, maxValAxis, step);
        }
        else if (this.analyst.indexTemplate === "Stacked") {
            var points = this.pointsLineChartStacked(numObj, colors, indexPrimaryOrSecondary, maxValAxis, step);
        }
        else if (this.analyst.indexTemplate === "Stacked100") {
            var points = this.pointsLineChartStacked100(numObj, colors, indexPrimaryOrSecondary, maxValAxis, step);
        }
        var lines = this.linesData(numObj, colors, indexPrimaryOrSecondary, maxValAxis, step, points, arrThickness);
        return drawLine(lines);
    };
    AnalystDataLineChart.prototype.pointsLineChartStacked100 = function (numObj, colors, indexPrimaryOrSecondary, maxVal, step) {
        var points = [];
        var numCategory = this.chart.numCategory();
        var numSeri = numObj;
        var minX = this.chart.xChart;
        var avgWidth = this.chart.avgWidth();
        var getData = this.analyst.analyst()[indexPrimaryOrSecondary];
        var margin = 0;
        var step;
        var maxVal;
        var stepHeight;
        for (var i = 0; i < numCategory; i++) {
            margin = avgWidth / 2 + i * avgWidth;
            for (var j = 0; j < numObj; j++) {
                var pointsOfSeri = [];
                var x = void 0;
                x = minX + margin;
                var val = getData[i][j] / getData[i][numObj - 1];
                var y_val;
                if (!val) {
                    val = 0;
                }
                stepHeight = this.chart.stepHeight(step, maxVal);
                y_val = this.chart.cacl_y(val, step, stepHeight);
                pointsOfSeri.push(x, y_val);
                points.push(pointsOfSeri);
            }
        }
        return points;
    };
    AnalystDataLineChart.prototype.pointsLineChartClustered = function (numObj, colors, indexPrimaryOrSecondary, maxVal, step) {
        var points = [];
        var numCategory = this.chart.numCategory();
        var numSeri = numObj;
        var minX = this.chart.xChart;
        var avgWidth = this.chart.avgWidth();
        var getData = this.analyst.analyst()[indexPrimaryOrSecondary];
        var margin = 0;
        var step;
        var maxVal;
        var stepHeight;
        for (var i = 0; i < numCategory; i++) {
            margin = avgWidth / 2 + i * avgWidth;
            for (var j = 0; j < numObj; j++) {
                var pointsOfSeri = [];
                var x = void 0;
                x = minX + margin;
                var val = getData[i][j];
                var y_val = null;
                if (val) {
                    stepHeight = this.chart.stepHeight(step, maxVal);
                    y_val = this.chart.cacl_y(val, step, stepHeight);
                }
                pointsOfSeri.push(x, y_val);
                points.push(pointsOfSeri);
            }
        }
        return points;
    };
    AnalystDataLineChart.prototype.pointsLineChartStacked = function (numObj, colors, indexPrimaryOrSecondary, maxVal, step) {
        var points = [];
        var numCategory = this.chart.numCategory();
        var numSeri = numObj;
        var minX = this.chart.xChart;
        var avgWidth = this.chart.avgWidth();
        var getData = this.analyst.analyst()[indexPrimaryOrSecondary];
        var margin = 0;
        var step;
        var maxVal;
        var stepHeight;
        for (var i = 0; i < numCategory; i++) {
            margin = avgWidth / 2 + i * avgWidth;
            for (var j = 0; j < numObj; j++) {
                var pointsOfSeri = [];
                var x = void 0;
                x = minX + margin;
                var val = getData[i][j];
                var y_val = null;
                if (!val) {
                    val = 0;
                }
                stepHeight = this.chart.stepHeight(step, maxVal);
                y_val = this.chart.cacl_y(val, step, stepHeight);
                pointsOfSeri.push(x, y_val);
                points.push(pointsOfSeri);
            }
        }
        return points;
    };
    AnalystDataLineChart.prototype.linesData = function (numObj, colors, indexPrimaryOrSecondary, maxValAxis, step, points, arrThickness) {
        var dataTotalCat = [];
        var dataCat = [];
        var numCategory = this.chart.numCategory();
        for (var j = 0; j < numObj; j++) {
            dataCat = [];
            for (var i = j; i <= j + (numCategory - 1) * numObj; i += numObj) {
                dataCat.push(points[i]);
            }
            dataTotalCat.push(dataCat);
        }
        var lines = [];
        for (var _j12 = 0; _j12 < dataTotalCat.length; _j12++) {
            for (var _i9 = 0; _i9 < dataTotalCat[_j12].length - 1; _i9++) {
                var point1 = new Point(dataTotalCat[_j12][_i9][0], dataTotalCat[_j12][_i9][1]);
                var point2 = new Point(dataTotalCat[_j12][_i9 + 1][0], dataTotalCat[_j12][_i9 + 1][1]);
                if (point1.y && point2.y) {
                    var color = colors[_j12 % colors.length];
                    GenerateLine(lines, point1, point2, color, arrThickness[_i9]);
                }
                else
                    continue;
            }
        }
        return lines;
    };
    return AnalystDataLineChart;
}());
var AnalystDataAreaChart = (function () {
    function AnalystDataAreaChart(analyst, keyTypeChart, maxValAxisPrimary, maxValAxisSecondary, stepPrimary, stepSecondary, truongHop, fillArea) {
        if (truongHop === void 0) { truongHop = 0; }
        if (fillArea === void 0) { fillArea = true; }
        this.analyst = analyst;
        this.ctx = this.analyst.chart.ctx;
        this.getData = analyst.getData;
        this.chart = analyst.chart;
        this.options = analyst.chart.options;
        this.maxValAxisPrimary = maxValAxisPrimary;
        this.maxValAxisSecondary = maxValAxisSecondary;
        this.stepPrimary = stepPrimary;
        this.stepSecondary = stepSecondary;
        this.frameChart = this.chart.options.FrameChart;
        this.fillArea = fillArea;
        this.truongHop = truongHop;
    }
    AnalystDataAreaChart.prototype.draw = function () {
        var dataAnalyst = this.analyst.analyst();
        var dataPrimary = dataAnalyst[2];
        var dataSecondary = dataAnalyst[3];
        var idsSeriPrimary = Object.keys(dataPrimary.groupBy("ID"));
        var idsSeriSecondary = Object.keys(dataSecondary.groupBy("ID"));
        var idsSeri = [];
        idsSeri.push(idsSeriPrimary, idsSeriSecondary);
        var colorsPrimary = [];
        var colorsSecondary = [];
        var seriesOption = this.chart.seriesOption;
        var indexTemplate = this.analyst.indexTemplate;
        var steps = [];
        steps.push(this.stepPrimary, this.stepSecondary);
        var stepHeights = [];
        var stepHeightPrimary, stepHeightSecondary;
        stepHeightPrimary = this.chart.stepHeight(this.stepPrimary, this.maxValAxisPrimary);
        stepHeightSecondary = this.chart.stepHeight(this.stepSecondary, this.maxValAxisSecondary);
        stepHeights.push(stepHeightPrimary, stepHeightSecondary);
        if (dataPrimary.length > 0 || dataSecondary.length > 0) {
            switch (indexTemplate) {
                case "Stacked":
                    $(this.chart.gChart).find('.drawChart')[0].appendChild(this.drawAreaStacked(idsSeri, steps, stepHeights));
                    break;
                case "Clustered":
                    $(this.chart.gChart).find('.drawChart')[0].appendChild(this.drawAreaClustered(idsSeri, steps, stepHeights));
                    break;
                case "Stacked100":
                    $(this.chart.gChart).find('.drawChart')[0].appendChild(this.drawAreaStacked100(idsSeri, steps, stepHeights));
            }
        }
    };
    AnalystDataAreaChart.prototype.drawPointClustered = function (points, color) {
        var dPath;
        dPath = "M " + points[0][0] + ' ' + points[0][1];
        for (var i = 1; i < points.length; i++) {
            dPath += ' L ' + points[i][0] + ' ' + points[i][1];
        }
        dPath += ' L ' + points[points.length - 1][0] + ' ' + (this.frameChart.h + this.frameChart.T);
        dPath += ' L ' + points[0][0] + ' ' + (this.frameChart.h + this.frameChart.T);
        dPath += ' L ' + points[0][0] + ' ' + points[0][1];
        var path = GlobalHelper.createTags('path', '', 'areaClustered');
        path.setAttribute('d', dPath);
        path.setAttribute('fill', color);
        return path;
    };
    AnalystDataAreaChart.prototype.drawAreaClustered = function (idsSeri, steps, stepHeights) {
        var seriAcording = idsSeri[0].concat(idsSeri[1]);
        var xAcordingCategory = this.xAcordingCategory();
        var numCategory = this.chart.numCategory();
        var seriesOption = this.chart.seriesOption;
        var g = GlobalHelper.createTags('g', '', 'areaClustered');
        for (var j = 0; j < 2; j++) {
            for (var m = 0; m < idsSeri[j].length; m++) {
                var point = [];
                var areaPoints = [];
                var index = parseInt(idsSeri[j][m]) - 1;
                var val = this.getDataAcordingIndexSeri(index);
                for (var i = 0; i < numCategory; i++) {
                    point = [];
                    point.push(xAcordingCategory[i]);
                    point.push(this.chart.cacl_y(val[i], steps[j], stepHeights[j]));
                    areaPoints.push(point);
                }
                g.appendChild(this.drawPointClustered(areaPoints, seriesOption[index].Fill.col));
            }
        }
        return g;
    };
    AnalystDataAreaChart.prototype.drawAreaStacked = function (idsSeri, steps, stepHeights) {
        var xAcordingCategory = this.xAcordingCategory();
        var yCalcAcordingTemplate = this.yCalcAcordingStacked(idsSeri, this.ctx);
        var _this = this;
        var arr = [];
        var points = [], points2 = [], arr1 = [], currentSeri = void 0, color = void 0;
        var g = GlobalHelper.createTags('g', '', 'areaStacked');
        for (var j = 0; j < 2; j++) {
            for (var i = 0; i < yCalcAcordingTemplate[j].length; i++) {
                currentSeri = parseInt(idsSeri[j][idsSeri[j].length - i - 1]) - 1;
                color = this.chart.seriesOption[currentSeri].Fill.col;
                arr = yCalcAcordingTemplate[j][i];
                arr = arr.map(function (x) {
                    return _this.chart.cacl_y(x, steps[j], stepHeights[j]);
                });
                points = xAcordingCategory.map(function (e, i) {
                    return [e, arr[i]];
                });
                if (i === yCalcAcordingTemplate[j].length - 1) {
                    g.appendChild(this.drawChildAreaStacked(color, points));
                }
                else {
                    arr1 = yCalcAcordingTemplate[j][i + 1];
                    arr1 = arr1.map(function (x) {
                        return _this.chart.cacl_y(x, steps[j], stepHeights[j]);
                    });
                    points2 = xAcordingCategory.map(function (e, i) {
                        return [e, arr1[i]];
                    });
                    g.appendChild(this.drawChildAreaStacked(color, points, points2));
                }
            }
        }
        return g;
    };
    AnalystDataAreaChart.prototype.drawAreaStacked100 = function (idsSeri) {
        var steps = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
        var stepHeights = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];
        var xAcordingCategory = this.xAcordingCategory();
        var yCalcAcordingTemplate = this.yCalcAcordingStacked(idsSeri, this.ctx);
        var _this = this;
        var arr = [];
        var points = [], points2 = [], arr1 = [], currentSeri = void 0, color = void 0;
        var g = GlobalHelper.createTags('g', '', 'areaStacked');
        for (var j = 0; j < 2; j++) {
            for (var i = 0; i < yCalcAcordingTemplate[j].length; i++) {
                currentSeri = parseInt(idsSeri[j][idsSeri[j].length - i - 1]) - 1;
                color = this.chart.seriesOption[currentSeri].Fill.col;
                arr = yCalcAcordingTemplate[j][i];
                arr = arr.map(function (x, index) {
                    return _this.chart.cacl_y(x / yCalcAcordingTemplate[j][0][index], steps[j], stepHeights[j]);
                });
                points = xAcordingCategory.map(function (e, i) {
                    return [e, arr[i]];
                });
                if (i === yCalcAcordingTemplate[j].length - 1) {
                    g.appendChild(this.drawChildAreaStacked(color, points));
                }
                else {
                    arr1 = yCalcAcordingTemplate[j][i + 1];
                    arr1 = arr1.map(function (x, index) {
                        return _this.chart.cacl_y(x / yCalcAcordingTemplate[j][0][index], steps[j], stepHeights[j]);
                    });
                    points2 = xAcordingCategory.map(function (e, i) {
                        return [e, arr1[i]];
                    });
                    g.appendChild(this.drawChildAreaStacked(color, points, points2));
                }
            }
        }
        return g;
    };
    AnalystDataAreaChart.prototype.drawChildAreaStacked = function (color, points, points2) {
        var dPath;
        dPath = "M " + points[0][0] + ' ' + points[0][1];
        for (var i = 1; i < points.length; i++) {
            dPath += ' L ' + points[i][0] + ' ' + points[i][1];
        }
        if (points2) {
            dPath += ' L ' + points[points.length - 1][0] + ' ' + points2[points2.length - 1][1];
            for (var j = points2.length - 1; j >= 0; j--) {
                dPath += ' L ' + points2[j][0] + ' ' + points2[j][1];
            }
            dPath += ' L ' + points2[0][0] + ' ' + points[0][1];
        }
        else {
            dPath += ' L ' + points[points.length - 1][0] + ' ' + (this.frameChart.h + this.frameChart.T);
            dPath += ' L ' + points[0][0] + ' ' + (this.frameChart.h + this.frameChart.T);
            dPath += ' L ' + points[0][0] + ' ' + points[0][1];
        }
        var path = GlobalHelper.createTags('path', '', 'areaStacked');
        path.setAttribute('d', dPath);
        path.setAttribute('fill', color);
        return path;
    };
    AnalystDataAreaChart.prototype.xAcordingCategory = function () {
        var xAcordingCategory = [];
        var numCategory = this.chart.numCategory();
        var frameChart = this.frameChart;
        var minX = frameChart.l;
        var avgWidth;
        var margin = 0;
        var x = void 0;
        var fillArea = true;
        if (!this.fillArea) {
            fillArea = false;
        }
        else {
            if (this.options.DataTable.IsDataTableKey) {
                fillArea = false;
            }
        }
        for (var i = 0; i < numCategory; i++) {
            if (fillArea) {
                avgWidth = this.frameChart.w / (numCategory - 1);
                margin = i * avgWidth;
                x = minX + margin;
            }
            else {
                avgWidth = frameChart.w / numCategory;
                margin = avgWidth / 2 + i * avgWidth;
                x = minX + margin;
            }
            xAcordingCategory.push(x);
        }
        return xAcordingCategory;
    };
    AnalystDataAreaChart.prototype.yCalcAcordingStacked = function (idsSeri, ctx) {
        var index = void 0, arr = void 0, arrayValue = void 0;
        var sum = void 0;
        var dataSeri = [];
        var valToDraw = [];
        for (var j = 0; j < 2; j++) {
            index = 0, arr = [], arrayValue = [];
            dataSeri = [];
            sum = [];
            for (var i = 0; i < idsSeri[j].length; i++) {
                index = idsSeri[j][i] - 1;
                arr = this.getDataAcordingIndexSeri(index);
                if (i === 0) {
                    sum = arr;
                }
                else {
                    arrayValue = new ArrayValue(arr);
                    sum = new ArrayValue(sum);
                    sum = sum.sumSingleIteration(arrayValue);
                }
                dataSeri.push(sum);
            }
            valToDraw.push(dataSeri.reverse());
        }
        return valToDraw;
    };
    AnalystDataAreaChart.prototype.getDataAcordingIndexSeri = function () {
        var index = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
        var dataSeri = [];
        for (var i = 0; i < this.options["Data"].length; i++) {
            dataSeri.push(this.options["Data"][i]["Series"][index].Value);
        }
        return dataSeri;
    };
    return AnalystDataAreaChart;
}());
function typeChartAcordingKey(keyTypeChart) {
    return keyTypeChart.split("_")[0];
}
function typeTemplate(keyTypeChart) {
    return keyTypeChart.split("_")[1];
}
