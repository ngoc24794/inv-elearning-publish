'use strict';
var ArrayValue = (function () {
    function ArrayValue(arr) {
        this.arr = arr;
    }
    ArrayValue.prototype.sumSingleIteration = function (arrayValue1) {
        if (arrayValue1) {
            return this.arr.map(function (currentValue, index) {
                return currentValue + arrayValue1.arr[index];
            });
        }
        else {
            return this.arr;
        }
    };
    return ArrayValue;
}());
var Chart = (function () {
    function Chart(options, frame) {
        options = ChartModule.convertOption(options, frame);
        this.options = options;
        this.wCanvas = options.Width;
        this.hCanvas = options.Height;
        this.topCanvas = options.Top;
        this.leftCanvas = options.Left;
        this.wChart = options.FrameChart.w;
        this.hChart = options.FrameChart.h;
        this.xChart = options.FrameChart.l;
        this.yChart = options.FrameChart.T;
        this.locationChart = new Point(this.xChart, this.yChart);
        this.seriesOption = options.Data[0].Series;
        this.dataTable = options.DataTable;
        this.data = options.Data || [];
        this.type = options.Type || "";
        this.linesOfGridLine = [];
        this.gChart = GlobalHelper.createTags('g', '', 'chart');
        var bieuDo;
        switch (this.type) {
            case "ComboChart":
                var comborChart = new ComboChart(this);
                comborChart.gChart();
                bieuDo = comborChart;
                break;
            case "PieChart":
                var pieChart = new PieChart(this);
                pieChart.gChart();
                bieuDo = pieChart;
                break;
        }
        this.bieuDo = bieuDo;
        return bieuDo.gChart();
    }
    Chart.prototype.getCategory = function () {
        var categoryName = this.options.Data.map(function (x) {
            return x.CategoryName;
        });
        return categoryName;
    };
    Chart.prototype.getArrData = function () {
        var result = this.options.Data.map(function (x) {
            return x.Series;
        });
        return result;
    };
    Chart.prototype.getVal = function (indexCategory, indexSeri) {
        var result = this.getArrData();
        return result[indexCategory][indexSeri].value;
    };
    Chart.prototype.arrOptionAxis = function () {
        var seriOption = [];
        var indexOfPrimaryAxis = [];
        var indexOfSecondAxis = [];
        for (var i = 0; i < this.seriesOption.length; i++) {
            if (!this.seriesOption[i].IsPrimaryAxis) {
                indexOfSecondAxis.push(i);
            }
            else {
                indexOfPrimaryAxis.push(i);
            }
        }
        seriOption.push(indexOfPrimaryAxis);
        seriOption.push(indexOfSecondAxis);
        return seriOption;
    };
    Chart.prototype.overlap = function () {
        var arrOverlap = [];
        var overlap_PrimaryAxis = this.options.config.primaryAxis.overlap;
        var overlap_SecondaryAxis = this.options.config.secondAxis.overlap;
        arrOverlap.push(overlap_PrimaryAxis);
        arrOverlap.push(overlap_SecondaryAxis);
        return arrOverlap;
    };
    Chart.prototype.gapWidth = function () {
        var arrGapWidth = [];
        var gapWidth_PrimaryAxis = this.options.config.primaryAxis.gapWidth;
        var gapWidth_SecondaryAxis = this.options.config.secondAxis.gapWidth;
        arrGapWidth.push(gapWidth_PrimaryAxis);
        arrGapWidth.push(gapWidth_SecondaryAxis);
        return arrGapWidth;
    };
    Chart.prototype.numCategory = function () {
        return this.data.length;
    };
    Chart.prototype.numSeri = function () {
        return this.seriesOption.length;
    };
    Chart.prototype.widthGap = function (numObj, gapWidth) {
        var w_col = this.wChart / (this.numCategory() * (numObj + gapWidth / 100));
        return w_col;
    };
    Chart.prototype.marginGap = function (numObj, gapWidth) {
        return gapWidth / 100 * this.widthGap(numObj, gapWidth) / 2;
    };
    Chart.prototype.cacl_y = function (val, step, stepHeight) {
        var y_val = this.hChart + this.locationChart.y - this.h_val(val, step, stepHeight);
        return y_val;
    };
    Chart.prototype.drawGrid = function (gridLine, maxVal, step, xMin, yMin) {
        var dataGrids = [];
        var stepHeight = this.stepHeight(step, maxVal);
        var xMax = xMin + this.wChart;
        var yMax = yMin + this.hChart;
        var y = yMax;
        var m = 0;
        var color = "#f2f2f2";
        var primaryMajorHorizontal = gridLine.primaryMajorHorizontal;
        var primaryMinorHorizontal = gridLine.primaryMinorHorizontal;
        var primaryMinorVertical = gridLine.primaryMinorVertical;
        var primaryMajorVertical = gridLine.primaryMajorVertical;
        var dem = 0;
        var point1, point2, h, soVongLap;
        if (primaryMajorHorizontal && !primaryMinorHorizontal) {
            color = gridLine.colorMajorHorizontal;
            for (h = maxVal; h >= 0; h -= step) {
                point1 = new Point(xMin, y);
                point2 = new Point(xMax, y);
                GenerateLine(dataGrids, point1, point2, color, 1);
                y -= stepHeight;
            }
        }
        else if (!primaryMajorHorizontal && primaryMinorHorizontal) {
            color = gridLine.primaryMinorHorizontal;
            soVongLap = maxVal / step * 5 + 1;
            for (h = 0; h < soVongLap; h++) {
                point1 = new Point(xMin, y);
                point2 = new Point(xMax, y);
                GenerateLine(dataGrids, point1, point2, color, 1);
                y -= stepHeight / 5;
            }
        }
        else if (primaryMajorHorizontal && primaryMinorHorizontal) {
            soVongLap = maxVal / step * 5 + 1;
            for (h = 0; h < soVongLap; h++) {
                if (dem % 5 === 0) {
                    color = gridLine.colorMajorHorizontal;
                }
                else {
                    color = gridLine.colorMinorHorizontal;
                }
                point1 = new Point(xMin, y);
                point2 = new Point(xMax, y);
                GenerateLine(dataGrids, point1, point2, color, 1);
                y -= stepHeight / 5;
                dem++;
            }
        }
        var numCategory = this.numCategory();
        var stepWidth = this.wChart / numCategory;
        if (!primaryMajorVertical && primaryMinorVertical) {
            stepWidth = stepWidth / 2;
        }
        else if (primaryMajorVertical && primaryMinorVertical) {
            stepWidth = stepWidth / 2;
        }
        var x;
        dem = 0;
        if (primaryMajorVertical && !primaryMinorVertical) {
            color = gridLine.colorMajorVertical;
            x = xMin;
            for (var j0 = 0; j0 <= (xMax - xMin) / stepWidth; j0++) {
                point1 = new Point(x, yMin);
                point2 = new Point(x, yMax);
                GenerateLine(dataGrids, point1, point2, color, 1);
                x += stepWidth;
            }
        }
        else if (!primaryMajorVertical && primaryMinorVertical) {
            color = gridLine.colorMinorVertical;
            x = xMin;
            for (var j0 = 0; j0 <= (xMax - xMin) / stepWidth; j0++) {
                point1 = new Point(x, yMin);
                point2 = new Point(x, yMax);
                GenerateLine(dataGrids, point1, point2, color, 1);
                dem++;
                x += stepWidth;
            }
        }
        else if (primaryMajorVertical && primaryMinorVertical) {
            x = xMin;
            for (var j0 = 0; j0 <= (xMax - xMin) / stepWidth; j0++) {
                if (dem % 2 === 0) {
                    color = gridLine.colorMajorVertical;
                }
                else {
                    color = gridLine.colorMinorVertical;
                }
                point1 = new Point(x, yMin);
                point2 = new Point(x, yMax);
                GenerateLine(dataGrids, point1, point2, color, 1);
                dem++;
                x += stepWidth;
            }
        }
        this.linesOfGridLine = dataGrids;
        var g = drawLine(dataGrids);
        return g;
    };
    Chart.prototype.h_val = function (val, step, stepHeight) {
        var h = Math.ceil(val / step) * stepHeight + stepHeight / step * (val - Math.ceil(val / step) * step);
        return h;
    };
    Chart.prototype.step = function (maxData) {
        var step = CalculatorStep(maxData);
        return step;
    };
    Chart.prototype.stepHeight = function (step, maxValAxis) {
        var heightChart = this.hChart;
        if (step != 0) {
            var khoang = maxValAxis / step;
            var stepHeight = heightChart / khoang;
            return stepHeight;
        }
        else {
            return 0;
        }
    };
    Chart.prototype.avgWidth = function (categoryTranslate) {
        if (categoryTranslate) {
            return this.wChart / (this.numCategory() - 1);
        }
        else {
            return this.wChart / this.numCategory();
        }
    };
    Chart.prototype.axis = function () {
        var primaryHorizontal = this.options.axes.axes.primaryHorizontal;
        var primaryVertical = this.options.axes.axes.primaryVertical;
        var secondaryHorizontal = this.options.axes.axes.secondaryHorizontal;
        var secondaryVertical = this.options.axes.axes.secondaryVertical;
    };
    Chart.prototype.getArrLegend = function () {
        var styleLegend = this.seriesOption.map(function (x) {
            return x.dataLable;
        });
        return styleLegend;
    };
    Chart.prototype.getArrSeries = function () {
        return this.seriesOption.map(function (x) {
            return x.Name;
        });
    };
    Chart.prototype.getColorSeries = function () {
        return this.seriesOption.map(function (x) {
            return x.color;
        });
    };
    Chart.prototype.displayDataTable = function () {
        var dataTableOfChartElement = this.dataTable.DataTable;
        var optionsDataTable = this.dataTable.TableBorder;
        var horizontal = optionsDataTable.IsHorizontal;
        var vertical = optionsDataTable.IsVertical;
        var outline = optionsDataTable.IsOutline;
        var heightCategory = this.dataTable.HeightTable.HeightCategory;
        var numSeri = this.numSeri();
        var numCategory = this.numCategory();
        var avgWidth = this.avgWidth();
        var xDataTable = this.xChart - this.dataTable.DataTable.WidthLeftTable;
        var yOx = this.yChart + this.hChart;
        var heightSum = 0;
        for (var q = 0; q < numSeri; q++) {
            heightSum += this.options.DataTable.HeightTable.Series[q].Height;
        }
        var yLineDataTable = yOx + heightCategory + heightSum;
        var heightSumStep;
        if (vertical) {
            for (var i = 0; i < numCategory - 1; i++) {
                var x = this.xChart + avgWidth * (i + 1);
                var point1 = new Point(x, yOx);
                var point2 = new Point(x, yLineDataTable);
                var line = new Line(point1, point2, "#d9d9d9");
                this.gChart.appendChild(line.getElement());
            }
        }
        if (horizontal) {
            heightSumStep = 0;
            for (var _i = 0; _i < numSeri - 1; _i++) {
                heightSumStep += this.options.DataTable.HeightTable.Series[_i].Height;
                var xBeginLine = xDataTable;
                var xEndLine = xBeginLine + (this.xChart - xDataTable) + this.wChart;
                var yLine = yOx + heightCategory + heightSumStep;
                var _point = new Point(xBeginLine, yLine);
                var _point2 = new Point(xEndLine, yLine);
                var _line = new Line(_point, _point2, "#d9d9d9");
                this.gChart.appendChild(_line.getElement());
            }
        }
        if (outline) {
            var _xBeginLine = xDataTable;
            var _xEndLine = _xBeginLine + (this.xChart - xDataTable) + this.wChart;
            var yBeginVertical = yOx + heightCategory;
            var yEndVertical = yBeginVertical + heightSum;
            var _point3 = new Point(_xBeginLine, yBeginVertical);
            var _point4 = new Point(_xEndLine, yBeginVertical);
            var line1 = new Line(_point3, _point4, "#d9d9d9");
            this.gChart.appendChild(line1.getElement());
            _point3 = new Point(_xBeginLine, yBeginVertical);
            _point4 = new Point(_xBeginLine, yEndVertical);
            line1 = new Line(_point3, _point4, "#d9d9d9");
            this.gChart.appendChild(line1.getElement());
            _point3 = new Point(_xEndLine, yEndVertical);
            _point4 = new Point(_xBeginLine, yEndVertical);
            line1 = new Line(_point3, _point4, "#d9d9d9");
            this.gChart.appendChild(line1.getElement());
            _point3 = new Point(this.xChart, yOx);
            _point4 = new Point(this.xChart, yEndVertical);
            line1 = new Line(_point3, _point4, "#d9d9d9");
            this.gChart.appendChild(line1.getElement());
            _point3 = new Point(_xEndLine, yOx);
            _point4 = new Point(_xEndLine, yEndVertical);
            line1 = new Line(_point3, _point4, "#d9d9d9");
            this.gChart.appendChild(line1.getElement());
        }
    };
    Chart.prototype.getDataAcordingIndexSeri = function (index) {
        var dataSeri = [];
        for (var i = 0; i < this.options["data"].length; i++) {
            dataSeri.push(this.options["data"][i]["series"][index].value);
        }
        return dataSeri;
    };
    Chart.prototype.maxRecent = function (maxData, step) {
        if (step === 0) {
            return 0;
        }
        else {
            return Math.ceil(maxData / step) * step;
        }
    };
    Chart.prototype.maxValAxis = function (maxData) {
        var step = CalculatorStep(maxData);
        var maxRecent = this.maxRecent(maxData, step);
        if ((maxData - (maxRecent - step)) / step > 0.7) {
            maxRecent = maxRecent + step;
        }
        if (step !== 0) {
            var a = maxRecent / step;
            maxRecent = parseInt(a) * step;
        }
        return maxRecent;
    };
    Chart.prototype.textPrimaryGrid = function (maxVal, step) {
        var truongHop = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;
        var percentChart = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;
        var verticalPrimary = this.options.DataAxes.ListDataAxes[1];
        var stepHeight = this.stepHeight(step, maxVal);
        var dataTexts = [];
        var xMin = verticalPrimary.Left;
        var y = this.yChart + this.hChart;
        var color, font, size;
        var text;
        if (verticalPrimary.IsChecked) {
            color = verticalPrimary.Foreground.Color;
            font = verticalPrimary.FontFamily;
            size = verticalPrimary.FontSize;
            var gText;
            var soVongLap = maxVal / step;
            if (truongHop === 1) {
                maxVal = 100;
                step = 10;
                percentChart = true;
            }
            else if (truongHop === 2) {
                maxVal = maxVal * 100;
                step = step * 100;
                percentChart = true;
            }
            else if (truongHop === 3) {
                percentChart = false;
            }
            var h = maxVal;
            for (var n = 0; n <= soVongLap; n++) {
                var point = new Point(xMin, y);
                text = convertFloatingPoint(maxVal - h);
                h -= step;
                GenerateText(dataTexts, "id", text, point, font, size, color, "end", "middle");
                y -= stepHeight;
            }
            if (percentChart) {
                gText = drawTextPercent(dataTexts, this.ctx);
            }
            else {
                gText = drawText(dataTexts, this.ctx);
            }
            return gText;
        }
    };
    Chart.prototype.textSecondaryGrid = function (maxVal, step, truongHop, percentChart) {
        if (percentChart === void 0) { percentChart = false; }
        var seccondaryVertical = this.options.DataAxes.ListDataAxes[2];
        var stepHeight = this.stepHeight(step, maxVal);
        var dataTexts = [];
        var xMax = seccondaryVertical.Left;
        var y = this.yChart + this.hChart;
        var color = 'black';
        var font = this.options.DataAxes.ListDataAxes[2].FontFamily;
        var size = this.options.DataAxes.ListDataAxes[2].FontSize;
        var soVongLap = -1;
        var multi100 = false;
        if (step !== 0) {
            soVongLap = maxVal / step;
        }
        if (truongHop === 1) {
            maxVal = 100;
            step = 10;
            percentChart = true;
        }
        else if (truongHop === 2) {
            maxVal = maxVal;
            step = step;
            percentChart = true;
            multi100 = true;
        }
        else if (truongHop === 3) {
            percentChart = false;
        }
        var h = maxVal;
        var text;
        var gText;
        if (this.options.DataAxes.ListDataAxes[2].IsChecked) {
            for (var n = 0; n <= soVongLap; n++) {
                if (multi100) {
                    text = convertFloatingPoint(maxVal - h) * 100;
                }
                else {
                    text = convertFloatingPoint(maxVal - h);
                }
                h -= step;
                var point = new Point(xMax, y);
                GenerateText(dataTexts, "id", text, point, font, size, color, "start", "middle");
                y -= stepHeight;
            }
            if (percentChart) {
                gText = drawTextPercent(dataTexts, this.ctx);
            }
            else {
                gText = drawText(dataTexts, this.ctx);
            }
        }
        return gText;
    };
    Chart.prototype.maxData = function (getArrData) {
        var numCategory = this.numCategory();
        var max = 0;
        for (var i = 0; i < getArrData.length; i++) {
            for (var j = 0; j < numCategory; j++) {
                if (max < getArrData[i][j]) {
                    max = getArrData[i][j];
                }
            }
        }
        return max;
    };
    Chart.prototype.maxArr = function () {
        var arrNumber = [];
        var numCategory = this.numCategory();
        var max = 0;
        for (var i = 0; i < numCategory; i++) {
            if (max < arrNumber[i]) {
                max = arrNumber[i];
            }
        }
        return max;
    };
    Chart.prototype.drawLegendTemplate = function () {
        var gChildLegend;
        var gLegend = GlobalHelper.createTags('g', '', 'legendsChart');
        for (var dem = 0; dem < this.options.DataLegend.Series.length; dem++) {
            var seri = this.options.DataLegend.Series[dem];
            var textAlign = "start";
            var textBaseline = "hanging";
            gChildLegend = ChartModule.drawLegendTien(seri, textAlign, textBaseline);
            gLegend.appendChild(gChildLegend);
        }
        return gLegend;
    };
    Chart.prototype.drawDataLabel = function (texts) {
        var dataTexts = [];
        var point;
        var index;
        for (var i = 0; i < texts.length; i++) {
            point = new Point(texts[i].Left, texts[i].Top);
            GenerateText(dataTexts, texts[i].Content, texts[i].Content, point, texts[i].FontFamily, texts[i].FontSize, texts[i].Foreground.Color, "left", "hanging");
        }
        return drawText(dataTexts, this.ctx);
    };
    Chart.prototype.fillTextCategory = function () {
        var categoryTranslate = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
        var axesHorizontal = this.options.DataAxes.ListDataAxes[0];
        if (axesHorizontal.IsChecked) {
            var heightCategory = axesHorizontal.Height;
            var numCategory = this.numCategory();
            var avgWidth = this.avgWidth(categoryTranslate);
            var fontFamily = axesHorizontal.FontFamily;
            var fontSize = axesHorizontal.FontSize;
            var colorText = axesHorizontal.Stroke.Color;
            var yOx = this.yChart + this.hChart;
            var dataTexts = [];
            var categoryName = this.getCategory();
            for (var i = 0; i < numCategory; i++) {
                if (categoryTranslate) {
                    var point = new Point(this.xChart + avgWidth * i, yOx + heightCategory / 2);
                }
                else {
                    var point = new Point(this.xChart + avgWidth * i + avgWidth / 2, yOx + heightCategory / 2);
                }
                GenerateText(dataTexts, categoryName[i], categoryName[i], point, fontFamily, fontSize, colorText, "middle", "hanging");
            }
            return drawText(dataTexts);
        }
    };
    Chart.prototype.fillTextAxisTitle = function () {
        return;
        var axisTitles = this.options.axisTitle.axisTitles;
        if (axisTitles) {
            this.ctx.fillStyle = 'red';
            for (var i = 0; i < axisTitles.length; i++) {
                var p = new Point(axisTitles[i].x, axisTitles[i].y);
                var t = new Text("id", axisTitles[i].text, p, axisTitles[i].fontFamily, axisTitles[i].size, axisTitles[i].color, "left", "start", axisTitles[i].rotate);
                t.draw(this.ctx);
            }
        }
    };
    return Chart;
}());
var ChartModule = function () {
    return {
        Level: GlobalEnum.ModuleLevel.Element,
        ContentType: "Chart",
        generateElement: function (elementData) {
            var g = new Chart(elementData);
            return g;
        },
        convertOption: function convertOption(options) {
            var data = [];
            for (var i = 0; i < options.C.length; i++) {
                var categoryNew = options.C[i];
                var _series = [];
                for (var j = 0; j < categoryNew.Se.length; j++) {
                    var _seriNew = categoryNew.Se[j];
                    var seri = {
                        "ID": _seriNew.id,
                        "Opacity": _seriNew.opc,
                        "IDSeri": _seriNew.idSr,
                        "ChartKey": _seriNew.cK,
                        "TemplateChart": _seriNew.tC,
                        "Type": _seriNew.typ,
                        "Name": _seriNew.nm,
                        "Value": _seriNew.vl,
                        "IsPrimaryAxis": _seriNew.IPA,
                        "SeriOption": _seriNew.sO,
                        "FontFamily": _seriNew.ff,
                        "FontSize": _seriNew.fs,
                        "ICircle": _seriNew.iC,
                        "PointStart": _seriNew.pS,
                        "Thickness": _seriNew.Tn,
                        "JoinType": _seriNew.jT,
                        "CapType": _seriNew.cT,
                        "DashType": _seriNew.dT,
                        "Fill": _seriNew.f,
                        "Stroke": _seriNew.st
                    };
                    _series.push(seri);
                }
                var category = {
                    "CategoryName": categoryNew.cN,
                    "FontSize": categoryNew.fs,
                    "FontFamily": categoryNew.ff,
                    "Series": _series
                };
                data.push(category);
            }
            var datalegendSeriesOld = [];
            if (options.dle) {
                var _dataLegendSeries = options.dle.Se;
                for (var i = 0; i < _dataLegendSeries.length; i++) {
                    var _seriLegend = _dataLegendSeries[i];
                    var _shapeNew = _dataLegendSeries[i].sh;
                    var _textNew = _dataLegendSeries[i].txt;
                    var _seriLegendOld = {
                        "Shape": {
                            "id": _shapeNew.id,
                            "Type": _shapeNew.typ,
                            "Top": _shapeNew.T,
                            "Left": _shapeNew.l,
                            "Width": _shapeNew.w,
                            "Height": _shapeNew.h,
                            "Fill": _shapeNew.f,
                            "Stroke": _shapeNew.st,
                            "Thickness": _shapeNew.Tn,
                            "JoinType": _shapeNew.jT,
                            "CapType": _shapeNew.cpT,
                            "DashType": _shapeNew.DaT,
                            "FontSize": _shapeNew.fs,
                            "FontFamily": _shapeNew.ff
                        },
                        "Text": {
                            "Top": _textNew.T,
                            "Left": _textNew.l,
                            "Width": _textNew.w,
                            "Height": _textNew.h,
                            "Content": _textNew.Con,
                            "Foreground": _textNew.fg,
                            "Thickness": _textNew.Tn,
                            "JoinType": _textNew.jT,
                            "CapType": _textNew.cpT,
                            "DashType": _textNew.dT,
                            "FontSize": _textNew.fs,
                            "FontFamily": _textNew.ff
                        }
                    };
                    datalegendSeriesOld.push(_seriLegendOld);
                }
            }
            var dataLegend = {
                "Series": datalegendSeriesOld
            };
            var _listDataAxesOld = [];
            if (options.DA) {
                var _listDataAxesNew = options.DA.LiDA;
                var primaryHorizontal = _listDataAxesNew[0];
                _listDataAxesOld.push({
                    "Key": primaryHorizontal.k,
                    "IsChecked": primaryHorizontal.iCk,
                    "Top": primaryHorizontal.T,
                    "Left": primaryHorizontal.l,
                    "Width": primaryHorizontal.w,
                    "Height": primaryHorizontal.h,
                    "Fill": primaryHorizontal.f,
                    "Stroke": primaryHorizontal.st,
                    "Foreground": primaryHorizontal.fg,
                    "Thickness": primaryHorizontal.Tn,
                    "JoinType": primaryHorizontal.jT,
                    "CapType": primaryHorizontal.cpT,
                    "DashType": primaryHorizontal.DaT,
                    "FontSize": primaryHorizontal.fs,
                    "FontFamily": primaryHorizontal.ff
                });
                var primaryVertical = _listDataAxesNew[1];
                _listDataAxesOld.push({
                    "Key": primaryVertical.k,
                    "IsChecked": primaryVertical.iCk,
                    "Top": primaryVertical.T,
                    "Left": primaryVertical.l,
                    "Width": primaryVertical.w,
                    "Height": primaryVertical.h,
                    "Fill": primaryVertical.f,
                    "Stroke": primaryVertical.st,
                    "Foreground": primaryVertical.fg,
                    "Thickness": primaryVertical.Tn,
                    "JoinType": primaryVertical.jT,
                    "CapType": primaryVertical.cpT,
                    "DashType": primaryVertical.DaT,
                    "FontSize": primaryVertical.fs,
                    "FontFamily": primaryVertical.ff
                });
                var primarySecondary = _listDataAxesNew[2];
                _listDataAxesOld.push({
                    "Key": primarySecondary.k,
                    "IsChecked": primarySecondary.iCk,
                    "Top": primarySecondary.T,
                    "Left": primarySecondary.l,
                    "Width": primarySecondary.w,
                    "Height": primarySecondary.h,
                    "Fill": primarySecondary.f,
                    "Stroke": primarySecondary.st,
                    "Foreground": primarySecondary.fg,
                    "Thickness": primarySecondary.Tn,
                    "JoinType": primarySecondary.jT,
                    "CapType": primarySecondary.cpT,
                    "DashType": primarySecondary.DaT,
                    "FontSize": primarySecondary.fs,
                    "FontFamily": primarySecondary.ff
                });
            }
            var dataAxes = {
                "ListDataAxes": _listDataAxesOld
            };
            var dataChartTitleNew = options.DCT;
            var dataChartTitleOld;
            if (dataChartTitleNew) {
                dataChartTitleOld = {
                    "Top": dataChartTitleNew.T,
                    "Left": dataChartTitleNew.l,
                    "Width": dataChartTitleNew.w,
                    "Height": dataChartTitleNew.h,
                    "Text": dataChartTitleNew.txt,
                    "Fill": dataChartTitleNew.f,
                    "Stroke": dataChartTitleNew.st,
                    "ForegroundText": dataChartTitleNew.fg,
                    "Thickness": dataChartTitleNew.Tn,
                    "JoinType": dataChartTitleNew.jT,
                    "CapType": dataChartTitleNew.cpT,
                    "DashType": dataChartTitleNew.DaT,
                    "FontSize": dataChartTitleNew.fs,
                    "FontFamily": dataChartTitleNew.ff
                };
            }
            var datagridLine = {
                "IsGridlineKey": (options.DGL) ? options.DGL.ik : null,
                "IsPrimaryMajorHorizontal": (options.DGL) ? options.DGL.iMah : null,
                "IsPrimaryMinorHorizontal": (options.DGL) ? options.DGL.iMih : null,
                "IsPrimaryMajorVertical": (options.DGL) ? options.DGL.iMav : null,
                "IsPrimaryMinorVertical": (options.DGL) ? options.DGL.iMiv : null,
                "StrokeMajorHorizontal": (options.DGL) ? options.DGL.smaH : null,
                "StrokeMinorHorizontal": (options.DGL) ? options.DGL.smiH : null,
                "StrokeMajorVertical": (options.DGL) ? options.DGL.smiV : null,
                "StrokeMinorVertical": (options.DGL) ? options.DGL.smaV : null
            };
            var _configSeriOptionNew = options.cSo;
            var configPrimaryAxisNew = _configSeriOptionNew.pA;
            var configSecondaryAxisNew = _configSeriOptionNew.sA;
            if (configPrimaryAxisNew != null) {
                var configprimaryAxisOld = {
                    "OverLap": configPrimaryAxisNew.ov,
                    "GapWidth": configPrimaryAxisNew.gW
                };
            }
            if (configSecondaryAxisNew != null) {
                var configSecondaryAxisOld = {
                    "OverLap": configSecondaryAxisNew.ov,
                    "GapWidth": configSecondaryAxisNew.gW
                };
            }
            var hTMLSeriOptionPie = null;
            if (options.typ === "PieChart") {
                hTMLSeriOptionPie = {
                    "AngleOfFirstSlice": options.cSo.hS.aO
                };
            }
            var _configSeriOptionOld = {
                "PrimaryAxis": configprimaryAxisOld,
                "SecondaryAxis": configSecondaryAxisOld,
                "HTMLSeriOptionPie": hTMLSeriOptionPie
            };
            var dataTableNew = options.DT;
            if (dataTableNew) {
                var tableBorderNew = dataTableNew.tB;
                var dataTableChildNew = dataTableNew.dT;
                if (dataTableChildNew != null) {
                    var dataTableChildOld = {
                        "WidthLeftTable": dataTableChildNew.hLt,
                        "WidthTableLegend": dataTableChildNew.wTl,
                        "WidthTableSeri": dataTableChildNew.wTs,
                        "Fill": dataTableChildNew.f,
                        "Stroke": dataTableChildNew.st,
                        "Thickness": dataTableChildNew.Tn,
                        "JoinType": dataTableChildNew.JT,
                        "CapType": dataTableChildNew.cpT,
                        "DashType": dataTableChildNew.DaT,
                        "FontSize": dataTableChildNew.fs,
                        "FontFamily": dataTableChildNew.ff
                    };
                }
                var heightTableNew = dataTableNew.hT;
                if (heightTableNew != null) {
                    var _seriDataTableNew = heightTableNew.Se;
                    var _seriDataTableOld = [];
                    for (var i = 0; i < _seriDataTableNew.length; i++) {
                        var seriTable = _seriDataTableNew[i];
                        _seriDataTableOld.push({
                            "id": seriTable.id,
                            "Height": seriTable.h,
                            "Name": seriTable.nm,
                            "WidthLegend": seriTable.wL,
                            "HeightLegend": seriTable.hL
                        });
                    }
                    var heightTableOld = {
                        "HeightCategory": heightTableNew.hC,
                        "Series": _seriDataTableOld
                    };
                }
                if (tableBorderNew != null) {
                    var tableBorderOld = {
                        "IsHorizontal": tableBorderNew.iH,
                        "IsVertical": tableBorderNew.iV,
                        "IsOutline": tableBorderNew.iO
                    };
                }
                var _dataTableOld = {
                    "IsDataTableKey": dataTableNew.iDt,
                    "IsWithLegendKey": dataTableNew.iWl,
                    "TableBorder": tableBorderOld,
                    "DataTable": dataTableChildOld,
                    "HeightTable": heightTableOld
                };
            }
            var dataLabelNew = options.dla;
            var textsOld = [];
            for (var i = 0; i < dataLabelNew.Te.length; i++) {
                var textNew = dataLabelNew.Te[i];
                textsOld.push({
                    "Top": textNew.T,
                    "Left": textNew.l,
                    "Width": textNew.w,
                    "Height": textNew.h,
                    "Content": textNew.Con,
                    "Foreground": textNew.fg,
                    "Thickness": textNew.Tk,
                    "JoinType": textNew.JT,
                    "CapType": textNew.cpT,
                    "DashType": textNew.DaT,
                    "FontSize": textNew.fs,
                    "FontFamily": textNew.ff
                });
            }
            var dataLabelOld = {
                "Texts": textsOld
            };
            var optionConverted = {
                "Type": options.typ,
                "DataChartTitle": dataChartTitleOld,
                "DataAxes": dataAxes,
                "DataAxisTitle": options.DataAxisTitle,
                "DataTable": _dataTableOld,
                "DataGridline": datagridLine,
                "DataLegend": dataLegend,
                "DataLabel": dataLabelOld,
                "Data": data,
                "FrameChart": options.frC,
                "ChartKey": options.Ck,
                "TemplateChart": options.TCh,
                "ConfigSeriOption": _configSeriOptionOld,
                "BorderThickness": options.bTh,
                "BorderBrush": options.bB,
                "PathData": options.pD,
                "CompoundType": options.cpT,
                "DashType": options.dT,
                "CapType": options.cT,
                "Fill": options.fi,
                "Effects": options.efs,
                "Shapes": options.shs,
                "Width": options.w,
                "Height": options.h,
                "Top": options.T,
                "Left": options.l,
                "Angle": options.agl,
                "ZIndex": options.z,
                "Animations": options.a,
                "Timing": options.tim,
                "ContentKeyType": options.c,
                "ID": options.id,
                "Name": options.nm,
                "Triggers": options.tri
            };
            return optionConverted;
        },
        drawLegend: function drawLegend(id, point, w, h, pointText, thickness, content, font, size, colorStroke, textAlign, textBaseline, type) {
            if (type === void 0) { type = 'rect'; }
            var xSquare = point.x;
            var ySquare = point.y;
            var colorSquare = point.color;
            var gLegend = GlobalHelper.createTags('g');
            if (type === "rect") {
                var rect = new Rect(id, point, w, h, colorSquare, true, colorStroke, thickness);
                var fillColor = FillColorHelper.genGTag(colorSquare, ConfigFrame(rect.getElement()));
                if (colorSquare.CoT !== "Solid")
                    gLegend.appendChild(fillColor.Tag);
                rect.color = { "col": fillColor.ID, "opc": fillColor.opacity };
                gLegend.appendChild(rect.getElement());
            }
            else if (type === "line") {
                var point1 = new Point(xSquare + w, ySquare);
                var line = new Line(point, point1, colorSquare.col, thickness);
                gLegend.appendChild(line.getElement());
            }
            var text = new TextChart(content, content, pointText, font, size, pointText.colorText, textAlign, textBaseline);
            gLegend.appendChild(text.getElement());
            return gLegend;
        },
        drawLegendTien: function drawLegendTien(seri, textAlign, textBaseline) {
            var gLegend = GlobalHelper.createTags('g');
            var point = new Point(seri.Shape.Left, seri.Shape.Top);
            if (seri.Shape.Type === "rect") {
                var rect = new Rect(seri.Shape.id, point, seri.Shape.Width, seri.Shape.Height, seri.Shape.Fill, true, seri.Shape.Stroke, seri.Shape.Thickness);
                var fillColor = FillColorHelper.genGTag(seri.Shape.Fill, ConfigFrame(rect.getElement()));
                if (seri.Shape.Fill.CoT !== "Solid")
                    gLegend.appendChild(fillColor.Tag);
                rect.color = { "col": fillColor.ID, "opc": fillColor.opacity };
                gLegend.appendChild(rect.getElement());
            }
            else if (seri.Shape.Type === "line") {
                var point1 = new Point(seri.Shape.Left + seri.Shape.Width, seri.Shape.Top);
                var line = new Line(point, point1, seri.Shape.Stroke.col, seri.Shape.Thickness);
                gLegend.appendChild(line.getElement());
            }
            var pointText = new Point(seri.Text.Left, seri.Text.Top);
            var text = new TextChart(seri.Text.Content, seri.Text.Content, pointText, seri.Text.FontFamily, seri.Text.FontSize, seri.Text.Foreground.Color, textAlign, textBaseline);
            gLegend.appendChild(text.getElement());
            return gLegend;
        }
    };
}();
GlobalResources.addModule(ChartModule);
