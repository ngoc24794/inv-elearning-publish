"use strict";
function convertToFixed(inputnum) {
    var mynum = inputnum.toPrecision(16);
    var mynumstr = mynum.toString();
    var final = parseFloat(mynumstr);
    return final;
}
function convertFloatingPoint(num) {
    var precision = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 12;
    return parseFloat(num.toPrecision(precision));
}

Array.prototype.uniqueMerge = function (a) {
    for (var nonDuplicates = [], i = 0, l = a.length; i < l; ++i) {
        if (this.indexOf(a[i]) === -1) {
            nonDuplicates.push(a[i]);
        }
    }
    return this.concat(nonDuplicates);
};
var Point = (function () {
    function Point(x, y, color) {
        this.x = x;
        this.y = y;
        this.color = color || "black";
    }
    Point.prototype.distance = function (point1) {
        return Math.sqrt(Math.pow(this.x - point1.x, 2) + Math.pow(this.y - point1.y, 2));
    };
    return Point;
}());
var Line = (function () {
    function Line(point1, point2, color, lineWidth) {
        this.point1 = point1;
        this.point2 = point2;
        this.color = color || "black";
        this.lineWidth = lineWidth || 1;
    }
    Line.prototype.getElement = function () {
        var path = GlobalHelper.createTags("path");
        path.setAttribute("d", "M " + this.point1.x + " " + this.point1.y + " L " + this.point2.x + " " + this.point2.y);
        path.setAttribute("stroke", this.color);
        path.setAttribute("fill", 'none');
        path.setAttribute("stroke-width", this.lineWidth);
        return path;
    };
    return Line;
}());
var Arc = (function () {
    function Arc(center, radius, startAngle, endAngle, color, fill) {
        this.center = center;
        this.radius = radius || 3;
        this.startAngle = startAngle || 0;
        this.endAngle = endAngle || 2 * Math.PI;
        this.color = color || this.center.color || "black";
        this.fill = fill || true;
    }
    Arc.prototype.draw = function (ctx) {
        ctx.beginPath();
        ctx.arc(this.center.x, this.center.y, this.radius, this.startAngle, this.endAngle);
        if (this.fill) {
            ctx.fillStyle = this.color;
            ctx.fill();
        }
        else {
            ctx.strokeStyle = this.color;
            ctx.stroke();
        }
    };
    return Arc;
}());
var Rect = (function () {
    function Rect(id, point, w, h, color, fill, colorStroke, strokeWidth, opacity) {
        if (strokeWidth === void 0) { strokeWidth = 0; }
        if (opacity === void 0) { opacity = 1; }
        this.id = id;
        this.point = point;
        this.w = w;
        this.h = h;
        this.color = color;
        this.colorStroke = colorStroke;
        this.strokeWidth = strokeWidth;
        this.fill = fill;
    }
    Rect.prototype.getElement = function () {
        var rect = GlobalHelper.createTags("rect");
        rect.setAttribute("id", this.id);
        rect.setAttribute("x", this.point.x);
        rect.setAttribute("y", this.point.y);
        rect.setAttribute("width", this.w);
        rect.setAttribute("height", this.h);
        rect.setAttribute("fill", this.color.col);
        if (this.colorStroke) {
            rect.setAttribute('stroke', this.colorStroke.col);
        }
        if (this.color.opc) {
            rect.setAttribute('opacity', this.color.opc);
        }
        return rect;
    };
    return Rect;
}());
var TextChart = (function () {
    function TextChart(id, text, point, font, size, color, textAlign, textBaseline, rotate) {
        this.id = id;
        this.point = point;
        this.text = text;
        this.font = font;
        this.size = size;
        this.color = color || "black";
        this.textAlign = textAlign || "start";
        this.textBaseline = textBaseline || "hanging";
        this.rotate = rotate;
    }
    TextChart.prototype.getElement = function (isPercent) {
        if (isPercent === void 0) { isPercent = false; }
        var text = GlobalHelper.createTags('text');
        text.setAttribute('x', this.point.x);
        text.setAttribute('y', this.point.y);
        text.setAttribute('style', 'color:' + this.color + '; font-size:' + this.size + 'px; fill:' + this.point.color);
        text.setAttribute('text-anchor', this.textAlign);
        text.setAttribute('dominant-baseline', this.textBaseline);
        text.setAttribute('font-family', this.font);
        var content = document.createTextNode(this.text);
        text.appendChild(content);
        return text;
    };
    return TextChart;
}());
var Canvas = (function () {
    function Canvas(id, w_canvas, h_canvas, top_canvas, left_canvas) {
        if (w_canvas === void 0) { w_canvas = 800; }
        if (h_canvas === void 0) { h_canvas = 600; }
        if (top_canvas === void 0) { top_canvas = 0; }
        if (left_canvas === void 0) { left_canvas = 0; }
        this.id = id;
        this.w_canvas = w_canvas;
        this.h_canvas = h_canvas;
        this.top_canvas = top_canvas;
        this.left_canvas = left_canvas;
    }
    Canvas.prototype.genCanvas = function () {
        var canv = document.createElement("canvas");
        var ctx = canv.getContext("2d");
        canv.id = this.id;
        canv.width = this.w_canvas;
        canv.height = this.h_canvas;
        canv.style.position = "absolute";
        canv.style.top = this.top_canvas + "px";
        canv.style.left = this.left_canvas + "px";
        return canv;
    };
    Canvas.prototype.returnElement = function () {
        return this.genCanvas();
    };
    return Canvas;
}());
var Axes = (function () {
    function Axes(primaryHorizontal, primaryVertical) {
        if (primaryHorizontal === void 0) { primaryHorizontal = true; }
        if (primaryVertical === void 0) { primaryVertical = true; }
        this.primaryHorizontal = primanyHorizontal;
        this.primaryVertical = primaryVertical;
    }
    return Axes;
}());
var GridLine = (function () {
    function GridLine(primaryMajorHorizontal, primaryMajorVertical, primaryMinorHorizontal, primaryMinorVertical, colorMajorHorizontal, colorMinorHorizontal, colorMajorVertical, colorMinorVertical) {
        this.primaryMajorHorizontal = primaryMajorHorizontal || false;
        this.primaryMajorVertical = primaryMajorVertical || false;
        this.primaryMinorVertical = primaryMinorVertical || false;
        this.primaryMinorHorizontal = primaryMinorHorizontal || false;
        this.colorMajorHorizontal = colorMajorHorizontal || "#000000";
        this.colorMinorHorizontal = colorMinorHorizontal || "#DFDFDF";
        this.colorMajorVertical = colorMajorVertical || "#7F7F7F";
        this.colorMinorVertical = colorMinorVertical || "#6F6F6F";
    }
    return GridLine;
}());
var DataLable = (function () {
    function DataLable(center, insideEnd, insideBase, outsideEnd, dataCallout) {
        this.center = center || false;
        this.insideBase = insideBase || false;
        this.insideEnd = insideEnd || false;
        this.outsideEnd = outsideEnd || false;
        this.dataCallout = dataCallout || false;
    }
    return DataLable;
}());
var DataTable = (function () {
    function DataTable(horizontal, vertical, outline, showLegendKey, tableKey) {
        this.horizontal = horizontal;
        this.vertical = vertical;
        this.outline = outline;
        this.showLegendKey = showLegendKey;
        this.tableKey = tableKey;
    }
    DataTable.prototype.DataTable = function () {
        vertical = false;
        horizontal = false;
        outline = false;
        showLegendKey = false;
        tableKey = false;
    };
    return DataTable;
}());
var SeriOption = (function () {
    function SeriOption(primaryAxis, secondaryAxis, gapWidth, serisOverlap) {
        this.primaryAxis = primaryAxis;
        this.secondaryAxis = secondaryAxis;
        this.gapWidth = gapWidth;
        this.serisOverlap = serisOverlap;
    }
    return SeriOption;
}());
var Seri = (function () {
    function Seri(value, name, w, h, seriOverlap, gapWidth, gapWidthChangeOverlap, point, dataLable, isCheckLeft, isCheckRight, isSecondary) {
        this.value = value;
        this.name = name;
        this.w = w;
        this.h = h;
        this.serisOverlap = serisOverlap;
        this.gapWidth = gapWidth;
        this.gapWidthChangeOverlap = gapWidthChangeOverlap;
        this.point = point;
        this.dataLable = dataLable;
        this.isCheckLeft = isCheckLeft;
        this.isCheckRight = isCheckRight;
        this.isSecondary = isSecondary;
    }
    return Seri;
}());
function CalculatorStep(sMax) {
    var m = 0;
    var a = [1.0, 2.0, 5.0, 10.0];
    var x = sMax / 9.52;
    var z = Math.floor(Math.log(x) / Math.log(10));
    for (var i = 0; i < a.length; i++) {
        a[i] = a[i] * Math.pow(10, z);
        if (x <= a[i]) {
            m = a[i];
            break;
        }
    }
    return m;
}
function GenerateLine(dataLines, point1, point2, color, stroke, lineWidth) {
    if (color === void 0) { color = "black"; }
    if (lineWidth === void 0) { lineWidth = 1; }
    dataLines.push(new Line(point1, point2, color, stroke, lineWidth));
}
function GenerateText(dataTexts, id, text, point, font, size, color, textAlign, textBaseline, rotate) {
    dataTexts.push(new TextChart(id, text, point, font, size, color, textAlign, textBaseline, rotate));
}
function drawLine(dataLines) {
    var g = GlobalHelper.createTags('g');
    for (var k = 0; k < dataLines.length; k++) {
        var line = new Line(dataLines[k].point1, dataLines[k].point2, dataLines[k].color, dataLines[k].lineWidth);
        g.appendChild(line.getElement());
    }
    return g;
}
function drawText(dataTexts) {
    var g = GlobalHelper.createTags('g');
    for (var i = 0; i < dataTexts.length; i++) {
        var text = new TextChart(dataTexts[i].id, dataTexts[i].text, dataTexts[i].point, dataTexts[i].font, dataTexts[i].size, dataTexts[i].color, dataTexts[i].textAlign, dataTexts[i].textBaseline, dataTexts[i].rotate);
        g.appendChild(text.getElement());
    }
    return g;
}
function drawTextPercent(dataTexts) {
    var g = GlobalHelper.createTags('g');
    for (var i = 0; i < dataTexts.length; i++) {
        var text = new TextChart(dataTexts[i].id, dataTexts[i].text + "%", dataTexts[i].point, dataTexts[i].font, dataTexts[i].size, dataTexts[i].color, dataTexts[i].textAlign, dataTexts[i].textBaseline, dataTexts[i].rotate);
        g.appendChild(text.getElement());
    }
    return g;
}
function upperCaseString(str) {
    return str.charAt(0).toUpperCase().concat(str.slice(1));
}
function isExistInArray(arr, value) {
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] === value)
            return true;
    }
    return false;
}
function findValInArrayAttributeId(seriesOption, val) {
    for (var t = 0; t < seriesOption.length; t++) {
        if (seriesOption[t].ID === val)
            return seriesOption[t];
    }
}
