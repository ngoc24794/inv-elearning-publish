var PieChart = (function () {
    function PieChart(chart) {
        this.chart = chart;
        this.options = chart.options;
        this.fillColors = chart.options.Data.map(function (x) { return x.Series[0].Fill.col; });
        this.strokeColors = chart.options.Data.map(function (x) { return x.Series[0].Stroke.col; });
        this.strokeTypeDashs = chart.options.Data.map(function (x) { return x.Series[0].DashType; });
        this.thickness = chart.options.Data.map(function (x) { return x.Series[0].Thickness; });
        this.nameSeries = chart.options.Data.map(function (x) { return x.CategoryName; });
        var iCircles = chart.options.Data.map(function (x) { return x.Series[0].ICircle; });
        var pointStart = chart.options.Data.map(function (x) { return x.Series[0].PointStart; });
        var points = [];
        for (var i = 0; i < this.nameSeries.length; i++) {
            var currentName = this.nameSeries[i];
            var pointSeri = { ICircle: iCircles[i], PointStart: pointStart[i] };
            points.push(pointSeri);
        }
        this.points = points;
    }
    PieChart.prototype.gChart = function (ctx) {
        this.chart.gChart = GlobalHelper.createTags('g', '', 'pieChart');
        var slice_angles = this.slice_angles();
        var color_index = 0;
        var start_angle = this.options.ConfigSeriOption.HTMLSeriOptionPie.AngleOfFirstSlice;
        for (var i = 0; i < this.nameSeries.length; i++) {
            var x1 = parseFloat(this.points[i].ICircle.split(",")[0]);
            var y1 = parseFloat(this.points[i].ICircle.split(",")[1]);
            var x2 = parseFloat(this.points[i].PointStart.split(",")[0]);
            var y2 = parseFloat(this.points[i].PointStart.split(",")[1]);
            var x_cp = x2 - x1;
            var y_cp = y2 - y1;
            var point1 = new Point(x1, y1);
            var point2 = new Point(x2, y2);
            var radius = point1.distance(point2);
            color_index = i;
            this.chart.gChart.appendChild(this.drawPieSlice(point1, radius, start_angle, start_angle + slice_angles[i], this.fillColors[color_index % this.fillColors.length], this.strokeColors[color_index % this.strokeColors.length], this.thickness[color_index % this.thickness.length], ShapeHelper.getDashArray(this.strokeTypeDashs[i])));
            start_angle += slice_angles[i];
        }
        var gLegend = this.chart.drawLegendTemplate();
        this.chart.gChart.appendChild(gLegend);
        if (this.chart.options.DataLabel.Texts.length > 0) {
            var gDataLabel = this.chart.drawDataLabel(this.chart.options.DataLabel.Texts);
            gDataLabel.setAttribute('class', 'dataLabel');
            this.chart.gChart.appendChild(gDataLabel);
        }
        return this.chart.gChart;
    };
    PieChart.prototype.drawPieSlice = function (center, radius, startAngle, endAngle, fillColor, strokeColor, strokeWidth, dashArray, strokeLineJoin) {
        if (strokeColor === void 0) { strokeColor = 'white'; }
        if (strokeWidth === void 0) { strokeWidth = 1; }
        if (strokeLineJoin === void 0) { strokeLineJoin = 'round'; }
        var path = GlobalHelper.createTags('path');
        path.setAttribute('stroke', strokeColor);
        path.setAttribute('stroke-width', strokeWidth);
        path.setAttribute('stroke-dasharray', dashArray);
        path.setAttribute('stroke-linejoin', strokeLineJoin);
        path.setAttribute('fill', fillColor);
        path.setAttribute('d', this.describeArc(center.x, center.y, radius, startAngle, endAngle));
        return path;
    };
    PieChart.prototype.slice_angles = function () {
        var total_value = 0;
        var color_index = 0;
        for (var i = 0; i < this.options.Data.length; i++) {
            var val = this.options.Data[i].Series[0].Value;
            total_value += val;
        }
        var slice_angles = [];
        for (i = 0; i < this.options.Data.length; i++) {
            val = this.options.Data[i].Series[0].Value;
            var slice_angle = 2 * Math.PI * val / total_value * 180 / Math.PI;
            slice_angles.push(slice_angle);
        }
        return slice_angles;
    };
    PieChart.prototype.polarToCartesian = function (centerX, centerY, radius, angleInDegrees) {
        var angleInRadians = (angleInDegrees - 90) * Math.PI / 180.0;
        return {
            x: centerX + (radius * Math.cos(angleInRadians)),
            y: centerY + (radius * Math.sin(angleInRadians))
        };
    };
    PieChart.prototype.describeArc = function (x, y, radius, startAngle, endAngle) {
        var start = this.polarToCartesian(x, y, radius, endAngle);
        var end = this.polarToCartesian(x, y, radius, startAngle);
        var largeArcFlag = (endAngle - startAngle <= 180) ? "0" : "1";
        var d = [
            "M", start.x, start.y,
            "A", radius, radius, 0, largeArcFlag, 0, end.x, end.y,
            "L", x, y,
            "L", start.x, start.y
        ].join(" ");
        return d;
    };
    return PieChart;
}());
