"use strict";
var TriggerActionModule = (function() {
    return {
        setTriggerAction: function(elementID, eventName) { //Thực thi hành động khi xảy ra sự kiện eventName lên đối tượng có ID là elementID
            elementID = elementID.split('_')[0];
            let elementData = findElementData(elementID);
            if (elementData != null) {
                //let eventData = elementData.tri.filter(x => x.E == eventName);
                var eventData = elementData.tri.filter(function(x){
                    if(x.E==eventName)
                    {
                        return x;
                    }
                });
                for (var i = 0; i < eventData.length; i++) {
                    this.getTriggerAction(eventData[i]);
                }
            }
        },

        getTriggerAction: function(triggerData) { //Thực thi các hành động
            let actionData = triggerData.A;
            if (actionData != null) {
                if (!checkConditions(triggerData.C, DOC)) return; // Nếu không thỏa mãn các giá trị kèm theo
                switch (actionData) {
                    /*==== BEGIN Group Common Action ====*/
                    case "ShowLayer":
                        layerEffect = triggerData.S;
                        var layout = GlobalResources.CurrentSlide.getLayoutById(layerEffect),
                            Timeline = layout.Timeline;
                        if (layout != null) {
                            layout.show(true);
                        }
                        break;
                    case "HideLayer":
                        layerEffect = triggerData.S;
                        var layout = GlobalResources.CurrentSlide.getLayoutById(layerEffect);
                        if (layout != null) {
                            layout.hide();
                        }
                        QuizzModule.showEventSubmit(layerEffect);
                        break;
                    case "JumpToSlide":
                        layerEffect = triggerData.S;
                        GlobalHelper.jumToSlideID(layerEffect);
                        break;
                    case "LightboxSlide":
                        layerEffect = triggerData.S;
                        if (layerEffect != null && layerEffect != "Unassigned") {
                            insertLightboxSlide(layerEffect, triggerData);
                        }
                        break;
                    case "CloseLightbox":
                        closeLightBox();
                        break;
                    case "Move":
                        moveAction(triggerData);
                        break;
                        /*==== END Group Common Action ====*/

                        /*==== BEGIN Group Media Action ====*/
                    case "PlayMedia":
                        layerEffect = triggerData.S;
                        if (layerEffect != null && layerEffect != "Unassigned" && actionData != null) {
                            $('*[id$=video_' + layerEffect + ']').each(function() {
                                PlayPauseStopVideo($(this), actionData);
                            });
                        }
                        break;
                    case "PauseMedia":
                        layerEffect = triggerData.S;
                        if (layerEffect != null && layerEffect != "Unassigned" && actionData != null) {
                            $('*[id$=video_' + layerEffect + ']').each(function() {
                                PlayPauseStopVideo($(this), actionData);
                            });
                        }
                        break;
                    case "StopMedia":
                        layerEffect = triggerData.S;
                        if (layerEffect != null && layerEffect != "Unassigned" && actionData != null) {
                            $('*[id$=video_' + layerEffect + ']').each(function() {
                                PlayPauseStopVideo($(this), actionData);
                            });
                        }
                        break;
                        /*==== END Group Media Action ====*/

                        /*==== BEGIN Group Project Action ====*/
                    case "RestartCourse":
                        if (actionData != null) {
                            location.reload('_self');
                        }
                        break;
                    case "ExitCourse":
                        if (actionData != null) {
                            close();
                            if (navigator.userAgent.indexOf("Firefox") != -1) {
                                alert('NOTE: This command can not be used in brower firefox');
                            }
                        }
                        break;
                        /*==== END Group Project Action ====*/

                        /*==== BEGIN Group More Action ====*/
                    case "AdjustVariable":
                        ajustVariable(triggerData);
                        break;
                    case "PauseTimeline":
                        var layerEffect = triggerData.S,
                            layout = GlobalResources.CurrentSlide.getLayoutById(layerEffect);
                        if (layout != null) {
                            var Timeline = layout.Timeline;
                            if (Timeline != null && actionData != null) {
                                timelineAction(Timeline, actionData);
                            }
                        }
                    case "ResumeTimeline":
                        var layerEffect = triggerData.S,
                            layout = GlobalResources.CurrentSlide.getLayoutById(layerEffect);
                        if (layout != null) {
                            var Timeline = layout.Timeline;
                            if (Timeline != null && actionData != null) {
                                timelineAction(Timeline, actionData);
                            }
                        }
                        break;
                    case "JumpToURLFile":
                        if (actionData != null) {
                            JumToURLFile(triggerData);
                        }
                        break;
                    case "SendEmailTo":
                        if (actionData != null) {
                            SendToEmail(triggerData);
                        }
                        break;
                    case "ExcuteJS":
                        layerEffect = triggerData.S;
                        var scr = document.createElement('script');
                        scr.innerHTML = triggerData.S;
                        //console.log(document.getElementsByTagName('body'));
                        document.getElementsByTagName('body')[0].appendChild(scr);
                        break;
                        /*==== END Group More Action ====*/

                        /*==== BEGIN Group Quiz Action ====*/
                    case "SubmitInteraction":
                        layerEffect = triggerData.S;
                        QuizzModule.SubmitInteraction(layerEffect);
                        break;
                    case "SubmitResults":
                        layerEffect = triggerData.S;
                        QuizzModule.SubmitResults(layerEffect);
                        break;
                    case "ReviewResults":
                        layerEffect = triggerData.S;
                        var naviC = triggerData.sD.nC;
                        QuizzModule.ReviewResults(layerEffect, naviC);
                        break;
                    case "ResetResults":
                        layerEffect = triggerData.S;
                        QuizzModule.ResetResults(layerEffect);
                        break;
                    case "PrintResults":
                        layerEffect = triggerData.S;
                        QuizzModule.PrintResults(layerEffect);
                        break;
                        /*==== END Group Quiz Action ====*/
                    default:
                        break;
                }
            }
        }
    }

    //Lightbox Slide Action Handling
    function insertLightboxSlide(idSlide, triggerData) {
        var divModalWrapper = document.createElement('div');
        var divModal = document.createElement('div');
        var divNaviga = document.createElement('div');
        var btnPrev = document.createElement("button");
        var btnNext = document.createElement("button");
        var span = document.createElement('span');
        var divData = document.getElementById('content');
        var divContainer = document.createElement('div');
        divData.parentElement.removeChild(divData);
        divContainer.id = 'content';
        var divContent = null;
        var slide;
        //var index = 0;
        var l = DOC.slds.length;
        for (var i = 0; i < l; i++) {
            if (DOC.slds[i].id == idSlide) {
                slide = new Slide(DOC.slds[i]);
                GlobalResources.CurrentSlide = slide;
                divContent = slide.generateHTML();
                break;
            }
        }
        var divClone = document.getElementById('modal-wrapper');
        if (divClone != null && divClone.parentElement != null) {
            divClone.parentElement.removeChild(divClone);
        }
        divModalWrapper.id = "modal-wrapper";
        divModal.id = 'myModal';
        divModal.className = 'modal';
        divContainer.className = "modal-content";
        span.className = 'close';
        span.innerHTML = "&times;";
        divNaviga.className = "navigation-control";
        btnPrev.className = "btn prev";
        btnPrev.innerHTML = "Prev";
        btnNext.className = "btn next";
        btnNext.innerHTML = "Next";
        divModalWrapper.appendChild(divModal);
        divContainer.appendChild(divContent);
        divModal.appendChild(divContainer);
        //divContent.innerHTML = divData.outerHTML;
        divModal.appendChild(span);
        divModal.appendChild(divNaviga);
        divNaviga.appendChild(btnPrev);
        divNaviga.appendChild(btnNext);
        document.getElementById('mainContent').appendChild(divModalWrapper);
        CallEvent();
        slide.load();
        slide.setTriggers();
        if (triggerData.sD.nC == true) {
            $(".navigation-control").css("display", "flex");
        }

        btnPrev.onclick = function() {
            GlobalHelper.previousSlide();
        }

        btnNext.onclick = function() {
            GlobalHelper.nextSlide();
        }

        span.onclick = function() {
            let divClone = document.getElementById('modal-wrapper');
            if (divClone != null && divClone.parentElement != null) {
                divClone.parentElement.removeChild(divClone);
            }
            var slide = new Slide(DOC.slds[GlobalHelper.DataLocal.CurrentSlide]);
            GlobalResources.CurrentSlide = slide;
            var divSlide = slide.generateHTML();
            var divContainer = document.getElementById('container');
            var divContent = document.createElement('div');
            var cap = document.getElementById('caption');
            divContent.id = 'content';
            divContent.appendChild(divSlide);
            divContainer.appendChild(divContent);
            Swape(divContent, cap);
            CallEvent();
            slide.setTriggers();
            slide.load();
        }
    }

    //function Close Lightbox Action
    function closeLightBox() {
        let divClone = document.getElementById('modal-wrapper');
        if (divClone != null || divClone.parentElement != null) {
            divClone.parentElement.removeChild(divClone);
        }
        var slide = new Slide(DOC.slds[GlobalHelper.DataLocal.CurrentSlide]);
        GlobalResources.CurrentSlide = slide;
        var divSlide = slide.generateHTML();
        var divContainer = document.getElementById('container');
        var divContent = document.createElement('div');
        var cap = document.getElementById('caption');
        divContent.id = 'content';
        divContent.appendChild(divSlide);
        divContainer.appendChild(divContent);
        Swape(divContent, cap);
        CallEvent();
        slide.setTriggers();
        slide.load();
    }

    // BEGIN Action Move Handling
    function moveAction(trigger) {
        if (trigger != null && trigger.S != null) {
            let elementData = findElementData(trigger.S);
            if (elementData != null) {
                //AnimationModule.setAnimations(elementData, null);
                //return;
                let animation = null;
                for (var i = 0; i < elementData.a.length; i++) {
                    if (elementData.a[i].typ == "MotionPath" && elementData.a[i].id == trigger.sD.oI) {
                        animation = elementData.a[i];
                        break;
                    }
                }
                var tl = new TimelineMax();
                if (animation != null) {
                    MotionPathAnimation(animation, elementData, tl);
                }
            }
        }
    }

    //Action Group Media Handling
    function PlayPauseStopVideo(idVideo, state) {
        var videos = $(idVideo);
        videos.currentTime = 0;
        if (videos.length > 0) {
            switch (state) {
                case "PlayMedia":
                    for (var i = 0; i < videos.length; i++) {
                        videos[i].play();
                    }
                    break;
                case "PauseMedia":
                    for (var i = 0; i < videos.length; i++) {
                        videos[i].pause();
                    }
                    break;
                case "StopMedia":
                    for (var i = 0; i < videos.length; i++) {
                        videos[i].currentTime = 0;
                        videos[i].pause();
                    }
                    break;
            }

        }
    }

    // Reload tab brower
    // function reloadCurrentTab() {
    //     var conf = confirm("Are you sure, you want to reload this tab?");
    //     if (conf == true) {
    //         location.reload('_self');
    //     }
    // }

    // close tab brower
    // function closeCurrentTab(cmd) {
    //     var conf = confirm("Are you sure, you want to close this tab?\n\NOTE: This command can not be used in brower firefox");
    //     if (conf == true) {
    //         close();
    //     }
    // }

    //Timeline Action Handling
    function timelineAction(tline, state) {
        if (state === "PauseTimeline") {
            tline.pause();
            PlayerControl.pauseIcon();
            mediaControl("pause");
        } else if (state === "ResumeTimeline") {
            tline.resume();
            PlayerControl.playIcon();
            mediaControl("play");

        }
    }

    function mediaControl(stage) {
        var videos = $("video");
        var audios = $("audio");
        if (videos.length > 0) {
            switch (stage) {
                case "pause":
                    for (var i = 0; i < videos.length; i++) {
                        videos[i].pause();
                    }
                    if (audios.length > 0) {
                        for (var i = 0; i < audios.length; i++) {
                            audios[i].pause();
                        }
                    }
                    break;
                case "play":
                    for (var i = 0; i < videos.length; i++) {
                        videos[i].play();
                    }
                    if (audios.length > 0) {

                        for (var i = 0; i < audios.length; i++) {
                            audios[i].play();
                        }
                    }
                    break;
                case "stop":
                    for (var i = 0; i < videos.length; i++) {
                        videos[i].currentTime = 0;
                        videos[i].pause();
                    }
                    if (audios.length > 0) {
                        for (var i = 0; i < audios.length; i++) {
                            videos[i].currentTime = 0;
                            audios[i].pause();
                        }
                    }
                    break;
            }

        }
    }

    // Action JumToURL File Handling
    function JumToURLFile(trigger) {
        if (trigger.sD.bW.iCb) {
            window.open(trigger.S, "_self");
        } else {
            if (trigger.sD.bW.iN) {
                if (trigger.sD.bW.wS == "Default") {
                    window.open(trigger.S, "_blank");
                } else if (trigger.sD.bW.wS == "FullScreen") {
                    window.open(trigger.S, "_blank", "width=100% height=100%");
                } else if (trigger.sD.bW.wS == "Custom") {
                    var widthHeight = "width=" + trigger.sD.bW.w + " height=" + trigger.sD.bW.h;
                    popitup(trigger.S, '123', widthHeight);
                }
            } else {
                window.open(trigger.S, "_self");
            }
        }
    }

    // Action SendToEmail Handling
    function SendToEmail(trigger) {
        let nameMail = trigger.S;
        let mailtoLink = 'mailto:' + nameMail + '?subject=subject&body=body';
        window.open(mailtoLink, "_self");
    }

    //Action AjustVariable Handling (Thay đổi giá trị cho biến)
    function ajustVariable(triggerData) {
        //debugger;
        if (triggerData != null && triggerData.sD != null && triggerData.sD.vDm != null) {
            let _sourceVariable = getVariableExits(triggerData.S, DOC); //Tìm biến cần thay đổi giá trị
            if (_sourceVariable != null) {
                let _value = null,
                    _oldValue = _sourceVariable.val;
                if (triggerData.sD.vDm.vT == "Value") //Tìm giá trị cần thay đổi
                {
                    _value = triggerData.sD.vDm.val;
                } else if (triggerData.sD.vDm.vT == "Variable") {
                    _value = getVariablevalueByID(triggerData.sD.vDm.val, DOC);
                }
                switch (triggerData.sD.vDm.O) {
                    case "Add":
                        _sourceVariable.val = Number(_sourceVariable.val) + Number(_value);
                        break;
                    case "Subtract":
                        _sourceVariable.val = Number(_sourceVariable.val) - Number(_value);
                        break;
                    case "Multiply":
                        _sourceVariable.val = Number(_sourceVariable.val) * Number(_value);
                        break;
                    case "Divide":
                        _sourceVariable.val = Number(_sourceVariable.val) / Number(_value);
                        break;
                    case "Assignment":
                        _sourceVariable.val = (_value);
                        break;
                    case "NotAssignment":
                        if (typeof _sourceVariable.val == "boolean") {
                            _sourceVariable.val = !_sourceVariable.val;
                        } else {
                            _sourceVariable.val = !(!_sourceVariable.val);
                        }
                        break;
                }

                if (_sourceVariable.val != _oldValue) {
                    onVariableChanged(_sourceVariable); //Gọi hàm variable cho event variableChange
                }
            }
        }
    }

})();

//Tìm dữ liệu của phần tử với ID truyền vào
function findElementData(elementID) {
    if (GlobalResources.CurrentSlide != null) {
        return GlobalResources.CurrentSlide.getElementById(elementID).Data;
    }
    return null;
}

//Tìm đường padth cho action move
function MotionPathAnimation(animationData, elementData, tl) {
    let id = $("#" + elementData.id);
    let angle = Math.PI * animationData.agl / 180;
    let cos = Math.cos(angle);
    let sin = Math.sin(angle);
    var strs = id[0].style.transform.split(')');

    let deltaLeft = elementData.l - Number(animationData.r.split(',')[0]);
    let deltaTop = elementData.T - Number(animationData.r.split(',')[1]);
    let points = MorphSVGPlugin.pathDataToBezier(animationData.dt, { matrix: [cos, sin, -sin, cos, -deltaLeft, -deltaTop], centerX: Number(animationData.r.split(',')[2]) / 2, centerY: Number(animationData.r.split(',')[3]) / 2 });

    var completeMotionPath = function() {
        onCompleMotionPath(animationData, elementData);
    }
    let value = { bezier: { type: "cubic", autoRotate: animationData.agl != 0 }, /*function(){
                        onCompleMotionPath(animationData, elementData); }*/onComplete: completeMotionPath  }

    for (var i = 0; i < animationData.eO.length; i++) {
        var aniData = animationData.eO[i];
        if (aniData.grNm == "Easing") {
            switch (aniData.nm) {
                case "Slow":
                    value.ease = Power2.easeIn;
                    break;
                case "Medium":
                    value.ease = Power3.easeInOut;
                    break;
                case "Fast":
                    value.ease = Power4.easeInOut;
                    break;
                case "VeryFast":
                    value.ease = Power2.easeInOut;
                    break;
                case "Bounce":
                    value.ease = Bounce.easeInOut;
                    break;
            }
        } else if (aniData.grNm == "Path") {
            switch (aniData.nm) {
                case "Relative Start Point": //Tinh toán lại điểm
                    var lf = 0,
                        tp = 0;
                    var style = window.getComputedStyle(id[0]);
                    var matrix = new WebKitCSSMatrix(style.webkitTransform);
                    lf = matrix.e;
                    tp = matrix.f;
                    for (var i = 0; i < points.length; i++) {
                        points[i].x += lf;
                        points[i].y += tp;
                    }
                    break;
            }
        }
    }

    value.bezier.values = points; //Cài đặt điểm cho path
    let ani = TweenMax.to($("#" + elementData.id), animationData.dA, value);
    TweenLite.set($("#" + elementData.id), { xPercent: -50, yPercent: -50 });
    if (tl != null) {
        tl.add(ani, 0);
    }
}

//Hàm Khi đã có tabBrowser thì khi click lại đối tượng có Event sẽ ko bị load thêm 1 tabBrowser mới
function popitup(url, nm, string) {
    var newwindows = {};
    if ((newwindows[nm] == null) || (newwindows[nm].closed)) {
        newwindows[nm] = window.open(url, nm, 'width=1200,height=650,scrollbars=yes,resizable=yes');
    }
    newwindows[nm].focus();
}

/*==== BEGIN Condition Handling====*/
function checkConditions(conditions, documentData) {
    if (conditions == null || conditions.length == 0) return true;

    let _result = false;
    for (var i = conditions.length - 1; i >= 0; i--) {
        let _checkFlag = checkCondition(conditions[i], documentData);
        if (!_checkFlag && conditions[i].Lo == "And") return false;
        _result = _result || _checkFlag;
    }
    return _result;
}

function checkCondition(condition, documentData) {
    if (condition == null) return true;
    switch (condition.typ) {
        case "Variable":
            return checkVariableCondition(condition.vC, documentData);
        case "Shape":
            break;
        case "Window":
            break;
    }
    return true;
}

function checkVariableCondition(variableCondition, documentData) {
    let _sourceVariable = getVariablevalueByID(variableCondition.V, documentData);
    if (_sourceVariable != null) //Nếu tìm được giá trị
    {
        let _targetVariable = null;
        if (variableCondition.vT == "Value") //Tìm giá trị
        {
            _targetVariable = variableCondition.val;
        } else if (variableCondition.vT == "Variable") {
            _targetVariable = getVariablevalueByID(variableCondition.val, documentData);
        }

        if (_targetVariable != null) {
            switch (variableCondition.O) {
                case "Equal":
                    if (_sourceVariable.toString().toUpperCase() == _targetVariable.toString().toUpperCase()) {
                        return true;
                    }
                    return false;
                case "NotEqual":
                    if (_sourceVariable.toString().toUpperCase() != _targetVariable.toString().toUpperCase()) {
                        return true;
                    }
                    return false;
                case "EqualCase":
                    if (_sourceVariable == _targetVariable) {
                        return true;
                    }
                    return false;
                case "NotEqualCase":
                    if (_sourceVariable != _targetVariable) {
                        return true;
                    }
                    return false;
                case "LessThan":
                    if (_sourceVariable < _targetVariable) {
                        return true;
                    }
                    return false;
                case "GreaterThan":
                    if (_sourceVariable > _targetVariable) {
                        return true;
                    }
                    return false;
                case "LessThanEqual":
                    if (_sourceVariable <= _targetVariable) {
                        return true;
                    }
                    return false;
                case "GreaterThanEqual":
                    if (_sourceVariable >= _targetVariable) {
                        return true;
                    }
                    return false;
            }
        }
    }
    return true;
}

//Hàm kiểm tra tồn tại của biến trong dữ liệu
function getVariableExits(varID, documentData) {
    for (var i = 0; i < documentData.vars.length; i++) {
        if (documentData.vars[i].id == varID) return documentData.vars[i];
    }
    return null;
}

//Tìm giá trị của biến
function getVariablevalueByID(id, documentData) {
    for (var i = documentData.vars.length - 1; i >= 0; i--) {
        if (documentData.vars[i].id == id) {
            return documentData.vars[i].val;
        }
    }
    return null;
}
/*==== END Condition Handling====*/

//Ham goi su kien
function onVariableChanged(variable) {
    let evt = $.Event('variableValueChanged');
    evt.variable = variable;
    $(window).trigger(evt);
}

//Custom event variableChange
$(window).on('variableValueChanged', function(e) {
    //console.log('Gia tri cua bien thay doi', e.variable);

    var triggerData = [];
    var lts = 0;
    var obj = 0;
    lts = GlobalResources.CurrentSlide.Data.lts;
    for (var i = 0; i < lts.length; i++) {
        var elementData = GlobalResources.CurrentSlide.Data.lts[i].tri;
        for (var j = 0; j < elementData.length; j++) {
            if (GlobalResources.CurrentSlide.Data.lts[i].tri[j] != null) {
                if (GlobalResources.CurrentSlide.Data.lts[i].tri[j].tD != null) {
                    triggerData.push(GlobalResources.CurrentSlide.Data.lts[i].tri[j]);
                }
            }
        }
    }
    if (triggerData.length > 0) {
        // obj = triggerData.filter(x => x.tD.oI == e.variable.id);
        obj = triggerData.filter(function(x){
            if(x.tD.oI==e.variable.id)
            {
                return x;
            }
        });
        if (obj.length > 0) {
            TriggerActionModule.getTriggerAction(obj[0]);
        }
    }
});
