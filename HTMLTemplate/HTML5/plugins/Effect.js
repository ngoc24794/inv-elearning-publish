'use strict';
var EffectModule = (function () {
    return {
        genGEffect: function (data, svg, angleObject) {
            return addEffect(data, svg, angleObject);
        }
    };
    function addEffect(data, svg, angleObject) {
        switch (data.typ) {
            case "Shadow":
                var shadowEffect = new Shadow(data, svg, angleObject);
                if (data.shT === "Inner") {
                    return { g: shadowEffect.g, use: shadowEffect.use };
                }
                return { g: shadowEffect.g };
            case "Glow":
                var glowEffect = new Glow(data, svg);
                return { g: glowEffect.genGlowEffect() };
            case "Reflection":
                var reflectionEffect = new Reflection(data, svg, angleObject);
                return { g: reflectionEffect.genReflect() };
            case "SoftEdge":
                var softEdge = new SoftEdge(data, svg);
                return { g: softEdge.genSoftEdge() };
        }
    }
})();
var Glow = (function () {
    function Glow(effectData, svg) {
        this.svg = svg;
        this.effectData = effectData;
        if (svg.Id != undefined) {
            this.frame = { "id": svg.Id, "width": svg.width, "height": svg.height };
        }
        else {
            var idFrame = svg.attributes["id"].value.split("_")[0];
            var widthFrame = parseFloat(svg.attributes["width"].value);
            var heightFrame = parseFloat(svg.attributes["height"].value);
            this.frame = { "id": idFrame, "width": widthFrame, "height": heightFrame };
        }
    }
    Glow.prototype.calWidthFilter = function () {
        return this.frame.width + 100;
    };
    Glow.prototype.calHeightFilter = function () {
        return this.frame.height + 100;
    };
    Glow.prototype.calStdDeviation = function () {
        return this.effectData.std / 3;
    };
    Glow.prototype.genGlowEffect = function () {
        var xmlNs = "http://www.w3.org/2000/svg";
        var currentGlow = this.effectData;
        var gGlow = $(this.svg).find('.effects');
        if (gGlow.length == 0) {
            gGlow = GlobalHelper.createTags('g', this.frame.id + "_ef", 'effects');
            var s = document.createElementNS(xmlNs, 'svg');
            s.appendChild(gGlow);
        }
        else {
            gGlow = gGlow[0];
        }
        var defsGlow = $(gGlow).find('defs');
        if (defsGlow.length == 0) {
            defsGlow = document.createElementNS(xmlNs, 'defs');
        }
        var filterGlow = document.createElementNS(xmlNs, "filter");
        var feGaussianBlurGlow = document.createElementNS(xmlNs, "feGaussianBlur");
        var feFloodGlow = document.createElementNS(xmlNs, "feFlood");
        var feCompositeGlow = document.createElementNS(xmlNs, "feComposite");
        var useGlow = document.createElementNS(xmlNs, 'use');
        filterGlow.setAttribute('id', "filterGlow" + this.frame.id);
        filterGlow.setAttribute('width', this.calWidthFilter());
        filterGlow.setAttribute('height', this.calHeightFilter());
        filterGlow.setAttribute('filterUnits', "userSpaceOnUse");
        useGlow.setAttribute("id", "useGlow");
        useGlow.setAttribute("transform", "translate(0, 0) scale(1, 1) translate(0, 0)");
        useGlow.setAttribute("xlink:href", "#" + this.frame.id + "_gp");
        useGlow.setAttribute("filter", "url(#filterGlow" + this.frame.id + ")");
        feGaussianBlurGlow.setAttribute('result', "gaussOut");
        feGaussianBlurGlow.setAttribute('in', "SourceAlpha");
        feGaussianBlurGlow.setAttribute('stdDeviation', this.calStdDeviation());
        feFloodGlow.setAttribute('result', "floodOut");
        feFloodGlow.setAttribute('flood-color', currentGlow.col);
        feFloodGlow.setAttribute('flood-opacity', "1");
        feCompositeGlow.setAttribute('in', "floodOut");
        feCompositeGlow.setAttribute('in2', "gaussOut");
        feCompositeGlow.setAttribute('operator', "in");
        filterGlow.appendChild(feGaussianBlurGlow);
        filterGlow.appendChild(feFloodGlow);
        filterGlow.appendChild(feCompositeGlow);
        defsGlow.appendChild(filterGlow);
        gGlow.appendChild(defsGlow);
        $(gGlow).prepend(useGlow);
        gGlow.innerHTML = gGlow.innerHTML;
        return gGlow;
    };
    return Glow;
}());
var Reflection = (function () {
    function Reflection(effectData, svgObj, angleObject) {
        this.svgObj = svgObj;
        this.effectData = effectData;
        this.angleObject = angleObject;
        if (svgObj.Id != undefined) {
            this.frame = { "id": svgObj.Id, "width": svgObj.width, "height": svgObj.height };
        }
        else {
            var idFrame = svgObj.attributes["id"].value.split("_")[0];
            var widthFrame = parseFloat(svgObj.attributes["width"].value);
            var heightFrame = parseFloat(svgObj.attributes["height"].value);
            this.frame = { "id": idFrame, "width": widthFrame, "height": heightFrame };
        }
    }
    Reflection.prototype.rotateWidth = function () {
        if (this.angleObject <= 90) {
            return this.frame.width;
        }
        else if (90 < this.angleObject && this.angleObject <= 180) {
            return this.frame.width / 1.12 + this.distanceReflect() + 2;
        }
        else {
            return this.frame.width / 5.6 - (this.distanceReflect() + 15);
        }
    };
    Reflection.prototype.rotateHeight = function () {
        if (this.angleObject <= 90 || this.angleObject > 270) {
            return this.frame.height;
        }
        else {
            return 0;
        }
    };
    Reflection.prototype.rotateReflect = function () {
        return (-this.angleObject);
    };
    Reflection.prototype.HeightTotal = function () {
        if (this.angleObject <= 90 || this.angleObject > 270) {
            return this.frame.height * 2;
        }
        else {
            return 0;
        }
    };
    Reflection.prototype.graRotate = function () {
        return 90 - this.angleObject;
    };
    Reflection.prototype.sizeReflect = function () {
        return 100 - this.effectData.sz;
    };
    Reflection.prototype.distanceReflect = function () {
        return this.effectData.dis + 1;
    };
    Reflection.prototype.genReflect = function () {
        var xmlNs = "http://www.w3.org/2000/svg";
        var currentReflect = this.effectData;
        var gReflection = $(this.svgObj).find('.effects');
        if (gReflection.length == 0) {
            gReflection = GlobalHelper.createTags('g', this.frame.id + "_ef", 'effects');
        }
        else {
            gReflection = gReflection[0];
        }
        var defs = $(gReflection).find('defs');
        if (defs.length == 0) {
            defs = document.createElementNS(xmlNs, 'defs');
        }
        var linearGradient = document.createElementNS(xmlNs, 'linearGradient');
        var stop = document.createElementNS(xmlNs, 'stop');
        var stop1 = document.createElementNS(xmlNs, 'stop');
        var stop2 = document.createElementNS(xmlNs, 'stop');
        var mask = document.createElementNS(xmlNs, 'mask');
        var rect = document.createElementNS(xmlNs, 'rect');
        var use = document.createElementNS(xmlNs, 'use');
        linearGradient.setAttribute("gradientTransform", "rotate(" + this.graRotate() + " ,0.5,0.5)");
        linearGradient.setAttribute("id", "LinRef" + this.frame.id);
        stop.setAttribute("offset", "0%");
        stop.setAttribute("stop-color", "#FFFFFF");
        stop.setAttribute("stop-opacity", "0");
        stop1.setAttribute("offset", "" + this.sizeReflect() + "%");
        stop1.setAttribute("stop-color", "#FFFFFF");
        stop1.setAttribute("stop-opacity", "0");
        stop2.setAttribute("offset", "100%");
        stop2.setAttribute("stop-color", "#FFFFFF");
        stop2.setAttribute("stop-opacity", "0.5");
        mask.setAttribute("id", "mask" + this.frame.id);
        mask.setAttribute("maskContentUnits", "objectBoundingBox");
        rect.setAttribute("fill", "url(#LinRef" + this.frame.id + ")");
        rect.setAttribute("width", "1.2");
        rect.setAttribute("height", "1.2");
        rect.setAttribute("x", "-0.1");
        rect.setAttribute("y", "-0.1");
        use.setAttribute("transform", "rotate(" + this.rotateReflect() + ", " + this.rotateWidth() + "," + this.rotateHeight() + ") translate(0, " + this.distanceReflect() + ") rotate(" + this.rotateReflect() + ", " + this.rotateWidth() + "," + this.rotateHeight() + ") translate(0, " + this.HeightTotal() + ") scale(1, -1)");
        use.setAttribute("xlink:href", "#" + this.frame.id + "_sh");
        use.setAttribute("mask", "url(#mask" + this.frame.id + ")");
        defs.appendChild(linearGradient);
        defs.appendChild(mask);
        linearGradient.appendChild(stop);
        linearGradient.appendChild(stop1);
        linearGradient.appendChild(stop2);
        mask.appendChild(rect);
        gReflection.appendChild(defs);
        $(gReflection).prepend(use);
        var s = document.createElementNS(xmlNs, 'svg');
        s.appendChild(gReflection);
        gReflection.innerHTML = gReflection.innerHTML;
        return gReflection;
    };
    return Reflection;
}());
var ShadowOuter = (function () {
    function ShadowOuter(effectData, svgObj) {
        this.effectData = effectData;
        this.svgObj = svgObj;
        if (svgObj.Id != undefined) {
            this.frame = { "id": svgObj.Id, "width": svgObj.width, "height": svgObj.height };
        }
        else {
            var idFrame = svgObj.attributes["id"].value.split("_")[0];
            var widthFrame = parseFloat(svgObj.attributes["width"].value);
            var heightFrame = parseFloat(svgObj.attributes["height"].value);
            this.frame = { "id": idFrame, "width": widthFrame, "height": heightFrame };
        }
    }
    ShadowOuter.prototype.scaleOuter = function () {
        return this.effectData.sz / 100;
    };
    ShadowOuter.prototype.stdDeviationOuter = function () {
        return (this.effectData.bl / 2) * 7 / 20;
    };
    ShadowOuter.prototype.marginTopOuter = function () {
        var angleRadLeftOuter = Math.PI * this.effectData.agl / 180;
        return this.effectData.dis * Math.sin(angleRadLeftOuter);
    };
    ShadowOuter.prototype.marginRightOuter = function () {
        var angleRadRightOuter = Math.PI * this.effectData.agl / 180;
        return this.effectData.dis * Math.cos(angleRadRightOuter);
    };
    ShadowOuter.prototype.calWidthFilter = function () {
        return this.frame.width + 100;
    };
    ShadowOuter.prototype.calHeightFilter = function () {
        return this.frame.height + 100;
    };
    ShadowOuter.prototype.calOpacity = function () {
        return this.effectData.opc;
    };
    ShadowOuter.prototype.genShadowOuter = function () {
        var xmlNs = "http://www.w3.org/2000/svg";
        var currentShadowOuter = this.effectData;
        var gShadowOuter = $(this.svgObj).find('.effects');
        if (gShadowOuter.length == 0) {
            gShadowOuter = GlobalHelper.createTags('g', this.frame.id + "_ef", 'effects');
        }
        else {
            gShadowOuter = gShadowOuter[0];
        }
        var defsOuter = $(gShadowOuter).find('defs');
        if (defsOuter.length == 0) {
            defsOuter = document.createElementNS(xmlNs, 'defs');
        }
        var filterOuter = document.createElementNS(xmlNs, "filter");
        var feGaussianBlurOuter = document.createElementNS(xmlNs, "feGaussianBlur");
        var feFloodOuter = document.createElementNS(xmlNs, "feFlood");
        var feCompositeOuter = document.createElementNS(xmlNs, "feComposite");
        var pathOuter = document.createElementNS(xmlNs, 'path');
        var useOuter = document.createElementNS(xmlNs, 'use');
        var gOuter = document.createElementNS(xmlNs, 'g');
        filterOuter.setAttribute("id", "shadowOuter" + this.frame.id);
        filterOuter.setAttribute("filterUnits", "userSpaceOnUse");
        filterOuter.setAttribute("width", this.calWidthFilter());
        filterOuter.setAttribute("height", this.calHeightFilter());
        feFloodOuter.setAttribute("result", "floodOut");
        feFloodOuter.setAttribute("flood-color", currentShadowOuter.col);
        feFloodOuter.setAttribute("flood-opacity", this.calOpacity());
        feGaussianBlurOuter.setAttribute("result", "gaussOut");
        feGaussianBlurOuter.setAttribute("in", "SourceAlpha");
        feGaussianBlurOuter.setAttribute("stdDeviation", this.stdDeviationOuter() + "," + this.stdDeviationOuter());
        feCompositeOuter.setAttribute("in", "floodOut");
        feCompositeOuter.setAttribute("in2", "gaussOut");
        feCompositeOuter.setAttribute("operator", "in");
        useOuter.setAttribute("id", "useOuter");
        useOuter.setAttribute("transform", "translate(0, 0) scale(" + this.scaleOuter() + "," + this.scaleOuter() + ") translate(" + this.marginRightOuter() + "," + this.marginTopOuter() + ")");
        useOuter.setAttribute("xlink:href", "#" + this.frame.id + "_gp");
        useOuter.setAttribute("filter", "url(#shadowOuter" + this.frame.id + ")");
        filterOuter.appendChild(feFloodOuter);
        filterOuter.appendChild(feGaussianBlurOuter);
        filterOuter.appendChild(feCompositeOuter);
        defsOuter.appendChild(filterOuter);
        gShadowOuter.appendChild(useOuter);
        gShadowOuter.appendChild(defsOuter);
        var s = document.createElementNS(xmlNs, 'svg');
        s.appendChild(gShadowOuter);
        gShadowOuter.innerHTML = gShadowOuter.innerHTML;
        return gShadowOuter;
    };
    return ShadowOuter;
}());
var ShadowInner = (function () {
    function ShadowInner(effectData, svgObj) {
        this.effectData = effectData;
        this.svgObj = svgObj;
        if (svgObj.Id != undefined) {
            this.frame = { "id": svgObj.Id, "width": svgObj.width, "height": svgObj.height };
        }
        else {
            var idFrame = svgObj.attributes["id"].value.split("_")[0];
            var widthFrame = parseFloat(svgObj.attributes["width"].value);
            var heightFrame = parseFloat(svgObj.attributes["height"].value);
            this.frame = { "id": idFrame, "width": widthFrame, "height": heightFrame };
        }
    }
    ShadowInner.prototype.stdDeviationInner = function () {
        return (this.effectData.bl * 7 / 20) / 2;
    };
    ShadowInner.prototype.marginTopInner = function () {
        var angleRadLeftInner = Math.PI * this.effectData.agl / 180;
        return -(this.effectData.dis) * Math.sin(angleRadLeftInner);
    };
    ShadowInner.prototype.marginRightInner = function () {
        var angleRadRightInner = Math.PI * this.effectData.agl / 180;
        return -(this.effectData.dis) * Math.cos(angleRadRightInner);
    };
    ShadowInner.prototype.calWidthFilter = function () {
        return this.frame.width + 100;
    };
    ShadowInner.prototype.calHeightFilter = function () {
        return this.frame.height + 100;
    };
    ShadowInner.prototype.genShadowInner = function () {
        var xmlNs = "http://www.w3.org/2000/svg";
        var currentShadowInner = this.effectData;
        var gShadowInner = $(this.svgObj).find('.effects');
        if (gShadowInner.length == 0) {
            gShadowInner = GlobalHelper.createTags('g', this.frame.id + "_ef", 'effects');
        }
        else {
            gShadowInner = gShadowInner[0];
        }
        var defsInner = $(gShadowInner).find('defs');
        if (defsInner.length == 0) {
            defsInner = document.createElementNS(xmlNs, 'defs');
        }
        var filterInner = document.createElementNS(xmlNs, "filter");
        var feGaussianBlurInner = document.createElementNS(xmlNs, "feGaussianBlur");
        var feFloodInner = document.createElementNS(xmlNs, "feFlood");
        var feCompositeInner = document.createElementNS(xmlNs, "feComposite");
        var feCompositeInner1 = document.createElementNS(xmlNs, "feComposite");
        var feCompositeInner2 = document.createElementNS(xmlNs, "feComposite");
        var feOffsetInner = document.createElementNS(xmlNs, "feOffset");
        var gInner = document.createElementNS(xmlNs, 'g');
        var gInner1 = document.createElementNS(xmlNs, 'g');
        var pathInner1 = document.createElementNS(xmlNs, 'path');
        filterInner.setAttribute("id", "shadowInner" + this.frame.id);
        filterInner.setAttribute("filterUnits", "userSpaceOnUse");
        filterInner.setAttribute("width", this.calWidthFilter());
        filterInner.setAttribute("height", this.calHeightFilter());
        feFloodInner.setAttribute("result", "floodOut");
        feFloodInner.setAttribute("flood-color", currentShadowInner.col);
        feFloodInner.setAttribute("flood-opacity", currentShadowInner.opc);
        feOffsetInner.setAttribute("dx", this.marginRightInner());
        feOffsetInner.setAttribute("dy", this.marginTopInner());
        feGaussianBlurInner.setAttribute("stdDeviation", this.stdDeviationInner() + "," + this.stdDeviationInner());
        feCompositeInner.setAttribute("in2", "SourceAlpha");
        feCompositeInner.setAttribute("k2", "-1");
        feCompositeInner.setAttribute("k3", "1");
        feCompositeInner.setAttribute("operator", "arithmetic");
        feCompositeInner.setAttribute("result", "shadowDiff");
        feCompositeInner1.setAttribute("operator", "in");
        feCompositeInner1.setAttribute("in2", "shadowDiff");
        feCompositeInner2.setAttribute("operator", "over");
        feCompositeInner2.setAttribute("in2", "SourceGraphic");
        filterInner.appendChild(feGaussianBlurInner);
        filterInner.appendChild(feOffsetInner);
        filterInner.appendChild(feCompositeInner);
        filterInner.appendChild(feFloodInner);
        filterInner.appendChild(feCompositeInner1);
        filterInner.appendChild(feCompositeInner2);
        defsInner.appendChild(filterInner);
        gInner1.appendChild(pathInner1);
        gShadowInner.appendChild(defsInner);
        return gShadowInner;
    };
    ShadowInner.prototype.useHTML = function () {
        var xmlNs = "http://www.w3.org/2000/svg";
        var useInner = document.createElementNS(xmlNs, 'use');
        useInner.setAttribute("id", "useInner");
        useInner.setAttribute("xlink:href", "#" + this.frame.id + "_gp");
        useInner.setAttribute("filter", "url(#shadowInner" + this.frame.id + ")");
        return useInner;
    };
    return ShadowInner;
}());
var ShadowPerspective = (function () {
    function ShadowPerspective(effectData, svgObj, angleObject) {
        this.effectData = effectData;
        this.angleObject = angleObject;
        this.svgObj = svgObj;
        if (svgObj.Id != undefined) {
            this.frame = { "id": svgObj.Id, "width": svgObj.width, "height": svgObj.height };
        }
        else {
            var idFrame = svgObj.attributes["id"].value.split("_")[0];
            var widthFrame = parseFloat(svgObj.attributes["width"].value);
            var heightFrame = parseFloat(svgObj.attributes["height"].value);
            this.frame = { "id": idFrame, "width": widthFrame, "height": heightFrame };
        }
    }
    ShadowPerspective.prototype.scalePerspective = function () {
        return this.effectData.bl / 6;
    };
    ShadowPerspective.prototype.Rotate = function () {
        return -this.angleObject;
    };
    ShadowPerspective.prototype.RotateBehind = function () {
        return this.angleObject;
    };
    ShadowPerspective.prototype.RotateWidth = function () {
        if(this.angleObject <= 180){
            return this.frame.width;
        }
        else {
            return 0;
        }
    };
    ShadowPerspective.prototype.RotateHeight = function () {
        if(this.angleObject <= 90 || this.angleObject >= 270){
            return this.frame.height;
        }
        else{
            return this.frame.height / 2 + 4;
        }
    };
    ShadowPerspective.prototype.ScaleX = function () {
        return this.effectData.sz / 100;
    };
    ShadowPerspective.prototype.ScaleY = function () {
        if (this.effectData.peT === "LowerRight" || this.effectData.peT === "LowerLeft" || this.effectData.peT === "Below") {
            return -this.ScaleX() * 0.28;
        }
        else {
            return this.ScaleX() * 0.28;
        }
    };
    ShadowPerspective.prototype.TranslateHeight = function () {
        if (this.effectData.peT === "LowerLeft" || this.effectData.peT === "Below" || this.effectData.peT === "LowerRight") {
            return this.frame.height * 0.8372 - 1 + 2 * (this.frame.height * 0.8372 * (-this.ScaleY()));
        }
        else {
            return this.frame.height * (1 - this.ScaleY());
        }
    };
    ShadowPerspective.prototype.marginTopPerspective = function () {
        var angleRadLeftPerspective = Math.PI * this.effectData.agl / 180;
        return this.effectData.dis * Math.sin(angleRadLeftPerspective);
    };
    ShadowPerspective.prototype.marginRightPerspective = function () {
        var angleRadRightPerspective = Math.PI * this.effectData.agl / 180;
        return this.effectData.dis * Math.cos(angleRadRightPerspective);
    };
    ShadowPerspective.prototype.calWidthFilter = function () {
        return this.frame.width + 100;
    };
    ShadowPerspective.prototype.calHeightFilter = function () {
        return this.frame.height + 100;
    };
    ShadowPerspective.prototype.calOpacity = function () {
        return this.effectData.opc;
    };
    ShadowPerspective.prototype.calSkew = function () {
        if (this.effectData.peT === "UpperLeft" || this.effectData.peT === "LowerLeft") {
            return 14.63;
        }
        else if (this.effectData.peT === "LowerRight" || this.effectData.peT === "UpperRight") {
            return -14.63;
        }
        else {
            return 0;
        }
    };
    ShadowPerspective.prototype.callTranslateX = function () {
        var heightScale = this.frame.height * this.ScaleY();
        var skew = Math.abs(this.calSkew());
        var heightSkew = heightScale + 2 * (heightScale * (skew / 100));
        var rectBoundHeight = Math.tan(skew) * heightSkew;
        if (this.effectData.peT === "UpperLeft") {
            return rectBoundHeight / 3 - 6;
        }
        else if (this.effectData.peT === "UpperRight") {
            return -rectBoundHeight / 3 + 6;
        }
        else if (this.effectData.peT === "Below") {
            return 0;
        }
        else if (this.effectData.peT === "LowerLeft") {
            var translateX = void 0;
            var angleRad = Math.PI * (89 + this.calSkew()) / 180;
            translateX = (this.frame.height - 3) / Math.tan(angleRad);
            return translateX;
        }
        else if (this.effectData.peT === "LowerRight") {
            var translateX = void 0;
            var angleRad = Math.PI * (91 + this.calSkew()) / 180;
            translateX = this.frame.height / Math.tan(angleRad);
            return translateX;
        }
    };
    ShadowPerspective.prototype.genShadowPerspective = function () {
        var xmlNs = "http://www.w3.org/2000/svg";
        var currentShadowPerspective = this.effectData;
        var gShadowPerspective = $(this.svgObj).find('.effects');
        if (gShadowPerspective.length == 0) {
            gShadowPerspective = GlobalHelper.createTags('g', this.frame.id + "_ef", 'effects');
        }
        else {
            gShadowPerspective = gShadowPerspective[0];
        }
        var defsPerspective = $(gShadowPerspective).find('defs');
        if (defsPerspective.length == 0) {
            defsPerspective = document.createElementNS(xmlNs, 'defs');
        }
        var filterPerspective = document.createElementNS(xmlNs, "filter");
        var feGaussianBlurPerspective = document.createElementNS(xmlNs, "feGaussianBlur");
        var feFloodPerspective = document.createElementNS(xmlNs, "feFlood");
        var feCompositePerspective = document.createElementNS(xmlNs, "feComposite");
        var usePerspective = document.createElementNS(xmlNs, 'use');
        var gPerspective = document.createElementNS(xmlNs, 'g');
        filterPerspective.setAttribute("id", "filterPerspective" + this.frame.id);
        filterPerspective.setAttribute("filterUnits", "userSpaceOnUse");
        filterPerspective.setAttribute("width", this.calWidthFilter());
        filterPerspective.setAttribute("height", this.calHeightFilter());
        feFloodPerspective.setAttribute("result", "floodOut");
        feFloodPerspective.setAttribute("flood-color", currentShadowPerspective.col);
        feFloodPerspective.setAttribute("flood-opacity", this.calOpacity());
        feGaussianBlurPerspective.setAttribute("result", "gaussOut");
        feGaussianBlurPerspective.setAttribute("in", "SourceAlpha");
        feGaussianBlurPerspective.setAttribute("stdDeviation", this.scalePerspective() + "," + this.scalePerspective());
        feCompositePerspective.setAttribute("in", "floodOut");
        feCompositePerspective.setAttribute("in2", "gaussOut");
        feCompositePerspective.setAttribute("operator", "in");
        usePerspective.setAttribute("id", "usePerspective");
        usePerspective.setAttribute("transform", "rotate(" + this.Rotate() + ", " + this.RotateWidth() + ", " + this.RotateHeight() + ") translate(0, -1) translate(" + this.marginRightPerspective() + "," + this.marginTopPerspective() + ") translate(0, " + this.TranslateHeight() + ") scale(" + this.ScaleX() + ", " + this.ScaleY() + ") skewX(" + this.calSkew() + ") translate(" + this.callTranslateX() + ", 0) skewY(0) translate(0, 0) rotate(" + this.RotateBehind() + ", " + this.RotateWidth() + ", " + this.RotateHeight() + ")");
        usePerspective.setAttribute("xlink:href", "#" + this.frame.id + "_sh");
        usePerspective.setAttribute("filter", "url(#filterPerspective" + this.frame.id + ")");
        filterPerspective.appendChild(feFloodPerspective);
        filterPerspective.appendChild(feGaussianBlurPerspective);
        filterPerspective.appendChild(feCompositePerspective);
        defsPerspective.appendChild(filterPerspective);
        gShadowPerspective.appendChild(defsPerspective);
        $(gShadowPerspective).prepend(usePerspective);
        gShadowPerspective.innerHTML = gShadowPerspective.innerHTML;
        return gShadowPerspective;
    };
    return ShadowPerspective;
}());
var SoftEdge = (function () {
    function SoftEdge(effectData, svgObj) {
        this.effectData = effectData;
        this.svgObj = svgObj;
        if (svgObj.Id != undefined) {
            this.frame = { "id": svgObj.Id, "width": svgObj.width, "height": svgObj.height };
        }
        else {
            var idFrame = svgObj.attributes["id"].value.split("_")[0];
            var widthFrame = parseFloat(svgObj.attributes["width"].value);
            var heightFrame = parseFloat(svgObj.attributes["height"].value);
            this.frame = { "id": idFrame, "width": widthFrame, "height": heightFrame };
        }
    }
    SoftEdge.prototype.genSoftEdge = function () {
        var xmls = "http://www.w3.org/2000/svg";
        var currentSoftEdges = this.effectData;
        var gSoftEdge = $(this.svgObj).find('.effects');
        if (gSoftEdge.length == 0) {
            gSoftEdge = GlobalHelper.createTags('g', this.frame.id + "_ef", 'effects');
        }
        else {
            gSoftEdge = gSoftEdge[0];
        }
        var image = document.createElementNS(xmls, 'image');
        image.setAttribute("id", this.frame.id + "_sh");
        image.setAttribute("preserveAspectRatio", "none");
        image.setAttribute("xlink:href", currentSoftEdges.sU);
        image.setAttribute("width", this.frame.width);
        image.setAttribute("height", this.frame.height);
        gSoftEdge.appendChild(image);
        gSoftEdge.innerHTML = gSoftEdge.innerHTML;
        return gSoftEdge;
    };
    return SoftEdge;
}());
var Shadow = (function () {
    function Shadow(effectData, svg, angleObject) {
        this.effectData = effectData;
        this.svg = svg;
        this.angleObject = angleObject;
        this.shadowType = effectData.ShadowType;
        return this.accordingType();
    }
    Shadow.prototype.accordingType = function () {
        var currentShadow = this.effectData;
        switch (currentShadow.shT) {
            case "Outer":
                var shadowOuter = new ShadowOuter(this.effectData, this.svg);
                return { g: shadowOuter.genShadowOuter() };
            case "Inner":
                var shadowInner = new ShadowInner(this.effectData, this.svg);
                return { g: shadowInner.genShadowInner(), use: shadowInner.useHTML() };
            case "Perspective":
                var shadowPerspective = new ShadowPerspective(this.effectData, this.svg, this.angleObject);
                return { g: shadowPerspective.genShadowPerspective() };
            default: return;
        }
    };
    return Shadow;
}());
