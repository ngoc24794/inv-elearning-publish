"use strict";
var AnimationModule = (function() {
    return {
        setAnimations: function(elementData, timeline /*, fromTrigger*/ ) {
            if (elementData.a != null && elementData.a.length > 0) {
                for (var i = 0; i < elementData.a.length; i++) {
                    let ani = elementData.a[i];
                    this.getAnimation(elementData.a[i], elementData, timeline, function() {
                        onCompleteAnimate(ani, elementData);
                    } );
                }
            }
        },
        getAnimation: function(animationData, elementData, timeline, onComplete) {
            switch (animationData.nm) {
                case AnimationName.Fade:
                    getFadeAnimation(animationData, elementData, timeline, onComplete);
                    break;
                case AnimationName.Grow:
                    getGrowAnimation(animationData, elementData, timeline, onComplete);
                    break;
                case AnimationName.Fly:
                    getFlyAnimation(animationData, elementData, timeline, onComplete);
                    break;
                case AnimationName.Float:
                    getFloatAnimation(animationData, elementData, timeline, onComplete);
                    break;
                case AnimationName.Split:
                    getSplitAnimation(animationData, elementData, timeline, onComplete);
                    break;
                case AnimationName.Wipe:
                    getWipeAnimation(animationData, elementData, timeline, onComplete)
                    break;
                case AnimationName.Shape:
                    for (var i = 0; i < animationData.eO.length; i++) {
                        let group = animationData.eO[i];
                        if (group.grNm == AnimationEffect.Shapes) {
                            switch (group.nm) {
                                case EffectOptions.Circle:
                                    getEllipseAnimation(animationData, elementData, timeline);
                                    break;
                                case EffectOptions.Box:
                                    getBoxAnimation(animationData, elementData, timeline);
                                    break;
                                case EffectOptions.Diamond:
                                    getDiamonAnimation(animationData, elementData, timeline);
                                    break;
                                case EffectOptions.Plus:
                                    getPlusAnimation(animationData, elementData, timeline);
                                    break;
                                default:
                                    getEllipseAnimation(animationData, elementData, timeline);
                                    break;
                            }
                        }
                    }
                    break;
                case AnimationName.Wheel:
                    getWheelAnimation(animationData, elementData, timeline, onComplete);
                    break;
                case AnimationName.RandomBars:
                    getRandomBarsAnimation(animationData, elementData, timeline, onComplete);
                    break;
                case AnimationName.Spin:
                    getSpinAnimation(animationData, elementData, timeline, onComplete);
                    break;
                case AnimationName.SpinGrow:
                    getSpinGrowAnimation(animationData, elementData, timeline, onComplete);
                    break;
                case AnimationName.GrowSpin:
                    getGrowSpinAnimation(animationData, elementData, timeline, onComplete);
                    break;
                case AnimationName.Zoom:
                    getZoomAnimation(animationData, elementData, timeline, onComplete);
                    break;
                case AnimationName.Swivel:
                    getSwivelAnimation(animationData, elementData, timeline, onComplete);
                    break;
                case AnimationName.Bounce:
                    getBounceAnimation(animationData, elementData, timeline, onComplete);
                    break;
            }
        }
    }

    /*Hiệu ứng Fade: tác động đến thuộc tính opacity*/
    function getFadeAnimation(animationData, elementData, timeline, onComplete) {
        let fromValue = null,
            toValue = null,
            entrance = null,
            exit = null,
            elementID = "#" + elementData.id;
        switch (animationData.typ) {
            case AnimationAction.Entrance:
                if ($(elementID).find("text") == null) {
                    fromValue = { opacity: 0 };
                    toValue = { opacity: 1, onComplete: onComplete };
                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                    entrance = TweenLite.fromTo(elementID, animationData.dA, fromValue, toValue);
                    if (timeline != null) {
                        let startTime = elementData.tim.sT || 0;
                        timeline.add(entrance, startTime);
                    }

                } else if ($(elementID).find("text") != null) {
                    for (var i = 0; i < animationData.eO.length; i++) {
                        if (animationData.eO[i].grNm == AnimationEffect.Sequence) {
                            switch (animationData.eO[i].nm) {
                                case EffectOptions.AsOneObject:
                                    fromValue = { opacity: 0 };
                                    toValue = { opacity: 1, onComplete: onComplete };
                                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                                    entrance = TweenLite.fromTo(elementID, animationData.dA, fromValue, toValue);
                                    if (timeline != null) {
                                        let startTime = elementData.tim.sT || 0;
                                        timeline.add(entrance, startTime);
                                    }
                                    break;
                                case EffectOptions.ByParagraph:
                                    fromValue = { opacity: 0 };
                                    toValue = { opacity: 1, onCompleteAll: onComplete };
                                    var text = $(elementID).find("text");
                                    AnimationHelpers.setEnterSqeValue(animationData.eO, elementData, fromValue, toValue);
                                    let entraceSta = TweenMax.staggerFromTo(text, animationData.dA / text.length, fromValue, toValue, animationData.dA / text.length /*, reset*/ );
                                    if (timeline != null) {
                                        let startTime = elementData.tim.sT || 0;
                                        timeline.add(entraceSta, startTime);
                                    }
                                    // function reset() {
                                    //     TweenMax.to(text, elementData.tim.dT / text.length, { autoAlpha: 1 });
                                    // }
                                    break;
                            }
                        }
                    }
                }
                break;
            case AnimationAction.Exit:
                if ($(elementID).find("text") == null) {
                    fromValue = { opacity: 0 };
                    toValue = { opacity: 1, onComplete: onComplete };
                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                    exit = TweenLite.fromTo(elementID, animationData.dA, toValue, fromValue);
                    if (timeline != null) {
                        let endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                        timeline.add(exit, endTime);
                    }
                } else if ($(elementID).find("text") != null) {
                    for (var i = 0; i < animationData.eO.length; i++) {
                        if (animationData.eO[i].grNm == AnimationEffect.Sequence) {
                            switch (animationData.eO[i].nm) {
                                case EffectOptions.AsOneObject:
                                    fromValue = { opacity: 0, onComplete: onComplete };
                                    toValue = { opacity: 1 };
                                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                                    exit = TweenLite.fromTo(elementID, animationData.dA, toValue, fromValue);
                                    if (timeline != null) {
                                        let endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                                        timeline.add(exit, endTime);
                                    }
                                    break;
                                case EffectOptions.ByParagraph:
                                    fromValue = { opacity: 0, onCompleteAll: onComplete };
                                    toValue = { opacity: 1 };
                                    var text = $(elementID).find("text");
                                    AnimationHelpers.setEnterSqeValue(animationData.eO, elementData, toValue, fromValue);
                                    let exitSta = TweenMax.staggerFromTo(text, animationData.dA / text.length, toValue, fromValue, animationData.dA / text.length);
                                    if (timeline != null) {
                                        let endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                                        timeline.add(exitSta, endTime);
                                    }
                                    break;
                            }
                        }
                    }
                }
                break;

        }
    }

    /*Hiệu ứng Grow: tác động đến thuộc tính ScaleTransform*/
    function getGrowAnimation(animationData, elementData, timeline, onComplete) {
        let fromValue = null,
            toValue = null,
            entrance = null,
            exit = null,
            elementID = "#" + elementData.id;
        switch (animationData.typ) {
            case AnimationAction.Entrance:
                if ($(elementID).find("text") == null) {
                    fromValue = { scaleX: 0, scaleY: 0 };
                    toValue = { scaleX: 1, scaleY: 1, onComplete: onComplete };
                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                    entrance = TweenLite.fromTo(elementID, animationData.dA, fromValue, toValue);
                    if (timeline != null) {
                        let startTime = elementData.tim.sT || 0;
                        timeline.add(entrance, startTime);
                    }
                } else if ($(elementID).find("text") != null) {
                    for (var i = 0; i < animationData.eO.length; i++) {
                        if (animationData.eO[i].grNm == AnimationEffect.Sequence) {
                            switch (animationData.eO[i].nm) {
                                case EffectOptions.AsOneObject:
                                    fromValue = { scaleX: 0, scaleY: 0 };
                                    toValue = { scaleX: 1, scaleY: 1, onComplete: onComplete };
                                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                                    entrance = TweenLite.fromTo(elementID, animationData.dA, fromValue, toValue);
                                    if (timeline != null) {
                                        let startTime = elementData.tim.sT || 0;
                                        timeline.add(entrance, startTime);
                                    }
                                    break;
                                case EffectOptions.ByParagraph:
                                    fromValue = { scaleX: 0, scaleY: 0, x: elementData.w / 2 };
                                    toValue = { scaleX: 1, scaleY: 1, x: 0, onCompleteAll: onComplete };
                                    var text = $(elementID).find("text");
                                    AnimationHelpers.setEnterSqeValue(animationData.eO, elementData, fromValue, toValue);
                                    let entranceSta = TweenMax.staggerFromTo(text, animationData.dA / text.length, fromValue, toValue, animationData.dA / text.length);
                                    if (timeline != null) {
                                        let startTime = elementData.tim.sT || 0;
                                        timeline.add(entranceSta, startTime);
                                    }
                                    break;
                            }
                        }
                    }
                }
                break;
            case AnimationAction.Exit:
                if ($(elementID).find("text") == null) {
                    fromValue = { scaleX: 1, scaleY: 1 };
                    toValue = { scaleX: 0, scaleY: 0, onComplete: onComplete };
                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                    exit = TweenLite.fromTo(elementID, animationData.dA, toValue, fromValue);
                    if (timeline != null) {
                        let endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                        timeline.add(exit, endTime);
                    }
                } else if ($(elementID).find("text") != null) {
                    for (var i = 0; i < animationData.eO.length; i++) {
                        if (animationData.eO[i].grNm == AnimationEffect.Sequence) {
                            switch (animationData.eO[i].nm) {
                                case EffectOptions.AsOneObject:
                                    fromValue = { scaleX: 1, scaleY: 1 };
                                    toValue = { scaleX: 0, scaleY: 0, onComplete: onComplete };
                                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                                    exit = TweenLite.fromTo(elementID, animationData.dA, toValue, fromValue);
                                    if (timeline != null) {
                                        let endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                                        timeline.add(exit, endTime);
                                    }
                                    break;
                                case EffectOptions.ByParagraph:
                                    fromValue = { scaleX: 0, scaleY: 0, x: elementData.w / 2, onCompleteAll: onComplete };
                                    toValue = { scaleX: 1, scaleY: 1, x: 0 };
                                    var text = $(elementID).find("text");
                                    AnimationHelpers.setEnterSqeValue(animationData.eO, elementData, toValue, fromValue);
                                    let exitSta = TweenMax.staggerFromTo(text, animationData.dA / text.length, toValue, fromValue, animationData.dA / text.length);
                                    if (timeline != null) {
                                        let endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                                        timeline.add(exitSta, endTime);
                                    }
                                    break;
                            }
                        }
                    }
                }
                break;
        }
    }

    //Hiệu ứng Fly: tác động đến thuộc tính Left Right
    function getFlyAnimation(animationData, elementData, timeline, onComplete) {
        let fromValue = null,
            toValue = null,
            entrance = null,
            exit = null,
            elementID = "#" + elementData.id;
        switch (animationData.typ) {
            case AnimationAction.Entrance:
                if ($(elementID).find("text") == null) {
                    fromValue = {};
                    toValue = { onComplete: onComplete };
                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                    fromValue.ease = Power2.easeOut;
                    entrance = TweenLite.fromTo(elementID, animationData.dA, fromValue, toValue);
                    if (timeline != null) {
                        let startTime = elementData.tim.sT || 0;
                        timeline.add(entrance, startTime);
                    }
                } else if ($(elementID).find("text") != null) {
                    for (var i = 0; i < animationData.eO.length; i++) {
                        if (animationData.eO[i].grNm == AnimationEffect.Sequence) {
                            switch (animationData.eO[i].nm) {
                                case EffectOptions.AsOneObject:
                                    fromValue = {};
                                    toValue = { onComplete: onComplete };
                                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                                    fromValue.ease = Power2.easeOut;
                                    entrance = TweenLite.fromTo(elementID, animationData.dA, fromValue, toValue);
                                    if (timeline != null) {
                                        let startTime = elementData.tim.sT || 0;
                                        timeline.add(entrance, startTime);
                                    }
                                    break;
                                case EffectOptions.ByParagraph:
                                    fromValue = { autoAlpha: 0 };
                                    toValue = { autoAlpha: 1, onCompleteAll: onComplete };
                                    fromValue.ease = Power2.easeOut;
                                    var text = $(elementID).find("text");
                                    AnimationHelpers.setEnterSqeValue(animationData.eO, elementData, fromValue, toValue);
                                    let entranceSta = TweenMax.staggerFromTo(text, animationData.dA / text.length, fromValue, toValue, animationData.dA / text.length);
                                    if (timeline != null) {
                                        let startTime = elementData.tim.sT || 0;
                                        timeline.add(entranceSta, startTime);
                                    }
                                    break;
                            }
                        }
                    }
                }
                break;
            case AnimationAction.Exit:
                if ($(elementID).find("text") == null) {
                    fromValue = {};
                    toValue = { onComplete: onComplete };
                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                    toValue.ease = Power2.easeOut;
                    exit = TweenLite.fromTo("#" + elementData.id, animationData.dA, toValue, fromValue);
                    if (timeline != null) {
                        let endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                        timeline.add(exit, endTime);
                    }
                } else if ($(elementID).find("text") != null) {
                    for (var i = 0; i < animationData.eO.length; i++) {
                        if (animationData.eO[i].grNm == AnimationEffect.Sequence) {
                            switch (animationData.eO[i].nm) {
                                case EffectOptions.AsOneObject:
                                    fromValue = { onComplete: onComplete };
                                    toValue = {};
                                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                                    fromValue.ease = Power2.easeOut;
                                    exit = TweenLite.fromTo("#" + elementData.id, animationData.dA, toValue, fromValue);
                                    if (timeline != null) {
                                        let endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                                        timeline.add(exit, endTime);
                                    }
                                    break;
                                case EffectOptions.ByParagraph:
                                    fromValue = { autoAlpha: 1 };
                                    toValue = { autoAlpha: 0, onCompleteAll: onComplete };
                                    fromValue.ease = Power2.easeOut;
                                    var text = $(elementID).find("text");
                                    AnimationHelpers.setEnterSqeValue(animationData.eO, elementData, toValue, fromValue);
                                    let exitSta = TweenMax.staggerFromTo(text, animationData.dA / text.length, fromValue, toValue, animationData.dA / text.length);
                                    if (timeline != null) {
                                        let endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                                        timeline.add(exitSta, endTime);
                                    }
                                    break;
                            }
                        }
                    }
                }
                break;

        }
    }

    //Hiệu ứng Float: tác động đến thuộc tính Opacity và Top
    function getFloatAnimation(animationData, elementData, timeline, onComplete) {
        let fromValue = null,
            toValue = null,
            entrance = null,
            exit = null,
            elementID = "#" + elementData.id;
        switch (animationData.typ) {
            case AnimationAction.Entrance:
                if ($(elementID).find("text") == null) {
                    fromValue = { opacity: 0 };
                    toValue = { opacity: 1, onComplete: onComplete };
                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                    fromValue.ease = Power2.easeOut;
                    entrance = TweenLite.fromTo(elementID, animationData.dA, fromValue, toValue);
                    if (timeline != null) {
                        let startTime = elementData.tim.sT || 0;
                        timeline.add(entrance, startTime);
                    }
                } else if ($(elementID).find("text") != null) {
                    for (var i = 0; i < animationData.eO.length; i++) {
                        if (animationData.eO[i].grNm == AnimationEffect.Sequence) {
                            switch (animationData.eO[i].nm) {
                                case EffectOptions.AsOneObject:
                                    fromValue = { opacity: 0 };
                                    toValue = { opacity: 1, onComplete: onComplete };
                                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                                    fromValue.ease = Power2.easeOut;
                                    entrance = TweenLite.fromTo(elementID, animationData.dA, fromValue, toValue);
                                    if (timeline != null) {
                                        let startTime = elementData.tim.sT || 0;
                                        timeline.add(entrance, startTime);
                                    }
                                    break;
                                case EffectOptions.ByParagraph:
                                    fromValue = { opacity: 0 };
                                    toValue = { opacity: 1, onCompleteAll: onComplete };
                                    var text = $(elementID).find("text");
                                    AnimationHelpers.setEnterSqeValue(animationData.eO, elementData, fromValue, toValue);
                                    let entranceSta = TweenMax.staggerFromTo(text, animationData.dA / text.length, fromValue, toValue, animationData.dA / text.length);
                                    if (timeline != null) {
                                        let startTime = elementData.tim.sT || 0;
                                        timeline.add(entranceSta, startTime);
                                    }
                                    break;
                            }
                        }
                    }
                }
                break;
            case AnimationAction.Exit:
                if ($(elementID).find("text") == null) {
                    fromValue = { opacity: 0, onComplete: onComplete };
                    toValue = { opacity: 1 };
                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, toValue, fromValue);
                    toValue.ease = Power2.easeOut;
                    exit = TweenLite.fromTo(elementID, animationData.dA, toValue, fromValue);
                    if (timeline != null) {
                        let endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                        timeline.add(exit, endTime);
                    }
                } else if ($(elementID).find("text") != null) {
                    for (var i = 0; i < animationData.eO.length; i++) {
                        if (animationData.eO[i].grNm == AnimationEffect.Sequence) {
                            switch (animationData.eO[i].nm) {
                                case EffectOptions.AsOneObject:
                                    fromValue = { opacity: 0 };
                                    toValue = { opacity: 1, onComplete: onComplete };
                                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, toValue, fromValue);
                                    toValue.ease = Power2.easeOut;
                                    exit = TweenLite.fromTo(elementID, animationData.dA, toValue, fromValue);
                                    if (timeline != null) {
                                        let endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                                        timeline.add(exit, endTime);
                                    }
                                    break;
                                case EffectOptions.ByParagraph:
                                    fromValue = { opacity: 0, onCompleteAll: onComplete };
                                    toValue = { opacity: 1 };
                                    toValue.ease = Power2.easeOut;
                                    var text = $(elementID).find("text");
                                    AnimationHelpers.setEnterSqeValue(animationData.eO, elementData, toValue, fromValue);
                                    let exitSta = TweenMax.staggerFromTo(text, animationData.dA / text.length, toValue, fromValue, animationData.dA / text.length);
                                    if (timeline != null) {
                                        let endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                                        timeline.add(exitSta, endTime);
                                    }
                                    break;
                            }
                        }
                    }
                }
                break;
        }
    }

    //Hiệu ứng Spin: tác động đến thuộc tính Rotation
    function getSpinAnimation(animationData, elementData, timeline, onComplete) {
        let fromValue = null,
            toValue = null,
            entrance = null,
            exit = null,
            elementID = "#" + elementData.id;
        switch (animationData.typ) {
            case AnimationAction.Entrance:
                if ($(elementID).find("text") == null) {
                    fromValue = {};
                    toValue = { onComplete: onComplete };
                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                    fromValue.ease = Power2.easeOut;
                    entrance = TweenLite.fromTo(elementID, animationData.dA, fromValue, toValue);
                    if (timeline != null) {
                        let startTime = elementData.tim.sT || 0;
                        timeline.add(entrance, startTime);
                    }
                } else if ($(elementID).find("text") != null) {
                    for (var i = 0; i < animationData.eO.length; i++) {
                        if (animationData.eO[i].grNm == AnimationEffect.Sequence) {
                            switch (animationData.eO[i].nm) {
                                case EffectOptions.AsOneObject:
                                    fromValue = {};
                                    toValue = { onComplete: onComplete };
                                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                                    fromValue.ease = Power2.easeOut;
                                    entrance = TweenLite.fromTo(elementID, animationData.dA, fromValue, toValue);
                                    if (timeline != null) {
                                        let startTime = elementData.tim.sT || 0;
                                        timeline.add(entrance, startTime);
                                    }
                                    break;
                                case EffectOptions.ByParagraph:
                                    fromValue = {};
                                    toValue = { onCompleteAll: onComplete };
                                    var text = $(elementID).find("text");
                                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                                    //entrance = TweenLite.fromTo(elementID, animationData.dA, fromValue, toValue);
                                    fromValue.ease = Power2.easeOut;
                                    let from = { autoAlpha: 0, transformOrigin: "50% 125px", rotation: 0 };
                                    let to = { autoAlpha: 1, transformOrigin: "50% 125px", rotation: 90 };
                                    let entranceSta = TweenMax.staggerFromTo(text, animationData.dA / text.length, from, to, animationData.dA / text.length);
                                    if (timeline != null) {
                                        let startTime = elementData.tim.sT || 0;
                                        //timeline.add(entrance, startTime);
                                        timeline.add(entranceSta, startTime);
                                    }
                                    break;
                            }
                        }
                    }
                }

                break;
            case AnimationAction.Exit:
                if ($(elementID).find("text") == null) {
                    fromValue = { opacity: 1 };
                    toValue = { opacity: 0, onComplete: onComplete };
                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, toValue, fromValue);
                    fromValue.ease = Power2.easeOut;
                    exit = TweenLite.fromTo(elementID, animationData.dA, fromValue, toValue);
                    if (timeline != null) {
                        let endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                        timeline.add(exit, endTime);
                    }
                    break;

                } else if ($(elementID).find("text") != null) {
                    for (var i = 0; i < animationData.eO.length; i++) {
                        if (animationData.eO[i].grNm == AnimationEffect.Sequence) {
                            switch (animationData.eO[i].nm) {
                                case EffectOptions.AsOneObject:
                                    fromValue = { opacity: 1 };
                                    toValue = { opacity: 0, onComplete: onComplete };
                                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, toValue, fromValue);
                                    fromValue.ease = Power2.easeOut;
                                    exit = TweenLite.fromTo(elementID, animationData.dA, fromValue, toValue);
                                    if (timeline != null) {
                                        let endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                                        timeline.add(exit, endTime);
                                    }
                                    break;
                                case EffectOptions.ByParagraph:
                                    fromValue = { opacity: 1 };
                                    toValue = { opacity: 1, onComplete: onComplete };
                                    var text = $(elementID).find("text");
                                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, toValue, fromValue);
                                    fromValue.ease = Power2.easeOut;
                                    exit = TweenLite.fromTo(elementID, animationData.dA, fromValue, toValue);
                                    let from = { autoAlpha: 1, y: 10 };
                                    let to = { autoAlpha: 0, y: 0 };
                                    let exitSta = TweenMax.staggerFromTo(text, animationData.dA / text.length, from, to, animationData.dA / text.length);
                                    if (timeline != null) {
                                        let endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                                        timeline.add(exitSta, endTime);
                                    }
                                    break;
                            }
                        }
                    }
                }
        }
    }

    //Hiệu ứng Spin&Grow: tác động đến thuộc tính Rotation Scale & top left
    function getSpinGrowAnimation(animationData, elementData, timeline, onComplete) {
        let fromValue = null,
            toValue = null,
            entrance = null,
            exit = null,
            elementID = "#" + elementData.id;
        switch (animationData.typ) {
            case AnimationAction.Entrance:
                if ($(elementID).find("text") == null) {
                    fromValue = { scaleX: 0, scaleY: 0 };
                    toValue = { scaleX: 1, scaleY: 1, onComplete: onComplete };
                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                    fromValue.ease = Power2.easeOut;
                    entrance = TweenLite.fromTo("#" + elementData.id, animationData.dA, fromValue, toValue);
                    if (timeline != null) {
                        let startTime = elementData.tim.sT || 0;
                        timeline.add(entrance, startTime);
                    }
                } else if ($(elementID).find("text") != null) {
                    for (var i = 0; i < animationData.eO.length; i++) {
                        if (animationData.eO[i].grNm == AnimationEffect.Sequence) {
                            switch (animationData.eO[i].nm) {
                                case EffectOptions.AsOneObject:
                                    fromValue = { scaleX: 0, scaleY: 0 };
                                    toValue = { scaleX: 1, scaleY: 1, onComplete: onComplete };
                                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                                    fromValue.ease = Power2.easeOut;
                                    entrance = TweenLite.fromTo("#" + elementData.id, animationData.dA, fromValue, toValue);
                                    if (timeline != null) {
                                        let startTime = elementData.tim.sT || 0;
                                        timeline.add(entrance, startTime);
                                    }
                                    break;
                                case EffectOptions.ByParagraph:
                                    fromValue = { scaleX: 0, scaleY: 0 };
                                    toValue = { scaleX: 1, scaleY: 1, onComplete: onComplete };
                                    var text = $(elementID).find("text");
                                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                                    entrance = TweenLite.fromTo(elementID, animationData.dA, fromValue, toValue);
                                    let from = { scaleX: 0, scaleY: 0, opacity: 0 };
                                    let to = { scaleX: 1, scaleY: 1, opacity: 1 };
                                    let entranceSta = TweenMax.staggerFromTo(text, animationData.dA / text.length, from, to, animationData.dA / text.length);
                                    if (timeline != null) {
                                        let startTime = elementData.tim.sT || 0;
                                        timeline.add(entranceSta, startTime);
                                    }
                                    break;
                            }
                        }
                    }
                }
                break;
            case AnimationAction.Exit:
                if ($(elementID).find("text") == null) {
                    fromValue = { scaleX: 1, scaleY: 1 };
                    toValue = { scaleX: 0, scaleY: 0, onComplete: onComplete };
                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                    toValue.ease = Power2.easeOut;
                    exit = TweenLite.fromTo("#" + elementData.id, animationData.dA, fromValue, toValue);
                    if (timeline != null) {
                        let endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                        timeline.add(exit, endTime);
                    }
                } else if ($(elementID).find("text") != null) {
                    for (var i = 0; i < animationData.eO.length; i++) {
                        if (animationData.eO[i].grNm == AnimationEffect.Sequence) {
                            switch (animationData.eO[i].nm) {
                                case EffectOptions.AsOneObject:
                                    fromValue = { scaleX: 1, scaleY: 1 };
                                    toValue = { scaleX: 0, scaleY: 0, onComplete: onComplete };
                                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                                    //toValue.ease = Power2.easeOut;
                                    exit = TweenLite.fromTo("#" + elementData.id, animationData.dA, fromValue, toValue);
                                    if (timeline != null) {
                                        let endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                                        timeline.add(exit, endTime);
                                    }
                                    break;
                                case EffectOptions.ByParagraph:
                                    fromValue = { scaleX: 0.5, scaleY: 0.5, onComplete: onComplete };
                                    toValue = { scaleX: 1, scaleY: 1 };
                                    var text = $(elementID).find("text");
                                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                                    exit = TweenLite.fromTo("#" + elementData.id, animationData.dA, toValue, fromValue);
                                    let from = { autoAlpha: 1 };
                                    let to = { autoAlpha: 0 };
                                    let exitSta = TweenMax.staggerFromTo(text, animationData.dA / text.length, from, to, animationData.dA / text.length);
                                    if (timeline != null) {
                                        let endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                                        timeline.add(exitSta, endTime);
                                    }
                                    break;
                            }
                        }
                    }
                }
                break;
        }
    }

    //Hiệu ứng Grow&Spin: tác động đến thuộc tính Rotation Scale & top left
    function getGrowSpinAnimation(animationData, elementData, timeline, onComplete) {
        let fromValue = null,
            toValue = null,
            entrance = null,
            exit = null,
            elementID = "#" + elementData.id;
        switch (animationData.typ) {
            case AnimationAction.Entrance:
                if ($(elementID).find("text") == null) {
                    fromValue = { scaleX: 0, scaleY: 0, opacity: 0, rotation: elementData.agl + 90 };
                    toValue = { scaleX: 1, scaleY: 1, opacity: 1, rotation: elementData.agl, onComplete: onComplete };
                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                    fromValue.ease = Power2.easeOut;
                    entrance = TweenLite.fromTo("#" + elementData.id, animationData.dA, fromValue, toValue);
                    if (timeline != null) {
                        let startTime = elementData.tim.sT || 0;
                        timeline.add(entrance, startTime);
                    }
                } else if ($(elementID).find("text") != null) {
                    for (var i = 0; i < animationData.eO.length; i++) {
                        if (animationData.eO[i].grNm == AnimationEffect.Sequence) {
                            switch (animationData.eO[i].nm) {
                                case EffectOptions.AsOneObject:
                                    fromValue = { scaleX: 0, scaleY: 0, opacity: 0, rotation: elementData.agl + 90 };
                                    toValue = { scaleX: 1, scaleY: 1, opacity: 1, rotation: elementData.agl, onComplete: onComplete };
                                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                                    fromValue.ease = Power2.easeOut;
                                    entrance = TweenLite.fromTo("#" + elementData.id, animationData.dA, fromValue, toValue);
                                    if (timeline != null) {
                                        let startTime = elementData.tim.sT || 0;
                                        timeline.add(entrance, startTime);
                                    }
                                    break;
                                case EffectOptions.ByParagraph:
                                    fromValue = { scaleX: 0, scaleY: 0, opacity: 0, rotation: elementData.agl + 90, x: elementData.w / 2 };
                                    toValue = { scaleX: 1, scaleY: 1, opacity: 1, rotation: elementData.agl, x: 0, onComplete: onComplete };
                                    var text = $(elementID).find("text");
                                    AnimationHelpers.setEnterSqeValue(animationData.eO, elementData, fromValue, toValue);
                                    fromValue.ease = Power2.easeOut;
                                    let entranceSta = TweenMax.staggerFromTo(text, animationData.dA / text.length, fromValue, toValue, animationData.dA / text.length);
                                    if (timeline != null) {
                                        let startTime = elementData.tim.sT || 0;
                                        timeline.add(entranceSta, startTime);
                                    }
                                    break;
                            }
                        }
                    }
                }
                break;
            case AnimationAction.Exit:
                if ($(elementID).find("text") == null) {
                    fromValue = { scaleX: 0, scaleY: 0, opacity: 0, rotation: elementData.agl + 90, onComplete: onComplete };
                    toValue = { scaleX: 1, scaleY: 1, opacity: 1, rotation: elementData.agl };
                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                    toValue.ease = Power2.easeOut;
                    exit = TweenLite.fromTo("#" + elementData.id, animationData.dA, toValue, fromValue);
                    if (timeline != null) {
                        let endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                        timeline.add(exit, endTime);
                    }
                } else if ($(elementID).find("text") != null) {
                    for (var i = 0; i < animationData.eO.length; i++) {
                        if (animationData.eO[i].grNm == AnimationEffect.Sequence) {
                            switch (animationData.eO[i].nm) {
                                case EffectOptions.AsOneObject:
                                    fromValue = { scaleX: 0, scaleY: 0, opacity: 0, rotation: elementData.agl + 90, onComplete: onComplete };
                                    toValue = { scaleX: 1, scaleY: 1, opacity: 1, rotation: elementData.agl };
                                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                                    toValue.ease = Power2.easeOut;
                                    exit = TweenLite.fromTo("#" + elementData.id, animationData.dA, toValue, fromValue);
                                    if (timeline != null) {
                                        let endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                                        timeline.add(exit, endTime);
                                    }
                                    break;
                                case EffectOptions.ByParagraph:
                                    fromValue = { scaleX: 0, scaleY: 0, opacity: 0, rotation: elementData.agl + 90, x: elementData.w / 2, onComplete: onComplete };
                                    toValue = { scaleX: 1, scaleY: 1, opacity: 1, rotation: elementData.agl, x: 0 };
                                    var text = $(elementID).find("text");
                                    AnimationHelpers.setEnterSqeValue(animationData.eO, elementData, fromValue, toValue);
                                    fromValue.ease = Power2.easeOut;
                                    let exitSta = TweenMax.staggerFromTo(text, animationData.dA / text.length, toValue, fromValue, animationData.dA / text.length);
                                    if (timeline != null) {
                                        let endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                                        timeline.add(exitSta, endTime);
                                    }
                                    break;
                            }
                        }
                    }
                }
                break;
        }
    }

    //Hiệu ứng Zoom: tác động đến thuộc tính Scale & center object & center slide
    function getZoomAnimation(animationData, elementData, timeline, onComplete) {
        let fromValue = null,
            toValue = null,
            entrance = null,
            exit = null,
            elementID = "#" + elementData.id;
        switch (animationData.typ) {
            case AnimationAction.Entrance:
                if ($(elementID).find("text") == null) {
                    fromValue = { scaleX: 0, scaleY: 0, opacity: 0 };
                    toValue = { scaleX: 1, scaleY: 1, opacity: 1, onComplete: onComplete };
                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                    entrance = TweenLite.fromTo(elementID, animationData.dA, fromValue, toValue);
                    if (timeline != null) {
                        let startTime = elementData.tim.sT || 0;
                        timeline.add(entrance, startTime);
                    }
                } else if ($(elementID).find("text") != null) {
                    for (var i = 0; i < animationData.eO.length; i++) {
                        if (animationData.eO[i].grNm == AnimationEffect.Sequence) {
                            switch (animationData.eO[i].nm) {
                                case EffectOptions.AsOneObject:
                                    fromValue = { scaleX: 0, scaleY: 0, opacity: 0 };
                                    toValue = { scaleX: 1, scaleY: 1, opacity: 1, onComplete: onComplete };
                                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                                    entrance = TweenLite.fromTo(elementID, animationData.dA, fromValue, toValue);
                                    if (timeline != null) {
                                        let startTime = elementData.tim.sT || 0;
                                        timeline.add(entrance, startTime);
                                    }
                                    break;
                                case EffectOptions.ByParagraph:
                                    fromValue = { scaleX: 0, scaleY: 0, opacity: 0 };
                                    toValue = { scaleX: 1, scaleY: 1, opacity: 1, onCompleteAll: onComplete };
                                    var text = $(elementID).find("text");
                                    AnimationHelpers.setEnterSqeValue(animationData.eO, elementData, fromValue, toValue);
                                    let entranceSta = TweenMax.staggerFromTo(text, animationData.dA / text.length, fromValue, toValue, animationData.dA / text.length);
                                    if (timeline != null) {
                                        let startTime = elementData.tim.sT || 0;
                                        timeline.add(entranceSta, startTime);
                                    }
                                    break;
                            }
                        }
                    }
                }
                break;
            case AnimationAction.Exit:
                if ($(elementID).find("text") == null) {
                    fromValue = { scaleX: 0, scaleY: 0, opacity: 0, onComplete: onComplete };
                    toValue = { scaleX: 1, scaleY: 1, opacity: 1 };
                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                    exit = TweenLite.fromTo("#" + elementData.id, animationData.dA, toValue, fromValue);
                    if (timeline != null) {
                        let endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                        timeline.add(exit, endTime);
                    }
                } else if ($(elementID).find("text") != null) {
                    for (var i = 0; i < animationData.eO.length; i++) {
                        if (animationData.eO[i].grNm == AnimationEffect.Sequence) {
                            switch (animationData.eO[i].nm) {
                                case EffectOptions.AsOneObject:
                                    fromValue = { scaleX: 0, scaleY: 0, opacity: 0, onComplete: onComplete };
                                    toValue = { scaleX: 1, scaleY: 1, opacity: 1 };
                                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                                    exit = TweenLite.fromTo("#" + elementData.id, animationData.dA, toValue, fromValue);
                                    if (timeline != null) {
                                        let endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                                        timeline.add(exit, endTime);
                                    }
                                    break;
                                case EffectOptions.ByParagraph:
                                    fromValue = { scaleX: 0, scaleY: 0, opacity: 0, onCompleteAll: onComplete };
                                    toValue = { scaleX: 1, scaleY: 1, opacity: 1 };
                                    var text = $(elementID).find("text");
                                    AnimationHelpers.setEnterSqeValue(animationData.eO, elementData, fromValue, toValue);
                                    let exitSta = TweenMax.staggerFromTo(text, animationData.dA / text.length, fromValue, toValue, animationData.dA / text.length);
                                    if (timeline != null) {
                                        let endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                                        timeline.add(exit, endTime);
                                    }
                                    break;
                            }
                        }
                    }
                }

                break;
        }
    }

    //Hiệu ứng Swivel: tác động đến thuộc tính opacity & scale
    function getSwivelAnimation(animationData, elementData, timeline, onComplete) {
        let fromValue = null,
            toValue = null,
            half1 = null,
            half2 = null,
            entrance = null,
            exit = null,
            elementID = "#" + elementData.id;
        switch (animationData.typ) {
            case AnimationAction.Entrance:
                if ($(elementID).find("text") == null) {
                    fromValue = { opacity: 0 };
                    toValue = { opacity: 1, onComplete: onComplete };
                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                    entrance = TweenLite.fromTo(elementID, animationData.dA, fromValue, toValue);
                    half1 = TweenLite.to(elementID, animationData.dA / 2, { scaleX: -1 });
                    half2 = TweenLite.to(elementID, animationData.dA / 2, { scaleX: 1 });
                    if (timeline != null) {
                        let startTime = elementData.tim.sT || 0;
                        timeline.add(entrance, startTime);
                        timeline.add(half1, startTime);
                        timeline.add(half2, startTime + animationData.dA / 2);
                    }
                } else if ($(elementID).find("text") != null) {
                    for (var i = 0; i < animationData.eO.length; i++) {
                        if (animationData.eO[i].grNm == AnimationEffect.Sequence) {
                            switch (animationData.eO[i].nm) {
                                case EffectOptions.AsOneObject:
                                    fromValue = { opacity: 0 };
                                    toValue = { opacity: 1, onComplete: onComplete };
                                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                                    entrance = TweenLite.fromTo(elementID, animationData.dA, fromValue, toValue);
                                    half1 = TweenLite.to(elementID, animationData.dA / 2, { scaleX: -1 });
                                    half2 = TweenLite.to(elementID, animationData.dA / 2, { scaleX: 1 });
                                    if (timeline != null) {
                                        let startTime = elementData.tim.sT || 0;
                                        timeline.add(entrance, startTime);
                                        timeline.add(half1, startTime);
                                        timeline.add(half2, startTime + animationData.dA / 2);
                                    }
                                    break;
                                case EffectOptions.ByParagraph:
                                    fromValue = { opacity: 0 };
                                    toValue = { opacity: 1, onComplete: onComplete };
                                    var text = $(elementID).find("text");
                                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                                    half1 = TweenLite.to(text, animationData.dA / 2, { scaleX: -1 }, elementData.tim.dT / text.length);
                                    half2 = TweenLite.to(text, animationData.dA / 2, { scaleX: 1 }, elementData.tim.dT / text.length);
                                    //let entranceSta = TweenMax.staggerFromTo(text, elementData.tim.dT / text.length, fromValue, toValue, elementData.tim.dT / text.length);
                                    if (timeline != null) {
                                        let startTime = elementData.tim.sT || 0;
                                        //timeline.add(entranceSta, startTime);
                                        timeline.add(half1, startTime);
                                        timeline.add(half2, startTime + animationData.dA / 2);
                                    }
                                    break;
                            }
                        }
                    }
                }
                break;
            case AnimationAction.Exit:
                if ($(elementID).find("text") == null) {
                    fromValue = { opacity: 0, onComplete: onComplete };
                    toValue = { opacity: 1 };
                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                    let exit = TweenLite.fromTo("#" + elementData.id, animationData.dA, toValue, fromValue);
                    half1 = TweenLite.to(elementID, animationData.dA / 2, { scaleX: -1 });
                    half2 = TweenLite.to(elementID, animationData.dA / 2, { scaleX: 1 });
                    if (timeline != null) {
                        let endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                        timeline.add(exit, endTime);
                        timeline.add(half1, endTime);
                        timeline.add(half2, endTime + animationData.dA / 2);
                    }
                    break;
                } else if ($(elementID).find("text") != null) {
                    for (var i = 0; i < animationData.eO.length; i++) {
                        if (animationData.eO[i].grNm == AnimationEffect.Sequence) {
                            switch (animationData.eO[i].nm) {
                                case EffectOptions.AsOneObject:
                                    fromValue = { opacity: 0, onComplete: onComplete };
                                    toValue = { opacity: 1 };
                                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, toValue, fromValue);
                                    let exit = TweenLite.fromTo("#" + elementData.id, animationData.dA, toValue, fromValue);
                                    half1 = TweenLite.to(elementID, animationData.dA / 2, { scaleX: -1 });
                                    half2 = TweenLite.to(elementID, animationData.dA / 2, { scaleX: 1 });
                                    if (timeline != null) {
                                        let endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                                        timeline.add(exit, endTime);
                                        timeline.add(half1, endTime);
                                        timeline.add(half2, endTime + animationData.dA / 2);
                                    }
                                    break;
                                case EffectOptions.ByParagraph:
                                    fromValue = { opacity: 0, onComplete: onComplete };
                                    toValue = { opacity: 1 };
                                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, toValue, fromValue);
                                    half1 = TweenLite.to(text, animationData.dA / 2, { scaleX: -1 }, elementData.tim.dT / text.length);
                                    half2 = TweenLite.to(text, animationData.dA / 2, { scaleX: 1 }, elementData.tim.dT / text.length);
                                    //let exitSta = TweenMax.staggerFromTo(text, elementData.tim.dT / text.length, toValue, fromValue, elementData.tim.dT / text.length);
                                    if (timeline != null) {
                                        let endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                                        //timeline.add(exitSta, endTime);
                                        timeline.add(half1, endTime);
                                        timeline.add(half2, endTime + animationData.dA / 2);
                                    }
                                    break;
                            }
                        }
                    }
                }
                break;
        }
    }

    //Hiệu ứng Bounce:
    function getBounceAnimation(animationData, elementData, timeline, onComplete) {
        let fromValue = null,
            toValue = null,
            entrance = null,
            exit = null,
            elementID = "#" + elementData.id;
        switch (animationData.typ) {
            case AnimationAction.Entrance:
                if ($(elementID).find("text") == null) {
                    // fromValue = { left: elementData.l - 250, top: elementData.T - 250 };
                    // toValue = { left: elementData.l, top: elementData.T, onComplete: onComplete };
                    fromValue = { top: elementData.T - 250 };
                    toValue = { top: elementData.T, onComplete: onComplete };
                    toValue.ease = Bounce.easeOut;
                    entrance = TweenLite.fromTo("#" + elementData.id, animationData.dA, fromValue, toValue);
                    var leftEn = TweenLite.fromTo("#" + elementData.id, animationData.dA, { left: elementData.l - 250 }, { left: elementData.l });
                    if (timeline != null) {
                        let startTime = elementData.tim.sT || 0;
                        timeline.add(entrance, startTime);
                        timeline.add(leftEn, startTime);
                    }
                } else if ($(elementID).find("text") != null) {
                    for (var i = 0; i < animationData.eO.length; i++) {
                        if (animationData.eO[i].grNm == AnimationEffect.Sequence) {
                            switch (animationData.eO[i].nm) {
                                case EffectOptions.AsOneObject:
                                    fromValue = { top: elementData.T - 250 };
                                    toValue = { top: elementData.T, onComplete: onComplete };
                                    toValue.ease = Bounce.easeOut;
                                    entrance = TweenLite.fromTo("#" + elementData.id, animationData.dA, fromValue, toValue);
                                    var leftEn = TweenLite.fromTo("#" + elementData.id, animationData.dA, { left: elementData.l - 450 }, { left: elementData.l });
                                    if (timeline != null) {
                                        let startTime = elementData.tim.sT || 0;
                                        timeline.add(entrance, startTime);
                                        timeline.add(leftEn, startTime);
                                    }
                                    break;
                                case EffectOptions.ByParagraph:
                                    fromValue = { autoAlpha: 0, x: -250, y: -250 };
                                    toValue = { autoAlpha: 1, x: 0, y: 0, onComplete: onComplete };
                                    var text = $(elementID).find("text");
                                    toValue.ease = Bounce.easeOut;
                                    let entranceSta = TweenMax.staggerFromTo(text, animationData.dA / text.length, fromValue, toValue, animationData.dA / text.length);
                                    if (timeline != null) {
                                        let startTime = elementData.tim.sT || 0;
                                        timeline.add(entranceSta, startTime);
                                    }
                                    break;
                            }
                        }
                    }
                }
                break;
            case AnimationAction.Exit:
                if ($(elementID).find("text") == null) {
                    // fromValue = { left: elementData.l, top: elementData.T };
                    // toValue = { left: elementData.l + 150, top: elementData.T + 200, onComplete: onComplete };
                    fromValue = { top: elementData.T };
                    toValue = { top: elementData.T + 200, onComplete: onComplete };
                    toValue.ease = Bounce.easeOut;
                    exit = TweenLite.fromTo("#" + elementData.id, animationData.dA, fromValue, toValue);
                    var leftEx = TweenLite.fromTo("#" + elementData.id, animationData.dA, { left: elementData.l }, { left: elementData.l + 250 });
                    if (timeline != null) {
                        let endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                        timeline.add(exit, endTime);
                        timeline.add(leftEx, endTime);
                    }
                } else if ($(elementID).find("text") != null) {
                    for (var i = 0; i < animationData.eO.length; i++) {
                        if (animationData.eO[i].grNm == AnimationEffect.Sequence) {
                            switch (animationData.eO[i].nm) {
                                case EffectOptions.AsOneObject:
                                    fromValue = { top: elementData.T };
                                    toValue = { top: elementData.T + 200, onComplete: onComplete };
                                    toValue.ease = Bounce.easeOut;
                                    exit = TweenLite.fromTo("#" + elementData.id, animationData.dA, fromValue, toValue);
                                    var leftEx = TweenLite.fromTo("#" + elementData.id, animationData.dA, { left: elementData.l }, { left: elementData.l + 250 });
                                    if (timeline != null) {
                                        let endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                                        timeline.add(exit, endTime);
                                        timeline.add(leftEx, endTime);
                                    }
                                    break;
                                case EffectOptions.ByParagraph:
                                    fromValue = { opacity: 1, x: 0, y: 0 };
                                    toValue = { opacity: 0, x: 250, y: 250, onComplete: onComplete };
                                    var text = $(elementID).find("text");
                                    toValue.ease = Bounce.easeOut;
                                    let exitSta = TweenMax.staggerFromTo(text, animationData.dA / text.length, fromValue, toValue, animationData.dA / text.length);
                                    if (timeline != null) {
                                        let endTime = elementData.tim.sT || 0;
                                        timeline.add(exitSta, endTime);
                                    }
                                    break;
                            }
                        }
                    }
                }
                break;
        }
    }

    //Hiệu ứng Wheel
    function getWheelAnimation(animationData, elementData, timeline, onComplete) {
        let fromValue = null,
            toValue = null,
            wheelAnimation = null,
            wheelAnimation2 = null,
            value = null,
            entrance1 = null,
            entrance2 = null,
            exit1 = null,
            exit2 = null,
            elementID = "#" + elementData.id;
        switch (animationData.typ) {
            case AnimationAction.Entrance:
                if ($(elementID).find("text") == null) {
                    wheelAnimation = WheelAnimation(animationData, elementData);
                    value = { angle: 360 };
                    entrance1 = TweenLite.to(wheelAnimation, animationData.dA, value);
                    fromValue = {};
                    toValue = { onComplete: onComplete };
                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                    entrance2 = TweenLite.fromTo(elementID, animationData.dA, fromValue, toValue);
                    if (timeline != null) {
                        let startTime = elementData.tim.sT || 0;
                        timeline.add(entrance1, startTime);
                        timeline.add(entrance2, startTime);
                    }
                } else if ($(elementID).find("text") != null) {
                    for (var i = 0; i < animationData.eO.length; i++) {
                        if (animationData.eO[i].grNm == AnimationEffect.Sequence) {
                            switch (animationData.eO[i].nm) {
                                case EffectOptions.AsOneObject:
                                    wheelAnimation = WheelAnimation(animationData, elementData);
                                    value = { angle: 360 };
                                    entrance1 = TweenLite.to(wheelAnimation, animationData.dA, value);
                                    fromValue = {};
                                    toValue = { onComplete: onComplete };
                                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                                    entrance2 = TweenLite.fromTo(elementID, animationData.dA, fromValue, toValue);
                                    if (timeline != null) {
                                        let startTime = elementData.tim.sT || 0;
                                        timeline.add(entrance1, startTime);
                                        timeline.add(entrance2, startTime);
                                    }
                                    break;
                                case EffectOptions.ByParagraph:
                                    wheelAnimation = WheelAnimation(animationData, elementData);
                                    value = { angle: 360 };
                                    entrance1 = TweenLite.to(wheelAnimation, animationData.dA, value);
                                    var text = $(elementID).find("text");
                                    fromValue = { autoAlpha: 0 };
                                    toValue = { autoAlpha: 1, onComplete: onComplete };
                                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                                    let entrance2Sta = TweenMax.staggerFromTo(text, animationData.dA / text.length, fromValue, toValue, animationData.dA / text.length);
                                    if (timeline != null) {
                                        let startTime = elementData.tim.sT || 0;
                                        timeline.add(entrance1, startTime);
                                        timeline.add(entrance2Sta, startTime);
                                    }
                                    break;
                            }
                        }
                    }
                }
                break;
            case AnimationAction.Exit:
                if ($(elementID).find("text") == null) {
                    wheelAnimation2 = WheelAnimation(animationData, elementData);
                    value = { angle: 360 };
                    exit1 = TweenLite.to(wheelAnimation2, animationData.dA, value);
                    fromValue = {};
                    toValue = { onComplete: onComplete };
                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                    exit2 = TweenLite.fromTo(elementID, animationData.dA, fromValue, toValue);
                    if (timeline != null) {
                        let endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                        timeline.add(exit1, endTime);
                        timeline.add(exit2, endTime);
                    }
                } else if ($(elementID).find("text") != null) {
                    for (var i = 0; i < animationData.eO.length; i++) {
                        if (animationData.eO[i].grNm == AnimationEffect.Sequence) {
                            switch (animationData.eO[i].nm) {
                                case EffectOptions.AsOneObject:
                                    wheelAnimation2 = WheelAnimation(animationData, elementData);
                                    value = { angle: 360 };
                                    exit1 = TweenLite.to(wheelAnimation2, animationData.dA, value);
                                    fromValue = {};
                                    toValue = { onComplete: onComplete };
                                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                                    exit2 = TweenLite.fromTo(elementID, animationData.dA, fromValue, toValue);
                                    if (timeline != null) {
                                        let endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                                        timeline.add(exit1, endTime);
                                        timeline.add(exit2, endTime);
                                    }
                                    break;
                                case EffectOptions.ByParagraph:
                                    wheelAnimation2 = WheelAnimation(animationData, elementData);
                                    value = { angle: 360 };
                                    exit1 = TweenLite.to(wheelAnimation2, animationData.dA, value);
                                    var text = $(elementID).find("text");
                                    fromValue = { autoAlpha: 1 };
                                    toValue = { autoAlpha: 0, onComplete: onComplete };
                                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                                    let exit2Sta = TweenMax.staggerFromTo(text, animationData.dA / text.length, fromValue, toValue, animationData.dA / text.length);
                                    if (timeline != null) {
                                        let endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                                        timeline.add(exit1, endTime);
                                        timeline.add(exit2Sta, endTime);
                                    }
                                    break;
                            }
                        }
                    }
                }
                break;
        }
    }

    //Hiệu ứng dấu cộng
    function getPlusAnimation(animationData, elementData, timeline, onComplete) {
        let fromValue = null,
            toValue = null,
            plusAnimation = null,
            plusAnimation2 = null,
            value = null,
            entrance1 = null,
            entrance2 = null,
            exit1 = null,
            exit2 = null,
            startTime = null,
            endTime = null,
            elementID = "#" + elementData.id;
        switch (animationData.typ) {
            case AnimationAction.Entrance:
                if ($(elementID).find("text") == null) {
                    plusAnimation = PlusAnimation(animationData, elementData);
                    value = { percent: 100 };
                    entrance1 = TweenLite.to(plusAnimation, animationData.dA, value);
                    fromValue = {};
                    toValue = { onComplete: onComplete };
                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                    entrance2 = TweenLite.fromTo(elementID, animationData.dA, fromValue, toValue);
                    if (timeline != null) {
                        startTime = elementData.tim.sT || 0;
                        timeline.add(entrance1, startTime);
                        timeline.add(entrance2, startTime);
                    }
                } else if ($(elementID).find("text") != null) {
                    for (var i = 0; i < animationData.eO.length; i++) {
                        if (animationData.eO[i].grNm == AnimationEffect.Sequence) {
                            switch (animationData.eO[i].nm) {
                                case EffectOptions.AsOneObject:
                                    plusAnimation = PlusAnimation(animationData, elementData);
                                    value = { percent: 100 };
                                    entrance1 = TweenLite.to(plusAnimation, animationData.dA, value);
                                    fromValue = {};
                                    toValue = { onComplete: onComplete };
                                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                                    entrance2 = TweenLite.fromTo(elementID, animationData.dA, fromValue, toValue);
                                    if (timeline != null) {
                                        startTime = elementData.tim.sT || 0;
                                        timeline.add(entrance1, startTime);
                                        timeline.add(entrance2, startTime);
                                    }
                                    break;
                                case EffectOptions.ByParagraph:
                                    plusAnimation = PlusAnimation(animationData, elementData);
                                    value = { percent: 100 };
                                    entrance1 = TweenLite.to(plusAnimation, animationData.dA, value);
                                    fromValue = {};
                                    toValue = { onComplete: onComplete };
                                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                                    entrance2 = TweenLite.fromTo(elementID, animationData.dA, fromValue, toValue);
                                    // var text = $(elementID).find("text");
                                    // let entrance2Sta = TweenMax.staggerFromTo(text, animationData.dA / text.length, fromValue, toValue, animationData.dA / text.length);
                                    if (timeline != null) {
                                        startTime = elementData.tim.sT || 0;
                                        timeline.add(entrance1, startTime);
                                        timeline.add(entrance2, startTime);
                                    }
                                    break;
                            }
                        }
                    }
                }
                break;
            case AnimationAction.Exit:
                if ($(elementID).find("text") == null) {
                    plusAnimation2 = PlusAnimation(animationData, elementData);
                    boxAnimation2.percent = 99.9999;
                    exit1 = TweenLite.to(plusAnimation2, animationData.dA, { percent: 0 });
                    fromValue = {};
                    toValue = { onComplete: onComplete };
                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                    exit2 = TweenLite.fromTo(elementID, animationData.dA, fromValue, toValue);
                    if (timeline != null) {
                        endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                        timeline.add(exit1, endTime);
                        timeline.add(exit2, endTime);
                    }
                } else if ($(elementID).find("text") != null) {
                    for (var i = 0; i < animationData.eO.length; i++) {
                        if (animationData.eO[i].grNm == AnimationEffect.Sequence) {
                            switch (animationData.eO[i].nm) {
                                case EffectOptions.AsOneObject:
                                    plusAnimation2 = PlusAnimation(animationData, elementData);
                                    boxAnimation2.percent = 99.9999;
                                    exit1 = TweenLite.to(plusAnimation2, animationData.dA, { percent: 0 });
                                    fromValue = {};
                                    toValue = { onComplete: onComplete };
                                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                                    exit2 = TweenLite.fromTo(elementID, animationData.dA, fromValue, toValue);
                                    if (timeline != null) {
                                        endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                                        timeline.add(exit1, endTime);
                                        timeline.add(exit2, endTime);
                                    }
                                    break;
                                case EffectOptions.ByParagraph:
                                    plusAnimation2 = PlusAnimation(animationData, elementData);
                                    boxAnimation2.percent = 99.9999;
                                    exit1 = TweenLite.to(plusAnimation2, animationData.dA, { percent: 0 });
                                    fromValue = {};
                                    toValue = { onComplete: onComplete };
                                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                                    exit2 = TweenLite.fromTo(elementID, animationData.dA, fromValue, toValue);
                                    // var text = $(elementID).find("text");
                                    // let exit2Sta = TweenMax.staggerFromTo(text, animationData.dA / text.length, fromValue, toValue, animationData.dA / text.length);
                                    if (timeline != null) {
                                        endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                                        timeline.add(exit1, endTime);
                                        timeline.add(exit2, endTime);
                                    }
                                    break;
                            }
                        }
                    }
                }
                break;
        }
    }

    //Hiệu ứng hình thoi
    function getDiamonAnimation(animationData, elementData, timeline, onComplete) {
        let fromValue = null,
            toValue = null,
            diamonAnimation = null,
            diamonAnimation2 = null,
            value = null,
            entrance1 = null,
            entrance2 = null,
            exit1 = null,
            exit2 = null,
            startTime = null,
            endTime = null,
            elementID = "#" + elementData.id;
        switch (animationData.typ) {
            case AnimationAction.Entrance:
                if ($(elementID).find("text") == null) {
                    diamonAnimation = DiamonAnimation(animationData, elementData);
                    value = { percent: 100 };
                    entrance1 = TweenLite.to(diamonAnimation, animationData.dA, value);
                    fromValue = {};
                    toValue = { onComplete: onComplete };
                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                    entrance2 = TweenLite.fromTo("#" + elementData.id, animationData.dA, fromValue, toValue);
                    if (timeline != null) {
                        startTime = elementData.tim.sT || 0;
                        timeline.add(entrance1, startTime);
                        timeline.add(entrance2, startTime);
                    }
                } else if ($(elementID).find("text") != null) {
                    for (var i = 0; i < animationData.eO.length; i++) {
                        if (animationData.eO[i].grNm == AnimationEffect.Sequence) {
                            switch (animationData.eO[i].nm) {
                                case EffectOptions.AsOneObject:
                                    diamonAnimation = DiamonAnimation(animationData, elementData);
                                    value = { percent: 100 };
                                    entrance1 = TweenLite.to(diamonAnimation, animationData.dA, value);
                                    fromValue = {};
                                    toValue = { onComplete: onComplete };
                                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                                    entrance2 = TweenLite.fromTo("#" + elementData.id, animationData.dA, fromValue, toValue);
                                    if (timeline != null) {
                                        startTime = elementData.tim.sT || 0;
                                        timeline.add(entrance1, startTime);
                                        timeline.add(entrance2, startTime);
                                    }
                                    break;
                                case EffectOptions.ByParagraph:
                                    diamonAnimation = DiamonAnimation(animationData, elementData);
                                    value = { percent: 100 };
                                    entrance1 = TweenLite.to(diamonAnimation, animationData.dA, value);
                                    fromValue = {};
                                    toValue = { onComplete: onComplete };
                                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                                    entrance2 = TweenLite.fromTo("#" + elementData.id, animationData.dA, fromValue, toValue);
                                    //var text = $(elementID).find("text");
                                    //AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                                    //let entrance2Sta = TweenMax.staggerFromTo(text, animationData.dA / text.length, fromValue, toValue, animationData.dA / text.length);
                                    if (timeline != null) {
                                        startTime = elementData.tim.sT || 0;
                                        timeline.add(entrance1, startTime);
                                        timeline.add(entrance2, startTime);
                                    }
                                    break;
                            }
                        }
                    }
                }
                break;
            case AnimationAction.Exit:
                if ($(elementID).find("text") == null) {
                    diamonAnimation2 = DiamonAnimation(animationData, elementData);
                    boxAnimation2.percent = 99.9999;
                    exit1 = TweenLite.to(diamonAnimation2, animationData.dA, { percent: 0 });
                    fromValue = {};
                    toValue = { onComplete: onComplete };
                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                    exit2 = TweenLite.fromTo("#" + elementData.id, animationData.dA, fromValue, toValue);
                    if (timeline != null) {
                        endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                        timeline.add(exit1, endTime);
                        timeline.add(exit2, endTime);
                    }
                } else if ($(elementID).find("text") != null) {
                    for (var i = 0; i < animationData.eO.length; i++) {
                        if (animationData.eO[i].grNm == AnimationEffect.Sequence) {
                            switch (animationData.eO[i].nm) {
                                case EffectOptions.AsOneObject:
                                    diamonAnimation2 = DiamonAnimation(animationData, elementData);
                                    boxAnimation2.percent = 99.9999;
                                    exit1 = TweenLite.to(diamonAnimation2, animationData.dA, { percent: 0 });
                                    fromValue = {};
                                    toValue = { onComplete: onComplete };
                                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                                    exit2 = TweenLite.fromTo("#" + elementData.id, animationData.dA, fromValue, toValue);
                                    if (timeline != null) {
                                        endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                                        timeline.add(exit1, endTime);
                                        timeline.add(exit2, endTime);
                                    }
                                    break;
                                case EffectOptions.ByParagraph:
                                    diamonAnimation2 = DiamonAnimation(animationData, elementData);
                                    boxAnimation2.percent = 99.9999;
                                    exit1 = TweenLite.to(diamonAnimation2, animationData.dA, { percent: 0 });
                                    fromValue = {};
                                    toValue = { onComplete: onComplete };
                                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                                    exit2 = TweenLite.fromTo("#" + elementData.id, animationData.dA, fromValue, toValue);
                                    //AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                                    //exit2 = TweenLite.fromTo("#" + elementData.id, animationData.dA, fromValue, toValue);
                                    if (timeline != null) {
                                        endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                                        timeline.add(exit1, endTime);
                                        timeline.add(exit2, endTime);
                                    }
                                    break;
                            }
                        }
                    }
                }
                break;
        }
    }

    //Hiệu ứng hình vuông
    function getBoxAnimation(animationData, elementData, timeline, onComplete) {
        let fromValue = null,
            toValue = null,
            boxAnimation = null,
            boxAnimation2 = null,
            value = null,
            entrance1 = null,
            entrance2 = null,
            exit1 = null,
            exit2 = null,
            startTime = null,
            endTime = null,
            elementID = "#" + elementData.id;
        switch (animationData.typ) {
            case AnimationAction.Entrance:
                if ($(elementID).find("text") == null) {
                    boxAnimation = BoxAnimation(animationData, elementData);
                    value = { percent: 100 };
                    entrance1 = TweenLite.to(boxAnimation, animationData.dA, value);
                    fromValue = {};
                    toValue = { onComplete: onComplete };
                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                    entrance2 = TweenLite.fromTo("#" + elementData.id, animationData.dA, fromValue, toValue);
                    if (timeline != null) {
                        startTime = elementData.tim.sT || 0;
                        timeline.add(entrance1, startTime);
                        timeline.add(entrance2, startTime);
                    }
                } else if ($(elementID).find("text") != null) {
                    for (var i = 0; i < animationData.eO.length; i++) {
                        if (animationData.eO[i].grNm == AnimationEffect.Sequence) {
                            switch (animationData.eO[i].nm) {
                                case EffectOptions.AsOneObject:
                                    boxAnimation = BoxAnimation(animationData, elementData);
                                    value = { percent: 100 };
                                    entrance1 = TweenLite.to(boxAnimation, animationData.dA, value);
                                    fromValue = {};
                                    toValue = { onComplete: onComplete };
                                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                                    entrance2 = TweenLite.fromTo("#" + elementData.id, animationData.dA, fromValue, toValue);
                                    if (timeline != null) {
                                        startTime = elementData.tim.sT || 0;
                                        timeline.add(entrance1, startTime);
                                        timeline.add(entrance2, startTime);
                                    }
                                    break;
                                case EffectOptions.ByParagraph:
                                    boxAnimation = BoxAnimation(animationData, elementData);
                                    value = { percent: 100 };
                                    entrance1 = TweenLite.to(boxAnimation, animationData.dA, value);
                                    fromValue = {};
                                    toValue = { onComplete: onComplete };
                                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                                    entrance2 = TweenLite.fromTo("#" + elementData.id, animationData.dA, fromValue, toValue);
                                    //var text = $(elementID).find("text");
                                    //AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                                    //let entrance2Sta = TweenMax.staggerFromTo(text, animationData.dA / text.length, fromValue, toValue, animationData.dA / text.length);
                                    if (timeline != null) {
                                        startTime = elementData.tim.sT || 0;
                                        timeline.add(entrance1, startTime);
                                        timeline.add(entrance2, startTime);
                                    }
                                    break;
                            }
                        }
                    }
                }
                break;
            case AnimationAction.Exit:
                if ($(elementID).find("text") == null) {
                    boxAnimation2 = BoxAnimation(animationData, elementData);
                    boxAnimation2.percent = 99.9999;
                    exit1 = TweenLite.to(boxAnimation2, animationData.dA, { percent: 0 });
                    fromValue = {};
                    toValue = { onComplete: onComplete };
                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                    exit2 = TweenLite.fromTo("#" + elementData.id, animationData.dA, fromValue, toValue);
                    if (timeline != null) {
                        endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                        timeline.add(exit1, endTime);
                        timeline.add(exit2, endTime);
                    }
                } else if ($(elementID).find("text") != null) {
                    for (var i = 0; i < animationData.eO.length; i++) {
                        if (animationData.eO[i].grNm == AnimationEffect.Sequence) {
                            switch (animationData.eO[i].nm) {
                                case EffectOptions.AsOneObject:
                                    boxAnimation2 = BoxAnimation(animationData, elementData);
                                    boxAnimation2.percent = 99.9999;
                                    exit1 = TweenLite.to(boxAnimation2, animationData.dA, { percent: 0 });
                                    fromValue = {};
                                    toValue = { onComplete: onComplete };
                                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                                    exit2 = TweenLite.fromTo("#" + elementData.id, animationData.dA, fromValue, toValue);
                                    if (timeline != null) {
                                        endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                                        timeline.add(exit1, endTime);
                                        timeline.add(exit2, endTime);
                                    }
                                    break;
                                case EffectOptions.ByParagraph:
                                    boxAnimation2 = BoxAnimation(animationData, elementData);
                                    boxAnimation2.percent = 99.9999;
                                    exit1 = TweenLite.to(boxAnimation2, animationData.dA, { percent: 0 });
                                    fromValue = {};
                                    toValue = { onComplete: onComplete };
                                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                                    exit2 = TweenLite.fromTo("#" + elementData.id, animationData.dA, fromValue, toValue);
                                    //var text = $(elementID).find("text");
                                    //AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                                    //let exit2Sta = TweenMax.staggerFromTo(text, animationData.dA / text.length, fromValue, toValue, animationData.dA / text.length);
                                    if (timeline != null) {
                                        endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                                        timeline.add(exit1, endTime);
                                        timeline.add(exit2, endTime);
                                    }
                                    break;
                            }
                        }
                    }
                }
                break;
        }
    }

    //Hiệu ứng hình ellipse
    function getEllipseAnimation(animationData, elementData, timeline, onComplete) {
        let fromValue = null,
            toValue = null,
            ellipseAnimation = null,
            ellipseAnimation2 = null,
            value = null,
            entrance1 = null,
            entrance2 = null,
            exit1 = null,
            exit2 = null,
            startTime = null,
            endTime = null,
            elementID = "#" + elementData.id;
        switch (animationData.typ) {
            case AnimationAction.Entrance:
                if ($(elementID).find("text") == null) {
                    ellipseAnimation = EllipseAnimation(animationData, elementData);
                    value = { percent: 100 };
                    entrance1 = TweenLite.to(ellipseAnimation, animationData.dA, value);
                    fromValue = {};
                    toValue = { onComplete: onComplete };
                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                    entrance2 = TweenLite.fromTo("#" + elementData.id, animationData.dA, fromValue, toValue);
                    if (timeline != null) {
                        startTime = elementData.tim.sT || 0;
                        timeline.add(entrance1, startTime);
                        timeline.add(entrance2, startTime);
                    }
                } else if ($(elementID).find("text") != null) {
                    for (var i = 0; i < animationData.eO.length; i++) {
                        if (animationData.eO[i].grNm == AnimationEffect.Sequence) {
                            switch (animationData.eO[i].nm) {
                                case EffectOptions.AsOneObject:
                                    ellipseAnimation = EllipseAnimation(animationData, elementData);
                                    value = { percent: 100 };
                                    entrance1 = TweenLite.to(ellipseAnimation, animationData.dA, value);
                                    fromValue = {};
                                    toValue = { onComplete: onComplete };
                                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                                    entrance2 = TweenLite.fromTo("#" + elementData.id, animationData.dA, fromValue, toValue);
                                    if (timeline != null) {
                                        startTime = elementData.tim.sT || 0;
                                        timeline.add(entrance1, startTime);
                                        timeline.add(entrance2, startTime);
                                    }
                                    break;
                                case EffectOptions.ByParagraph:
                                    ellipseAnimation = EllipseAnimation(animationData, elementData);
                                    value = { percent: 100 };
                                    entrance1 = TweenLite.to(ellipseAnimation, animationData.dA, value);
                                    fromValue = {};
                                    toValue = { onComplete: onComplete };
                                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                                    entrance2 = TweenLite.fromTo("#" + elementData.id, animationData.dA, fromValue, toValue);
                                    //var text = $(elementID).find("text");
                                    //AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                                    //let entrance2Sta = TweenMax.staggerFromTo(text, animationData.dA / text.length, fromValue, toValue, animationData.dA / text.length);
                                    if (timeline != null) {
                                        startTime = elementData.tim.sT || 0;
                                        timeline.add(entrance1, startTime);
                                        timeline.add(entrance2, startTime);
                                    }
                                    break;
                            }
                        }
                    }
                }
                break;
            case AnimationAction.Exit:
                if ($(elementID).find("text") == null) {
                    ellipseAnimation2 = EllipseAnimation(animationData, elementData);
                    boxAnimation2.percent = 99.9999;
                    exit1 = TweenLite.to(ellipseAnimation2, animationData.dA, { percent: 0, onComplete: onComplete });
                    fromValue = {};
                    toValue = { onComplete: onComplete };
                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                    exit2 = TweenLite.fromTo("#" + elementData.id, animationData.dA, fromValue, toValue);
                    if (timeline != null) {
                        endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                        timeline.add(exit1, endTime);
                        timeline.add(exit2, endTime);
                    }
                } else if ($(elementID).find("text") != null) {
                    for (var i = 0; i < animationData.eO.length; i++) {
                        if (animationData.eO[i].grNm == AnimationEffect.Sequence) {
                            switch (animationData.eO[i].nm) {
                                case EffectOptions.AsOneObject:
                                    ellipseAnimation2 = EllipseAnimation(animationData, elementData);
                                    boxAnimation2.percent = 99.9999;
                                    exit1 = TweenLite.to(ellipseAnimation2, animationData.dA, { percent: 0, onComplete: onComplete });
                                    fromValue = {};
                                    toValue = { onComplete: onComplete };
                                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                                    exit2 = TweenLite.fromTo("#" + elementData.id, animationData.dA, fromValue, toValue);
                                    if (timeline != null) {
                                        endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                                        timeline.add(exit1, endTime);
                                        timeline.add(exit2, endTime);
                                    }
                                    break;
                                case EffectOptions.ByParagraph:
                                    ellipseAnimation2 = EllipseAnimation(animationData, elementData);
                                    boxAnimation2.percent = 99.9999;
                                    exit1 = TweenLite.to(ellipseAnimation2, animationData.dA, { percent: 0, onComplete: onComplete });
                                    fromValue = {};
                                    toValue = { onComplete: onComplete };
                                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                                    exit2 = TweenLite.fromTo("#" + elementData.id, animationData.dA, fromValue, toValue);
                                    //var text = $(elementID).find("text");
                                    //AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                                    //let exit2Sta = TweenMax.staggerFromTo(text, animationData.dA / text.length, fromValue, toValue, animationData.dA / text.length);
                                    if (timeline != null) {
                                        endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                                        timeline.add(exit1, endTime);
                                        timeline.add(exit2, endTime);
                                    }
                                    break;
                            }
                        }
                    }
                }
                break;
        }
    }

    //Hiệu ứng hình split
    function getSplitAnimation(animationData, elementData, timeline, onComplete) {
        let fromValue = null,
            toValue = null,
            value = null,
            entrance1 = null,
            entrance2 = null,
            exit1 = null,
            exit2 = null,
            from = null,
            to = null,
            elementID = "#" + elementData.id;
        let boxAnimation = null;
        switch (animationData.typ) {
            case AnimationAction.Entrance:
                if ($(elementID).find("text") == null) {
                    boxAnimation = SplitAnimation(animationData, elementData);
                    value = { percent: 100 };
                    entrance1 = TweenLite.to(boxAnimation, animationData.dA, value);
                    fromValue = {};
                    toValue = { onComplete: onComplete };
                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                    entrance2 = TweenLite.fromTo("#" + elementData.id, animationData.dA, fromValue, toValue);
                    if (timeline != null) {
                        let startTime = elementData.tim.sT || 0;
                        timeline.add(entrance1, startTime);
                        timeline.add(entrance2, startTime);
                    }
                } else if ($(elementID).find("text") != null) {
                    for (var i = 0; i < animationData.eO.length; i++) {
                        if (animationData.eO[i].grNm == AnimationEffect.Sequence) {
                            switch (animationData.eO[i].nm) {
                                case EffectOptions.AsOneObject:
                                    boxAnimation = SplitAnimation(animationData, elementData);
                                    value = { percent: 100 };
                                    entrance1 = TweenLite.to(boxAnimation, animationData.dA, value);
                                    fromValue = {};
                                    toValue = { onComplete: onComplete };
                                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                                    entrance2 = TweenLite.fromTo("#" + elementData.id, animationData.dA, fromValue, toValue);
                                    if (timeline != null) {
                                        let startTime = elementData.tim.sT || 0;
                                        timeline.add(entrance1, startTime);
                                        timeline.add(entrance2, startTime);
                                    }
                                    break;
                                case EffectOptions.ByParagraph:
                                    boxAnimation = SplitAnimation(animationData, elementData);
                                    value = { percent: 100 };
                                    entrance1 = TweenLite.to(boxAnimation, animationData.dA, value);
                                    fromValue = { autoAlpha: 0 };
                                    toValue = { autoAlpha: 1, onComplete: onComplete };
                                    var text = $(elementID).find("text");
                                    //AnimationHelpers.setEnterSqeValue(animationData.eO, elementData, fromValue, toValue);
                                    let entranceSta = TweenMax.staggerFromTo(text, animationData.dA / text.length, fromValue, toValue, animationData.dA / text.length);
                                    if (timeline != null) {
                                        let startTime = elementData.tim.sT || 0;
                                        timeline.add(entrance1, startTime);
                                        timeline.add(entranceSta, startTime);
                                    }
                                    break;
                            }
                        }
                    }
                }
                break;
            case AnimationAction.Exit:
                if ($(elementID).find("text") == null) {
                    boxAnimation = SplitAnimation(animationData, elementData);
                    form = { percent: 99.99999999 };
                    to = { percent: 0 }
                    exit1 = TweenLite.fromTo(boxAnimation, animationData.dA, form, to);
                    fromValue = {};
                    toValue = { onComplete: onComplete };
                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, toValue, fromValue);
                    exit2 = TweenLite.fromTo("#" + elementData.id, animationData.dA, fromValue, toValue);
                    if (timeline != null) {
                        let endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                        timeline.add(exit1, endTime);
                        timeline.add(exit2, endTime);
                    }
                } else if ($(elementID).find("text") != null) {
                    for (var i = 0; i < animationData.eO.length; i++) {
                        if (animationData.eO[i].grNm == AnimationEffect.Sequence) {
                            switch (animationData.eO[i].nm) {
                                case EffectOptions.AsOneObject:
                                    boxAnimation = SplitAnimation(animationData, elementData);
                                    form = { percent: 99.99999999 };
                                    to = { percent: 0 }
                                    exit1 = TweenLite.fromTo(boxAnimation, animationData.dA, form, to);
                                    fromValue = {};
                                    toValue = { onComplete: onComplete };
                                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, toValue, fromValue);
                                    exit2 = TweenLite.fromTo("#" + elementData.id, animationData.dA, fromValue, toValue);
                                    if (timeline != null) {
                                        let endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                                        timeline.add(exit1, endTime);
                                        timeline.add(exit2, endTime);
                                    }
                                    break;
                                case EffectOptions.ByParagraph:
                                    boxAnimation = SplitAnimation(animationData, elementData);
                                    form = { percent: 99.99999999 };
                                    to = { percent: 0 }
                                    exit1 = TweenLite.fromTo(boxAnimation, animationData.dA, form, to);
                                    fromValue = { autoAlpha: 1 };
                                    toValue = { autoAlpha: 0, onComplete: onComplete };
                                    var text = $(elementID).find("text");
                                    //AnimationHelpers.setEnterSqeValue(animationData.eO, elementData, fromValue, toValue);
                                    let exitSta = TweenMax.staggerFromTo(text, animationData.dA / text.length, fromValue, toValue, animationData.dA / text.length);
                                    if (timeline != null) {
                                        let endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                                        timeline.add(exit1, endTime);
                                        timeline.add(exitSta, endTime);
                                    }
                                    break;
                            }
                        }
                    }
                }

                break;
        }
    }

    //Hiệu ứng hình Wipe
    function getWipeAnimation(animationData, elementData, timeline, onComplete) {
        let fromValue = null,
            toValue = null,
            entrance1 = null,
            entrance2 = null,
            entranceSta = null,
            value = null,
            boxAnimation = null,
            boxAnimation2 = null,
            exit1 = null,
            exit2 = null,
            endTime = null,
            startTime = null,
            elementID = "#" + elementData.id;
        switch (animationData.typ) {
            case AnimationAction.Entrance:
                if ($(elementID).find("text") == null) {
                    boxAnimation = WipeAnimation(animationData, elementData);
                    value = { percent: 100 };
                    entrance1 = TweenLite.to(boxAnimation, animationData.dA, value);
                    fromValue = {};
                    toValue = {};
                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                    entrance2 = TweenLite.fromTo("#" + elementData.id, animationData.dA, fromValue, toValue);
                    if (timeline != null) {
                        startTime = elementData.tim.sT || 0;
                        timeline.add(entrance1, startTime);
                        timeline.add(entrance2, startTime);
                    }
                } else if ($(elementID).find("text") != null) {
                    for (var i = 0; i < animationData.eO.length; i++) {
                        if (animationData.eO[i].grNm == AnimationEffect.Sequence) {
                            switch (animationData.eO[i].nm) {
                                case EffectOptions.AsOneObject:
                                    boxAnimation = WipeAnimation(animationData, elementData);
                                    value = { percent: 100 };
                                    entrance1 = TweenLite.to(boxAnimation, animationData.dA, value);
                                    fromValue = {};
                                    toValue = {};
                                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                                    entrance2 = TweenLite.fromTo("#" + elementData.id, animationData.dA, fromValue, toValue);
                                    if (timeline != null) {
                                        startTime = elementData.tim.sT || 0;
                                        timeline.add(entrance1, startTime);
                                        timeline.add(entrance2, startTime);
                                    }
                                    break;
                                case EffectOptions.ByParagraph:
                                    for (var i = 0; i < animationData.eO.length; i++) {
                                        var aniEo = animationData.eO[i];
                                        if (aniEo.grNm == "Enter" && aniEo.nm != "None" && animationData) {
                                            fromValue = { autoAlpha: 0 };
                                            toValue = { autoAlpha: 1 };
                                            AnimationHelpers.setEnterSqeValue(animationData.eO, elementData, fromValue, toValue);
                                            var text = $(elementID).find("text");
                                            entranceSta = TweenMax.staggerFromTo(text, animationData.dA / text.length, fromValue, toValue, animationData.dA / text.length);
                                            if (timeline != null) {
                                                startTime = elementData.tim.sT || 0;
                                                timeline.add(entranceSta, startTime);
                                            }
                                        } else if (aniEo.grNm == "Enter" && aniEo.nm == "None") {
                                            boxAnimation = WipeAnimation(animationData, elementData);
                                            value = { percent: 100 };
                                            entrance1 = TweenLite.to(boxAnimation, animationData.dA, value);
                                            if (timeline != null) {
                                                startTime = elementData.tim.sT || 0;
                                                timeline.add(entrance1, startTime);
                                            }
                                        } else if (aniEo.grNm == "Direction") {
                                            switch (aniEo.nm) {
                                                case "Left":
                                                    fromValue = { autoAlpha: 0, x: -elementData.w, y: elementData.h };
                                                    toValue = { autoAlpha: 1, x: 0, y: 0, onCompleteAll: onComplete };
                                                    var text = $(elementID).find("text");
                                                    entranceSta = TweenMax.staggerFromTo(text, animationData.dA / text.length, fromValue, toValue, animationData.dA / text.length);
                                                    startTime = elementData.tim.sT || 0;
                                                    timeline.add(entranceSta, startTime);
                                                    break;
                                                case "Right":
                                                    fromValue = { autoAlpha: 0, x: elementData.w, y: elementData.h };
                                                    toValue = { autoAlpha: 1, x: 0, y: 0, onCompleteAll: onComplete };
                                                    var text = $(elementID).find("text");
                                                    entranceSta = TweenMax.staggerFromTo(text, animationData.dA / text.length, fromValue, toValue, animationData.dA / text.length);
                                                    startTime = elementData.tim.sT || 0;
                                                    timeline.add(entranceSta, startTime);
                                                    break;
                                            }
                                        }
                                    }
                                    break;
                            }
                        }
                    }
                }
                break;
            case AnimationAction.Exit:
                if ($(elementID).find("text") == null) {
                    boxAnimation2 = WipeAnimation(animationData, elementData);
                    boxAnimation2.percent = 99.9999;
                    exit1 = TweenLite.to(boxAnimation2, animationData.dA, { percent: 0 });
                    fromValue = {};
                    toValue = { onComplete: onComplete };
                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, toValue, fromValue);
                    exit2 = TweenLite.fromTo("#" + elementData.id, animationData.dA, fromValue, toValue);
                    if (timeline != null) {
                        endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                        timeline.add(exit1, startTime);
                        timeline.add(exit2, startTime);
                    }
                } else if ($(elementID).find("text") != null) {
                    for (var i = 0; i < animationData.eO.length; i++) {
                        if (animationData.eO[i].grNm == AnimationEffect.Sequence) {
                            switch (animationData.eO[i].nm) {
                                case EffectOptions.AsOneObject:
                                    boxAnimation2 = WipeAnimation(animationData, elementData);
                                    boxAnimation2.percent = 99.9999;
                                    exit1 = TweenLite.to(boxAnimation2, animationData.dA, { percent: 0 });
                                    fromValue = {};
                                    toValue = { onComplete: onComplete };
                                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, toValue, fromValue);
                                    exit2 = TweenLite.fromTo("#" + elementData.id, animationData.dA, fromValue, toValue);
                                    if (timeline != null) {
                                        endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                                        timeline.add(exit1, startTime);
                                        timeline.add(exit2, startTime);
                                    }
                                    break;
                                case EffectOptions.ByParagraph:
                                    for (var i = 0; i < animationData.eO.length; i++) {
                                        var aniEo = animationData.eO[i];
                                        if (aniEo.grNm == "Enter" && aniEo.nm != "None" && animationData) {
                                            fromValue = { autoAlpha: 1 };
                                            toValue = { autoAlpha: 0 };
                                            AnimationHelpers.setEnterSqeValue(animationData.eO, elementData, fromValue, toValue);
                                            var text = $(elementID).find("text");
                                            entranceSta = TweenMax.staggerFromTo(text, animationData.dA / text.length, fromValue, toValue, animationData.dA / text.length);
                                            if (timeline != null) {
                                                endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                                                timeline.add(entranceSta, endTime);
                                            }
                                        } else if (aniEo.grNm == "Enter" && aniEo.nm == "None") {
                                            boxAnimation2 = WipeAnimation(animationData, elementData);
                                            boxAnimation2.percent = 99.9999;
                                            exit1 = TweenLite.to(boxAnimation2, animationData.dA, { percent: 0 });
                                            if (timeline != null) {
                                                endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                                                timeline.add(exit1, endTime);
                                            }
                                        } else if (aniEo.grNm == "Direction") {
                                            switch (aniEo.nm) {
                                                case "Left":
                                                    fromValue = { autoAlpha: 1, x: 0, y: 0 };
                                                    toValue = { autoAlpha: 0, x: -elementData.w, y: elementData.h, onCompleteAll: onComplete };
                                                    var text = $(elementID).find("text");
                                                    entranceSta = TweenMax.staggerFromTo(text, animationData.dA / text.length, fromValue, toValue, animationData.dA / text.length);
                                                    if (timeline != null) {
                                                        endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                                                        timeline.add(entranceSta, endTime);
                                                    }
                                                    break;
                                                case "Right":
                                                    fromValue = { autoAlpha: 1, x: 0, y: 0 };
                                                    toValue = { autoAlpha: 0, x: elementData.w, y: elementData.h, onCompleteAll: onComplete };
                                                    var text = $(elementID).find("text");
                                                    entranceSta = TweenMax.staggerFromTo(text, animationData.dA / text.length, fromValue, toValue, animationData.dA / text.length);
                                                    if (timeline != null) {
                                                        endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                                                        timeline.add(entranceSta, endTime);
                                                    }
                                                    break;
                                            }
                                        }
                                    }
                                    break;
                            }
                        }
                    }
                }
                break;
        }
    }

    //Hiệu ứng hình Random bars
    function getRandomBarsAnimation(animationData, elementData, timeline, onComplete) {
        let fromValue = null,
            toValue = null,
            randomAnimation = null,
            randomAnimation2 = null,
            value = null,
            entrance1 = null,
            entrance2 = null,
            exit1 = null,
            exit2 = null,
            elementID = "#" + elementData.id;
        switch (animationData.typ) {
            case AnimationAction.Entrance:
                if ($(elementID).find("text") == null) {
                    randomAnimation = RandomBarsAnimation(animationData, elementData);
                    value = { percent: 100 };
                    entrance1 = TweenLite.to(randomAnimation, animationData.dA, value);
                    fromValue = {};
                    toValue = { onComplete: onComplete };
                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                    entrance2 = TweenLite.fromTo(elementID, animationData.dA, fromValue, toValue);
                    if (timeline != null) {
                        let startTime = elementData.tim.sT || 0;
                        timeline.add(entrance1, startTime);
                        timeline.add(entrance2, startTime);
                    }
                } else if ($(elementID).find("text") != null) {
                    for (var i = 0; i < animationData.eO.length; i++) {
                        if (animationData.eO[i].grNm == AnimationEffect.Sequence) {
                            switch (animationData.eO[i].nm) {
                                case EffectOptions.AsOneObject:
                                    randomAnimation = RandomBarsAnimation(animationData, elementData);
                                    value = { percent: 100 };
                                    entrance1 = TweenLite.to(randomAnimation, animationData.dA, value);
                                    fromValue = {};
                                    toValue = { onComplete: onComplete };
                                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, fromValue, toValue);
                                    entrance2 = TweenLite.fromTo(elementID, animationData.dA, fromValue, toValue);
                                    if (timeline != null) {
                                        let startTime = elementData.tim.sT || 0;
                                        timeline.add(entrance1, startTime);
                                        timeline.add(entrance2, startTime);
                                    }
                                    break;
                                case EffectOptions.ByParagraph:
                                    randomAnimation = RandomBarsAnimation(animationData, elementData);
                                    value = { percent: 100 };
                                    entrance1 = TweenLite.to(randomAnimation, animationData.dA, value);
                                    var text = $(elementID).find("text");
                                    fromValue = { autoAlpha: 0 };
                                    toValue = { autoAlpha: 1, onCompleteAll: onComplete };
                                    AnimationHelpers.setEnterSqeValue(animationData.eO, elementData, fromValue, toValue);
                                    let entrance2Sta = TweenMax.staggerFromTo(text, elementData.tim.dT / text.length, fromValue, toValue, elementData.tim.dT / text.length);
                                    if (timeline != null) {
                                        let startTime = elementData.tim.sT || 0;
                                        timeline.add(entrance1, startTime);
                                        timeline.add(entrance2Sta, startTime);
                                    }
                                    break;
                            }
                        }
                    }
                }
                break;
            case AnimationAction.Exit:
                if ($(elementID).find("text") == null) {
                    randomAnimation2 = RandomBarsAnimation(animationData, elementData);
                    randomAnimation2.percent = 99.9999999999999999;
                    exit1 = TweenLite.to(randomAnimation2, animationData.dA, { percent: 0 });
                    fromValue = {};
                    toValue = { onComplete: onComplete };
                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, toValue, fromValue);
                    exit2 = TweenLite.fromTo(elementID, animationData.dA, fromValue, toValue);
                    if (timeline != null) {
                        let endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                        timeline.add(exit1, endTime);
                        timeline.add(exit2, endTime);
                    }
                } else if ($(elementID).find("text") != null) {
                    for (var i = 0; i < animationData.eO.length; i++) {
                        if (animationData.eO[i].grNm == AnimationEffect.Sequence) {
                            switch (animationData.eO[i].nm) {
                                case EffectOptions.AsOneObject:
                                    randomAnimation2 = RandomBarsAnimation(animationData, elementData);
                                    randomAnimation2.percent = 99.9999999999999999;
                                    exit1 = TweenLite.to(randomAnimation2, animationData.dA, { percent: 0 });
                                    fromValue = {};
                                    toValue = { onComplete: onComplete };
                                    AnimationHelpers.setOptionsValue(animationData.eO, elementData, toValue, fromValue);
                                    exit2 = TweenLite.fromTo(elementID, animationData.dA, fromValue, toValue);
                                    if (timeline != null) {
                                        let endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                                        timeline.add(exit1, endTime);
                                        timeline.add(exit2, endTime);
                                    }
                                    break;
                                case EffectOptions.ByParagraph:
                                    randomAnimation2 = RandomBarsAnimation(animationData, elementData);
                                    randomAnimation2.percent = 99.9999;
                                    exit1 = TweenLite.to(randomAnimation2, animationData.dA, { percent: 0 });
                                    var text = $(elementID).find("text");
                                    fromValue = { autoAlpha: 1 };
                                    toValue = { autoAlpha: 0, onCompleteAll: onComplete };
                                    AnimationHelpers.setEnterSqeValue(animationData.eO, elementData, toValue, fromValue);
                                    let exit2Sta = TweenMax.staggerFromTo(text, elementData.tim.dT / text.length, fromValue, toValue, elementData.tim.dT / text.length);
                                    if (timeline != null) {
                                        let endTime = elementData.tim.sT + elementData.tim.dT - animationData.dA || 0;
                                        timeline.add(exit1, endTime);
                                        timeline.add(exit2Sta, endTime);
                                    }
                                    break;
                            }
                        }
                    }
                }
                break;
        }
    }

})();

//Hỗ trợ các xử lý cho hiệu ứng
var AnimationHelpers = (function() {
    //Lấy dữ liệu slide hiện tại
    function getCurrentslideData() {
        if (GlobalResources.CurrentSlide != null) {
            return GlobalResources.CurrentSlide.Data;
        }
        return null;
    }
    //Lấy dữ liệu cấu hình enter
    function setEnterOption(option, elementData, fromValue, toValue) {
        let currentSlideData = getCurrentslideData();
        let slideSize = { w: currentSlideData.w || 0.0, h: currentSlideData.h || 0.0 };
        switch (option) {
            case EffectOptions.Left:
                fromValue.left = -elementData.w;
                toValue.left = elementData.l;
                break;
            case EffectOptions.Right:
                fromValue.left = slideSize.w;
                toValue.left = elementData.l;
                break;
            case EffectOptions.Top:
                fromValue.top = -elementData.h;
                toValue.top = elementData.T;
                break;
            case EffectOptions.TopLeft:
                fromValue.top = -elementData.h;
                toValue.top = elementData.T;
                fromValue.left = -elementData.w;
                toValue.left = elementData.l;
                break;
            case EffectOptions.TopRight:
                fromValue.top = -elementData.h;
                toValue.top = elementData.T;
                fromValue.left = slideSize.w;
                toValue.left = elementData.l;
                break;
            case EffectOptions.Bottom:
                fromValue.top = slideSize.h + 2;
                toValue.top = elementData.T;
                break;
            case EffectOptions.BottomLeft:
                fromValue.top = slideSize.h;
                toValue.top = elementData.T;
                fromValue.left = -elementData.w;
                toValue.left = elementData.l;
                break;
            case EffectOptions.BottomRight:
                fromValue.top = slideSize.h;
                toValue.top = elementData.T;
                fromValue.left = slideSize.w;
                toValue.left = elementData.l;
                break;
        }
    }

    //Lấy dữ liệu cấu hình Amount
    function setAmountOption(option, elementData, fromValue, toValue) {
        let currentSlideData = getCurrentslideData();
        let slideSize = { w: currentSlideData.w || 0.0, h: currentSlideData.h || 0.0 };
        let addOrSub = toValue.rotation === undefined ? -1 : 1;
        switch (option) {
            case EffectOptions.Quarter:
                fromValue.rotation = elementData.agl;
                toValue.rotation = elementData.agl + addOrSub * 90;
                break;
            case EffectOptions.Half:
                fromValue.rotation = elementData.agl;
                toValue.rotation = elementData.agl + addOrSub * 180;
                break;
            case EffectOptions.Full:
                fromValue.rotation = elementData.agl;
                toValue.rotation = elementData.agl + addOrSub * 360;
                break;
            case EffectOptions.Two:
                fromValue.rotation = elementData.agl;
                toValue.rotation = elementData.agl + addOrSub * 720;
                break;
        }
    }

    //Lấy dữ liệu cấu hình Direction
    function setDirectionOption(option, elementData, fromValue, toValue) {
        let currentSlideData = getCurrentslideData();
        let slideSize = { w: currentSlideData.w || 0.0, h: currentSlideData.h || 0.0 };
        switch (option) {
            case EffectOptions.Up:
                fromValue.top = elementData.T + 50;
                toValue.top = elementData.T;
                break;
            case EffectOptions.Down:
                fromValue.top = elementData.T - 100;
                toValue.top = elementData.T;
                break;
            case EffectOptions.Clockwise:
                if (toValue.rotation == undefined) {
                    toValue.rotation = 1;
                }
                break;
            case EffectOptions.Counterclockwise:
                if (toValue.rotation === undefined) {
                    toValue.rotation = 1;
                    toValue.rotation = elementData.agl - (toValue.rotation - elementData.agl);
                }
                break;
        }
    }

    //Lấy dữ liệu cấu hình Vanishing Point
    function setVanishingPointOption(option, elementData, fromValue, toValue) {
        let currentSlideData = getCurrentslideData();
        let slideSize = { w: currentSlideData.w || 0.0, h: currentSlideData.h || 0.0 };
        switch (option) {
            case EffectOptions.SlideCenter:
                fromValue.left = elementData.l - elementData.w;
                toValue.left = elementData.l;
                break;
        }
    }

    function setZoomOption(option, elementData, fromValue, toValue) {
        let currentSlideData = getCurrentslideData();
        let slideSize = { w: currentSlideData.w || 0.0, h: currentSlideData.h || 0.0 };
        switch (option) {
            case EffectOptions.SlideCenter:
                fromValue.left = currentSlideData.w / 2 - elementData.w / 2;
                fromValue.top = currentSlideData.h / 2 - elementData.h / 2;
                toValue.left = elementData.l;
                toValue.top = elementData.T;
                break;
        }
    }

    return {
        setOptionsValue: function(options, elementData, fromValue, toValue) {
            if (options != null) {
                options.forEach(function(item) {
                    if (item.grNm == "Enter") {
                        setEnterOption(item.nm, elementData, fromValue, toValue);
                    } else if (item.grNm == "Amount") {
                        setAmountOption(item.nm, elementData, fromValue, toValue);
                    } else if (item.grNm == "Direction") {
                        setDirectionOption(item.nm, elementData, fromValue, toValue);
                    } else if (item.grNm == "VanishingPoint") {
                        setVanishingPointOption(item.nm, elementData, fromValue, toValue);
                    } else if (item.grNm == "Zoom") {
                        setZoomOption(item.nm, elementData, fromValue, toValue);
                    }
                })
            }
        },

        setEnterSqeValue: function(option, elementData, fromValue, toValue) {
            let currentSlideData = getCurrentslideData();
            let slideSize = { w: currentSlideData.w || 0.0, h: currentSlideData.h || 0.0 };
            if (option != null) {
                option.forEach(function(item) {
                    if (item.grNm == "Enter") {
                        switch (item.nm) {
                            case EffectOptions.Left:
                                fromValue.x = -slideSize.w;
                                toValue.x = 0;
                                break;
                            case EffectOptions.Right:
                                fromValue.x = slideSize.w;
                                toValue.x = 0;
                                break;
                            case EffectOptions.Top:
                                fromValue.y = -slideSize.h;
                                toValue.y = 0;
                                break;
                            case EffectOptions.TopLeft:
                                fromValue.y = -slideSize.h;
                                toValue.y = 0;
                                fromValue.x = -slideSize.w;
                                toValue.x = 0;
                                break;
                            case EffectOptions.TopRight:
                                fromValue.y = -slideSize.h;
                                toValue.y = 0;
                                fromValue.x = slideSize.w;
                                toValue.x = 0;
                                break;
                            case EffectOptions.Bottom:
                                fromValue.y = slideSize.h;
                                toValue.y = 0;
                                break;
                            case EffectOptions.BottomLeft:
                                fromValue.y = slideSize.h;
                                toValue.y = 0;
                                fromValue.x = -slideSize.w;
                                toValue.x = 0;
                                break;
                            case EffectOptions.BottomRight:
                                fromValue.y = slideSize.h;
                                toValue.y = 0;
                                fromValue.x = slideSize.w;
                                toValue.x = 0;
                                break;
                        }
                    } else if (item.grNm == "Direction") {
                        switch (item.nm) {
                            case EffectOptions.Up:
                                fromValue.y = 50;
                                toValue.y = 0;
                                break;
                            case EffectOptions.Down:
                                fromValue.y = 0;
                                toValue.y = 50;
                                break;
                            case EffectOptions.Clockwise:
                                toValue.rotation = 1;
                                break;
                            case EffectOptions.Counterclockwise:
                                toValue.rotation = -1;
                                break;
                        }
                    } else if (item.grNm == "Zoom") {
                        switch (item.nm) {
                            case EffectOptions.SlideCenter:
                                fromValue.x = currentSlideData.w / 2 - elementData.w / 2;
                                fromValue.h = currentSlideData.h / 2 - elementData.h / 2;
                                toValue.x = elementData.l;
                                toValue.h = elementData.T;
                                break;
                        }
                    }
                })
            }
        }
    }
})();


/*Các hiệu ứng không hỗ trợ bởi Greensock*/
function WheelAnimation(animationData, elementData) {

    let _currentAngle = 0;
    let wrapper = $("#" + elementData.id);
    let path = null,
        svg = null;
    let width = elementData.w;
    let height = elementData.h;
    let spokes = getSpokes(animationData);

    let r = Math.sqrt(Math.pow(width, 2) / 4 + Math.pow(height, 2) / 4);
    let x = width / 2,
        y = 0 - r + height / 2;
    let xt = width / 2;
    let yt = height / 2;
    let t = width / height;
    let t1 = height / width;
    //Hàm vẽ đường Clip theo góc quay alpha
    function drawShapeEn(alpha) {
        if (alpha == 360) {
            $(svg).remove();
            path = null;
            $(wrapper).css("clipPath", "");
            return;
        }
        let large = 0;
        if (alpha > 180)
            large = 1;
        let str = "";
        alpha /= spokes;
        for (var i = 0; i <= spokes; i++) {
            let x1 = xt + (x - xt) * Math.cos((alpha + 360 * i / spokes) / 180 * Math.PI) - (y - yt) * Math.sin((alpha + 360 * i / spokes) / 180 * Math.PI);
            let y1 = yt + (x - xt) * Math.sin((alpha + 360 * i / spokes) / 180 * Math.PI) + (y - yt) * Math.cos((alpha + 360 * i / spokes) / 180 * Math.PI);
            str += "M " + xt + " " + yt + " L " + PointStart(i).x + " " + PointStart(i).y + " " +
                " A " + r + " " + r + " " + (alpha / 180 * Math.PI) + " " + large + " 1 " +
                " " + x1 + " " + y1 + " Z";
        }

        if (path != null && str != null)
            path.setAttribute("d", str);
    }

    function drawShapeEx(alpha) {
        if (alpha == 360) {
            $(svg).remove();
            path = null;
            $(wrapper).css("clipPath", "");
            return;
        }
        let large = 1;
        if (alpha > 180)
            large = 0;
        let str = "";
        alpha /= spokes;
        for (var i = 0; i <= spokes; i++) {
            let x1 = xt + (x - xt) * Math.cos((alpha + 360 * i / spokes) / 180 * Math.PI) - (y - yt) * Math.sin((alpha + 360 * i / spokes) / 180 * Math.PI);
            let y1 = yt + (x - xt) * Math.sin((alpha + 360 * i / spokes) / 180 * Math.PI) + (y - yt) * Math.cos((alpha + 360 * i / spokes) / 180 * Math.PI);
            str += "M " + xt + " " + yt + " L " + PointStart(i).x + " " + PointStart(i).y + " " +
                " A " + r + " " + r + " " + (alpha / 180 * Math.PI) + " " + large + " 0 " +
                " " + x1 + " " + y1 + " Z";
        }

        if (path != null && str != null)
            path.setAttribute("d", str);
    }

    //Lấy số góc cần quay
    function getSpokes(animationData) {
        for (var i = 0; i < animationData.eO.length; i++) {
            let group = animationData.eO[i];
            if (group.grNm == "Spokes") {
                switch (group.nm) {
                    case "OneSpoke":
                        return 1;
                    case "TwoSpokes":
                        return 2;
                    case "ThreeSpokes":
                        return 3;
                    case "FourSpokes":
                        return 4;
                    case "EightSpokes":
                        return 8;
                }
            }
        }
        return 1;
    }

    //Lưu trữ giá trị điểm
    function Point(x, y) {
        this.x = x;
        this.y = y;
    }

    //Tính điểm bắt đầu
    function PointStart(index) {
        var pointStart = new Point(0, 0);
        pointStart.x = xt + (x - xt) * Math.cos(360 * index / spokes / 180 * Math.PI) - (y - yt) * Math.sin(360 * index / spokes / 180 * Math.PI);
        pointStart.y = yt + (x - xt) * Math.sin(360 * index / spokes / 180 * Math.PI) + (y - yt) * Math.cos(360 * index / spokes / 180 * Math.PI);
        return pointStart;
    }

    //Khởi tạo ban đầu, tạo lớp phủ Clip nếu chưa có
    function init() {
        path = document.getElementById("awh_p1_" + elementData.id);
        if (path == null) {
            let xmlNs = "http://www.w3.org/2000/svg";
            let parent = $(wrapper).parent();
            svg = document.createElementNS(xmlNs, 'svg');
            svg.setAttributeNS(null, "class", "abc");
            svg.setAttributeNS(null, "id", "awh_svg");

            svg.setAttribute("width", "" + width);
            svg.setAttribute("height", "" + height);
            let clipPath = document.createElementNS(xmlNs, 'clipPath');
            clipPath.setAttributeNS(null, "id", "awh_clp1_" + elementData.id);
            let path = document.createElementNS(xmlNs, 'path');
            path.setAttributeNS(null, "id", "awh_p1_" + elementData.id);
            clipPath.appendChild(path);
            svg.appendChild(clipPath);

            $(wrapper).css("clipPath", "url('#" + clipPath.id + "')");
            path = path;
            $(parent).append(svg);
            clipPath.InnerHTML = clipPath.OuterHTML;
        }
    }

    return {
        get angle() {
            return _currentAngle;
        },
        set angle(value) {
            _currentAngle = value;
            init();
            if (animationData.typ == "Entrance") {
                drawShapeEn(_currentAngle);
            } else if (animationData.typ == "Exit") {
                drawShapeEx(_currentAngle);
                if (_currentAngle == 360) {
                    let idObj = "#" + elementData.id;
                    $(idObj).css("display", "none");
                }
            }
        }
    }
}

//Hiệu ứng hình dấu cộng
function PlusAnimation(animationData, elementData) {
    let x1 = 0,
        x2 = 0,
        y1 = 0,
        y2 = 0; //4 điểm điều khiển
    let thickness = elementData.shs.pOb[0].sTh / 2 || 0;
    let width = elementData.w;
    let height = elementData.h;
    let wrapper = $("#" + elementData.id);
    let path = null,
        svg = null;
    let _percent = 0;
    let isInput = null;

    //Vẽ theo kiểu đi ra
    function drawShapeOut(percent) {
        if (percent == 100) {
            $(svg).remove();
            path = null;
            $(wrapper).css("clipPath", "");
            return;
        }

        x1 = width * (100 - percent) / 200;
        x2 = width * (100 + percent) / 200;
        y1 = height * (100 - percent) / 200;
        y2 = height * (100 + percent) / 200;
        let str = "M" + x1 + " " + (-thickness) + " " + x2 + " " + (-thickness) + " " + x2 + " " + y1 + " " + (width + thickness) + " " + y1 + " " + (width + thickness) + " " + y2 + " " + x2 + " " + y2 + " " +
            x2 + " " + (height + thickness) + " " + x1 + " " + (height + thickness) + " " + x1 + " " + y2 + " " + (-thickness) + " " + y2 + " " + (-thickness) + " " + y1 + " " + x1 + " " + y1 + "z";

        if (path != null && str != null)
            path.setAttribute("d", str);
    }

    //Vẽ theo kiểu đi vào
    function drawShapeIn(percent) {
        if (percent == 100) {
            $(svg).remove();
            path = null;
            $(wrapper).css("clipPath", "");
            return;
        }

        x1 = percent * width / 200;
        x2 = width - x1;
        y1 = percent * height / 200;
        y2 = height - y1;
        let str = "M" + (-thickness) + " " + (-thickness) + " " + x1 + " " + (-thickness) + " " + x1 + " " + y1 + " " + (-thickness) + " " + y1 + " z " +
            "M" + (width + thickness) + " " + (-thickness) + " " + (width + thickness) + " " + y1 + " " + x2 + " " + y1 + " " + x2 + " " + (-thickness) + "z " +
            "M" + (width + thickness) + " " + y2 + " " + (width + thickness) + " " + (height + thickness) + " " + x2 + " " + (height + thickness) + " " + x2 + " " + y2 + "z " +
            "M" + x1 + " " + y2 + " " + x1 + " " + (height + thickness) + " " + (-thickness) + " " + (height + thickness) + " " + (-thickness) + " " + y2 + "z";

        if (path != null && str != null)
            path.setAttribute("d", str);
    }

    function init() {
        path = document.getElementById("awh_p1_" + elementData.id);
        if (path == null) {
            let xmlNs = "http://www.w3.org/2000/svg";
            let parent = $(wrapper).parent();
            svg = document.createElementNS(xmlNs, 'svg');
            svg.setAttributeNS(null, "class", "abc");
            svg.setAttributeNS(null, "id", "awh_svg");

            svg.setAttribute("width", "" + width);
            svg.setAttribute("height", "" + height);
            let clipPath = document.createElementNS(xmlNs, 'clipPath');
            clipPath.setAttributeNS(null, "id", "awh_clp1_" + elementData.id);
            path = document.createElementNS(xmlNs, 'path');
            path.setAttributeNS(null, "id", "awh_p1_" + elementData.id);
            clipPath.appendChild(path);
            svg.appendChild(clipPath);

            $(wrapper).css("clipPath", "url('#" + clipPath.id + "')");
            $(parent).append(svg);
            clipPath.InnerHTML = clipPath.OuterHTML;
        }
    }

    return {
        get percent() {
            return _percent;
        },
        set percent(value) {
            _percent = value;
            init();
            if (animationData.typ == "Entrance") {
                for (var i = 0; i < animationData.eO.length; i++) {
                    let group = animationData.eO[i];
                    if (group.grNm == AnimationEffect.Direction) {
                        switch (group.nm) {
                            case EffectOptions.In:
                                drawShapeIn(_percent);
                                break;
                            case EffectOptions.Out:
                                drawShapeOut(_percent);
                                break;
                        }
                    }
                }
            } else if (animationData.typ == "Exit") {
                for (var i = 0; i < animationData.eO.length; i++) {
                    let group = animationData.eO[i];
                    if (group.grNm == AnimationEffect.Direction) {
                        switch (group.nm) {
                            case EffectOptions.In:
                                drawShapeOut(_percent);
                                break;
                            case EffectOptions.Out:
                                drawShapeIn(_percent);
                                break;
                        }
                    }
                }
            }
        }
    }
}

//Hiệu ứng hình thoi
function DiamonAnimation(animationData, elementData) {
    let x1 = 0,
        x2, x3 = 0,
        x4 = 0,
        x5 = 0,
        y1 = 0,
        y2 = 0,
        y3 = 0,
        y4 = 0,
        y5 = 0;
    let thickness = elementData.shs.pOb[0].sTh / 2 || 0;
    let width = elementData.w;
    let height = elementData.h;
    let wrapper = $("#" + elementData.id);
    let path = null,
        svg = null;
    let _percent = 0;
    let isInput = null;

    //Tính các điểm cố định ban đầu
    x1 = -width / 2;
    x3 = elementData.w / 2;
    x5 = width + width / 2;
    y1 = -height / 2;
    y3 = elementData.h / 2;
    y5 = height + height / 2;

    //Vẽ theo kiểu đi ra
    function drawShapeOut(percent) {
        if (percent == 100) {
            $(svg).remove();
            path = null;
            $(wrapper).css("clipPath", "");
            return;
        }

        x1 = width / 2 - percent / 100 * width - thickness;
        x2 = width / 2 + percent / 100 * width + thickness;
        y1 = height / 2 - percent / 100 * height - thickness;
        y2 = height / 2 + percent / 100 * height + thickness;
        let str = "M" + x1 + " " + y3 + " " + x3 + " " + y1 + " " + x2 + " " + y3 + " " + x3 + " " + y2 + "z";

        if (path != null && str != null)
            path.setAttribute("d", str);
    }

    //Vẽ theo kiểu đi vào
    function drawShapeIn(percent) {
        if (percent == 100) {
            $(svg).remove();
            path = null;
            $(wrapper).css("clipPath", "");
            return;
        }

        x2 = x1 + percent * width / 100;
        x4 = x5 - percent * width / 100;
        y2 = y1 + percent * height / 100;
        y4 = y5 - percent * height / 100;

        let str = "M" + x1 + " " + y1 + " " + x5 + " " + y1 + " " + x5 + " " + y5 + " " + x1 + " " + y5 + "z" +
            "M" + x2 + " " + y3 + " " + x3 + " " + y4 + " " + x4 + " " + y3 + " " + x3 + " " + y2 + "z";
        if (path != null && str != null)
            path.setAttribute("d", str);
    }

    function init() {
        path = document.getElementById("awh_p1_" + elementData.id);
        if (path == null) {
            let xmlNs = "http://www.w3.org/2000/svg";
            let parent = $(wrapper).parent();
            svg = document.createElementNS(xmlNs, 'svg');
            svg.setAttributeNS(null, "class", "abc");
            svg.setAttributeNS(null, "id", "awh_svg");

            svg.setAttribute("width", "" + width);
            svg.setAttribute("height", "" + height);
            let clipPath = document.createElementNS(xmlNs, 'clipPath');
            clipPath.setAttributeNS(null, "id", "awh_clp1_" + elementData.id);
            path = document.createElementNS(xmlNs, 'path');
            path.setAttributeNS(null, "id", "awh_p1_" + elementData.id);
            clipPath.appendChild(path);
            svg.appendChild(clipPath);

            $(wrapper).css("clipPath", "url('#" + clipPath.id + "')");
            $(parent).append(svg);
            clipPath.InnerHTML = clipPath.OuterHTML;
        }
    }

    return {
        get percent() {
            return _percent;
        },
        set percent(value) {
            _percent = value;
            init();
            if (animationData.typ == "Entrance") {
                for (var i = 0; i < animationData.eO.length; i++) {
                    let group = animationData.eO[i];
                    if (group.grNm == AnimationEffect.Direction) {
                        switch (group.nm) {
                            case EffectOptions.In:
                                drawShapeIn(_percent);
                                break;
                            case EffectOptions.Out:
                                drawShapeOut(_percent);
                                break;
                        }
                    }
                }
            } else if (animationData.typ == "Exit") {
                for (var i = 0; i < animationData.eO.length; i++) {
                    let group = animationData.eO[i];
                    if (group.grNm == AnimationEffect.Direction) {
                        switch (group.nm) {
                            case EffectOptions.In:
                                drawShapeOut(_percent);
                                break;
                            case EffectOptions.Out:
                                drawShapeIn(_percent);
                                break;
                        }
                    }
                }
            }
        }
    }

}

//Hiệu ứng hình vuông
function BoxAnimation(animationData, elementData) {
    let x1 = 0,
        x2 = 0,
        x3 = 0,
        x4 = 0,
        y1 = 0,
        y2 = 0,
        y3 = 0,
        y4 = 0;
    let thickness = elementData.shs.pOb[0].sTh / 2 || 0;
    let width = elementData.w;
    let height = elementData.h;
    let wrapper = $("#" + elementData.id);
    let path = null,
        svg = null;
    let _percent = 0;
    let isInput = null;

    //Tính các điểm cố định ban đầu
    x1 = -thickness;
    x4 = width + thickness;
    y1 = -thickness;
    y4 = height + thickness;

    //Vẽ theo kiểu đi ra
    function drawShapeOut(percent) {
        if (percent == 100) {
            $(svg).remove();
            path = null;
            $(wrapper).css("clipPath", "");
            return;
        }

        x1 = width / 2 - percent / 100 * width - thickness;
        x2 = width / 2 + percent / 100 * width + thickness;
        y1 = height / 2 - percent / 100 * height - thickness;
        y2 = height / 2 + percent / 100 * height + thickness;
        let str = "M" + x1 + " " + y1 + " " + x2 + " " + y1 + " " + x2 + " " + y2 + " " + x1 + " " + y2 + "z";

        if (path != null && str != null)
            path.setAttribute("d", str);
    }

    //Vẽ theo kiểu đi vào
    function drawShapeIn(percent) {
        if (percent == 100) {
            $(svg).remove();
            path = null;
            $(wrapper).css("clipPath", "");
            return;
        }

        x1 = -thickness;
        x4 = width + thickness;
        y1 = -thickness;
        y4 = height + thickness;

        x2 = -thickness + percent * width / 200;
        x3 = width + thickness - percent * width / 200;
        y2 = -thickness + percent * height / 200;
        y3 = height + thickness - percent * height / 200;

        let str = "M" + x1 + " " + y1 + " " + x4 + " " + y1 + " " + x4 + " " + y4 + " " + x1 + " " + y4 + "z" +
            "M" + x2 + " " + y2 + " " + x2 + " " + y3 + " " + x3 + " " + y3 + " " + x3 + " " + y2 + "z";

        if (path != null && str != null)
            path.setAttribute("d", str);
    }

    function init() {
        path = document.getElementById("awh_p1_" + elementData.id);
        if (path == null) {
            let xmlNs = "http://www.w3.org/2000/svg";
            let parent = $(wrapper).parent();
            svg = document.createElementNS(xmlNs, 'svg');
            svg.setAttributeNS(null, "class", "abc");
            svg.setAttributeNS(null, "id", "awh_svg");

            svg.setAttribute("width", "" + width);
            svg.setAttribute("height", "" + height);
            let clipPath = document.createElementNS(xmlNs, 'clipPath');
            clipPath.setAttributeNS(null, "id", "awh_clp1_" + elementData.id);
            path = document.createElementNS(xmlNs, 'path');
            path.setAttributeNS(null, "id", "awh_p1_" + elementData.id);
            clipPath.appendChild(path);
            svg.appendChild(clipPath);

            $(wrapper).css("clipPath", "url('#" + clipPath.id + "')");
            $(parent).append(svg);
            clipPath.InnerHTML = clipPath.OuterHTML;
        }
    }

    return {
        get percent() {
            return _percent;
        },
        set percent(value) {
            _percent = value;
            init();
            if (animationData.typ == "Entrance") {
                for (var i = 0; i < animationData.eO.length; i++) {
                    let group = animationData.eO[i];
                    if (group.grNm == AnimationEffect.Direction) {
                        switch (group.nm) {
                            case EffectOptions.In:
                                drawShapeIn(_percent);
                                break;
                            case EffectOptions.Out:
                                drawShapeOut(_percent);
                                break;
                        }
                    }
                }
            } else if (animationData.typ == "Exit") {
                for (var i = 0; i < animationData.eO.length; i++) {
                    let group = animationData.eO[i];
                    if (group.grNm == AnimationEffect.Direction) {
                        switch (group.nm) {
                            case EffectOptions.In:
                                drawShapeOut(_percent);
                                break;
                            case EffectOptions.Out:
                                drawShapeIn(_percent);
                                break;
                        }
                    }
                }
            }
        }
    }

}

//Hiệu ứng hình Tròn 
function EllipseAnimation(animationData, elementData) {
    let cx = 0,
        cy = 0,
        rx = 0,
        ry = 0,
        x1 = 0,
        x2 = 0,
        y1 = 0,
        y2 = 0;
    let thickness = elementData.shs.pOb[0].sTh / 2 || 0;
    let width = elementData.w;
    let height = elementData.h;
    let wrapper = $("#" + elementData.id);
    let path = null,
        svg = null;
    let _percent = 0;
    let isInput = null;

    //Tính các điểm cố định ban đầu
    cx = width / 2; //Tọa độ X điểm trung tâm.
    cy = height / 2; //Tọa độ Y điểm trung tâm.

    x1 = -width / 2;
    x2 = width + width / 2;
    y1 = -height / 2;
    y2 = height + height / 2;

    //Vẽ theo kiểu đi ra
    function drawShapeOut(percent) {
        if (percent == 100) {
            $(svg).remove();
            path = null;
            $(wrapper).css("clipPath", "");
            return;
        }

        rx = width * percent / 100 + thickness; //Bán kính chiều ngang.
        ry = height * percent / 100 + thickness; //Bán kính chiều dọc.
        let str = calculatePathData();

        if (path != null && str != null)
            path.setAttribute("d", str);
    }

    //Tính toán data dựa vào các điểm đặt biệt
    function calculatePathData() {
        let d = [
            "M", cx, cy,
            "m", -rx, 0,
            "a", rx, ry,
            0, 1, 0, 2 * rx, 0,
            "a", rx, ry,
            0, 1, 0, -2 * rx, 0
        ];
        return d.join(" ");
    }

    //Vẽ theo kiểu đi vào
    function drawShapeIn(percent) {
        if (percent == 100) {
            $(svg).remove();
            path = null;
            $(wrapper).css("clipPath", "");
            return;
        }

        rx = (width + thickness) * (100 - percent) / 100; //Bán kính chiều ngang.
        ry = (height + thickness) * (100 - percent) / 100; //Bán kính chiều dọc.
        let str = "M" + x1 + " " + y1 + " " + x2 + " " + y1 + " " + x2 + " " + y2 + " " + x1 + " " + y2 + "z" + calculatePathData();
        if (path != null && str != null)
            path.setAttribute("d", str);
    }

    function init() {
        path = document.getElementById("awh_p1_" + elementData.id);
        if (path == null) {
            let xmlNs = "http://www.w3.org/2000/svg";
            let parent = $(wrapper).parent();
            svg = document.createElementNS(xmlNs, 'svg');
            svg.setAttributeNS(null, "class", "abc");
            svg.setAttributeNS(null, "id", "awh_svg");

            svg.setAttribute("width", "" + width);
            svg.setAttribute("height", "" + height);
            let clipPath = document.createElementNS(xmlNs, 'clipPath');
            clipPath.setAttributeNS(null, "id", "awh_clp1_" + elementData.id);
            path = document.createElementNS(xmlNs, 'path');
            path.setAttributeNS(null, "id", "awh_p1_" + elementData.id);
            clipPath.appendChild(path);
            svg.appendChild(clipPath);

            $(wrapper).css("clipPath", "url('#" + clipPath.id + "')");
            $(parent).append(svg);
            clipPath.InnerHTML = clipPath.OuterHTML;
        }
    }

    return {
        get percent() {
            return _percent;
        },
        set percent(value) {
            _percent = value;
            init();
            if (animationData.typ == "Entrance") {
                for (var i = 0; i < animationData.eO.length; i++) {
                    let group = animationData.eO[i];
                    if (group.grNm == AnimationEffect.Direction) {
                        switch (group.nm) {
                            case EffectOptions.In:
                                drawShapeIn(_percent);
                                break;
                            case EffectOptions.Out:
                                drawShapeOut(_percent);
                                break;
                        }
                    }
                }
            } else if (animationData.typ == "Exit") {
                for (var i = 0; i < animationData.eO.length; i++) {
                    let group = animationData.eO[i];
                    if (group.grNm == AnimationEffect.Direction) {
                        switch (group.nm) {
                            case EffectOptions.In:
                                drawShapeOut(_percent);
                                break;
                            case EffectOptions.Out:
                                drawShapeIn(_percent);
                                break;
                        }
                    }
                }
            }
        }
    }

}

//Hiệu ứng hình split
function SplitAnimation(animationData, elementData) {
    let x1 = 0,
        x2 = 0,
        x3 = 0,
        x4 = 0,
        y1 = 0,
        y2 = 0,
        y3 = 0,
        y4 = 0;
    let thickness = elementData.shs.pOb[0].sTh / 2 || 0;
    let width = elementData.w;
    let height = elementData.h;
    let wrapper = $("#" + elementData.id);
    let path = null,
        svg = null;
    let _percent = 0;

    //Tính các điểm cố định ban đầu
    x1 = -thickness;
    x4 = width + thickness;
    y1 = -thickness;
    y4 = height + thickness;

    //Vẽ theo kiểu đi ra theo chiều ngang
    function drawShapeVOut(percent) {
        if (percent == 100) {
            $(svg).remove();
            path = null;
            $(wrapper).css("clipPath", "");
            return;
        }
        x2 = width / 2 - percent * (width / 2 + thickness) / 100;
        x3 = width / 2 + percent * (width / 2 + thickness) / 100;
        let str = "M" + x2 + " " + y1 + " " + x3 + " " + y1 + " " + x3 + " " + y4 + " " + x2 + " " + y4 + "z";

        if (path != null && str != null)
            path.setAttribute("d", str);
    }

    //Vẽ theo kiểu đi vào theo chiều ngang
    function drawShapeVIn(percent) {
        if (percent == 100) {
            $(svg).remove();
            path = null;
            $(wrapper).css("clipPath", "");
            return;
        }
        x2 = width / 2 - (100 - percent) * (width / 2 + thickness) / 100;
        x3 = width / 2 + (100 - percent) * (width / 2 + thickness) / 100;
        let str = "M" + x1 + " " + y1 + " " + x2 + " " + y1 + " " + x2 + " " + y4 + " " + x1 + " " + y4 + "z" +
            "M" + x3 + " " + y1 + " " + x4 + " " + y1 + " " + x4 + " " + y4 + " " + x3 + " " + y4 + "z";
        if (path != null && str != null)
            path.setAttribute("d", str);
    }

    //Vẽ theo kiểu đi ra theo chiều dọc
    function drawShapeHOut(percent) {
        if (percent == 100) {
            $(svg).remove();
            path = null;
            $(wrapper).css("clipPath", "");
            return;
        }
        y2 = height / 2 - percent * (height / 2 + thickness) / 100;
        y3 = height / 2 + percent * (height / 2 + thickness) / 100;
        let str = "M" + x1 + " " + y2 + " " + x4 + " " + y2 + " " + x4 + " " + y3 + " " + x1 + " " + y3 + "z";

        if (path != null && str != null)
            path.setAttribute("d", str);
    }

    //Vẽ theo kiểu đi vào theo chiều dọc
    function drawShapeHIn(percent) {
        if (percent == 100) {
            $(svg).remove();
            path = null;
            $(wrapper).css("clipPath", "");
            return;
        }
        y2 = height / 2 - (100 - percent) * (height / 2 + thickness) / 100;
        y3 = height / 2 + (100 - percent) * (height / 2 + thickness) / 100;
        let str = "M" + x1 + " " + y1 + " " + x4 + " " + y1 + " " + x4 + " " + y2 + " " + x1 + " " + y2 + "z" +
            "M" + x1 + " " + y3 + " " + x4 + " " + y3 + " " + x4 + " " + y4 + " " + x1 + " " + y4 + "z";
        if (path != null && str != null)
            path.setAttribute("d", str);
    }

    function init() {
        path = document.getElementById("awh_p1_" + elementData.id);
        if (path == null) {
            let xmlNs = "http://www.w3.org/2000/svg";
            let parent = $(wrapper).parent();
            svg = document.createElementNS(xmlNs, 'svg');
            svg.setAttributeNS(null, "class", "abc");
            svg.setAttributeNS(null, "id", "awh_svg");

            svg.setAttribute("width", "" + width);
            svg.setAttribute("height", "" + height);
            let clipPath = document.createElementNS(xmlNs, 'clipPath');
            clipPath.setAttributeNS(null, "id", "awh_clp1_" + elementData.id);
            path = document.createElementNS(xmlNs, 'path');
            path.setAttributeNS(null, "id", "awh_p1_" + elementData.id);
            clipPath.appendChild(path);
            svg.appendChild(clipPath);

            $(wrapper).css("clipPath", "url('#" + clipPath.id + "')");
            $(parent).append(svg);
            clipPath.InnerHTML = clipPath.OuterHTML;
        }
    }

    function drawShape(percent) {
        if (animationData.typ == "Entrance") {
            for (var i = 0; i < animationData.eO.length; i++) {
                let group = animationData.eO[i];
                if (group.grNm == AnimationEffect.Direction) {
                    switch (group.nm) {
                        case EffectOptions.VerticalIn:
                            drawShapeVIn(percent);
                            break;
                        case EffectOptions.VerticalOut:
                            drawShapeVOut(percent);
                            break;
                        case EffectOptions.HorizontalIn:
                            drawShapeHIn(percent);
                            break;
                        case EffectOptions.HorizontalOut:
                            drawShapeHOut(percent);
                            break;
                        default:
                            drawShapeVIn(percent);
                            break;
                    }
                }
            }
        } else if (animationData.typ == "Exit") {
            for (var i = 0; i < animationData.eO.length; i++) {
                let group = animationData.eO[i];
                if (group.grNm == AnimationEffect.Direction) {
                    switch (group.nm) {
                        case EffectOptions.VerticalIn:
                            drawShapeVOut(percent);
                            break;
                        case EffectOptions.VerticalOut:
                            drawShapeVIn(percent);
                            break;
                        case EffectOptions.HorizontalIn:
                            drawShapeHOut(percent);
                            break;
                        case EffectOptions.HorizontalOut:
                            drawShapeHIn(percent);
                            break;
                        default:
                            drawShapeVIn(percent);
                            break;
                    }
                }
            }
        }
    }

    return {
        get percent() {
            return _percent;
        },
        set percent(value) {
            _percent = value;
            init();
            drawShape(_percent);
        }
    }

}

//Hiệu ứng hình wipe
function WipeAnimation(animationData, elementData) {
    let x1 = 0,
        x2 = 0,
        y1 = 0,
        y2 = 0;
    let thickness = elementData.shs.pOb[0].sTh / 2 || 0;
    let width = elementData.w;
    let height = elementData.h;
    let wrapper = $("#" + elementData.id);
    let path = null,
        svg = null;
    let _percent = 0;

    //Vẽ theo kiểu đi từ phải sang
    function drawShapeLeft(percent) {
        if (percent == 100) {
            $(svg).remove();
            path = null;
            $(wrapper).css("clipPath", "");
            return;
        }

        x1 = -thickness;
        y1 = -thickness;
        y2 = height + thickness;
        x2 = -thickness + percent * (width + 2 * thickness) / 100;

        let str = "M" + x1 + " " + y1 + " " + x2 + " " + y1 + " " + x2 + " " + y2 + " " + x1 + " " + y2 + "z";

        if (path != null && str != null)
            path.setAttribute("d", str);
    }

    //Vẽ theo kiểu đi từ trái sang
    function drawShapeRight(percent) {
        if (percent == 100) {
            $(svg).remove();
            path = null;
            $(wrapper).css("clipPath", "");
            return;
        }

        x1 = (width + thickness) - percent * (width + 2 * thickness) / 100;
        y1 = -thickness;
        x2 = width + thickness;
        y2 = height + thickness;

        let str = "M" + x2 + " " + y1 + " " + x2 + " " + y2 + " " + x1 + " " + y2 + " " + x1 + " " + y1 + "z";

        if (path != null && str != null)
            path.setAttribute("d", str);
    }

    //Vẽ theo kiểu đi từ trên xuống
    function drawShapeTop(percent) {
        if (percent == 100) {
            $(svg).remove();
            path = null;
            $(wrapper).css("clipPath", "");
            return;
        }

        x1 = -thickness;
        y1 = -thickness;
        x2 = width + thickness;
        y2 = -thickness + percent * (height + 2 * thickness) / 100;

        let str = "M" + x1 + " " + y1 + " " + x2 + " " + y1 + " " + x2 + " " + y2 + " " + x1 + " " + y2 + "z";

        if (path != null && str != null)
            path.setAttribute("d", str);
    }

    //Vẽ theo kiểu đi từ dưới lên
    function drawShapeBottom(percent) {
        if (percent == 100) {
            $(svg).remove();
            path = null;
            $(wrapper).css("clipPath", "");
            return;
        }

        y1 = (height + thickness) - percent * (height + 2 * thickness) / 100;
        x1 = -thickness;
        x2 = width + thickness;
        y2 = height + thickness;

        let str = "M" + x2 + " " + y1 + " " + x2 + " " + y2 + " " + x1 + " " + y2 + " " + x1 + " " + y1 + "z";

        if (path != null && str != null)
            path.setAttribute("d", str);
    }

    function init() {
        path = document.getElementById("awh_p1_" + elementData.id);
        if (path == null) {
            let xmlNs = "http://www.w3.org/2000/svg";
            let parent = $(wrapper).parent();
            svg = document.createElementNS(xmlNs, 'svg');
            svg.setAttributeNS(null, "class", "abc");
            svg.setAttributeNS(null, "id", "awh_svg");

            svg.setAttribute("width", "" + width);
            svg.setAttribute("height", "" + height);
            let clipPath = document.createElementNS(xmlNs, 'clipPath');
            clipPath.setAttributeNS(null, "id", "awh_clp1_" + elementData.id);
            path = document.createElementNS(xmlNs, 'path');
            path.setAttributeNS(null, "id", "awh_p1_" + elementData.id);
            clipPath.appendChild(path);
            svg.appendChild(clipPath);

            $(wrapper).css("clipPath", "url('#" + clipPath.id + "')");
            $(parent).append(svg);
            clipPath.InnerHTML = clipPath.OuterHTML;
        }
    }

    function drawShape(percent) {
        if (animationData.typ == "Entrance") {
            for (var i = 0; i < animationData.eO.length; i++) {
                let group = animationData.eO[i];
                if (group.grNm == AnimationEffect.Direction) {
                    switch (group.nm) {
                        case EffectOptions.Bottom:
                            drawShapeBottom(percent);
                            break;
                        case EffectOptions.Left:
                            drawShapeLeft(percent);
                            break;
                        case EffectOptions.Right:
                            drawShapeRight(percent);
                            break;
                        case EffectOptions.Top:
                            drawShapeTop(percent);
                            break;
                        default:
                            drawShapeBottom(percent);
                            break;
                    }
                }
            }
        } else if (animationData.typ == "Exit") {
            for (var i = 0; i < animationData.eO.length; i++) {
                let group = animationData.eO[i];
                if (group.grNm == AnimationEffect.Direction) {
                    switch (group.nm) {
                        case EffectOptions.Bottom:
                            drawShapeTop(percent);
                            break;
                        case EffectOptions.Left:
                            drawShapeRight(percent);
                            break;
                        case EffectOptions.Right:
                            drawShapeLeft(percent);
                            break;
                        case EffectOptions.Top:
                            drawShapeBottom(percent);
                            break;
                        default:
                            drawShapeTop(percent);
                            break;
                    }
                }
            }
        }
    }

    return {
        get percent() {
            return _percent;
        },
        set percent(value) {
            _percent = value;
            init();
            drawShape(_percent);
        }
    }

}

//Hiệu ứng hình random bars
function RandomBarsAnimation(animationData, elementData) {
    let x1 = 0,
        x2 = 0,
        y1 = 0,
        y2 = 0;
    let thickness = elementData.shs.pOb[0].sTh / 2 || 0;
    let width = elementData.w;
    let height = elementData.h;
    let wrapper = $("#" + elementData.id);
    let path = null,
        svg = null;
    let _percent = 0;
    let preNum = 0;
    let radomPointsV = getVPoints();
    let radomPointsH = getHPoints();

    //Random theo chiều ngang
    function drawShapeH(percent) {
        if (percent == 100) {
            $(svg).remove();
            path = null;
            $(wrapper).css("clipPath", "");
            return;
        }

        let numOfPoint = Math.floor(percent * (radomPointsH.length) / 100);
        if (numOfPoint == preNum)
            return;
        preNum = numOfPoint;

        let str = "";
        for (var i = 0; i < numOfPoint - 1; i++) {
            //str += (` M ${-thickness} ${radomPointsH[i]} h ${width + 2 * thickness} v 1 h ${-width - 2 * thickness}z`);
            str += 'M ' + -thickness + ' ' + radomPointsH[i] + ' h ' + (width + 2 * thickness) + ' v 1 h ' + (-width - 2 * thickness) + 'z';
        }

        if (path != null && str != null)
            path.setAttribute("d", str);
    }

    //Random theo chiều dọc
    function drawShapeV(percent) {
        if (percent == 100) {
            $(svg).remove();
            path = null;
            $(wrapper).css("clipPath", "");
            return;
        }

        let numOfPoint = Math.floor(percent * (radomPointsV.length) / 100);
        if (numOfPoint == preNum)
            return;
        preNum = numOfPoint;

        let str = "";
        for (var i = 0; i < numOfPoint - 1; i++) {
            //str += (` M ${radomPointsV[i]} ${-thickness} h 1 v ${height + 2 * thickness} h -1 z`);
            str += 'M' + radomPointsV[i] + ' ' + -thickness + ' h 1 v ' + (height + 2 * thickness) + ' h -1 ' + 'z';
        }

        if (path != null && str != null)
            path.setAttribute("d", str);
    }

    function getHPoints() {
        let points = [],
            result = [];
        for (var i = -thickness; i < height + thickness; i++) {
            points.push(i);
        }

        for (var i = 0; i < height + thickness; i++) {
            let number = getRandomInt(0, points.length - 1);
            result.push(points[number]);
            points.splice(number, 1);
        }
        return result;
    }

    function getVPoints() {
        let points = [],
            result = [];
        for (var i = -thickness; i < width + thickness; i++) {
            points.push(i);
        }

        for (var i = 0; i < width + thickness; i++) {
            let number = getRandomInt(0, points.length - 1);
            result.push(points[number]);
            points.splice(number, 1);
        }
        return result;
    }

    //Lấy số ngẫu nhiên
    function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    function init() {
        path = document.getElementById("awh_p1_" + elementData.id);
        if (path == null) {
            let xmlNs = "http://www.w3.org/2000/svg";
            let parent = $(wrapper).parent();
            svg = document.createElementNS(xmlNs, 'svg');
            svg.setAttributeNS(null, "class", "abc");
            svg.setAttributeNS(null, "id", "awh_svg");

            svg.setAttribute("width", "" + width);
            svg.setAttribute("height", "" + height);
            let clipPath = document.createElementNS(xmlNs, 'clipPath');
            clipPath.setAttributeNS(null, "id", "awh_clp1_" + elementData.id);
            path = document.createElementNS(xmlNs, 'path');
            path.setAttributeNS(null, "id", "awh_p1_" + elementData.id);
            clipPath.appendChild(path);
            svg.appendChild(clipPath);

            $(wrapper).css("clipPath", "url('#" + clipPath.id + "')");
            $(parent).append(svg);
            clipPath.InnerHTML = clipPath.OuterHTML;
        }
    }

    function drawShape(percent) {
        for (var i = 0; i < animationData.eO.length; i++) {
            let group = animationData.eO[i];
            if (group.grNm == AnimationEffect.Direction) {
                switch (group.nm) {
                    case EffectOptions.Vertical:
                        drawShapeV(percent);
                        break;
                    case EffectOptions.Horizontal:
                        drawShapeH(percent);
                        break;
                    default:
                        drawShapeV(percent);
                        break;
                }
            }
        }
    }

    return {
        get percent() {
            return _percent;
        },
        set percent(value) {
            _percent = value;
            init();
            drawShape(_percent);
        }
    }

}