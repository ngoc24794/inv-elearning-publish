'use strict';
var Text = (function () {
    function Text(options) {
        this.options = options;
    }
    Text.prototype.GenTexts = function () {
        var currentTextObj = this.options;
        var xmlNs = "http://www.w3.org/2000/svg";
        var grText = GlobalHelper.createTags('g', '', 'text');
        var grUnderLines = GlobalHelper.createTags('g', '', 'underline');
        var grStrikeThrough = GlobalHelper.createTags('g', '', 'strikeThrough');
        var grWords = GlobalHelper.createTags('g', '', 'words');
        var paragraphs = this.options;
        for (var p = 0; p < paragraphs.length; p++) {
            var text = document.createElementNS(xmlNs, "text");
            var inline = paragraphs[p].inls;
            for (var i = 0; i < inline.length; i++) {
                var tSpan = document.createElementNS(xmlNs, "tspan");
                tSpan.setAttribute("font-size", inline[i].fs);
                tSpan.setAttribute("font-family", inline[i].ff);
                tSpan.setAttribute("font-weight", inline[i].fw);
                tSpan.setAttribute("font-style", inline[i].fst);
                tSpan.setAttribute("fill", inline[i].col.col);
                tSpan.setAttribute("fill-opacity", inline[i].opt);

                if (inline[i].ad) {
                    var tHyperlink = document.createElementNS("http://www.w3.org/2000/svg", "a");
                    // tHyperlink.setAttributeNS("http://www.w3.org/2000/svg", 'href', inline[i].ad);
                    // tHyperlink.setAttributeNS("http://www.w3.org/2000/svg", 'target', '_blank');
                    var onclick = tHyperlink.getAttribute("onclick");  

                    // if onclick is not a function, it's not IE7, so use setAttribute
                    if(typeof(onclick) != "function") { 
                        tHyperlink.setAttribute('onclick','window.open("' + inline[i].ad +'");' + onclick); // for FF,IE8,Chrome
                    
                    // if onclick is a function, use the IE7 method and call onclick() in the anonymous function
                    } else {
                        tHyperlink.onclick = function() { 
                            window.open(inline[i].ad);;
                            onclick();
                        }; // for IE7
                    }
                    
                    tSpan.textContent = inline[i].Txt;
                    tSpan.setAttribute("x", inline[i].Ls.join(" "));
                    tSpan.setAttribute("y", inline[i].T);
                    var startX = inline[i].Ls[0];
                    for (var l = 0; l < inline[i].Ls.length; l++) {
                        var endX = inline[i].Ls[0] + inline[i].w;
                    }
                    if (inline[i].Ul) {
                        var underlineColor = inline[i].uCol.col;
                        grText.appendChild(grUnderLines);
                        if (inline[i].u === "Single") {
                            var underline = document.createElementNS(xmlNs, 'line');
                            var singleUnderlineHeight = inline[i].Ul.Y2 - inline[i].Ul.Y;
                            underline.style.stroke = underlineColor;
                            underline.style.strokeWidth = inline[i].Ul.W / 10;
                            underline.setAttribute("x1", inline[i].Ul.X1);
                            underline.setAttribute("y1", inline[i].Ul.Y);
                            underline.setAttribute("x2", inline[i].Ul.X2);
                            underline.setAttribute("y2", inline[i].Ul.Y);
                            grUnderLines.appendChild(underline);
                        }
                        else if (inline[i].u === "Double") {
                            var underlineColor_1 = inline[i].uCol.col;
                            var underline1 = document.createElementNS(xmlNs, 'line');
                            var underline2 = document.createElementNS(xmlNs, 'line');
                            underline1.style.stroke = underline2.style.stroke = underlineColor_1;
                            underline1.style.strokeWidth = underline2.style.strokeWidth = inline[i].Ul.W / 15;
                            underline1.setAttribute("x1", inline[i].Ul.X1);
                            underline1.setAttribute("y1", inline[i].Ul.Y - inline[i].Ul.W / 15);
                            underline1.setAttribute("x2", inline[i].Ul.X2);
                            underline1.setAttribute("y2", inline[i].Ul.Y - inline[i].Ul.W / 15);
                            underline2.setAttribute("x1", inline[i].Ul.X1);
                            underline2.setAttribute("y1", inline[i].Ul.Y + inline[i].Ul.W / 15);
                            underline2.setAttribute("x2", inline[i].Ul.X2);
                            underline2.setAttribute("y2", inline[i].Ul.Y + inline[i].Ul.W / 15);
                            grUnderLines.appendChild(underline1);
                            grUnderLines.appendChild(underline2);
                        }
                    }
                    var strikeThroughColor = inline[i].col.col;
                    if (inline[i].stk !== "None") {
                        grText.appendChild(grStrikeThrough);
                        if (inline[i].stk === "Single") {
                            var strikeThrough = document.createElementNS(xmlNs, 'line');
                            var singleStrikeThroughHeight = inline[i].fs * 72 / 96 / 10;
                            var singleStrikeThroughStartY = inline[i].T - inline[i].fs / 3 + singleStrikeThroughHeight / 2;
                            strikeThrough.style.stroke = strikeThroughColor;
                            strikeThrough.style.strokeWidth = singleStrikeThroughHeight;
                            strikeThrough.setAttribute("x1", startX);
                            strikeThrough.setAttribute("y1", singleStrikeThroughStartY);
                            strikeThrough.setAttribute("x2", endX);
                            strikeThrough.setAttribute("y2", singleStrikeThroughStartY);
                            grStrikeThrough.appendChild(strikeThrough);
                        }
                        else if (inline[i].stk === "Double") {
                            var strikeThrough1 = document.createElementNS(xmlNs, 'line');
                            var strikeThrough2 = document.createElementNS(xmlNs, 'line');
                            var doubleStrikeThroughHeight = inline[i].fs * 72 / 96 / 10;
                            var doubleStartYStrikeThrough1 = inline[i].T - inline[i].fs * 72 / 96 / 2;
                            var doubleStartYStrikeThrough2 = inline[i].T - inline[i].fs * 72 / 96 / 4;
                            strikeThrough1.style.stroke = strikeThrough2.style.stroke = strikeThroughColor;
                            strikeThrough1.style.strokeWidth = strikeThrough2.style.strokeWidth = doubleStrikeThroughHeight;
                            strikeThrough1.setAttribute("x1", startX);
                            strikeThrough1.setAttribute("y1", doubleStartYStrikeThrough1);
                            strikeThrough1.setAttribute("x2", endX);
                            strikeThrough1.setAttribute("y2", doubleStartYStrikeThrough1);
                            strikeThrough2.setAttribute("x1", startX);
                            strikeThrough2.setAttribute("y1", doubleStartYStrikeThrough2);
                            strikeThrough2.setAttribute("x2", endX);
                            strikeThrough2.setAttribute("y2", doubleStartYStrikeThrough2);
                            grStrikeThrough.appendChild(strikeThrough1);
                            grStrikeThrough.appendChild(strikeThrough2);
                        }
                    }
                    tHyperlink.appendChild(tSpan);
                    grText.setAttribute('class', 'hyp');
                    grWords.setAttribute('class', 'hyp');
                    text.appendChild(tHyperlink);
                }

                else {
                    tSpan.textContent = inline[i].Txt;
                    tSpan.setAttribute("x", inline[i].Ls.join(" "));
                    tSpan.setAttribute("y", inline[i].T);
                    var startX = inline[i].Ls[0];
                    for (var l = 0; l < inline[i].Ls.length; l++) {
                        var endX = inline[i].Ls[0] + inline[i].w;
                    }
                    if (inline[i].Ul) {
                        var underlineColor = inline[i].uCol.col;
                        grText.appendChild(grUnderLines);
                        if (inline[i].u === "Single") {
                            var underline = document.createElementNS(xmlNs, 'line');
                            var singleUnderlineHeight = inline[i].Ul.Y2 - inline[i].Ul.Y;
                            underline.style.stroke = underlineColor;
                            underline.style.strokeWidth = inline[i].Ul.W / 10;
                            underline.setAttribute("x1", inline[i].Ul.X1);
                            underline.setAttribute("y1", inline[i].Ul.Y);
                            underline.setAttribute("x2", inline[i].Ul.X2);
                            underline.setAttribute("y2", inline[i].Ul.Y);
                            grUnderLines.appendChild(underline);
                        }
                        else if (inline[i].u === "Double") {
                            var underlineColor_1 = inline[i].uCol.col;
                            var underline1 = document.createElementNS(xmlNs, 'line');
                            var underline2 = document.createElementNS(xmlNs, 'line');
                            underline1.style.stroke = underline2.style.stroke = underlineColor_1;
                            underline1.style.strokeWidth = underline2.style.strokeWidth = inline[i].Ul.W / 15;
                            underline1.setAttribute("x1", inline[i].Ul.X1);
                            underline1.setAttribute("y1", inline[i].Ul.Y - inline[i].Ul.W / 15);
                            underline1.setAttribute("x2", inline[i].Ul.X2);
                            underline1.setAttribute("y2", inline[i].Ul.Y - inline[i].Ul.W / 15);
                            underline2.setAttribute("x1", inline[i].Ul.X1);
                            underline2.setAttribute("y1", inline[i].Ul.Y + inline[i].Ul.W / 15);
                            underline2.setAttribute("x2", inline[i].Ul.X2);
                            underline2.setAttribute("y2", inline[i].Ul.Y + inline[i].Ul.W / 15);
                            grUnderLines.appendChild(underline1);
                            grUnderLines.appendChild(underline2);
                        }
                    }
                    var strikeThroughColor = inline[i].col.col;
                    if (inline[i].stk !== "None") {
                        grText.appendChild(grStrikeThrough);
                        if (inline[i].stk === "Single") {
                            var strikeThrough = document.createElementNS(xmlNs, 'line');
                            var singleStrikeThroughHeight = inline[i].fs * 72 / 96 / 10;
                            var singleStrikeThroughStartY = inline[i].T - inline[i].fs / 3 + singleStrikeThroughHeight / 2;
                            strikeThrough.style.stroke = strikeThroughColor;
                            strikeThrough.style.strokeWidth = singleStrikeThroughHeight;
                            strikeThrough.setAttribute("x1", startX);
                            strikeThrough.setAttribute("y1", singleStrikeThroughStartY);
                            strikeThrough.setAttribute("x2", endX);
                            strikeThrough.setAttribute("y2", singleStrikeThroughStartY);
                            grStrikeThrough.appendChild(strikeThrough);
                        }
                        else if (inline[i].stk === "Double") {
                            var strikeThrough1 = document.createElementNS(xmlNs, 'line');
                            var strikeThrough2 = document.createElementNS(xmlNs, 'line');
                            var doubleStrikeThroughHeight = inline[i].fs * 72 / 96 / 10;
                            var doubleStartYStrikeThrough1 = inline[i].T - inline[i].fs * 72 / 96 / 2;
                            var doubleStartYStrikeThrough2 = inline[i].T - inline[i].fs * 72 / 96 / 4;
                            strikeThrough1.style.stroke = strikeThrough2.style.stroke = strikeThroughColor;
                            strikeThrough1.style.strokeWidth = strikeThrough2.style.strokeWidth = doubleStrikeThroughHeight;
                            strikeThrough1.setAttribute("x1", startX);
                            strikeThrough1.setAttribute("y1", doubleStartYStrikeThrough1);
                            strikeThrough1.setAttribute("x2", endX);
                            strikeThrough1.setAttribute("y2", doubleStartYStrikeThrough1);
                            strikeThrough2.setAttribute("x1", startX);
                            strikeThrough2.setAttribute("y1", doubleStartYStrikeThrough2);
                            strikeThrough2.setAttribute("x2", endX);
                            strikeThrough2.setAttribute("y2", doubleStartYStrikeThrough2);
                            grStrikeThrough.appendChild(strikeThrough1);
                            grStrikeThrough.appendChild(strikeThrough2);
                        }
                    }
                    text.appendChild(tSpan);
                }


            }
            text.innerHTML = text.innerHTML;
            grWords.appendChild(text);
        }
        grText.appendChild(grWords);
        return grText;
    };
    return Text;
}());
var TextModule = (function () {
    return {
        Level: GlobalEnum.ModuleLevel.Element,
        ContentType: "Text",
        generateElement: function (elementData, slideDataOwner) {
            var gText = new Text(elementData.pr);
            return gText.GenTexts();
        },
        generateElementByParas: function (paragraphs) {
            var gText = new Text(paragraphs);
            return gText.GenTexts();
        }
    };
})();
GlobalResources.addModule(TextModule);
