var xmlNs = "http://www.w3.org/2000/svg";
var FillColorHelper = (function () {
    return {
        genGTag: function (data, frame) {
            return fillColor(data, frame);
        }
    };
    function fillColor(data, frame) {
        if (data) {
            var id = data.col;
            switch (data.CoT) {
                case "Solid":
                    return { "ID": id, "opacity": data.opc };
                case "Gradient":
                    var gradient = new Gradient(data, frame);
                    var gReturn = gradient.genHTML();
                    id = 'url(#' + gradient.id + ')';
                    return { "Tag": gReturn, "ID": id, "opacity": data.opc };
                case "Image":
                    var image = new Picture(data, frame);
                    var gReturnImage = image.genHTML();
                    id = 'url(#' + image.id + ')';
                    return { "Tag": gReturnImage, "ID": id, "opacity": data.opc };
            }
        }
        else {
            return { "ID": "transparent" };
        }
    }
})();
function ConfigFrame(svg) {
    var idFrame = svg.attributes["id"].value.split("_")[0];
    var widthFrame = svg.attributes["width"].value;
    var heightFrame = svg.attributes["height"].value;
    var frame = { "id": idFrame, "width": widthFrame, "height": heightFrame };
    return frame;
}
var Gradient = (function () {
    function Gradient(fillColorData, frame) {
        this.fillColorData = fillColorData;
        this.frame = frame;
        this.gradientType = fillColorData.gT;
    }
    Gradient.prototype.genHTML = function () {
        return this.accordingType();
    };
    Gradient.prototype.accordingType = function () {
        var currentGradient = this.fillColorData;
        switch (currentGradient.gT) {
            case "Linear":
                var gradientLinear = new GradientLinear(this.fillColorData, this.frame);
                var gGradientLinear = gradientLinear.gLinear();
                this.id = gradientLinear.getID();
                return gGradientLinear;
                break;
            case "Radial":
                var radialGradient = new GradientRadial(this.fillColorData, this.frame);
                var gGradientRadial = radialGradient.gGradient();
                this.id = radialGradient.getID();
                return gGradientRadial;
                break;
        }
        return;
    };
    return Gradient;
}());
function createStop(ofs, col, opc) {
    var stop = GlobalHelper.createTags("stop");
    stop.setAttribute('offset', ofs);
    stop.setAttribute('stop-color', col);
    stop.setAttribute('stop-opacity', opc);
    return stop;
}
var GradientLinear = (function () {
    function GradientLinear(fillColorData, frame) {
        this.fillColorData = fillColorData;
        this.frame = frame;
    }
    GradientLinear.prototype.getID = function () {
        return 'Gradient' + this.frame.id;
    };
    GradientLinear.prototype.gLinear = function () {
        var defsLinear = document.createElementNS(xmlNs, 'defs');
        var linearGradient = document.createElementNS(xmlNs, "linearGradient");
        var stop;
        linearGradient.setAttribute('id', this.getID());
        linearGradient.setAttribute('gradientTransform', 'rotate('+this.fillColorData.agl+', 0.5, 0.5)');
        var currentLinear = this.fillColorData;
        for (var i = 0; i < currentLinear.sts.length; i++) {
            var currentStop = currentLinear.sts[i];
            var offset = this.calculatorOffset(currentStop.ofs) + "%";
            var opacity = this.calculatorOpacity(currentStop.opc);
            var color = currentStop.col;
            stop = createStop(offset, color, opacity);
            linearGradient.appendChild(stop);
        }
        defsLinear.appendChild(linearGradient);
        var gLinear = GlobalHelper.createTags('g', '', 'linear');
        gLinear.appendChild(defsLinear);
        return gLinear;
    };
    GradientLinear.prototype.calculatorOpacity = function (opacityVal) {
        return opacityVal / 255;
    };
    GradientLinear.prototype.calculatorOffset = function (offset) {
        return 100 * offset;
    };
    return GradientLinear;
}());
var GradientRadial = (function () {
    function GradientRadial(fillColorData, frame) {
        this.fillColorData = fillColorData;
        this.frame = frame;
        return;
    }
    GradientRadial.prototype.getID = function () {
        return 'Gradient' + this.frame.id;
    };
    GradientRadial.prototype.gGradient = function () {
        var defsRadial = document.createElementNS(xmlNs, 'defs');
        var radialGradient = document.createElementNS(xmlNs, "radialGradient");
        var stop;
        radialGradient.setAttribute('id', this.getID());
        radialGradient.setAttribute('gradientTransform', 'rotate('+this.fillColorData.agl+', 0.5, 0.5)');
        var currentRadial = this.fillColorData;
        for (var j = 0; j < currentRadial.sts.length; j++) {
            var currentStop = currentRadial.sts[j];
            var offset = this.calculatorOffset(currentStop.ofs) + "%";
            var opacity = this.calculatorOpacity(currentStop.opc);
            var color = currentStop.col;
            stop = createStop(offset, color, opacity);
            radialGradient.appendChild(stop);
        }
        defsRadial.appendChild(radialGradient);
        var gRadial = GlobalHelper.createTags('g', '', 'radial');
        gRadial.appendChild(defsRadial);
        return gRadial;
    };
    GradientRadial.prototype.calculatorOpacity = function (opacityVal) {
        return opacityVal / 255;
    };
    GradientRadial.prototype.calculatorOffset = function (offset) {
        return 100 * offset;
    };
    return GradientRadial;
}());
var Picture = (function () {
    function Picture(fillColorData, frame) {
        this.fillColorData = fillColorData;
        this.frame = frame;
        return;
    }
    Picture.prototype.genHTML = function () {
        return this.gPicture();
    };
    Picture.prototype.getID = function () {
        return 'Gradient' + this.frame.id;
    };
    Picture.prototype.calTranslateX = function () {
        return this.fillColorData.oL * parseFloat(this.frame.width);
    };
    Picture.prototype.calTranslateY = function () {
        return this.fillColorData.oT * parseFloat(this.frame.height);
    };
    Picture.prototype.calScaleX = function () {
        return this.fillColorData.sX - (this.fillColorData.oL + this.fillColorData.oR);
    };
    Picture.prototype.calScaleY = function () {
        return this.fillColorData.sY - (this.fillColorData.oT + this.fillColorData.oB);
    };
    Picture.prototype.gPicture = function () {
        this.id = this.getID();
        var currentPicture = this.fillColorData;
        var defsPicture = document.createElementNS(xmlNs, 'defs');
        var pattern = document.createElementNS(xmlNs, "pattern");
        var defs = document.createElementNS(xmlNs, "defs");
        var image = document.createElementNS(xmlNs, "image");
        var use = document.createElementNS(xmlNs, "use");
        pattern.setAttribute("patternTransform", "translate(0, 0) rotate(0) translate(" + this.calTranslateX() + "," + this.calTranslateY() + ")");
        pattern.setAttribute("patternUnits", "userSpaceOnUse");
        pattern.setAttribute('id', this.getID());
        pattern.setAttribute("width", this.fillColorData.iR ? this.fillColorData.ow : this.frame.width);
        pattern.setAttribute("height", this.fillColorData.iR ? this.fillColorData.oh : this.frame.height);
        image.setAttribute("transform", "scale(" + this.calScaleX() + ", " + this.calScaleY() + ")");
        image.setAttribute("id", "image" + this.frame.id);
        image.setAttribute("opacity", "1");
        image.setAttribute("preserveAspectRatio", "none");
        image.setAttribute("xlink:href", currentPicture.iU);
        image.setAttribute("width", this.fillColorData.iR ? this.fillColorData.ow : this.frame.width);
        image.setAttribute("height", this.fillColorData.iR ? this.fillColorData.oh : this.frame.height);
        use.setAttribute("xlink:href", "#image" + this.frame.id);
        defs.appendChild(image);
        pattern.appendChild(defs);
        pattern.appendChild(use);
        defsPicture.appendChild(pattern);
        var gPicture = GlobalHelper.createTags('g', '', 'image');
        gPicture.appendChild(defsPicture);
        gPicture.innerHTML = gPicture.innerHTML;
        return gPicture;
    };
    return Picture;
}());
