"use strict";
var TriggerModule = (function() {
    return {
        setTriggers: function(element, data, object) {
            let container = $("#" + element);
            if (container.length > 0) {
                let gs = container[0].getElementsByTagName("g");
                if (gs.length > 0) {
                    for (var i = 0; i < gs.length; i++) {
                        if (gs[i].id != null && gs[i].id != "") {
                            element = gs[i].id;
                            $(gs[i]).attr("data-accepts", "events");
                        } else if (gs[i].getAttribute("class") == "drop-list-cover" || gs[i].getAttribute("class") == "drop-list-item" ||
                            gs[i].getAttribute("class") == "gImg" || gs[i].getAttribute("class") == "hyp") {
                            $(gs[i]).attr("pointer-events", "unset");
                        } else {
                            $(gs[i]).attr("pointer-events", "none");
                        }
                    }
                } else if (gs.length == 0) {
                    $(container).addClass("triggerEvent");
                }

                let oldClasses = $("#" + element).attr("class");
                if (oldClasses == undefined) oldClasses = "";
                $("#" + element).attr("class", oldClasses + " triggerEvent");
            } else {
                $(container).addClass("triggerEvent");
            }

            var self = this;
            data.forEach(function(item) {
                if (item.E == "UserClicksOutside") {
                    for (var i = 0; i < item.tD.osI.length; i++) {
                        let idOS = item.tD.osI[i];
                        $("#" + idOS).addClass("closeEvent");
                    }
                }
                self.getTriggers(element, item, object);
            })
        },

        getTriggers: function(element, dataTrigger, object /*= null*/ ) {
            let eventName = dataTrigger.E;
            if (eventName != null) {
                switch (eventName) {
                    /*==== BEGIN Mouse Events ====*/
                    case "UserClicks":
                        eventAll(element, dataTrigger);
                        break;
                    case "UserDoubleClicks":
                        eventAll(element, dataTrigger);
                        break;
                    case "UserClicksOutside":
                        userClickOutside(element, dataTrigger);
                        break;
                    case "UserRightClicks":
                        eventAll(element, dataTrigger);
                        break;
                    case "MouseHoversOver":
                        mouseHover(element, dataTrigger);
                        break;
                        /*==== END Mouse Events ====*/

                        /*==== BEGIN Object Events ====*/
                    case "ObjectIntersects":
                        objectIntersect(dataTrigger, element);
                        break;
                    case "ObjectIntersectionEnds":
                        objectIntersectEnds(dataTrigger, element);
                        break;
                    case "ObjectEntersSlide":
                        objectEnterSlide(dataTrigger, element);
                        break;
                    case "ObjectLeavesSlide":
                        objectLeavesSlide(dataTrigger, element);
                        break;
                        /*==== END Object Events ====*/

                        /*=== BEGIN Control Events ====*/
                    case "MediaCompletes":
                        mediaComplete(element, dataTrigger);
                        break;
                    case "UserPressesAKey":
                        userPressesAKey(dataTrigger.Ta, dataTrigger);
                        break;
                    case "ControlLosesFocus":
                        eventAll(element, dataTrigger);
                        break;
                        /*=== END Control Events ====*/

                        /*==== BEGIN Timeline Events ====*/
                    case "TimelineStarts":
                        setTimelineTrigger(dataTrigger, object);
                        break;
                    case "TimelineEnds":
                        setTimelineTrigger(dataTrigger, object);
                        break;
                    case "TimelineReaches":
                        setTimelineTrigger(dataTrigger, object);
                        break;
                        /*==== END Timeline Events ====*/

                        /*==== BEGIN Drag Drop Events ====*/
                    case "ObjectDraggedOver":
                        dragEvent(element, dataTrigger);
                        break;
                    case "ObjectDroppedOn":
                        dropEvent(element, dataTrigger);
                        break;
                        /*==== END Drag Drop Events ====*/
                }
            }
        }
    }

    //function Dùng cho nhiều Event
    function eventAll(element, data) {
        let oldClasses = $("#" + element).attr("class");
        if (oldClasses == undefined)
            oldClasses = "";
        //Kiểm tra đối tượng để gán trigger và Gán cursor point cho đối tượng được trigger
        $("#" + element).attr("class", oldClasses + " " + element + "_" + data.E + " cursorPoint");
    }

    // Event ClickOutside
    function userClickOutside(element, data) {
        let oldClasses = $("#" + element).attr("class");
        if (oldClasses == undefined)
            oldClasses = "";
        //Kiểm tra đối tượng để gán trigger cho đối tượng được trigger
        $("#" + element).attr("class", oldClasses + " " + element + "_" + data.E);
        //Lưu vào danh sách các sự kiện
        let event = {
            EventKey: data.E,
            Data: data,
            ObjectID: element
        }
        listTriggerEvent = [];
        listTriggerEvent.push(event);
        //Viet: ẩn volume với setting nếu nó đang hi
        GlobalHelper.hiddenVolumeSetting();
    }

    //Event UserPressesAKey
    function userPressesAKey(element, data) {
        let oldClasses = $("#" + element).attr("class");
        if (oldClasses == undefined)
            oldClasses = "";
        //Kiểm tra đối tượng để gán trigger và Gán cursor point cho đối tượng được trigger
        $("#" + element).attr("class", oldClasses + " " + element + "_" + data.E);
        //Lưu vào danh sách các sự kiện
        let event = {
            EventKey: element + "_" + data.E,
            Data: data,
            ObjectID: element
        }
        listTriggerEvent.push(event);
        $("#" + element).addClass("triggerEvent");
        document.getElementById(element).setAttribute('tabindex', 0);
        $("#" + element).css("outline", "none");
        $("#" + element).focus();
    }

    //Sự kiện xử lý Trigger EVENT Hover
    function mouseHover(element, dataTrigger) {
        let oldClasses = $("#" + element).attr("class");
        if (oldClasses == undefined)
            oldClasses = "";
        //Kiểm tra đối tượng để gán trigger cho đối tượng được trigger
        $("#" + element).attr("class", oldClasses + " " + element + "_" + dataTrigger.E);
        //Lấy giá trị mouseLeave
        let _mouseHoverLeave = dataTrigger.tD.rMl;

        if (_mouseHoverLeave === false) {
            $("#" + element).mouseenter(function() {
                TriggerActionModule.getTriggerAction(dataTrigger);
            });
        } else if (_mouseHoverLeave === true) {
            $("#" + element).mouseenter(function() {
                TriggerActionModule.getTriggerAction(dataTrigger);
            });
            $("#" + element).mouseleave(function() {
                let _checkAction = dataTrigger.A;
                switch (_checkAction) { // Nếu Action là Action data truyền vào thì mouse leave sẽ thực thi action ngược lại action data
                    case "ShowLayer":
                        dataTrigger.A = "HideLayer";
                        TriggerActionModule.getTriggerAction(dataTrigger);
                        dataTrigger.A = "ShowLayer";
                        break;
                    case "HideLayer":
                        dataTrigger.A = "ShowLayer";
                        TriggerActionModule.getTriggerAction(dataTrigger);
                        dataTrigger.A = "HideLayer"
                        break;
                    case "PlayMedia":
                        dataTrigger.A = "StopMedia";
                        TriggerActionModule.getTriggerAction(dataTrigger);
                        dataTrigger.A = "PlayMedia";
                        break;
                    case "PauseMedia":
                        dataTrigger.A = "PlayMedia";
                        TriggerActionModule.getTriggerAction(dataTrigger);
                        dataTrigger.A = "PauseMedia";
                        break;
                    case "StopMedia":
                        dataTrigger.A = "PlayMedia";
                        TriggerActionModule.getTriggerAction(dataTrigger);
                        dataTrigger.A = "StopMedia";
                        break;
                    case "LightboxSlide":
                        dataTrigger.A = "CloseLightbox";
                        TriggerActionModule.getTriggerAction(dataTrigger);
                        dataTrigger.A = "LightboxSlide";
                        break;
                    case "CloseLightbox":
                        dataTrigger.A = "LightboxSlide";
                        TriggerActionModule.getTriggerAction(dataTrigger);
                        dataTrigger.A = "CloseLightbox";
                        break;
                    case "PauseTimeline":
                        dataTrigger.A = "ResumeTimeline";
                        TriggerActionModule.getTriggerAction(dataTrigger);
                        dataTrigger.A = "PauseTimeline";
                        break;
                    case "ResumeTimeline":
                        dataTrigger.A = "PauseTimeline";
                        TriggerActionModule.getTriggerAction(dataTrigger);
                        dataTrigger.A = "ResumeTimeline";
                        break;
                }
            });
        }
    }

    //Sự kiện xử lý MediaComplete
    function mediaComplete(id, dataTrigger) {
        idvs = id.split("_sh");
        var video = document.getElementById('video_' + idvs[0]);
        video.addEventListener('timeupdate', function(event) {
            if (video.ended) {
                TriggerActionModule.getTriggerAction(dataTrigger);
            }
        });
    }

    //Sự kiện xử lý drag
    function dragEvent(element, dataTrigger) {
        let elementID = $("#" + element).parents("div")[0];
        let id = elementID.id;
        for (var i = 0; i < dataTrigger.tD.osI.length; i++) {
            let elementObjectID = dataTrigger.tD.osI[i];
            $(elementID).addClass("triggerEvent");
            let elementIDs = $("#" + id).addClass(id + "_" + "ObjectDraggedOver");
            $(elementIDs).css("z-index", "1");
            $(elementIDs).addClass("cursorPoint");
            var click = {
                x: 0,
                y: 0
            };
            var startL, startT;
            $(elementIDs).draggable({
                revert: true,
                revertDuration: 1,
                appendTo: 'body',
                cursor: 'move',
                start: function(event, ui) {
                    startL = ui.helper.context.offsetLeft;
                    startT = ui.helper.context.offsetTop;
                    click.x = event.clientX;
                    click.y = event.clientY;
                },
                drag: function(event, ui) {
                    var original = ui.originalPosition;

                    // jQuery will simply use the same object we alter here
                    ui.position = {
                        left: (event.clientX - click.x + original.left) / (mainContent.offsetWidth / content.offsetWidth),
                        top: (event.clientY - click.y + original.top) / (mainContent.offsetHeight / content.offsetHeight)
                    };
                },
                stop: function(event, ui) {
                    ui.helper.context.style.left = startL + "px";
                    ui.helper.context.style.top = startT + "px";
                }
            });
            $("#" + elementObjectID).droppable({
                tolerance: "intersect",
                helper: "clone",
                cursorAt: { left: 1, top: 1 },
                over: function(event, ui) {
                    TriggerActionModule.getTriggerAction(dataTrigger);
                },
            });
        }
    }

    //Sự kiện xử lý drop
    function dropEvent(element, dataTrigger) {
        let elementID = $("#" + element).parents("div")[0];
        let id = elementID.id;
        for (var i = 0; i < dataTrigger.tD.osI.length; i++) {
            let elementObjectID = dataTrigger.tD.osI[i];
            $(elementID).addClass("triggerEvent");
            let elementIDs = $("#" + id).addClass(id + "_" + "ObjectDroppedOn");
            $(elementIDs).css("z-index", "2");
            $(elementIDs).addClass("cursorPoint");
            var startX, startY, isCheckDrop = false,
                lefts = elementIDs[0].style.left,
                tops = elementIDs[0].style.top;
            $(elementIDs).draggable({
                revert: true,
                revertDuration: 1,
                appendTo: 'body',
                cursor: 'move',
                start: function(event) {
                    click.x = event.clientX;
                    click.y = event.clientY;
                    startX = this.style.left;
                    startY = this.style.top;
                    isCheckDrop = false;
                },

                drag: function(event, ui) {

                    // This is the parameter for scale()
                    var zoom = 1.5;

                    var original = ui.originalPosition;

                    // jQuery will simply use the same object we alter here
                    ui.position = {
                        left: (event.clientX - click.x + original.left) / (mainContent.offsetWidth / content.offsetWidth),
                        top: (event.clientY - click.y + original.top) / (mainContent.offsetHeight / content.offsetHeight)
                    };

                },
                stop: function() {
                    if (!isCheckDrop) {
                        this.style.left = lefts;
                        this.style.top = tops;
                    } else {
                        this.style.left = startX;
                        this.style.top = startY;
                    }
                }
            });
            $("#" + elementObjectID).droppable({
                tolerance: 'intersect',
                helper: 'clone',
                hoverClass: function(event, ui) {
                    $("#" + elementObjectID).find("path")[0].setAttribute('stroke-width', 2);
                    $("#" + elementObjectID).css("z-index", "0");
                },
                drop: function(event, ui) {
                    TriggerActionModule.getTriggerAction(dataTrigger);
                    var x = Math.abs(this.offsetWidth - ui.helper.context.offsetWidth);
                    var y = Math.abs(this.offsetHeight - ui.helper.context.offsetHeight);
                    startX = this.offsetLeft + x / 2 + 'px';
                    startY = this.offsetTop - y / 2 + 'px';
                    isCheckDrop = true;
                }
            });
        }
    }

    //Ham set Event Timeline
    function setTimelineTrigger(triggerData, object) {
        let nameE = triggerData.E;
        var timeline = object.tl;
        var startTime = 0,
            endTime = 0;
        if (timeline == undefined && object != null) {
            for (var i = 0; i < GlobalResources.CurrentSlide.Layouts.length; i++) {
                var layout = GlobalResources.CurrentSlide.Layouts[i];
                if (layout.Elements.indexOf(object) > -1) {
                    timeline = layout.Timeline;
                    break;
                }
            }
            startTime = object.Data.tim.sT;
            endTime = object.Data.tim.sT + object.Data.tim.dT;
        } else {
            endTime = object.data.tim.dT;
        }

        switch (nameE) {
            case "TimelineStarts":
                timeline.addCallback(timeLineAction, startTime, [triggerData], null);
                break;
            case "TimelineReaches":
                timeline.addCallback(timeLineAction, triggerData.tD.tRm.Time, [triggerData], null);
                break;
            case "TimelineEnds":
                timeline.addCallback(timeLineAction, endTime, [triggerData], null);
                break;
        }
    }

    // event object intersect handling
    function objectIntersect(dataTrigger, elementID) {
        let element1 = $("#" + elementID).parents("div")[0];
        let trigger = dataTrigger;
        if (element1 != null) {
            for (var i = 0; i < dataTrigger.tD.osI.length; i++) {
                let element2 = $("#" + dataTrigger.tD.osI[i]);

                if (element2 != null) {
                    let isCollision = false;
                    let interval = setInterval(function() {
                        if (collision(element2, $(element1))) {
                            if (!isCollision) {
                                isCollision = true;
                                TriggerActionModule.getTriggerAction(trigger);
                                // clearTimeout(interval);
                            }
                        } else if (isCollision) {
                            isCollision = false;
                        }
                    }, 200);
                }
            }
        }

    }

    //event object intersectends handling
    function objectIntersectEnds(dataTrigger, elementID) {
        let element1 = $("#" + elementID).parents("div")[0];
        let trigger = dataTrigger;
        if (element1 != null) {
            for (var i = 0; i < dataTrigger.tD.osI.length; i++) {
                let element2 = $("#" + dataTrigger.tD.osI[i]);

                if (element2 != null) {
                    let isCollision = false;
                    let interval = setInterval(function() {
                        if (isCollision) {
                            if (!collision(element2, $(element1))) {
                                isCollision = false;
                                TriggerActionModule.getTriggerAction(trigger);
                                // clearTimeout(interval);
                            }
                        } else if (collision(element2, $(element1))) {
                            isCollision = true;
                        }
                    }, 200);
                }
            }
        }
    }

    //event enter slide handling
    function objectEnterSlide(dataTrigger, elementID) {
        let element = $("#" + elementID).parents("div")[0],
            trigger = dataTrigger,
            interval = setInterval(function() {
                let width = document.body.offsetWidth,
                    height = document.body.offsetHeight;

                if ($(element).offset() != null) {
                    let tlX = $(element).offset().left,
                        tlY = $(element).offset().top,
                        brX = tlX + $(element).width(),
                        brY = tlY + $(element).height();

                    if (Inside(tlX, tlY, width, height) && Inside(brX, brY, width, height)) {
                        TriggerActionModule.getTriggerAction(trigger);
                        clearTimeout(interval);
                    }
                }
            }, 200);
    }

    //Ham event leave slide
    function objectLeavesSlide(dataTrigger, elementID) {
        let element = $("#" + elementID).parents("div")[0],
            trigger = dataTrigger,
            interval = setInterval(function() {
                let width = document.body.offsetWidth,
                    height = document.body.offsetHeight;

                if ($(element).offset() != null) {
                    let tlX = $(element).offset().left,
                        tlY = $(element).offset().top,
                        brX = tlX + $(element).width(),
                        brY = tlY + $(element).height();

                    if (Outside(tlX, tlY, width, height) || Outside(brX, brY, width, height)) {
                        TriggerActionModule.getTriggerAction(trigger);
                        clearTimeout(interval);
                    }
                }
            }, 200);

    }

})();

// check goi ham ExcuteAction (timeline Action)l
function timeLineAction(value) {
    TriggerActionModule.getTriggerAction(value);
}

function Inside(x, y, width, height) {
    return x >= 0 && x <= width && y >= 0 && y <= height;
}

function Outside(x, y, width, height) {
    return !Inside(x, y, width, height);
}

function collision($div1, $div2) {
    let x1 = $div1.offset().left,
        y1 = $div1.offset().top,
        h1 = $div1.outerHeight(true),
        w1 = $div1.outerWidth(true),
        b1 = y1 + h1,
        r1 = x1 + w1,
        x2 = $div2.offset().left,
        y2 = $div2.offset().top,
        h2 = $div2.outerHeight(true),
        w2 = $div2.outerWidth(true),
        b2 = y2 + h2,
        r2 = x2 + w2;

    if (b1 < y2 || y1 > b2 || r1 < x2 || x1 > r2) return false;
    return true;
}

//Event oncomplete Animation
function onCompleteAnimate(source, eleData) {
    if (eleData != null) {
        for (var i = 0; i < eleData.tri.length; i++) {
            if (eleData.tri[i].tD.val == "EntranceAnimation" || eleData.tri[i].tD.val == "ExitAnimation") {
                TriggerActionModule.getTriggerAction(eleData.tri[i]);
            }
        }
    }
}

function onCompleMotionPath(source, eleData) {
    var listTrigger = [];
    for (var i = 0; i < eleData.tri.length; i++) {
        listTrigger.push(eleData.tri[i]);
    }
    if (listTrigger.length > 0) {
        var eventData = listTrigger.filter(function(x) {
            if (x.E == "AnimationCompletes") {
                return x;
            }
        });
        for (var i = 0; i < eventData.length; i++) {
            TriggerActionModule.getTriggerAction(eventData[i]);
        }
    }
}