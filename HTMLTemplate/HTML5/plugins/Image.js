var ImageModule = (function () {
    return {
        Level: GlobalEnum.ModuleLevel.Element,
        ContentType: "Image",
        generateElement: function (elementData, slideDataOwner) {
            var image = new Image(elementData);
            return image.genImage();
        }
    };
})();
GlobalResources.addModule(ImageModule);
var Image = (function () {
    function Image(elementData) {
        this.options = elementData;
    }
    Image.prototype.createEleEffect = function () {
        var useHTML = null;
        if (this.options.efs) {
            for (var i = 0; i < this.options.efs.length; i++) {
                var effEle = EffectModule.genGEffect(this.options.efs[i], { Id: this.options.id, width: this.options.w, height: this.options.h }, this.options.agl);
                if (this.options.efs[i].shT === "Inner") {
                    useHTML = effEle.use;
                    break;
                }
            }
        }
        if (useHTML != null) {
            return useHTML;
        }
        return null;
    };
    Image.prototype.genImage = function () {
        var currentImg = this.options;
        var xmls = "http://www.w3.org/2000/svg";
        var grImg = GlobalHelper.createTags('g', '', 'gImg');
        var defsImg = document.createElementNS(xmls, 'defs');
        var pattern = document.createElementNS(xmls, 'pattern');
        var GPath = document.createElementNS(xmls, 'g');
        var path = document.createElementNS(xmls, 'path');
        var path1 = document.createElementNS(xmls, 'path');
        var useChild = document.createElementNS(xmls, 'use');
        var image = document.createElementNS(xmls, 'image');
        GPath.setAttribute("id", currentImg.id + '_gp');
        pattern.setAttribute("patternTransform", "translate(0, 0)");
        pattern.setAttribute("patternUnits", "userSpaceOnUse");
        pattern.setAttribute("id", 'pattern' + currentImg.id);
        pattern.setAttribute("width", currentImg.w);
        pattern.setAttribute("height", currentImg.h);
        path.setAttribute("id", "path" + currentImg.id);
        path.setAttribute("fill", "none");
        if (currentImg.shs.pOb[0].str) {
            path.setAttribute("stroke", currentImg.shs.pOb[0].str.col);
            path.setAttribute("stroke-width", currentImg.shs.pOb[0].sTh);
            path.setAttribute("d", currentImg.shs.pOb[0].dt);
        }
        useChild.setAttribute("xlink:href", "#image" + currentImg.id);
        path1.setAttribute("id", "path2");
        path1.setAttribute("fill", 'url(#pattern' + currentImg.id + ')');
        path1.setAttribute("d", currentImg.shs.pOb[0].dt);
        image.setAttribute("id", 'image' + currentImg.id);
        image.setAttribute("preserveAspectRatio", "none");
        image.setAttribute("opacity", "1");
        if(currentImg.imgU){
            if(currentImg.efs){
                for(var i = 0; i < currentImg.efs.length; i++){
                    if(currentImg.efs[i].typ === "SoftEdge"){
                        image.setAttribute("xlink:href", currentImg.efs[i].sU);
                    }
                    else{
                        image.setAttribute("xlink:href", currentImg.imgU);
                    }
                }
            }
            else{
                image.setAttribute("xlink:href", currentImg.imgU);
            }
        }
        image.setAttribute("width", currentImg.w);
        image.setAttribute("height", currentImg.h);
        defsImg.appendChild(pattern);
        defsImg.appendChild(useChild);
        pattern.appendChild(image);
        GPath.appendChild(path);
        GPath.appendChild(path1);
        grImg.appendChild(defsImg);
        grImg.appendChild(GPath);
        if (this.createEleEffect(grImg) != null) {
            grImg.appendChild(this.createEleEffect(grImg));
        }
        grImg.innerHTML = grImg.innerHTML;
        return grImg;
    };
    return Image;
}());
