var ShapeHelper = (function () {
    return {
        generateShapes: function (shape, frame) {
            var currentShapeObj = shape;
            var gShape = GlobalHelper.createTags('g', GlobalHelper.generateBranchId(frame.id, "sh"), 'shape');
            if (currentShapeObj) {
                for (var i = 0; i < currentShapeObj.pOb.length; i++) {
                    var pathShape = GlobalHelper.createTags('path', GlobalHelper.generateBranchId(frame.id, "gp"), '');
                    var currentPathObjects = currentShapeObj.pOb[i];
                    pathShape.setAttribute("d", currentPathObjects.dt);
                    if (currentPathObjects.str) {
                        if (currentPathObjects.str.CoT === "Solid") {
                            pathShape.setAttribute("stroke", currentPathObjects.str.col);
                        }
                    }
                    pathShape.setAttribute("stroke-width", currentPathObjects.sTh);
                    var gFill = FillColorHelper.genGTag(currentPathObjects.fi, frame);
                    if (gFill) {
                        if (gFill.Tag)
                            GlobalHelper.appendIdContent(gShape, gFill.Tag);
                        pathShape.setAttribute("fill", gFill.ID);
                    }
                    pathShape.setAttribute("stroke-linecap", currentShapeObj.cT);
                    pathShape.setAttribute("stroke-linejoin", currentShapeObj.jT);
                    pathShape.setAttribute("stroke-dasharray", this.getDashArray(currentShapeObj.dT));
                    GlobalHelper.appendIdContent(gShape, pathShape);
                }
            }
            return gShape;
        },
        getDashArray: function (dashType) {
            switch (dashType) {
                case "Solid":
                    return "";
                case "RoundDot":
                    return "1, 5";
                case "SquareDot":
                    return "5, 8";
                case "Dash":
                    return "10, 10";
                case "DashDot":
                    return "12, 12, 1, 12";
                case "LongDash":
                    return "18, 10";
                case "LongDashDot":
                    return "18, 10, 1, 10";
                case "LongDashDotDot":
                    return "18, 10, 1, 10, 1, 10";
            }
            return "";
        }
    };
})();
