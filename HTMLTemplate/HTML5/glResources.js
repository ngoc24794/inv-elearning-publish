var GlobalResources = (function () {
    var eleModuleConnects = [], layModuleConnects = [], sliModuleConnects = [], showCaption = false;
    return {
        addModule: function (module) {
            switch (module.Level) {
                case GlobalEnum.ModuleLevel.Element:
                    if (typeof module.generateElement != undefined)
                        eleModuleConnects.push(module);
                    break;
                case GlobalEnum.ModuleLevel.Layout:
                    if (typeof module.generateLayout != undefined)
                        layModuleConnects.push(module);
                    break;
                case GlobalEnum.ModuleLevel.Slide:
                    if (typeof module.generateSlide != undefined)
                        sliModuleConnects.push(module);
                    break;
            }
        },
        getElementModule: function (contentType) {
            for (var i = 0; i < eleModuleConnects.length; i++) {
                if (eleModuleConnects[i].ContentType == contentType)
                    return eleModuleConnects[i];
            }
            return null;
        },
        getLayoutModule: function (contentType) {
            for (var i = 0; i < layModuleConnects.length; i++) {
                if (layModuleConnects[i].ContentType == contentType)
                    return layModuleConnects[i];
            }
            return NormalLayoutModule;
        },
        CurrentSlide: null,
        CurrentLayer: null,
        get ShowCaption() {
            return showCaption;
        },
        set ShowCaption(value) {
            var caption = document.getElementById("caption");
            if (caption) {
                caption.style.display = value ? "block" : "none";
            }
            showCaption = value;
        },
        get PreviousSlide() {
            return getPreviousSlide();
        }
    };
    function getPreviousSlide() {
        for (var i = 0; i < $("#content")[0].children.length; i++) {
            var child = $("#content")[0].children[i];
            if (child.id != GlobalResources.CurrentSlide.Data.id && child.id != "caption") {
                return child;
            }
        }
    }
})();
var GroupModule = (function () {
    return {
        Level: GlobalEnum.ModuleLevel.Element,
        ContentType: "Group",
        generateElement: function (groupData, slideDataOwner) {
            var divContainer = GlobalHelper.createTags('div', '', '');
            divContainer.style.top = -groupData.T + "px";
            divContainer.style.left = -groupData.l + "px";
            divContainer.style.position = "absolute";
            for (var i = 0; i < groupData.els.length; i++) {
                var elementData = groupData.els[i];
                var element = new Element(elementData, slideDataOwner);
                if (element && (typeof element.generateHTML !== 'underfined')) {
                    var elementTag = element.generateHTML();
                    if (elementTag) {
                        GlobalHelper.appendIdContent(divContainer, elementTag);
                    }
                }
            }
            return divContainer;
        }
    };
})();
GlobalResources.addModule(GroupModule);
