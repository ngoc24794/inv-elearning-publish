﻿using INV.Elearning.Core.Helper;
using INV.Elearning.Core.ViewModel;
using INV.Elearning.HTMLHelper.HTMLData.Helpers;
using INV.Elearning.Text;
using INV.Elearning.Text.ViewModels;
using INV.Elearning.Text.ViewModels.Text;
using INV.Elearning.Text.Views;
using INV.Elearning.Trigger;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;

namespace INV.Elearning.HTMLHelper
{
    public class HTMLReferenceText
    {
        public static string CheckReference( string text,List<Reference> list)
        {
            string result = text;
            if (list?.Count > 0)
            {
                foreach (var item in list)
                {
                    var _index = result.IndexOf("%"+item.Key+"%");
                    if (_index >= 0)
                    {
                        result = result.Substring(0, _index) + "<span>" + result.Substring(_index,item.Key.Length+2)+"</span>"+ result.Substring(_index+item.Key.Length+2);
                    }
                    
                }
            }
            return result;
        }
        public static string SetHtmlReferenText(RichTextEditor richTextEditor)
        {

            string result = "";
            string _paraHtml = "";
            string _inlineHtml = "";
            HTMLDataHelper dataHelper = new HTMLDataHelper();
            HtmlViewModel htmlViewModel = new HtmlViewModel();

            var _listReference = GetListReference();
            //if (_listReference?.Count > 0)
            //{
            //    foreach (var item in _listReference)
            //    {
            //        htmlViewModel.SetTextRenferenceCommand(richTextEditor,"%"+item.Value+"%", new List<string>() { "%"+ item.Value+"%"});
            //    }
            //}

            var richTextEditorReference = richTextEditor;
            if (richTextEditorReference != null)
            {
               
                TextRange textRange = new TextRange();
                var _listPara = textRange.GetParagraphs(richTextEditorReference.TextContainer.Document.Blocks);
                int _countPara = _listPara.Count;
                double lineHeight;
                
                for (int i = 0; i < _countPara; i++)
                {
                    lineHeight = _listPara[i].LineHeight * ((Convert.ToInt32(_listPara[i].Inlines[0].FontSize * 96 / 72) * 10) / 10);
                    if (lineHeight == 0) lineHeight = ((Convert.ToInt32(_listPara[i].Inlines[0].FontSize * 96 / 72) * 10) / 10);
                    if (i == 0)
                    {
                        _paraHtml = "<p style='text-align:" + _listPara[i].TextAlign + ";line-height:" + lineHeight + "px; direction: ltr; margin: 0; padding: 5px;'>";
                    }
                    else
                    {
                        _paraHtml = "<p style='text-align:" + _listPara[i].TextAlign + ";line-height:" + lineHeight + "px; direction: ltr; margin: 0'>";
                    }
                   
                    var _inlineLast = _listPara[i].Inlines.Last();

                    foreach (var inline in _listPara[i].Inlines)
                    {
                        if ((inline as Run).Text == "\r") continue;

                        #region Chỉ số trên
                        if ((inline as Run).ScriptOffset > 0)
                        {

                            _inlineHtml = "<sup style='top: -" + (((inline as Run).ScriptOffset / 100.0)) + "em; position: relative; font-size: " + ((Convert.ToInt32((inline.FontSize * 96 / 72 * richTextEditorReference.TextContainer.Document.CoefficientSizeChange) * 10) / 10) * 2 / 3) + "px;'> <span style='position: relative; font-family: " + inline.Fontfamily +
                            ";font-weight: " + inline.FontWeight.ToString().ToLower() +
                            ";font-style: " + inline.FontStyle.ToString().ToLower() +
                            ";color: " + dataHelper.ConvertColorTextToHtmlColor(richTextEditor, inline.Foreground) + "'>";

                            #region Underline

                            //Single
                            if (inline.Underline?.Style == UnderlineStyle.Single)
                            {
                                if (inline.Underline.Color == null)
                                {
                                    _inlineHtml += "<span style='border-bottom: " + ((Convert.ToInt32((inline.FontSize * 96 / 72 * richTextEditorReference.TextContainer.Document.CoefficientSizeChange) * 10) / 10) / 16) + "px solid " + dataHelper.ConvertColorTextToHtmlColor(richTextEditor, inline.Foreground) + ";' class='udl'></span>";

                                }
                                else
                                {
                                    _inlineHtml += "<span style='border-bottom: " + ((Convert.ToInt32((inline.FontSize * 96 / 72 * richTextEditorReference.TextContainer.Document.CoefficientSizeChange) * 10) / 10) / 16) + "px solid " + dataHelper.ConvertColorTextToHtmlColor(richTextEditor, inline.Underline.Color) + ";' class='udl'></span>";
                                }

                            }

                            //Double
                            else if (inline.Underline?.Style == UnderlineStyle.Double)
                            {
                                if (!(inline.Underline.Color?.Brush != null))
                                {
                                    _inlineHtml += "<span style='border-bottom: " + ((Convert.ToInt32((inline.FontSize * 96 / 72 * richTextEditorReference.TextContainer.Document.CoefficientSizeChange) * 10) / 10) / 16) + "px solid " + dataHelper.ConvertColorTextToHtmlColor(richTextEditor, inline.Foreground) + "; border-top: " + ((Convert.ToInt32((inline.FontSize * 96 / 72 * richTextEditorReference.TextContainer.Document.CoefficientSizeChange) * 10) / 10) / 16) + "px solid " + dataHelper.ConvertColorTextToHtmlColor(richTextEditor, inline.Foreground) + "; bottom: -5%;' class='db-udl'></span>";

                                }
                                else
                                {
                                    _inlineHtml += "<span style='border-bottom: " + ((Convert.ToInt32((inline.FontSize * 96 / 72 * richTextEditorReference.TextContainer.Document.CoefficientSizeChange) * 10) / 10) / 16) + "px solid " + dataHelper.ConvertColorTextToHtmlColor(richTextEditor, inline.Underline.Color) + "; border-top: " + ((Convert.ToInt32((inline.FontSize * 96 / 72 * richTextEditorReference.TextContainer.Document.CoefficientSizeChange) * 10) / 10) / 16) + "px solid " + dataHelper.ConvertColorTextToHtmlColor(richTextEditor, inline.Underline.Color) + "; bottom: -5%;' class='db-udl'></span>";
                                }
                            }
                            #endregion

                            #region Strike Through

                            //Single
                            if (inline.StrikeThrough == StrikeThrough.Single)
                            {
                                _inlineHtml += "<span style='border-bottom: " + ((Convert.ToInt32((inline.FontSize * 96 / 72 * richTextEditorReference.TextContainer.Document.CoefficientSizeChange) * 10) / 10) / 16) + "px solid " + dataHelper.ConvertColorTextToHtmlColor(richTextEditor, inline.Foreground) + ";' class='strTh'></span>";
                            }

                            //Double
                            else if (inline.StrikeThrough == StrikeThrough.Double)
                            {
                                _inlineHtml += "<span style='border-bottom: " + ((Convert.ToInt32((inline.FontSize * 96 / 72 * richTextEditorReference.TextContainer.Document.CoefficientSizeChange) * 10) / 10) / 16) + "px solid " + dataHelper.ConvertColorTextToHtmlColor(richTextEditor, inline.Foreground) + "; border-top: " + ((Convert.ToInt32((inline.FontSize * 96 / 72 * richTextEditorReference.TextContainer.Document.CoefficientSizeChange) * 10) / 10) / 16) + "px solid " + dataHelper.ConvertColorTextToHtmlColor(richTextEditor, inline.Foreground) + "; bottom: 30%;' class='db-strTh'></span>";
                            }
                            #endregion

                            if (inline == _inlineLast)
                            {
                                if ((inline as Run).Text.Length > 0 && (inline as Run).Text[(inline as Run).Text.Length - 1] == '\r')
                                    _inlineHtml += (inline as Run).Text.Substring(0, (inline as Run).Text.Length - 1);
                            }
                            else
                            {
                                _inlineHtml += (inline as Run).Text;
                            }


                            _inlineHtml += "</span></sup>";
                            _paraHtml += _inlineHtml;
                        }
                        #endregion

                        #region Chỉ số dưới
                        else if ((inline as Run).ScriptOffset < 0)
                        {
                            _inlineHtml = "<sub style='bottom: " + (((inline as Run).ScriptOffset) / 100.0) + "em; position: relative; font-size: " + ((Convert.ToInt32((inline.FontSize * 96 / 72 * richTextEditorReference.TextContainer.Document.CoefficientSizeChange) * 10) / 10) * 2 / 3) + "px;'> <span style='position: relative; font-family: " + inline.Fontfamily +
                           ";font-weight: " + inline.FontWeight.ToString().ToLower() +
                           ";font-style: " + inline.FontStyle.ToString().ToLower() +
                           ";color: " + dataHelper.ConvertColorTextToHtmlColor(richTextEditor, inline.Foreground) + "'>";

                            #region Underline
                            //Single
                            if (inline.Underline?.Style == UnderlineStyle.Single)
                            {
                                if (inline.Underline.Color == null)
                                {
                                    _inlineHtml += "<span style='border-bottom: " + ((Convert.ToInt32((inline.FontSize * 96 / 72 * richTextEditorReference.TextContainer.Document.CoefficientSizeChange) * 10) / 10) / 16) + "px solid " + dataHelper.ConvertColorTextToHtmlColor(richTextEditor, inline.Foreground) + ";' class='udl'></span>";

                                }
                                else
                                {
                                    _inlineHtml += "<span style='border-bottom: " + ((Convert.ToInt32((inline.FontSize * 96 / 72 * richTextEditorReference.TextContainer.Document.CoefficientSizeChange) * 10) / 10) / 16) + "px solid " + dataHelper.ConvertColorTextToHtmlColor(richTextEditor, inline.Underline.Color) + ";' class='udl'></span>";
                                }

                            }

                            //Double
                            else if (inline.Underline?.Style == UnderlineStyle.Double)
                            {
                                if (!(inline.Underline.Color?.Brush != null))
                                {
                                    _inlineHtml += "<span style='border-bottom: " + ((Convert.ToInt32((inline.FontSize * 96 / 72 * richTextEditorReference.TextContainer.Document.CoefficientSizeChange) * 10) / 10) / 16) + "px solid " + dataHelper.ConvertColorTextToHtmlColor(richTextEditor, inline.Foreground) + "; border-top: " + ((Convert.ToInt32((inline.FontSize * 96 / 72 * richTextEditorReference.TextContainer.Document.CoefficientSizeChange) * 10) / 10) / 16) + "px solid " + dataHelper.ConvertColorTextToHtmlColor(richTextEditor, inline.Foreground) + "; bottom: -5%;' class='db-udl'></span>";

                                }
                                else
                                {
                                    _inlineHtml += "<span style='border-bottom: " + ((Convert.ToInt32((inline.FontSize * 96 / 72 * richTextEditorReference.TextContainer.Document.CoefficientSizeChange) * 10) / 10) / 16) + "px solid " + dataHelper.ConvertColorTextToHtmlColor(richTextEditor, inline.Underline.Color) + "; border-top: " + ((Convert.ToInt32((inline.FontSize * 96 / 72 * richTextEditorReference.TextContainer.Document.CoefficientSizeChange) * 10) / 10) / 16) + "px solid " + dataHelper.ConvertColorTextToHtmlColor(richTextEditor, inline.Underline.Color) + "; bottom: -5%;' class='db-underline'></span>";
                                }
                            }
                            #endregion

                            #region Strike Through
                            //Single
                            if (inline.StrikeThrough == StrikeThrough.Single)
                            {
                                _inlineHtml += "<span style='border-bottom: " + ((Convert.ToInt32((inline.FontSize * 96 / 72 * richTextEditorReference.TextContainer.Document.CoefficientSizeChange) * 10) / 10) / 16) + "px solid " + dataHelper.ConvertColorTextToHtmlColor(richTextEditor, inline.Foreground) + ";' class='strTh'></span>";
                            }
                            //Double
                            else if (inline.StrikeThrough == StrikeThrough.Double)
                            {
                                _inlineHtml += "<span style='border-bottom: " + ((Convert.ToInt32((inline.FontSize * 96 / 72 * richTextEditorReference.TextContainer.Document.CoefficientSizeChange) * 10) / 10) / 16) + "px solid " + dataHelper.ConvertColorTextToHtmlColor(richTextEditor, inline.Foreground) + "; border-top: " + ((Convert.ToInt32((inline.FontSize * 96 / 72 * richTextEditorReference.TextContainer.Document.CoefficientSizeChange) * 10) / 10) / 16) + "px solid " + dataHelper.ConvertColorTextToHtmlColor(richTextEditor, inline.Foreground) + "; bottom: 30%;' class='db-strTh'></span>";
                            }
                            #endregion

                            if (inline == _inlineLast)
                            {
                                if ((inline as Run).Text.Length > 0 && (inline as Run).Text[(inline as Run).Text.Length - 1] == '\r')
                                    _inlineHtml += (inline as Run).Text.Substring(0, (inline as Run).Text.Length - 1);
                            }
                            else
                            {
                                _inlineHtml += (inline as Run).Text;
                            }
                            _inlineHtml += "</span></sub>";
                            _paraHtml += _inlineHtml;
                        }
                        #endregion

                        #region Text bình thường
                        else
                        {
                            _inlineHtml = "<span style='position: relative; font-family: " + inline.Fontfamily +
                            ";font-size: " + (Convert.ToInt32((inline.FontSize * 96 / 72* richTextEditorReference.TextContainer.Document.CoefficientSizeChange) * 10) / 10) + "px;" +
                            "font-weight: " + inline.FontWeight.ToString().ToLower() +
                            ";font-style: " + inline.FontStyle.ToString().ToLower() +
                            ";color: " + dataHelper.ConvertColorTextToHtmlColor(richTextEditor, inline.Foreground) + "'>";

                            #region Underline
                            //Single
                            if (inline.Underline?.Style == UnderlineStyle.Single)
                            {
                                if (inline.Underline.Color == null)
                                {
                                    _inlineHtml += "<span style='border-bottom: " + ((Convert.ToInt32((inline.FontSize * 96 / 72 * richTextEditorReference.TextContainer.Document.CoefficientSizeChange) * 10) / 10) / 16) + "px solid " + dataHelper.ConvertColorTextToHtmlColor(richTextEditor, inline.Foreground) + ";' class='udl'></span>";

                                }
                                else
                                {
                                    _inlineHtml += "<span style='border-bottom: " + ((Convert.ToInt32((inline.FontSize * 96 / 72 * richTextEditorReference.TextContainer.Document.CoefficientSizeChange) * 10) / 10) / 16) + "px solid " + dataHelper.ConvertColorTextToHtmlColor(richTextEditor, inline.Underline.Color) + ";' class='udl'></span>";
                                }

                            }
                            //Double
                            else if (inline.Underline?.Style == UnderlineStyle.Double)
                            {
                                if (!(inline.Underline.Color?.Brush != null))
                                {
                                    _inlineHtml += "<span style='border-bottom: " + ((Convert.ToInt32((inline.FontSize * 96 / 72 * richTextEditorReference.TextContainer.Document.CoefficientSizeChange) * 10) / 10) / 16) + "px solid " + dataHelper.ConvertColorTextToHtmlColor(richTextEditor, inline.Foreground) + "; border-top: " + ((Convert.ToInt32((inline.FontSize * 96 / 72 * richTextEditorReference.TextContainer.Document.CoefficientSizeChange) * 10) / 10) / 16) + "px solid " + dataHelper.ConvertColorTextToHtmlColor(richTextEditor, inline.Foreground) + "; bottom: -5%;' class='db-udl'></span>";

                                }
                                else
                                {
                                    _inlineHtml += "<span style='border-bottom: " + ((Convert.ToInt32((inline.FontSize * 96 / 72 * richTextEditorReference.TextContainer.Document.CoefficientSizeChange) * 10) / 10) / 16) + "px solid " + dataHelper.ConvertColorTextToHtmlColor(richTextEditor, inline.Underline.Color) + "; border-top: " + ((Convert.ToInt32((inline.FontSize * 96 / 72 * richTextEditorReference.TextContainer.Document.CoefficientSizeChange) * 10) / 10) / 16) + "px solid " + dataHelper.ConvertColorTextToHtmlColor(richTextEditor, inline.Underline.Color) + "; bottom: -5%;' class='db-udl'></span>";
                                }
                            }
                            #endregion

                            #region Strike Through
                            //Single
                            if (inline.StrikeThrough == StrikeThrough.Single)
                            {
                                _inlineHtml += "<span style='border-bottom: " + ((Convert.ToInt32((inline.FontSize * 96 / 72 * richTextEditorReference.TextContainer.Document.CoefficientSizeChange) * 10) / 10) / 16) + "px solid " + dataHelper.ConvertColorTextToHtmlColor(richTextEditor, inline.Foreground) + ";' class='strTh'></span>";
                            }
                            //Double
                            else if (inline.StrikeThrough == StrikeThrough.Double)
                            {
                                _inlineHtml += "<span style='border-bottom: " + ((Convert.ToInt32((inline.FontSize * 96 / 72 * richTextEditorReference.TextContainer.Document.CoefficientSizeChange) * 10) / 10) / 16) + "px solid " + dataHelper.ConvertColorTextToHtmlColor(richTextEditor, inline.Foreground) + "; border-top: " + ((Convert.ToInt32((inline.FontSize * 96 / 72 * richTextEditorReference.TextContainer.Document.CoefficientSizeChange) * 10) / 10) / 16) + "px solid " + dataHelper.ConvertColorTextToHtmlColor(richTextEditor, inline.Foreground) + "; bottom: 30%;' class='db-strTh'></span>";
                            }
                            #endregion

                            if (inline == _inlineLast)
                            {
                                if ((inline as Run).Text.Length > 0 && (inline as Run).Text[(inline as Run).Text.Length - 1] == '\r')
                                    _inlineHtml += CheckReference((inline as Run).Text.Substring(0, (inline as Run).Text.Length - 1),_listReference);
                            }
                            else
                            {
                                
                                _inlineHtml += CheckReference((inline as Run).Text, _listReference); 
                            }
                            _inlineHtml += "</span>";
                            _paraHtml += _inlineHtml;
                        }
                        #endregion

                    }

                    _paraHtml += "</p>";
                    result += _paraHtml;
                }

            }
         //   result += "";
            return result;
        }

        private string GetIdReference(string valueReference, List<Reference> list)
        {
            string _result = "";
            if (list?.Count > 0)
            {
                foreach (var item in list)
                {
                    if (item.Value == valueReference)
                        return "id=";
                }
            }
            return _result;
        }
        public static List<Reference> GetListReference()
        {
            List<Reference> result = new List<Reference>();
            ObservableCollection<VariableViewModelBase> list = (Application.Current as IAppGlobal).DocumentControl?.Variables;
            if (list != null)
            {
                foreach (var item in list)
                {
                    if (item is VariableViewModel variableViewModel && variableViewModel.GetModel()?.GetData() is VariableData variable)
                    {
                        result.Add(new Reference(variable.Name, variable.Value));
                    }
                }
            }
            return result;
        }
    }
    public class Reference
    {
        public Reference(string key, string value)
        {
            Key = key;
            Value = value;
        }

        public string Key { get; set; }
        public string Value { get; set; }

    }
}
