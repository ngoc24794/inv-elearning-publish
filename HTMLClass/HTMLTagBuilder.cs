﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INV.Elearning.HTMLHelper
{
    public class TagBuilder
    {
        public string TagName { get; private set; }
        public Dictionary<string, string> Attributes { get; set; }
        public string InnerHtml { get; set; }
        public List<string> CssClass { get; private set; }
        public List<string> CssId { get; set; }
        public List<string> InnerCSS { get; set; }
        public List<string> Width { get; set; }
        public List<string> Height { get; set; }
        public TagBuilder(string name)
        {
            TagName = name.ToLower().Trim();
            Attributes = new Dictionary<string, string>();
            CssClass = new List<string>();
            InnerHtml = string.Empty;
            InnerCSS = new List<string>();
            CssId = new List<string>();
            Width = new List<string>();
            Height = new List<string>();
        }

        public void AddCssClass(string value)
        {
            if (!CssClass.Contains(value))
            {
                CssClass.Add(value);
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder("");
            sb.AppendFormat("<{0} ", TagName);
            foreach (var attr in Attributes.Keys)
            {
                sb.AppendFormat("{0}=\"{1}\" ", attr, Attributes[attr]);
            }
            int i = 0;
            if (CssClass.Count > 0)
            {
                sb.Append("class=\"");

                foreach (var css in CssClass)
                {
                    sb.AppendFormat("{0}", css);
                    if (i < CssClass.Count - 1)
                    {
                        sb.Append(" ");
                    }
                }
                sb.Append("\"");
            }
            if (CssId.Count > 0)
            {
                sb.Append("id=\"");

                foreach (var css in CssId)
                {
                    sb.AppendFormat("{0}", css);
                    if (i < CssId.Count - 1)
                    {
                        sb.Append(" ");
                    }
                }
                sb.Append("\"");
            }

            if (InnerCSS.Count > 0)
            {
                sb.Append(" style=\"");
                foreach (var css in InnerCSS)
                {
                    sb.AppendFormat("{0};", css);
                    if (i < CssClass.Count - 1)
                    {
                        sb.Append(" ");
                    }
                }
                sb.Append("\"");
            }
            if (Width.Count > 0)
            {
                sb.Append(" width=\"");
                foreach (var width in Width)
                {
                    sb.AppendFormat("{0};", width);
                    if (i < Width.Count - 1)
                    {
                        sb.Append(" ");
                    }
                }
                sb.Append("\"");
            }
            if (Height.Count > 0)
            {
                sb.Append(" height=\"");
                foreach (var height in Height)
                {
                    sb.AppendFormat("{0};", height);
                    if (i < Height.Count - 1)
                    {
                        sb.Append(" ");
                    }
                }
                sb.Append("\"");
            }
            string[] tagWithoutClose = { "area", "base", "br", "col", "command", "embed", "hr", "img", "input", "keygen", "link", "meta", "param", "source", "track", "wbr", "path" };
            if (tagWithoutClose.Contains(TagName))
            {
                sb.Append(" />\n");
            }
            else
            {
                sb.AppendFormat(">{0}</{1}>\n", InnerHtml, TagName);
            }

            return sb.ToString();
        }
    }
}
