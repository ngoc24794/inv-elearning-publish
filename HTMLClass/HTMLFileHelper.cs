﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INV.Elearning.HTMLHelper
{

    public class HTMLFileHelper
    {
        public static string GetValueFromFile(string path)
        {
            StreamReader reader = StreamReader.Null;
            path = AppDomain.CurrentDomain.BaseDirectory + path;
            try
            {
                FileInfo file = new FileInfo(path);
                if (!file.Exists)
                {
                    throw new Exception("Không tồn tại file này");
                }
                reader = file.OpenText();
                string result = reader.ReadToEnd();
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Dispose();
                reader.Close();
            }
        }
        public static void SetValueToFile(string folder, string nameFile, string extension, string content)
        {
            StreamWriter writer = StreamWriter.Null;
            try
            {
                if (!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);
                }
                string fullPath = Path.Combine(folder, nameFile + extension);
                writer = new StreamWriter(fullPath, false, Encoding.Unicode);
                writer.WriteLine(content);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (writer != null)
                {
                    writer.Dispose();
                    writer.Close();
                }
            }
        }
    }
}
