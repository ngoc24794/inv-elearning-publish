﻿using INV.Elearning.Core.Helper.Effect;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace INV.Elearning.HTMLHelper.HTMLData.EffectClass
{
    public abstract class HTMLEffect
    {
        /// <summary>
        /// Kích thước
        /// </summary>
        [JsonProperty(PropertyName = "sz")]
        public double Size { get; set; }
        /// <summary>
        /// ID của Filter
        /// </summary>
        //[JsonProperty(PropertyName = "idF")]
        //public string IDFilter { get; set; }
        /// <summary>
        /// Kiểu hiệu ứng
        /// </summary>
        [JsonProperty("typ")]
        [JsonConverter(typeof(StringEnumConverter))]
        public HTMLEffectType Type { get; set; }
        public enum HTMLEffectType
        {
            Glow,
            Shadow,
            Reflection,
            SoftEdge
        }
    }

    public class HTMLGlowEffect : HTMLEffect
    {
        /// <summary>
        /// Màu của hiệu ứng tỏa sáng
        /// </summary>
        [JsonProperty(PropertyName = "col")]
        public string Color { get; set; }
        [JsonProperty(PropertyName = "std")]
        public double StdDeviation { get; set; }
        public HTMLGlowEffect()
        {
            Type = HTMLEffectType.Glow;
        }
    }

    public class HTMLShadowEffect : HTMLEffect
    {
        public enum HTMLShadowType
        {
            Inner,
            Outer,
            Perspective
        }

        public HTMLShadowEffect()
        {
            Type = HTMLEffectType.Shadow;
        }
        /// <summary>
        /// Loại hiệu ứng đổ bóng (có 3 loại)
        /// </summary>
        [JsonProperty("shT")]
        [JsonConverter(typeof(StringEnumConverter))]
        public HTMLShadowType ShadowType { get; set; }
        /// <summary>
        /// Màu của bóng
        /// </summary>
        [JsonProperty(PropertyName = "col")]
        public string Color { get; set; }
        /// <summary>
        /// Độ mờ của bóng
        /// </summary>
        [JsonProperty(PropertyName = "opc")]
        public double Opacity { get; set; }
        /// <summary>
        /// Khoảng cách của bóng so với đối tượng
        /// </summary>
        [JsonProperty(PropertyName = "dis")]
        public double Distance { get; set; }
        /// <summary>
        /// Góc xoay của bóng so với đối tượng
        /// </summary>
        [JsonProperty(PropertyName = "agl")]
        public double Angle { get; set; }
        /// <summary>
        /// Độ nhòe của bóng
        /// </summary>
        [JsonProperty(PropertyName = "bl")]
        public double Blur { get; set; }
        /// <summary>
        /// Data phần trong của shadow inner
        /// </summary>
        [JsonProperty(PropertyName = "dtInner")]
        public string DataInner { get; set; }
        /// <summary>
        /// Loại perspective shadow
        /// </summary>
        [JsonProperty(PropertyName = "peT")]
        [JsonConverter(typeof(StringEnumConverter))]
        public PerspectiveShadowEnum PerspectiveShadowType { get; set; }
    }

    public class HTMLReflectionEffect : HTMLEffect
    {
        /// <summary>
        /// Độ mờ của đối tượng phản chiếu
        /// </summary>
        [JsonProperty(PropertyName = "opc")]
        public double Opacity { get; set; }
        /// <summary>
        /// Khoảng cách giữa đối tượng chính với đối tượng phản chiếu
        /// </summary>
       [JsonProperty(PropertyName = "dis")]
        public double Distance { get; set; }
        /// <summary>
        /// Độ nhòe của đối tượng phản chiếu
        /// </summary>
       [JsonProperty(PropertyName = "bl")]
        public double Blur { get; set; }
        public HTMLReflectionEffect()
        {
            Type = HTMLEffectType.Reflection;
        }
    }

    public class HTMLSoftEdgeEffect : HTMLEffect {
        /// <summary>
        /// Link của hiệu ứng SoftEdge
        /// </summary>
        [JsonProperty(PropertyName = "sU")]
        public string SoftUrl { get; set; }
        public HTMLSoftEdgeEffect()
        {
            Type = HTMLEffectType.SoftEdge;
        }
    }
}
