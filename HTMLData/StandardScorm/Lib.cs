﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INV.Elearning.HTMLHelper.HTMLData.StandardScorm
{
    public class Lib
    {
        public static string AppPath = AppDomain.CurrentDomain.BaseDirectory;
        public static string ContentPath = Path.Combine(AppPath, "HTMLTemplate");
        public static string DriverPath = "Driver";
        public static string CDPath = "HTMLTemplate\\CD";
        public static string Scorm2004Path = ContentPath + "\\SCORM2004";
        public static string Scorm12Path = ContentPath + "\\SCORM12\\main";
        public static string Xapi = ContentPath + "\\Xapi\\config";

        public static string Tab(int level)
        {
            return "".PadLeft(level * 4);
        }
        public static string Line(int number)
        {
            string k = "";
            for (int i = 0; i < number; i++)
            {
                k += Environment.NewLine;
            }
            return k;
        }
        #region Search and copy folder & file content
        public static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
        {
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);
            DirectoryInfo[] dirs = dir.GetDirectories();
            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }
            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                string temppath = System.IO.Path.Combine(destDirName, file.Name);
                file.CopyTo(temppath, true);
            }
            if (copySubDirs)
            {

                foreach (DirectoryInfo subdir in dirs)
                {
                    string temppath = System.IO.Path.Combine(destDirName, subdir.Name);
                    DirectoryCopy(subdir.FullName, temppath, copySubDirs);
                }
            }
        }
        public static void FileToDirectoryCopy(string _sourceDirectory, string _destDirectory)
        {
            DirectoryInfo dirInfo = new DirectoryInfo(_sourceDirectory);
            FileInfo[] files = dirInfo.GetFiles();
            foreach (var file in files)
            {
                string destFile = Path.Combine(_destDirectory, file.Name);
                file.CopyTo(destFile, true);
            }
        }
        public static List<String> DirectorySearch(string sDir, string dirName)
        {
            List<String> files = new List<String>();
            try
            {
                foreach (string f in Directory.GetFiles(sDir))
                {
                    files.Add(f);
                }
                foreach (string d in Directory.GetDirectories(sDir))
                {
                    string _dirName = new DirectoryInfo(d).Name;
                    files.AddRange(DirectorySearch(d, _dirName));
                }
            }
            catch (System.Exception ex)
            {
            }
            return files;
        }
        #endregion
    }
    public enum ExportType
    {
        Xapi,
        CDRoom,
        Scorm12,
        Scorm2004
    }
}
