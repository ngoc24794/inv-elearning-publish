﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INV.Elearning.HTMLHelper.HTMLData.StandardScorm
{
    public class Info
    {
        private string _id = "http://sieuthigiaoduc.com/";
        public string ID
        {
            get { return _id; }
        }
        private string _type = "http://adlnet.gov/expapi/activities/course";
        public string Type
        {
            get { return _type; }
        }
        private string _nameFile = string.Empty;
        public string NameFile
        {
            get { return _nameFile; }
            set { _nameFile = value; }
        }
        private string _title = string.Empty;
        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }
        private string _desc = string.Empty;
        public string Desc
        {
            get { return _desc; }
            set { _desc = value; }
        }
        private string _path = string.Empty;
        public string Path
        {
            set { _path = value; }
            get { return _path; }
        }
        private string _identifier = string.Empty;

        public string Identifier
        {
            get { return _identifier; }
            set { _identifier = value; }
        }

    }
}
