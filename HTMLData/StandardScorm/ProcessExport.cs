﻿using Antlr4.StringTemplate;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INV.Elearning.HTMLHelper.HTMLData.StandardScorm
{
    public class ProcessExport
    {
        //Thư mục đích dùng để zip
        string destFolder = string.Empty;
        public ProcessExport(string rootfolder)
        {
            this.destFolder = rootfolder;
        }
        public void ExportXapi(Info _info)
        {
            if (!string.IsNullOrEmpty(_info.Path))
            {
                var str = "HTMLTemplate\\XAPI\\";
                Lib.DirectoryCopy(Lib.Xapi + "", destFolder, true);
                string pathTinCan = HTMLFileHelper.GetValueFromFile(Path.Combine( str, "tincan.stg"));
                Template tincanTemplate = new Template(pathTinCan, '$', '$');
                tincanTemplate.Add("id", _info.ID);
                tincanTemplate.Add("Name", _info.Title);
                tincanTemplate.Add("Desc", _info.Desc);
                HTMLFileHelper.SetValueToFile(destFolder, "tincan", ".xml", tincanTemplate.Render());
            }
        }
        public void ExportCDRoom(Info _info)
        {
            //Sao chép nội dung
            Lib.DirectoryCopy(Lib.ContentPath + "\\CD", destFolder, true);
            //Sao chép file autorun.inf
        }
        public void ExportScorm12(Info _info)
        {
            //Sao chép thư mục scorm
            Lib.DirectoryCopy(Lib.Scorm12Path + "", destFolder, true);
            //Sao chép nội dung vào thư mục scorm
            //Lib.DirectoryCopy(Lib.ContentPath, rootFolder, true);
            //Cấu hình nội dung file imsmanifest.xml
            string pathImsmanifest = HTMLFileHelper.GetValueFromFile(Path.Combine( "HTMLTemplate\\SCORM12\\", "imsmanifest1.2.stg"));
            Template imsmanifestTemplate = new Template(pathImsmanifest, '$', '$');
            imsmanifestTemplate.Add("Title", _info.Title);
            imsmanifestTemplate.Add("identifier", _info.Identifier);
            //Thêm các file resources tại đây
            string strFile = string.Empty;
            List<string> _lstAssets = Lib.DirectorySearch(destFolder, "");
            if (_lstAssets.Count > 0)
            {
                foreach (string _name in _lstAssets)
                {
                    string _href = _name.Substring(destFolder.Length, _name.Length - destFolder.Length);
                    strFile += Lib.Tab(3) + "<file href=" + '"' + _href + '"' + " />" + Lib.Line(1);
                }
            }
            imsmanifestTemplate.Add("resource", strFile);
            HTMLFileHelper.SetValueToFile(destFolder, "imsmanifest", ".xml", imsmanifestTemplate.Render());
        }
        public void ExportScorm2004(Info _info,string edition)
        {
            var str = "";
            //Sao chép thư mục scorm
            //Lib.DirectoryCopy(Lib.Scorm2004Path, destFolder, true);
            //Sao chép nội dung vào thư mục scorm
            if(edition== "SecondEdition")
            {
                str = "\\2004_2nd";
            }
            else if(edition== "ThirdEdition")
            {
                str = "\\2004_3rd";
            }
            else
            {
                str = "\\2004_4th";
            }
            //Cấu hình nội dung file imsmanifest.xml
            string pathImsmanifest = HTMLFileHelper.GetValueFromFile(Path.Combine("HTMLTemplate\\SCORM2004"+ str, "imsmanifest.stg"));
            Template imsmanifestTemplate = new Template(pathImsmanifest, '$', '$');
            imsmanifestTemplate.Add("Title", _info.Title);
            imsmanifestTemplate.Add("identifier", _info.Identifier);

            //Thêm các file resources tại đây
            string strFile = string.Empty;
            List<string> _lstAssets = Lib.DirectorySearch(destFolder, "");
            if (_lstAssets.Count > 0)
            {
                foreach (string _name in _lstAssets)
                {
                    if (!_name.Contains("imsss_v1p0.xsd") &&
                        !_name.Contains("adlcp_v1p3.xsd") &&
                        !_name.Contains("adlnav_v1p3.xsd") &&
                        !_name.Contains("adlseq_v1p3.xsd") &&
                        !_name.Contains("imscp_v1p1.xsd") &&
                        !_name.Contains("index.html"))
                    {
                        string _href = _name.Substring(destFolder.Length, _name.Length - destFolder.Length);
                        strFile += Lib.Tab(3) + "<file href=" + '"' + _href + '"' + " />" + Lib.Line(1);
                    }
                }
            }
            imsmanifestTemplate.Add("resource", strFile);
            HTMLFileHelper.SetValueToFile(destFolder, "imsmanifest", ".xml", imsmanifestTemplate.Render());
            Lib.DirectoryCopy(Lib.Scorm2004Path + str + "\\main", destFolder, true);
        }
    }
}
