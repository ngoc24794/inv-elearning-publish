﻿using INV.Elearning.Trigger;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INV.Elearning.HTMLHelper.HTMLData.TriggerClass
{
    public class HTMLConditionData
    {
        [JsonProperty(PropertyName = "Lo")]
        [JsonConverter(typeof(StringEnumConverter))]
        public EValue Logic { get; set; }

        [JsonProperty(PropertyName = "typ")]
        [JsonConverter(typeof(StringEnumConverter))]
        public EConditionType Type { get; set; }

        [JsonProperty(PropertyName = "vC")]
        public HTMLVariableConditionData VariableCondition { get; set; }

        [JsonProperty(PropertyName = "shC")]
        public HTMLShapeConditionData ShapeCondition { get; set; }

        [JsonProperty(PropertyName = "wC")]
        [JsonConverter(typeof(StringEnumConverter))]
        public EValue WindowCondition { get; set; }

    }
}
