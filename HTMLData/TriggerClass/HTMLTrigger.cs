﻿using INV.Elearning.HTMLHelper.HTMLData.TriggerClass;
using INV.Elearning.Trigger;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;

namespace INV.Elearning.HTMLHelper.HTMLData.Trigger
{
    public class HTMLTrigger
    {
        [JsonProperty(PropertyName = "A")]
        [JsonConverter(typeof(StringEnumConverter))]
        public EAction Action { get; set; }

        [JsonProperty(PropertyName = "S")]
        public string Source { get; set; }

        [JsonProperty(PropertyName = "sD")]
        public HTMLSourceDetail SourceDetail { get; set; }
        [JsonProperty(PropertyName = "E")]
        [JsonConverter(typeof(StringEnumConverter))]
        public EEvent Event { get; set; }

        [JsonProperty(PropertyName = "Ta")]
        public string Target { get; set; }

        [JsonProperty(PropertyName = "tD")]
        public HTMLTargetDetail TargetDetail { get; set; }

        [JsonProperty(PropertyName = "C")]
        public List<HTMLConditionData> Conditions { get; set; }
    }
}
