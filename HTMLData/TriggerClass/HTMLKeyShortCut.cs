﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace INV.Elearning.HTMLHelper
{
    public class HTMLKeyShortCut
    {
        public HTMLKeyShortCut(bool isControl, bool isShift, bool isAlt, bool isWindow, Key key)
        {
            IsControl = isControl;
            IsShift = isShift;
            IsAlt = isAlt;
            IsWindow = isWindow;
            Key = key;
        }
        [JsonProperty(PropertyName = "iC")]
        public bool IsControl { get; set; }

        [JsonProperty(PropertyName = "iS")]
        public bool IsShift { get; set; }

        [JsonProperty(PropertyName = "iA")]
        public bool IsAlt { get; set; }

        [JsonProperty(PropertyName = "iW")]
        public bool IsWindow { get; set; }

        [JsonProperty(PropertyName = "K")]
        [JsonConverter(typeof(StringEnumConverter))]
        public Key Key { get; set; }
    }
}
