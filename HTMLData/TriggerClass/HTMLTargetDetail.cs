﻿using INV.Elearning.Trigger;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INV.Elearning.HTMLHelper
{
    public class HTMLTargetDetail
    {
        public HTMLTargetDetail(bool restoreOnMouseLeave, EValue value, string objectId, HTMLKeyShortCut keyShortCut, TimelineReachesData timelineReachesModel, List<string> objectsId)
        {
            RestoreOnMouseLeave = restoreOnMouseLeave;
            Value = value;
            ObjectId = objectId;
            KeyShortCut = keyShortCut;
            TimelineReachesModel = timelineReachesModel;
            ObjectsId = objectsId;
        }
        [JsonProperty(PropertyName = "rMl")]
        public bool RestoreOnMouseLeave { get; set; }

        [JsonProperty(PropertyName = "val")]
        [JsonConverter(typeof(StringEnumConverter))]
        public EValue Value { get; set; }

        [JsonProperty(PropertyName = "oI")]
        public string ObjectId { get; set; }

        [JsonProperty(PropertyName = "kS")]
        public HTMLKeyShortCut KeyShortCut { get; set; }

        [JsonProperty(PropertyName = "tRm")]
        public TimelineReachesData TimelineReachesModel { get; set; }

        [JsonProperty(PropertyName = "osI")]
        public List<string> ObjectsId { get; set; }
    }
}
