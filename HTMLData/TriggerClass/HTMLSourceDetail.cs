﻿using INV.Elearning.Trigger;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INV.Elearning.HTMLHelper
{
    public class HTMLSourceDetail
    {
        public HTMLSourceDetail(HTMLBrowserWindowModel browserWindow, bool navigationControls, EValue value, string objectId, HTMLVariableDetailModel variableDetailModel)
        {
            BrowserWindow = browserWindow;
            NavigationControls = navigationControls;
            Value = value;
            ObjectId = objectId;
            VariableDetailModel = variableDetailModel;
        }

        [JsonProperty(PropertyName = "bW")]
        public HTMLBrowserWindowModel BrowserWindow { get; set; }

        [JsonProperty(PropertyName = "nC")]
        public bool NavigationControls { get; set; }

        [JsonProperty(PropertyName = "val")]
        [JsonConverter(typeof(StringEnumConverter))]
        public EValue Value { get; set; }

        [JsonProperty(PropertyName = "oI")]
        public string ObjectId { get; set; }

        [JsonProperty(PropertyName = "vDm")]
        public HTMLVariableDetailModel VariableDetailModel { get; set; }
    }
}
