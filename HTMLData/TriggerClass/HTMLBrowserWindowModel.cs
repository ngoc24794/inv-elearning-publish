﻿using INV.Elearning.Trigger;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INV.Elearning.HTMLHelper
{
    public class HTMLBrowserWindowModel
    {
        public HTMLBrowserWindowModel(bool isCurrentBrowser, bool isNewBrowser, EValue browserControl, EValue windowSize, double width, double height)
        {
            IsCurrentBrowser = isCurrentBrowser;
            IsNewBrowser = isNewBrowser;
            BrowserControl = browserControl;
            WindowSize = windowSize;
            Width = width;
            Height = height;
        }

        [JsonProperty(PropertyName = "iCb")]
        public bool IsCurrentBrowser { get; set; }

        [JsonProperty(PropertyName = "iN")]
        public bool IsNewBrowser { get; set; }

        [JsonProperty(PropertyName = "brC")]
        [JsonConverter(typeof(StringEnumConverter))]
        public EValue BrowserControl { get; set; }

        [JsonProperty(PropertyName = "wS")]
        [JsonConverter(typeof(StringEnumConverter))]
        public EValue WindowSize { get; set; }

        [JsonProperty(PropertyName = "w")]
        public double Width { get; set; }

        [JsonProperty(PropertyName = "h")]
        public double Height { get; set; }
    }
}
