﻿using INV.Elearning.Trigger;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INV.Elearning.HTMLHelper.HTMLData.TriggerClass
{
    public class HTMLVariableConditionData
    {
        [JsonProperty(PropertyName = "V")]
        public string Variable { get; set; }

        [JsonProperty(PropertyName = "O")]
        [JsonConverter(typeof(StringEnumConverter))]
        public EValue Operator { get; set; }

        [JsonProperty(PropertyName = "vT")]
        [JsonConverter(typeof(StringEnumConverter))]
        public EValue ValueType { get; set; }

        [JsonProperty(PropertyName = "val")]
        public string Value { get; set; }

        [JsonProperty(PropertyName = "mi")]
        public double Min { get; set; }

        [JsonProperty(PropertyName = "ma")]
        public double Max { get; set; }
    }
}
