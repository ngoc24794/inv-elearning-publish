﻿using INV.Elearning.Trigger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INV.Elearning.HTMLHelper.HTMLData.TriggerClass
{
    public static class ListConditionDataExtension
    {
        public static List<HTMLConditionData> GetHTML(this List<ConditionData> list)
        {
            List<HTMLConditionData> result = new List<HTMLConditionData>();
            if (list != null)
            {
                foreach (ConditionData item in list)
                {
                    if (item == null) continue;
                    HTMLConditionData hTMLConditionData = new HTMLConditionData()
                    {
                        Logic = item.Logic,
                        Type = item.Type,
                        ShapeCondition = new HTMLShapeConditionData()
                        {

                        },
                        WindowCondition = item.WindowCondition
                    };
                    if (item.VariableCondition != null)
                    {
                        hTMLConditionData.VariableCondition = new HTMLVariableConditionData()
                        {
                            Variable = item.VariableCondition.Variable,
                            Operator = item.VariableCondition.Operator,
                            ValueType = item.VariableCondition.ValueType,
                            Value = item.VariableCondition.Value,
                            Min = item.VariableCondition.Min,
                            Max = item.VariableCondition.Max
                        };
                    }
                    result.Add(hTMLConditionData);
                }
            }
            return result;
        }
    }

    public static class SourceDetailExtension
    {
        public static HTMLSourceDetail GetHTML(this SourceDetailData sourceDetail)
        {
            if (sourceDetail != null)
            {
                return new HTMLSourceDetail(sourceDetail.BrowserWindow.GetHTML(), sourceDetail.NavigationControls, sourceDetail.Value, sourceDetail.ObjectId, sourceDetail.VariableDetailModel.GetHTML());
            }
            return null;
        }
    }

    public static class BrowserWindowModelExtension
    {
        public static HTMLBrowserWindowModel GetHTML(this BrowserWindowData model)
        {
            if (model != null)
            {
                return new HTMLBrowserWindowModel(model.IsCurrentBrowser, model.IsNewBrowser, model.BrowserControl, model.WindowSize, model.Width, model.Height);
            }
            return null;
        }
    }

    public static class VariableDetailModelExtension
    {
        public static HTMLVariableDetailModel GetHTML(this VariableDetailData data)
        {
            if (data != null)
            {
                return new HTMLVariableDetailModel(data.Operator, data.ValueType, data.Value, data.Min, data.Max);
            }
            return null;
        }
    }

    public static class TargetDetailExtension
    {
        public static HTMLTargetDetail GetHTML(this TargetDetailData targetDetail)
        {
            if (targetDetail != null)
            {
                return new HTMLTargetDetail(targetDetail.RestoreOnMouseLeave, targetDetail.Value, targetDetail.ObjectId, targetDetail.KeyShortCut.GetHTML(), targetDetail.TimelineReachesModel, targetDetail.ObjectsId);
            }
            return null;
        }
    }

    public static class KeyShortCutExtension
    {
        public static HTMLKeyShortCut GetHTML(this KeyShortCut model)
        {
            if (model != null)
            {
                return new HTMLKeyShortCut(model.IsControl, model.IsShift, model.IsAlt, model.IsWindow, model.Key);
            }
            return null;
        }
    }
}
