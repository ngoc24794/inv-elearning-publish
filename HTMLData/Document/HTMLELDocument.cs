﻿using INV.Elearning.Core.Model;
using INV.Elearning.HTMLHelper.HTMLData.PlayerClass;
using INV.Elearning.HTMLHelper.HTMLData.SlideBase;
using INV.Elearning.Player.ViewModel;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace INV.Elearning.HTMLHelper.HTMLData.Document
{
    public class HTMLELDocument
    {
        /// <summary>
        /// License
        /// </summary>
        [JsonProperty(PropertyName = "iLis")]
        public bool IsLicense { get; set; }
        /// <summary>
        /// Ngôn ngữ
        /// </summary>
        [JsonProperty(PropertyName = "Lang")]
        public string Language { get; set; }
        /// <summary>
        /// Id của bài giảng
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public string ID { get; set; }
        /// <summary>
        /// Tên bài giảng
        /// </summary>
        [JsonProperty(PropertyName = "nm")]
        public string Name { get; set; }
        /// <summary>
        /// Danh sách các slide
        /// </summary>
        [JsonProperty(PropertyName = "slds")]
        public List<HTMLSlideBase> Slides { get; set; }
        /// <summary>
        /// Danh sách biến
        /// </summary>
        [JsonProperty(PropertyName = "vars")]
        public List<HTMLVariable> Variables { get; set; }

        /// <summary>
        /// Thuộc tính của player
        /// </summary>
        [JsonProperty(PropertyName = "plr")]
        public HTMLPlayer Player { get; set; }

        /// <summary>
        /// Có tồn tại Scorm không
        /// </summary>
        [JsonProperty(PropertyName = "iS")]
        public bool Scorm { get; set; }

        /// <summary>
        /// kiểm tra Preview
        /// </summary>
        [JsonProperty(PropertyName = "iPv")]
        public bool IsPreview { get; set; }

        /// <summary>
        /// Key lưu local
        /// </summary>
        [JsonProperty(PropertyName = "k")]
        public string Key { get; set; }

        /// <summary>
        /// Thuộc tính của scorm
        /// </summary>
        [JsonProperty(PropertyName = "scp")]
        public HTMLScorm ScormProp { get; set; }

        /// <summary>
        /// Thuộc tính của Tracking scorm
        /// </summary>
        [JsonProperty(PropertyName = "sctrc")]
        public HTMLTracking ScormTracking { get; set; }
    }
}
