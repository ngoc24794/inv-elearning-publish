﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;

namespace INV.Elearning.HTMLHelper.HTMLData.ColorClass
{
    /// <summary>
    /// Lớp màu gốc
    /// </summary>
    public abstract class HTMLColorBase
    {
        /// <summary>
        /// Độ trong suốt của màu
        /// </summary>
        [JsonProperty(PropertyName = "opc")]
        public double Opacity { get; set; }
        /// <summary>
        /// Loại màu
        /// </summary>
       [JsonProperty("CoT")]
        [JsonConverter(typeof(StringEnumConverter))]
        public HTMLColorType ColorType { get; set; }

        /// <summary>
        /// Các loại màu
        /// </summary>
        public enum HTMLColorType
        {
            /// <summary>
            /// Màu đơn
            /// </summary>
            Solid,
            /// <summary>
            /// Màu dải
            /// </summary>
            Gradient,
            /// <summary>
            /// Hình ảnh
            /// </summary>
            Image
        }
    }

    /// <summary>
    /// Màu đơn
    /// </summary>
    public class HTMLSolid : HTMLColorBase
    {
        /// <summary>
        /// Màu sắc
        /// </summary>
        [JsonProperty(PropertyName = "col")]
        public string Color { get; set; }
        /// <summary>
        /// Khởi tạo mặc định
        /// </summary>
        public HTMLSolid()
        {
            this.ColorType = HTMLColorType.Solid;
        }
    }

    /// <summary>
    /// Lớp lưu trữ dữ liệu dải màu
    /// </summary>
    public class HTMLGradient : HTMLColorBase
    {
        /// <summary>
        /// Các loại đỗ màu dải
        /// </summary>
        public enum HTMLGradientType
        {
            /// <summary>
            /// Đường thẳng
            /// </summary>
            Linear,
            /// <summary>
            /// Cung tròn
            /// </summary>
            Radial
        }
        /// <summary>
        /// Kiểu đỗ màu
        /// </summary>
        [JsonProperty(PropertyName = "gT")]
        [JsonConverter(typeof(StringEnumConverter))]
        public HTMLGradientType GradientType { get; set; }
        /// <summary>
        /// Tọa độ X bắt đầu đổ
        /// </summary>
        [JsonProperty(PropertyName = "x1")]
        public double X1 { get; set; }
        /// <summary>
        /// Tọa độ Y bắt đầu đổ
        /// </summary>
        [JsonProperty(PropertyName = "y1")]
        public double Y1 { get; set; }
        /// <summary>
        /// Tọa độ X kết thúc đỗ
        /// </summary>
        [JsonProperty(PropertyName = "x2")]
        public double X2 { get; set; }
        /// <summary>
        /// Tọa độ Y kết thúc đỗ
        /// </summary>
        [JsonProperty(PropertyName = "y2")]
        public double Y2 { get; set; }
        /// <summary>
        /// Danh sách các điểm đổ
        /// </summary>
        [JsonProperty(PropertyName = "sts")]
        public List<HTMLGradientStop> Stops { get; set; }
        /// <summary>
        /// Tọa độ X điểm trung tâm
        /// </summary>
        [JsonProperty(PropertyName = "cX")]
        public double CenterX { get; set; }
        /// <summary>
        /// Tọa độ Y điểm trung tâm
        /// </summary>
        [JsonProperty(PropertyName = "cY")]
        public double CenterY { get; set; }
        /// <summary>
        /// Góc đổ màu
        /// </summary>
        [JsonProperty(PropertyName = "agl")]
        public double Angle { get; set; }
        /// <summary>
        /// Khởi tạo mặc định
        /// </summary>
        public HTMLGradient()
        {
            this.ColorType = HTMLColorType.Gradient;
        }

    }

    /// <summary>
    /// Lớp lưu trữ dữ liệu cho phần tử thuộc dải màu
    /// </summary>
    public class HTMLGradientStop
    {
        /// <summary>
        /// Vị trí đổ
        /// </summary>
        [JsonProperty(PropertyName = "ofs")]
        public double Offset { get; set; }

        /// <summary>
        /// Màu sắc
        /// </summary>
        [JsonProperty(PropertyName = "col")]
        public string Color { get; set; }
        /// <summary>
        /// Độ trong suốt
        /// </summary>
        /// 
        [JsonProperty(PropertyName = "opc")]
        public double Opacity { get; set; }

    }

    /// <summary>
    /// Lớp lưu trữ dữ liệu cho màu nền là hình ảnh
    /// </summary>
    public class HTMLImageColor : HTMLColorBase
    {
        /// <summary>
        /// Đường dẫn hình ảnh
        /// </summary>
        [JsonProperty(PropertyName = "iU")]
        public string ImageUrl { get; set; }
        /// <summary>
        /// Có lặp lại hay không
        /// </summary>
        [JsonProperty(PropertyName = "iR")]
        public bool IsRepeated { get; set; }
        [JsonProperty(PropertyName = "oX")]
        public double OffsetX { get; set; }
        [JsonProperty(PropertyName = "oY")]
        public double OffsetY { get; set; }
        [JsonProperty(PropertyName = "sX")]
        public double ScaleX { get; set; }
        [JsonProperty(PropertyName = "sY")]
        public double ScaleY { get; set; }
       [JsonProperty(PropertyName = "oL")]
        public double OffsetLeft { get; set; }
        [JsonProperty(PropertyName = "oR")]
        public double OffsetRight { get; set; }
        [JsonProperty(PropertyName = "oT")]
        public double OffsetTop { get; set; }
        [JsonProperty(PropertyName = "oB")]
        public double OffsetBottom { get; set; }
        [JsonProperty(PropertyName = "iRt")]
        public bool IsRotateWidthShape { get; set; }
        [JsonProperty(PropertyName = "ow")]
        public double OriginWidth { get; set; }
        [JsonProperty(PropertyName = "oh")]
        public double OriginHeight { get; set; }
        [JsonProperty(PropertyName = "iP")]
        public bool IsPattern { get; set; }
        [JsonProperty("iA")]
        [JsonConverter(typeof(StringEnumConverter))]
        public HTMLImageAlign ImageAlign { get; set; }
        [JsonProperty("mT")]
        [JsonConverter(typeof(StringEnumConverter))]
        public HTMLImageMirrorType MirrorType { get; set; }

        public HTMLImageColor()
        {
            this.ColorType = HTMLColorType.Image;
        }

        /// <summary>
        /// Các loại căn lề hình ảnh
        /// </summary>
        public enum HTMLImageAlign
        {            
            TopLeft = 0,
            Top = 1,
            TopRight = 2,
            Left = 3,
            Center = 4,
            Right = 5,
            BottomLeft = 6,
            Bottom = 7,
            BottomRight = 8
        }

        /// <summary>
        /// Các loại tương phản
        /// </summary>
        public enum HTMLImageMirrorType
        {
            None,
            Horizontal,
            Vertical,
            Both
        }
    }
}
