﻿using INV.Elearning.Text.Html;
using Newtonsoft.Json;

namespace INV.Elearning.HTMLHelper.HTMLData.TextClass
{
    public class HTMLWord
    {
        /// <summary>
        /// Nội dung chữ
        /// </summary>
        [JsonProperty(PropertyName = "txt")]
        public string Text { get; set; }
        /// <summary>
        /// Vị trí của chữ (trên, trái)
        /// </summary>
        [JsonProperty(PropertyName = "l")]
        public double Left { get; set; }

        [JsonProperty(PropertyName = "T")]
        public double Top { get; set; }
        /// <summary>
        /// Độ dài chữ
        /// </summary>
        [JsonProperty(PropertyName = "tw")]
        public double TextWidth { get; set; }
        /// <summary>
        /// Độ cao chữ
        /// </summary>
        [JsonProperty(PropertyName = "h")]
        public double Height { get; set; }
        /// <summary>
        /// Vị trí underline
        /// </summary>
        [JsonProperty(PropertyName = "un")]
        public HtmlUnderline HtmlUnderline { get; set; }
    }
}
