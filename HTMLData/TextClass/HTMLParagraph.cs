﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace INV.Elearning.HTMLHelper.HTMLData.TextClass
{
    public class HTMLParagraph
    {
        [JsonProperty(PropertyName = "cSc")]
        public double CoefficientSizeChange { get; set; }
        /// <summary>
        /// Danh sách các chữ có chung thuộc tính
        /// </summary>
        [JsonProperty(PropertyName = "inls")]
        public List<HTMLInline> Inlines { get; set; }
    }
}
