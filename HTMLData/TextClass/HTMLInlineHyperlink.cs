﻿using Newtonsoft.Json;

namespace INV.Elearning.HTMLHelper.HTMLData.TextClass
{
    public class HTMLInlineHyperlink:HTMLInline
    {
        /// <summary>
        /// Địa chỉ link của text
        /// </summary>
        [JsonProperty(PropertyName = "ad")]
        public string Address { get; set; }
    }
}
