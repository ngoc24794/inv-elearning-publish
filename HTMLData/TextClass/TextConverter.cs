﻿namespace INV.Elearning.HTMLHelper.HTMLData
{
    using INV.Elearning.Controls;
    using System.Windows.Media;
    using sys = System.Windows;
    public  class TextConverter
    {
        /// <summary>
        /// Chuyển đổi kiểu chữ in nghiêng sang dữ liệu lưu trữ
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string FontStyleConverter(sys.FontStyle source)
        {
            return source.Equals(sys.FontStyles.Italic) ? "Italic" : "Normal";
        }

        /// <summary>
        /// Chuyển đổi từ dữ liệu lưu trữ sang kiểu in nghiêng
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static sys.FontStyle FontStyleConverter(string source)
        {
            return source == "Italic" ? sys.FontStyles.Italic : sys.FontStyles.Normal;
        }



        /// <summary>
        /// Chuyển đổi kiểu chữ đậm sang dữ liệu lưu trữ
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string FontWeightConverter(sys.FontWeight source)
        {
            return source.Equals(sys.FontWeights.Bold) ? "Bold" : "Normal";
        }
        /// <summary>
        /// Chuyển đổi từ dữ liệu lưu trữ sang kiểu đậm
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static sys.FontWeight FontWeightConverter(string source)
        {
            return source == "Bold" ? sys.FontWeights.Bold : sys.FontWeights.Normal;
        }
        public static SolidColor ColorConverter(SolidColorBrush color)
        {
            return new SolidColor() { Color = color != null ? color.ToString() : "Black" };
        }
    }
}
