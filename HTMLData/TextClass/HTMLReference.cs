﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INV.Elearning.HTMLHelper.HTMLData.TextClass
{
    public class HTMLReference
    {
        /// <summary>
        /// Reference Text
        /// </summary>
        [JsonProperty(PropertyName = "rStr")]
        public string ReferenceText { get; set; }
        /// <summary>
        /// Reference Text
        /// </summary>
        [JsonProperty(PropertyName = "t")]
        public double Top { get; set; }
    }
}
