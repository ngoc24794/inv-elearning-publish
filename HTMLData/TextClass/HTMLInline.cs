﻿using INV.Elearning.HTMLHelper.HTMLData.ColorClass;
using INV.Elearning.Text.Html;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace INV.Elearning.HTMLHelper.HTMLData.TextClass
{
    public class HTMLInline
    {
        /// <summary>
        /// Kích cỡ của chữ
        /// </summary>
        [JsonProperty(PropertyName = "fs")]
        public double FontSize { get; set; }
        /// <summary>
        /// Chỉ số trên dưới
        /// </summary>
        [JsonProperty(PropertyName = "sOf")]
        public double ScripOffsets { get; set; }
        /// <summary>
        /// Kiểu chữ
        /// </summary>
        [JsonProperty(PropertyName = "ff")]
        public string FontFamily { get; set; }
        /// <summary>
        /// Chữ đậm hay nhạt
        /// </summary>
        [JsonProperty(PropertyName = "fw")]
        public string FontWeight { get; set; }
        /// <summary>
        /// Chữ in nghiêng hay thường
        /// </summary>
        [JsonProperty(PropertyName = "fst")]
        public string FontStyle { get; set; }
        /// <summary>
        /// Màu chữ
        /// </summary>
        [JsonProperty(PropertyName = "col")]
        public HTMLColorBase Color { get; set; }
        /// <summary>
        /// Độ mờ của chữ
        /// </summary>
        [JsonProperty(PropertyName = "opt")]
        public double OpacityText { get; set; }
        /// <summary>
        /// Loại gạch chân chữ
        /// </summary>
        [JsonProperty(PropertyName = "u")]
        public string Underline { get; set; }
        /// <summary>
        /// Màu của đường gạch chân chữ
        /// </summary>
        [JsonProperty(PropertyName = "uCol")]
        public HTMLColorBase ColorUnderline { get; set; }
        /// <summary>
        /// Loại gạch ngang chữ
        /// </summary>
        [JsonProperty(PropertyName = "stk")]
        public string StrikeThrough { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty(PropertyName = "T")]
        public double Top { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty(PropertyName = "Ls")]
        public double[] Lefts { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty(PropertyName = "Txt")]
        public string Text { get; set; }

        [JsonProperty(PropertyName = "w")]
        public double Width { get; set; }
        [JsonProperty(PropertyName = "Ul")]
        public HtmlUnderline HtmlUnderline { get; set; }

        /// <summary>
        /// Danh sách các chữ
        /// </summary>
        //[JsonProperty(PropertyName = "ws")]
        //public List<HTMLWord> Words { get; set; }
    }
}
