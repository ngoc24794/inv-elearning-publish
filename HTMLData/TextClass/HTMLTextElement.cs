﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace INV.Elearning.HTMLHelper.HTMLData.TextClass
{
    public class HTMLTextElement : HTMLStandardElement
    {
        public HTMLTextElement()
        {
            this.ContentKeyType = "Text";
        }
        /// <summary>
        /// Danh sách chữ trong đối tượng text
        /// </summary>
        [JsonProperty(PropertyName = "pr")]
        public List<HTMLParagraph> Paragraphs { get; set; }
        /// <summary>
        /// Reference Text
        /// </summary>
        [JsonProperty(PropertyName = "ref")]
        public HTMLReference Reference { get; set; }
    }
}
