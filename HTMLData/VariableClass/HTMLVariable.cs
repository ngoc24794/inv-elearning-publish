﻿using INV.Elearning.Trigger;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace INV.Elearning.HTMLHelper
{
    public class HTMLVariable
    {
        public HTMLVariable(string iD, string name, EVariableType type, string value)
        {
            ID = iD;
            Name = name;
            Type = type;
            Value = value;
        }
        [JsonProperty(PropertyName = "id")]
        public string ID { get; set; }

        [JsonProperty(PropertyName = "nm")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "typ")]
        [JsonConverter(typeof(StringEnumConverter))]
        public EVariableType Type { get; set; }

        [JsonProperty(PropertyName = "val")]
        public string Value { get; set; }
    }
}
