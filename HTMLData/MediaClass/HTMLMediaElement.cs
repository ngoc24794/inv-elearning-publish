﻿using Newtonsoft.Json;

namespace INV.Elearning.HTMLHelper.HTMLData.MediaClass
{
    public abstract class HTMLMediaElement : HTMLStandardElement
    {
        /// <summary>
        /// Đường dẫn media
        /// </summary>
        [JsonProperty(PropertyName = "mU")]
        public string MediaUrl { get; set; }
        /// <summary>
        /// Đường dẫn caption
        /// </summary>
        [JsonProperty(PropertyName = "cp")]
        public string Caption { get; set; }

    }

    public class HTMLVideoElement : HTMLMediaElement
    {
        [JsonProperty(PropertyName = "pV")]
        public string PlayVideo { get; set; }
        [JsonProperty(PropertyName = "cpr")]
        public string Compression { get; set; }
        [JsonProperty(PropertyName = "sV")]
        public string ShowVideo { get; set; }
        [JsonProperty(PropertyName = "sC")]
        public string ShowControl { get; set; }
        [JsonProperty(PropertyName = "vl")]
        public double Volume { get; set; }
        public HTMLVideoElement()
        {
            ContentKeyType = "Video";
        }
    }

    public class HTMLYoutubeElement : HTMLMediaElement
    {
        [JsonProperty(PropertyName = "idVi")]
        public string IDVIdeo { get; set; }
        [JsonProperty(PropertyName = "urlvi")]
        public string URLVideo { get; set; }
        [JsonProperty(PropertyName = "fullscr")]
        public bool IsAllowFullScreen { get; set; }
        [JsonProperty(PropertyName = "hiCo")]
        public bool IsHideControl { get; set; }
        [JsonProperty(PropertyName = "hiAn")]
        public bool IsHideAnnotation { get; set; }
        [JsonProperty(PropertyName = "aupl")]
        public bool IsAutoPlay { get; set; }
        [JsonProperty(PropertyName = "sppa")]
        public bool IsSpecificPart { get; set; }
        [JsonProperty(PropertyName = "st")]
        public double StartTime { get; set; }
        [JsonProperty(PropertyName = "end")]
        public double EndTime { get; set; }

        public HTMLYoutubeElement()
        {
            ContentKeyType = "Youtube";
        }
    }

    public class HTMLAudioElement : HTMLMediaElement
    {
        public HTMLAudioElement()
        {
            ContentKeyType = "Audio";
        }
        /// <summary>
        /// play across slides
        /// </summary>
        [JsonProperty(PropertyName = "Acr")]
        public bool IsAcross { get; set; }
        /// <summary>
        /// Loop util stop
        /// </summary>
        [JsonProperty(PropertyName = "lp")]
        public bool IsLoop { get; set; }
        /// <summary>
        /// Loop util stop
        /// </summary>
        [JsonProperty(PropertyName = "vl")]
        public string Volume { get; set; }

    }

    public class HTMLFlashElement : HTMLStandardElement
    {
        [JsonProperty(PropertyName = "fU")]
        public string FlashUrl { get; set; }
        public HTMLFlashElement()
        {
            ContentKeyType = "Flash";
        }
    }
}
