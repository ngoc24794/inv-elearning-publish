﻿using INV.Elearning.HTMLHelper.HTMLData.ColorClass;
using INV.Elearning.HTMLHelper.HTMLData.Trigger;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace INV.Elearning.HTMLHelper.HTMLData.LayoutBase
{
    public class HTMLLayoutBase
    {
        /// <summary>
        /// Có phải main layout hay không
        /// </summary>
        [JsonProperty(PropertyName = "iM")]
        public bool IsMainLayout { set; get; }
        /// <summary>
        /// Loại của layout
        /// </summary>
        [JsonProperty(PropertyName = "lT")]
        public string LayoutKeyType { set; get; }
        /// <summary>
        /// ID của layout
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public string ID { get; set; }
        /// <summary>
        /// Tên layout
        /// </summary>
        [JsonProperty(PropertyName = "nm")]
        public string Name { get; set; }
        /// <summary>
        /// Màu của layout
        /// </summary>
        [JsonProperty(PropertyName = "bg")]
        public HTMLColorBase Background { get; set; }
        /// <summary>
        /// Layout ẩn hay hiện
        /// </summary>
        [JsonProperty(PropertyName = "iV")]
        public bool IsVisible { set; get; }
        /// <summary>
        /// Danh sách các đối tượng trong layout
        /// </summary>
        [JsonProperty(PropertyName = "e")]
        public List<HTMLObjectElement> Elements { get; set; }
        /// <summary>
        /// Hiệu ứng chuyển trang
        /// </summary>
        [JsonProperty(PropertyName = "trans")]
        public HTMLTransition Transition { get; set; }
        /// <summary>
        /// Thời gian
        /// </summary>
        [JsonProperty(PropertyName = "tim")]
        public HTMLTiming Timing { get; set; }
        [JsonProperty(PropertyName = "tri")]
        public List<HTMLTrigger> Triggers { get; set; }
        /// <summary>
        /// Layout Config
        /// </summary>
        [JsonProperty(PropertyName = "lC")]
        public HTMLLayoutConfig LayoutConfig { get; set; }
    }

}
