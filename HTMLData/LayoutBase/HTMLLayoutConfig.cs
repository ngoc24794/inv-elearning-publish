﻿using INV.Elearning.Core.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INV.Elearning.HTMLHelper.HTMLData.LayoutBase
{
    public class HTMLLayoutConfig
    { /// <summary>
      /// Ẩn tất cả các Layer khác
      /// </summary>
        [JsonProperty(PropertyName = "hO")]
        public bool HideOtherLayer { get; set; }
        /// <summary>
        /// Ẩn các đối tượng của Layer chính
        /// </summary>
        [JsonProperty(PropertyName = "Ob")]
        public bool HideObjectsOnBaseLayer { get; set; }
        /// <summary>
        /// Ẩn Layer khi kết thúc timeline
        /// </summary>
        [JsonProperty(PropertyName = "Wf")]
        public bool HideWhenTimelineFinished { get; set; }
        /// <summary>
        /// Ngăn chặn Click trên đối tượng ở Layer chính
        /// </summary>
        [JsonProperty(PropertyName = "pC")]
        public bool PreventClickToObjectsOnBaseLayer { get; set; }
        /// <summary>
        /// Dừng Timeline ở Layer chính
        /// </summary>
        [JsonProperty(PropertyName = "pT")]
        public bool PauseTimeOfBaseLayer { get; set; }
        /// <summary>
        /// Chế độ tìm kiếm
        /// </summary>
        [JsonProperty(PropertyName = "aS")]
        [JsonConverter(typeof(StringEnumConverter))]
        public AllowSeekingMode AllowSeekingMode { get; set; }
        /// <summary>
        /// Chế độ quay lại trang
        /// </summary>
        [JsonProperty(PropertyName = "rM")]
        [JsonConverter(typeof(StringEnumConverter))]
        public RevisitMode RevisitMode { get; set; }

        public void LoadData(PageLayerConfig source)
        {
            HideObjectsOnBaseLayer = source.HideObjectsOnBaseLayer;
            HideOtherLayer = source.HideOtherLayer;
            HideWhenTimelineFinished = source.HideWhenTimelineFinished;
            PreventClickToObjectsOnBaseLayer = source.PreventClickToObjectsOnBaseLayer;
            PauseTimeOfBaseLayer = source.PauseTimeOfBaseLayer;
            AllowSeekingMode = source.AllowSeekingMode;
            RevisitMode = source.RevisitMode;
        }

    }
}
