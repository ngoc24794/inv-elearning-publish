﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace INV.Elearning.HTMLHelper.HTMLData.AnimationClass
{
    public class HTMLEffectOptionGroupAnimation
    {
        /// <summary>
        /// Tên của nhóm tùy chỉnh hiệu ứng đối tượng (1 đối tượng có thể có nhiều tùy chỉnh hiệu ứng)
        /// </summary>
        [JsonProperty(PropertyName = "grNm")]
        public string GroupName { get; set; }
        /// <summary>
        /// Danh sách hiệu ứng đối tượng
        /// </summary>
        [JsonProperty(PropertyName = "nm")]
        public string Name { get; set; }
    }
}
