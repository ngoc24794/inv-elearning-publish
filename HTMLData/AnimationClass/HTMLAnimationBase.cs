﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace INV.Elearning.HTMLHelper.HTMLData.AnimationClass
{
    public abstract class HTMLAnimationBase
    {
        /// <summary>
        /// Tên của hiệu ứng
        /// </summary>
        [JsonProperty(PropertyName = "nm")]
        public string Name { get; set; }
        /// <summary>
        /// Nhóm các tùy chỉnh của hiệu ứng đối tượng
        /// </summary>
        [JsonProperty(PropertyName = "eO")]
        public List<HTMLEffectOptionGroupAnimation> HTMLEffectOptionGroupAnimation { get; set; }
        /// <summary>
        /// Thời gian diễn ra hiệu ứng đối tượng
        /// </summary>
        [JsonProperty(PropertyName = "dA")]
        public double HTMLDurationAnimation { get; set; }

        /// <summary>
        /// Thời gian diễn ra hiệu ứng đối tượng
        /// </summary>
        [JsonProperty(PropertyName = "sA")]
        public double HTMLSumDurationBeforeAnimation { get; set; }


        /// <summary>
        /// Loại hiệu ứng đối tượng (có 3 loại: Vào, ra và chuyển động theo đường path)
        /// </summary>
        [JsonProperty("typ")]
        [JsonConverter(typeof(StringEnumConverter))]
        public HTMLAnimationType Type { get; set; }
        public enum HTMLAnimationType
        {
            Entrance,
            Exit,
            MotionPath
        }
        /// <summary>
        /// Top của văn bản
        /// </summary>
        [JsonProperty(PropertyName = "ltp")]
        public List<double> listTopPara { get; set; }
    }

    public class HTMLEntranceAnimation : HTMLAnimationBase
    {
        public HTMLEntranceAnimation()
        {
            Type = HTMLAnimationType.Entrance;
        }
    }

    public class HTMLExitAnimation : HTMLAnimationBase
    {
        public HTMLExitAnimation()
        {
            Type = HTMLAnimationType.Exit;
        }
    }

    public class HTMLMotionPathAnimation : HTMLAnimationBase
    {
        public HTMLMotionPathAnimation()
        {
            Type = HTMLAnimationType.MotionPath;
        }

        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("dt")]
        public string Data { get; internal set; }
        [JsonProperty("r")]
        public Rect Rect { get; internal set; }
        [JsonProperty("agl")]
        public double Angle { get; internal set; }
    }

    public class HTMLPoint
    {
        public HTMLPoint()
        {
        }

        public HTMLPoint(double x, double y)
        {
            X = x;
            Y = y;
        }
        [JsonProperty(PropertyName = "x")]
        public double X { get; set; }
        [JsonProperty(PropertyName = "y")]
        public double Y { get; set; }
    }
}
