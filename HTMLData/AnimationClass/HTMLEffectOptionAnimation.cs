﻿using Newtonsoft.Json;

namespace INV.Elearning.HTMLHelper.HTMLData.AnimationClass
{
    public class HTMLEffectOptionAnimation
    {
        /// <summary>
        /// Tên tùy chỉnh hiệu ứng đối tượng
        /// </summary>
        [JsonProperty(PropertyName = "nm")]
        public string Name { get; set; }
    }
}
