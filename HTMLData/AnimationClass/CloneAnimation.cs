﻿using INV.Elearning.Animations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INV.Elearning.HTMLHelper.HTMLData.AnimationClass
{
    public class CloneAnimation
    {
        public static void CopyEffectOption(HTMLEffectOptionAnimation HTMLEffectOption, IEffectOption effectOption)
        {
            HTMLEffectOption.Name = effectOption.Name;
            // HTMLEffectOption.IsSelected = effectOption.IsSelected;
        }

        public  static void CopyEffectOptionGroup(HTMLEffectOptionGroupAnimation HTMLEffectOptionGroup, IEffectOptionGroup EffectOptionGroup)
        {
            HTMLEffectOptionGroup.GroupName = EffectOptionGroup.GroupName;
            HTMLEffectOptionAnimation HTMLEffectOption = new HTMLEffectOptionAnimation();
         //   HTMLEffectOptionGroup.HTMLEffectOptionAnimation = new List<HTMLEffectOptionAnimation>();

            foreach (var item in EffectOptionGroup.Values)
            {

                HTMLEffectOption = new HTMLEffectOptionAnimation();

                if (item is EffectOptionAdapter)
                {
                    foreach (var effectoption in (item as EffectOptionAdapter).Values)
                    {
                        if (effectoption.IsSelected)
                        {
                            HTMLEffectOption = new HTMLEffectOptionAnimation();
                            CopyEffectOption(HTMLEffectOption, effectoption);
                            HTMLEffectOptionGroup.Name = HTMLEffectOption.Name;
                         //   HTMLEffectOptionGroup.HTMLEffectOptionAnimation.Add(HTMLEffectOption);
                        }

                    }
                }
                else if (item is IEffectOption)
                {
                    if (item.IsSelected)
                    {
                        CopyEffectOption(HTMLEffectOption, item);
                        HTMLEffectOptionGroup.Name = HTMLEffectOption.Name;
                        // HTMLEffectOptionGroup.HTMLEffectOptionAnimation.Add(HTMLEffectOption);
                    }

                }

            }
        }

        public void CopyListEffectOptionGroup(List<HTMLEffectOptionGroupAnimation> listHTMLEffectOptionGroup, List<IEffectOptionGroup> listEffectOptionGroup)
        {
            listHTMLEffectOptionGroup = new List<HTMLEffectOptionGroupAnimation>();
            HTMLEffectOptionGroupAnimation HTMLEffectOptionGroup = new HTMLEffectOptionGroupAnimation();
            foreach (var item in listEffectOptionGroup)
            {
                HTMLEffectOptionGroup = new HTMLEffectOptionGroupAnimation();

                CopyEffectOptionGroup(HTMLEffectOptionGroup, item as IEffectOptionGroup);
                listHTMLEffectOptionGroup.Add(HTMLEffectOptionGroup);
            }
        }
    }
}
