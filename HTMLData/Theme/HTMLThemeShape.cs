﻿using INV.Elearning.HTMLHelper.HTMLData.ColorClass;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace INV.Elearning.HTMLHelper.HTMLData.Theme
{
    public abstract class HTMLThemeShape
    {
        /// <summary>
        /// ID của hình dạng Theme
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public string ID { get; set; }
        /// <summary>
        /// Tên hình dạng Theme
        /// </summary>
        [JsonProperty(PropertyName = "nm")]
        public string Name { get; set; }
        /// <summary>
        /// Chiều rộng của hình vẽ trên Theme
        /// </summary>
        [JsonProperty(PropertyName = "w")]
        public double Width { get; set; }
        /// <summary>
        /// Chiều dài của hình vẽ trên Theme
        /// </summary>
        [JsonProperty(PropertyName = "h")]
        public double Height { get; set; }
        /// <summary>
        /// Vị trí của hình vẽ trên Theme (trên, trái)
        /// </summary>
        [JsonProperty(PropertyName = "t")]
        public double Top { get; set; }
        [JsonProperty(PropertyName = "l")]
        public double Left { get; set; }
        /// <summary>
        /// Độ dày của đường viền hình vẽ trên Theme
        /// </summary>
        [JsonProperty(PropertyName = "sTh")]
        public double StrokeThickness { get; set; }
        /// <summary>
        /// Đường viền hình vẽ trên Theme
        /// </summary>
        [JsonProperty(PropertyName = "str")]
        public HTMLColorBase Stroke { get; set; }
        /// <summary>
        /// Màu của hình vẽ trên Theme
        /// </summary>
        [JsonProperty(PropertyName = "fi")]
        public HTMLColorBase Fill { get; set; }
        /// <summary>
        /// Loại hình vẽ trên Theme (có 3 loại: hình chữ nhật, elip và vẽ theo đường path)
        /// </summary>
        [JsonProperty("shT")]
        [JsonConverter(typeof(StringEnumConverter))]
        public HTMLThemeShapeType ShapeType { get; set; }

        public enum HTMLThemeShapeType
        {
            Rectangle,
            Ellipse,
            Path
        }
    }

    public class HTMLThemeRectangle : HTMLThemeShape
    {
        public HTMLThemeRectangle()
        {
            this.ShapeType = HTMLThemeShapeType.Rectangle;
        }
    }

    public class HTMLThemeEllipse : HTMLThemeShape
    {
        public HTMLThemeEllipse()
        {
            this.ShapeType = HTMLThemeShapeType.Ellipse;
        }
    }

    public class HTMLThemePath : HTMLThemeShape
    {
        /// <summary>
        /// Dữ liệu của hình vẽ trên Theme
        /// </summary>
        [JsonProperty(PropertyName = "dt")]
        public string Data { get; set; }
        public HTMLThemePath()
        {
            this.ShapeType = HTMLThemeShapeType.Path;
        }
    }
}
