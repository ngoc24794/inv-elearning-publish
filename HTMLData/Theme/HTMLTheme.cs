﻿using INV.Elearning.HTMLHelper.HTMLData.ColorClass;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace INV.Elearning.HTMLHelper.HTMLData.Theme
{
    public class HTMLTheme
    {
        /// <summary>
        /// ID của theme
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public string ID { get; set; }
        /// <summary>
        /// Tên Theme
        /// </summary>
        [JsonProperty(PropertyName = "nm")]
        public string Name { get; set; }
        /// <summary>
        /// Hình dạng Theme
        /// </summary>
        [JsonProperty(PropertyName = "shs")]
        public List<HTMLThemeShape> Shapes { get; set; }
        /// <summary>
        /// Màu của layout
        /// </summary>
        [JsonProperty(PropertyName = "bg")]
        public HTMLColorBase Background { get; set; }
        /// <summary>
        /// Danh sách các đối tượng trong layout
        /// </summary>
        [JsonProperty(PropertyName = "e")]
        public List<HTMLObjectElement> Elements { get; set; }
    }
}
