﻿
using INV.Elearning.HTMLHelper.HTMLData.AnimationClass;
using INV.Elearning.HTMLHelper.HTMLData.Trigger;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace INV.Elearning.HTMLHelper.HTMLData
{
    public abstract class HTMLObjectElement
    {
        /// <summary>
        /// Chiều rộng của đối tượng
        /// </summary>
        [JsonProperty(PropertyName = "w")]
        public double Width { get; set; }
        /// <summary>
        /// Chiều dài của đối tượng
        /// </summary>
        [JsonProperty(PropertyName = "h")]
        public double Height { get; set; }
        /// <summary>
        /// Vị trí của đối tượng
        /// </summary>
        [JsonProperty(PropertyName = "T")]
        public double Top { get; set; }
        [JsonProperty(PropertyName = "l")]
        public double Left { get; set; }
        /// <summary>
        /// Góc xoay của đối tượng
        /// </summary>
        [JsonProperty(PropertyName = "agl")]
        public double Angle { get; set; }
        /// <summary>
        /// Z Index của đối tượng
        /// </summary>
        [JsonProperty(PropertyName = "z")]
        public int ZIndex { get; set; }
        /// <summary>
        /// Animation (mọi đối tượng đều có hiệu ứng)
        /// </summary>
       [JsonProperty(PropertyName = "a")]
        public List<HTMLAnimationBase> Animations { get; set; }
        /// <summary>
        /// Thời gian
        /// </summary>
        [JsonProperty(PropertyName = "tim")]
        public HTMLTiming Timing { get; set; }
        /// <summary>
        /// Loại đối tượng
        /// </summary>
        [JsonProperty(PropertyName = "c")]
        public string ContentKeyType { get; set; }
        /// <summary>
        /// Có đảo chiều ngang
        /// </summary>
        [JsonProperty(PropertyName = "isX")]
        public bool IsScaleX { get; set; }
        /// <summary>
        /// Có đảo chiều dọc
        /// </summary>
        [JsonProperty(PropertyName = "isY")]
        public bool IsScaleY { get; set; }
        /// <summary>
        /// ID của đối tượng
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public string ID { get; set; }
        /// <summary>
        /// Tên đối tượng
        /// </summary>
       [JsonProperty(PropertyName = "nm")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "tri")]
        public List<HTMLTrigger> Triggers { get; set; }

    }

}
