﻿using INV.Elearning.HTMLHelper.HTMLData.EffectClass;
using INV.Elearning.HTMLHelper.HTMLData.ShapeClass;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace INV.Elearning.HTMLHelper.HTMLData
{
    public class HTMLStandardElement : HTMLObjectElement
    {
        /// <summary>
        /// Danh sách hiệu ứng
        /// </summary>
        [JsonProperty(PropertyName = "efs")]
        public List<HTMLEffect> Effects { get; set; }
        /// <summary>
        /// Hình vẽ
        /// </summary>
       [JsonProperty(PropertyName = "shs")]
        public HTMLShape Shapes { get; set; }

        public HTMLStandardElement()
        {
            ContentKeyType = "Stand";
        }
    }
}
