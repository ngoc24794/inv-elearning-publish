﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace INV.Elearning.HTMLHelper.HTMLData
{
    /// <summary>
    /// Đối tượng nhóm 
    /// </summary>
    public class HTMLGroupElements : HTMLObjectElement
    {
        private List<HTMLObjectElement> elements;
        /// <summary>
        /// Hàm khởi tạo mặc định
        /// </summary>
        public HTMLGroupElements()
        {
            ContentKeyType = "Group";
        }

        /// <summary>
        /// Danh sách các phần tử con
        /// </summary>
        [JsonProperty(PropertyName = "els")]
        public List<HTMLObjectElement> Elements
        {
            get { return elements ?? (elements = new List<HTMLObjectElement>()); }
            set { elements = value; }
        }


    }
}
