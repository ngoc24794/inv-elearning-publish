﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INV.Elearning.HTMLHelper.HTMLData.PlayerClass
{
    public class HTMLTracking
    {
        /// <summary>
        /// Tracking status
        /// </summary>
        [JsonProperty(PropertyName = "trs")]
        public string TrackingStatus { get; set; }

        /// <summary>
        /// Số slide được chọn
        /// </summary>
        [JsonProperty(PropertyName = "noS")]
        public int NumberOfSlides { get; set; }

        /// <summary>
        /// Id của result slide được chọn
        /// </summary>
        [JsonProperty(PropertyName = "RsID")]
        public string ResultID { get; set; }
    }
}
