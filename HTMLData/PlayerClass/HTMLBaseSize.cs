﻿using Newtonsoft.Json;

namespace INV.Elearning.HTMLHelper.HTMLData
{
    public class HTMLBaseSize
    {
        /// <summary>
        /// LayoutHeight 
        /// </summary>
        [JsonProperty(PropertyName = "lH")]
        public double LayoutHeight { get; set; }
        /// <summary>
        /// MainTabHeight 
        /// </summary>
        [JsonProperty(PropertyName = "mH")]
        public double MainTabHeight { get; set; }
        /// <summary>
        /// NoteTabTop 
        /// </summary>
        [JsonProperty(PropertyName = "nT")]
        public double NoteTabTop { get; set; }
        /// <summary>
        /// NoteTabHeight 
        /// </summary>
        [JsonProperty(PropertyName = "nH")]
        public double NoteTabHeight { get; set; }
    }
}
