﻿using Newtonsoft.Json;

namespace INV.Elearning.HTMLHelper.HTMLData.PlayerClass
{
    public class HTMLResource
    {
        /// <summary>
        /// Title Note
        /// </summary>
        [JsonProperty(PropertyName = "ti")]
        public string Title { get; set; }

        /// <summary>
        /// URL
        /// </summary>
        [JsonProperty(PropertyName = "u")]
        public string URL { get; set; }

        /// <summary>
        /// File
        /// </summary>
        [JsonProperty(PropertyName = "fU")]
        public string FileURL { get; set; }
    }
}
