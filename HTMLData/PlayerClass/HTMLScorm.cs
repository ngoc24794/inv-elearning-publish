﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INV.Elearning.HTMLHelper.HTMLData.PlayerClass
{
    public class HTMLScorm
    {
        /// <summary>
        /// Phiên bản xuất bản scorm
        /// </summary>
        [JsonProperty(PropertyName = "se")]
        public string ScormEdition { get; set; }

        /// <summary>
        /// Định danh của Scorrm
        /// </summary>
        [JsonProperty(PropertyName = "ident")]
        public string Identifier { get; set; }

        /// <summary>
        /// Phiên bản scorm
        /// </summary>
        [JsonProperty(PropertyName = "ver")]
        public string Version { get; set; }

        /// <summary>
        /// Thời lượng scorm
        /// </summary>
        [JsonProperty(PropertyName = "dur")]
        public string Duration { get; set; }

        /// <summary>
        /// Keyword
        /// </summary>
        [JsonProperty(PropertyName = "kw")]
        public string KeyWord { get; set; }

        /// <summary>
        /// Tiêu đề bài học
        /// </summary>
        [JsonProperty(PropertyName = "tl")]
        public string TitleLesson { get; set; }

        /// <summary>
        /// Định danh bài học
        /// </summary>
        [JsonProperty(PropertyName = "il")]
        public string IdentifyLesson { get; set; }

        /// <summary>
        /// Trạng thái report
        /// </summary>
        [JsonProperty(PropertyName = "rs")]
        public string ReportStatus { get; set; }
        /// <summary>
        /// tên scorm được chọn
        /// </summary>
        [JsonProperty(PropertyName = "nC")]
        public string NameScorm { get; set; }
    }
}
