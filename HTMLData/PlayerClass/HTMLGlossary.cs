﻿using Newtonsoft.Json;

namespace INV.Elearning.HTMLHelper.HTMLData.PlayerClass
{
    public class HTMLGlossary
    {
        /// <summary>
        /// Definition 
        /// </summary>
        [JsonProperty(PropertyName = "def")]
        public string Definition { get; set; }

        /// <summary>
        /// Term
        /// </summary>
        [JsonProperty(PropertyName = "tm")]
        public string Term { get; set; }
    }
}
