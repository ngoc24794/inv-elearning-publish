﻿using Newtonsoft.Json;

namespace INV.Elearning.HTMLHelper.HTMLData
{
    public class HTMLFeaturesPlayer
    {
        /// <summary>
        /// MenuTab 
        /// </summary>
        [JsonProperty(PropertyName = "mT")]
        public string MenuTab { get; set; }
        /// <summary>
        /// ResourcesTab 
        /// </summary>
        [JsonProperty(PropertyName = "rT")]
        public string ResourcesTab { get; set; }
        /// <summary>
        /// GlossaryTab 
        /// </summary>
        [JsonProperty(PropertyName = "gT")]
        public string GlossaryTab { get; set; }
        /// <summary>
        /// NoteTab 
        /// </summary>
        [JsonProperty(PropertyName = "nB")]
        public string NoteTab { get; set; }
        /// <summary>
        /// MenuContent 
        /// </summary>
        [JsonProperty(PropertyName = "mC")]
        public string MenuContent { get; set; }
        /// <summary>
        /// ResourcesContent 
        /// </summary>
        [JsonProperty(PropertyName = "rC")]
        public string ResourcesContent { get; set; }
        /// <summary>
        /// GlossaryContent 
        /// </summary>
        [JsonProperty(PropertyName = "gC")]
        public string GlossaryContent { get; set; }
        /// <summary>
        /// NoteContent 
        /// </summary>
        [JsonProperty(PropertyName = "nC")]
        public string NoteContent { get; set; }
        /// <summary>
        /// Subject 
        /// </summary>
        [JsonProperty(PropertyName = "sb")]
        public bool Subject { get; set; }
        /// <summary>
        /// Title 
        /// </summary>
        [JsonProperty(PropertyName = "ti")]
        public bool Title { get; set; }
        /// <summary>
        /// Writer 
        /// </summary>
        [JsonProperty(PropertyName = "wr")]
        public bool Writer { get; set; }
        /// <summary>
        /// SubjectInfo 
        /// </summary>
        [JsonProperty(PropertyName = "sI")]
        public string SubjectInfo { get; set; }
        /// <summary>
        /// TitleInfo 
        /// </summary>
        [JsonProperty(PropertyName = "tI")]
        public string TitleInfo { get; set; }
        /// <summary>
        /// WriterInfo 
        /// </summary>
        [JsonProperty(PropertyName = "wI")]
        public string WriterInfo { get; set; }
        /// <summary>
        /// Search 
        /// </summary>
        [JsonProperty(PropertyName = "s")]
        public bool Search { get; set; }
        /// <summary>
        /// Logo 
        /// </summary>
        [JsonProperty(PropertyName = "lg")]
        public string Logo { get; set; }
        /// <summary>
        /// LogoUrl 
        /// </summary>
        [JsonProperty(PropertyName = "lU")]
        public string LogoUrl { get; set; }


    }
}
