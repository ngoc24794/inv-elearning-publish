﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace INV.Elearning.HTMLHelper.HTMLData.PlayerClass
{
    public class HTMLPlayer
    {
        /// <summary>
        /// Nếu Menu hiện
        /// </summary>
        [JsonProperty(PropertyName = "iM")]
        public bool IsMenuEnable { get; set; }
        /// <summary>
        /// Nếu Glossary hiện
        /// </summary>
        [JsonProperty(PropertyName = "iG")]
        public bool IsGlossaryEnable { get; set; }
        /// <summary>
        /// Nếu Note hiện
        /// </summary>
        [JsonProperty(PropertyName = "iN")]
        public bool IsNotesEnable { get; set; }
        /// <summary>
        /// Nếu Resource hiện
        /// </summary>
        [JsonProperty(PropertyName = "iR")]
        public bool IsResourceEnable { get; set; }
        /// <summary>
        /// Nếu volume hiện
        /// </summary>
        [JsonProperty(PropertyName = "iV")]
        public bool IsVolumeEnable { get; set; }
        /// <summary>
        /// Nếu thanh control hiện
        /// </summary>
        [JsonProperty(PropertyName = "iSb")]
        public bool IsSeekBarEnable { get; set; }

        /// <summary>
        /// Nếu logo hiện
        /// </summary>
        [JsonProperty(PropertyName = "iL")]
        public bool IsLogoEnable { get; set; }

        /// <summary>
        /// Nếu search hiện
        /// </summary>
        [JsonProperty(PropertyName = "iS")]
        public bool IsSearchEnable { get; set; }
      
        /// <summary>
        /// Vị trí của Menu
        /// </summary>
        [JsonProperty(PropertyName = "mPo")]
        public int MenuPosition { get; set; }
        /// <summary>
        /// Vị trí của Glossary
        /// </summary>
        [JsonProperty(PropertyName = "glP")]
        public int GlossaryPosition { get; set; }
        /// <summary>
        /// Vị trí của Note
        /// </summary>
        [JsonProperty(PropertyName = "nPo")]
        public int NotePosition { get; set; }
        /// <summary>
        /// Vị trí của Resource
        /// </summary>
        [JsonProperty(PropertyName = "rPo")]
        public int ResourcePosition { get; set; }

        /// <summary>
        /// Title của player
        /// </summary>
        [JsonProperty(PropertyName = "ti")]
        public string Title { get; set; }

        /// <summary>
        /// User
        /// </summary>
        [JsonProperty(PropertyName = "u")]
        public string User { get; set; }

        /// <summary>
        /// Description của player
        /// </summary>
        [JsonProperty(PropertyName = "ds")]
        public string Description { get; set; }

        /// <summary>
        /// Glossary
        /// </summary>        
        [JsonProperty(PropertyName = "gl")]
        public List<HTMLGlossary> Glossaries { get; set; }

        /// <summary>
        /// Resource
        /// </summary>
        [JsonProperty(PropertyName = "rDes")]
        public string ResourcesDescription { get; set; }

        /// <summary>
        /// Resource
        /// </summary>
        [JsonProperty(PropertyName = "res")]
        public List<HTMLResource> Resources { get; set; }

        /// <summary>
        /// Logo url
        /// </summary>
        [JsonProperty(PropertyName = "lU")]
        public string LogoUrl { get; set; }

        /// <summary>
        /// Màu chính của giao diện
        /// </summary>
        [JsonProperty(PropertyName = "cThe")]
        public string ColorTheme { get; set; }
    }
}
