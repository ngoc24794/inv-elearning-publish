﻿using Newtonsoft.Json;

namespace INV.Elearning.HTMLHelper.HTMLData
{
    public class HTMLColorPlayer
    { 
        /// <summary>
        /// Màu của body
        /// </summary>
        [JsonProperty(PropertyName = "mg")]
        public string MainPageBackground { get; set; }
        /// <summary>
        /// Màu của Main Content
        /// </summary>
        [JsonProperty(PropertyName = "ml")]
        public string MainPlayerBackground { get; set; }
        /// <summary>
        /// Màu của Header và Bottom
        /// </summary>
        [JsonProperty(PropertyName = "bB")]
        public string BarBackground { get; set; }
        /// <summary>
        /// Màu của tiêu đề, người soạn
        /// </summary>
        [JsonProperty(PropertyName = "bT")]
        public string BarText { get; set; }
        /// <summary>
        /// Màu của nút bấm và tab
        /// </summary>
        [JsonProperty(PropertyName = "B")]
        public string ButtonBackground { get; set; }
        /// <summary>
        /// Màu chữ hoặc icon của nút bấm và tab
        /// </summary>
        [JsonProperty(PropertyName = "T")]
        public string ButtonText { get; set; }
        /// <summary>
        /// Màu hover của background và tab
        /// </summary>
        [JsonProperty(PropertyName = "bH")]
        public string ButtonHoverBackground { get; set; }
        /// <summary>
        /// Màu hover của text hoặc icon của background và tab
        /// </summary>
        [JsonProperty(PropertyName = "Ht")]
        public string ButtonHoverText { get; set; }
        /// <summary>
        /// Màu nền của menu bên
        /// </summary>
        [JsonProperty(PropertyName = "sB")]
        public string SidebarBackground { get; set; }
        /// <summary>
        /// Màu chữ của menu bên
        /// </summary>
        [JsonProperty(PropertyName = "sT")]
        public string SidebarText { get; set; }
        /// <summary>
        /// Màu hover của item của menu bên
        /// </summary>
        [JsonProperty(PropertyName = "Hb")]
        public string SidebarHoverItemBackground { get; set; }
        /// <summary>
        /// Màu hover của chữ của menu bên
        /// </summary>
        [JsonProperty(PropertyName = "st")]
        public string SidebarHoverItemText { get; set; }
        /// <summary>
        /// Màu nền của item được chọn của menu bên
        /// </summary>
        [JsonProperty(PropertyName = "sIb")]
        public string SidebarSelectedItemBackground { get; set; }
        /// <summary>
        /// Màu chữ của item được chọn của menu bên
        /// </summary>
        [JsonProperty(PropertyName = "sIt")]
        public string SidebarSelectedItemText { get; set; }
        /// <summary>
        /// Màu chữ của item đã được click của menu bên
        /// </summary>
        [JsonProperty(PropertyName = "vt")]
        public string SidebarVisitedItemText { get; set; }
        /// <summary>
        /// Màu của đường dẫn của menu bên
        /// </summary>
        [JsonProperty(PropertyName = "sl")]
        public string SidebarHyperlink { get; set; }
        /// <summary>
        /// Màu nền của thanh control của slide và của volume
        /// </summary>
        [JsonProperty(PropertyName = "se")]
        public string SeekbarBackground { get; set; }
        /// <summary>
        /// Màu của thanh control của slide và của volume (phần đã được kéo)
        /// </summary>
        [JsonProperty(PropertyName = "sP")]
        public string SeekbarPlayback { get; set; }

    }
}
