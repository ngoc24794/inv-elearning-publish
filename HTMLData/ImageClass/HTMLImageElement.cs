﻿using Newtonsoft.Json;

namespace INV.Elearning.HTMLHelper.HTMLData.ImageClass
{
    public class HTMLImageElement : HTMLStandardElement
    {
        /// <summary>
        /// Đường dẫn ảnh
        /// </summary>
        [JsonProperty(PropertyName = "imgU")]
        public string ImageUrl { get; set; }
        public HTMLImageElement()
        {
            ContentKeyType = "Image";
        }
    }
}
