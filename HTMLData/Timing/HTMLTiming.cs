﻿using Newtonsoft.Json;

namespace INV.Elearning.HTMLHelper.HTMLData
{
    public class HTMLTiming
    {
        [JsonProperty(PropertyName = "sT")]
        public double StartTime { get; set; }
        [JsonProperty(PropertyName = "dT")]
        public double Duration { get; set; }
    }
}
