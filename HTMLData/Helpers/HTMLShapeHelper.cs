﻿using INV.Elearing.Controls.Shapes;
using INV.Elearing.Controls.Shapes.Basics;
using INV.Elearning.Core;
using INV.Elearning.Core.View;
using INV.Elearning.HTMLHelper.HTMLData.ShapeClass;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Media;

namespace INV.Elearning.HTMLHelper.HTMLData.Helpers
{
    public partial class HTMLDataHelper
    {
        public void ExportWebData()
        {
        }

        ///// <summary>
        ///// Chuyển <see cref="ShapeBase"/> thành <see cref="PathObject"/>
        ///// </summary>
        ///// <param name="face"></param>
        ///// <returns></returns>
        //private PathObject ConvertToPathObject(ShapeBase face)
        //{
        //    bool
        //        isStroked = face.Stroke != null || face.StrokeThickness != 0,
        //        isFilled = face.Fill != null;

        //    double fillOpacity = face.Fill != null ? face.Fill.Opacity : 1;

        //    string
        //        fill = ConvertToRGBA(face.Fill ?? Brushes.Transparent),
        //        stroke = ConvertToRGBA(face.Stroke ?? Brushes.Transparent);

        //    PathObject pathObject = new PathObject()
        //    {
        //        Data = CalculateData(face),
        //        IsStroked = isStroked,
        //        IsFilled = isFilled,
        //        Fill = fill,
        //        Stroke = stroke,
        //        StrokeThickness = face.StrokeThickness
        //    };

        //    return pathObject;
        //}

        ///// <summary>
        ///// Chuyển màu Brush sang RGBA
        ///// </summary>
        ///// <param name="brush"></param>
        ///// <returns></returns>
        //private string ConvertToRGBA(Brush brush)
        //{
        //    if (brush == Brushes.Transparent)
        //        return "transparent";

        //    SolidColorBrush solidColor = brush as SolidColorBrush;
        //    Color color = solidColor.Color;

        //    int R = color.R,
        //        G = color.G,
        //        B = color.B;
        //    double A = Math.Round(brush.Opacity, 2);

        //    return $"rgba({R},{G},{B},{A})";
        //}

        /// <summary>
        /// Lấy dữ liệu cho hình vẽ
        /// </summary>
        /// <param name="standardElement"></param>
        /// <returns></returns>
        private HTMLShape GetHTMLShape(StandardElement standardElement)
        {
            ShapeBase shape = standardElement.ShapePresent;


            List<HTMLPathObject> pathObjects = new List<HTMLPathObject>();
            if (shape != null)
            {
                // Kích thước (width, height) của hình
                double width = shape.Width, height = shape.Height;

                if (shape is MultiShapeBase)                                                    // Xử lí cho MultiShapeBase
                {
                    MultiShapeBase multiShape = shape as MultiShapeBase;
                    PathGeometry geometryShape = multiShape.PathGeometry;
                    List<ShapeBase> faces = multiShape.Shapes;
                    HTMLPathObject bgpathObject = ConvertToPathObject(standardElement);
                    //pathObjects.Add(bgpathObject);

                    foreach (ShapeBase face in faces)
                    {
                        HTMLPathObject pathObject = ConvertToPathObject(face, standardElement.Fill);
                        pathObjects.Add(pathObject);
                    }
                }

                else
                {
                    HTMLPathObject pathObject = ConvertToPathObject(standardElement);
                    pathObjects.Add(pathObject);
                }

                return new HTMLShape()
                {
                    CapType = shape.CapType,
                    JoinType = shape.JoinType,
                    DashType = shape.DashType,
                    PathObjects = pathObjects
                };
            }
            else
            {
                HTMLPathObject pathObject = new HTMLPathObject()
                {
                    Data = "M0,0 " + standardElement.Width + ",0 " + standardElement.Width + "," + standardElement.Height + " 0," + standardElement.Height + "z",
                };
                //DataShapeBase dataShapeBase = GetShapeData(standardElement);
                //pathObject.Data = dataShapeBase.Data;
                pathObjects.Add(pathObject);
                return new HTMLShape()
                {
                    PathObjects = pathObjects
                };
            }
        }

        //private DataShapeBase GetShapeData(StandardElement standardElement)
        //{
        //    ShapeBase shape = standardElement.ShapePresent;


        //    double
        //        left = standardElement.Left, top = standardElement.Top,                     // Tọa độ gốc O1(left, top) của hình so với gốc O của canvas
        //        width = standardElement.Width, height = standardElement.Height;             // Kích thước (width, height) của hình

        //    int
        //        zIndex = standardElement.ZIndex,                                            // ZIndex của hình
        //        id = 0;

        //    //
        //    //----------------------- TẠO PathObjects --------------------//
        //    // ---------------------- BEGIN ------------------------------//
        //    List<PathObject> pathObjects = new List<PathObject>();

        //    if (shape is MultiShapeBase)                                                    // Xử lí cho MultiShapeBase
        //    {
        //        MultiShapeBase multiShape = shape as MultiShapeBase;
        //        PathGeometry geometryShape = multiShape.PathGeometry;
        //        List<ShapeBase> faces = multiShape.Shapes;

        //        foreach (ShapeBase face in faces)
        //        {
        //            PathObject pathObject = ConvertToPathObject(face);

        //            pathObjects.Add(pathObject);
        //        }
        //    }

        //    else
        //    {
        //        PathObject pathObject = ConvertToPathObject(shape);

        //        pathObjects.Add(pathObject);
        //    }

        //    // ---------------------- END ------------------------------//


        //    DataShapeBase dataShapeBase = new DataShapeBase()
        //    {
        //        Id = shape.GetType().Name + id,
        //        Width = width,
        //        Height = height,
        //        Position = new Point(left, top),
        //        ZIndex = zIndex,
        //        CapType = shape.CapType,
        //        JoinType = shape.JoinType,
        //        DashType = shape.DashType,
        //        PathObjects = pathObjects
        //    };

        //    return dataShapeBase;
        //}

        /// <summary>
        /// Chuyển <see cref="StandardElement"/> thành <see cref="HTMLPathObject"/>
        /// </summary>
        /// <param name="standardElement"></param>
        /// <returns></returns>
        private HTMLPathObject ConvertToPathObject(StandardElement standardElement)
        {
            bool
                isStroked = standardElement.Stroke != null || standardElement.Thickness != 0,
                isFilled = standardElement.Fill != null;

            double fillOpacity = standardElement.Fill != null ? standardElement.Fill.Brush.Opacity : 1;

            HTMLPathObject pathObject = new HTMLPathObject()
            {
                Data = CalculateData(standardElement.ShapePresent),
                IsStroked = isStroked,
                IsFilled = isFilled,
                Fill = GetColor(standardElement.Fill),
                Stroke = GetColor(standardElement.Stroke),
                StrokeThickness = standardElement.Thickness,
                DashType = standardElement.DashType
            };

            return pathObject;
        }

        private HTMLPathObject ConvertToPathObject(ShapeBase face, ColorBrushBase fill)
        {
            bool
                isStroked = face.Stroke != null || face.StrokeThickness != 0,
                isFilled = face.Fill != null;

            double fillOpacity = face.Fill != null ? face.Fill.Opacity : 1;

            HTMLPathObject pathObject = new HTMLPathObject()
            {
                Data = CalculateData(face),
                IsStroked = isStroked,
                IsFilled = isFilled,
                Fill = GetColor(face.Fill == null ? Brushes.Transparent : face.Fill as SolidColorBrush),
                Stroke = GetColor(face.Stroke == null ? Brushes.Transparent : face.Stroke as SolidColorBrush),
                StrokeThickness = face.StrokeThickness,
                DashType = face.DashType
            };

            if (!(face.Fill is SolidColorBrush) && face.Fill != null)
            {
                pathObject.Fill = GetColor(fill);
            }

            return pathObject;
        }

        private string ConvertToRGBA(Brush brush)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Tính toán dữ liệu
        /// </summary>
        /// <param name="face"></param>
        /// <returns></returns>
        private string CalculateData(ShapeBase face)
        {
            PathFigureCollection figureCollection = new PathFigureCollection();
            foreach (PathFigure figure in face.PathGeometry.Figures)
            {
                PathSegmentCollection segmentCollection = new PathSegmentCollection();
                Point previousPoint = figure.StartPoint;
                foreach (PathSegment segment in figure.Segments)
                {
                    if (segment is LineSegment)
                    {
                        LineSegment line = segment as LineSegment;
                        LineSegment newLine = line.Clone();
                        newLine.Point = RoundPoint(line.Point);
                        double x = newLine.Point.X, y = newLine.Point.Y;
                        if (previousPoint.X == newLine.Point.X)
                        {
                            newLine.Point = new Point(x + 0.0000000001, y);
                        }
                        if (previousPoint.Y == newLine.Point.Y)
                        {
                            newLine.Point = new Point(x, y + 0.0000000001);
                        }
                        previousPoint = newLine.Point;
                        segmentCollection.Add(newLine);
                    }
                    else if (segment is PolyLineSegment)
                    {
                        PolyLineSegment polyLine = segment as PolyLineSegment;
                        PolyLineSegment newPolyLine = polyLine.Clone();
                        newPolyLine.Points = RoundPointCollection(polyLine.Points);
                        segmentCollection.Add(newPolyLine);
                        for (int i = 0; i < newPolyLine.Points.Count; i++)
                        {
                            if (previousPoint.Y == newPolyLine.Points[i].Y)
                            {
                                double x = newPolyLine.Points[i].X, y = newPolyLine.Points[i].Y;
                                if (previousPoint.X == newPolyLine.Points[i].X)
                                {
                                    newPolyLine.Points[i] = new Point(x + 0.0000000001, y);
                                }
                                if (previousPoint.Y == newPolyLine.Points[i].Y)
                                {
                                    newPolyLine.Points[i] = new Point(x, y + 0.0000000001);
                                }
                            }
                            previousPoint = newPolyLine.Points[i];
                        }
                        previousPoint = newPolyLine.Points.Last();
                    }
                    else if (segment is ArcSegment)
                    {
                        ArcSegment arc = segment as ArcSegment;
                        ArcSegment newArc = arc.Clone();
                        newArc.Point = RoundPoint(arc.Point);
                        segmentCollection.Add(newArc);
                        previousPoint = newArc.Point;
                    }
                    else if (segment is BezierSegment)
                    {
                        BezierSegment curve = segment as BezierSegment;
                        BezierSegment bezierSegment = curve.Clone();
                        bezierSegment.Point1 = RoundPoint(curve.Point1);
                        bezierSegment.Point2 = RoundPoint(curve.Point2);
                        bezierSegment.Point3 = RoundPoint(curve.Point3);
                        segmentCollection.Add(bezierSegment);
                        previousPoint = bezierSegment.Point3;
                    }
                    else if (segment is PolyBezierSegment)
                    {
                        PolyBezierSegment polyCurve = segment as PolyBezierSegment;
                        PolyBezierSegment polyBezierSegment = polyCurve.Clone();
                        polyBezierSegment.Points = RoundPointCollection(polyCurve.Points);
                        segmentCollection.Add(polyBezierSegment);
                        previousPoint = polyBezierSegment.Points.Last();
                    }
                }

                PathFigure newFigure = new PathFigure
                {
                    StartPoint = RoundPoint(figure.StartPoint),
                    IsClosed = figure.IsClosed,
                    IsFilled = figure.IsFilled,
                    Segments = segmentCollection
                };

                figureCollection.Add(newFigure);
            }
            PathGeometry pathGeometry = new PathGeometry
            {
                Figures = figureCollection
            };

            return pathGeometry.ToString(CultureInfo.InvariantCulture);
        }
        /// <summary>
        /// Làm tròn tọa độ một điểm
        /// </summary>
        /// <param name="point"></param>
        /// <param name="decimals"></param>
        /// <returns></returns>
        private Point RoundPoint(Point point, int decimals = 2)
        {
            return new Point()
            {
                X = Math.Round(point.X, decimals),
                Y = Math.Round(point.Y, decimals)
            };
        }

        /// <summary>
        /// Làm tròn tọa độ một danh sách điểm
        /// </summary>
        /// <param name="pointCollection"></param>
        /// <param name="decimals"></param>
        /// <returns></returns>
        private PointCollection RoundPointCollection(PointCollection pointCollection, int decimals = 2)
        {
            PointCollection result = new PointCollection();
            foreach (Point point in pointCollection)
            {
                Point p = new Point()
                {
                    X = Math.Round(point.X, decimals),
                    Y = Math.Round(point.Y, decimals)
                };
                result.Add(p);
            }
            return result;
        }
    }
}
