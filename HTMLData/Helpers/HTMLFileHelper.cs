﻿using INV.Elearning.ImageProcess.Views;
using System;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace INV.Elearning.HTMLHelper.HTMLData.Helpers
{
    public partial class HTMLDataHelper
    {
        /// <summary>
        /// Ghi dữ liệu ra tập tin
        /// </summary>
        /// <param name="dataString"></param>
        /// <param name="filePath"></param>
        public void WriteFile(string dataString, string filePath)
        {
            StreamWriter writer = StreamWriter.Null;
            try
            {
                writer = new StreamWriter(filePath, false, Encoding.Unicode);
                writer.WriteLine(dataString);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (writer != null)
                {
                    writer.Dispose();
                    writer.Close();
                }
            }
        }

        /// <summary>
        /// Sao chép ảnh vào thư mục chứa
        /// </summary>
        /// <param name="targetUrl"></param>
        /// <returns></returns>
        private string CopyImageToExportFolder(string targetUrl, ImageBrush imageBrush = null)
        {
            if (imageBrush != null) return GetImage(imageBrush, targetUrl); //Nếu lấy dữ liệu từ app

            if (!File.Exists(targetUrl)) return string.Empty;
            var destUrl = Path.Combine(this.ImageFolder, $"{Path.GetFileNameWithoutExtension(targetUrl)}{Path.GetExtension(targetUrl)}");

            File.Copy(targetUrl, destUrl, true);
            if (File.Exists(destUrl))
                return destUrl;


            imageCount--;
            return string.Empty;
        }

        /// <summary>
        /// Lấy ảnh cho đối tượng Image
        /// </summary>
        /// <param name="imageContent"></param>
        /// <returns></returns>
        private string GetImageFromImageContent(ImageContent imageContent)
        {
            string imageUrl = "";

            if (imageContent.ArtisticEffect != null || imageContent.ColorSaturationEffect != null || imageContent.ContrastAdjustEffect != null || imageContent.MonochromeEffect != null
                || imageContent.SharpenEffect != null || imageContent.ToonShaderEffect != null) //Nếu tồn tại ít nhất việc áp dụng 1 hiệu ứng, cần lấy ảnh đã áp dụng hiệu ứng
            {
                imageUrl = Path.Combine(ImageFolder, $"{Path.GetFileNameWithoutExtension(imageContent.ImageSource)}_{imageCount++}.png");
                imageContent.SaveImageEffects(imageUrl);
                return imageUrl;
            }
            if (imageContent.CropRect.Width > 0 && imageContent.Height > 0 && imageContent.CropRect.Width != imageContent.OriginSize.Width && imageContent.Height != imageContent.OriginSize.Height)
              return CropImage(imageContent.ImageSource, new Int32Rect((int)imageContent.CropRect.X, (int)imageContent.CropRect.Y, (int)imageContent.CropRect.Width, (int)imageContent.CropRect.Height));
            return CopyImageToExportFolder(imageContent.ImageSource);
        }

        /// <summary>
        /// Cắt ảnh và lưu
        /// </summary>
        /// <param name="inputFile"></param>
        /// <param name="outputFile"></param>
        /// <param name="rect"></param>
        private string CropImage(string inputFile, Int32Rect rect)
        {
            var outputFile = Path.Combine(this.ImageFolder, $"{Path.GetFileNameWithoutExtension(inputFile)}_{rect.X}_{rect.Y}_{rect.Width}_{rect.Height}{Path.GetExtension(inputFile)}");
            if (File.Exists(outputFile)) //Nếu đã tồn tại file trước đây
                return outputFile;

            using (Stream stream = new FileStream(inputFile, FileMode.Open))
            {
                BitmapImage _bitmapImage = new BitmapImage();
                _bitmapImage.BeginInit();
                _bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                _bitmapImage.StreamSource = stream;

                _bitmapImage.EndInit();
                _bitmapImage.Freeze();

                CroppedBitmap _croppedBitmap = new CroppedBitmap(_bitmapImage, rect);
                var _compressBitmap = new BitmapImage();
                using (FileStream outStream = new FileStream(outputFile, FileMode.Create))
                {
                    BitmapEncoder encoder = null;
                    // Use png encoder for our data
                    switch (Path.GetExtension(outputFile))
                    {
                        case ".png":
                            encoder = new PngBitmapEncoder();
                            break;
                        case ".jpg":
                            encoder = new JpegBitmapEncoder();
                            break;
                        case ".bmp":
                            encoder = new BmpBitmapEncoder();
                            break;
                        default:
                            encoder = new PngBitmapEncoder();
                            break;
                    }
                    encoder.Frames.Add(BitmapFrame.Create(_croppedBitmap));
                    encoder.Save(outStream);
                }

                if (File.Exists(outputFile))
                    return outputFile;
                return string.Empty;
            }
        }

        private string GetImageFromImageContent(ImageContent imageContent, bool Preview)
        {
            string imageUrl = "";
            //FileInfo fileInfo = new FileInfo(imageContent.ImageSource);
            if (imageContent.ArtisticEffect != null || imageContent.ColorSaturationEffect != null || imageContent.ContrastAdjustEffect != null || imageContent.MonochromeEffect != null
                || imageContent.SharpenEffect != null || imageContent.ToonShaderEffect != null) //Nếu tồn tại ít nhất việc áp dụng 1 hiệu ứng, cần lấy ảnh đã áp dụng hiệu ứng
            {
                imageUrl = Path.Combine(ImageFolder, $"{Path.GetFileNameWithoutExtension(imageContent.ImageSource)}_{imageCount++}.png");
                imageContent.SaveImageEffects(imageUrl);
                return imageUrl;
            }
            return imageContent.ImageSource;
        }



        /// <summary>
        /// Sao chép video vào thư mục chứa
        /// </summary>
        /// <param name="targetUrl"></param>
        /// <returns></returns>
        private string CopyVideoToExportFolder(string targetUrl)
        {
            if (targetUrl.Equals(""))
            {
                return string.Empty;
            }
            FileInfo fileInfo = new FileInfo(targetUrl);
            //var destUrl = Path.Combine(this.VideoFolder, $"{fileInfo.Name}{fileInfo.Extension}");
            var destUrl = Path.Combine(this.VideoFolder, $"{Path.GetFileNameWithoutExtension(targetUrl)}{Path.GetExtension(targetUrl)}");

            if (File.Exists(destUrl)) return destUrl;

            File.Copy(targetUrl, destUrl, true);
            if (File.Exists(destUrl))
                return destUrl;

            return string.Empty;
        }

        /// <summary>
        /// Sao chép audio vào thư mục chứa
        /// </summary>
        /// <param name="targetUrl"></param>
        /// <returns></returns>
        private string CopyAudioToExportFolder(string targetUrl)
        {
            if (targetUrl.Equals(""))
            {
                return string.Empty;
            }

            var destUrl = Path.Combine(this.AudioFolder, $"{Path.GetFileNameWithoutExtension(targetUrl)}{Path.GetExtension(targetUrl)}");
            if (File.Exists(destUrl)) return destUrl;
            File.Copy(targetUrl, destUrl, true);
            if (File.Exists(destUrl))
                return destUrl;

            return string.Empty;
        }

        /// <summary>
        /// Sao chép flash vào thư mục chứa
        /// </summary>
        /// <param name="targetUrl"></param>
        /// <returns></returns>
        private string CopyFlashToExportFolder(string targetUrl)
        {
            if (targetUrl.Equals(""))
            {
                return string.Empty;
            }
            var destUrl = Path.Combine(this.FlashFolder, $"{Path.GetFileNameWithoutExtension(targetUrl)}{Path.GetExtension(targetUrl)}");
            if (File.Exists(destUrl)) return destUrl;
            File.Copy(targetUrl, destUrl, true);
            if (File.Exists(destUrl))
                return destUrl;

            return string.Empty;
        }

        /// <summary>
        /// Sao chép resource file vào thư mục chứa
        /// </summary>
        /// <param name="targetUrl"></param>
        /// <returns></returns>
        private string CopyResourceFileToExportFolder(string targetUrl)
        {
            var destUrl = Path.Combine(this.ResourceFileFolder, $"{Path.GetFileNameWithoutExtension(targetUrl)}{Path.GetExtension(targetUrl)}");
            if (File.Exists(destUrl)) return destUrl;
            File.Copy(targetUrl, destUrl, true);
            if (File.Exists(destUrl))
                return destUrl;

            return string.Empty;
        }

        /// <summary>
        /// đọc file vtt và trả về chuổi caption của video hoặc audio
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public string GetSubtitle(string path)
        {
            if (path != null)
            {
                var sub = "";
                try
                {
                    string contents = File.ReadAllText(path); //Đọc tất cả nội dung của tập tin
                    if (contents != null)
                    {
                        sub = contents.Replace("\r\n", "\n");
                        return sub;
                    }
                }
                catch (Exception e)
                {
                    Console.Write("Error:" + e.Message);
                }
            }
            return null;
        }
    }
}
