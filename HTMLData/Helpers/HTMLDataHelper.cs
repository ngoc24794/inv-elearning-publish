﻿using INV.Elearning.Core.View;
using INV.Elearning.HTMLHelper.HTMLData.Document;
using System;
using System.Collections.Generic;
using INV.Elearning.HTMLHelper.HTMLData.SlideBase;
using INV.Elearning.HTMLHelper.HTMLData.LayoutBase;
using INV.Elearning.Core.Model.Theme;
using INV.Elearning.HTMLHelper.HTMLData.Theme;
using System.IO;
using INV.Elearning.Text.Views;
using INV.Elearning.Animations;
using INV.Elearning.HTMLHelper.HTMLData.AnimationClass;
using INV.Elearning.HTMLHelper.HTMLData.EffectClass;
using INV.Elearning.Core.Model;
using INV.Elearning.HTMLHelper.HTMLData.ImageClass;
using System.Linq;
using System.Windows.Media;
using System.Globalization;
using INV.Elearning.ImageProcess.Views;
using INV.Elearning.VideoElementControl;
using INV.Elearning.HTMLHelper.HTMLData.MediaClass;
using INV.Elearning.Controls.Audio;
using INV.Elearning.Controls.Flash;
using Newtonsoft.Json;
using System.Windows;
using INV.Elearning.Core.Helper;
using INV.Elearning.Charts.Views;
using INV.Elearning.Player.ViewModel;
using Antlr4.StringTemplate;
using INV.Elearning.Quizz;
using INV.Elearning.HTMLHelper.HTMLData.Questions.Answers;
using INV.Elearning.Quizz.Controls.GroupControl;
using INV.Elearning.Quizz.Controls;
using INV.Elearning.Quizz.Controls.Layer;
using INV.Elearning.Quizz.Controls.Result;
using INV.Elearning.Quizz.Models;
using INV.Elearning.Text.ViewModels.Text;
using INV.Elearning.Quizz.Result;
using INV.Elearning.Trigger;
using INV.Elearning.HTMLHelper.HTMLData.Trigger;
using INV.Elearning.HTMLHelper.HTMLData.TriggerClass;
using INV.Elearing.Controls.Shapes;
using INV.Elearning.Text;
using INV.Elearning.HTMLHelper.HTMLData.ColorClass;
using INV.Elearning.Quizz.Controls.ControlsUI;
using System.Windows.Media.Imaging;
using INV.Elearning.Core.Timeline;
using INV.Elearning.HTMLHelper.HTMLData.PlayerClass;
using INV.Elearning.HTMLHelper.HTMLData.TextClass;
using INV.Elearning.Core.Model.Player;
using INV.Elearning.YoutubeVideo;
using INV.Elearning.HTMLHelper.HTMLData.StandardScorm;
using System.IO.Compression;
using INV.Elearning.Text.Html;
using INV.Elearning.Core.View.Theme;
using INV.Elearning.Animations.UI;
using INV.Elearning.HTMLHelper.HTMLData.ShapeClass;

namespace INV.Elearning.HTMLHelper.HTMLData.Helpers
{
    public partial class HTMLDataHelper
    {
        private bool _checkPreview = false;
        public static string GetHTMLData(DependencyObject obj)
        {
            return (string)obj.GetValue(HTMLDataProperty);
        }

        public static void SetHTMLData(DependencyObject obj, string value)
        {
            obj.SetValue(HTMLDataProperty, value);
        }

        // Using a DependencyProperty as the backing store for HTMLData.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HTMLDataProperty =
            DependencyProperty.RegisterAttached("HTMLData", typeof(string), typeof(HTMLDataHelper), new PropertyMetadata(""));



        public static string GetNextSlide(DependencyObject obj)
        {
            return (string)obj.GetValue(NextSlideProperty);
        }

        public static void SetNextSlide(DependencyObject obj, string value)
        {
            obj.SetValue(NextSlideProperty, value);
        }

        // Using a DependencyProperty as the backing store for NextSlide.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty NextSlideProperty =
            DependencyProperty.RegisterAttached("NextSlide", typeof(string), typeof(HTMLDataHelper), new PropertyMetadata(""));



        public static string GetPreviousSlide(DependencyObject obj)
        {
            return (string)obj.GetValue(PreviousSlideProperty);
        }

        public static void SetPreviousSlide(DependencyObject obj, string value)
        {
            obj.SetValue(PreviousSlideProperty, value);
        }

        // Using a DependencyProperty as the backing store for PreviousSlide.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PreviousSlideProperty =
            DependencyProperty.RegisterAttached("PreviousSlide", typeof(string), typeof(HTMLDataHelper), new PropertyMetadata(""));



        #region Fields
        private int imageCount = 0, videoCount = 0, audioCount = 0, flashCount = 0, resourcesCount = 0, checkScorm = 0;
        #endregion

        #region Folders Path
        private string rootFolder;
        /// <summary>
        /// Thư mục gốc
        /// </summary>
        public string RootFolder
        {
            get
            {
                if (!string.IsNullOrEmpty(rootFolder) && !Directory.Exists(rootFolder))
                {
                    Directory.CreateDirectory(rootFolder);
                }
                return rootFolder;
            }
            set { rootFolder = value; }
        }

        /// <summary>
        /// Đường dẫn chứa css
        /// </summary>
        public string CssFolder
        {
            get
            {
                var cssFolder = Path.Combine(RootFolder, "css");
                if (!Directory.Exists(cssFolder))
                {
                    Directory.CreateDirectory(cssFolder);
                }
                return cssFolder;
            }
        }

        /// <summary>
        /// Đường dẫn chứa data
        /// </summary>
        public string DataFolder
        {
            get
            {
                var pFolder = Path.Combine(RootFolder, "data");
                if (!Directory.Exists(pFolder))
                {
                    Directory.CreateDirectory(pFolder);
                }
                return pFolder;
            }
        }

        /// <summary>
        /// Đường dẫn chứa js
        /// </summary>
        public string JsFolder
        {
            get
            {
                var pFolder = Path.Combine(RootFolder, "plugins");
                if (!Directory.Exists(pFolder))
                {
                    Directory.CreateDirectory(pFolder);
                }
                return pFolder;
            }
        }

        /// <summary>
        /// Đường dẫn chứa libs
        /// </summary>
        public string LibsFolder
        {
            get
            {
                var pFolder = Path.Combine(RootFolder, "3rd-libs");
                if (!Directory.Exists(pFolder))
                {
                    Directory.CreateDirectory(pFolder);
                }
                return pFolder;
            }
        }

        /// <summary>
        /// Đường dẫn chứa resources
        /// </summary>
        public string ResourcesFolder
        {
            get
            {
                var pFolder = Path.Combine(RootFolder, "resources");
                if (!Directory.Exists(pFolder))
                {
                    Directory.CreateDirectory(pFolder);
                }
                return pFolder;
            }
        }
        /// <summary>
        /// Đường dẫn chứa pros
        /// </summary>
        public string ProsFolder
        {
            get
            {
                var pFolder = Path.Combine(RootFolder, "pros");
                if (!Directory.Exists(pFolder))
                {
                    Directory.CreateDirectory(pFolder);
                }
                return pFolder;
            }
        }
        /// <summary>
        /// Đường dẫn chứa image
        /// </summary>
        public string ImageFolder
        {
            get
            {
                var pFolder = Path.Combine(ResourcesFolder, "images");
                if (!Directory.Exists(pFolder))
                {
                    Directory.CreateDirectory(pFolder);
                }
                return pFolder;
            }
        }

        /// <summary>
        /// Đường dẫn chứa video
        /// </summary>
        public string VideoFolder
        {
            get
            {
                var pFolder = Path.Combine(ResourcesFolder, "video");
                if (!Directory.Exists(pFolder))
                {
                    Directory.CreateDirectory(pFolder);
                }
                return pFolder;
            }
        }

        /// <summary>
        /// Đường dẫn chứa audio
        /// </summary>
        public string AudioFolder
        {
            get
            {
                var pFolder = Path.Combine(ResourcesFolder, "audio");
                if (!Directory.Exists(pFolder))
                {
                    Directory.CreateDirectory(pFolder);
                }
                return pFolder;
            }
        }

        /// <summary>
        /// Đường dẫn chứa flash
        /// </summary>
        public string FlashFolder
        {
            get
            {
                var pFolder = Path.Combine(ResourcesFolder, "flash");
                if (!Directory.Exists(pFolder))
                {
                    Directory.CreateDirectory(pFolder);
                }
                return pFolder;
            }
        }

        /// <summary>
        /// Đường dẫn chứa resource trong player
        /// </summary>
        public string ResourceFileFolder
        {
            get
            {
                var pFolder = Path.Combine(ResourcesFolder, "resources");
                if (!Directory.Exists(pFolder))
                {
                    Directory.CreateDirectory(pFolder);
                }
                return pFolder;
            }
        }
        /// <summary>
        /// Đường dẫn chứa Chart
        /// </summary>
        public string ChartFolder
        {
            get
            {
                var pFolder = Path.Combine(JsFolder, "Chart");
                if (!Directory.Exists(pFolder))
                {
                    Directory.CreateDirectory(pFolder);
                }
                return pFolder;
            }
        }
        /// <summary>
        /// Đường dẫn chứa Player
        /// </summary>
        public string PlayerFolder
        {
            get
            {
                var pFolder = Path.Combine(JsFolder, "Player");
                if (!Directory.Exists(pFolder))
                {
                    Directory.CreateDirectory(pFolder);
                }
                return pFolder;
            }
        }

        public object HTMLShapeDataHelper { get; private set; }

        #endregion

        #region Constructors
        public HTMLDataHelper(string sFolder)
        {
            this.RootFolder = sFolder;
        }
        public HTMLDataHelper()
        {
        }
        #endregion


        #region Sinh ra HTML
        public void RenderHtml(DocumentControl documentControl)
        {
            DirectoryCopy(System.IO.Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "HTMLTemplate/HTML5"), RootFolder, true, new string[] { "style.css", "style.stg" });
            //SINH MÃ GIAO DIỆN
            GetPlayerConfigData();

            var dataPath = Path.Combine(DataFolder, "data.js");
            //nhieu dong
            //WriteFile($"var DOC=" + JsonConvert.SerializeObject(GetHTMLData(documentControl), Formatting.Indented) + ";" + Environment.NewLine, dataPath); //Ghi dữ liệu của bài giảng - dạng JSON ra tập tin
            //1 dong
            WriteFile($"var DOC=" + JsonConvert.SerializeObject(GetHTMLData(documentControl), Formatting.None) + ";" + Environment.NewLine, dataPath); //Ghi dữ liệu của bài giảng - dạng JSON ra tập tin
            if (checkScorm == 1)
            {
                if (File.Exists(rootFolder + ".zip"))
                {
                    try
                    {
                        File.Delete(rootFolder + ".zip");
                    }
                    catch (Exception msg)
                    {

                    }
                }
                if (Directory.Exists(rootFolder))
                {
                    try
                    {
                        ZipFile.CreateFromDirectory(rootFolder, rootFolder + ".zip");
                        Directory.Delete(rootFolder, true);
                    }
                    catch
                    {
                    }
                }
                checkScorm = 0;
            }
        }

        /// <summary>
        /// Stop Media
        /// </summary>
        /// <param name="objectElement"></param>
        public void StopMedia(ObjectElement objectElement)
        {
            if (objectElement is VideoPlayer videoElement)
            {
                videoElement.Stop();
            }
            else if (objectElement is Audio audio)
            {
                audio.Stop();
            }
        }

        public void RenderPreview(DocumentControl documentControl, string slideId)
        {
            //Stop Media khi Preview
            if (Application.Current is IAppGlobal app)
            {
                foreach (var item in app.SelectedSlide.MainLayout.Elements)
                {
                    StopMedia(item);
                }
            }
            _checkPreview = true;
            DirectoryCopy(System.IO.Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "HTMLTemplate/HTML5"), RootFolder, true, new string[] { "style.css", "style.stg" });
            //SINH MÃ GIAO DIỆN
            GetPlayerConfigData();

            var dataPath = Path.Combine(DataFolder, "data.js");
            WriteFile($"var DOC=" + JsonConvert.SerializeObject(GetHTMLData(documentControl, slideId), Formatting.None) + ";" + Environment.NewLine, dataPath); //Ghi dữ liệu của bài giảng - dạng JSON ra tập tin
        }

        /// <summary>
        /// Sao chép folder
        /// </summary>
        /// <param name="sourceDirName">Đường dẫn mực mục nguồn</param>
        /// <param name="destDirName">Đường dẫn cần chuyển tới</param>
        /// <param name="copySubDirs">Có sao chép thư mục con hay không</param>
        /// <param name="extFiles">Danh sách tên tập tin không sao chép</param>
        private static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs, string[] extFiles)
        {
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);

            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            DirectoryInfo[] dirs = dir.GetDirectories();
            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }


            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                if (!extFiles.Contains(file.Name))
                {
                    string temppath = Path.Combine(destDirName, file.Name);
                    file.CopyTo(temppath, true);
                }
            }

            if (copySubDirs)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    string temppath = Path.Combine(destDirName, subdir.Name);
                    DirectoryCopy(subdir.FullName, temppath, copySubDirs, extFiles);
                }
            }
        }

        #endregion

        object FindResource(string key)
        {
            return System.Windows.Application.Current?.TryFindResource(key);
        }

        /// <summary>
        /// Lấy dữ liệu bài giảng
        /// </summary>
        /// <param name="elDocument"></param>
        /// <returns></returns>
        public HTMLELDocument GetHTMLData(DocumentControl elDocument)
        {
            HTMLELDocument htmlElDoc = new HTMLELDocument();
            htmlElDoc.Slides = new List<SlideBase.HTMLSlideBase>();
            if ((Application.Current as IAppGlobal).Language == null)
            {
                htmlElDoc.Language = "en-us";
            }
            else
            {
                htmlElDoc.Language = (Application.Current as IAppGlobal).Language.ToLower();
            }

            htmlElDoc.IsLicense = (Application.Current as IAppGlobal).IsLicensed;
            foreach (var slide in elDocument.Slides)
            {
                htmlElDoc.Slides.Add(GetHTMLSlideData(slide));
            }

            htmlElDoc.Variables = new List<HTMLVariable>();

            foreach (VariableViewModel item in elDocument.Variables)
            {
                if (item != null)
                {
                    string value = ((item.GetModel() as VariableModel)?.GetData() as VariableData)?.Value;
                    htmlElDoc.Variables.Add(new HTMLVariable(item.ID, item.Name, item.Type, value));
                }
            }
            ((Application.Current as IAppGlobal).PlayerDataSetting as MainDataContextViewModel).RefreshData();
            ///Lấy data của player
            EPlayer eplayer = new EPlayer();
            eplayer = ((Application.Current as IAppGlobal).PlayerDataSetting as MainDataContextViewModel).Data;
            //VIET:
            //kiểm tra xem có xuất theo LMS
            htmlElDoc.ScormProp = new HTMLScorm();
            ProcessExport processExport;
            Info info;
            if (eplayer.EPublishModel.PublishType == PublishType.LMS)
            {
                processExport = new ProcessExport(this.RootFolder);
                info = new Info();
                info.Path = eplayer.EPublishModel.Folder;
                info.Desc = "Avina tri viet";
                info.Title = eplayer.EPublishModel.Title;
                info.Identifier = eplayer.EPublishModel.Identifier;
                if (eplayer.EPublishModel.LMSOutput.ToString().ToUpper() == "SCORM12")
                {
                    processExport.ExportScorm12(info);
                    checkScorm = 1;
                    htmlElDoc.ScormProp.NameScorm = "1.2";
                }
                else if (eplayer.EPublishModel.LMSOutput.ToString().ToUpper() == "SCORM2004")
                {
                    var versionScorm = eplayer.EPublishModel.LMSEdition.ToString();
                    processExport.ExportScorm2004(info, versionScorm);
                    checkScorm = 1;
                    htmlElDoc.ScormProp.NameScorm = "2004";
                }
                else if (eplayer.EPublishModel.LMSOutput.ToString().ToUpper() == "TINCANAPI")
                {
                    processExport.ExportXapi(info);
                    checkScorm = 1;
                    htmlElDoc.ScormProp.NameScorm = "xapi";
                }
            }
            if (eplayer.EPublishModel.PublishType == PublishType.CD)
            {
                processExport = new ProcessExport(this.RootFolder);
                info = new Info();
                processExport.ExportCDRoom(info);
            }

            ///Lưu trữ vào json
            HTMLPlayer player = new HTMLPlayer();
            //Lưu trữ các thuộc tính ẩn hiện các tab
            string menuTab = FindResource("UCMENU_Menu")?.ToString();
            string resourceTab = FindResource("UCRESOURCE_Resource")?.ToString();
            string glossaryTab = FindResource("UCGlossary_Title")?.ToString();
            string noteTab = FindResource("MAINDATACONTEXT_Note")?.ToString();
            foreach (EFunctionCatalog item in eplayer.EFeatures.Catalogs)
            {
                if (item.NameCatalog == menuTab)
                {
                    player.IsMenuEnable = item.Selected;
                    player.MenuPosition = eplayer.EFeatures.Catalogs.IndexOf(item) + 1;
                }
                else if (item.NameCatalog == resourceTab)
                {
                    player.IsResourceEnable = item.Selected;
                    player.ResourcePosition = eplayer.EFeatures.Catalogs.IndexOf(item) + 1;
                }
                else if (item.NameCatalog == glossaryTab)
                {
                    player.IsGlossaryEnable = item.Selected;
                    player.GlossaryPosition = eplayer.EFeatures.Catalogs.IndexOf(item) + 1;
                }
                else if (item.NameCatalog == noteTab)
                {
                    player.IsNotesEnable = item.Selected;
                    player.NotePosition = eplayer.EFeatures.Catalogs.IndexOf(item) + 1;
                }


            }
            player.IsSeekBarEnable = eplayer.EFeatures.IsSeekbarEnabled;
            player.IsVolumeEnable = eplayer.EFeatures.IsVolumeEnabled;

            player.IsLogoEnable = eplayer.EFeatures.IsLogoEnabled;

            player.IsSearchEnable = eplayer.EFeatures.IsSearchEnabled;

            if (player.IsLogoEnable && !string.IsNullOrEmpty(eplayer.EFeatures.ImgLogo.Path))
            {
                player.LogoUrl = CopyImageToExportFolder(eplayer.EFeatures.ImgLogo.Path).Replace(RootFolder, "").Replace("\\", "/");
            }

            ///Lưu trữ glossary
            List<HTMLGlossary> ListGlossary = new List<HTMLGlossary>();
            foreach (EGlossary glossaryItem in eplayer.EUCGlossary.EGlossaries)
            {
                HTMLGlossary glossary = new HTMLGlossary();
                glossary.Definition = glossaryItem.Definition;
                glossary.Term = glossaryItem.Term;
                ListGlossary.Add(glossary);
            }
            player.Glossaries = ListGlossary;
            player.ResourcesDescription = eplayer.EUCResource.Description;

            ///Lưu trữ Resource
            List<HTMLResource> ListResource = new List<HTMLResource>();
            foreach (EResource resourceItem in eplayer.EUCResource.EResources)
            {
                HTMLResource resource = new HTMLResource();
                if (resourceItem.URL != null)
                {
                    resource.URL = resourceItem.URL;
                }
                if (resourceItem.File != null)
                {
                    resource.FileURL = "./" + CopyResourceFileToExportFolder(resourceItem.File).Replace(RootFolder, "").Replace("\\", "/"); ;
                }
                resource.Title = resourceItem.Title;
                ListResource.Add(resource);
            }
            player.Resources = ListResource;
            player.Title = eplayer.EPublishModel.Title;
            player.User = eplayer.EFeatures.Writer;
            player.Description = eplayer.EPublishModel.Description;

            if (!eplayer.EFeatures.IsTitleEnabled)
            {
                player.Title = "";
            }


            if (!eplayer.EFeatures.IsWriterEnabled)
            {
                player.User = "";
            }

            htmlElDoc.Player = player;
            player.ColorTheme = eplayer.EUCBaseColor.SelectedScheme.TemplateColor.BackgroundTopBottomBar.KeyColor;
            htmlElDoc.Key = ObjectElementsHelper.RandomString(10);
            if (checkScorm == 1)
            {
                htmlElDoc.Scorm = true;
            }
            else
            {
                htmlElDoc.Scorm = false;
            }
            htmlElDoc.ScormProp.ScormEdition = eplayer.EPublishModel.LMSEdition.ToString();
            htmlElDoc.ScormProp.Identifier = eplayer.EPublishModel.Identifier;
            htmlElDoc.ScormProp.Version = eplayer.EPublishModel.Version;
            htmlElDoc.ScormProp.Duration = eplayer.EPublishModel.Duration;
            htmlElDoc.ScormProp.KeyWord = eplayer.EPublishModel.KeyWord;
            htmlElDoc.ScormProp.TitleLesson = eplayer.EPublishModel.TitleLMSLesson;
            htmlElDoc.ScormProp.IdentifyLesson = eplayer.EPublishModel.IdentifierLMSLesson;
            htmlElDoc.ScormProp.ReportStatus = eplayer.EPublishModel.LMSReportStatus.ToString();
            htmlElDoc.ScormTracking = new HTMLTracking();
            htmlElDoc.ScormTracking.NumberOfSlides = eplayer.EPublishModel.NumberOfSlides;
            htmlElDoc.ScormTracking.ResultID = eplayer.EPublishModel.IDResultSlide;
            htmlElDoc.ScormTracking.TrackingStatus = eplayer.EPublishModel.TrackingStatus.ToString();
            return htmlElDoc;
        }

        #region Preview Tool
        public int GetIndexSlide(DocumentControl documentControl, string idSlide)
        {
            for (int i = 0; i < documentControl.Slides.Count; i++)
            {
                if (documentControl.Slides[i].ID.Equals(idSlide))
                {
                    return i;
                }
            }
            return -1;
        }
        /// <summary>
        /// Lấy dữ liệu bài giảng cho Preview
        /// </summary>
        /// <param name="elDocument"></param>
        /// <param name="slideId"></param>
        /// <param name="previewSlide">Load slide</param>
        /// <returns></returns>
        public HTMLELDocument GetHTMLData(DocumentControl elDocument, string slideId = null)
        {

            HTMLELDocument htmlElDoc = new HTMLELDocument();
            htmlElDoc.IsPreview = true;
            htmlElDoc.Slides = new List<SlideBase.HTMLSlideBase>();
            if ((Application.Current as IAppGlobal).Language == null)
            {
                htmlElDoc.Language = "en-us";
            }
            else
            {
                htmlElDoc.Language = (Application.Current as IAppGlobal).Language.ToLower();
            }
            htmlElDoc.IsLicense = (Application.Current as IAppGlobal).IsLicensed;
            if (slideId == null)
            {
                foreach (var slide in elDocument.Slides)
                {
                    htmlElDoc.Slides.Add(GetHTMLSlideData(slide));
                }
            }
            else
            {
                var slideCurrent = elDocument.Slides.FirstOrDefault(x => x.ID.Equals(slideId));
                if (slideCurrent != null)
                {
                    htmlElDoc.Slides.Add(GetHTMLSlideData(slideCurrent));
                }
            }

            htmlElDoc.Variables = new List<HTMLVariable>();

            foreach (VariableViewModel item in elDocument.Variables)
            {
                if (item != null)
                {
                    string value = ((item.GetModel() as VariableModel)?.GetData() as VariableData)?.Value;
                    htmlElDoc.Variables.Add(new HTMLVariable(item.ID, item.Name, item.Type, value));
                }
            }
            ((Application.Current as IAppGlobal).PlayerDataSetting as MainDataContextViewModel).RefreshData();
            ///Lấy data của player
            EPlayer eplayer = new EPlayer();
            eplayer = ((Application.Current as IAppGlobal).PlayerDataSetting as MainDataContextViewModel).Data;


            ///Lưu trữ vào json
            HTMLPlayer player = new HTMLPlayer();

            //Lưu trữ các thuộc tính ẩn hiện các tab
            string menuTab = FindResource("UCMENU_Menu")?.ToString();
            string resourceTab = FindResource("UCRESOURCE_Resource")?.ToString();
            string glossaryTab = FindResource("UCGlossary_Title")?.ToString();
            string noteTab = FindResource("MAINDATACONTEXT_Note")?.ToString();
            foreach (EFunctionCatalog item in eplayer.EFeatures.Catalogs)
            {
                if (item.NameCatalog == menuTab)
                {
                    player.IsMenuEnable = item.Selected;
                    player.MenuPosition = eplayer.EFeatures.Catalogs.IndexOf(item) + 1;
                }
                else if (item.NameCatalog == resourceTab)
                {
                    player.IsResourceEnable = item.Selected;
                    player.ResourcePosition = eplayer.EFeatures.Catalogs.IndexOf(item) + 1;
                }
                else if (item.NameCatalog == glossaryTab)
                {
                    player.IsGlossaryEnable = item.Selected;
                    player.GlossaryPosition = eplayer.EFeatures.Catalogs.IndexOf(item) + 1;
                }
                else if (item.NameCatalog == noteTab)
                {
                    player.IsNotesEnable = item.Selected;
                    player.NotePosition = eplayer.EFeatures.Catalogs.IndexOf(item) + 1;
                }
            }
            player.IsSeekBarEnable = eplayer.EFeatures.IsSeekbarEnabled;
            player.IsVolumeEnable = eplayer.EFeatures.IsVolumeEnabled;

            player.IsLogoEnable = eplayer.EFeatures.IsLogoEnabled;

            player.IsSearchEnable = eplayer.EFeatures.IsSearchEnabled;

            if (player.IsLogoEnable && !string.IsNullOrEmpty(eplayer.EFeatures.ImgLogo.Path))
            {
                player.LogoUrl = CopyImageToExportFolder(eplayer.EFeatures.ImgLogo.Path).Replace(RootFolder, "").Replace("\\", "/");
            }

            ///Lưu trữ glossary
            List<HTMLGlossary> ListGlossary = new List<HTMLGlossary>();
            foreach (EGlossary glossaryItem in eplayer.EUCGlossary.EGlossaries)
            {
                HTMLGlossary glossary = new HTMLGlossary();
                glossary.Definition = glossaryItem.Definition;
                glossary.Term = glossaryItem.Term;
                ListGlossary.Add(glossary);
            }
            player.Glossaries = ListGlossary;
            player.ResourcesDescription = eplayer.EUCResource.Description;

            ///Lưu trữ Resource
            List<HTMLResource> ListResource = new List<HTMLResource>();
            foreach (EResource resourceItem in eplayer.EUCResource.EResources)
            {
                HTMLResource resource = new HTMLResource();
                resource.FileURL = resourceItem.File;
                resource.URL = resourceItem.URL;
                resource.Title = resourceItem.Title;
                ListResource.Add(resource);
            }
            player.Resources = ListResource;



            player.Title = eplayer.EPublishModel.Title;
            player.User = eplayer.EFeatures.Writer;

            if (!eplayer.EFeatures.IsTitleEnabled)
            {
                player.Title = "";
            }


            if (!eplayer.EFeatures.IsWriterEnabled)
            {
                player.User = "";
            }

            htmlElDoc.Player = player;
            player.ColorTheme = eplayer.EUCBaseColor.SelectedScheme.TemplateColor.BackgroundTopBottomBar.KeyColor;
            return htmlElDoc;
        }
        #endregion


        /// <summary>
        /// Lấy dữ liệu trang trình chiếu
        /// </summary>
        /// <param name="slide"></param>
        /// <returns></returns>
        private HTMLSlideBase GetHTMLSlideData(Core.View.SlideBase slide)
        {
            HTMLSlideBase htmlSlide = null;
            if (slide is QuestionBaseView question)
            {
                htmlSlide = CreatHTMLQuestion(question.Type);
                (htmlSlide as HTMLQuestionBase).GroupType = question.GroupType;
                (htmlSlide as HTMLQuestionBase).ID = question.ID;
                (htmlSlide as HTMLQuestionBase).Attemps = question.Attemps;
                (htmlSlide as HTMLQuestionBase).IsShuffle = question.IsShuffleAnswer;
                (htmlSlide as HTMLQuestionBase).SlideKeyType = "QuestionSlide";
                (htmlSlide as HTMLQuestionBase).FeedbackType = Enum.GetName(typeof(FeedBackType), question.FeedBackType).ToString();
                if (htmlSlide is HTMLNumericQuestion numericSlide)
                {
                    numericSlide.NumericConfig = new HMLTNumericConfig();
                    numericSlide.NumericConfig.NumericMode = (question as NumericQuestionView).Mode;
                    numericSlide.NumericConfig.Values = new List<Questions.Answers.HTMLNumericValue>();
                    foreach (var value in (question as NumericQuestionView).NumericAnswers)
                    {
                        if (value is ENumericAnswer valuec)
                        {
                            var newValue = new HTMLNumericValue() { Compare = valuec.Compare, Value1 = valuec.Value1, Value2 = valuec.Value2 };
                            numericSlide.NumericConfig.Values.Add(newValue);
                        }
                    }
                }
                else if (htmlSlide is HTMLEssayQuestion essay)
                {
                    essay.MaxLength = (question as EssayQuestionView).MaxLenght;
                }

            }
            else if (slide is ResultSlide resultSlide)
            {
                htmlSlide = new HTMLResultSlide();
                (htmlSlide as HTMLResultSlide).ID = resultSlide.ID;
                (htmlSlide as HTMLResultSlide).SlideKeyType = "Result";
                (htmlSlide as HTMLResultSlide).Duration = resultSlide.GroupQuestion.Duration * 60;
                (htmlSlide as HTMLResultSlide).Attemps = resultSlide.GroupQuestion.Attemps;
                (htmlSlide as HTMLResultSlide).Passing = resultSlide.GroupQuestion.Pass;
                (htmlSlide as HTMLResultSlide).TimerFormat = resultSlide.GroupQuestion.TimerFomat.ToString();
                (htmlSlide as HTMLResultSlide).StartTimer = resultSlide.GroupQuestion.StartTimer.ToString();
                (htmlSlide as HTMLResultSlide).IsTimeLimit = resultSlide.GroupQuestion.IsTimeLimit;
                (htmlSlide as HTMLResultSlide).IsGradedResult = resultSlide.IsGradedResult;
                (htmlSlide as HTMLResultSlide).Questions = new List<string>();
                (htmlSlide as HTMLResultSlide).Variables = new List<VariableResult>() {
                    new VariableResult(resultSlide.ScorePoints.ID, resultSlide.ScorePoints.Type),
                    new VariableResult(resultSlide.ScorePercents.ID, resultSlide.ScorePercents.Type),
                    new VariableResult(resultSlide.PassPoints.ID, resultSlide.PassPoints.Type),
                    new VariableResult(resultSlide.PassPercents.ID, resultSlide.PassPercents.Type),

                };

                // lấy danh sách các slide chưa trong result
                foreach (GroupQuestion qtResult in resultSlide.GroupQuestion.Groups)
                {
                    if (qtResult.IsGroup && (Application.Current as IAppGlobal).DocumentControl.Slides.FirstOrDefault(p => p.ID == qtResult.ID) != null)
                        (htmlSlide as HTMLResultSlide).Questions.Add(qtResult.ID);
                }
            }
            else
            {
                htmlSlide = new HTMLSlideBase();
                htmlSlide.SlideKeyType = "Normal";
            }


            htmlSlide.ID = slide.ID;
            htmlSlide.Name = slide.SlideName;
            htmlSlide.IsHideBackground = slide.IsHideBackground;
            //htmlSlide.Theme = GetHTMLThemeData(new ETheme());
            htmlSlide.CanShowInMenu = slide.CanShowInMenu;
            htmlSlide.Layouts = new List<HTMLLayoutBase>();
            htmlSlide.Width = (Application.Current as IAppGlobal).SlideSize.Width;
            htmlSlide.Height = (Application.Current as IAppGlobal).SlideSize.Height;
            htmlSlide.Note = slide.Note;

            //Lưu trữ slideConfig
            HTMLSlideConfig slideConfig = new HTMLSlideConfig();
            if (slide.PageConfig != null)
            {
                slideConfig.LoadData(slide.PageConfig);
            }
            htmlSlide.SlideConfig = slideConfig;

            SetHTMLSubInfo(slide);
            if (!slide.IsHideBackground)
            {
                HTMLLayoutBase themeLayout = GetHTMLLayoutData(slide.ThemeLayout);
                themeLayout.LayoutKeyType = "Theme";
                htmlSlide.ThemeLayout = themeLayout;
            }

            htmlSlide.Layouts.Add(GetHTMLLayoutData(slide.MainLayout));

            if (slide.MainLayout.ThumbnailBitmap is BitmapSource bitmap)
            {
                htmlSlide.ThumbnailUrl = GetImage(bitmap, Path.Combine(this.ImageFolder, $"{slide.MainLayout.ID}_thumbnail.png")).Replace(this.RootFolder, "").Replace("\\", "/");
            }

            if (slide.Timing != null)
            {
                htmlSlide.Timing = new HTMLTiming();
                htmlSlide.Timing.StartTime = 0;
                htmlSlide.Timing.Duration = Math.Round(slide.Timing.TotalTime, 0);
            }
            if (slide.MainLayout?.Timing is Timing timing)
            {
                htmlSlide.Timing.Duration = Math.Round(timing.TotalTime, 0);
            }
            foreach (var layout in slide.Layouts)
            {
                SetHTMLData(layout, slide.ID);
                htmlSlide.Layouts.Add(GetHTMLLayoutData(layout));
            }
            htmlSlide.Transition = CopyTransitionData(slide);
            return htmlSlide;
        }

        private void SetHTMLSubInfo(Core.View.SlideBase slide)
        {
            if ((Application.Current as IAppGlobal).DocumentControl?.Slides != null)
            {
                int
                    count = (Application.Current as IAppGlobal).DocumentControl.Slides.Count,
                    currentId = (Application.Current as IAppGlobal).DocumentControl.Slides.IndexOf(slide);
                string previousId = "", nextId = "";

                if (currentId > -1)
                {
                    if (currentId + 1 < count && (Application.Current as IAppGlobal).DocumentControl.Slides[currentId + 1] is Core.View.SlideBase nextSlide)
                    {
                        int tempc = 1;
                        while (!nextSlide.CanShowInMenu)
                        {
                            tempc++;
                            if (currentId + tempc < count)
                            {
                                nextSlide = (Application.Current as IAppGlobal).DocumentControl.Slides[currentId + tempc];
                            }
                            else
                            {
                                nextSlide = null;
                                break;
                            }
                        }
                        if (nextSlide != null)
                            nextId = nextSlide.ID;
                    }

                    if (currentId - 1 > -1 && (Application.Current as IAppGlobal).DocumentControl.Slides[currentId - 1] is Core.View.SlideBase previousSlide)
                    {
                        int tempc = 1;
                        while (!previousSlide.CanShowInMenu)
                        {
                            tempc++;
                            if (currentId - tempc > -1)
                            {
                                previousSlide = (Application.Current as IAppGlobal).DocumentControl.Slides[currentId - tempc];
                            }
                            else
                            {
                                previousSlide = null;
                                break;
                            }
                        }
                        if (previousSlide != null)
                            previousId = previousSlide.ID;
                    }
                }
                SetLayerSubInfo(slide.MainLayout, previousId, nextId);
                foreach (Core.View.LayoutBase layer in slide.Layouts)
                {
                    SetLayerSubInfo(layer, previousId, nextId);
                }
            }
        }

        private void SetLayerSubInfo(Core.View.LayoutBase mainLayout, string previousId, string nextId)
        {
            SetPreviousSlide(mainLayout, previousId);
            SetNextSlide(mainLayout, nextId);
            foreach (ObjectElement item in mainLayout.Elements)
            {
                SetPreviousSlide(item, previousId);
                SetNextSlide(item, nextId);
            }
        }

        /// <summary>
        /// Lưu ảnh từ bitmap
        /// </summary>
        /// <param name="bitmap"></param>
        /// <param name="imagePath"></param>
        private static string GetImage(BitmapSource bitmap, string imagePath)
        {
            using (FileStream outStream = new FileStream(imagePath, FileMode.Create))
            {
                BitmapEncoder encoder = new PngBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(bitmap));
                encoder.Save(outStream);
                encoder = null;
            }
            return imagePath;
        }

        /// <summary>
        /// Lấy dữ liệu Giao diện theme
        /// </summary>
        /// <param name="eTheme"></param>
        /// <returns></returns>
        private HTMLTheme GetHTMLThemeData(ETheme eTheme)
        {
            //if (eTheme.SelectedVariant?.Layout?.ShapeLayouts.Count > 0)
            //{
            //    HTMLTheme htmlTheme = new HTMLTheme();
            //    htmlTheme.Shapes = new List<HTMLThemeShape>();
            //    foreach (var shape in eTheme.SelectedVariant.Layout.ShapeLayouts)
            //    {
            //        if (shape is EThemeRectangle rect)
            //        {
            //            htmlTheme.Shapes.Add(new HTMLThemeRectangle()
            //            {
            //                Height = rect.Height,
            //                Width = rect.Width,
            //                Top = rect.Top,
            //                Left = rect.Left,
            //                StrokeThickness = rect.StrokeThickness,

            //            });
            //        }
            //    }
            //    return htmlTheme;
            //}
            return null;
        }
        //int indexELement = 0;
        /// <summary>
        /// Lấy dữ liệu một lớp hiển thị
        /// </summary>
        /// <param name="mainLayout"></param>
        /// <returns></returns>
        private HTMLLayoutBase GetHTMLLayoutData(Core.View.LayoutBase mainLayout)
        {
            HTMLLayoutBase htmlLayout = null;
            if (mainLayout is GradedFeedbackLayer feedbackLayer)
            {

                htmlLayout = new HTMLFeedBack();
                (htmlLayout as HTMLFeedBack).LayoutKeyType = Enum.GetName(typeof(FeedBackStatus), feedbackLayer.FeedBackStatus).Replace("_", " ");
                if (feedbackLayer.Feedback != null)
                    (htmlLayout as HTMLFeedBack).Score = feedbackLayer.Feedback.Score;
                var listSlides = (Application.Current as IAppGlobal).DocumentControl.Slides;
                (htmlLayout as HTMLFeedBack).AudioSource = feedbackLayer.Feedback.AudioUri;
            }
            else if (mainLayout is FailureLayer failureLayer)
            {
                htmlLayout = new HTMLResultLayout() { LayoutKeyType = "FailureLayout" };
            }
            else if (mainLayout is SuccessLayer)
            {
                htmlLayout = new HTMLResultLayout() { LayoutKeyType = "ScuccessLayout" };
            }
            else
            {
                htmlLayout = new HTMLLayoutBase();
                htmlLayout.LayoutKeyType = "Normal";
            }

            if (htmlLayout != null && mainLayout != null)
            {
                htmlLayout.IsMainLayout = mainLayout.IsMainLayout;
                htmlLayout.ID = mainLayout.ID;
                htmlLayout.Name = mainLayout.Name;
                if (mainLayout is ThemeLayout)
                {
                    htmlLayout.Background = new HTMLSolid() { Color = "transparent" };
                }
                else htmlLayout.Background = GetColor(mainLayout.Fill);



                if (mainLayout?.Timing != null)
                {
                    htmlLayout.Timing = new HTMLTiming();
                    htmlLayout.Timing.StartTime = 0;
                    htmlLayout.Timing.Duration = Math.Round(mainLayout.Timing.TotalTime, 0);
                }

                if (htmlLayout.IsMainLayout == true)
                {
                    htmlLayout.IsVisible = true;
                }
                else
                {
                    //Layout Config
                    HTMLLayoutConfig layoutConfig = new HTMLLayoutConfig();
                    if (mainLayout?.LayoutConfig != null)
                    {
                        layoutConfig.LoadData(mainLayout.LayoutConfig);
                    }
                    htmlLayout.LayoutConfig = layoutConfig;
                }
                htmlLayout.Elements = new List<HTMLObjectElement>();
                //indexELement = 0;
                foreach (var element in mainLayout.Elements)
                {
                    HTMLDataHelper.SetHTMLData(element, mainLayout?.ID);
                    var _elementData = _isCheckPreview ? GetHTMLElementData(element, true) : GetHTMLElementData(element);
                    if (_elementData != null)
                    {
                        htmlLayout.Elements.Add(_elementData);
                    }
                }

                htmlLayout.Triggers = new List<Trigger.HTMLTrigger>();

                foreach (TriggerViewModel item in mainLayout.TriggerData)
                {
                    if (item != null)
                    {
                        item.UpdateModel();
                        if (item.Trigger is TriggerModel model)
                        {
                            if (model.GetData() is TriggerDataObject data)
                            {
                                if (data.Source?.ToString().ToUpper() == EValue.ThisLayer.ToString().ToUpper())
                                {
                                    data.Source = mainLayout?.ID;
                                }
                                else if (data.Source?.ToString().ToUpper() == EValue.NextSlide.ToString().ToUpper())
                                {
                                    data.Source = GetNextSlide(mainLayout);
                                }
                                else if (data.Source?.ToString().ToUpper() == EValue.PreviousSlide.ToString().ToUpper())
                                {
                                    data.Source = GetPreviousSlide(mainLayout);
                                }

                                if (data.Action == EAction.CloseLightbox)
                                {
                                    data.Source = mainLayout?.ID;
                                }

                                HTMLTrigger trigger = new HTMLTrigger()
                                {
                                    Action = data.Action,
                                    Source = data.Source,
                                    SourceDetail = data.SourceDetail.GetHTML(),
                                    Event = data.Event,
                                    Target = data.Target,
                                    TargetDetail = data.TargetDetail.GetHTML(),
                                    Conditions = data.Conditions.GetHTML()
                                };
                                htmlLayout.Triggers.Add(trigger);
                                //if (trigger.Event == EEvent.UserPressesAKey)
                                //{
                                //    trigger.Target += "_w";
                                //}
                            }
                        }
                    }
                }
            }

            return htmlLayout;
        }

        #region Get Data

        /// <summary>
        /// Lấy dữ liệu cho đối tượng
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        private HTMLObjectElement GetHTMLElementData(ObjectElement element)
        {
            HTMLObjectElement resultData = null;
            if (element is StandardElement standard && element.IsShow)
            {
                if (resultData != null) goto CopyStandardProperties;

                #region Text Element
                if (element is TextEditor textEditor) //Lấy dữ liệu cho đối tượng văn bản
                {
                    resultData = new HTMLTextElement();
                    CopyDataTextElement(textEditor.RichTextEditor, resultData as HTMLTextElement);
                    goto CopyStandardProperties;
                }
                #endregion

                #region Answer Element

                if (element is GroupAnswer groupAnswer && !(element is WordbankContent)) // lấy dữ liệu của group Answer 
                {
                    resultData = GetAnswerContentQuestion(groupAnswer);
                    goto CopyStandardProperties;
                }
                #endregion

                #region Image Element

                if (element is ImageContent imageContent) //Lấy dữ liệu cho đối tượng hình ảnh
                {
                    resultData = new HTMLImageElement();

                    (resultData as HTMLImageElement).ImageUrl = GetImageFromImageContent(imageContent).Replace(RootFolder + "\\", "");
                    goto CopyStandardProperties;
                }
                #endregion

                if (resultData != null)
                {

                }
                CopyStandardProperties:
                if (resultData == null) resultData = new HTMLStandardElement();
                CopyData(standard, (resultData as HTMLStandardElement));
                if (element is TextEditor text)
                {
                    if (text.ShapePresent.StrokeThickness == 1.191)
                    {
                        List<HTMLPathObject> list = (resultData as HTMLStandardElement).Shapes.PathObjects;
                        if (list != null)
                        {
                            foreach (HTMLPathObject item in list)
                            {
                                item.StrokeThickness = 0;
                            }
                        }
                    }
                }
            }

            #region Audio Element

            if (element is Audio audio) //Lấy dữ liệu cho đối tượng audio
            {
                resultData = new HTMLAudioElement();
                (resultData as HTMLAudioElement).MediaUrl = CopyAudioToExportFolder(audio.AudioFileName).Replace(RootFolder + "\\", "");
                (resultData as HTMLAudioElement).Caption = GetSubtitle(audio.CaptionFileName);
                (resultData as HTMLAudioElement).IsLoop = audio.IsLoop;
                (resultData as HTMLAudioElement).IsAcross = audio.IsPlayAcross;
                (resultData as HTMLAudioElement).Volume = audio.VolumeType.ToString();
                goto CopyObjectProperties;
            }
            #endregion

            #region Flash Element

            if (element is FlashControl flash) //Lấy dữ liệu cho đối tượng flash
            {
                resultData = new HTMLFlashElement();
                (resultData as HTMLFlashElement).FlashUrl = CopyFlashToExportFolder(flash.FileName).Replace(RootFolder + "\\", "");
                goto CopyObjectProperties;
            }
            #endregion

            #region Video Element

            if (element is VideoPlayer videoPlayer) //Lấy dữ liệu cho đối tượng video
            {
                resultData = new HTMLVideoElement();
                var sub = GetSubtitle(videoPlayer?.SourceCaption?.OriginalString);
                (resultData as HTMLVideoElement).Caption = sub;
                (resultData as HTMLVideoElement).Compression = videoPlayer.Compression.ToString();
                (resultData as HTMLVideoElement).PlayVideo = videoPlayer.PlayVideo.ToString();
                (resultData as HTMLVideoElement).ShowVideo = videoPlayer.ShowVideo.ToString();
                (resultData as HTMLVideoElement).ShowControl = videoPlayer.ShowControl.ToString();
                (resultData as HTMLVideoElement).Volume = videoPlayer.Volume;
                (resultData as HTMLVideoElement).MediaUrl = CopyVideoToExportFolder(videoPlayer.SourceVideo.OriginalString).Replace(RootFolder + "\\", "");


                goto CopyObjectProperties;
            }
            #endregion

            #region YoutubeVideo
            if (element is YoutubeVideoElement youtube)
            {
                resultData = new HTMLYoutubeElement();
                (resultData as HTMLYoutubeElement).IDVIdeo = youtube.IDVideo;
                (resultData as HTMLYoutubeElement).URLVideo = youtube.URLVideo;
                (resultData as HTMLYoutubeElement).IsAllowFullScreen = youtube.IsAllowFullScreen;
                (resultData as HTMLYoutubeElement).IsAutoPlay = youtube.IsAutoPlay;
                (resultData as HTMLYoutubeElement).IsHideAnnotation = youtube.IsHideAnnotation;
                (resultData as HTMLYoutubeElement).IsHideControl = youtube.IsHideControl;
                (resultData as HTMLYoutubeElement).IsSpecificPart = youtube.IsSpecificPart;
                (resultData as HTMLYoutubeElement).StartTime = youtube.StartTime;
                (resultData as HTMLYoutubeElement).EndTime = youtube.EndTime;
                goto CopyObjectProperties;

            }
            #endregion

            #region Chart Element
            if (element is TChart chart) // Lấy dữ liệu cho đối tượng Chart
            {
                resultData = new HTMLChartElement();
                chart.RefreshData();
                CopyDataChartElement(chart, resultData as HTMLChartElement);

                goto CopyObjectProperties;
            }
            #endregion

            #region Group
            //Nếu là một group các đối tượng
            if (element is GroupContainer group)
            {
                resultData = new HTMLGroupElements();
                foreach (var item in group.Elements)
                {
                    var elementGroup = GetHTMLElementData(item);
                    if (elementGroup != null)
                        (resultData as HTMLGroupElements).Elements.Add(elementGroup);
                }
            }
            #endregion

            CopyObjectProperties:
            CopyData(element, resultData);
            return resultData;
        }

        /// <summary>
        /// Lấy dữ liệu cho đối tượng dùng trong Preview
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        private HTMLObjectElement GetHTMLElementData(ObjectElement element, bool preview = true)
        {
            HTMLObjectElement resultData = null;
            if (element is StandardElement standard && element.IsShow)
            {
                if (resultData != null) goto CopyStandardProperties;

                #region Text Element
                if (element is TextEditor textEditor) //Lấy dữ liệu cho đối tượng văn bản
                {
                    resultData = new HTMLTextElement();
                    CopyDataTextElement(textEditor.RichTextEditor, resultData as HTMLTextElement);
                    goto CopyStandardProperties;
                }
                #endregion

                #region Answer Element

                if (element is GroupAnswer groupAnswer && !(element is WordbankContent)) // lấy dữ liệu của group Answer 
                {
                    resultData = GetAnswerContentQuestion(groupAnswer);
                    goto CopyStandardProperties;
                }
                #endregion

                #region Image Element

                if (element is ImageContent imageContent) //Lấy dữ liệu cho đối tượng hình ảnh
                {
                    resultData = new HTMLImageElement();

                    (resultData as HTMLImageElement).ImageUrl = GetImageFromImageContent(imageContent, preview);
                    goto CopyStandardProperties;
                }
                #endregion
                CopyStandardProperties:
                if (resultData == null) resultData = new HTMLStandardElement();
                CopyData(standard, (resultData as HTMLStandardElement));
                if (element is TextEditor text)
                {
                    if (text.ShapePresent.StrokeThickness == 1.191)
                    {
                        List<HTMLPathObject> list = (resultData as HTMLStandardElement).Shapes.PathObjects;
                        if (list != null)
                        {
                            foreach (HTMLPathObject item in list)
                            {
                                item.StrokeThickness = 0;
                            }
                        }
                    }
                }
            }

            #region Audio Element

            if (element is Audio audio) //Lấy dữ liệu cho đối tượng audio
            {
                resultData = new HTMLAudioElement();
                (resultData as HTMLAudioElement).MediaUrl = audio.AudioFileName;
                (resultData as HTMLAudioElement).Caption = audio.CaptionFileName;
                goto CopyObjectProperties;
            }
            #endregion

            #region Flash Element

            if (element is FlashControl flash) //Lấy dữ liệu cho đối tượng flash
            {
                resultData = new HTMLFlashElement();
                (resultData as HTMLFlashElement).FlashUrl = flash.FileName;
                goto CopyObjectProperties;
            }
            #endregion

            #region Video Element

            if (element is VideoPlayer videoPlayer) //Lấy dữ liệu cho đối tượng video
            {
                resultData = new HTMLVideoElement();
                var sub = GetSubtitle(videoPlayer?.SourceCaption?.OriginalString);
                (resultData as HTMLVideoElement).Caption = sub;
                (resultData as HTMLVideoElement).Compression = videoPlayer.Compression.ToString();
                (resultData as HTMLVideoElement).PlayVideo = videoPlayer.PlayVideo.ToString();
                (resultData as HTMLVideoElement).ShowVideo = videoPlayer.ShowVideo.ToString();
                (resultData as HTMLVideoElement).ShowControl = videoPlayer.ShowControl.ToString();
                (resultData as HTMLVideoElement).MediaUrl = videoPlayer.SourceVideo.OriginalString;
                goto CopyObjectProperties;
            }
            #endregion

            #region YoutubeVideo
            if (element is YoutubeVideoElement youtube)
            {
                resultData = new HTMLYoutubeElement();
                (resultData as HTMLYoutubeElement).IDVIdeo = youtube.IDVideo;
                (resultData as HTMLYoutubeElement).URLVideo = youtube.URLVideo;
                (resultData as HTMLYoutubeElement).IsAllowFullScreen = youtube.IsAllowFullScreen;
                (resultData as HTMLYoutubeElement).IsAutoPlay = youtube.IsAutoPlay;
                (resultData as HTMLYoutubeElement).IsHideAnnotation = youtube.IsHideAnnotation;
                (resultData as HTMLYoutubeElement).IsHideControl = youtube.IsHideControl;
                (resultData as HTMLYoutubeElement).IsSpecificPart = youtube.IsSpecificPart;
                (resultData as HTMLYoutubeElement).StartTime = youtube.StartTime;
                (resultData as HTMLYoutubeElement).EndTime = youtube.EndTime;
                goto CopyObjectProperties;

            }
            #endregion

            #region Chart Element
            if (element is TChart chart) // Lấy dữ liệu cho đối tượng Chart
            {
                resultData = new HTMLChartElement();
                chart.RefreshData();
                CopyDataChartElement(chart, resultData as HTMLChartElement);

                goto CopyObjectProperties;
            }
            #endregion

            #region Group
            //Nếu là một group các đối tượng
            if (element is GroupContainer group)
            {
                resultData = new HTMLGroupElements();
                foreach (var item in group.Elements)
                {
                    var elementGroup = GetHTMLElementData(item);
                    if (elementGroup != null)
                        (resultData as HTMLGroupElements).Elements.Add(elementGroup);
                }
            }
            #endregion

            CopyObjectProperties:
            CopyData(element, resultData);
            return resultData;
        }
        #endregion


        #region Object Element
        /// <summary>
        /// Hàm lấy dữ liệu vào đối tượng lưu trữ<br/>
        /// Dùng cho đối tượng mặc định <see cref="HTMLObjectElement"/>
        /// </summary>
        /// <param name="element"></param>
        /// <param name="resultData"></param>
        private void CopyData(ObjectElement element, HTMLObjectElement resultData)
        {
            if (resultData != null && element != null && element.IsShow)
            {
                resultData.ID = element.ID;
                resultData.Name = element.TargetName;
                resultData.Width = element.Width;
                resultData.Height = element.Height;
                resultData.Top = element.Top;
                resultData.Left = element.Left;
                resultData.Angle = element.Angle;
                resultData.ZIndex = element.ZIndex;
                resultData.IsScaleX = element.IsScaleX;
                resultData.IsScaleY = element.IsScaleY;
                resultData.Timing = GetHTMLTimingData(element.Timing);

                if (!(element is Audio))
                    resultData.Animations = GetHTMLAnimationData(element);

                resultData.Triggers = new List<HTMLTrigger>();

                if (element.TriggerData != null)
                {
                    foreach (TriggerViewModel item in element.TriggerData)
                    {
                        if (item != null)
                        {
                            item.UpdateModel();
                            if (item.Trigger is TriggerModel model)
                            {
                                if (model.GetData() is TriggerDataObject data)
                                {

                                    if (data.Source?.ToString().ToUpper() == EValue.ThisLayer.ToString().ToUpper())
                                    {
                                        data.Source = GetHTMLData(element);
                                    }
                                    else if (data.Source?.ToString().ToUpper() == EValue.NextSlide.ToString().ToUpper())
                                    {
                                        data.Source = GetNextSlide(element);
                                    }
                                    else if (data.Source?.ToString().ToUpper() == EValue.PreviousSlide.ToString().ToUpper())
                                    {
                                        data.Source = GetPreviousSlide(element);
                                    }

                                    if (data.Action == EAction.CloseLightbox)
                                    {
                                        data.Source = GetHTMLData(element);
                                    }

                                    HTMLTrigger trigger = new HTMLTrigger()
                                    {
                                        Action = data.Action,
                                        Source = data.Source,
                                        SourceDetail = data.SourceDetail.GetHTML(),
                                        Event = data.Event,
                                        Target = data.Target,
                                        TargetDetail = data.TargetDetail.GetHTML(),
                                        Conditions = data.Conditions.GetHTML()
                                    };
                                    resultData.Triggers.Add(trigger);
                                }
                            }
                        }
                    }
                }
            }
        }
        #endregion

        #region Standard Elements
        /// <summary>
        /// Hàm lấy dữ liệu vào đối tượng lưu trữ<br/>
        /// Dùng cho đối tượng chuẩn <see cref="HTMLStandardElement"/>
        /// </summary>
        /// <param name="element"></param>
        /// <param name="resultData"></param>
        private void CopyData(StandardElement element, HTMLStandardElement resultData)
        {
            resultData.Effects = new List<HTMLEffect>();
            string urlImage = CheckElementSupport(element);

            var softE = element.Effects.FirstOrDefault(x => x is SoftEdgeEffect);

            if (softE != null)
            {
                var perspec = element.Effects.FirstOrDefault(x => x is ShadowEffect);
                var reflect = element.Effects.FirstOrDefault(x => x is ReflectionEffect);
                var softEData = GetEffectData(softE as SoftEdgeEffect, urlImage);
                if (softEData != null)
                    resultData.Effects.Add(softEData);
                if (reflect != null)
                {
                    var reflectData = GetEffectData(reflect);
                    if (reflectData != null)
                        resultData.Effects.Add(GetEffectData(reflect));
                }


                if (perspec != null && (perspec as ShadowEffect).Type == ShadowType.Perspective)
                {
                    var perspecData = GetEffectData(perspec);
                    if (perspecData != null)
                        resultData.Effects.Add(perspecData);
                }

            }
            else
            {
                foreach (var effect in element.Effects)
                {
                    var item = GetEffectData(effect);
                    if (item != null)
                        resultData.Effects.Add(item);
                }
            }
            if (resultData.Effects == null || resultData.Effects.Count() <= 0)
            {
                resultData.Effects = null;
            }
            resultData.Shapes = GetHTMLShape(element);
        }

        #endregion

        #region Text
        /// <summary>
        /// Hàm lấy dữ liệu của khung văn bản
        /// </summary>
        /// <param name="textEditor"></param>
        /// <param name="resultData"></param>
        private void CopyDataTextElement(RichTextEditor richTextEditor, HTMLTextElement resultData)
        {
            richTextEditor.TextContainer.RefreshUI();


            if (richTextEditor.TextContainer.Document.IsDisplayTitle)
            {
                List<HTMLInline> inlines = new List<HTMLInline>();

                inlines.Add(new HTMLInline() { Text = "" });
                resultData.Paragraphs = new List<HTMLParagraph>();
                resultData.Paragraphs.Add(new HTMLParagraph() { Inlines = inlines });

            }

            if (CheckReference(richTextEditor, HTMLReferenceText.GetListReference()))
            {
                #region Xử lý ReferenceText
                HTMLReference refe = new HTMLReference();
                double Top = 0;
                if (richTextEditor.TextContainer?.Words?.Count > 0)
                {
                    Top = richTextEditor.TextContainer.Words[0].Top - richTextEditor.TextContainer.Words[0].MaxHeight;
                }
                refe.ReferenceText = HTMLReferenceText.SetHtmlReferenText(richTextEditor);
                refe.Top = Top;
                resultData.Reference = refe;

                #endregion
            }
            else
            {

                #region Paragraph

                INV.Elearning.Text.Html.HtmlDocument getHtmlDocumment = INV.Elearning.Text.Helper.HtmlHelper.GetHtmlDocument(richTextEditor);
                List<HTMLParagraph> paragraphs = new List<HTMLParagraph>();
                int _countListWord = 0;
                double _leftInline = 0;
                double _topInline = 0;
                double left = 0;
                int index = 0;
                for (int i = 0; i < 1; i++)
                {
                    foreach (var paragraph in getHtmlDocumment.Blocks)
                    {
                        INV.Elearning.Text.ViewModels.Text.Paragraph para = new INV.Elearning.Text.ViewModels.Text.Paragraph();
                        List<HTMLInline> inlines = new List<HTMLInline>();
                        HTMLParagraph paragraphText = new HTMLParagraph();
                        paragraphText.Inlines = new List<HTMLInline>();
                        foreach (var inline in (paragraph as INV.Elearning.Text.Html.HtmlParagraph).Inlines)
                        {
                            INV.Elearning.Text.Html.HtmlRun _run = null;
                            HTMLInline inlineText = null;
                            if (inline is INV.Elearning.Text.Html.HtmlRunHyperlink)
                            {
                                _run = inline as INV.Elearning.Text.Html.HtmlRunHyperlink;
                                inlineText = new HTMLInlineHyperlink();
                            }
                            else
                            {
                                _run = inline as INV.Elearning.Text.Html.HtmlRun;
                                inlineText = new HTMLInline();
                            }



                            //Text Properties (match)
                            inlineText.FontSize = _run.ScriptOffset != 0 ? _run.FontSize * richTextEditor.TextContainer.Document.CoefficientSizeChange : _run.FontSize * 96 / 72 * richTextEditor.TextContainer.Document.CoefficientSizeChange;
                            inlineText.ScripOffsets = _run.ScriptOffset;
                            inlineText.FontFamily = _run._fontFamily;
                            inlineText.FontWeight = _run.FontWeight.ToString().ToLower();
                            inlineText.FontStyle = _run.FontStyle.ToString().ToLower();
                            inlineText.Color = GetColor(_run.Foreground);
                            inlineText.Underline = "None";

                            if (inline.Underline != null && inline.Underline.Style != UnderlineStyle.None)
                            {
                                inlineText.Underline = inline.Underline.Style.ToString();
                                if (inline.Underline.Color != null)
                                    inlineText.ColorUnderline = GetColor(inline.Underline.Color);
                                else
                                {
                                    inlineText.ColorUnderline = GetColor(_run.Foreground);
                                }
                            }
                            inlineText.StrikeThrough = inline.StrikeThrough.ToString();
                            if (inlineText is HTMLInlineHyperlink && _run is HtmlRunHyperlink)
                            {
                                if ((_run as HtmlRunHyperlink).Address != "" && ((_run as HtmlRunHyperlink).Address?.IndexOf("http://") != 0 && (_run as HtmlRunHyperlink).Address.IndexOf("https://") != 0))
                                {

                                    (inlineText as HTMLInlineHyperlink).Address = "https://" + (_run as HtmlRunHyperlink).Address;
                                }
                                else
                                {
                                    (inlineText as HTMLInlineHyperlink).Address = (_run as HtmlRunHyperlink).Address;
                                }
                            }
                            inlineText.OpacityText = 1;


                            string textInline = "";

                            _countListWord = (inline as INV.Elearning.Text.Html.HtmlRun).Words.Count;

                            if (_countListWord > 0)
                            {
                                // _topInline = (inline as INV.Elearning.Text.Html.HtmlRun).Words[0].Top -inline.FontSize * double.Parse(inline.ScriptOffset.ToString()) / 100;
                                _topInline = (inline as INV.Elearning.Text.Html.HtmlRun).Words[0].Top;
                                _leftInline = (inline as INV.Elearning.Text.Html.HtmlRun).Words[0].Left;
                                left = (inline as INV.Elearning.Text.Html.HtmlRun).Words[0].Left;
                            }
                            index = 0;
                            for (int j = 0; j < _countListWord; j++)
                            {

                                if (inline.CharSpacing != 0)
                                {
                                    if (j > 0 && (inline as INV.Elearning.Text.Html.HtmlRun).Words[j].Top > (inline as INV.Elearning.Text.Html.HtmlRun).Words[j - 1].Top)
                                    {
                                        HTMLInline inlineTextWord = null;
                                        if (_run is HtmlRunHyperlink)
                                        {
                                            inlineTextWord = new HTMLInlineHyperlink();
                                        }
                                        else
                                        {
                                            inlineTextWord = new HTMLInline();
                                        }

                                        inlineTextWord.FontSize = Convert.ToInt32((_run.ScriptOffset != 0 ? _run.FontSize * richTextEditor.TextContainer.Document.CoefficientSizeChange : _run.FontSize * 96 / 72 * richTextEditor.TextContainer.Document.CoefficientSizeChange) * 10) / 10.0;
                                        inlineText.ScripOffsets = _run.ScriptOffset;
                                        inlineTextWord.FontFamily = inlineText.FontFamily;
                                        inlineTextWord.FontWeight = inlineText.FontWeight.ToLower();
                                        inlineTextWord.FontStyle = inlineText.FontStyle.ToLower();
                                        inlineTextWord.Color = inlineText.Color;
                                        inlineTextWord.StrikeThrough = inlineText.StrikeThrough;
                                        inlineTextWord.OpacityText = 1;
                                        inlineTextWord.Underline = "None";
                                        inlineTextWord.Width = Convert.ToInt32(((inline as INV.Elearning.Text.Html.HtmlRun).Words[Math.Max(0, j - 1)].Left + (inline as INV.Elearning.Text.Html.HtmlRun).Words[Math.Max(0, j - 1)].Width - (inline as INV.Elearning.Text.Html.HtmlRun).Words[index].Left) * 10) / 10.0;
                                        if (inline.Underline != null && inline.Underline.Style != UnderlineStyle.None)
                                        {
                                            inlineTextWord.Underline = inline.Underline.Style.ToString();
                                            if (inline.Underline.Color != null)
                                                inlineTextWord.ColorUnderline = GetColor(inline.Underline.Color);
                                            else
                                            {
                                                inlineTextWord.ColorUnderline = GetColor(_run.Foreground);
                                            }

                                            //Xử lý tọa độ Underline
                                            if ((inline as INV.Elearning.Text.Html.HtmlRun)?.Words.Count > 0 && (inline as INV.Elearning.Text.Html.HtmlRun).Words[0].HtmlUnderline != null && (inline as INV.Elearning.Text.Html.HtmlRun).Words[(inline as INV.Elearning.Text.Html.HtmlRun).Words.Count - 1].HtmlUnderline != null)
                                            {
                                                inlineTextWord.HtmlUnderline = (inline as INV.Elearning.Text.Html.HtmlRun).Words[index].HtmlUnderline;

                                                inlineTextWord.HtmlUnderline.X2 = Convert.ToInt32(10 * (inline as INV.Elearning.Text.Html.HtmlRun).Words[Math.Max(0, j - 1)].HtmlUnderline.X2) / 10.0;

                                                inlineTextWord.HtmlUnderline.X1 = Convert.ToInt32(10 * inlineTextWord.HtmlUnderline.X1) / 10.0;
                                                inlineTextWord.HtmlUnderline.Y = Convert.ToInt32(10 * inlineTextWord.HtmlUnderline.Y) / 10.0;
                                            }
                                        }

                                        inlineTextWord.Text = textInline;

                                        inlineTextWord.Top = Convert.ToInt32(_topInline * 10) / 10.0;
                                        inlineTextWord.Lefts = new double[textInline.Length];
                                        if (inlineTextWord is HTMLInlineHyperlink)
                                        {
                                            if ((_run as HtmlRunHyperlink).Address != "" && ((_run as HtmlRunHyperlink).Address.IndexOf("http://") != 0 && (_run as HtmlRunHyperlink).Address.IndexOf("https://") != 0))
                                            {
                                                (inlineTextWord as HTMLInlineHyperlink).Address = "https://" + (_run as HtmlRunHyperlink).Address;
                                            }
                                            else
                                            {
                                                (inlineTextWord as HTMLInlineHyperlink).Address = (_run as HtmlRunHyperlink).Address;
                                            }

                                        }

                                        for (int h = 0; h < textInline.Length; h++)
                                        {
                                            //Get format of Word to get Width of it
                                            var format = new FormattedText(textInline[h].ToString(), CultureInfo.CurrentUICulture, System.Windows.FlowDirection.LeftToRight, new Typeface(new FontFamily(inlineText.FontFamily), TextConverter.FontStyleConverter(inlineText.FontStyle), TextConverter.FontWeightConverter(inlineText.FontWeight), FontStretches.Normal), _run.ScriptOffset != 0 ? _run.FontSize : _run.FontSize * 96 / 72, Brushes.Black);


                                            // inlineTextWord.Lefts[h] = Convert.ToInt32((format.WidthIncludingTrailingWhitespace + inline.CharSpacing*10))/10.0;
                                            inlineTextWord.Lefts[h] = Convert.ToInt32(left * 10) / 10.0;
                                            left += format.WidthIncludingTrailingWhitespace + inline.CharSpacing;

                                        }
                                        // inlineTextWord.Lefts = new double[] { _leftInline };
                                        //_topInline = (inline as INV.Elearning.Text.Html.HtmlRun).Words[j].Top - inline.FontSize * double.Parse(inline.ScriptOffset.ToString()) / 100; ;
                                        _topInline = (inline as INV.Elearning.Text.Html.HtmlRun).Words[j].Top;
                                        _leftInline = (inline as INV.Elearning.Text.Html.HtmlRun).Words[j].Left;
                                        left = (inline as INV.Elearning.Text.Html.HtmlRun).Words[j].Left;
                                        textInline = "";
                                        paragraphText.Inlines.Add(inlineTextWord);
                                        index = j;
                                    }

                                }
                                //Incase Spacing Text
                                else
                                {
                                    if (j > 0 && (inline as INV.Elearning.Text.Html.HtmlRun).Words[j].Top > (inline as INV.Elearning.Text.Html.HtmlRun).Words[j - 1].Top)
                                    {
                                        HTMLInline inlineTextWord = null;
                                        if (_run is HtmlRunHyperlink)
                                        {
                                            inlineTextWord = new HTMLInlineHyperlink();
                                        }
                                        else
                                        {
                                            inlineTextWord = new HTMLInline();
                                        }
                                        inlineTextWord.FontSize = Convert.ToInt32((_run.ScriptOffset != 0 ? _run.FontSize * richTextEditor.TextContainer.Document.CoefficientSizeChange : _run.FontSize * 96 / 72 * richTextEditor.TextContainer.Document.CoefficientSizeChange) * 10) / 10.0;
                                        inlineTextWord.ScripOffsets = _run.ScriptOffset;
                                        inlineTextWord.FontFamily = inlineText.FontFamily;
                                        inlineTextWord.FontWeight = inlineText.FontWeight.ToLower();
                                        inlineTextWord.FontStyle = inlineText.FontStyle.ToLower();
                                        inlineTextWord.Color = inlineText.Color;
                                        inlineTextWord.StrikeThrough = inlineText.StrikeThrough;
                                        inlineTextWord.OpacityText = 1;
                                        inlineTextWord.Underline = "None";
                                        // inlineTextWord.Width = (inline as INV.Elearning.Text.Html.HtmlRun).Words[Math.Max(0, j - 1)].Left + (inline as INV.Elearning.Text.Html.HtmlRun).Words[Math.Max(0, j - 1)].Width - (inline as INV.Elearning.Text.Html.HtmlRun).Words[index].Left;
                                        inlineTextWord.Width = Convert.ToInt32(((inline as INV.Elearning.Text.Html.HtmlRun).Words[Math.Max(0, j - 1)].Left + (inline as INV.Elearning.Text.Html.HtmlRun).Words[Math.Max(0, j - 1)].Width - (inline as INV.Elearning.Text.Html.HtmlRun).Words[index].Left) * 10) / 10.0;
                                        if (inline.Underline != null && inline.Underline.Style != UnderlineStyle.None)
                                        {
                                            inlineTextWord.Underline = inline.Underline.Style.ToString();
                                            if (inline.Underline.Color != null)
                                                inlineTextWord.ColorUnderline = GetColor(inline.Underline.Color);
                                            else
                                            {
                                                inlineTextWord.ColorUnderline = GetColor(_run.Foreground);
                                            }

                                            //Xử lý tọa độ Underline
                                            if ((inline as INV.Elearning.Text.Html.HtmlRun)?.Words.Count > 0 && (inline as INV.Elearning.Text.Html.HtmlRun).Words[0].HtmlUnderline != null && (inline as INV.Elearning.Text.Html.HtmlRun).Words[(inline as INV.Elearning.Text.Html.HtmlRun).Words.Count - 1].HtmlUnderline != null)
                                            {
                                                inlineTextWord.HtmlUnderline = (inline as INV.Elearning.Text.Html.HtmlRun).Words[index].HtmlUnderline;

                                                inlineTextWord.HtmlUnderline.X2 = Convert.ToInt32(10 * (inline as INV.Elearning.Text.Html.HtmlRun).Words[Math.Max(0, j - 1)].HtmlUnderline.X2) / 10.0;

                                                inlineTextWord.HtmlUnderline.X1 = Convert.ToInt32(10 * inlineTextWord.HtmlUnderline.X1) / 10.0;
                                                inlineTextWord.HtmlUnderline.Y = Convert.ToInt32(10 * inlineTextWord.HtmlUnderline.Y) / 10.0;
                                            }
                                        }

                                        inlineTextWord.Text = textInline;
                                        textInline = "";
                                        inlineTextWord.Top = Convert.ToInt32(_topInline * 10) / 10.0;
                                        inlineTextWord.Lefts = new double[] { Convert.ToInt32(_leftInline * 10) / 10.0 };
                                        // _topInline = (inline as INV.Elearning.Text.Html.HtmlRun).Words[j].Top - inline.FontSize * double.Parse(inline.ScriptOffset.ToString()) / 100; ;
                                        _topInline = (inline as INV.Elearning.Text.Html.HtmlRun).Words[j].Top;
                                        _leftInline = (inline as INV.Elearning.Text.Html.HtmlRun).Words[j].Left;

                                        if (inlineTextWord is HTMLInlineHyperlink)
                                        {
                                            if ((_run as HtmlRunHyperlink).Address != "" && ((_run as HtmlRunHyperlink).Address.IndexOf("http://") != 0 && (_run as HtmlRunHyperlink).Address.IndexOf("https://") != 0))
                                            {
                                                (inlineTextWord as HTMLInlineHyperlink).Address = "https://" + (_run as HtmlRunHyperlink).Address;
                                            }
                                            else
                                            {
                                                (inlineTextWord as HTMLInlineHyperlink).Address = (_run as HtmlRunHyperlink).Address;
                                            }
                                        }
                                        paragraphText.Inlines.Add(inlineTextWord);
                                        index = j;
                                    }
                                    //double left = word.Left;
                                    //for (int t = 0; t < word.Text.Length; t++)
                                    //{
                                    //    HTMLWord wordText = new HTMLWord();
                                    //    wordText.Text = word.Text[t].ToString();
                                    //    wordText.Left = left;
                                    //    wordText.Top = word.Top - inline.FontSize * double.Parse(inline.ScriptOffset.ToString()) / 100;
                                    //    wordText.TextWidth = word.Width;
                                    //    wordText.Height = word.Height;
                                    //    wordText.HtmlUnderline = word.HtmlUnderline;
                                    //    words.Add(wordText);

                                    //    //Get format of Word to get Width of it
                                    //    var format = new FormattedText(wordText.Text, CultureInfo.CurrentUICulture, System.Windows.FlowDirection.LeftToRight, new Typeface(new FontFamily(inlineText.FontFamily), TextConverter.FontStyleConverter(inlineText.FontStyle), TextConverter.FontWeightConverter(inlineText.FontWeight), FontStretches.Normal), _run.ScriptOffset != 0 ? _run.FontSize : _run.FontSize * 96 / 72, Brushes.Black);
                                    //    wordText.TextWidth = format.WidthIncludingTrailingWhitespace + inline.CharSpacing;
                                    //    left += format.WidthIncludingTrailingWhitespace + inline.CharSpacing;
                                    // }
                                }
                                textInline += (inline as INV.Elearning.Text.Html.HtmlRun).Words[j].Text;
                            }
                            #region Xử lý hàng cuối cùng
                            if (1 == 1)
                            {
                                if (inline.CharSpacing != 0)
                                {
                                    if (1 == 1)
                                    {
                                        HTMLInline inlineTextWord = null;
                                        if (_run is HtmlRunHyperlink)
                                        {
                                            inlineTextWord = new HTMLInlineHyperlink();
                                        }
                                        else
                                        {
                                            inlineTextWord = new HTMLInline();
                                        }

                                        inlineText.ScripOffsets = _run.ScriptOffset;
                                        inlineTextWord.FontSize = Convert.ToInt32((_run.ScriptOffset != 0 ? _run.FontSize * richTextEditor.TextContainer.Document.CoefficientSizeChange : _run.FontSize * 96 / 72 * richTextEditor.TextContainer.Document.CoefficientSizeChange) * 10) / 10.0;

                                        inlineTextWord.FontFamily = inlineText.FontFamily;
                                        inlineTextWord.FontWeight = inlineText.FontWeight.ToLower();
                                        inlineTextWord.FontStyle = inlineText.FontStyle.ToLower();
                                        inlineTextWord.Color = inlineText.Color;
                                        inlineTextWord.StrikeThrough = inlineText.StrikeThrough;
                                        inlineTextWord.OpacityText = 1;
                                        inlineTextWord.Underline = "None";
                                        inlineTextWord.Width = Convert.ToInt32(((inline as INV.Elearning.Text.Html.HtmlRun).Words[Math.Max(0, _countListWord - 1)].Left + (inline as INV.Elearning.Text.Html.HtmlRun).Words[Math.Max(0, _countListWord - 1)].Width - (inline as INV.Elearning.Text.Html.HtmlRun).Words[index].Left) * 10) / 10.0;
                                        if (inline.Underline != null && inline.Underline.Style != UnderlineStyle.None)
                                        {
                                            inlineTextWord.Underline = inline.Underline.Style.ToString();
                                            if (inline.Underline.Color != null)
                                                inlineTextWord.ColorUnderline = GetColor(inline.Underline.Color);
                                            else
                                            {
                                                inlineTextWord.ColorUnderline = GetColor(_run.Foreground);
                                            }

                                            //Xử lý tọa độ Underline
                                            if ((inline as INV.Elearning.Text.Html.HtmlRun)?.Words.Count > 0 && (inline as INV.Elearning.Text.Html.HtmlRun).Words[0].HtmlUnderline != null && (inline as INV.Elearning.Text.Html.HtmlRun).Words[(inline as INV.Elearning.Text.Html.HtmlRun).Words.Count - 1].HtmlUnderline != null)
                                            {
                                                inlineTextWord.HtmlUnderline = (inline as INV.Elearning.Text.Html.HtmlRun).Words[index].HtmlUnderline;

                                                inlineTextWord.HtmlUnderline.X2 = Convert.ToInt32(10 * (inline as INV.Elearning.Text.Html.HtmlRun).Words[Math.Max(0, _countListWord - 1)].HtmlUnderline.X2) / 10.0;

                                                inlineTextWord.HtmlUnderline.X1 = Convert.ToInt32(10 * inlineTextWord.HtmlUnderline.X1) / 10.0;
                                                inlineTextWord.HtmlUnderline.Y = Convert.ToInt32(10 * inlineTextWord.HtmlUnderline.Y) / 10.0;
                                            }
                                        }

                                        inlineTextWord.Width = (inline as INV.Elearning.Text.Html.HtmlRun).Words[Math.Max(0, _countListWord - 1)].Left + (inline as INV.Elearning.Text.Html.HtmlRun).Words[Math.Max(0, _countListWord - 1)].Width - (inline as INV.Elearning.Text.Html.HtmlRun).Words[index].Left;
                                        //textInline += (inline as INV.Elearning.Text.Html.HtmlRun).Words[Math.Max(0, _countListWord - 1)].Text;

                                        inlineTextWord.Text = textInline;

                                        inlineTextWord.Top = Convert.ToInt32(_topInline * 10) / 10.0;
                                        inlineTextWord.Lefts = new double[textInline.Length];
                                        if (inlineTextWord is HTMLInlineHyperlink)
                                        {
                                            if ((_run as HtmlRunHyperlink).Address != "" && ((_run as HtmlRunHyperlink).Address.IndexOf("http://") != 0 && (_run as HtmlRunHyperlink).Address.IndexOf("https://") != 0))
                                            {
                                                (inlineTextWord as HTMLInlineHyperlink).Address = "https://" + (_run as HtmlRunHyperlink).Address;
                                            }
                                            else
                                            {
                                                (inlineTextWord as HTMLInlineHyperlink).Address = (_run as HtmlRunHyperlink).Address;
                                            }
                                        }
                                        for (int h = 0; h < textInline.Length; h++)
                                        {
                                            //Get format of Word to get Width of it
                                            var format = new FormattedText(textInline[h].ToString(), CultureInfo.CurrentUICulture, System.Windows.FlowDirection.LeftToRight, new Typeface(new FontFamily(inlineText.FontFamily), TextConverter.FontStyleConverter(inlineText.FontStyle), TextConverter.FontWeightConverter(inlineText.FontWeight), FontStretches.Normal), _run.ScriptOffset != 0 ? _run.FontSize : _run.FontSize * 96 / 72, Brushes.Black);


                                            // inlineTextWord.Lefts[h] = Convert.ToInt32((format.WidthIncludingTrailingWhitespace + inline.CharSpacing*10))/10.0;
                                            inlineTextWord.Lefts[h] = Convert.ToInt32(left * 10) / 10.0;
                                            left += format.WidthIncludingTrailingWhitespace + inline.CharSpacing;

                                        }
                                        textInline = "";
                                        // inlineTextWord.Lefts = new double[] { _leftInline };

                                        paragraphText.Inlines.Add(inlineTextWord);

                                    }

                                }
                                //Incase Spacing Text
                                else
                                {
                                    if (1 == 1)
                                    {
                                        HTMLInline inlineTextWord = null;
                                        if (_run is HtmlRunHyperlink)
                                        {
                                            inlineTextWord = new HTMLInlineHyperlink();
                                        }
                                        else
                                        {
                                            inlineTextWord = new HTMLInline();
                                        }

                                        inlineTextWord.FontSize = Convert.ToInt32(inlineText.FontSize * 10) / 10.0;
                                        inlineText.ScripOffsets = _run.ScriptOffset;
                                        inlineTextWord.FontFamily = inlineText.FontFamily;
                                        inlineTextWord.FontWeight = inlineText.FontWeight.ToLower();
                                        inlineTextWord.FontStyle = inlineText.FontStyle.ToLower();
                                        inlineTextWord.Color = inlineText.Color;
                                        inlineTextWord.StrikeThrough = inlineText.StrikeThrough;
                                        inlineTextWord.OpacityText = 1;
                                        inlineTextWord.Underline = "None";
                                        inlineTextWord.Width = Convert.ToInt32(((inline as INV.Elearning.Text.Html.HtmlRun).Words[Math.Max(0, _countListWord - 1)].Left + (inline as INV.Elearning.Text.Html.HtmlRun).Words[Math.Max(0, _countListWord - 1)].Width - (inline as INV.Elearning.Text.Html.HtmlRun).Words[index].Left) * 10) / 10.0;
                                        if (inline.Underline != null && inline.Underline.Style != UnderlineStyle.None)
                                        {
                                            inlineTextWord.Underline = inline.Underline.Style.ToString();
                                            if (inline.Underline.Color != null)
                                                inlineTextWord.ColorUnderline = GetColor(inline.Underline.Color);
                                            else
                                            {
                                                inlineTextWord.ColorUnderline = GetColor(_run.Foreground);
                                            }

                                            //Xử lý tọa độ Underline
                                            if ((inline as INV.Elearning.Text.Html.HtmlRun)?.Words.Count > 0 && (inline as INV.Elearning.Text.Html.HtmlRun).Words[0].HtmlUnderline != null && (inline as INV.Elearning.Text.Html.HtmlRun).Words[(inline as INV.Elearning.Text.Html.HtmlRun).Words.Count - 1].HtmlUnderline != null)
                                            {
                                                inlineTextWord.HtmlUnderline = (inline as INV.Elearning.Text.Html.HtmlRun).Words[index].HtmlUnderline;

                                                inlineTextWord.HtmlUnderline.X2 = Convert.ToInt32(10 * (inline as INV.Elearning.Text.Html.HtmlRun).Words[Math.Max(0, _countListWord - 1)].HtmlUnderline.X2) / 10.0;

                                                inlineTextWord.HtmlUnderline.X1 = Convert.ToInt32(10 * inlineTextWord.HtmlUnderline.X1) / 10.0;
                                                inlineTextWord.HtmlUnderline.Y = Convert.ToInt32(10 * inlineTextWord.HtmlUnderline.Y) / 10.0;
                                            }
                                        }

                                        inlineTextWord.Width = Convert.ToInt32(((inline as INV.Elearning.Text.Html.HtmlRun).Words[Math.Max(0, _countListWord - 1)].Left + (inline as INV.Elearning.Text.Html.HtmlRun).Words[Math.Max(0, _countListWord - 1)].Width - (inline as INV.Elearning.Text.Html.HtmlRun).Words[index].Left) * 10) / 10.0;
                                        //  textInline += (inline as INV.Elearning.Text.Html.HtmlRun).Words[_countListWord-1].Text;

                                        inlineTextWord.Text = textInline;
                                        textInline = "";
                                        inlineTextWord.Top = Convert.ToInt32(_topInline * 10) / 10.0;
                                        inlineTextWord.Lefts = new double[] { Convert.ToInt32(_leftInline * 10) / 10.0 };
                                        if (inlineTextWord is HTMLInlineHyperlink)
                                        {
                                            if ((_run as HtmlRunHyperlink).Address != "" && ((_run as HtmlRunHyperlink).Address.IndexOf("http://") != 0 && (_run as HtmlRunHyperlink).Address.IndexOf("https://") != 0))
                                            {
                                                (inlineTextWord as HTMLInlineHyperlink).Address = "https://" + (_run as HtmlRunHyperlink).Address;
                                            }
                                            else
                                            {
                                                (inlineTextWord as HTMLInlineHyperlink).Address = (_run as HtmlRunHyperlink).Address;
                                            }
                                        }
                                        paragraphText.Inlines.Add(inlineTextWord);

                                    }

                                }
                            }
                            #endregion
                        }

                        if (richTextEditor?.TextContainer?.Document != null)
                            paragraphText.CoefficientSizeChange = richTextEditor.TextContainer.Document.CoefficientSizeChange;
                        //  paragraphText.Inlines = inlines;
                        paragraphText.CoefficientSizeChange = richTextEditor.TextContainer.Document.CoefficientSizeChange;
                        paragraphs.Add(paragraphText);
                    }
                }
                resultData.Paragraphs = paragraphs;
                #endregion
            }






        }
        private bool CheckReference(RichTextEditor richTextEditor, List<Reference> list)
        {
            if (richTextEditor != null && list.Count > 0)
            {
                TextRange textRange = new TextRange();
                var _listPara = textRange.GetParagraphs(richTextEditor.TextContainer.Document.Blocks);
                int _countPara = _listPara.Count;
                string text = "";
                for (int i = 0; i < _countPara; i++)
                {
                    foreach (var inline in _listPara[i].Inlines)
                    {
                        text += (inline as Run)?.Text;
                    }
                    text += "&";
                }
                foreach (var item in list)
                {
                    if (text.IndexOf("%" + item.Key + "%") >= 0) return true;
                }
            }
            return false;
        }
        #endregion


        #region Timing
        /// <summary>
        /// Lấy thời gian trình chiếu
        /// </summary>
        /// <param name="timing"></param>
        /// <returns></returns>
        private HTMLTiming GetHTMLTimingData(Core.Timeline.Timing timing)
        {
            if (timing != null)
            {
                return new HTMLTiming() { StartTime = timing.StartTime, Duration = timing.Duration };
            }
            return null;
        }
        #endregion

        #region Transition
        private HTMLTransition CopyTransitionData(Core.View.SlideBase slide)
        {
            if (slide.Transition != null)
            {
                var transition = new HTMLTransition();
                transition.HTMLDurationTransition = slide.Transition.Duration.TotalSeconds;
                transition.Name = slide.Transition.Name;
                if (slide.Transition.EffectOptions?.Count > 0)
                {
                    for (int i = 0; i < slide.Transition.EffectOptions[0].Values.Count(); i++)
                    {
                        if (slide.Transition.EffectOptions[0].Values[i].IsSelected)
                        {
                            transition.EffectOption = slide.Transition.EffectOptions[0].Values[i].Name;
                            return transition;
                        }
                    }
                }

                return transition;
            }
            return null;
        }

        #endregion

        #region Amimations

        /// <summary>
        /// Lấy thông tin hiệu ứng
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        private List<HTMLAnimationBase> GetHTMLAnimationData(ObjectElement element)
        {
            List<HTMLAnimationBase> listAnimation = new List<HTMLAnimationBase>();


            HTMLTiming timing = GetHTMLTimingData(element.Timing);

            if (timing != null)
            {
                double startTime = timing.StartTime, duration = timing.Duration;

                if (element.EntranceAnimation != null && element.EntranceAnimation is NoneAnimation)
                {
                    element.EntranceAnimation.Duration = TimeSpan.FromSeconds(0);
                }
                if (element.ExitAnimation != null && element.ExitAnimation is NoneAnimation)
                {
                    element.ExitAnimation.Duration = TimeSpan.FromSeconds(0);
                }

                var animationEntrance = element.EntranceAnimation;
                var animationExit = element.ExitAnimation;

                double durationAnimation = animationEntrance.Duration.TotalSeconds + animationExit.Duration.TotalSeconds;

                //Lấy giá trị EntranceAnimation
                var htmlEntrance = new HTMLEntranceAnimation
                {
                    HTMLSumDurationBeforeAnimation = startTime,
                    HTMLDurationAnimation = duration < durationAnimation ? Math.Min(duration / 2, animationEntrance.Duration.TotalSeconds) : animationEntrance.Duration.TotalSeconds,
                    Name = animationEntrance.Name,
                };

                if (animationEntrance.Name?.ToUpper() == "FLYIN")
                {
                    htmlEntrance.Name = "Fly";
                }
                if (animationEntrance.Name?.ToUpper() == "FLOATIN")
                {
                    htmlEntrance.Name = "Float";
                }
                if (animationEntrance.GetType() != typeof(NoneAnimation))
                {
                    htmlEntrance.HTMLEffectOptionGroupAnimation = new List<HTMLEffectOptionGroupAnimation>();
                }
                HTMLEffectOptionGroupAnimation HTMLEffectOptionGroup = new HTMLEffectOptionGroupAnimation();
                if (animationEntrance.EffectOptions != null)
                    foreach (var EffectOption in animationEntrance.EffectOptions)
                    {
                        HTMLEffectOptionGroup = new HTMLEffectOptionGroupAnimation();
                        CopyEffectOptionGroup(HTMLEffectOptionGroup, EffectOption as IEffectOptionGroup);
                        htmlEntrance.HTMLEffectOptionGroupAnimation?.Add(HTMLEffectOptionGroup);
                    }

                //Lấy List tọa độ Top của từng đoạn văn bản
                if (element is TextEditor)
                {
                    double _lineHeight;// Hệ số  khoảng cách các dòng trong đoạn
                    double _valueLineHeight;
                    htmlEntrance.listTopPara = new List<double>();
                    int _countWord = (element as TextEditor).RichTextEditor.TextContainer.Words.Count();
                    for (int i = 0; i < _countWord; i++)
                    {
                        if ((element as TextEditor).RichTextEditor.TextContainer.Words[i] is WordEnter)
                        {
                            Paragraph para = (((element as TextEditor).RichTextEditor.TextContainer.Words[i] as WordEnter).Parent as Run).Parent as Paragraph;
                            _lineHeight = para.LineHeight != 0 ? para.LineHeight : 1;// Hệ số  khoảng cách các dòng trong đoạn
                            _valueLineHeight = INV.Elearning.Text.ViewModels.Text.Document.DIPVALUELINEHEIGHT;
                            htmlEntrance.listTopPara.Add(Convert.ToInt32(((element as TextEditor).RichTextEditor.TextContainer.Words[i].Top + ((element as TextEditor).RichTextEditor.TextContainer.Words[i].MaxHeight * _lineHeight + _valueLineHeight) * INV.Elearning.Text.ViewModels.Text.Document.DIPLineHeight + para.Padding.Bottom + para.Margin.Bottom) * 10) / 10.0);
                        }
                    }
                    if (htmlEntrance.listTopPara.Count > 0)
                    {
                        htmlEntrance.listTopPara[htmlEntrance.listTopPara.Count - 1] = Convert.ToInt32(element.Height * 10) / 10.0;
                    }
                }
                //Lấy giá trị ExitAnimation

                var htmlExit = new HTMLExitAnimation
                {
                    HTMLDurationAnimation = duration < durationAnimation ? Math.Min(duration / 2, animationExit.Duration.TotalSeconds) : animationExit.Duration.TotalSeconds
                };
                htmlExit.HTMLSumDurationBeforeAnimation = startTime + duration - htmlExit.HTMLDurationAnimation;
                htmlExit.Name = animationExit.Name;
                if (animationExit.Name?.ToUpper() == "FLYOUT")
                {
                    htmlExit.Name = "Fly";
                }
                if (animationExit.Name?.ToUpper() == "FLOATOUT")
                {
                    htmlExit.Name = "Float";
                }
                if (animationExit.GetType() != typeof(NoneAnimation))
                {
                    htmlExit.HTMLEffectOptionGroupAnimation = new List<HTMLEffectOptionGroupAnimation>();
                }
                HTMLEffectOptionGroup = new HTMLEffectOptionGroupAnimation();
                if (animationExit.EffectOptions != null)
                    foreach (var EffectOption in animationExit.EffectOptions)
                    {
                        HTMLEffectOptionGroup = new HTMLEffectOptionGroupAnimation();
                        CopyEffectOptionGroup(HTMLEffectOptionGroup, EffectOption as IEffectOptionGroup);
                        htmlExit.HTMLEffectOptionGroupAnimation?.Add(HTMLEffectOptionGroup);
                    }

                //Lấy List tọa độ Top của từng đoạn văn bản
                if (element is TextEditor)
                {
                    double _lineHeight;// Hệ số  khoảng cách các dòng trong đoạn
                    double _valueLineHeight;
                    htmlExit.listTopPara = new List<double>();
                    int _countWord = (element as TextEditor).RichTextEditor.TextContainer.Words.Count();
                    for (int i = 0; i < _countWord; i++)
                    {
                        if ((element as TextEditor).RichTextEditor.TextContainer.Words[i] is WordEnter)
                        {
                            Paragraph para = (((element as TextEditor).RichTextEditor.TextContainer.Words[i] as WordEnter).Parent as Run).Parent as Paragraph;
                            _lineHeight = para.LineHeight != 0 ? para.LineHeight : 1;// Hệ số  khoảng cách các dòng trong đoạn
                            _valueLineHeight = INV.Elearning.Text.ViewModels.Text.Document.DIPVALUELINEHEIGHT;
                            htmlExit.listTopPara.Add(Convert.ToInt32(((element as TextEditor).RichTextEditor.TextContainer.Words[i].Top + ((element as TextEditor).RichTextEditor.TextContainer.Words[i].MaxHeight * _lineHeight + _valueLineHeight) * INV.Elearning.Text.ViewModels.Text.Document.DIPLineHeight + para.Padding.Bottom + para.Margin.Bottom) * 10) / 10.0);
                        }
                    }
                    if (htmlExit.listTopPara.Count > 0)
                    {
                        htmlExit.listTopPara[htmlExit.listTopPara.Count - 1] = Convert.ToInt32(element.Height * 10) / 10.0;
                    }
                }
                //Lấy giá trị Motion Path
                var animationMotionPath = element.MotionPaths;
                List<HTMLMotionPathAnimation> htmlMotionPaths = new List<HTMLMotionPathAnimation>();
                foreach (IMotionPathObject motionPathObject in animationMotionPath)
                {
                    if ((motionPathObject as ObjectElement)?.Content is ShapeBase path)
                    {
                        HTMLMotionPathAnimation htmlMotionPathAnimation = new HTMLMotionPathAnimation();
                        double
                            left = (motionPathObject as ObjectElement).Left,
                            top = (motionPathObject as ObjectElement).Top,
                            width = (motionPathObject as ObjectElement).Width,
                            height = (motionPathObject as ObjectElement).Height;
                        // var animationExit = element.ExitAnimation;
                        htmlMotionPathAnimation.Id = (motionPathObject as ObjectElement)?.ID;
                        htmlMotionPathAnimation.Rect = new Rect(left, top, width, height);
                        htmlMotionPathAnimation.Angle = (motionPathObject as ObjectElement).Angle;
                        htmlMotionPathAnimation.HTMLSumDurationBeforeAnimation = startTime;
                        htmlMotionPathAnimation.HTMLDurationAnimation = Math.Min(motionPathObject.Animation.Duration.TotalSeconds, duration);
                        htmlMotionPathAnimation.Name = motionPathObject.Animation.Name;
                        htmlMotionPathAnimation.HTMLEffectOptionGroupAnimation = new List<HTMLEffectOptionGroupAnimation>();

                        HTMLEffectOptionGroup = new HTMLEffectOptionGroupAnimation();
                        bool isReversed = GetIsSelectedPathEffectOption(motionPathObject.Animation, "Reverse Path Direction");
                        if (motionPathObject.Animation.EffectOptions != null)
                            foreach (var EffectOption in motionPathObject.Animation.EffectOptions)
                            {
                                HTMLEffectOptionGroup = new HTMLEffectOptionGroupAnimation();
                                CopyEffectOptionGroup(HTMLEffectOptionGroup, EffectOption as IEffectOptionGroup);
                                htmlMotionPathAnimation.HTMLEffectOptionGroupAnimation.Add(HTMLEffectOptionGroup);
                            }

                        if (path.PathGeometry != null)
                        {
                            double
                                scaleX = (motionPathObject as ObjectElement).IsScaleX ? -1 : 1,
                                scaleY = (motionPathObject as ObjectElement).IsScaleY ? -1 : 1,
                                centerX = width / 2,
                                centerY = height / 2,
                                reversed = isReversed ? -1 : 1,
                                angle = (motionPathObject as ObjectElement).Angle;
                            TransformGroup transform = new TransformGroup();
                            transform.Children.Add(new ScaleTransform(scaleX, scaleY)
                            {
                                CenterX = centerX,
                                CenterY = centerY,
                            });
                            //transform.Children.Add(new ScaleTransform(reversed, reversed)
                            //{
                            //    CenterX = centerX,
                            //    CenterY = centerY,
                            //});
                            //transform.Children.Add(new RotateTransform(angle)
                            //{
                            //    CenterX = centerX,
                            //    CenterY = centerY,
                            //});

                            PathGeometry pathGeometry = transform.Transform(path.PathGeometry);
                            htmlMotionPathAnimation.Data = pathGeometry.ToString(CultureInfo.InvariantCulture);
                        }
                        //List<Point> points = GeometryHelpers.GetPointCollection(path.PathGeometry);
                        //htmlMotionPathAnimation.Points = new List<HTMLPoint>();
                        //foreach (Point point in points)
                        //{
                        //    htmlMotionPathAnimation.Points.Add(new HTMLPoint(point.X, point.Y));
                        //}
                        //htmlMotionPathAnimation.Type=
                        htmlMotionPaths.Add(htmlMotionPathAnimation);
                    }
                }
                listAnimation.Add(htmlEntrance);
                listAnimation.Add(htmlExit);
                listAnimation.AddRange(htmlMotionPaths);
            }



            return listAnimation;
        }

        private Point GetTranslateVector(ObjectElement motionPathObject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Sao chép cài đặt hiệu ứng
        /// </summary>
        /// <param name="HTMLEffectOption"></param>
        /// <param name="effectOption"></param>
        private void CopyEffectOption(HTMLEffectOptionAnimation HTMLEffectOption, IEffectOption effectOption)
        {
            HTMLEffectOption.Name = effectOption.Name;
        }

        /// <summary>
        /// Sao chép cài đặt nhóm hiệu ứng
        /// </summary>
        /// <param name="HTMLEffectOptionGroup"></param>
        /// <param name="EffectOptionGroup"></param>
        private void CopyEffectOptionGroup(HTMLEffectOptionGroupAnimation HTMLEffectOptionGroup, IEffectOptionGroup EffectOptionGroup)
        {
            HTMLEffectOptionGroup.GroupName = EffectOptionGroup.GroupName;
            HTMLEffectOptionAnimation HTMLEffectOption = new HTMLEffectOptionAnimation();
            // HTMLEffectOptionGroup.HTMLEffectOptionAnimation = new List<HTMLEffectOptionAnimation>();

            foreach (var item in EffectOptionGroup.Values)
            {

                HTMLEffectOption = new HTMLEffectOptionAnimation();

                if (item is EffectOptionAdapter)
                {
                    foreach (var effectoption in (item as EffectOptionAdapter).Values)
                    {
                        if (effectoption.IsSelected)
                        {
                            HTMLEffectOption = new HTMLEffectOptionAnimation();
                            CopyEffectOption(HTMLEffectOption, effectoption);
                            HTMLEffectOptionGroup.Name = HTMLEffectOption.Name;
                            //HTMLEffectOptionGroup.HTMLEffectOptionAnimation.Add(HTMLEffectOption);
                        }

                    }
                }
                else if (item is IEffectOption)
                {
                    if (item.IsSelected)
                    {
                        CopyEffectOption(HTMLEffectOption, item);
                        HTMLEffectOptionGroup.Name = HTMLEffectOption.Name;
                        //HTMLEffectOptionGroup.HTMLEffectOptionAnimation.Add(HTMLEffectOption);
                    }

                }

            }
        }

        /// <summary>
        /// Sao chép danh sách cài đặt nhóm hiệu ứng
        /// </summary>
        /// <param name="listHTMLEffectOptionGroup"></param>
        /// <param name="listEffectOptionGroup"></param>
        private void CopyListEffectOptionGroup(List<HTMLEffectOptionGroupAnimation> listHTMLEffectOptionGroup, List<IEffectOptionGroup> listEffectOptionGroup)
        {
            listHTMLEffectOptionGroup = new List<HTMLEffectOptionGroupAnimation>();
            HTMLEffectOptionGroupAnimation HTMLEffectOptionGroup = new HTMLEffectOptionGroupAnimation();
            foreach (var item in listEffectOptionGroup)
            {
                HTMLEffectOptionGroup = new HTMLEffectOptionGroupAnimation();

                CopyEffectOptionGroup(HTMLEffectOptionGroup, item as IEffectOptionGroup);
                listHTMLEffectOptionGroup.Add(HTMLEffectOptionGroup);
            }
        }
        /// <summary>
        /// Lấy giá trị IsSelected hiệu ứng đường Path
        /// </summary>
        /// <param name="animation"></param>
        /// <param name="NameEffectOption"></param>
        /// <returns></returns>
        public static bool GetIsSelectedPathEffectOption(IAnimation animation, string NameEffectOption)
        {
            if (animation?.EffectOptions != null)
                foreach (var item in animation.EffectOptions)
                {
                    if (item.GroupName == "Path")
                        if (item.Values[0] is EffectOption)
                        {
                            foreach (var item2 in item.Values)
                            {
                                if (item2.Name == NameEffectOption)
                                {
                                    return item2.IsSelected;
                                }
                            }
                        }
                }

            return false;
        }

        #endregion

        #region Effects

        /// <summary>
        /// Lấy thông tin các hiệu ứng
        /// </summary>
        /// <param name="effect"></param>
        /// <returns></returns>
        private HTMLEffect GetEffectData(FrameEffectBase effect)
        {
            //if (effect is SoftEdgeEffect softE)
            //    return GetEffectData(softE, urlImage);

            if (effect is GlowEffect glow)
                return GetEffectData(glow);

            if (effect is ShadowEffect shadow)
                return GetEffectData(shadow);

            if (effect is ReflectionEffect refl)
                return GetEffectData(refl);

            return null;
        }

        /// <summary>
        /// Lấy dữ liệu cho hiệu ứng tỏa sáng
        /// </summary>
        /// <param name="effect"></param>
        /// <returns></returns>
        private HTMLGlowEffect GetEffectData(GlowEffect effect)
        {
            if (effect.Color == null)
                return null;
            return new HTMLGlowEffect()
            {
                Color = (GetColor(new SolidColorBrush() { Color = (Color)ColorConverter.ConvertFromString(effect.Color) }) as HTMLSolid).Color,
                Size = 100,
                StdDeviation = effect.Size,
                Type = HTMLEffect.HTMLEffectType.Glow
            };
        }

        /// <summary>
        /// Lấy dữ liệu cho hiệu ứng đổ bóng
        /// </summary>
        /// <param name="effect"></param>
        /// <returns></returns>
        private HTMLShadowEffect GetEffectData(ShadowEffect effect)
        {
            if (effect.Color == null)
                return null;
            return new HTMLShadowEffect()
            {
                Color = effect.Color,
                Size = 100,
                Angle = effect.Angle,
                Blur = effect.Blur,
                Distance = effect.Distance,
                Opacity = 1.0 - effect.Transparency,
                ShadowType = GetShadowType(effect.Type),
                PerspectiveShadowType = effect.PerspectiveShadowType,
                DataInner = effect.DataInner,
                Type = HTMLEffect.HTMLEffectType.Shadow
            };
        }

        /// <summary>
        /// Lấy kiểu đổ bóng
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private HTMLShadowEffect.HTMLShadowType GetShadowType(ShadowType type)
        {
            switch (type)
            {
                case ShadowType.Outer:
                    return HTMLShadowEffect.HTMLShadowType.Outer;
                case ShadowType.Inner:
                    return HTMLShadowEffect.HTMLShadowType.Inner;
                case ShadowType.Perspective:
                    return HTMLShadowEffect.HTMLShadowType.Perspective;
            }
            return HTMLShadowEffect.HTMLShadowType.Outer;
        }

        /// <summary>
        /// Lấy kiểu phản chiếu
        /// </summary>
        /// <param name="effect"></param>
        /// <returns></returns>
        private HTMLReflectionEffect GetEffectData(ReflectionEffect effect)
        {
            if (effect.Size == 0)
                return null;
            return new HTMLReflectionEffect()
            {
                Blur = effect.Blur,
                Distance = effect.Distance,
                Opacity = effect.Transparency,
                Size = effect.Size,
                Type = HTMLEffect.HTMLEffectType.Reflection
            };
        }

        /// <summary>
        /// Lấy kiểu SoftEdge
        /// </summary>
        /// <param name="effect"></param>
        /// <returns></returns>
        private HTMLSoftEdgeEffect GetEffectData(SoftEdgeEffect effect, string urlImage)
        {
            if (effect.Size == 0)
                return null;
            return new HTMLSoftEdgeEffect()
            {
                SoftUrl = urlImage.Replace(RootFolder + "\\", ""),
                Type = HTMLEffect.HTMLEffectType.SoftEdge
            };
        }
        #endregion

        #region CheckElement
        /// <summary>
        /// Kiểm tra các tồn tại các xử lý chưa thể hỗ trợ<br/>
        /// Sẽ xuất các đối tượng này thành hình ảnh
        /// </summary>
        /// <param name="element"></param>
        /// <returns>Url Image</returns>
        private string CheckElementSupport(StandardElement element)
        {
            if (element.Effects?.FirstOrDefault(x => x is SoftEdgeEffect) != null) //Nếu là hiệu ứng làm mềm, hiện tại chưa xử lý được=> xuất ra ảnh
            {
                var result = new HTMLImageElement();
                string imageUrl = Path.Combine(ImageFolder, $"s{imageCount++}.png");
                element.SaveToImage(imageUrl);
                //result.ImageUrl = imageUrl;
                return imageUrl;
            }

            return string.Empty;
        }
        #endregion

        #region Player Config

        public void GetPlayerConfigData()
        {
            MainDataContextViewModel dataSetting;
            if ((Application.Current as IAppGlobal).PlayerDataSetting as MainDataContextViewModel == null)
            {
                dataSetting = new MainDataContextViewModel();
            }
            else
            {
                dataSetting = (Application.Current as IAppGlobal).PlayerDataSetting as MainDataContextViewModel;
            }

            #region CSS

            string cssTemp = HTMLFileHelper.GetValueFromFile("HTMLTemplate/HTML5/css/style.stg");
            Template tempCss = new Template(cssTemp, '$', '$');
            HTMLColorPlayer colorPlayer = new HTMLColorPlayer();
            HTMLFeaturesPlayer featuresPlayer = new HTMLFeaturesPlayer();
            HTMLBaseSize baseSize = new HTMLBaseSize();

            #region Màu sắc
            var playerKeyColor = dataSetting.BaseColorViewModel.SelectedScheme.TemplateColor;

            //Main
            colorPlayer.MainPageBackground = playerKeyColor.PageBackgroundMain.KeyColor;
            colorPlayer.MainPlayerBackground = playerKeyColor.PlayerBackgroundMain.KeyColor;

            // Header và Bottom
            colorPlayer.BarBackground = playerKeyColor.BackgroundTopBottomBar.KeyColor;
            colorPlayer.BarText = playerKeyColor.TextTopBottomBar.KeyColor;

            // Nút bấm và tab
            colorPlayer.ButtonBackground = playerKeyColor.BackgroundButtonInactiveTab.KeyColor;
            colorPlayer.ButtonText = playerKeyColor.TextIconButtonInactiveTab.KeyColor;
            colorPlayer.ButtonHoverBackground = playerKeyColor.HoverbackgroundButtonInactiveTab.KeyColor;
            colorPlayer.ButtonHoverText = playerKeyColor.HovertexticonButtonInactiveTab.KeyColor;

            //Thanh bên
            colorPlayer.SidebarBackground = playerKeyColor.BackgroundSidebarPopups.KeyColor;
            colorPlayer.SidebarText = playerKeyColor.TextSidebarPopups.KeyColor;
            colorPlayer.SidebarHoverItemBackground = playerKeyColor.HoveritembackgroundSidebarPopups.KeyColor;
            colorPlayer.SidebarHoverItemText = playerKeyColor.HoveritemtextSidebarPopups.KeyColor;
            colorPlayer.SidebarSelectedItemBackground = playerKeyColor.SelecteditembackgroundSidebarPopups.KeyColor;
            colorPlayer.SidebarSelectedItemText = playerKeyColor.SelecteditemtextSidebarPopups.KeyColor;
            colorPlayer.SidebarVisitedItemText = playerKeyColor.VisiteditemtextSidebarPopups.KeyColor;
            colorPlayer.SidebarHyperlink = playerKeyColor.HyperlinkSidebarPopups.KeyColor;

            //Thanh điều khiển slide
            colorPlayer.SeekbarBackground = playerKeyColor.BackgroundSeekbarVolumeControl.KeyColor;
            colorPlayer.SeekbarPlayback = playerKeyColor.PlaybackSeekbarVolumeControl.KeyColor;

            tempCss.Add("color", colorPlayer);
            #endregion

            string fileNameCSS = "style.css";
            //Render CSS
            HTMLFileHelper.SetValueToFile(
               CssFolder,
               System.IO.Path.GetFileNameWithoutExtension(fileNameCSS),
               System.IO.Path.GetExtension(fileNameCSS),
               tempCss.Render()
               );
            #endregion
        }

        #endregion
        #region Question
        /// <summary>
        /// Tạo đối tượng câu hỏi nêu slide là câu hỏi
        /// </summary>
        /// <param name="questionType"></param>
        /// <returns></returns>
        public HTMLSlideBase CreatHTMLQuestion(QuestionType questionType)
        {
            switch (questionType)
            {
                case QuestionType.TrueFalseGraded:
                    return new HTMLTrueFalseQuestion() { TypeQuestion = QuestionType.TrueFalseGraded };
                case QuestionType.MultipleChoiceGraded:
                    return new HTMLMultiChoiceQuestion() { TypeQuestion = QuestionType.MultipleChoiceGraded };
                case QuestionType.MultipleResponseGraded:
                    return new HTMLMultiResponceQuestion() { TypeQuestion = QuestionType.MultipleResponseGraded };
                case QuestionType.TextInputGraded:
                    return new HTMLTextInputQuestion() { TypeQuestion = QuestionType.TextInputGraded };
                case QuestionType.FillTheBlank:
                    return new HTMLFillTheBlankQuestion() { TypeQuestion = QuestionType.FillTheBlank };
                case QuestionType.WordBankGraded:
                    return new HTMLWordBankQuestion() { TypeQuestion = QuestionType.WordBankGraded };
                case QuestionType.MatchingDrapDropGraded:
                    return new HTMLMatchingDrapDropQuestion() { TypeQuestion = QuestionType.MatchingDrapDropGraded };
                case QuestionType.MatchingDropDownGraded:
                    return new HTMLMatchingDropDownQuestion() { TypeQuestion = QuestionType.MatchingDropDownGraded };
                case QuestionType.SequenceDrapdropGraded:
                    return new HTMLSequenceDrapDropQuestion() { TypeQuestion = QuestionType.SequenceDrapdropGraded };
                case QuestionType.SequenceDropDownGraded:
                    return new HTMLSequenceDropDownQuestion() { TypeQuestion = QuestionType.SequenceDropDownGraded };
                case QuestionType.NumericGraded:
                    return new HTMLNumericQuestion() { TypeQuestion = QuestionType.NumericGraded };
                case QuestionType.LinkertScaleSurvey:
                    return new HTMlLikertScaleQuestion() { TypeQuestion = QuestionType.LinkertScaleSurvey };
                case QuestionType.PickOneSurvey:
                    return new HTMLPickOneQuestion() { TypeQuestion = QuestionType.PickOneSurvey };
                case QuestionType.PickManySurvey:
                    return new HTMLPickManyQuestion() { TypeQuestion = QuestionType.PickManySurvey };
                case QuestionType.WhichWordSurvey:
                    return new HTMLWhichWordQuestion() { TypeQuestion = QuestionType.WhichWordSurvey };
                case QuestionType.ShortAnswerSurvey:
                    return new HTMLShortAnswerQuestion() { TypeQuestion = QuestionType.ShortAnswerSurvey };
                case QuestionType.EassaySurvey:
                    return new HTMLEssayQuestion() { TypeQuestion = QuestionType.EassaySurvey };
                case QuestionType.RankingDrapDropSurvey:
                    return new HTMLRankingDrapDropQuestion() { TypeQuestion = QuestionType.RankingDrapDropSurvey };
                case QuestionType.RankingDropDownSurvey:
                    return new HTMLRankingDropDownQuestion() { TypeQuestion = QuestionType.RankingDropDownSurvey };
                case QuestionType.HowManySurvey:
                    return new HTMLHowManyQuestion() { TypeQuestion = QuestionType.HowManySurvey };
                default:
                    return new HTMLQuestionBase();
            }
        }

        public HTMLAnswersContent GetAnswerContentQuestion(GroupAnswer groupAnswer)
        {
            HTMLAnswersContent answers = null;
            if (!(groupAnswer is WordBankGroup))
            {
                answers = new HTMLAnswersContent();
                answers.ID = groupAnswer.ID;
                answers.Answers = new List<HTMLAnswerBase>();
                answers.ContentKeyType = "Answers";
                answers.ZIndex = groupAnswer.ZIndex;
            }
            if (groupAnswer is MatchingDragGroup matchingDrapDropGroup)
            {
                var groupInfor = matchingDrapDropGroup.GetGroupInfor();
                answers.Top = groupInfor.Top; answers.Left = groupInfor.Left; //thông tin TLWH của group answer 
                answers.Width = groupInfor.Width; answers.Height = groupInfor.Height;

                foreach (MatchingControl answer in groupAnswer.ItemsSource)
                {
                    var answerInfor = answer.GetAnswerInfor();
                    var answerResult = new HTMLMatchingDrapDropAnswer() { Top = answerInfor.Top, Left = answerInfor.Left, Width = answerInfor.Width, Height = answerInfor.Height, Correct = answer.Correct, ID = answer.ID };

                    var choiceInfor = answer.GetInforLeftMatch();
                    var matchInfor = answer.GetInforRightMatch();

                    answerResult.Choice = new HTMLPosition() { Top = choiceInfor.Top, Left = choiceInfor.Left, Width = choiceInfor.Width, Height = choiceInfor.Height, ID = "choice" + ObjectElementsHelper.RandomString(7) };
                    answerResult.Match = new HTMLPosition() { Top = matchInfor.Top, Left = matchInfor.Left, Width = matchInfor.Width, Height = matchInfor.Height, ID = "match" + ObjectElementsHelper.RandomString(7) };

                    answerResult.MatchColor = GetColor(answer.GetColor());
                    answerResult.ChoiceColor = GetColor(answer.GetColor());
                    var choiceTextInfor = answer.GetInforLeftText();
                    var choiceText = new HTMLTextElement();

                    CopyDataTextElement(answer.GetLeftRichText(), choiceText as HTMLTextElement);
                    //answerResult.ChoiceText.Reference = choiceText.Reference.ReferenceText;
                    answerResult.ChoiceText.Paragraphs = choiceText.Paragraphs;
                    answerResult.ChoiceText.Reference = choiceText.Reference;
                    answerResult.ChoiceText.Top = choiceTextInfor.Top;
                    answerResult.ChoiceText.Left = choiceTextInfor.Left;

                    var matchTextInfor = answer.GetInforRightText();
                    var matchText = new HTMLTextElement();
                    CopyDataTextElement(answer.GetRightRichText(), matchText);
                    answerResult.ChoicePath = answer.GetChoicePath().Replace("F1", "");
                    answerResult.matchPath = answer.GetMatchingPath().Replace("F1", ""); ;
                    answerResult.MatchText.Paragraphs = matchText.Paragraphs;
                    answerResult.MatchText.Reference = matchText.Reference;
                    answerResult.MatchText.Top = matchTextInfor.Top;
                    answerResult.MatchText.Left = matchTextInfor.Left;
                    CopyData(answer, (answerResult as HTMLStandardElement));
                    if (!string.IsNullOrEmpty(answer.ChoiceImage?.UriSource?.LocalPath))
                    {
                        var choiceImg = answer.GetChoiceImage();
                        answerResult.ChoiceImage = new HTMLAnswerImage() { Width = choiceImg.Width, Height = choiceImg.Height, Top = choiceImg.Top, Left = choiceImg.Left, Source = CopyImageToExportFolder(answer.ChoiceImage.UriSource.LocalPath.ToString()).Replace(RootFolder + "\\", "") };
                    }
                    if (!string.IsNullOrEmpty(answer.MatchImage?.UriSource?.LocalPath))
                    {
                        var matchImg = answer.GetMatchImage();
                        answerResult.MatchImage = new HTMLAnswerImage() { Width = matchImg.Width, Height = matchImg.Height, Top = matchImg.Top, Left = matchImg.Left, Source = CopyImageToExportFolder(answer.MatchImage.UriSource.LocalPath.ToString()).Replace(RootFolder + "\\", "") };
                    }
                    answers.Answers.Add(answerResult);
                }
                return answers;
            }
            else if (groupAnswer is LikertScaleSurveyGroup likertgroup)
            {
                var groupInfor = likertgroup.GetGroupInfor();
                answers.Top = groupInfor.Top; answers.Left = groupInfor.Left; //thông tin TLWH của group answer 
                answers.Width = groupInfor.Width; answers.Height = groupInfor.Height;

                foreach (LikertScaleSurveyControl control in likertgroup.ItemsSource)
                {
                    var answerInfor = control.GetAnswerInfor();
                    var answerResult = new HTMLLikertScaleAnswer() { Top = answerInfor.Top, Left = answerInfor.Left, Width = answerInfor.Width, Height = answerInfor.Height, Correct = control.Correct, ID = control.ID };
                    var choiceInfor = control.GetTextInfor();
                    answerResult.Choice = new HTMLPosition() { Top = choiceInfor.Top, Left = choiceInfor.Left, Width = choiceInfor.Width, Height = choiceInfor.Height, ID = ObjectElementsHelper.RandomString(13) };

                    var choiceText = new HTMLTextElement();
                    CopyDataTextElement(control.GetRichText(), choiceText);
                    answerResult.ChoiceText.Paragraphs = choiceText.Paragraphs;
                    answerResult.ChoiceText.Reference = choiceText.Reference;
                    answerResult.ChoiceText.Top = choiceInfor.Top;
                    answerResult.ChoiceText.Left = choiceInfor.Left;
                    answerResult.Scales = new List<HTMLScale>();
                    var likertsInfor = control.GetLikertInfor();
                    answerResult.LikertsContent = new HTMLPosition() { Top = likertsInfor.Top, Width = likertsInfor.Width, Left = likertsInfor.Left, Height = likertsInfor.Height };
                    for (int i = 0; i < control.ListLikertScale.Count; i++)
                    {
                        var scale = new HTMLScale();
                        scale.Text = (control.ListLikertScale[i] as LikertScale).Scale;
                        var likertInfor = control.GetLikertScaleControl();
                        var _HTMLRdButton = new HTMLRadioButton()
                        {
                            TopRadioButton = 0,
                            LeftRadioButton = i * 60,
                            RadiusCircleOut = likertInfor.Radius,
                            RadiusCircleCheck = likertInfor.CheckRadius,
                            ColorBorderOut = GetColor(likertInfor.RadioButtonBorderBrush),
                            ColorCircleChecked = GetColor(likertInfor.CheckFill),
                            WidthBorderOut = likertInfor.RadioButtonThickness,
                            RadiusCircleInside = (likertInfor.Radius - likertInfor.RadioButtonThickness),
                            ColorCircleInside = GetColor(likertInfor.RadioButtonBackground),
                        };
                        scale.Choice = _HTMLRdButton;
                        answerResult.Scales.Add(scale);
                    }
                    CopyData(control, answerResult);
                    answers.Answers.Add(answerResult);
                }
            }
            else if (groupAnswer is MatchingDropDown)
            {
                foreach (ComboBoxRichTextBox answer in groupAnswer.ItemsSource)
                {
                    var answerInfor = answer.GetAnswerInfor();
                    var answerResult = new HTMLMatchingDropDownAnswer() { Top = answerInfor.Top, Left = answerInfor.Left, Width = answerInfor.Width, Height = answerInfor.Height, Correct = answer.Correct, ID = answer.ID };
                    var choiceInfor = answer.GetChoiceTextInfor();
                    var matchInfor = answer.GetComboboxInfor();

                    var choice = new HTMLPosition() { Top = choiceInfor.Top, Left = choiceInfor.Left, Width = choiceInfor.Width, Height = choiceInfor.Height };
                    var match = new HTMLPosition() { Top = matchInfor.Top, Left = matchInfor.Left, Width = matchInfor.Width, Height = matchInfor.Height };
                    answerResult.Choice = choice;
                    answerResult.Combobox = match;

                    var choiceTextInfor = answer.GetChoiceTextInfor();
                    var choiceText = new HTMLTextElement();
                    CopyDataTextElement(answer.GetChoiceRichText(), choiceText as HTMLTextElement);
                    answerResult.ChoiceText.Reference = choiceText.Reference;
                    answerResult.ChoiceText.Paragraphs = choiceText.Paragraphs;
                    answerResult.ChoiceText.Top = choiceTextInfor.Top;
                    answerResult.ChoiceText.Left = choiceTextInfor.Left;

                    var matchTextInfor = answer.GetMatchTextInfor();
                    var matchText = new HTMLTextElement();
                    CopyDataTextElement(answer.GetMatchRichText(), matchText as HTMLTextElement); ;
                    answerResult.ComboboxText.Reference = matchText.Reference;
                    answerResult.ComboboxText.Paragraphs = matchText.Paragraphs;
                    answerResult.ComboboxText.Top = matchTextInfor.Top;
                    answerResult.ComboboxText.Left = matchTextInfor.Left;
                    CopyData(answer, answerResult);
                    answers.Answers.Add(answerResult);
                }
            }
            else if (groupAnswer is MultiChoiceGroup multiChoiceGroup)
            {
                var groupInfor = multiChoiceGroup.GetGroupInfor();
                answers.Top = groupInfor.Top; answers.Left = groupInfor.Left; //thông tin TLWH của group answer 
                answers.Width = groupInfor.Width; answers.Height = groupInfor.Height;

                foreach (MultiChoiceControl control in multiChoiceGroup.ItemsSource)
                {
                    var answerInfor = control.GetAnswerInfor();
                    var answerResult = new HTMLChoiceAnswer() { Top = answerInfor.Top, Left = answerInfor.Left, Width = answerInfor.Width, Height = answerInfor.Height, Correct = control.Correct, ID = control.ID };
                    var choiceInfor = control.GetTextInfor();
                    answerResult.Choice = new HTMLPosition() { Top = choiceInfor.Top, Left = choiceInfor.Left, Width = choiceInfor.Width, Height = choiceInfor.Height, ID = ObjectElementsHelper.RandomString(13) };


                    var choiceTextInfor = control.GetTextInfor();
                    var choiceText = new HTMLTextElement();
                    CopyDataTextElement(control.GetRichText(), choiceText);
                    answerResult.ChoiceText.Reference = choiceText.Reference;
                    answerResult.ChoiceText.Paragraphs = choiceText.Paragraphs;
                    answerResult.ChoiceText.Top = choiceTextInfor.Top;
                    answerResult.ChoiceText.Left = choiceTextInfor.Left;

                    // lấy thông tin radio button
                    var RdInfor = control.GetRDButtonInfor();
                    var RdButton = control.GetRadioButton();
                    answerResult.RadioButton = new HTMLRadioButton()
                    {
                        TopRadioButton = RdInfor.Top,
                        LeftRadioButton = RdInfor.Left,
                        RadiusCircleOut = RdButton.Radius,
                        RadiusCircleCheck = RdButton.CheckFillRadius,
                        ColorBorderOut = GetColor(RdButton.Stroke),
                        ColorCircleChecked = GetColor(RdButton.CheckFill),
                        WidthBorderOut = RdButton.ChoiceButtonThickness,
                        RadiusCircleInside = (RdButton.Radius - RdButton.ChoiceButtonThickness),
                        ColorCircleInside = GetColor(RdButton.Fill)
                    };
                    CopyData(control, answerResult);
                    answers.Answers.Add(answerResult);
                }
            }
            else if (groupAnswer is MultipleResponse multiResponce)
            {
                var groupInfor = multiResponce.GetGroupInfor();
                answers.Top = groupInfor.Top; answers.Left = groupInfor.Left; //thông tin TLWH của group answer 
                answers.Width = groupInfor.Width; answers.Height = groupInfor.Height;

                foreach (CheckBoxControl control in multiResponce.ItemsSource)
                {
                    var answerInfor = control.GetAnswerInfor();
                    var answerResult = new HTMLCheckBoxAnswer() { Top = answerInfor.Top, Left = answerInfor.Left, Width = answerInfor.Width, Height = answerInfor.Height, Correct = control.Correct, ID = control.ID };
                    var choiceInfor = control.GetTextInfor();
                    answerResult.Choice = new HTMLPosition() { Top = choiceInfor.Top, Left = choiceInfor.Left, Width = choiceInfor.Width, Height = choiceInfor.Height, ID = ObjectElementsHelper.RandomString(13) };


                    var choiceTextInfor = control.GetTextInfor();
                    var choiceText = new HTMLTextElement();
                    CopyDataTextElement(control.GetRichText(), choiceText);
                    answerResult.ChoiceText.Reference = choiceText.Reference;
                    answerResult.ChoiceText.Paragraphs = choiceText.Paragraphs;
                    answerResult.ChoiceText.Top = choiceTextInfor.Top;
                    answerResult.ChoiceText.Left = choiceTextInfor.Left;
                    ///Lấy thông tin checkbox
                    var checkBoxInfor = control.GetCheckBoxInfor();
                    var checkMark = control.GetCheckMark();
                    var checkBox = control.GetCheckBox();
                    answerResult.CheckBox = new HTMLCheckBox()
                    {
                        TopCheckBox = checkBoxInfor.Top + checkBox.WidthCheckbox / 4,
                        LeftCheckBox = checkBoxInfor.Left + checkBox.WidthCheckbox / 4,
                        WidthCheckBoxOut = checkBox.WidthCheckbox / 2,
                        WidthCheckBoxInside = (checkBox.WidthCheckbox / 2 - checkBox.BorderThicknessCheckbox.Top),
                        ColorCheckBoxInside = GetColor(checkBox.CheckboxBackGround),
                        ColorBorderOut = GetColor(checkBox.BorderBackgroundCheckbox),
                        WidthBorderOut = checkBox.BorderThicknessCheckbox.Top,
                        ColorPath = GetColor(checkBox.CheckMarkFill),
                        CornerRadius = checkBox.CornerRadiusCheckbox.TopLeft
                    };

                    answerResult.CheckBox.PathChecked = HTMLShapePathDataHelper.DefiningGeometry(checkMark.Data.ToString(), answerResult.CheckBox.WidthCheckBoxInside, answerResult.CheckBox.WidthCheckBoxInside);
                    CopyData(control, answerResult);
                    answers.Answers.Add(answerResult);
                }
            }
            else if (groupAnswer is SequenceDragDropGroup sequenceDrapGroup)
            {
                var groupInfor = sequenceDrapGroup.GetGroupInfor();
                answers.Top = groupInfor.Top; answers.Left = groupInfor.Left; //thông tin TLWH của group answer 
                answers.Width = groupInfor.Width; answers.Height = groupInfor.Height;

                foreach (BorderRichTextBoxControl control in sequenceDrapGroup.ItemsSource)
                {
                    var answerInfor = control.GetAnswerInfor();
                    var answerResult = new HTMLSequenceDrapDropAnswer() { Top = answerInfor.Top, Left = answerInfor.Left, Width = answerInfor.Width, Height = answerInfor.Height, Correct = control.Correct, ID = control.ID };
                    var choiceInfor = control.GetTextInfor();
                    answerResult.Choice = new HTMLPosition() { Top = choiceInfor.Top, Left = choiceInfor.Left, Width = choiceInfor.Width, Height = choiceInfor.Height, ID = ObjectElementsHelper.RandomString(13) };


                    var choiceTextInfor = control.GetTextInfor();
                    var choiceText = new HTMLTextElement();
                    CopyDataTextElement(control.GetRichText(), choiceText);
                    answerResult.ChoiceText.Reference = choiceText.Reference;
                    answerResult.ChoiceText.Paragraphs = choiceText.Paragraphs;
                    answerResult.ChoiceText.Top = choiceTextInfor.Top;
                    answerResult.ChoiceText.Left = choiceTextInfor.Left;
                    CopyData(control, answerResult);
                    answers.Answers.Add(answerResult);
                }
            }
            else if (groupAnswer is SequenceDropDownGroup sequenceDropDownControl)
            {
                var groupInfor = sequenceDropDownControl.GetGroupInfor();
                answers.Top = groupInfor.Top; answers.Left = groupInfor.Left; //thông tin TLWH của group answer 
                answers.Width = groupInfor.Width; answers.Height = groupInfor.Height;

                foreach (DropDownControl control in sequenceDropDownControl.ItemsSource)
                {
                    var answerInfor = control.GetAnswerInfor();
                    var answerResult = new HTMLSequenceDropDownAnswer() { Top = answerInfor.Top, Left = answerInfor.Left, Width = answerInfor.Width, Height = answerInfor.Height, Correct = control.Correct, ID = control.ID };
                    var choiceInfor = control.GetTextInfor();
                    answerResult.Choice = new HTMLPosition() { Top = choiceInfor.Top, Left = choiceInfor.Left, Width = choiceInfor.Width, Height = choiceInfor.Height, ID = ObjectElementsHelper.RandomString(13) };


                    var choiceTextInfor = control.GetTextInfor();
                    var choiceText = new HTMLTextElement();
                    CopyDataTextElement(control.GetRichText(), choiceText);
                    answerResult.ChoiceText.Reference = choiceText.Reference;
                    answerResult.ChoiceText.Paragraphs = choiceText.Paragraphs;
                    answerResult.ChoiceText.Top = choiceTextInfor.Top;
                    answerResult.ChoiceText.Left = choiceTextInfor.Left;
                    CopyData(control, answerResult);
                    answers.Answers.Add(answerResult);
                }
            }
            else if (groupAnswer is TrueFalseGroup trueFalseGroup)
            {
                var groupInfor = trueFalseGroup.GetGroupInfor();
                answers.Top = groupInfor.Top; answers.Left = groupInfor.Left; //thông tin TLWH của group answer 
                answers.Width = groupInfor.Width; answers.Height = groupInfor.Height;

                foreach (MultiChoiceControl control in trueFalseGroup.ItemsSource)
                {
                    var answerInfor = control.GetAnswerInfor();
                    var answerResult = new HTMLChoiceAnswer() { Top = answerInfor.Top, Left = answerInfor.Left, Width = answerInfor.Width, Height = answerInfor.Height, Correct = control.Correct, ID = control.ID };
                    var choiceInfor = control.GetTextInfor();
                    answerResult.Choice = new HTMLPosition() { Top = choiceInfor.Top, Left = choiceInfor.Left, Width = choiceInfor.Width, Height = choiceInfor.Height, ID = ObjectElementsHelper.RandomString(13) };


                    var choiceTextInfor = control.GetTextInfor();
                    var choiceText = new HTMLTextElement();
                    CopyDataTextElement(control.GetRichText(), choiceText);
                    answerResult.ChoiceText.Reference = choiceText.Reference;
                    answerResult.ChoiceText.Paragraphs = choiceText.Paragraphs;
                    answerResult.ChoiceText.Top = choiceTextInfor.Top;
                    answerResult.ChoiceText.Left = choiceTextInfor.Left;
                    // lấy thông tin radio button
                    var RdInfor = control.GetRDButtonInfor();
                    var RdButton = control.GetRadioButton();
                    answerResult.RadioButton = new HTMLRadioButton()
                    {
                        TopRadioButton = RdInfor.Top,
                        LeftRadioButton = RdInfor.Left,
                        RadiusCircleOut = RdButton.Radius,
                        RadiusCircleCheck = RdButton.CheckFillRadius,
                        ColorBorderOut = GetColor(RdButton.Stroke),
                        ColorCircleChecked = GetColor(RdButton.CheckFill),
                        WidthBorderOut = RdButton.ChoiceButtonThickness,
                        RadiusCircleInside = (RdButton.Radius - RdButton.ChoiceButtonThickness),
                        ColorCircleInside = GetColor(RdButton.Fill),
                    };
                    CopyData(control, answerResult);
                    answers.Answers.Add(answerResult);
                }
            }
            else if (groupAnswer is WordBankGroup wordBankGroup)
            {
                answers = new WordBankAnswers();
                answers.ZIndex = wordBankGroup.ZIndex;
                answers.ContentKeyType = "Answers";
                answers.Answers = new List<HTMLAnswerBase>();
                var parent = Find.FindParent<WordBankLayer>(wordBankGroup);
                if (parent != null)
                {
                    var wordBankInfor = parent.GetDropInfor();
                    (answers as WordBankAnswers).Wordbank = new HTMLWordBankContent() { Top = wordBankInfor.Top, Left = wordBankInfor.Left, Width = wordBankInfor.Width, Height = wordBankInfor.Height, ConerRadius = 30, ID = ObjectElementsHelper.RandomString(13) };
                    (answers as WordBankAnswers).Wordbank.ContentKeyType = "WordBankContent";
                }
                var groupInfor = wordBankGroup.GetGroupInfor();
                answers.Top = groupInfor.Top; answers.Left = groupInfor.Left; //thông tin TLWH của group answer 
                answers.Width = groupInfor.Width; answers.Height = groupInfor.Height;
                foreach (BorderRichTextBoxControl control in wordBankGroup.ItemsSource)
                {
                    var answerInfor = control.GetAnswerInfor();
                    var answerResult = new HTMLWordBankAnswer() { Top = answerInfor.Top, Left = answerInfor.Left, Width = answerInfor.Width, Height = answerInfor.Height, Correct = control.Correct, ConerRadius = 30, ID = control.ID };
                    var choiceInfor = control.GetTextInfor();
                    answerResult.Choice = new HTMLPosition() { Top = choiceInfor.Top, Left = choiceInfor.Left, Width = choiceInfor.Width, Height = choiceInfor.Height, ID = ObjectElementsHelper.RandomString(13) };
                    var choiceTextInfor = control.GetTextInfor();
                    var choiceText = new HTMLTextElement();
                    CopyDataTextElement(control.GetRichText(), choiceText);
                    answerResult.ChoiceText.Reference = choiceText.Reference;
                    answerResult.ChoiceText.Paragraphs = choiceText.Paragraphs;
                    answerResult.ChoiceText.Top = choiceTextInfor.Top;
                    answerResult.ChoiceText.Left = choiceTextInfor.Left;
                    CopyData(control, answerResult);
                    answers.Answers.Add(answerResult);
                }
            }
            else if (groupAnswer is TextInputGroup textInputGroup)
            {
                var inputInfor = textInputGroup.GetTextInfor();
                answers = new HTMLTextInputContent()
                {
                    Top = inputInfor.Top,
                    Left = inputInfor.Left,
                    Width = inputInfor.Width,
                    Height = inputInfor.Height,
                    ContentKeyType = "Answers",
                    ID = ObjectElementsHelper.RandomString(13),
                    Fill = GetColor(textInputGroup.Fill)
                };
                answers.ZIndex = textInputGroup.ZIndex;
                var inputText = new HTMLTextElement();
                CopyDataTextElement(textInputGroup.GetRichText(), inputText);
                (answers as HTMLTextInputContent).TextInput = new HTMLTextInput();
                GetTextInput(inputText, (answers as HTMLTextInputContent).TextInput, textInputGroup.GetRichText());
                if (string.IsNullOrEmpty((answers as HTMLTextInputContent).TextInput.Text))
                {
                    (answers as HTMLTextInputContent).TextInput.FontFamily = "Arial";
                    (answers as HTMLTextInputContent).TextInput.FontSize = 18;
                }
                answers.Answers = new List<HTMLAnswerBase>();

                foreach (AnswerBaseView control in textInputGroup.ItemsSource)
                {
                    HTMLTextInputAnswer answerResult = new HTMLTextInputAnswer() { Correct = control.Correct, ID = ObjectElementsHelper.RandomString(13) };
                    answerResult.Text = GetTextFromDocument(control.Choice);
                    CopyData(control, answerResult);
                    answers.Answers.Add(answerResult);
                }

            }
            else if (groupAnswer is EssayGroup essayGroup)
            {
                var inputInfor = essayGroup.GetTextInfor();
                answers = new HTMLTextInputContent() { Top = inputInfor.Top, Left = inputInfor.Left, Width = inputInfor.Width, Height = inputInfor.Height, ContentKeyType = "Answers", ID = ObjectElementsHelper.RandomString(13), Fill = GetColor(essayGroup.Fill) };
                answers.ZIndex = essayGroup.ZIndex;
                var inputText = new HTMLTextElement();
                CopyDataTextElement(essayGroup.GetRichText(), inputText);
                (answers as HTMLTextInputContent).TextInput = new HTMLTextInput();
                GetTextInput(inputText, (answers as HTMLTextInputContent).TextInput, essayGroup.GetRichText());
                if (string.IsNullOrEmpty((answers as HTMLTextInputContent).TextInput.Text))
                {
                    (answers as HTMLTextInputContent).TextInput.FontFamily = "Arial";
                    (answers as HTMLTextInputContent).TextInput.FontSize = 18;
                }
            }
            //else if (groupAnswer is FillTheBlankGroup fillGroup)
            //{
            //    var groupInfor = fillGroup.GetGroupInfor();
            //    answers.Top = groupInfor.Top; answers.Left = groupInfor.Left; //thông tin TLWH của group answer 
            //    answers.Width = groupInfor.Width; answers.Height = groupInfor.Height;

            //    foreach (FillTheBlankControl control in fillGroup.ItemsSource)
            //    {
            //        var answerInfor = control.GetAnswerInfor();
            //        HTMLFillTheBlankAnswer answerResult = new HTMLFillTheBlankAnswer() { Top = answerInfor.Top, Left = answerInfor.Left, Width = answerInfor.Width, Height = answerInfor.Height, Correct = control.Correct, ID = control.ID };
            //        answerResult.TextInput = new HTMLTextInput();
            //        var textHolder = new HTMLTextElement();
            //        CopyDataTextElement(control.GetRichText(), textHolder);
            //        GetTextInput(textHolder, answerResult.TextInput, control.GetRichText());
            //        var choiceInfor = control.GetTextInfor();
            //        var inputText = new HTMLTextElement();
            //        CopyDataTextElement(control.GetRichText(), inputText);
            //        answerResult.Text = GetTextFromDocument(control.Choice);
            //        answers.Answers.Add(answerResult);
            //    }
            //}
            CopyData(groupAnswer, answers);
            return answers;
        }
        /// <summary>
        /// Lấy string từ Text 
        /// </summary>
        /// <param name="HTMLText"></param>
        /// <returns></returns>
        public void GetTextInput(HTMLTextElement HTMLText, HTMLTextInput textInput, RichTextEditor richText)
        {
            textInput.Text = "";
            textInput.TextAlign = (richText.TextContainer.Document.Blocks[0] as Paragraph).TextAlign.ToString();
            textInput.TextVertical = richText.TextContainer.Document.VerticalAlign.ToString();
            //for (HTMLParagraph para in HTMLText.Paragraphs)
            for (int i = 0; i < HTMLText.Paragraphs.Count; i++)
            {
                if (i == 0)
                {
                    if (HTMLText.Paragraphs[0].Inlines.Count > 0)
                    {
                        var inline = HTMLText.Paragraphs[0].Inlines[0];
                        if (textInput.FontSize == 0) textInput.FontSize = inline.FontSize;
                        if (textInput.FontFamily == null) textInput.FontFamily = inline.FontFamily;
                        if (textInput.Foreground == null) textInput.Foreground = inline.Color;
                    }
                }
                foreach (HTMLInline inline in HTMLText.Paragraphs[i].Inlines)
                {
                    textInput.Text += inline.Text;
                }
                if (i != HTMLText.Paragraphs.Count - 1) textInput.Text += "\n";
            }

        }

        public string GetTextFromDocument(INV.Elearning.Text.ViewModels.Text.Document document)
        {
            if (document.IsDisplayTitle) return string.Empty;
            string text = "";
            foreach (Paragraph block in document.Blocks)
            {
                foreach (Run inline in block.Inlines)
                {
                    text += inline.Text;
                }
            }
            text = text.Replace("\r", " ");
            if ((text[text.Length - 1] == ' ')) text = text.Substring(0, text.Length - 1);
            return text;
        }
        #endregion
    }
}
