﻿using INV.Elearning.Charts.Model;
using INV.Elearning.Charts.Model.Data;
using INV.Elearning.Charts.Views;
using INV.Elearning.Core.View;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace INV.Elearning.HTMLHelper.HTMLData.Helpers
{
    public partial class HTMLDataHelper
    {
        #region Chart
        /// <summary>
        /// Chart Data 
        /// </summary>
        /// <param name="tchart"></param>
        /// <param name="resultData"></param>
        private void CopyDataChartElement(TChart tchart, HTMLChartElement resultData)
        {
            //Lấy tên loại biểu đồ, để cứng là combochart
            resultData.ID = tchart.ID;
            resultData.Type = "ComboChart";
            //lấy dữ liệu khung cavas ngoài
            resultData.ChartKey = tchart.ChartData.ChartKey;
            resultData.TemplateChart = tchart.TemplateChart;
            resultData.Opacity = tchart.Opacity;
            resultData.OpacityItem = tchart.SelectedChartItemStyle.OpacityChartItem;
            resultData.Top = tchart.Top;
            resultData.Left = tchart.Left;
            resultData.Width = tchart.Width;
            resultData.Height = tchart.Height;
            //resultData.Fill = GetColor(tchart.Fill);
            //resultData.StrokeThickness = tchart.Thickness;
            //resultData.Stroke = GetColor(tchart.Stroke);
            resultData.ContentKeyType = "Chart";
            resultData.Shapes = new ShapeClass.HTMLShape()
            {
                CapType = tchart.CapType,
                DashType = tchart.DashType,
                JoinType = tchart.JoinType,
                PathObjects = new List<ShapeClass.HTMLPathObject>()
                {
                    new ShapeClass.HTMLPathObject()
                    {
                        Data = "M " + 0 + " " + 0 + ", " + tchart.Width + ", " + 0 + " " + tchart.Width + ", " + tchart.Height + " " + 0 + ", " + tchart.Height +" Z",
                        Stroke = GetColor(tchart.Stroke),
                        Fill = GetColor(tchart.Fill),
                        StrokeThickness = tchart.Thickness,
                        IsFilled = true,
                        IsStroked = true,
                    },
                }
            };
     
            //lấy dữ liệu khung chart
            resultData.FrameChart = new HTMLFrameChart()
            {
                Name = "Frame Chart",
                Top = tchart.ChartItemsContainer.Top,
                Left = tchart.ChartItemsContainer.Left,
                Width = tchart.ChartItemsContainer.Width,
                Height = tchart.ChartItemsContainer.Height,
            };

            foreach (var category in tchart.ChartData.Categories)
            {
                var _htmlCategory = new HTMLTCategory()
                {
                    CategoryName = category.Name,
                    FontFamily = category.FontFamily.ToString(),
                    FontSize = category.FontSize,
                };
                int indexSeri = 1;
                foreach (var seri in category.Series)
                {
                    HTMLTSeriBase _htmlSeri;

                    if (seri.ChartKey != ChartType.PieChart)
                    {
                        _htmlSeri = new HTMLTSeriBase()
                        {
                            ID = indexSeri + "",
                            IDSeri = "seri" + Guid.NewGuid().ToString("N"),
                            ChartKey = seri.ChartKey.ToString(),
                            TemplateChart = seri.TemplateChart.ToString(),
                            Type = seri.ChartKey.ToString() + "_" + seri.TemplateChart.ToString(),
                            Name = seri.Name,
                            IsPrimaryAxis = seri.IsPrimaryAxis,
                            Value = seri.Value,
                            FontFamily = seri.FontFamily.ToString(),
                            FontSize = seri.FontSize,
                            Fill = GetColor(seri.Fill),
                            Stroke = GetColor(seri.Stroke),
                            CapType = seri.CapType,
                            DashType = seri.DashType,
                            JoinType = seri.JoinType,
                            StrokeThickness = seri.Thickness,
                        };
                    }
                    else
                    {

                        _htmlSeri = new HTMLTSeriBase()
                        {
                            ID = indexSeri + "",
                            IDSeri = "seri" + Guid.NewGuid().ToString("N"),
                            ChartKey = seri.ChartKey.ToString(),
                            TemplateChart = seri.TemplateChart.ToString(),
                            Type = seri.ChartKey.ToString() + "_" + seri.TemplateChart.ToString(),
                            Name = seri.Name,
                            IsPrimaryAxis = seri.IsPrimaryAxis,
                            Value = seri.Value,
                            FontFamily = seri.FontFamily.ToString(),
                            FontSize = seri.FontSize,
                            Fill = GetColor(seri.Fill),
                            Stroke = GetColor(seri.Stroke),
                            CapType = seri.CapType,
                            DashType = seri.DashType,
                            JoinType = seri.JoinType,
                            StrokeThickness = seri.Thickness,
                            ICircle = new Point((seri as TSeriSale).ICircle.X + tchart.ContainerLeft, (seri as TSeriSale).ICircle.Y + tchart.ContainerTop),
                            PointStart = new Point((seri as TSeriSale).PointStart.X + tchart.ContainerLeft, (seri as TSeriSale).PointStart.Y + tchart.ContainerTop),
                        };
                    }

                    _htmlCategory.Series.Add(_htmlSeri);
                    indexSeri++;
                }
                resultData.Categories.Add(_htmlCategory);
            }

            //Lấy dữ liệu Chart Title
            if (tchart.IsChartTitleKey)
            {
                var ftTitle = new FormattedText(tchart.ChartTitleContainer.Text, CultureInfo.CurrentUICulture, FlowDirection.RightToLeft,
                       new Typeface(tchart.ChartTitleContainer.FontFamily.ToString()), tchart.ChartTitleContainer.FontSize, Brushes.Black);
                resultData.DataChartTitle = new HTMLDataChartTitle()
                {
                    ID = "title" + Guid.NewGuid().ToString("N"),
                    Text = tchart.ChartTitleContainer.Text,
                    Top = tchart.ChartTitleContainer.Top + tchart.ChartTitleContainer.PaddingTitle + ftTitle.Height,
                    Left = tchart.ContainerChartTitleLeft,
                    TopRect = tchart.ChartTitleContainer.Top,
                    LeftRect = tchart.ChartTitleContainer.Left,
                    FontFamily = tchart.ChartTitleContainer.FontFamily.ToString(),
                    FontSize = tchart.ChartTitleContainer.FontSize,
                    Width = tchart.ContainerChartTitleWidth,
                    Height = tchart.ContainerChartTitleHeight,
                    Fill = GetColor(tchart.ChartTitleContainer.Fill),
                    Stroke = GetColor(tchart.ChartTitleContainer.Stroke),
                    ForegroundText = GetColor(tchart.ChartTitleContainer.ForegroundText),
                    StrokeThickness = tchart.ChartTitleContainer.Thickness,
                    CapType = tchart.ChartTitleContainer.CapType,
                    JoinType = tchart.ChartTitleContainer.JoinType,
                    DashType = tchart.ChartTitleContainer.DashType,
                };
            }

            if (!(tchart is TPieChart))
            {
                resultData.DataAxes = new HTMLTDataAxes();
                //Lấy dữ liệu primary horizontal
                if (tchart.IsPrimaryHorizontal)
                {
                    var primaryHorizontal = resultData.DataAxes.ListDataAxes[0];
                    var ftPrimaryHorizontal = new FormattedText("0", CultureInfo.CurrentUICulture, FlowDirection.LeftToRight,
                        new Typeface(tchart.LabelCategoryContainer.FontFamily.ToString()), tchart.LabelCategoryContainer.FontSize, Brushes.Black);
                    primaryHorizontal.ID = "prih" + Guid.NewGuid().ToString("N");
                    primaryHorizontal.Key = TypeOptions.PrimaryHorizontal.ToString();
                    primaryHorizontal.IsChecked = tchart.IsPrimaryHorizontal;
                    primaryHorizontal.Top = tchart.LabelCategoryContainer.Top;
                    primaryHorizontal.Left = tchart.LabelCategoryContainer.Left;
                    primaryHorizontal.HeightText = ftPrimaryHorizontal.Height;
                    primaryHorizontal.FontFamily = tchart.LabelCategoryContainer.FontFamily.ToString();
                    primaryHorizontal.FontSize = tchart.LabelCategoryContainer.FontSize;
                    primaryHorizontal.Width = tchart.LabelCategoryContainer.Width;
                    primaryHorizontal.Height = tchart.LabelCategoryContainer.Height;
                    primaryHorizontal.Fill = GetColor(tchart.LabelCategoryContainer.Fill);
                    primaryHorizontal.Stroke = GetColor(tchart.LabelCategoryContainer.Stroke);
                    primaryHorizontal.ForegroundText = GetColor(tchart.LabelCategoryContainer.ForegroundText);
                    primaryHorizontal.StrokeThickness = tchart.LabelCategoryContainer.Thickness;
                    primaryHorizontal.CapType = tchart.LabelCategoryContainer.CapType;
                    primaryHorizontal.JoinType = tchart.LabelCategoryContainer.JoinType;
                    primaryHorizontal.DashType = tchart.LabelCategoryContainer.DashType;

                    primaryHorizontal.ShapeBases = new List<HTMLShapeBase>();
                    foreach (var item in tchart.ChartData.Categories)
                    {
                        primaryHorizontal.ShapeBases.Add(new HTMLShapeBase()
                        {
                            Width = item.FormattedText.WidthIncludingTrailingWhitespace,
                            Height = item.FormattedText.Height,
                        });
                    }

                }
                //Lấy dữ liệu primary vertical
                if (tchart.IsPrimaryVertical)
                {
                    var primaryVertical = resultData.DataAxes.ListDataAxes[1];
                    var ftPrimaryVertical = new FormattedText("0", CultureInfo.CurrentUICulture, FlowDirection.LeftToRight,
                        new Typeface(tchart.VerticalItemContainer.FontFamily.ToString()), tchart.VerticalItemContainer.FontSize, Brushes.Black);
                    primaryVertical.ID = "priv" + Guid.NewGuid().ToString("N");
                    primaryVertical.Key = TypeOptions.PrimaryVertical.ToString();
                    primaryVertical.IsChecked = tchart.IsPrimaryVertical;
                    primaryVertical.Top = tchart.VerticalItemContainer.Top;
                    primaryVertical.Left = tchart.VerticalItemContainer.Left + tchart.VerticalItemContainer.Width - tchart.VerticalItemContainer.Padding.Right;
                    primaryVertical.HeightText = ftPrimaryVertical.Height;
                    primaryVertical.FontFamily = tchart.VerticalItemContainer.FontFamily.ToString();
                    primaryVertical.FontSize = tchart.VerticalItemContainer.FontSize;
                    primaryVertical.Width = tchart.VerticalItemContainer.Width;
                    primaryVertical.Height = tchart.VerticalItemContainer.Height;
                    primaryVertical.Fill = GetColor(tchart.VerticalItemContainer.Fill);
                    primaryVertical.Stroke = GetColor(tchart.VerticalItemContainer.Stroke);
                    primaryVertical.ForegroundText = GetColor(tchart.VerticalItemContainer.ForegroundText);
                    primaryVertical.StrokeThickness = tchart.VerticalItemContainer.Thickness;
                    primaryVertical.CapType = tchart.VerticalItemContainer.CapType;
                    primaryVertical.JoinType = tchart.VerticalItemContainer.JoinType;
                    primaryVertical.DashType = tchart.VerticalItemContainer.DashType;

                    primaryVertical.ShapeBases = GetListShapeBasesVertical(tchart.VerticalItemContainer);
                }
                //Lấy dữ liệu secondary vertical
                if (tchart.IsSecondaryVertical)
                {
                    var secondaryVertical = resultData.DataAxes.ListDataAxes[2];
                    var ftSecondaryVertical = new FormattedText("0", CultureInfo.CurrentUICulture, FlowDirection.LeftToRight,
                        new Typeface(tchart.VerticalSecondaryContainer.FontFamily.ToString()), tchart.VerticalSecondaryContainer.FontSize, Brushes.Black);
                    secondaryVertical.ID = "secv" + Guid.NewGuid().ToString("N");
                    secondaryVertical.Key = TypeOptions.SecondaryVertical.ToString();
                    secondaryVertical.IsChecked = tchart.IsSecondaryVertical;
                    secondaryVertical.Top = tchart.VerticalSecondaryContainer.Top;
                    secondaryVertical.Left = tchart.VerticalSecondaryContainer.Left + tchart.VerticalSecondaryContainer.Padding.Left;
                    secondaryVertical.FontFamily = tchart.VerticalSecondaryContainer.FontFamily.ToString();
                    secondaryVertical.FontSize = tchart.VerticalSecondaryContainer.FontSize;
                    secondaryVertical.HeightText = ftSecondaryVertical.Height;
                    secondaryVertical.Width = tchart.VerticalSecondaryContainer.Width;
                    secondaryVertical.Height = tchart.VerticalSecondaryContainer.Height;
                    secondaryVertical.Fill = GetColor(tchart.VerticalSecondaryContainer.Fill);
                    secondaryVertical.Stroke = GetColor(tchart.VerticalSecondaryContainer.Stroke);
                    secondaryVertical.ForegroundText = GetColor(tchart.VerticalItemContainer.ForegroundText);
                    secondaryVertical.StrokeThickness = tchart.VerticalSecondaryContainer.Thickness;
                    secondaryVertical.CapType = tchart.VerticalSecondaryContainer.CapType;
                    secondaryVertical.JoinType = tchart.VerticalSecondaryContainer.JoinType;
                    secondaryVertical.DashType = tchart.VerticalSecondaryContainer.DashType;
                    secondaryVertical.ShapeBases = GetListShapeBasesVertical(tchart.VerticalSecondaryContainer);
                }

                //Lấy dữ liệu data table
                if (tchart.IsDataTableKey)
                {
                    resultData.DataTable = new HTMLDataTable()
                    {
                        IsDataTableKey = tchart.IsDataTableKey,
                        IsWithLegendKey = tchart.IsWithLegendKey,
                        TableBorder = new TableBorder()
                        {
                            IsHorizontal = tchart.IsDataTableHorizontal,
                            IsVertical = tchart.IsDataTableVertical,
                            IsOutline = tchart.IsDataTableOutline,
                        },
                        DataTable = new DataTable()
                        {
                            ID = "tbl" + Guid.NewGuid().ToString("N"),
                            WidthLeftTable = tchart.TableContainer.WidthTableLeft,
                            WidthTableSeri = tchart.TableContainer.WidthTableSeri,
                            WidthTableLegend = tchart.TableContainer.WidthTableLegend,
                            FontFamily = tchart.TableContainer.FontFamily.ToString(),
                            FontSize = tchart.TableContainer.FontSize,
                            Fill = GetColor(tchart.TableContainer.Fill),
                            Stroke = GetColor(tchart.TableContainer.Stroke),
                            ForegroundText = GetColor(tchart.TableContainer.ForegroundText),
                            CapType = tchart.TableContainer.CapType,
                            DashType = tchart.TableContainer.DashType,
                            JoinType = tchart.TableContainer.JoinType,
                            StrokeThickness = tchart.TableContainer.Thickness,
                        },
                        HeightTable = new HeightTable()
                        {
                            HeightCategory = tchart.TableContainer.HeightCategory * 2,
                        }
                    };
                    var categoryTable = tchart.TableContainer.CategoriesDataTable.FirstOrDefault(x => x.Series.Count != 0);
                    var seriesTable = categoryTable.Series.OrderByDescending(x => x.ChartKey).ThenBy(x => (x.TemplateChart == TemplateChart.Clustered ? (int)x.TemplateChart : x.IDSeri)).ToList();
                    foreach (var item in seriesTable)
                    {
                        resultData.DataTable.HeightTable.Series.Add(new SeriTable()
                        {
                            ID = "rtbl" + Guid.NewGuid().ToString("N"),
                            Height = item.FormattedText.Height * 2,
                            HeightLegend = tchart.TableContainer.WidthLegend,
                            WidthLegend = tchart.TableContainer.WidthLegend,
                            Name = item.Name,
                        });
                    }
                    foreach (var categoryTab in tchart.TableContainer.CategoriesDataTable)
                    {
                        resultData.DataTable.DataTable.CateogriesShapBases.Add(new HTMLShapeBase()
                        {
                            Height = categoryTab.FormattedText.Height,
                            Width = categoryTab.FormattedText.WidthIncludingTrailingWhitespace,
                        });
                        
                        foreach (var seriTable in categoryTab.Series)
                        {
                            resultData.DataTable.DataTable.ValuesShapBases.Add(new HTMLShapeBase()
                            {
                                Height = seriTable.FormattedValue.Height,
                                Width = seriTable.FormattedValue.WidthIncludingTrailingWhitespace,
                            });
                        }
                    }
                }
                else
                {
                    resultData.DataTable = new HTMLDataTable()
                    {
                        IsDataTableKey = tchart.IsDataTableKey,
                    };
                }

                //lấy dữ liệu data gridline
                if (tchart.IsGridlineKey)
                {
                    resultData.DataGridline = new HTMLTDataGridline()
                    {
                        IsGridlineKey = tchart.IsGridlineKey,
                        IsPrimaryMajorHorizontal = tchart.IsPrimaryMajorHorizontal,
                        IsPrimaryMajorVertical = tchart.IsPrimaryMajorVertical,
                        IsPrimaryMinorHorizontal = tchart.IsPrimaryMinorHorizontal,
                        IsPrimaryMinorVertical = tchart.IsPrimaryMinorVertical,
                        StrokeThickness = tchart.GridlineContainer.Thickness,
                        CapType = tchart.GridlineContainer.CapType,
                        DashType = tchart.GridlineContainer.DashType,
                        JoinType = tchart.GridlineContainer.JoinType,
                        Stroke = GetColor(tchart.GridlineContainer.Stroke),
                        StrokeMinor = GetColor(tchart.GridlineContainer.StrokeMinor),

                    };
                }
                else
                {
                    resultData.DataGridline = new HTMLTDataGridline()
                    {
                        IsGridlineKey = tchart.IsGridlineKey,
                    };
                }

                var _chartDataXML = tchart.Data as TChartDataXML;

                //lấy dữ liệu data axistitle => chưa làm
                resultData.DataAxisTitle = new HTMLTDataAxisTitle()
                {
                    IsAxisTitleKey = tchart.IsAxisTitleKey,
                    IsAxisTitlePrimaryHorizontal = _chartDataXML.DataAxisTitle.IsAxisTitlePrimaryHorizontal,
                    IsAxisTitlePrimaryVertical = _chartDataXML.DataAxisTitle.IsAxisTitlePrimaryVertical,
                    IsAxisTitleSecondaryHorizontal = _chartDataXML.DataAxisTitle.IsAxisTitleSecondaryHorizontal,
                    IsAxisTitleSecondaryVertical = _chartDataXML.DataAxisTitle.IsAxisTitleSecondaryVertical,
                };

                //Lấy dữ liệu SeriOption
                var categorySeriOption = tchart.ChartData.Categories.FirstOrDefault(x => x.Series.Count != 0);
                resultData.ConfigSeriOption = new HTMLConfigSeriOption();
                var seriOption = categorySeriOption.Series.FirstOrDefault(x => x.IsPrimaryAxis && x.ChartKey == ChartType.ColumnChart);
                if (seriOption != null)
                {
                    resultData.ConfigSeriOption.PrimaryAxis = new HTMLSeriOption()
                    {
                        GapWidth = seriOption.GapWidth,
                        OverLap = seriOption.OverLap,
                    };
                }
                seriOption = categorySeriOption.Series.FirstOrDefault(x => !x.IsPrimaryAxis && x.ChartKey == ChartType.ColumnChart);
                if (seriOption != null)
                {
                    resultData.ConfigSeriOption.SecondaryAxis = new HTMLSeriOption()
                    {
                        GapWidth = seriOption.GapWidth,
                        OverLap = seriOption.OverLap,
                    };
                }
            }
            else
            {
                resultData.Type = "PieChart";
                resultData.ConfigSeriOption = new HTMLConfigSeriOption()
                {
                    HTMLSeriOptionPie = new HTMLSeriOptionPie()
                    {
                        AngleOfFirstSlice = (tchart as TPieChart).AngleOfFirstSlice,
                    }
                };
            }


            //lấy dữ liệu dât Legend
            if (tchart.IsLegendKey)
            {
                resultData.DataLegend = new HTMLTDataLegend()
                {
                    ID = "lgn" + Guid.NewGuid().ToString("N"),
                    Top = tchart.LegendContainer.Top,
                    Left = tchart.LegendContainer.Left,
                    FontFamily = tchart.LegendContainer.ToString(),
                    FontSize = tchart.LegendContainer.FontSize,
                    Width = tchart.LegendContainer.Width,
                    Height = tchart.LegendContainer.Height,
                    Fill = GetColor(tchart.LegendContainer.Fill),
                    Stroke = GetColor(tchart.LegendContainer.Stroke),
                    StrokeThickness = tchart.LegendContainer.Thickness,
                    CapType = tchart.LegendContainer.CapType,
                    JoinType = tchart.LegendContainer.JoinType,
                    DashType = tchart.LegendContainer.DashType,
                };
                resultData.DataLegend.IsLegendKey = tchart.IsLegendKey;
                foreach (var item in tchart.LegendContainer.ContainerLegends)
                {
                    resultData.DataLegend.Series.Add(new SeriLegend()
                    {
                        Shape = new ShapeLegend()
                        {
                            ID = "shape" + Guid.NewGuid().ToString("N"),
                            Top = item.Top,
                            Left = item.Left,
                            Fill = GetColor(item.Fill),
                            Stroke = GetColor(item.Stroke),
                            StrokeThickness = item.Thickness,
                            CapType = item.CapType,
                            DashType = item.DashType,
                            Height = item.Height,
                            Width = item.Width,
                            JoinType = item.JoinType,
                            Type = ShapeLegend.ConverterTypeShape(item.ChartKey),
                        },
                        Text = new TextLegend()
                        {
                            ID = "txt" + Guid.NewGuid().ToString("N"),
                            Top = item.TopText,
                            Left = item.LeftText,
                            Width = item.Width,
                            Height = item.Height,
                            Content = item.Name,
                            ForegroundText = GetColor(item.ForegroundText),
                            FontFamily = item.FontFamily,
                            FontSize = item.FontSize,
                        }
                    });
                }
            }

            //Data label
            
            resultData.DataLabel = new HTMLTDataLabel();
            foreach (var labelItem in tchart.LabelContainer?.ContainerLabels)
            {
                var ft = new FormattedText(labelItem.Value.ToString(), CultureInfo.CurrentUICulture, FlowDirection.LeftToRight,
                        new Typeface(labelItem.FontFamily.ToString()), labelItem.FontSize, labelItem.ForegroundText.Brush);
                if (labelItem.IsLabelKey)
                {
                    resultData.DataLabel.Labels.Add(new SeriLegend()
                    {
                        Text = new TextLegend()
                        {
                            ID = "txt" + Guid.NewGuid().ToString("N"),
                            //Top = labelItem.Top + ft.Height / 2,
                            Top = labelItem.Top + ft.Height / 2 + ft.Height,
                            Left = labelItem.Left + ft.WidthIncludingTrailingWhitespace / 2,
                            Content = labelItem.Value,
                            ForegroundText = GetColor(labelItem.ForegroundText),
                            FontFamily = labelItem.FontFamily.ToString(),
                            FontSize = labelItem.FontSize,
                        },
                        Shape = new ShapeLegend()
                        {
                            ID = "shape" + Guid.NewGuid().ToString("N"),
                            Top = labelItem.Top,
                            Left = labelItem.Left,
                            Width = labelItem.Width,
                            Height = labelItem.Height,
                            StrokeThickness = labelItem.Thickness,
                            CapType = labelItem.CapType,
                            DashType = labelItem.DashType,
                            JoinType = labelItem.JoinType,
                            Fill = GetColor(labelItem.Fill),
                            Stroke = GetColor(labelItem.Stroke),
                        }
                    });
                }
            }

        }


        public List<HTMLShapeBase> GetListShapeBasesVertical(TVerticalItem verticalItem)
        {
            List<HTMLShapeBase> shapeBases = new List<HTMLShapeBase>();
            double step = (verticalItem.MaxValue - verticalItem.MinValue) / verticalItem.NumOfRow;
            int coefficient = verticalItem.IsCheckPercent ? 100 : 1;
            string str = "";
            if (verticalItem.IsPercent)
                str = "% ";
            for (double i = verticalItem.MinValue; i <= verticalItem.MaxValue; i += step)
            {
                var ft = new FormattedText(str + (coefficient * i).ToString(), CultureInfo.CurrentUICulture, FlowDirection.RightToLeft,
                        new Typeface(verticalItem.FontFamily.ToString()), verticalItem.FontSize, verticalItem.ForegroundText.Brush);
                shapeBases.Add(new HTMLShapeBase()
                {
                    Width = ft.WidthIncludingTrailingWhitespace,
                    Height = verticalItem.FontSize,
                });
            }
            return shapeBases;
        }
        public List<HTMLShapeBase> GetListShapeBasesVertical(TVerticalSecondary verticalItem)
        {
            List<HTMLShapeBase> shapeBases = new List<HTMLShapeBase>();
            double step = (verticalItem.MaxValue - verticalItem.MinValue) / verticalItem.NumOfRow;
            int coefficient = verticalItem.IsCheckPercent ? 100 : 1;
            string str = "";
            if (verticalItem.IsPercent)
                str = "% ";
            for (double i = verticalItem.MinValue; i <= verticalItem.MaxValue; i += step)
            {
                var ft = new FormattedText(str + (coefficient * i).ToString(), CultureInfo.CurrentUICulture, FlowDirection.RightToLeft,
                        new Typeface(verticalItem.FontFamily.ToString()), verticalItem.FontSize, verticalItem.ForegroundText.Brush);
                shapeBases.Add(new HTMLShapeBase()
                {
                    Width = ft.WidthIncludingTrailingWhitespace,
                    Height = verticalItem.FontSize,
                });
            }
            return shapeBases;
        }

        #endregion

        //#region Chart
        ///// <summary>
        ///// Chart Data 
        ///// </summary>
        ///// <param name="tchart"></param>
        ///// <param name="resultData"></param>
        //private void CopyDataChartElement(TChart tchart, HTMLChartElement resultData, int c)
        //{
        //    //Lấy tên loại biểu đồ, để cứng là combochart
        //    resultData.ID = tchart.ID;
        //    resultData.Type = "ComboChart";
        //    //lấy dữ liệu khung cavas ngoài
        //    resultData.ChartKey = tchart.ChartData.ChartKey;
        //    resultData.TemplateChart = tchart.TemplateChart;
        //    resultData.Opacity = tchart.Opacity;
        //    resultData.OpacityItem = tchart.SelectedChartItemStyle.OpacityChartItem;
        //    resultData.Top = tchart.Top;
        //    resultData.Left = tchart.Left;
        //    resultData.Width = tchart.Width;
        //    resultData.Height = tchart.Height;
        //    resultData.Fill = GetColor(tchart.Fill);
        //    resultData.StrokeThickness = tchart.Thickness;
        //    resultData.Stroke = GetColor(tchart.Stroke);
        //    resultData.ContentKeyType = "Chart";

        //    //lấy dữ liệu khung chart
        //    resultData.FrameChart = new HTMLFrameChart()
        //    {
        //        Name = "Frame Chart",
        //        Top = tchart.ChartItemsContainer.Top,
        //        Left = tchart.ChartItemsContainer.Left,
        //        Width = tchart.ChartItemsContainer.Width,
        //        Height = tchart.ChartItemsContainer.Height,
        //    };

        //    foreach (var category in tchart.ChartData.Categories)
        //    {
        //        var _htmlCategory = new HTMLTCategory()
        //        {
        //            CategoryName = category.Name,
        //            FontFamily = category.FontFamily.ToString(),
        //            FontSize = category.FontSize,
        //        };
        //        int indexSeri = 1;
        //        foreach (var seri in category.Series)
        //        {
        //            HTMLTSeriBase _htmlSeri;

        //            if (seri.ChartKey != ChartType.PieChart)
        //            {
        //                _htmlSeri = new HTMLTSeriBase()
        //                {
        //                    ID = indexSeri + "",
        //                    IDSeri = "seri" + Guid.NewGuid().ToString("N"),
        //                    ChartKey = seri.ChartKey.ToString(),
        //                    TemplateChart = seri.TemplateChart.ToString(),
        //                    Type = seri.ChartKey.ToString() + "_" + seri.TemplateChart.ToString(),
        //                    Name = seri.Name,
        //                    IsPrimaryAxis = seri.IsPrimaryAxis,
        //                    Value = seri.Value,
        //                    FontFamily = seri.FontFamily.ToString(),
        //                    FontSize = seri.FontSize,
        //                    Fill = GetColor(seri.Fill),
        //                    Stroke = GetColor(seri.Stroke),
        //                    CapType = seri.CapType,
        //                    DashType = seri.DashType,
        //                    JoinType = seri.JoinType,
        //                    StrokeThickness = seri.Thickness,
        //                };
        //            }
        //            else
        //            {

        //                _htmlSeri = new HTMLTSeriBase()
        //                {
        //                    ID = indexSeri + "",
        //                    IDSeri = "seri" + Guid.NewGuid().ToString("N"),
        //                    ChartKey = seri.ChartKey.ToString(),
        //                    TemplateChart = seri.TemplateChart.ToString(),
        //                    Type = seri.ChartKey.ToString() + "_" + seri.TemplateChart.ToString(),
        //                    Name = seri.Name,
        //                    IsPrimaryAxis = seri.IsPrimaryAxis,
        //                    Value = seri.Value,
        //                    FontFamily = seri.FontFamily.ToString(),
        //                    FontSize = seri.FontSize,
        //                    Fill = GetColor(seri.Fill),
        //                    Stroke = GetColor(seri.Stroke),
        //                    CapType = seri.CapType,
        //                    DashType = seri.DashType,
        //                    JoinType = seri.JoinType,
        //                    StrokeThickness = seri.Thickness,
        //                    ICircle = new Point((seri as TSeriSale).ICircle.X + tchart.ContainerLeft, (seri as TSeriSale).ICircle.Y + tchart.ContainerTop),
        //                    PointStart = new Point((seri as TSeriSale).PointStart.X + tchart.ContainerLeft, (seri as TSeriSale).PointStart.Y + tchart.ContainerTop),
        //                };
        //            }

        //            _htmlCategory.Series.Add(_htmlSeri);
        //            indexSeri++;
        //        }
        //        resultData.Categories.Add(_htmlCategory);
        //    }

        //    //Lấy dữ liệu Chart Title
        //    if (tchart.IsChartTitleKey)
        //    {
        //        var ftTitle = new FormattedText(tchart.ChartTitleContainer.Text, CultureInfo.CurrentUICulture, FlowDirection.RightToLeft,
        //               new Typeface(tchart.ChartTitleContainer.FontFamily.ToString()), tchart.ChartTitleContainer.FontSize, Brushes.Black);
        //        resultData.DataChartTitle = new HTMLDataChartTitle()
        //        {
        //            ID = "title" + Guid.NewGuid().ToString("N"),
        //            Text = tchart.ChartTitleContainer.Text,
        //            //Top = tchart.ChartTitleContainer.Top + tchart.ChartTitleContainer.PaddingTitle + ftTitle.Height / 2,
        //            Top = tchart.ChartTitleContainer.Top + tchart.ChartTitleContainer.PaddingTitle + ftTitle.Height / 2 + ftTitle.Height,
        //            Left = tchart.ContainerChartTitleLeft,
        //            FontFamily = tchart.ContainerChartTitleFontFamily.ToString(),
        //            FontSize = tchart.ContainerChartTitleFontSize,
        //            Width = tchart.ContainerChartTitleWidth,
        //            Height = tchart.ContainerChartTitleHeight,
        //            Fill = GetColor(tchart.ChartTitleContainer.Fill),
        //            Stroke = GetColor(tchart.ChartTitleContainer.Stroke),
        //            ForegroundText = GetColor(tchart.ChartTitleContainer.ForegroundText),
        //            StrokeThickness = tchart.ChartTitleContainer.Thickness,
        //            CapType = tchart.ChartTitleContainer.CapType,
        //            JoinType = tchart.ChartTitleContainer.JoinType,
        //            DashType = tchart.ChartTitleContainer.DashType,
        //        };
        //    }

        //    if (!(tchart is TPieChart))
        //    {
        //        resultData.DataAxes = new HTMLTDataAxes();
        //        //Lấy dữ liệu primary horizontal
        //        if (tchart.IsPrimaryHorizontal)
        //        {
        //            var primaryHorizontal = resultData.DataAxes.ListDataAxes[0];
        //            var ftPrimaryHorizontal = new FormattedText("0", CultureInfo.CurrentUICulture, FlowDirection.LeftToRight,
        //                new Typeface(tchart.LabelCategoryContainer.FontFamily.ToString()), tchart.LabelCategoryContainer.FontSize, Brushes.Black);
        //            primaryHorizontal.ID = "prih" + Guid.NewGuid().ToString("N");
        //            primaryHorizontal.Key = TypeOptions.PrimaryHorizontal.ToString();
        //            primaryHorizontal.IsChecked = tchart.IsPrimaryHorizontal;
        //            primaryHorizontal.Top = tchart.LabelCategoryContainer.Top;
        //            primaryHorizontal.Left = tchart.LabelCategoryContainer.Left;
        //            primaryHorizontal.HeightText = ftPrimaryHorizontal.Height;
        //            primaryHorizontal.FontFamily = tchart.LabelCategoryContainer.FontFamily.ToString();
        //            primaryHorizontal.FontSize = tchart.LabelCategoryContainer.FontSize;
        //            primaryHorizontal.Width = tchart.LabelCategoryContainer.Width;
        //            primaryHorizontal.Height = tchart.LabelCategoryContainer.Height;
        //            primaryHorizontal.Fill = GetColor(tchart.LabelCategoryContainer.Fill);
        //            primaryHorizontal.Stroke = GetColor(tchart.LabelCategoryContainer.Stroke);
        //            primaryHorizontal.ForegroundText = GetColor(tchart.LabelCategoryContainer.ForegroundText);
        //            primaryHorizontal.StrokeThickness = tchart.LabelCategoryContainer.Thickness;
        //            primaryHorizontal.CapType = tchart.LabelCategoryContainer.CapType;
        //            primaryHorizontal.JoinType = tchart.LabelCategoryContainer.JoinType;
        //            primaryHorizontal.DashType = tchart.LabelCategoryContainer.DashType;
        //        }
        //        //Lấy dữ liệu primary vertical
        //        if (tchart.IsPrimaryVertical)
        //        {
        //            var primaryVertical = resultData.DataAxes.ListDataAxes[1];
        //            var ftPrimaryVertical = new FormattedText("0", CultureInfo.CurrentUICulture, FlowDirection.LeftToRight,
        //                new Typeface(tchart.VerticalItemContainer.FontFamily.ToString()), tchart.VerticalItemContainer.FontSize, Brushes.Black);
        //            primaryVertical.ID = "priv" + Guid.NewGuid().ToString("N");
        //            primaryVertical.Key = TypeOptions.PrimaryVertical.ToString();
        //            primaryVertical.IsChecked = tchart.IsPrimaryVertical;
        //            primaryVertical.Top = tchart.VerticalItemContainer.Top;
        //            primaryVertical.Left = tchart.VerticalItemContainer.Left + tchart.VerticalItemContainer.Width - tchart.VerticalItemContainer.Padding.Right;
        //            primaryVertical.HeightText = ftPrimaryVertical.Height;
        //            primaryVertical.FontFamily = tchart.VerticalItemContainer.FontFamily.ToString();
        //            primaryVertical.FontSize = tchart.VerticalItemContainer.FontSize;
        //            primaryVertical.Width = tchart.VerticalItemContainer.Width;
        //            primaryVertical.Height = tchart.VerticalItemContainer.Height;
        //            primaryVertical.Fill = GetColor(tchart.VerticalItemContainer.Fill);
        //            primaryVertical.Stroke = GetColor(tchart.VerticalItemContainer.Stroke);
        //            primaryVertical.ForegroundText = GetColor(tchart.VerticalItemContainer.ForegroundText);
        //            primaryVertical.StrokeThickness = tchart.VerticalItemContainer.Thickness;
        //            primaryVertical.CapType = tchart.VerticalItemContainer.CapType;
        //            primaryVertical.JoinType = tchart.VerticalItemContainer.JoinType;
        //            primaryVertical.DashType = tchart.VerticalItemContainer.DashType;

        //            primaryVertical.ShapeBases = GetListShapeBasesVertical(tchart.VerticalItemContainer);
        //        }
        //        //Lấy dữ liệu secondary vertical
        //        if (tchart.IsSecondaryVertical)
        //        {
        //            var secondaryVertical = resultData.DataAxes.ListDataAxes[2];
        //            var ftSecondaryVertical = new FormattedText("0", CultureInfo.CurrentUICulture, FlowDirection.LeftToRight,
        //                new Typeface(tchart.VerticalSecondaryContainer.FontFamily.ToString()), tchart.VerticalSecondaryContainer.FontSize, Brushes.Black);
        //            secondaryVertical.ID = "secv" + Guid.NewGuid().ToString("N");
        //            secondaryVertical.Key = TypeOptions.SecondaryVertical.ToString();
        //            secondaryVertical.IsChecked = tchart.IsSecondaryVertical;
        //            secondaryVertical.Top = tchart.VerticalSecondaryContainer.Top;
        //            secondaryVertical.Left = tchart.VerticalSecondaryContainer.Left + tchart.VerticalSecondaryContainer.Padding.Left;
        //            secondaryVertical.FontFamily = tchart.VerticalSecondaryContainer.FontFamily.ToString();
        //            secondaryVertical.FontSize = tchart.VerticalSecondaryContainer.FontSize;
        //            secondaryVertical.HeightText = ftSecondaryVertical.Height;
        //            secondaryVertical.Width = tchart.VerticalSecondaryContainer.Width;
        //            secondaryVertical.Height = tchart.VerticalSecondaryContainer.Height;
        //            secondaryVertical.Fill = GetColor(tchart.VerticalSecondaryContainer.Fill);
        //            secondaryVertical.Stroke = GetColor(tchart.VerticalSecondaryContainer.Stroke);
        //            secondaryVertical.ForegroundText = GetColor(tchart.VerticalItemContainer.ForegroundText);
        //            secondaryVertical.StrokeThickness = tchart.VerticalSecondaryContainer.Thickness;
        //            secondaryVertical.CapType = tchart.VerticalSecondaryContainer.CapType;
        //            secondaryVertical.JoinType = tchart.VerticalSecondaryContainer.JoinType;
        //            secondaryVertical.DashType = tchart.VerticalSecondaryContainer.DashType;

        //            secondaryVertical.ShapeBases = GetListShapeBasesVertical(tchart.VerticalSecondaryContainer);
        //        }

        //        //Lấy dữ liệu data table
        //        if (tchart.IsDataTableKey)
        //        {
        //            resultData.DataTable = new HTMLDataTable()
        //            {
        //                IsDataTableKey = tchart.IsDataTableKey,
        //                IsWithLegendKey = tchart.IsWithLegendKey,
        //                TableBorder = new TableBorder()
        //                {
        //                    IsHorizontal = tchart.IsDataTableHorizontal,
        //                    IsVertical = tchart.IsDataTableVertical,
        //                    IsOutline = tchart.IsDataTableOutline,
        //                },
        //                DataTable = new DataTable()
        //                {
        //                    ID = "tbl" + Guid.NewGuid().ToString("N"),
        //                    WidthLeftTable = tchart.TableContainer.WidthTableLeft,
        //                    WidthTableSeri = tchart.TableContainer.WidthTableSeri,
        //                    WidthTableLegend = tchart.TableContainer.WidthTableLegend,
        //                    FontFamily = tchart.TableContainer.FontFamily.ToString(),
        //                    FontSize = tchart.TableContainer.FontSize,
        //                    Fill = GetColor(tchart.TableContainer.Fill),
        //                    Stroke = GetColor(tchart.TableContainer.Stroke),
        //                    ForegroundText = GetColor(tchart.TableContainer.ForegroundText),
        //                    CapType = tchart.TableContainer.CapType,
        //                    DashType = tchart.TableContainer.DashType,
        //                    JoinType = tchart.TableContainer.JoinType,
        //                    StrokeThickness = tchart.TableContainer.Thickness,
        //                },
        //                HeightTable = new HeightTable()
        //                {
        //                    HeightCategory = tchart.TableContainer.HeightCategory * 2,
        //                }
        //            };

        //            var categoryTable = tchart.TableContainer.CategoriesDataTable.FirstOrDefault(x => x.Series.Count != 0);
        //            var seriesTable = categoryTable.Series.OrderByDescending(x => x.ChartKey).ThenBy(x => (x.TemplateChart == TemplateChart.Clustered ? (int)x.TemplateChart : x.IDSeri)).ToList();
        //            foreach (var item in seriesTable)
        //            {
        //                resultData.DataTable.HeightTable.Series.Add(new SeriTable()
        //                {
        //                    ID = "rtbl" + Guid.NewGuid().ToString("N"),
        //                    Height = item.FormattedText.Height * 2,
        //                    HeightLegend = tchart.TableContainer.WidthLegend,
        //                    WidthLegend = tchart.TableContainer.WidthLegend,
        //                    Name = item.Name,
        //                });
        //            }
        //        }
        //        else
        //        {
        //            resultData.DataTable = new HTMLDataTable()
        //            {
        //                IsDataTableKey = tchart.IsDataTableKey,
        //            };

        //        }

        //        //lấy dữ liệu data gridline
        //        if (tchart.IsGridlineKey)
        //        {
        //            resultData.DataGridline = new HTMLTDataGridline()
        //            {
        //                IsGridlineKey = tchart.IsGridlineKey,
        //                IsPrimaryMajorHorizontal = tchart.IsPrimaryMajorHorizontal,
        //                IsPrimaryMajorVertical = tchart.IsPrimaryMajorVertical,
        //                IsPrimaryMinorHorizontal = tchart.IsPrimaryMinorHorizontal,
        //                IsPrimaryMinorVertical = tchart.IsPrimaryMinorVertical,
        //                Stroke = GetColor(tchart.GridlineContainer.Stroke),
        //                StrokeThickness = tchart.GridlineContainer.Thickness,
        //            };
        //        }
        //        else
        //        {
        //            resultData.DataGridline = new HTMLTDataGridline()
        //            {
        //                IsGridlineKey = tchart.IsGridlineKey,
        //            };
        //        }

        //        var _chartDataXML = tchart.Data as TChartDataXML;

        //        //lấy dữ liệu data axistitle => chưa làm
        //        resultData.DataAxisTitle = new HTMLTDataAxisTitle()
        //        {
        //            IsAxisTitleKey = tchart.IsAxisTitleKey,
        //            IsAxisTitlePrimaryHorizontal = _chartDataXML.DataAxisTitle.IsAxisTitlePrimaryHorizontal,
        //            IsAxisTitlePrimaryVertical = _chartDataXML.DataAxisTitle.IsAxisTitlePrimaryVertical,
        //            IsAxisTitleSecondaryHorizontal = _chartDataXML.DataAxisTitle.IsAxisTitleSecondaryHorizontal,
        //            IsAxisTitleSecondaryVertical = _chartDataXML.DataAxisTitle.IsAxisTitleSecondaryVertical,
        //        };

        //        //Lấy dữ liệu SeriOption
        //        var categorySeriOption = tchart.ChartData.Categories.FirstOrDefault(x => x.Series.Count != 0);
        //        resultData.ConfigSeriOption = new HTMLConfigSeriOption();
        //        var seriOption = categorySeriOption.Series.FirstOrDefault(x => x.IsPrimaryAxis && x.ChartKey == ChartType.ColumnChart);
        //        if (seriOption != null)
        //        {
        //            resultData.ConfigSeriOption.PrimaryAxis = new HTMLSeriOption()
        //            {
        //                GapWidth = seriOption.GapWidth,
        //                OverLap = seriOption.OverLap,
        //            };
        //        }
        //        seriOption = categorySeriOption.Series.FirstOrDefault(x => !x.IsPrimaryAxis && x.ChartKey == ChartType.ColumnChart);
        //        if (seriOption != null)
        //        {
        //            resultData.ConfigSeriOption.SecondaryAxis = new HTMLSeriOption()
        //            {
        //                GapWidth = seriOption.GapWidth,
        //                OverLap = seriOption.OverLap,
        //            };
        //        }
        //    }
        //    else
        //    {
        //        resultData.Type = "PieChart";
        //        resultData.ConfigSeriOption = new HTMLConfigSeriOption()
        //        {
        //            HTMLSeriOptionPie = new HTMLSeriOptionPie()
        //            {
        //                AngleOfFirstSlice = (tchart as TPieChart).AngleOfFirstSlice,
        //            }
        //        };
        //    }


        //    //lấy dữ liệu dât Legend
        //    if (tchart.IsLegendKey)
        //    {
        //        resultData.DataLegend = new HTMLTDataLegend()
        //        {
        //            ID = "lgn" + Guid.NewGuid().ToString("N"),
        //            Top = tchart.LegendContainer.Top,
        //            Left = tchart.LegendContainer.Left,
        //            FontFamily = tchart.LegendContainer.ToString(),
        //            FontSize = tchart.LegendContainer.FontSize,
        //            Width = tchart.LegendContainer.Width,
        //            Height = tchart.LegendContainer.Height,
        //            Fill = GetColor(tchart.LegendContainer.Fill),
        //            Stroke = GetColor(tchart.LegendContainer.Stroke),
        //            ForegroundText = GetColor(tchart.LegendContainer.ForegroundText),
        //            StrokeThickness = tchart.LegendContainer.Thickness,
        //            CapType = tchart.LegendContainer.CapType,
        //            JoinType = tchart.LegendContainer.JoinType,
        //            DashType = tchart.LegendContainer.DashType,
        //        };
        //        resultData.DataLegend.IsLegendKey = tchart.IsLegendKey;
        //        foreach (var item in tchart.LegendContainer.ContainerLegends)
        //        {
        //            resultData.DataLegend.Series.Add(new SeriLegend()
        //            {
        //                Shape = new ShapeLegend()
        //                {
        //                    ID = "shape" + Guid.NewGuid().ToString("N"),
        //                    Top = item.Top,
        //                    Left = item.Left,
        //                    Fill = GetColor(item.Fill),
        //                    Stroke = GetColor(item.Stroke),
        //                    StrokeThickness = item.Thickness,
        //                    CapType = item.CapType,
        //                    DashType = item.DashType,
        //                    Height = item.Height,
        //                    Width = item.Width,
        //                    JoinType = item.JoinType,
        //                    Type = ShapeLegend.ConverterTypeShape(item.ChartKey),
        //                },
        //                Text = new TextLegend()
        //                {
        //                    ID = "txt" + Guid.NewGuid().ToString("N"),
        //                    Top = item.TopText,
        //                    Left = item.LeftText,
        //                    Width = item.Width,
        //                    Height = item.Height,
        //                    Content = item.Name,
        //                    ForegroundText = GetColor(item.ForegroundText),
        //                    FontFamily = item.FontFamily,
        //                    FontSize = item.FontSize,
        //                }
        //            });
        //        }
        //    }

        //    //Data label
        //    resultData.DataLabel = new HTMLTDataLabel();
        //    foreach (var labelItem in tchart.LabelContainer?.ContainerLabels)
        //    {
        //        var ft = new FormattedText(labelItem.Value.ToString(), CultureInfo.CurrentUICulture, FlowDirection.LeftToRight,
        //                new Typeface(labelItem.FontFamily.ToString()), labelItem.FontSize, labelItem.ForegroundText.Brush);
        //        if (labelItem.IsLabelKey)
        //        {
        //            resultData.DataLabel.Texts.Add(new TextLegend()
        //            {
        //                ID = "txt" + Guid.NewGuid().ToString("N"),
        //                //Top = labelItem.Top + ft.Height / 2,
        //                Top = labelItem.Top + ft.Height / 2 + ft.Height,
        //                Left = labelItem.Left + ft.WidthIncludingTrailingWhitespace / 2,
        //                Width = labelItem.Width,
        //                Height = labelItem.Height,
        //                Content = labelItem.Value,
        //                ForegroundText = GetColor(labelItem.ForegroundText),
        //                FontFamily = labelItem.FontFamily.ToString(),
        //                FontSize = labelItem.FontSize,
        //                StrokeThickness = labelItem.Thickness,
        //                CapType = labelItem.CapType,
        //                DashType = labelItem.DashType,
        //                JoinType = labelItem.JoinType,
        //            });
        //        }
        //    }

        //}
        //#endregion
    }
}
