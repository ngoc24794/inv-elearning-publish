﻿using INV.Elearning.Core;
using INV.Elearning.Core.Helper;
using INV.Elearning.HTMLHelper.HTMLData.ColorClass;
using INV.Elearning.Text.Views;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace INV.Elearning.HTMLHelper.HTMLData.Helpers
{
    public partial class HTMLDataHelper
    {
        private bool _isCheckPreview;
        /// <summary>
        /// Lấy dữ liệu màu sắc
        /// </summary>
        /// <param name="colorBase"></param>
        /// <returns></returns>
        public HTMLColorBase GetColor(ColorBrushBase colorBase)
        {
            if (colorBase is ColorSolidBrush solid)
                return GetColor(solid);

            if (colorBase is ColorGradientBrush gradient)
                return GetColor(gradient);

            if (colorBase is ColorTextureBrush image)
                return GetColor(image);

            if (colorBase is ColorPatternBrush pattern)
                return GetColor(pattern);

            if (colorBase?.Brush is SolidColorBrush solidBrush)
                return new HTMLSolid() { Color = ConvertToRGBA(solidBrush.Color), Opacity = 1.0, ColorType = HTMLColorBase.HTMLColorType.Solid };

            if (colorBase == null)
                return GetColor(new ColorSolidBrush() { Color = Colors.Transparent });

            //Còn cái pattern
            return null;
        }
        public string ConvertColorTextToHtmlColor(RichTextEditor richTextEditor, ColorBrushBase colorBrushBase)
        {
            HTMLColorBase htmlColorBase = null;
            if (colorBrushBase?.Brush != null)
            {
                htmlColorBase = GetColor(colorBrushBase);
            }
            else if (richTextEditor?.TextContainer?.Document?.Foreground?.Brush != null)
            {
                htmlColorBase = GetColor(richTextEditor.TextContainer.Document.Foreground);
            }
            if (htmlColorBase == null)
                htmlColorBase = GetColor(new ColorBrushBase() { Brush = Brushes.Black });
            if (htmlColorBase is HTMLSolid)
            {
                return (htmlColorBase as HTMLSolid).Color.ToString();
            }
            if (htmlColorBase is HTMLSolid)
            {
                return (htmlColorBase as HTMLSolid).Color.ToString();
            }

            return "";
        }

        public HTMLColorBase GetColor(Brush brush)
        {
            if (brush is SolidColorBrush solidBrush)
            {
                return new HTMLSolid() { Color = ConvertToRGBA(solidBrush.Color, solidBrush.Opacity), Opacity = 1, ColorType = HTMLColorBase.HTMLColorType.Solid };

            }
            if (brush is ImageBrush imageBrush)
            {
                return new HTMLImageColor();
            }
            return null;
        }

        private string ConvertToRGBA(Color color, double opacity)
        {
            if (color == Colors.Transparent)
                return $"rgba(0,0,0,0)";

            int R = color.R,
                            G = color.G,
                            B = color.B,
                            A = color.A;
            if (opacity < 1.0)
            {
                return $"rgba({R},{G},{B},{opacity})";
            }

            return $"rgba({R},{G},{B},{A})";
        }

        /// <summary>
        /// Lấy màu kiểu màu dải
        /// </summary>
        /// <param name="color"></param>
        /// <returns></returns>
        private HTMLGradient GetColor(ColorGradientBrush color)
        {
            var resultColor = new HTMLGradient();
            if (color.Brush is GradientBrush gradient)
            {
                resultColor.Opacity = gradient.Opacity;
                resultColor.GradientType = color.GradientType == Core.Model.GradientColorType.Linear ? HTMLGradient.HTMLGradientType.Linear : HTMLGradient.HTMLGradientType.Radial;
                resultColor.Stops = GetStops(gradient);

                if (gradient is LinearGradientBrush linear)
                {
                    resultColor.X1 = linear.StartPoint.X;
                    resultColor.Y1 = linear.StartPoint.Y;
                    resultColor.X2 = linear.EndPoint.X;
                    resultColor.Y2 = linear.EndPoint.Y;
                }
                else if (gradient is RadialGradientBrush radial)
                {
                    resultColor.CenterX = radial.Center.X;
                    resultColor.CenterY = radial.Center.Y;
                    //resultColor.ColorType = 
                }

            }
            resultColor.Angle = color.Angle;
            return resultColor;
        }

        /// <summary>
        /// Lấy danh sách điểm dừng
        /// </summary>
        /// <param name="gradient"></param>
        /// <returns></returns>
        private List<HTMLGradientStop> GetStops(GradientBrush gradient)
        {
            var result = new List<HTMLGradientStop>();
            foreach (var stop in gradient.GradientStops.OrderBy(x => x.Offset))
            {
                if (stop.Offset <= 1)
                    result.Add(new HTMLGradientStop()
                    {
                        Color = ConvertToRGBA(stop.Color),
                        Offset = stop.Offset,
                        Opacity = stop.Color.A
                    });
            }
            return result;
        }

        /// <summary>
        /// Lấy màu kiểu hình ảnh
        /// </summary>
        /// <param name="color"></param>
        /// <returns></returns>
        private HTMLImageColor GetColor(ColorTextureBrush color)
        {
            var imageSize = GetImageSize(color.Brush as ImageBrush);
            var result = new HTMLImageColor()
            {
                ImageAlign = (HTMLImageColor.HTMLImageAlign)(color.Alignement),
                ImageUrl = CopyImageToExportFolder(color.ImagePath, color.Brush as ImageBrush).Replace(RootFolder + "\\", ""),
                IsRotateWidthShape = color.CanRotate,
                MirrorType = (HTMLImageColor.HTMLImageMirrorType)(color.Mirror),
                OffsetBottom = color.OffsetBottom,
                OffsetLeft = color.OffsetLeft,
                OffsetRight = color.OffsetRight,
                IsRepeated = color.IsTiled,
                OffsetTop = color.OffsetTop,
                OffsetX = color.OffsetX,
                OffsetY = color.OffsetY,
                Opacity = 1- color.Transparentcy,
                ScaleX = color.ScaleX,
                ScaleY = color.ScaleY,
                OriginWidth = imageSize.Width,
                OriginHeight = imageSize.Height,
                ColorType = HTMLImageColor.HTMLColorType.Image
            };

            return result;
        }

        /// <summary>
        /// Lấy kích thước ảnh dựa vào Brush
        /// </summary>
        /// <param name="imageBrush"></param>
        /// <returns></returns>
        private Size GetImageSize(ImageBrush imageBrush)
        {
            if (imageBrush == null || imageBrush.ImageSource == null) return new Size();

            return new Size(imageBrush.ImageSource.Width, imageBrush.ImageSource.Height);
        }

        /// <summary>
        /// Lấy kích thước ảnh dựa vào Brush
        /// </summary>
        /// <param name="imageBrush"></param>
        /// <returns></returns>
        private string GetImage(ImageBrush imageBrush, string imgUri)
        {
            if (imageBrush == null || imageBrush.ImageSource == null) return string.Empty;

            var encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create((BitmapSource)imageBrush.ImageSource));
            int indexImg = imgUri.LastIndexOf("/");
            if (indexImg == -1)
            {
                indexImg = imgUri.LastIndexOf("\\");
            }
            var destUrl = Path.Combine(this.ImageFolder, $"{imgUri.Substring(indexImg + 1)}");
            if (File.Exists(destUrl)) return destUrl;

            using (FileStream stream = new FileStream(destUrl, FileMode.Create))
                encoder.Save(stream);

            return destUrl;
        }

        /// <summary>
        /// Lấy màu kiểu Pattern
        /// </summary>
        /// <param name="color"></param>
        /// <returns></returns>
        private HTMLImageColor GetColor(ColorPatternBrush color)
        {
            var result = new HTMLImageColor()
            {
                ImageUrl = GetImageFromPattern(color).Replace(RootFolder + "\\", ""),
                MirrorType = HTMLImageColor.HTMLImageMirrorType.None,
                IsRepeated = true,
                ScaleX = 1.0,
                ScaleY = 1.0,
                OriginWidth = 8.0,
                OriginHeight = 8.0,
                IsPattern = true,
                ColorType = HTMLImageColor.HTMLColorType.Image
            };

            return result;
        }

        /// <summary>
        /// Lấy ảnh từ Brush
        /// </summary>
        /// <param name="brush"></param>
        /// <returns></returns>
        private string GetImageFromPattern(ColorPatternBrush pattern)
        {
            string imageUrl = Path.Combine(ImageFolder, $"{pattern.HatchStyle}_{pattern.PatternBackground?.Color?.Replace("#", "")}_{pattern.PatternForeground?.Color?.Replace("#", "")}.png");
            if (File.Exists(imageUrl)) return imageUrl; //Tồn tại file trước đó

            Canvas canvas = new Canvas();
            canvas.Width = 8;
            canvas.Height = 8;
            canvas.Background = pattern.Brush;
            FileHelper.SaveToImageOriginSize(imageUrl, canvas, new Size(8, 8));
            return imageUrl;
        }

        /// <summary>
        /// Lấy màu kiểu màu đơn
        /// </summary>
        /// <param name="color"></param>
        /// <returns></returns>
        private HTMLSolid GetColor(ColorSolidBrush color)
        {
            return new HTMLSolid()
            {
                Color = ConvertToRGBA(color),
                Opacity = color.Color.ScA,
                ColorType = HTMLSolid.HTMLColorType.Solid
            };
        }

        /// <summary>
        /// Lấy màu theo kiểu RGBA
        /// </summary>
        /// <param name="colorBase"></param>
        /// <returns></returns>
        private string ConvertToRGBA(ColorSolidBrush colorBase)
        {
            Color color = colorBase.Color;
            if (color == Colors.Transparent) return $"transparent";
            int R = color.R,
                G = color.G,
                B = color.B;
            //Tiến 07-08-2018
            //double A = Math.Min(Math.Round(colorBase.Opacity, 2), color.A);
            double A = Math.Min(Math.Round(colorBase.Opacity, 2), color.ScA);



            return $"rgba({R},{G},{B},{A})";
        }

        /// <summary>
        /// Lấy màu theo kiểu RGB
        /// </summary>
        /// <param name="color"></param>
        /// <returns></returns>
        private string ConvertToRGBA(Color color)
        {
            int R = color.R,
                G = color.G,
                B = color.B,
                A = color.A;
            return $"rgba({R},{G},{B},{A})";
        }


    }
}
