﻿using INV.Elearning.HTMLHelper.HTMLData.LayoutBase;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace INV.Elearning.HTMLHelper.HTMLData.SlideBase
{
    public class HTMLSlideBase
    {
        /// <summary>
        /// ID của slide
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public string ID { get; set; }
        /// <summary>
        /// kiểu của slide
        /// </summary>
        [JsonProperty(PropertyName = "slt")]
        public string SlideKeyType { get; set; }
        /// <summary>
        /// Tên slide
        /// </summary>
        [JsonProperty(PropertyName = "nm")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "w")]
        public double Width { get; set; }
        [JsonProperty(PropertyName = "h")]
        public double Height { get; set; }
        /// <summary>
        /// Theme
        /// </summary>
        //[JsonProperty(PropertyName = "thm")]
        //public HTMLTheme Theme { get; set; }
        /// <summary>
        /// Layout chính sẽ hiển thị mặc định
        /// </summary>
        // [JsonProperty(PropertyName = "mLt")]
        //  public HTMLLayoutBase MainLayout { set; get; }
        /// <summary>
        /// Danh sách các layout có trong slide
        /// </summary>
        [JsonProperty(PropertyName = "lts")]
        public List<HTMLLayoutBase> Layouts { get; set; }
        /// <summary>
        /// Thumbnail của slide
        /// </summary>
        [JsonProperty(PropertyName = "thmU")]
        public string ThumbnailUrl { get; set; }
        /// <summary>
        /// Hiệu ứng chuyển trang
        /// </summary>
        [JsonProperty(PropertyName = "trans")]
        public HTMLTransition Transition { get; set; }
        /// <summary>
        /// Thời gian của 1 slide
        /// </summary>
        [JsonProperty(PropertyName = "tim")]
        public HTMLTiming Timing { get; set; }
        /// <summary>
        /// Hiển thị trên menu player hay không
        /// </summary>
        [JsonProperty(PropertyName = "cs")]
        public bool CanShowInMenu { get; set; }

        /// <summary>
        /// ThemeLayout
        /// </summary>
        [JsonProperty(PropertyName = "the")]
        public HTMLLayoutBase ThemeLayout { get; set; }

        /// <summary>
        /// Config Slide
        /// </summary>        
        [JsonProperty(PropertyName = "slC")]
        public HTMLSlideConfig SlideConfig { get; set; }


        /// <summary>
        /// Note
        /// </summary>        
        [JsonProperty(PropertyName = "nt")]
        public string Note { get; set; }

        /// <summary>
        /// IsHideBackground
        /// </summary>        
        [JsonProperty(PropertyName = "IhBa")]
        public bool IsHideBackground { get; set; }
    }
}
