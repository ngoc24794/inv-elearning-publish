﻿using INV.Elearning.Core.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INV.Elearning.HTMLHelper.HTMLData.SlideBase
{
    public class HTMLSlideConfig
    {
        /// <summary>
        /// Hiển thị nút quay lại
        /// </summary>
        [JsonProperty(PropertyName = "pB")]
        public bool PreviousButtonEnable { get; set; }
        /// <summary>
        /// Hiển thị nút tiếp theo
        /// </summary>
        [JsonProperty(PropertyName = "nB")]
        public bool NextButtonEnable { get; set; }
        /// <summary>
        /// Hiển thị nút Submit
        /// </summary>
        [JsonProperty(PropertyName = "sB")]
        public bool SubmitButtonEnable { get; set; }
        /// <summary>
        /// Cho phép vuốt quay lại
        /// </summary>
        [JsonProperty(PropertyName = "pS")]
        public bool PreviousSwipeEnable { get; set; }
        /// <summary>
        /// Cho phép vuốt tiếp theo
        /// </summary>
        [JsonProperty(PropertyName = "nS")]
        public bool NextSwipeEnable { get; set; }
        /// <summary>
        /// Chế độ xem lại
        /// </summary>
        [JsonProperty(PropertyName = "rM")]
        [JsonConverter(typeof(StringEnumConverter))]
        public RevisitMode RevisitMode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty(PropertyName = "aM")]
        [JsonConverter(typeof(StringEnumConverter))]
        public SlideAdvanceMode AdvanceMode { get; set; }

        [JsonProperty(PropertyName = "pfM")]
        [JsonConverter(typeof(StringEnumConverter))]
        public PlayerFeaturesMode PlayerFeatureMode { get; set; }

        private bool _menuEnable;
        /// <summary>
        /// Cho phép player hiển thị menu
        /// </summary>
        [JsonProperty(PropertyName = "mE")]
        public bool MenuEnable
        {
            get { return _menuEnable; }
            set { _menuEnable = value; ; }
        }
        private bool _glossaryEnable;
        /// <summary>
        /// Cho phép player hiển thị Glosarry
        /// </summary>
        [JsonProperty(PropertyName = "glE")]
        public bool GlosarryEnable
        {
            get { return _glossaryEnable; }
            set { _glossaryEnable = value; }
        }

        private bool _seekEnable;
        /// <summary>
        /// Cho phép player hiển thị seekbar
        /// </summary>
        [JsonProperty(PropertyName = "sbE")]
        public bool SeekbarEnable
        {
            get { return _seekEnable; }
            set { _seekEnable = value; }
        }

        private bool _resourcesEnable;
        /// <summary>
        /// Cho phép player hiển thị resources
        /// </summary>
        [JsonProperty(PropertyName = "reE")]
        public bool ResourcesEnable
        {
            get { return _resourcesEnable; }
            set { _resourcesEnable = value; }
        }

        private bool _noteEnable;
        /// <summary>
        /// Cho phép player hiển thị notes
        /// </summary>
        [JsonProperty(PropertyName = "nE")]
        public bool NotesEnable
        {
            get { return _noteEnable; }
            set { _noteEnable = value; }
        }

        public void LoadData(PageConfig source)
        {
            PreviousButtonEnable = source.PreviousButtonEnable;
            NextButtonEnable = source.NextButtonEnable;
            GlosarryEnable = source.GlosarryEnable;
            MenuEnable = source.MenuEnable;
            SubmitButtonEnable = source.SubmitButtonEnable;
            PreviousSwipeEnable = source.PreviousSwipeEnable;
            NextSwipeEnable = source.NextSwipeEnable;
            RevisitMode = source.RevisitMode;
            AdvanceMode = source.AdvanceMode;
            PlayerFeatureMode = source.PlayerFeaturesMode;
            SeekbarEnable = source.SeekbarEnable;
            ResourcesEnable = source.ResourcesEnable;
            NotesEnable = source.NotesEnable;
        }
    }
}
