﻿using Newtonsoft.Json;

namespace INV.Elearning.HTMLHelper.HTMLData
{
    public class HTMLConfigSeriOption
    {
        [JsonProperty(PropertyName = "pA")]
        public HTMLSeriOption PrimaryAxis { get; set; }
        [JsonProperty(PropertyName = "sA")]
        public HTMLSeriOption SecondaryAxis { get; set; }
        [JsonProperty(PropertyName = "hS")]
        public HTMLSeriOptionPie HTMLSeriOptionPie { get; set; }

    }
    public class HTMLSeriOptionPie
    {
        [JsonProperty(PropertyName = "aO")]
        public double AngleOfFirstSlice { get; set; }
    }

    public class HTMLSeriOption
    {
        [JsonProperty(PropertyName = "ov")]
        public double OverLap { get; set; }
        [JsonProperty(PropertyName = "gW")]
        public double GapWidth { get; set; }
    }


}
