﻿using INV.Elearning.Charts.Views;
using INV.Elearning.Controls.Enums;
using INV.Elearning.HTMLHelper.HTMLData.ColorClass;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace INV.Elearning.HTMLHelper.HTMLData
{
    public class HTMLTSeriBase
    {
        [JsonProperty(PropertyName = "id")]
        public string ID { get; set; }
        [JsonProperty(PropertyName = "idSr")]
        public string IDSeri { get; set; }
        [JsonProperty(PropertyName = "cK")]
        public string ChartKey { get; set; }
        [JsonProperty(PropertyName = "tC")]
        public string TemplateChart { get; set; }
        [JsonProperty(PropertyName = "typ")]
        public string Type { get; set; }
        [JsonProperty(PropertyName = "nm")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "vl")]
        public double? Value { get; set; }


        private bool _isPrimaryAxis;
        [JsonProperty(PropertyName = "IPA")]
        public bool IsPrimaryAxis
        {
            get { return _isPrimaryAxis; }
            set
            {
                _isPrimaryAxis = value;
                if (value)
                {
                    SeriOption = 1;
                }
                else
                    SeriOption = 2;
            }
        }

        [JsonProperty(PropertyName = "sO")]
        public int SeriOption { get; set; }
        [JsonProperty(PropertyName = "ff")]
        public string FontFamily { get; set; }
        [JsonProperty(PropertyName = "fs")]
        public double FontSize { get; set; }
        [JsonProperty(PropertyName = "iC")]
        public Point ICircle { get; set; }
        [JsonProperty(PropertyName = "pS")]
        public Point PointStart { get; set; }

        //private HTMLTLabelItem _labelItemHTML;

        //public HTMLTLabelItem LabelItemHTML
        //{
        //    get { return _labelItemHTML ?? (_labelItemHTML = new HTMLTLabelItem()); }
        //    set { _labelItemHTML = value; }
        //}

        //public bool IsLabelKey { get; set; }

        #region Impliments BorderSupport
        /// <summary>
        /// Độ rộng đường viền
        /// </summary>
        [JsonProperty(PropertyName = "sTh")]
        public double StrokeThickness { get; set; }
        [JsonProperty("cT")]
        [JsonConverter(typeof(StringEnumConverter))]
        public System.Windows.Media.PenLineCap CapType { get; set; }
        [JsonProperty("jT")]
        [JsonConverter(typeof(StringEnumConverter))]
        public System.Windows.Media.PenLineJoin JoinType { get; set; }
        [JsonProperty("dT")]
        [JsonConverter(typeof(StringEnumConverter))]
        public DashType DashType { get; set; }
        /// <summary>
        /// Màu hình vẽ
        /// </summary>
        [JsonProperty(PropertyName = "fi")]
        public HTMLColorBase Fill { get; set; }
        /// <summary>
        /// Màu đường viền
        /// </summary>
        [JsonProperty(PropertyName = "str")]
        public HTMLColorBase Stroke { get; set; }
        #endregion

    }
}
