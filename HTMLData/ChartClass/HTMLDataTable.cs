﻿using INV.Elearning.Controls.Enums;
using INV.Elearning.HTMLHelper.HTMLData.ColorClass;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace INV.Elearning.HTMLHelper.HTMLData
{
    public class HTMLDataTable
    {
        [JsonProperty(PropertyName = "iDt")]
        public bool IsDataTableKey { get; set; }
        [JsonProperty(PropertyName = "iWl")]
        public bool IsWithLegendKey { get; set; }
        [JsonProperty(PropertyName = "tB")]
        public TableBorder TableBorder { get; set; }
        [JsonProperty(PropertyName = "dT")]
        public DataTable DataTable { get; set; }
        [JsonProperty(PropertyName = "hT")]
        public HeightTable HeightTable { get; set; }
    }

    public class SeriTable
    {
        [JsonProperty(PropertyName = "id")]
        public string ID { get; set; }
        [JsonProperty(PropertyName = "h")]
        public double Height { get; set; }
        [JsonProperty(PropertyName = "nm")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "wL")]
        public double WidthLegend { get; set; }
        [JsonProperty(PropertyName = "hL")]
        public double HeightLegend { get; set; }
    }

    public class DataTable
    {
        [JsonProperty(PropertyName = "id")]
        public string ID { get; set; }
        [JsonProperty(PropertyName = "hLt")]
        public double WidthLeftTable { get; set; }
        [JsonProperty(PropertyName = "wTl")]
        public double WidthTableLegend { get; set; }
        [JsonProperty(PropertyName = "wTs")]
        public double WidthTableSeri { get; set; }

        /// <summary>
        /// Màu chữ
        /// </summary>
        [JsonProperty(PropertyName = "fgT")]
        public HTMLColorBase ForegroundText { get; set; }        

        /// <summary>
        /// Kích cỡ của chữ
        /// </summary>
        [JsonProperty(PropertyName = "fs")]
        public double FontSize { get; set; }
        /// <summary>
        /// Kiểu chữ
        /// </summary>
        [JsonProperty(PropertyName = "ff")]
        public string FontFamily { get; set; }

        #region Impliments BorderSupport
        /// <summary>
        /// Độ rộng đường viền
        /// </summary>
        [JsonProperty(PropertyName = "sTh")]
        public double StrokeThickness { get; set; }
        [JsonProperty("cT")]
        [JsonConverter(typeof(StringEnumConverter))]
        public System.Windows.Media.PenLineCap CapType { get; set; }
        [JsonProperty("jT")]
        [JsonConverter(typeof(StringEnumConverter))]
        public System.Windows.Media.PenLineJoin JoinType { get; set; }
        [JsonProperty("dT")]
        [JsonConverter(typeof(StringEnumConverter))]
        public DashType DashType { get; set; }
        /// <summary>
        /// Màu hình vẽ
        /// </summary>
        [JsonProperty(PropertyName = "fi")]
        public HTMLColorBase Fill { get; set; }
        /// <summary>
        /// Màu đường viền
        /// </summary>
        [JsonProperty(PropertyName = "str")]
        public HTMLColorBase Stroke { get; set; }
        #endregion

        [JsonProperty(PropertyName = "cSB")]
        public List<HTMLShapeBase> CateogriesShapBases { get; set; } = new List<HTMLShapeBase>();
        [JsonProperty(PropertyName = "vSB")]
        public List<HTMLShapeBase> ValuesShapBases { get; set; } = new List<HTMLShapeBase>();
    }

    public class TableBorder
    {
        [JsonProperty(PropertyName = "iH")]
        public bool IsHorizontal { get; set; }
        [JsonProperty(PropertyName = "iV")]
        public bool IsVertical { get; set; }
        [JsonProperty(PropertyName = "iO")]
        public bool IsOutline { get; set; }
    }

    public class HeightTable
    {
        [JsonProperty(PropertyName = "hC")]
        public double HeightCategory { get; set; }
        private List<SeriTable> _series;

        [JsonProperty(PropertyName = "Se")]
        public List<SeriTable> Series
        {
            get { return _series ?? (_series = new List<SeriTable>()); }
            set { _series = value; }
        }
    }
}

