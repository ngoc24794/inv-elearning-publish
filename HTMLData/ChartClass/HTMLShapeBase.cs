﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;


namespace INV.Elearning.HTMLHelper.HTMLData
{
    public class HTMLShapeBase
    {
        [JsonProperty(PropertyName = "id")]
        public string ID { get; set; }
        [JsonProperty(PropertyName = "T")]
        public double Top { get; set; }
        [JsonProperty(PropertyName = "l")]
        public double Left { get; set; }
        [JsonProperty(PropertyName = "w")]
        public double Width { get; set; }
        [JsonProperty(PropertyName = "h")]
        public double Height { get; set; }
    }
}
