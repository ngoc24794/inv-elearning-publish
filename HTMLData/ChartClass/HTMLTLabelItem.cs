﻿using Newtonsoft.Json;

namespace INV.Elearning.HTMLHelper.HTMLData
{
    public class HTMLTLabelItem
    {
        [JsonProperty(PropertyName = "kC")]
        public string KeyCategoryName { get; set; }
        [JsonProperty(PropertyName = "kS")]
        public string KeySeriName { get; set; }
        [JsonProperty(PropertyName = "iL")]
        public bool IsLabelKey { get; set; }
        [JsonProperty(PropertyName = "iC")]
        public bool IsCenter { get; set; }
        [JsonProperty(PropertyName = "iI")]
        public bool IsInsideEnd { get; set; }
        [JsonProperty(PropertyName = "iIb")]
        public bool IsInsideBase { get; set; }
        [JsonProperty(PropertyName = "iO")]
        public bool IsOutsideEnd { get; set; }
        [JsonProperty(PropertyName = "iL")]
        public bool IsLeft { get; set; }
        [JsonProperty(PropertyName = "iR")]
        public bool IsRight { get; set; }

    }
}
