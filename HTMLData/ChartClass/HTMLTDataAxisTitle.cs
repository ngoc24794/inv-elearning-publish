﻿using Newtonsoft.Json;

namespace INV.Elearning.HTMLHelper.HTMLData
{
    public class HTMLTDataAxisTitle
    {
        [JsonProperty(PropertyName = "ik")]
        public bool IsAxisTitleKey { get; set; }
        [JsonProperty(PropertyName = "iH")]
        public bool IsAxisTitlePrimaryHorizontal { get; set; }
        [JsonProperty(PropertyName = "iV")]
        public bool IsAxisTitlePrimaryVertical { get; set; }
        [JsonProperty(PropertyName = "iS")]
        public bool IsAxisTitleSecondaryHorizontal { get; set; }
        [JsonProperty(PropertyName = "iSv")]
        public bool IsAxisTitleSecondaryVertical { get; set; }
    }
}
