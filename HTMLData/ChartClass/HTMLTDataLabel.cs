﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace INV.Elearning.HTMLHelper.HTMLData
{
    public class HTMLTDataLabel
    {
        private List<SeriLegend> _labels;

        [JsonProperty(PropertyName = "Te")]
        public List<SeriLegend> Labels
        {
            get { return _labels ?? (_labels = new List<SeriLegend>()); }
            set { _labels = value; }
        }
    }
}
