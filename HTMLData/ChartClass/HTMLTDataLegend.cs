﻿using INV.Elearning.Charts.Views;
using INV.Elearning.Controls.Enums;
using INV.Elearning.HTMLHelper.HTMLData.ColorClass;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace INV.Elearning.HTMLHelper.HTMLData
{
    public class HTMLTDataLegend
    {
        [JsonProperty(PropertyName = "iLk")]
        public bool IsLegendKey { get; set; }
        private List<SeriLegend> _series;

        [JsonProperty(PropertyName = "Se")]
        public List<SeriLegend> Series
        {
            get { return _series ?? (_series = new List<SeriLegend>()); }
            set { _series = value; }
        }
        [JsonProperty(PropertyName = "id")]
        public string ID { get; set; }
        [JsonProperty(PropertyName = "T")]
        public double Top { get; set; }
        [JsonProperty(PropertyName = "l")]
        public double Left { get; set; }
        [JsonProperty(PropertyName = "w")]
        public double Width { get; set; }
        [JsonProperty(PropertyName = "h")]
        public double Height { get; set; }
        [JsonProperty(PropertyName = "fgT")]
        public HTMLColorBase ForegroundText { get; set; }

        /// <summary>
        /// Kích cỡ của chữ
        /// </summary>
        [JsonProperty(PropertyName = "fs")]
        public double FontSize { get; set; }
        /// <summary>
        /// Kiểu chữ
        /// </summary>
        [JsonProperty(PropertyName = "ff")]
        public string FontFamily { get; set; }

        #region Impliments BorderSupport
        /// <summary>
        /// Độ rộng đường viền
        /// </summary>
        [JsonProperty(PropertyName = "sTh")]
        public double StrokeThickness { get; set; }
        [JsonProperty("cT")]
        [JsonConverter(typeof(StringEnumConverter))]
        public System.Windows.Media.PenLineCap CapType { get; set; }
        [JsonProperty("jT")]
        [JsonConverter(typeof(StringEnumConverter))]
        public System.Windows.Media.PenLineJoin JoinType { get; set; }
        [JsonProperty("dT")]
        [JsonConverter(typeof(StringEnumConverter))]
        public DashType DashType { get; set; }
        /// <summary>
        /// Màu hình vẽ
        /// </summary>
        [JsonProperty(PropertyName = "fi")]
        public HTMLColorBase Fill { get; set; }
        /// <summary>
        /// Màu đường viền
        /// </summary>
        [JsonProperty(PropertyName = "str")]
        public HTMLColorBase Stroke { get; set; }
        #endregion

    }

    public class SeriLegend
    {
        [JsonProperty(PropertyName = "sh")]
        public ShapeLegend Shape { get; set; }
        [JsonProperty(PropertyName = "txt")]
        public TextLegend Text { get; set; }
    }

    public class ShapeLegend : HTMLShapeBase
    {
        [JsonProperty(PropertyName = "typ")]
        public string Type { get; set; }

        /// <summary>
        /// Kích cỡ của chữ
        /// </summary>
        [JsonProperty(PropertyName = "fs")]
        public double FontSize { get; set; }
        /// <summary>
        /// Kiểu chữ
        /// </summary>
        [JsonProperty(PropertyName = "ff")]
        public string FontFamily { get; set; }

        #region Impliments BorderSupport
        /// <summary>
        /// Độ rộng đường viền
        /// </summary>
        [JsonProperty(PropertyName = "sTh")]
        public double StrokeThickness { get; set; }
        [JsonProperty("cT")]
        [JsonConverter(typeof(StringEnumConverter))]
        public System.Windows.Media.PenLineCap CapType { get; set; }
        [JsonProperty("jT")]
        [JsonConverter(typeof(StringEnumConverter))]
        public System.Windows.Media.PenLineJoin JoinType { get; set; }
        [JsonProperty("dT")]
        [JsonConverter(typeof(StringEnumConverter))]
        public DashType DashType { get; set; }
        /// <summary>
        /// Màu hình vẽ
        /// </summary>
        [JsonProperty(PropertyName = "fi")]
        public HTMLColorBase Fill { get; set; }
        /// <summary>
        /// Màu đường viền
        /// </summary>
        [JsonProperty(PropertyName = "str")]
        public HTMLColorBase Stroke { get; set; }
        #endregion

        public static string ConverterTypeShape(ChartType chartKey)
        {
            if (chartKey == ChartType.LineChart)
                return "line";
            else if (chartKey == ChartType.ColumnChart || chartKey == ChartType.AreaChart || chartKey == ChartType.PieChart)
                return "rect";
            return string.Empty;
        }
    }

    public class TextLegend
    {
        [JsonProperty(PropertyName = "id")]
        public string ID { get; set; }
        [JsonProperty(PropertyName = "T")]
        public double Top { get; set; }
        [JsonProperty(PropertyName = "l")]
        public double Left { get; set; }
        [JsonProperty(PropertyName = "w")]
        public double Width { get; set; }
        [JsonProperty(PropertyName = "h")]
        public double Height { get; set; }
        [JsonProperty(PropertyName = "Con")]
        public string Content { get; set; }
        [JsonProperty(PropertyName = "fgT")]
        public HTMLColorBase ForegroundText { get; set; }

        /// <summary>
        /// Kích cỡ của chữ
        /// </summary>
        [JsonProperty(PropertyName = "fs")]
        public double FontSize { get; set; }
        /// <summary>
        /// Kiểu chữ
        /// </summary>
        [JsonProperty(PropertyName = "ff")]
        public string FontFamily { get; set; }
        #region Impliments BorderSupport
        /// <summary>
        /// Độ rộng đường viền
        /// </summary>
        [JsonProperty(PropertyName = "sTh")]
        public double StrokeThickness { get; set; }
        [JsonProperty("cT")]
        [JsonConverter(typeof(StringEnumConverter))]
        public System.Windows.Media.PenLineCap CapType { get; set; }
        [JsonProperty("jT")]
        [JsonConverter(typeof(StringEnumConverter))]
        public System.Windows.Media.PenLineJoin JoinType { get; set; }
        [JsonProperty("dT")]
        [JsonConverter(typeof(StringEnumConverter))]
        public DashType DashType { get; set; }
        #endregion
    }
}
