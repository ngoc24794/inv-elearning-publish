﻿using INV.Elearning.Controls.Enums;
using INV.Elearning.HTMLHelper.HTMLData.ColorClass;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Windows.Media;

namespace INV.Elearning.HTMLHelper.HTMLData
{

    public class HTMLTDataGridline
    {
        [JsonProperty(PropertyName = "iK")]
        public bool IsGridlineKey { get; set; }
        [JsonProperty(PropertyName = "iMah")]
        public bool IsPrimaryMajorHorizontal { get; set; }
        [JsonProperty(PropertyName = "iMih")]
        public bool IsPrimaryMinorHorizontal { get; set; }
        [JsonProperty(PropertyName = "iMav")]
        public bool IsPrimaryMajorVertical { get; set; }
        [JsonProperty(PropertyName = "iMiv")]
        public bool IsPrimaryMinorVertical { get; set; }


        //[JsonProperty(PropertyName = "smaH")]
        //public HTMLColorBase StrokeMajorHorizontal { get; set; }
        //[JsonProperty(PropertyName = "smiH")]
        //public HTMLColorBase StrokeMinorHorizontal { get; set; }
        //[JsonProperty(PropertyName = "smiV")]
        //public HTMLColorBase StrokeMajorVertical { get; set; }
        //[JsonProperty(PropertyName = "smaV")]
        //public HTMLColorBase StrokeMinorVertical { get; set; }
        

        #region Impliments BorderSupport
        /// <summary>
        /// Độ rộng đường viền
        /// </summary>
        [JsonProperty(PropertyName = "sTh")]
        public double StrokeThickness { get; set; }
        [JsonProperty("cT")]
        [JsonConverter(typeof(StringEnumConverter))]
        public System.Windows.Media.PenLineCap CapType { get; set; }
        [JsonProperty("jT")]
        [JsonConverter(typeof(StringEnumConverter))]
        public System.Windows.Media.PenLineJoin JoinType { get; set; }
        [JsonProperty("dT")]
        [JsonConverter(typeof(StringEnumConverter))]
        public DashType DashType { get; set; }
        /// <summary>
        /// Màu đường viền chính
        /// </summary>
        [JsonProperty(PropertyName = "str")]
        public HTMLColorBase Stroke { get; set; }

        /// <summary>
        /// Màu đường viền phụ
        /// </summary>
        [JsonProperty(PropertyName = "strmi")]
        public HTMLColorBase StrokeMinor { get; set; }
        #endregion
    }
}
