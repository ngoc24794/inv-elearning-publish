﻿using Newtonsoft.Json;

namespace INV.Elearning.HTMLHelper.HTMLData
{
    public class HTMLFrameChart
    {
        [JsonProperty(PropertyName = "nm")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "T")]
        public double Top { get; set; }
        [JsonProperty(PropertyName = "l")]
        public double Left { get; set; }
        [JsonProperty(PropertyName = "w")]
        public double Width { get; set; }
        [JsonProperty(PropertyName = "h")]
        public double Height { get; set; }
    }
}
