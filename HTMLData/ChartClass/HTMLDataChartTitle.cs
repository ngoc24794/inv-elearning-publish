﻿using INV.Elearning.Controls.Enums;
using INV.Elearning.HTMLHelper.HTMLData.ColorClass;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace INV.Elearning.HTMLHelper.HTMLData
{
    public class HTMLDataChartTitle
    {
        [JsonProperty(PropertyName = "id")]
        public string ID { get; set; }
        [JsonProperty(PropertyName = "T")]
        public double Top { get; set; }
        [JsonProperty(PropertyName = "l")]
        public double Left { get; set; }
        [JsonProperty(PropertyName = "w")]
        public double Width { get; set; }
        [JsonProperty(PropertyName = "h")]
        public double Height { get; set; }
        [JsonProperty(PropertyName = "txt")]
        public string Text { get; set; }
        [JsonProperty(PropertyName = "tR")]
        public double TopRect { get; set; }
        [JsonProperty(PropertyName = "lR")]
        public double LeftRect { get; set; }
        [JsonProperty(PropertyName = "fgT")]
        public HTMLColorBase ForegroundText { get; set; }

        /// <summary>
        /// Kích cỡ của chữ
        /// </summary>
        [JsonProperty(PropertyName = "fs")]
        public double FontSize { get; set; }
        /// <summary>
        /// Kiểu chữ
        /// </summary>
        [JsonProperty(PropertyName = "ff")]
        public string FontFamily { get; set; }

        #region Impliments BorderSupport
        /// <summary>
        /// Độ rộng đường viền
        /// </summary>
        [JsonProperty(PropertyName = "sTh")]
        public double StrokeThickness { get; set; }
        [JsonProperty("cT")]
        [JsonConverter(typeof(StringEnumConverter))]
        public System.Windows.Media.PenLineCap CapType { get; set; }
        [JsonProperty("jT")]
        [JsonConverter(typeof(StringEnumConverter))]
        public System.Windows.Media.PenLineJoin JoinType { get; set; }
        [JsonProperty("dT")]
        [JsonConverter(typeof(StringEnumConverter))]
        public DashType DashType { get; set; }
        /// <summary>
        /// Màu hình vẽ
        /// </summary>
        [JsonProperty(PropertyName = "fi")]
        public HTMLColorBase Fill { get; set; }
        /// <summary>
        /// Màu đường viền
        /// </summary>
        [JsonProperty(PropertyName = "str")]
        public HTMLColorBase Stroke { get; set; }
        #endregion
    }
}
