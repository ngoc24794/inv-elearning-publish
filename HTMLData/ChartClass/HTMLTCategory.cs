﻿using Newtonsoft.Json;
using System.Collections.ObjectModel;

namespace INV.Elearning.HTMLHelper.HTMLData
{
    public class HTMLTCategory
    {
        [JsonProperty(PropertyName = "cN")]
        public string CategoryName { get; set; }
        [JsonProperty(PropertyName = "fs")]
        public double FontSize { get; set; }
        [JsonProperty(PropertyName = "ff")]
        public string FontFamily { get; set; }

        private ObservableCollection<HTMLTSeriBase> _series;

        [JsonProperty(PropertyName = "Se")]
        public ObservableCollection<HTMLTSeriBase> Series
        {
            get => _series ?? (_series = new ObservableCollection<HTMLTSeriBase>());
            set { _series = value; }
        }
    }

}
