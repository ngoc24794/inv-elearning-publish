﻿using INV.Elearning.Charts.Model;
using INV.Elearning.Charts.Views;
using System.Collections.ObjectModel;
using System.Windows.Media;
using System;
using System.Linq;
using INV.Elearning.Charts.Model.Data;
using Newtonsoft.Json;
using INV.Elearning.HTMLHelper.HTMLData.ColorClass;
using INV.Elearning.Controls.Enums;
using Newtonsoft.Json.Converters;

namespace INV.Elearning.HTMLHelper.HTMLData
{
    public class HTMLChartElement : HTMLStandardElement
    {
        /// <summary>
        /// Tên Loại Biểu Đồ
        /// </summary>
        [JsonProperty(PropertyName = "typ")]
        public string Type { get; set; }

        [JsonProperty(PropertyName = "opc")]
        public double Opacity { get; set; }

        [JsonProperty(PropertyName = "opci")]
        public double OpacityItem { get; set; }

        private HTMLDataChartTitle _dataChartTitle;
        [JsonProperty(PropertyName = "DCT")]
        public HTMLDataChartTitle DataChartTitle
        {
            get { return _dataChartTitle; }
            set { _dataChartTitle = value; }
        }

        private HTMLTDataAxes _dataAxes;
        [JsonProperty(PropertyName = "DA")]
        public HTMLTDataAxes DataAxes
        {
            get { return _dataAxes; }
            set { _dataAxes = value; }
        }

        private HTMLTDataAxisTitle _dataAxisTitle;
        [JsonProperty(PropertyName = "DAT")]
        public HTMLTDataAxisTitle DataAxisTitle
        {
            get { return _dataAxisTitle; }
            set { _dataAxisTitle = value; }
        }

        private HTMLDataTable _dataTable;
        [JsonProperty(PropertyName = "DT")]
        public HTMLDataTable DataTable
        {
            get { return _dataTable; }
            set { _dataTable = value; }
        }

        private HTMLTDataGridline _dataGridline;
        [JsonProperty(PropertyName = "DGL")]
        public HTMLTDataGridline DataGridline
        {
            get { return _dataGridline; }
            set { _dataGridline = value; }
        }

        private HTMLTDataLegend _dataLegend;
        [JsonProperty(PropertyName = "dle")]
        public HTMLTDataLegend DataLegend
        {
            get { return _dataLegend; }
            set { _dataLegend = value; }
        }

        private HTMLTDataLabel _dataLabel;
        [JsonProperty(PropertyName = "dla")]
        public HTMLTDataLabel DataLabel
        {
            get { return _dataLabel; }
            set { _dataLabel = value; }
        }

        private ObservableCollection<HTMLTCategory> _categories;
        [JsonProperty(PropertyName = "C")]
        public ObservableCollection<HTMLTCategory> Categories
        {
            get { return _categories ?? (_categories = new ObservableCollection<HTMLTCategory>()); }
            set { _categories = value; }
        }
        [JsonProperty(PropertyName = "frC")]
        public HTMLFrameChart FrameChart { get; set; }
        [JsonProperty(PropertyName = "Ck")]
        [JsonConverter(typeof(StringEnumConverter))]
        public ChartType ChartKey { get; set; }
        [JsonProperty(PropertyName = "TCh")]
        [JsonConverter(typeof(StringEnumConverter))]
        public TemplateChart TemplateChart { get; set; }
        [JsonProperty(PropertyName = "cSo")]
        public HTMLConfigSeriOption ConfigSeriOption { get; set; }
        [JsonProperty(PropertyName = "cpT")]
        [JsonConverter(typeof(StringEnumConverter))]
        public Core.Model.CompoundType CompoundType { get; set; }
        [JsonProperty(PropertyName = "pD")]
        public string PathData { get; set; }

        #region Impliments BorderSupport
        /// <summary>
        /// Độ rộng đường viền
        /// </summary>
        [JsonProperty(PropertyName = "sTh")]
        public double StrokeThickness { get; set; }
        [JsonProperty("cT")]
        [JsonConverter(typeof(StringEnumConverter))]
        public System.Windows.Media.PenLineCap CapType { get; set; }
        [JsonProperty("jT")]
        [JsonConverter(typeof(StringEnumConverter))]
        public System.Windows.Media.PenLineJoin JoinType { get; set; }
        [JsonProperty("dT")]
        [JsonConverter(typeof(StringEnumConverter))]
        public DashType DashType { get; set; }
        /// <summary>
        /// Màu hình vẽ
        /// </summary>
        [JsonProperty(PropertyName = "fi")]
        public HTMLColorBase Fill { get; set; }
        /// <summary>
        /// Màu đường viền
        /// </summary>
        [JsonProperty(PropertyName = "str")]
        public HTMLColorBase Stroke { get; set; }
        #endregion


    }
}
