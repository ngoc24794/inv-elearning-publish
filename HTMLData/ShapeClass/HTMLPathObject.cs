﻿using INV.Elearning.Controls.Enums;
using INV.Elearning.HTMLHelper.HTMLData.ColorClass;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace INV.Elearning.HTMLHelper.HTMLData.ShapeClass
{
    public class HTMLPathObject
    {
        /// <summary>
        /// Data của hình vẽ (đường path)
        /// </summary>
        [JsonProperty(PropertyName = "dt")]
        public string Data { get; set; }
        /// <summary>
        /// Hình vẽ có đường viền không
        /// </summary>
        [JsonProperty(PropertyName = "iSt")]
        public bool IsStroked { get; set; }
        /// <summary>
        /// Hình vẽ có màu không
        /// </summary>
        [JsonProperty(PropertyName = "iF")]
        public bool IsFilled { get; set; }
        /// <summary>
        /// Màu hình vẽ
        /// </summary>
        [JsonProperty(PropertyName = "fi")]
        public HTMLColorBase Fill { get; set; }
        /// <summary>
        /// Màu đường viền
        /// </summary>
        [JsonProperty(PropertyName = "str")]
        public HTMLColorBase Stroke { get; set; }
        /// <summary>
        /// Độ rộng đường viền
        /// </summary>
        [JsonProperty(PropertyName = "sTh")]
        public double StrokeThickness { get; set; }
        [JsonProperty("dT")]
        [JsonConverter(typeof(StringEnumConverter))]
        public DashType DashType { get; set; }
    }
}
