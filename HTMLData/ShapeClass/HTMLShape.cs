﻿
using INV.Elearning.Controls.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;

namespace INV.Elearning.HTMLHelper.HTMLData.ShapeClass
{
    public class HTMLShape
    {
        [JsonProperty("cT")]
        [JsonConverter(typeof(StringEnumConverter))]
        public System.Windows.Media.PenLineCap CapType { get; set; }
        [JsonProperty("jT")]
        [JsonConverter(typeof(StringEnumConverter))]
        public System.Windows.Media.PenLineJoin JoinType { get; set; }
        [JsonProperty("dT")]
        [JsonConverter(typeof(StringEnumConverter))]
        public DashType DashType { get; set; }
        [JsonProperty(PropertyName = "pOb")]
        public List<HTMLPathObject> PathObjects { get; set; }
    }
}
