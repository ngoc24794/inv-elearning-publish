﻿using Newtonsoft.Json;

namespace INV.Elearning.HTMLHelper.HTMLData
{
    public class HTMLWordBankQuestion : HTMLQuestionBase
    {
        /// <summary>
        /// nơi chứa câu trả lời
        /// </summary>
        [JsonProperty(PropertyName = "coA")]
        public HTMLWordBankAnswer ContentAnswer { get; set; }
    }

}
