﻿using INV.Elearning.HTMLHelper.HTMLData.Questions.Answers;
using INV.Elearning.Quizz;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace INV.Elearning.HTMLHelper.HTMLData
{
    public class HTMLNumericQuestion : HTMLQuestionBase
    {
        [JsonProperty(PropertyName = "nuC")]
        public HMLTNumericConfig NumericConfig { get; set; }
    }

    public class HMLTNumericConfig
    {
        public NumericMode NumericMode { get; set; }

        [JsonProperty(PropertyName = "val")]
        public List<HTMLNumericValue> Values { get; set; }
    }
}
