﻿using INV.Elearning.HTMLHelper.HTMLData.LayoutBase;
using Newtonsoft.Json;

namespace INV.Elearning.HTMLHelper.HTMLData
{
    public class HTMLFeedBack : HTMLLayoutBase
    {
        /// <summary>
        /// Điểm 
        /// </summary>
        [JsonProperty(PropertyName = "sC")]
        public double Score { get; set; }
        /// <summary>
        /// Audio fiole name
        /// </summary>
        [JsonProperty(PropertyName = "aS")]
        public string AudioSource { get; set; }
        /// <summary>
        /// ID Slide tiếp theo
        /// </summary>
        [JsonProperty(PropertyName = "brS")]
        public string BranchSlide { get; set; }
    }
}
