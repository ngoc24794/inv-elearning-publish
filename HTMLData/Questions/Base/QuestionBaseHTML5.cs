﻿using INV.Elearning.HTMLHelper.HTMLData;
using INV.Elearning.HTMLHelper.HTMLData.ColorClass;
using INV.Elearning.HTMLHelper.HTMLData.SlideBase;
using INV.Elearning.HTMLHelper.HTMLData.TextClass;
using INV.Elearning.Quizz;
using INV.Elearning.Quizz.Result;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;


namespace INV.Elearning.HTMLHelper
{
    /// <summary>
    /// Lớp dữ liệu cớ sở
    /// </summary>
    public class HTMLQuestionBase : HTMLSlideBase
    {
        /// <summary>
        /// Loại câu hỏi
        /// </summary>
        [JsonProperty(PropertyName = "tq")]
        public QuestionType TypeQuestion { get; set; }

        /// <summary>
        /// Nhóm câu hỏi
        /// </summary>
        [JsonProperty(PropertyName = "gt")]
        public Quizz.GroupType GroupType { get; set; }


        /// <summary>
        /// có xáo trộn đáp án hay không
        /// </summary>
        [JsonProperty(PropertyName = "sff")]
        public bool IsShuffle { get; set; }

        /// <summary>
        /// Số lần lặp lại
        /// </summary>
        [JsonProperty(PropertyName = "atm")]
        public int Attemps { get; set; }

        /// <summary>
        /// điểm của từng câu hỏi
        /// </summary>
        [JsonProperty(PropertyName = "p")]
        public int Points { get; set; }

        /// <summary>
        /// điểm phạt khi trả lời sai
        /// </summary>
        [JsonProperty(PropertyName = "plt")]
        public int Penalty { get; set; }
        /// <summary>
        /// đối tượng chứa nhóm câu trả lời
        /// </summary>
        [JsonProperty(PropertyName = "ft")]
        public string FeedbackType { get; set; }
    }



    public class HTMLQuestionConfig
    {
        /// <summary>
        /// Loại câu hỏi
        /// </summary>
        [JsonProperty(PropertyName = "tq")]
        public QuestionType TypeQuestion { get; set; }

        /// <summary>
        /// Nhóm câu hỏi
        /// </summary>
        [JsonProperty(PropertyName = "gt")]
        public Quizz.GroupType GroupType { get; set; }


        /// <summary>
        /// có xáo trộn đáp án hay không
        /// </summary>
        [JsonProperty(PropertyName = "sff")]
        public bool IsShuffle { get; set; }

        /// <summary>
        /// Số lần lặp lại
        /// </summary>
        [JsonProperty(PropertyName = "atm")]
        public int Attemps { get; set; }

        /// <summary>
        /// điểm của từng câu hỏi
        /// </summary>
        [JsonProperty(PropertyName = "p")]
        public int Points { get; set; }

        /// <summary>
        /// điểm phạt khi trả lời sai
        /// </summary>
        [JsonProperty(PropertyName = "plt")]
        public int Penalty { get; set; }
        /// <summary>
        /// đối tượng chứa nhóm câu trả lời
        /// </summary>
       // public HTMLAnswersContent AnswerContent { get; set; }
    }

    /// <summary>
    /// lớp câu cơ sở cho đối tượng
    /// </summary>
    public class HTMLAnswersContent : HTMLStandardElement
    {
        /// <summary>
        /// list các câu trả lời
        /// </summary>
        [JsonProperty(PropertyName = "as")]
        public List<HTMLAnswerBase> Answers { get; set; }
    }

    public class HTMLTextInputContent : HTMLAnswersContent
    {
        [JsonProperty(PropertyName = "tip")]
        public HTMLTextInput TextInput { get; set; }

        [JsonProperty(PropertyName = "f")]
        public HTMLColorBase Fill { get; set; }
    }

    public class HTMLTextInput
    {
        [JsonProperty(PropertyName = "txt")]
        public string Text { get; set; }

        [JsonProperty(PropertyName = "fs")]
        public double FontSize { get; set; }

        [JsonProperty(PropertyName = "ff")]
        public string FontFamily { get; set; }

        [JsonProperty(PropertyName = "fg")]
        public HTMLColorBase Foreground { set; get; }

        [JsonProperty(PropertyName = "talg")]
        public string TextAlign { set; get; }

        [JsonProperty(PropertyName = "tvt")]
        public string TextVertical { set; get; }
    }

    public class WordBankAnswers : HTMLAnswersContent
    {
        [JsonProperty(PropertyName = "wb")]
        public HTMLWordBankContent Wordbank { get; set; }
    }


    /// <summary>
    /// 
    /// </summary>
    public class HTMLAnswerBase : HTMLStandardElement
    {
        /// <summary>
        /// list các word của Text
        /// </summary>
       // [JsonProperty(PropertyName = "cht")]
        private QuestionText choiceText;

        [JsonProperty(PropertyName = "ct")]
        public QuestionText ChoiceText
        {
            get { return choiceText ?? (choiceText = new QuestionText()); }
            set { choiceText = value; }
        }

        /// <summary>
        /// Thông tin vị trí khung text
        /// </summary>
        [JsonProperty(PropertyName = "ch")]
        public HTMLPosition Choice { get; set; }
        /// <summary>
        /// đáp án đúng 
        /// </summary>
        [JsonProperty(PropertyName = "cr")]
        public bool Correct { get; set; }
    }

    public class QuestionText
    {
        /// <summary>
        /// Vị trí của đối tượng
        /// </summary>
        [JsonProperty(PropertyName = "T")]
        public double Top { get; set; }
        [JsonProperty(PropertyName = "l")]
        public double Left { get; set; }
        /// <summary>
        /// Text
        /// </summary>
        private List<HTMLParagraph> paragraphs;
        [JsonProperty(PropertyName = "pr")]
        public List<HTMLParagraph> Paragraphs
        {
            get { return paragraphs ?? (paragraphs = new List<HTMLParagraph>()); }
            set { paragraphs = value; }
        }
        /// <summary>
        /// Reference Text
        /// </summary>
        [JsonProperty(PropertyName = "ref")]
        public HTMLReference Reference { get; set; }

    }

    public class VariableResult
    {
        public VariableResult(string iD, ResultType type)
        {
            ID = iD;
            Type = type;
        }

        [JsonProperty(PropertyName = "id")]
        public string ID { get; set; }
        [JsonProperty(PropertyName = "typ")]
        [JsonConverter(typeof(StringEnumConverter))]
        public ResultType Type { get; set; }
    }


}
