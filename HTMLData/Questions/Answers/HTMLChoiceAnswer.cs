﻿using Newtonsoft.Json;

namespace INV.Elearning.HTMLHelper.HTMLData
{
    public class HTMLChoiceAnswer : HTMLAnswerBase
    {
        [JsonProperty(PropertyName = "rB")]
        public HTMLRadioButton RadioButton { get; set; }
    }
}
