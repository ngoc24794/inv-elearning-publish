﻿using Newtonsoft.Json;

namespace INV.Elearning.HTMLHelper.HTMLData
{
    public class HTMLCheckBoxAnswer: HTMLAnswerBase
    {
        [JsonProperty(PropertyName = "chb")]
        public HTMLCheckBox CheckBox { get; set; }
    }
}
