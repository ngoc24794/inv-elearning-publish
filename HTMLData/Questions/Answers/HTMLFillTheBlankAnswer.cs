﻿using Newtonsoft.Json;

namespace INV.Elearning.HTMLHelper
{
    class HTMLFillTheBlankAnswer : HTMLAnswerBase
    {
        [JsonProperty(PropertyName = "txt")]
        public string Text { get; set; }

        [JsonProperty(PropertyName = "ti")]
        public HTMLTextInput TextInput { get; set; }
    }
}
