﻿using Newtonsoft.Json;

namespace INV.Elearning.HTMLHelper.HTMLData
{
    public class HTMLMatchingDropDownAnswer:HTMLAnswerBase
    {
        /// <summary>
        /// Thông tị vị trí phần comboxbox
        /// </summary>
        [JsonProperty(PropertyName = "Cb")]
        public HTMLPosition Combobox { get; set; }
        
        private QuestionText _comboboxText;
        /// <summary>
        /// text chứa trong combobox
        /// </summary>
        [JsonProperty(PropertyName = "cbT")]
        public QuestionText ComboboxText
        {
            get { return _comboboxText??(_comboboxText=new QuestionText()); }
            set { _comboboxText = value; }
        }
    }
}
