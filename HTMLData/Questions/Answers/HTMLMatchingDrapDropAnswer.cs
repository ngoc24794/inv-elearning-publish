﻿using INV.Elearning.HTMLHelper.HTMLData;
using INV.Elearning.HTMLHelper.HTMLData.ColorClass;
using INV.Elearning.HTMLHelper.HTMLData.TextClass;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace INV.Elearning.HTMLHelper
{
    public class HTMLMatchingDrapDropAnswer : HTMLAnswerBase
    {
        /// <summary>
        /// list các word của Text
        /// </summary>
        [JsonProperty(PropertyName = "mT")]
        private QuestionText matchText;

        public QuestionText MatchText
        {
            get { return matchText ?? (matchText = new QuestionText()); }
            set { matchText = value; }
        }

        /// <summary>
        /// Thông tin vị trí của Match
        /// </summary>
        [JsonProperty(PropertyName = "M")]
        public HTMLPosition Match { get; set; }
        /// <summary>
        /// path
        /// </summary>
        /// 
        [JsonProperty(PropertyName = "cP")]
        public string ChoicePath { get; set; }
        [JsonProperty(PropertyName = "mP")]
        public string matchPath { get; set; }

        [JsonProperty(PropertyName = "mC")]
        public HTMLColorBase MatchColor { get; set; }
        [JsonProperty(PropertyName = "cC")]
        public HTMLColorBase ChoiceColor { get; set; }
        [JsonProperty(PropertyName = "cim")]
        public HTMLAnswerImage ChoiceImage { get; set; }

        [JsonProperty(PropertyName = "mim")]
        public HTMLAnswerImage MatchImage { get; set; }
    }
    public class HTMLAnswerImage
    {
        /// <summary>
        /// Chiều rộng của đối tượng
        /// </summary>
        [JsonProperty(PropertyName = "w")]
        public double Width { get; set; }
        /// <summary>
        /// Chiều dài của đối tượng
        /// </summary>
        [JsonProperty(PropertyName = "h")]
        public double Height { get; set; }
        /// <summary>
        /// Vị trí của đối tượng
        /// </summary>
        [JsonProperty(PropertyName = "T")]
        public double Top { get; set; }
        [JsonProperty(PropertyName = "l")]
        public double Left { get; set; }

        /// <summary>
        /// đường dẫn ảnh
        /// </summary>
        [JsonProperty(PropertyName = "s")]
        public string Source { get; set; }
    }

}