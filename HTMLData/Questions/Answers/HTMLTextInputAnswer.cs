﻿using Newtonsoft.Json;

namespace INV.Elearning.HTMLHelper.HTMLData
{
    public class HTMLTextInputAnswer :HTMLAnswerBase
    {
        [JsonProperty(PropertyName = "txt")]
        public string  Text { get; set; }
    }
}
