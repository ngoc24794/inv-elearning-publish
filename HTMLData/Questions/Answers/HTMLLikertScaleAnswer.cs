﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace INV.Elearning.HTMLHelper.HTMLData
{
    public class HTMLLikertScaleAnswer : HTMLAnswerBase
    {
        /// <summary>
        /// LikertsContent
        /// </summary>
        [JsonProperty(PropertyName = "lC")]
        public HTMLPosition LikertsContent { get; set; }
        /// <summary>
        /// Scales
        /// </summary>
        [JsonProperty(PropertyName = "S")]
        public List<HTMLScale> Scales { get; set; }
    }

    public class HTMLScale
    {
        /// <summary>
        /// Scales
        /// </summary>
        [JsonProperty(PropertyName = "txt")]
        public string Text { get; set; }
        /// <summary>
        /// Scales
        /// </summary>
        [JsonProperty(PropertyName = "ch")]
        public HTMLRadioButton Choice { get; set; }
    }

}
