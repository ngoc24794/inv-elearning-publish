﻿using Newtonsoft.Json;

namespace INV.Elearning.HTMLHelper.HTMLData
{
    public class HTMLWordBankAnswer : HTMLAnswerBase
    {
        /// <summary>
        /// đường bo góc của checkbox
        /// </summary>
        [JsonProperty(PropertyName = "cRad")]
        public double ConerRadius { get; set; }

    }

    public class HTMLWordBankContent : HTMLPosition
    {
        /// <summary>
        /// đường bo góc của checkbox
        /// </summary>
        [JsonProperty(PropertyName = "cRad")]
        public double ConerRadius { get; set; }
    }
}
