﻿using INV.Elearning.HTMLHelper.HTMLData.ColorClass;
using Newtonsoft.Json;

namespace INV.Elearning.HTMLHelper.HTMLData
{
    public class HTMLRadioButton
    {
        /// <summary>
        /// left của hình
        /// </summary>
        [JsonProperty(PropertyName = "lR")]
        public double LeftRadioButton { get; set; }
        /// <summary>
        /// top của hình
        /// </summary>
        [JsonProperty(PropertyName = "tR")]
        public double TopRadioButton { get; set; }
        /// <summary>
        /// màu của dấu tích vào câu hỏi
        /// </summary>
        [JsonProperty(PropertyName = "C")]
        public HTMLColorBase ColorCircleInside { get; set; }
        /// <summary>
        /// màu của border
        /// </summary>
        [JsonProperty(PropertyName = "cB")]
        public HTMLColorBase ColorBorderOut { get; set; }
        /// <summary>
        /// màu khi click vào
        /// </summary>
        [JsonProperty(PropertyName = "cC")]
        public HTMLColorBase ColorCircleChecked { get; set; }
        /// <summary>
        /// chiều dài của border
        /// </summary>
        [JsonProperty(PropertyName = "wB")]
        public double WidthBorderOut { get; set; }
        ///bán kính đường tròn ngoài
        [JsonProperty(PropertyName = "r")]
        public double RadiusCircleOut { get; set; }
        /// <summary>
        /// bán kính đường tròn ở giữa
        /// </summary>
        [JsonProperty(PropertyName = "rC")]
        public double RadiusCircleCheck { get; set; }
        /// <summary>
        /// bán kính đường tròn bên trong
        /// </summary>
        [JsonProperty(PropertyName = "rCi")]
        public double RadiusCircleInside { get; set; }

    }
}
