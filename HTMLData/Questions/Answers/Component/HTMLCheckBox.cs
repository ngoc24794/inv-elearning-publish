﻿using INV.Elearning.HTMLHelper.HTMLData.ColorClass;
using Newtonsoft.Json;

namespace INV.Elearning.HTMLHelper.HTMLData
{
    public class HTMLCheckBox
    {
        /// <summary>
        /// left của hình
        /// </summary>
        [JsonProperty(PropertyName = "lC")]
        public double LeftCheckBox { get; set; }
        /// <summary>
        /// top của hình
        /// </summary>
        [JsonProperty(PropertyName = "tC")]
        public double TopCheckBox { get; set; }
        /// <summary>
        /// màu của dấu tích vào câu hỏi
        /// </summary>
        [JsonProperty(PropertyName = "cC")]
        public HTMLColorBase ColorCheckBoxInside { get; set; }
        /// <summary>
        /// chiều dài của hình chử nhật bên trong
        /// </summary>
        [JsonProperty(PropertyName = "wC")]
        public double WidthCheckBoxInside { get; set; }

        /// <summary>
        /// lấy width hình chử nhật ở ngoài 
        /// </summary>
        [JsonProperty(PropertyName = "wCo")]
        public double WidthCheckBoxOut { get; set; }
        /// <summary>
        /// màu của border
        /// </summary>
        [JsonProperty(PropertyName = "cB")]
        public HTMLColorBase ColorBorderOut { get; set; }
        /// <summary>
        /// chiều dài của border
        /// </summary>
        [JsonProperty(PropertyName = "wB")]
        public double WidthBorderOut { get; set; }
        /// <summary>
        /// đường path khi click vào
        /// </summary>
        [JsonProperty(PropertyName = "pC")]
        public string PathChecked { get; set; }
        /// <summary>
        /// màu của đường path
        /// </summary>
        [JsonProperty(PropertyName = "cP")]
        public HTMLColorBase ColorPath { get; set; }
        /// <summary>
        /// đường bo góc của checkbox
        /// </summary>
        [JsonProperty(PropertyName = "cR")]
        public double CornerRadius { get; set; }
    }
}
