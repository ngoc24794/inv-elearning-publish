﻿using Newtonsoft.Json;

namespace INV.Elearning.HTMLHelper.HTMLData
{
    public class HTMLPosition
    {
        /// <summary>
        /// Chiều rộng của đối tượng
        /// </summary>
        [JsonProperty(PropertyName = "w")]
        public double Width { get; set; }
        /// <summary>
        /// Chiều dài của đối tượng
        /// </summary>
        [JsonProperty(PropertyName = "h")]
        public double Height { get; set; }
        /// <summary>
        /// Vị trí của đối tượng
        /// </summary>
        [JsonProperty(PropertyName = "T")]
        public double Top { get; set; }
        [JsonProperty(PropertyName = "l")]
        public double Left { get; set; }
        /// <summary>
        /// Z Index của đối tượng
        /// </summary>
        [JsonProperty(PropertyName = "z")]
        public int ZIndex { get; set; }
        /// <summary>
        /// Animation (mọi đối tượng đều có hiệu ứng)
        /// </summary>
       // [JsonProperty(PropertyName = "anis")]

        /// <summary>
        /// Loại đối tượng
        /// </summary>
        [JsonProperty(PropertyName = "cKey")]
        public string ContentKeyType { get; set; }

        /// <summary>
        /// ID của đối tượng
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public string ID { get; set; }
    }
}
