﻿using INV.Elearning.Quizz;
using Newtonsoft.Json;

namespace INV.Elearning.HTMLHelper.HTMLData.Questions.Answers
{
    public class HTMLNumericValue
    {
        [JsonProperty(PropertyName = "v1")]
        public double Value1 { get; set; }
        [JsonProperty(PropertyName = "v2")]
        public double Value2 { get; set; }
        [JsonProperty(PropertyName = "Cp")]
        public Compare Compare { set; get; }
    }
}
