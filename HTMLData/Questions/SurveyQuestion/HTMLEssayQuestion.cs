﻿using Newtonsoft.Json;

namespace INV.Elearning.HTMLHelper.HTMLData
{
    public class HTMLEssayQuestion : HTMLQuestionBase
    {
        /// <summary>
        /// chiều dài tối đa ký tự
        /// </summary>
        [JsonProperty(PropertyName = "ml")]
        public int MaxLength { get; set; }
    }
}
