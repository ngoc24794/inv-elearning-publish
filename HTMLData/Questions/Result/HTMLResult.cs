﻿using INV.Elearning.HTMLHelper.HTMLData.SlideBase;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace INV.Elearning.HTMLHelper.HTMLData
{
    public class HTMLResultSlide : HTMLSlideBase
    {
        /// <summary>
        /// loại
        /// </summary>
        [JsonProperty(PropertyName = "c")]
        public string ContentKeyType { get; set; }

        /// <summary>
        /// pasing
        /// </summary>
        [JsonProperty(PropertyName = "ps")]
        public double Passing { get; set; }

        /// <summary>
        /// Loại Result
        /// </summary>
        [JsonProperty(PropertyName = "iG")]
        public bool IsGradedResult { get; set; }

        /// <summary>
        /// có giới hạn thời gian hay ko?
        /// </summary>
        [JsonProperty(PropertyName = "iT")]
        public bool IsTimeLimit { get; set; }

        /// <summary>
        /// có giới hạn thời gian hay ko?
        /// </summary>
        [JsonProperty(PropertyName = "iS")]
        public bool IsShowResultSlide { get; set; }

        /// <summary>
        /// Thời gian bài quiz
        /// </summary>
        [JsonProperty(PropertyName = "du")]
        public double Duration { get; set; }
        /// <summary>
        /// Số lần lắp lại câu hỏi
        /// </summary>
        [JsonProperty(PropertyName = "atm")]
        public int Attemps { get; set; }

        /// <summary>
        /// List ID các câu hỏi có trong Result
        /// </summary>
        [JsonProperty(PropertyName = "q")]
        public List<string> Questions { get; set; }

        [JsonProperty(PropertyName = "vars")]
        public List<VariableResult> Variables { get; set; }

        [JsonProperty(PropertyName = "stm")]
        // cấu hình slide bắt đầu đếm thời gian
        public string StartTimer { get; set; }

        [JsonProperty(PropertyName = "tfm")]
        //định dạng đếm thời gian
        public string TimerFormat { get; set; }
    }
}
