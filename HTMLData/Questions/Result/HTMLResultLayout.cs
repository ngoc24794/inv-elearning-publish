﻿using INV.Elearning.HTMLHelper.HTMLData.LayoutBase;
using Newtonsoft.Json;

namespace INV.Elearning.HTMLHelper.HTMLData
{
    public class HTMLResultLayout : HTMLLayoutBase
    {
        /// <summary>
        /// ContentType
        /// </summary>
        [JsonProperty(PropertyName = "lKt")]
        public string LayoutKeyType { get; set; }
    }
}
