﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INV.Elearning.HTMLHelper.HTMLData
{
    public class HTMLTransition
    {
        /// <summary>
        /// Tên của hiệu ứng
        /// </summary>
        [JsonProperty(PropertyName = "nm")]
        public string Name { get; set; }
        /// <summary>
        /// Nhóm các tùy chỉnh của hiệu ứng đối tượng
        /// </summary>
        [JsonProperty(PropertyName = "effOpt")]
        public string EffectOption { get; set; }
        /// <summary>
        /// Thời gian diễn ra hiệu ứng đối tượng
        /// </summary>
        [JsonProperty(PropertyName = "durTra")]
        public double HTMLDurationTransition { get; set; }
    }
}
